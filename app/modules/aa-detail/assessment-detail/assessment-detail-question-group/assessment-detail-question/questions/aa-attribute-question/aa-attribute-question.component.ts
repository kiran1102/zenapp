// 3rd Party
import {
    Component,
    Inject,
    Input,
    EventEmitter,
    Output,
    OnChanges,
    SimpleChanges,
    OnInit,
} from "@angular/core";
import {
    includes,
    map,
    filter,
    forEach,
    find,
} from "lodash";
import moment from "moment";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// RXJS
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

// Redux
import {
    getSubQuestionResponseIds,
    getSubQuestionsInOrder,
    getSubQuestionResponseModels,
    AssessmentDetailAction,
    getAssessmentDetailStatus,
    getAssessmentDetailInventoryAttributeUserOptions,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AAQuestionCommentViewLogicService } from "modules/assessment/services/view-logic/aa-question-comment-view-logic.service";
import { isValidGuidOrEmail } from "modules/utils/index";
import { AAIncidentAdapterService } from "modules/assessment/services/adapter/aa-incident-adapter.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Constants
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";
import {
    AttributeType,
    AAOtherOptionConfig,
    AttributeQuestionResponseType,
    AttributeQuestionNameKey,
    QuestionTypes,
} from "constants/question-types.constant";
import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { QuestionAnswerTypes } from "enums/question-types.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStore } from "interfaces/redux.interface";
import { IQuestionResponseV2 } from "interfaces/assessment/question-response-v2.interface";
import { IUser } from "interfaces/user.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IIncidentJurisdiction } from "incidentModule/shared/interfaces/incident-jurisdiction.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

@Component({
    selector: "aa-attribute-question",
    templateUrl: "./aa-attribute-question.component.html",
})
export class AAAttributeQuestionComponent implements OnChanges, OnInit {
    @Input() isDisabled = false;
    @Input() question: IQuestionModel;

    @Output() datetimeChange = new EventEmitter();
    @Output() handleNotSureNotApplicable = new EventEmitter();
    @Output() handleOtherForButtons = new EventEmitter();
    @Output() justificationChange = new EventEmitter();
    @Output() multichoiceChange = new EventEmitter();
    @Output() multichoiceReset = new EventEmitter();
    @Output() multiselectChange = new EventEmitter();
    @Output() jurisdictionChange = new EventEmitter();
    @Output() otherChange = new EventEmitter();
    @Output() textboxUpdated = new EventEmitter();

    otherConfig = AAOtherOptionConfig;
    newOtherConfig = { ...this.otherConfig, clearOnSelect: true };
    newOtherModel = "";
    subQuestions: IQuestionModelSubQuestion[];
    userAttributeType = AttributeType.USERS;
    users: IUser[] = [];
    canShowCommentButton: boolean;
    questionAnswerTypes = QuestionAnswerTypes;
    invalidOption: IQuestionModelOption;
    questionTypes = QuestionTypes;
    attributeQuestionResponseType = AttributeQuestionResponseType;
    attributeQuestionNameKey = AttributeQuestionNameKey;
    isMultiChoiceButton = false;
    isAssessmentCompleted = false;

    private destroy$ = new Subject();

    constructor(
        @Inject("$rootScope") public rootScope: IExtendedRootScopeService,
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private AAQuestionCommentViewLogic: AAQuestionCommentViewLogicService,
        private assessmentDetailAction: AaDetailActionService,
        private aAIncidentAdapterService: AAIncidentAdapterService,
        private notificationService: NotificationService,
    ) { }

    ngOnInit() {
        this.isAssessmentCompleted = getAssessmentDetailStatus(this.store.getState()) === AssessmentStatus.Completed;
        if (this.question.attributes.allowOther && !this.isAssessmentCompleted
            && (this.question.attributes.responseType === AttributeQuestionResponseType.SINGLE_SELECT
            || (this.question.attributes.responseType === AttributeQuestionResponseType.MULTI_SELECT && !this.isJurisdictionAttribute()))) {
            forEach(this.subQuestions, (subQuestion: IQuestionModelSubQuestion) => {
                let invalidOptionExists = false;
                forEach(subQuestion.responses.allIds, (responseId: string) => {
                    if (!find(this.question.options, (option: IQuestionModelOption) => option.id === responseId)
                        && subQuestion.responses.byId[responseId].type !== QuestionResponseType.Others) {
                        subQuestion.responses.byId[responseId].type = QuestionResponseType.Others;
                        invalidOptionExists = true;
                    }
                });
                if (invalidOptionExists) {
                    this.store.dispatch({ type: AssessmentDetailAction.UPDATE_SUB_QUESTION_RESPONSES, updatedResponses: subQuestion.responses, questionId: this.question.id, subQuestionId: subQuestion.parentAssessmentDetailId });
                    this.assessmentDetailAction.markQuestionToBeSaved(this.question.id, subQuestion.parentAssessmentDetailId);
                }
            });
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.canShowCommentButton = this.AAQuestionCommentViewLogic.permissionCheckForComment();
        if (changes.question) {

            if (this.aAIncidentAdapterService.isIncidentAttribute(this.question)) {
                this.componentWillReceiveState(this.aAIncidentAdapterService.setIncidentAttributeMessage(this.question));
            } else {
                this.componentWillReceiveState();
            }

            if (this.isUserAttribute(this.question)) {
                this.handleUserTypeAttribute();
            }

            if (this.question.attributes && this.question.attributes.displayAnswerType === QuestionAnswerTypes.Button
                && (this.question.attributes.responseType === AttributeQuestionResponseType.SINGLE_SELECT
                    || (this.question.attributes.responseType === AttributeQuestionResponseType.MULTI_SELECT && !this.isJurisdictionAttribute()))) {
                        this.isMultiChoiceButton = true;
                    }
        }

        if (!this.question.attributes.allowOther && !this.isAssessmentCompleted) {
            forEach(this.subQuestions, (subQuestion: IQuestionModelSubQuestion) => {
                forEach(subQuestion.responses.allIds, (responseId: string) => {
                    if (!find(this.question.options, (option: IQuestionModelOption) => option.id === responseId)
                        && subQuestion.responses.byId[responseId]) {
                        subQuestion.responses.byId[responseId].valid = false;
                    }
                });
            });
        }
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    isJurisdictionAttribute(): boolean {
        return (this.question.attributes && this.question.attributes.responseType === AttributeQuestionResponseType.MULTI_SELECT) && (this.question.attributes && this.question.attributes.nameKey === AttributeQuestionNameKey.JURISDICTIONS);
    }

    isResponseSelected(optionId: string, subQuestionId: string): boolean {
        const responseIds = getSubQuestionResponseIds(this.question.id, subQuestionId)(this.store.getState());
        return includes(responseIds, optionId);
    }

    trackByFn(index: number): number {
        return index;
    }

    shouldShowOtherOption(subQuestion: IQuestionModelSubQuestion): boolean {
        if (this.question && this.question.attributes && !this.question.attributes.isMultiSelect) {
            if (!subQuestion.responses || !subQuestion.responses.allIds.length) {
                return true;
            } else if (subQuestion.responses && subQuestion.responses.allIds.length) {
                const responseId = subQuestion.responses.allIds[0];
                const responseSubQuestionModels = getSubQuestionResponseModels(this.question.id, subQuestion.parentAssessmentDetailId)(this.store.getState());
                const response: IQuestionResponseV2 = responseSubQuestionModels[responseId];
                // If there is a response selected, but that response isn't Other, show the Other button
                return response.type !== QuestionResponseType.Others;
            }
        } else {
            return true;
        }
    }

    onTextboxUpdated(event: { response: string, responseId: string }, subQuestionId: string): void {
        if (this.textboxUpdated.observers.length) {
            this.textboxUpdated.emit({ ...event, subQuestionId });
        }
    }

    onMultiselectChange(event: { options: IQuestionModelOption[] }, subQuestionId: string): void {
        if (this.multiselectChange.observers.length) {
            this.multiselectChange.emit({ ...event, subQuestionId });
        }
    }

    onMultichoiceChange(event: { option: IQuestionModelOption }, subQuestionId: string): void {
        if (this.multichoiceChange.observers.length) {
            this.multichoiceChange.emit({ ...event, subQuestionId });
        }
    }

    onOtherChange(event: { response: string }, subQuestionId: string): void {
        if (this.isUserAttribute(this.question) && !isValidGuidOrEmail(event.response)) {
            this.notificationService.alertWarning(this.translatePipe.transform("Warning"), this.translatePipe.transform("EnterAValidEmailAddress"));
            return;
        }

        if (this.otherChange.observers.length) {
            this.otherChange.emit({ ...event, subQuestionId });
        }
    }

    onJustificationChange(event: { response: string, responseId: string }, subQuestionId: string): void {
        if (this.justificationChange.observers.length) {
            this.justificationChange.emit({ ...event, subQuestionId });
        }
    }

    onMultichoiceReset(subQuestionId: string): void {
        if (this.multichoiceReset.observers.length) {
            this.multichoiceReset.emit(subQuestionId);
        }
    }

    onHandleNotSureNotApplicable(event: { option: IQuestionModelOption }, subQuestionId: string): void {
        if (this.handleNotSureNotApplicable.observers.length) {
            this.handleNotSureNotApplicable.emit({ ...event, subQuestionId });
        }
    }

    onHandleOtherButtonEdit(response: IQuestionResponseV2, subQuestionId: string): void {
        this.handleOtherForButtons.emit({ response: response.response, responseId: response.responseId, subQuestionId });
    }

    onDateChange(event: { value: string, responseId: string }, questionId: string, subQuestionId: string, responseType: string): void {
        if (this.datetimeChange.observers.length) {
            if (event.value && responseType === this.attributeQuestionResponseType.DATE) {
                event.value = moment(event.value).format("YYYY-MM-DD");
            }
            this.datetimeChange.emit({ ...event, questionId, subQuestionId });
        }
    }

    saveJurisdictions(jurisdictions: IIncidentJurisdiction[], subQuestionId: string) {
        if (this.jurisdictionChange.observers.length) {
            this.jurisdictionChange.emit({ jurisdictions, subQuestionId });
        }
    }

    getDisplayLabel(subQuestions: IQuestionModelSubQuestion[], subQuestion: IQuestionModelSubQuestion): string {
       return subQuestions.length > 1 ? subQuestion.displayLabel : null;
    }

    private isUserAttribute(question: IQuestionModel): boolean {
        return question.attributes.attributeType === AttributeType.USERS;
    }

    private handleUserTypeAttribute(): void {
        const activeUsers = getAssessmentDetailInventoryAttributeUserOptions(this.store.getState());
        if (activeUsers && activeUsers.length) {
            const configuredUserAttributeOptions = filter(this.question.options, (userOption: IQuestionModelOption): boolean => {
                return userOption.optionType !== QuestionResponseType.Default;
            });
            this.question.options = map(activeUsers, (user: IOrgUserAdapted): IQuestionModelOption => {
                return {
                    id: user.Id,
                    option: user.FullName,
                    optionType: QuestionResponseType.Default,
                    sequence: null,
                };
            });
            if (configuredUserAttributeOptions && configuredUserAttributeOptions.length > 0) {
                this.question.options.push(...configuredUserAttributeOptions);
            }
        }
    }

    private componentWillReceiveState(displayLabel: string = null): void {
        if (displayLabel) {
            this.subQuestions = forEach(getSubQuestionsInOrder(this.question.id)(this.store.getState()), (subQuestion: IQuestionModelSubQuestion) => {
                subQuestion.displayLabel = displayLabel;
            });
        } else {
            this.subQuestions = getSubQuestionsInOrder(this.question.id)(this.store.getState());
        }
    }
}
