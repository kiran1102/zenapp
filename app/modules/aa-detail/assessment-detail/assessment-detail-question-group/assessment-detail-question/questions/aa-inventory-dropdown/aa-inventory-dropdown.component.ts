// Rxjs
import { Subject } from "rxjs";
import {
    takeUntil,
    debounceTime,
    map,
} from "rxjs/operators";

// 3rd Party
import {
    Component,
    Inject,
    Input,
    OnInit,
    EventEmitter,
    Output,
    OnChanges,
    SimpleChanges,
} from "@angular/core";
import {
    trim,
    sortBy,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getInventoryOptionsArray } from "modules/assessment/reducers/assessment-detail.reducer";
import { getIsIE11 } from "oneRedux/reducers/setting.reducer";

// Services
import {
    AssessmentMultichoiceDropdownViewLogicService,
    IMultichoiceDropdownSelection,
} from "modules/assessment/services/view-logic/assessment-multichoice-dropdown-view-logic.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Interfaces
import {
    IQuestionModel,
    IQuestionModelOption,
} from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";
import { IKeyValue } from "interfaces/generic.interface";

// Enums / Constants
import { QuestionResponseType } from "constants/question-types.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-inventory-dropdown",
    templateUrl: "./aa-inventory-dropdown.component.html",
})
export class AAInventoryDropdownComponent implements OnInit, OnChanges {

    @Input() disabled = false;
    @Input() question: IQuestionModel;

    @Output() multichoiceChange = new EventEmitter();
    @Output() multiselectChange = new EventEmitter();
    @Output() multichoiceReset = new EventEmitter();
    @Output() otherChange = new EventEmitter();

    allowHint = false;
    allowOther = false;
    allowOtherOptionValue = "";
    allowOtherPrependText = "AddOption";
    applySingleSelectInputStyle = false;
    applySingleSelectInvalidInputStyle = false;
    applySingleSelectOtherInputStyle = false;
    checkForDuplicates = true;
    defaultOptions: IQuestionModelOption[];
    errorText = "Error";
    inputValue: string;
    invalidResponses: IQuestionModelOption[];
    isIE11 = false;
    labelKey = "optionDisplay";
    multiSelect = false;
    placeholder = "";
    response: IQuestionModelOption;
    responses: IQuestionModelOption[];
    showAllowOtherOption = false;
    showError = false;
    validPillKey = "valid";

    input$ = new Subject<string>();
    loading$ = new Subject<boolean>();
    filteredOptions$ = new Subject<IQuestionModelOption[]>();
    destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentDetailAction: AaDetailActionService,
        private DropdownService: AssessmentMultichoiceDropdownViewLogicService,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.isIE11 = getIsIE11(this.store.getState());
        this.placeholder = (this.disabled || this.isIE11) ? "" : this.translatePipe.transform("TypeOrSelectAnOption");

        this.input$
            .asObservable().pipe(
                takeUntil(this.destroy$),
                debounceTime(300),
                map((val = "") => this.getFilteredOptions(val)),
            ).subscribe((options: IQuestionModelOption[]) => {
                this.filteredOptions$.next(options);
                this.loading$.next(false);
            });

        this.input$.next("");
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            this.componentWillReceiveState();
        }
    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    onModelChange(payload: IMultichoiceDropdownSelection): void {
        if (this.disabled) return;
        if (this.multiSelect) {
            this.multiselectChange.emit({ options: payload.currentValue as IQuestionModelOption[] });
        } else {
            if (payload.currentValue) {
                this.multichoiceChange.emit({ option: payload.currentValue as IQuestionModelOption });
            } else {
                this.multichoiceReset.emit();
            }
        }
        this.resetAllowOtherOption();
        this.resetOptions();
    }

    onInputChange(event: IKeyValue<string>): void {
        const { key, value } = event;
        this.inputValue = value;

        if (!key && !value) {
            if (!this.multiSelect && !this.response) {
                this.resetAllowOtherOption();
                this.resetOptions();
                return this.assessmentDetailAction.removeQuestionResponses(this.question.id);
            } else {
                return;
            }
        }
        if (this.disabled) {
            this.resetAllowOtherOption();
            return;
        }
        if (key === "Enter") {
            this.addOtherOption();
            this.resetAllowOtherOption();
            this.resetOptions();
            return;
        }
        if (this.allowOther) {
            this.showAllowOtherOption = Boolean(value.length);
            this.allowOtherOptionValue = value;
        } else {
            if (!this.response) {
                this.resetAllowOtherOption();
            }
        }

        this.input$.next(event.value);
        this.loading$.next(true);
    }

    addOtherOption(): void {
        if (!this.allowOther || !this.inputValue) return;
        const inputVal: string = trim(this.inputValue);
        const hasDuplicateResponse: boolean = this.checkForDuplicateResponse(inputVal);
        const hasDuplicateOption: boolean = this.checkForDuplicateOption(inputVal);

        if (hasDuplicateResponse || hasDuplicateOption) {
            this.notificationService.alertWarning(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateOptionsNotAllowed"));
            this.resetAllowOtherOption();
        } else {
            this.addNewOptionToModel(inputVal);
        }
    }

    checkForDuplicateResponse(inputVal: string): boolean {
        if (this.responses && this.responses.length) {
            return this.DropdownService.checkForDuplicateOptions(inputVal, this.responses, this.labelKey);
        } else {
            return false;
        }
    }

    checkForDuplicateOption(inputVal: string): boolean {
        return this.DropdownService.checkForDuplicateInventoryOptions(inputVal, this.question.attributes.inventoryType, "option");
    }

    resetAllowOtherOption(): void {
        this.applySingleSelectInputStyle = false;
        this.applySingleSelectOtherInputStyle = false;
        this.showAllowOtherOption = false;
        this.allowOtherOptionValue = "";
    }

    resetOptions(): void {
        this.input$.next("");
    }

    addNewOptionToModel(inputVal: string): void {
        this.resetAllowOtherOption();
        this.resetOptions();
        this.otherChange.emit({ response: inputVal });
    }

    getFilteredOptions(val = ""): IQuestionModelOption[] {
        if (val.length > 1) {
            return this.DropdownService.filterOptionsByInputValue(val, this.labelKey, this.defaultOptions);
        } else {
            return this.defaultOptions;
        }
    }

    private componentWillReceiveState(): void {
        if (this.question && this.question.id) {
            this.defaultOptions = getInventoryOptionsArray(this.question.attributes.inventoryType)(this.store.getState());
            this.multiSelect = !!this.question.attributes.isMultiSelect;
            this.allowOther = this.DropdownService.canAllowOther(this.question);
            this.allowHint = this.DropdownService.canAllowHint(this.question);

            if (this.multiSelect) {
                const { selectedOptions, invalidOptions } = this.DropdownService.formatMultiSelectInventoryResponses(this.question.id, this.question.attributes.inventoryType);
                this.responses = sortBy(selectedOptions, "optionDisplay");
                this.invalidResponses = invalidOptions;
            } else {
                const { selectedOption, invalidOptions } = this.DropdownService.formatSingleSelectInventoryResponse(this.question.id, this.question.attributes.inventoryType);
                this.response = selectedOption;
                this.invalidResponses = invalidOptions;
                this.applySingleSelectInputStyle = this.response && this.response.optionType === QuestionResponseType.Default;
                this.applySingleSelectOtherInputStyle = this.response && this.response.optionType === QuestionResponseType.Others;
                this.applySingleSelectInvalidInputStyle = Boolean(this.response && this.invalidResponses.length);
            }
        }
    }
}
