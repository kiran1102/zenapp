// Core
import { Component, Input, SimpleChanges, OnChanges } from "@angular/core";

// Services
import { AAIncidentAdapterService } from "modules/assessment/services/adapter/aa-incident-adapter.service";

// Interfaces
import { IQuestionModel } from "interfaces/assessment/question-model.interface";

@Component({
    selector: "aa-incident",
    templateUrl: "./aa-incident.component.html",
})

export class AAIncidentComponent implements OnChanges {

    @Input() question: IQuestionModel;

    message: string;

    constructor(
        private aAIncidentAdapterService: AAIncidentAdapterService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question.currentValue) {
            this.message = this.aAIncidentAdapterService.setIncidentBannerMessage(this.question);
        }
    }

}
