// 3rd Party
import { Component, Inject, Input, OnInit, EventEmitter, Output, OnChanges, SimpleChanges, ChangeDetectorRef } from "@angular/core";
import {
    trim,
    debounce,
    forEach,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getIsIE11 } from "oneRedux/reducers/setting.reducer";

// Services
import {
    AssessmentMultichoiceDropdownViewLogicService,
    IMultichoiceDropdownSelection,
} from "modules/assessment/services/view-logic/assessment-multichoice-dropdown-view-logic.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Interfaces
import {
    IQuestionModel,
    IQuestionModelOption,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";
import { IKeyValue } from "interfaces/generic.interface";

// Enums / Constants
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "aa-multichoice-dropdown",
    templateUrl: "./aa-multichoice-dropdown.component.html",
})
export class AAMultichoiceDropdownComponent implements OnInit, OnChanges {

    @Input() disabled = false;
    @Input() isRequired = false;
    @Input() label = "";
    @Input() question: IQuestionModel;
    @Input() subQuestion: IQuestionModelSubQuestion;

    @Output() handleNotSureNotApplicable = new EventEmitter<IQuestionModelOption>();
    @Output() multichoiceChange = new EventEmitter();
    @Output() multiselectChange = new EventEmitter();
    @Output() multichoiceReset = new EventEmitter();
    @Output() otherChange = new EventEmitter();

    allowHint = false;
    allowOther = false;
    allowOtherOptionValue = "";
    allowOtherPrependText = "AddOption";
    applySingleSelectInputStyle = false;
    applySingleSelectInvalidInputStyle = false;
    applySingleSelectOtherInputStyle = false;
    checkForDuplicates = true;
    defaultOptions: IQuestionModelOption[];
    filteredOptions: IQuestionModelOption[];
    inputValue: string;
    invalidResponses: IQuestionModelOption[];
    isIE11 = false;
    labelKey = "translatedOption";
    multiSelect = false;
    notApplicableOption: IQuestionModelOption = null;
    notSureOption: IQuestionModelOption = null;
    placeholder = "";
    response: IQuestionModelOption;
    responses: IQuestionModelOption[];
    showAllowOtherOption = false;
    validPillKey = "valid";

    filterOptionsByInputValue = debounce((): void => {
        if (this.inputValue && this.inputValue.length > 1) {
            const options = this.multiSelect
                ? this.DropdownService.filterDropdownOptions(this.question.options, this.responses)
                : this.DropdownService.filterDropdownOption(this.question.options, this.response);

            this.filteredOptions = this.tranlsateAttributeDropdownOptions(this.DropdownService.filterOptionsByInputValue(this.inputValue, this.labelKey, options));
        } else {
            this.resetOptions();
        }

        this.changeDetectorRef.markForCheck();
    }, 300);

    constructor(
        @Inject(StoreToken) private store: IStore,
        private assessmentDetailAction: AaDetailActionService,
        private DropdownService: AssessmentMultichoiceDropdownViewLogicService,
        private changeDetectorRef: ChangeDetectorRef,
        private translatePipe: TranslatePipe,
        private notificationService: NotificationService,
    ) { }

    ngOnInit(): void {
        this.isIE11 = getIsIE11(this.store.getState());
        this.placeholder = (this.disabled || this.isIE11) ? "" : this.translatePipe.transform("TypeOrSelectAnOption");

        if (this.question && this.question.options) {
            this.question.options.every((option: IQuestionModelOption) => {
                switch (option.optionType) {
                    case QuestionResponseType.NotSure:
                        this.notSureOption = option;
                        break;
                    case QuestionResponseType.NotApplicable:
                        this.notApplicableOption = option;
                        break;
                }

                return !(this.notSureOption && this.notApplicableOption);
            });
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            this.componentWillReceiveState();
        }
    }

    onModelChange(payload: IMultichoiceDropdownSelection): void {
        if (this.disabled) return;

        if (this.multiSelect) {
            this.multiselectChange.emit({ options: payload.currentValue as IQuestionModelOption[] });
        } else {
            if (payload.currentValue) {
                this.multichoiceChange.emit({ option: payload.currentValue as IQuestionModelOption });
            } else {
                this.multichoiceReset.emit();
            }
        }
        this.resetAllowOtherOption();
    }

    onInputChange(event: IKeyValue<string>): void {
        const { key, value } = event;
        this.inputValue = value;

        if (!key && !value) {
            this.resetAllowOtherOption();
            this.resetOptions();
            if (!this.multiSelect && this.response) {
                return this.assessmentDetailAction.removeQuestionResponses(this.question.id);
            } else {
                return;
            }
        }
        if (this.disabled) {
            this.resetAllowOtherOption();
            return;
        }
        if (key === "Enter") {
            this.addOtherOption();
            this.resetAllowOtherOption();
            this.resetOptions();
            return;
        }
        if (this.allowOther) {
            this.showAllowOtherOption = Boolean(value.length);
            this.allowOtherOptionValue = value;
        } else {
            this.resetAllowOtherOption();
        }

        this.filterOptionsByInputValue();
    }

    addOtherOption(): void {
        if (!this.allowOther || !this.inputValue) return;
        const inputVal: string = trim(this.inputValue);

        // De-dupe "Not Sure" / "Not Applicable" options
        let attributeOptionSelected = false;
        if (this.notSureOption && inputVal.toLowerCase() === this.translatePipe.transform("NotSure").toLowerCase()) {
            this.handleNotSureNotApplicable.emit(this.notSureOption);
            attributeOptionSelected = true;
        } else if (this.notApplicableOption && inputVal.toLowerCase() === this.translatePipe.transform("NotApplicable").toLowerCase()) {
            this.handleNotSureNotApplicable.emit(this.notApplicableOption);
            attributeOptionSelected = true;
        }

        if (attributeOptionSelected) {
            this.inputValue = "";
            this.resetAllowOtherOption();
        } else if (this.checkForDuplicates) {
            const hasDuplicateResponse: boolean = this.checkForDuplicateResponse(inputVal);
            const hasDuplicateOption: boolean = this.checkForDuplicateOption(inputVal);

            if (hasDuplicateResponse || hasDuplicateOption) {
                this.notificationService.alertWarning(this.translatePipe.transform("Error"), this.translatePipe.transform("DuplicateOptionsNotAllowed"));
                this.resetAllowOtherOption();
            } else {
                this.addNewOptionToModel(inputVal);
            }
        } else {
            this.addNewOptionToModel(inputVal);
        }
    }

    checkForDuplicateResponse(inputVal: string): boolean {
        if (this.responses && this.responses.length) {
            return this.DropdownService.checkForDuplicateOptions(inputVal, this.responses, this.labelKey);
        } else {
            return false;
        }
    }

    checkForDuplicateOption(inputVal: string): boolean {
        return this.DropdownService.checkForDuplicateOptions(inputVal, this.tranlsateAttributeDropdownOptions(this.question.options), this.labelKey);
    }

    resetAllowOtherOption(): void {
        this.applySingleSelectInputStyle = false;
        this.applySingleSelectOtherInputStyle = false;
        this.showAllowOtherOption = false;
        this.allowOtherOptionValue = "";
    }

    resetOptions(): void {
        this.filteredOptions = this.tranlsateAttributeDropdownOptions(this.defaultOptions);
    }

    addNewOptionToModel(inputVal: string): void {
        this.resetAllowOtherOption();
        this.otherChange.emit({ response: inputVal });
    }

    private componentWillReceiveState(): void {
        if (this.question && this.question.id) {
            this.multiSelect = !!this.question.attributes.isMultiSelect;
            this.allowOther = this.DropdownService.canAllowOther(this.question);
            this.allowHint = this.DropdownService.canAllowHint(this.question);

            if (this.question.options) {
                const subQuestionId = this.subQuestion ? this.subQuestion.parentAssessmentDetailId : null;
                if (this.multiSelect) {
                    const { selectedOptions, invalidOptions } = this.DropdownService.formatMultiSelectResponses(this.question.id, subQuestionId);
                    this.responses = this.setSelectedAndInvalidResponse(selectedOptions);
                    this.invalidResponses = this.setSelectedAndInvalidResponse(invalidOptions);
                    this.defaultOptions = this.DropdownService.filterDropdownOptions(this.question.options, this.responses);
                } else {
                    const { selectedOption, invalidOptions } = this.DropdownService.formatSingleSelectResponse(this.question.id, subQuestionId);
                    const selectedOptions = [];
                    selectedOptions.push(selectedOption);
                    this.response = this.setSelectedAndInvalidResponse(selectedOptions)[0];
                    this.invalidResponses = this.setSelectedAndInvalidResponse(invalidOptions);
                    this.defaultOptions = this.DropdownService.filterDropdownOption(this.question.options, this.response);
                    this.applySingleSelectInputStyle = this.response && this.response.optionType === QuestionResponseType.Default;
                    this.applySingleSelectOtherInputStyle = this.response && this.response.optionType === QuestionResponseType.Others;
                    this.applySingleSelectInvalidInputStyle = Boolean(this.response && this.invalidResponses.length);
                }
            }

            this.resetOptions();
        }
    }

    private tranlsateAttributeDropdownOptions(attributeOptions: IQuestionModelOption[]): IQuestionModelOption[] {
        forEach(attributeOptions, (attributeOption: IQuestionModelOption): void => {
            attributeOption.translatedOption = attributeOption.optionKey ? this.translatePipe.transform(attributeOption.optionKey) : attributeOption.option;
        });
        return attributeOptions;
    }

    private setSelectedAndInvalidResponse(selectedOptions: IQuestionModelOption[]): IQuestionModelOption[] {
        forEach(this.question.options, (option: IQuestionModelOption): void => {
            forEach(selectedOptions, (selectedOption: IQuestionModelOption): void => {
                if (selectedOption && !selectedOption.translatedOption && option.id === selectedOption.id) {
                    selectedOption.optionKey = option.optionKey;
                    selectedOption.translatedOption = selectedOption.optionKey ? this.translatePipe.transform(selectedOption.optionKey) : selectedOption.option;
                } else if (selectedOption && (selectedOption.optionType === QuestionResponseType.Others || selectedOption.optionType === QuestionResponseType.Default)) {
                    selectedOption.translatedOption = selectedOption.option;
                }
            });
        });
        return selectedOptions;
    }

}
