// Rxjs
import { Subject } from "rxjs";
import {
    distinctUntilChanged,
    debounceTime,
    takeUntil,
} from "rxjs/operators";

// 3rd Party
import {
    Component,
    Inject,
    Input,
    OnInit,
    EventEmitter,
    Output,
    OnDestroy,
    SimpleChanges,
} from "@angular/core";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getQuestionResponseIds,
    getQuestionResponseModels,
    getQuestionModels,
    getSubQuestionModels,
    getSubQuestionResponseIds,
    getSubQuestionResponseModels,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { getIsIE11 } from "oneRedux/reducers/setting.reducer";

// Interfaces
import {
    IQuestionModel,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IStore } from "interfaces/redux.interface";

// Enums & Constants
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";

@Component({
    selector: "aa-textbox",
    template: `
        <div
            otFormElement
            [label]="label"
            [required]="isRequired"
        >
            <textarea
                otTextarea
                [ngModel]="inputValue"
                (ngModelChange)="inputChanged($event)"
                [disabled]="isDisabled"
                [attr.rows]="'3'"
                [placeholder]="(isIE11 ? '' : placeholder)"
                [attr.ot-auto-id]="otAutoId"
            ></textarea>
        </div>
    `,
})
export class AATextboxComponent implements OnInit, OnDestroy {

    @Input() isDisabled: boolean;
    @Input() isJustification: boolean;
    @Input() isRequired: boolean;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() question: IQuestionModel;
    @Input() questionIndex: string;
    @Input() subQuestionId: string;

    @Output() textboxUpdated = new EventEmitter();

    debouncer$: Subject<string> = new Subject<string>();
    inputValue = "";
    otAutoId: string;
    responseId = "";
    isIE11 = false;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
    ) {}

    ngOnInit(): void {
        this.debouncer$.pipe(
            debounceTime(750),
            distinctUntilChanged(),
            takeUntil(this.destroy$),
        ).subscribe((text: string) => {
            if (this.textboxUpdated) {
                this.textboxUpdated.emit({ response: text, responseId: this.responseId });
            }
        });
        this.isIE11 =  getIsIE11(this.store.getState());
        this.otAutoId = this.questionIndex
            ? `AssessmentQuestionDetail_${this.questionIndex}_Textbox`
            : `AssessmentQuestionDetail_Textbox`;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            this.findWhichResponseIsSelected();
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    inputChanged(value: string): void {
        this.debouncer$.next(value);
    }

    private findWhichResponseIsSelected(): void {
        const question: IQuestionModel = getQuestionModels(this.store.getState())[this.question.id];
        const subQuestion: IQuestionModelSubQuestion = this.subQuestionId ? getSubQuestionModels(question.id)(this.store.getState())[this.subQuestionId] : null;

        if (this.isJustification) {
            if (question && question.justification) {
                this.inputValue = question.justification;
            } else if (question && subQuestion && subQuestion.justification) {
                this.inputValue = subQuestion.justification;
            } else {
                this.inputValue = null;
            }
            return;
        }

        const responseIds = this.question && this.subQuestionId
            ? getSubQuestionResponseIds(this.question.id, this.subQuestionId)(this.store.getState())
            : getQuestionResponseIds(this.question.id)(this.store.getState());

        const responseModels = this.question && this.subQuestionId
            ? getSubQuestionResponseModels(this.question.id, this.subQuestionId)(this.store.getState())
            : getQuestionResponseModels(this.question.id)(this.store.getState());

        if (responseIds.length) {
            for (const id of responseIds) {
                if (responseModels[id] && (responseModels[id].type === QuestionResponseType.Default)) {
                    this.inputValue = responseModels[id].response;
                    this.responseId = responseModels[id].responseId;
                    break;
                } else {
                    this.inputValue = null;
                }
            }
        } else {
            this.inputValue = null;
        }
    }

}
