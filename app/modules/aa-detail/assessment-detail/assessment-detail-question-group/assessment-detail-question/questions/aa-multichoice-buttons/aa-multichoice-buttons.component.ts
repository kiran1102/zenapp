// 3rd Party
import { Component, Inject, Input, OnInit, EventEmitter, Output, OnChanges, SimpleChanges, ChangeDetectionStrategy } from "@angular/core";
import {
    includes,
    cloneDeep,
    debounce,
    forEach,
    every,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getSubQuestionResponseIds,
    getSubQuestionResponseModels,
    getQuestionResponseIds,
    getQuestionResponseModels,
    getSubQuestionResponsesArray,
    getQuestionResponsesArray,
    getInvalidSubQuestionResponsesArray,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { AssessmentMultichoiceDropdownViewLogicService } from "modules/assessment/services/view-logic/assessment-multichoice-dropdown-view-logic.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IEditableTypeConfig } from "interfaces/editable-type-config.interface";
import { IQuestionResponseV2 } from "interfaces/assessment/question-response-v2.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";

// Enums / Constants
import { QuestionResponseType } from "constants/question-types.constant";
import { QuestionResponseType as QuestionResponseTypeEnum } from "enums/assessment/assessment-question.enum";
import { AAOtherOptionConfig } from "constants/question-types.constant";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: "aa-multichoice-buttons",
    templateUrl: "./aa-multichoice-buttons.component.html",
})

export class AAMultichoiceButtonsComponent implements OnInit, OnChanges {

    @Input() disabled = false;
    @Input() question: IQuestionModel;
    @Input() selectionKey: string;
    @Input() isSubQuestion = false;
    @Input() subQuestion: IQuestionModelSubQuestion;

    @Output() handleNotSureNotApplicable = new EventEmitter();
    @Output() handleOtherChange = new EventEmitter();
    @Output() handleMultichoiceChange = new EventEmitter();

    invalidResponses: IQuestionResponseV2[];
    otherConfig: IEditableTypeConfig;
    newOtherConfig: IEditableTypeConfig;
    newOtherModel = "";
    notApplicableOption: IQuestionModelOption = null;
    notSureOption: IQuestionModelOption = null;
    options: IQuestionModelOption[];
    response: IQuestionModelOption;
    responses: IQuestionModelOption[];
    translations: IStringMap<string> = {};

    onMultichoiceChange = debounce((option: IQuestionModelOption): void => {
        if (this.handleMultichoiceChange.observers.length) {
            this.handleMultichoiceChange.emit({ option });
        }
    }, 100);

    onNotSureNotApplicableChange = debounce((option: IQuestionModelOption): void => {
        if (this.handleNotSureNotApplicable.observers.length) {
            this.handleNotSureNotApplicable.emit({ option });
        }
    }, 100);

    onOtherChange = debounce((response: string, responseId: string = null, prevResponse: string = null): void => {
        // When new response is same as previous response, prevent further actions
        if (responseId && response === prevResponse) return;

        // De-dupe "Not Sure" / "Not Applicable"
        if (this.notSureOption && response.toLowerCase() === this.translations.NotSureLowerCase) {
            this.handleNotSureNotApplicable.emit({ option: this.notSureOption, forceSelect: true });
            return;
        }

        if (this.notApplicableOption && response.toLowerCase() === this.translations.NotApplicableLowerCase) {
            this.handleNotSureNotApplicable.emit({ option: this.notApplicableOption, forceSelect: true } );
            return;
        }

        const questionResponses: IQuestionResponseV2[] = this.isSubQuestion && this.subQuestion.parentAssessmentDetailId
            ? getSubQuestionResponsesArray(this.question.id, this.subQuestion.parentAssessmentDetailId)(this.store.getState())
            : getQuestionResponsesArray(this.question.id)(this.store.getState());

        const hasDuplicateOption = this.AssessmentMultichoiceService.checkForDuplicateOptions(response, this.question.options, "option");
        const hasDuplicateResponse = this.AssessmentMultichoiceService.checkForDuplicateOptions(response, questionResponses, "response");

        this.handleOtherChange.emit({
            response,
            responseId,
            isDuplicate: hasDuplicateOption || hasDuplicateResponse,
        });
    }, 100);

    constructor(
        @Inject(StoreToken) private store: IStore,
        private AssessmentMultichoiceService: AssessmentMultichoiceDropdownViewLogicService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        this.otherConfig = AAOtherOptionConfig;
        this.newOtherConfig = { ...AAOtherOptionConfig, clearOnSelect: true };

        every(this.question.options, (option: IQuestionModelOption) => {
            switch (option.optionType) {
                case QuestionResponseTypeEnum.NotSure:
                    this.notSureOption = option;
                    break;
                case QuestionResponseTypeEnum.NotApplicable:
                    this.notApplicableOption = option;
                    break;
            }

            return  !(this.notSureOption && this.notApplicableOption);
        });

        this.translations = {
            NotApplicableLowerCase: this.translatePipe.transform("NotApplicable").toLowerCase(),
            NotSureLowerCase: this.translatePipe.transform("NotSure").toLowerCase(),
        };

    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.isSubQuestion ? changes.subQuestion : changes.question) {
            this.componentWillReceiveState();
        }
    }

    trackByIndex(index: number): number {
        return index;
    }

    shouldShowOtherOption(question: IQuestionModel): boolean {
        if (question && question.attributes && !question.attributes.isMultiSelect) {
            if (!question.responses || !question.responses.allIds.length) {
                return true;
            } else if (question.responses && question.responses.allIds.length) {
                const responseId = question.responses.allIds[0];
                const responseModels = getQuestionResponseModels(question.id)(this.store.getState());
                const response: IQuestionResponseV2 = responseModels[responseId];
                // If there is a response selected, but that response isn't Other, show the Other button
                return response.type !== QuestionResponseType.Others;
            }
        } else {
            return true;
        }
    }

    shouldShowOtherOptionSubQuestion(subQuestion: IQuestionModelSubQuestion): boolean {
        if (this.question && this.question.attributes && !this.question.attributes.isMultiSelect) {
            if (!subQuestion.responses || !subQuestion.responses.allIds.length) {
                return true;
            } else if (subQuestion.responses && subQuestion.responses.allIds.length) {
                const responseId = subQuestion.responses.allIds[0];
                const responseSubQuestionModels = getSubQuestionResponseModels(this.question.id, subQuestion.parentAssessmentDetailId)(this.store.getState());
                const response: IQuestionResponseV2 = responseSubQuestionModels[responseId];
                // If there is a response selected, but that response isn't Other, show the Other button
                return response.type !== QuestionResponseType.Others;
            }
        } else {
            return true;
        }
    }

    isResponseSelected(questionId: string, optionId: string): boolean {
        const responseIds = this.isSubQuestion && this.subQuestion && this.subQuestion.parentAssessmentDetailId
            ? getSubQuestionResponseIds(questionId, this.subQuestion.parentAssessmentDetailId)(this.store.getState())
            : getQuestionResponseIds(questionId)(this.store.getState());

        return includes(responseIds, optionId);
    }

    isResponseValid(questionId: string, optionId: string): boolean {
        const responseModels = this.isSubQuestion && this.subQuestion && this.subQuestion.parentAssessmentDetailId
            ? getSubQuestionResponseModels(questionId, this.subQuestion.parentAssessmentDetailId)(this.store.getState())
            : getQuestionResponseModels(questionId)(this.store.getState());

        return responseModels[optionId] ? responseModels[optionId].valid : null;
    }

    private componentWillReceiveState(): void {
        this.options = cloneDeep(this.question.options);
        forEach(this.options, (option: IQuestionModelOption): void => {
            option.selected = this.isResponseSelected(this.question.id, option.id);
            option.valid = this.isResponseValid(this.question.id, option.id);
            option.translatedOption = this.translateOptions(option);

        });

        if (this.isSubQuestion) {
            this.invalidResponses = getInvalidSubQuestionResponsesArray(this.question.id, this.subQuestion.parentAssessmentDetailId)(this.store.getState());
        }
    }

    private translateOptions(option: IQuestionModelOption): string {
        return option.optionKey ? this.translatePipe.transform(option.optionKey) : option.option;
    }
}
