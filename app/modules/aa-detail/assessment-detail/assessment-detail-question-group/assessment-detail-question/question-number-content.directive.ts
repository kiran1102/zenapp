import { Directive, TemplateRef } from "@angular/core";

@Directive({ selector: "[questionNumberContent]" })
export class QuestionNumberContentDirective {
    constructor(public templateRef: TemplateRef<any>) {
    }
}
