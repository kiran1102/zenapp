
import {
    Component,
    Inject,
    Input,
    OnDestroy,
    OnChanges,
    SimpleChanges,
} from "@angular/core";

// 3rd Party
import {
    map as lodashMap,
    sortBy,
    remove,
    forEach,
} from "lodash";

// Rxjs
import { Subject } from "rxjs";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getAssessmentDetailOrg,
    getAssessmentDetailAssessmentId,
    getAssessmentDetailSelectedSectionId,
    getPersonalDataQuestionResponses,
    getQuestionPersonalDataUpdateResponseMap,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Services
import { AaDetailActionService } from "modules/assessment/services/action/aa-detail-action.service";

// Interface
import { IStore } from "interfaces/redux.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { ISaveSectionResponsesContract } from "interfaces/assessment/assessment-api.interface";
import {
    IPersonalDataQuestionResponse,
    IAssessementDataElementResponse,
    IPersonalDataCategory,
    IPersonalDataUpdateResponseMap,
    IQuestionModelResponse,
} from "interfaces/assessment/question-response-v2.interface";
import { IRelatedDataSubjectModalData } from "modules/assessment/components/assessment-detail-question-related-data-subject-modal/assessment-detail-question-related-data-subject-modal.component";
import {
    IQuestionModelOption,
    IQuestionResponsesState,
} from "interfaces/assessment/question-model.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "assessment-detail-question-personal-data",
    templateUrl: "assessment-detail-question-personal-data.component.html",
})

export class AssessmentDetailQuestionPersonalDataComponent implements OnChanges, OnDestroy {

    @Input() isDisabled = false;
    @Input() question: IQuestionModel;

    dataSubjectResponses: IPersonalDataQuestionResponse[];
    invalidResponses: IQuestionModelOption[];
    invalidDataAttributesMap: Map<string, boolean>;

    private destroy$ = new Subject();

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translate: TranslatePipe,
        private assessmentDetailAction: AaDetailActionService,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        const question = changes.question;
        if (question) {
            this.dataSubjectResponses = getPersonalDataQuestionResponses(this.question.id)(this.store.getState());
            this.dataSubjectResponses = sortBy(this.dataSubjectResponses, (item: IPersonalDataQuestionResponse) => item.name);
            lodashMap(this.dataSubjectResponses, (response: IPersonalDataQuestionResponse): void => {
                response.categories = sortBy(response.categories, (item: IPersonalDataCategory) => item.categoryValue);
                lodashMap(response.categories, (category: IPersonalDataCategory): void => {
                    category.elementResponses = sortBy(category.elementResponses, (item: IAssessementDataElementResponse) => item.name);
                });
            });

            this.getInvalidResponses(question.currentValue);
        }
    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    trackByFn(index: number): number {
        return index;
    }

    addPersonalData(): void {
        const modalData: IRelatedDataSubjectModalData = {
            currentOrg: getAssessmentDetailOrg(this.store.getState()),
            dataSubjectCategoryId: null,
            dataSubjectId: null,
            isEditMode: false,
            modalTitle: this.translate.transform("AddDataElements"),
            questionId: this.question.id,
            callback: null,
        };

        this.assessmentDetailAction.openAssessmentDataSubjectElementModal(modalData);
    }

    editPersonalData(dataSubject: IPersonalDataQuestionResponse, category?: IPersonalDataCategory): void {
        const modalData: IRelatedDataSubjectModalData = {
            currentOrg: getAssessmentDetailOrg(this.store.getState()),
            dataSubjectCategoryId: category ? category.categoryId : null,
            dataSubjectId: dataSubject.id,
            isEditMode: true,
            modalTitle: this.translate.transform("ManageDataElements"),
            questionId: this.question.id,
            callback: null,
        };

        this.assessmentDetailAction.openAssessmentDataSubjectElementModal(modalData);
    }

    deletePersonalData(dataSubject: IPersonalDataQuestionResponse): void {
        const assessmentId = getAssessmentDetailAssessmentId(this.store.getState());
        const sectionId = getAssessmentDetailSelectedSectionId(this.store.getState());
        const currentlySelectedElements = getQuestionPersonalDataUpdateResponseMap(this.question.id)(this.store.getState());
        remove(currentlySelectedElements, (map: IPersonalDataUpdateResponseMap) => map.DATA_SUBJECTS.id === dataSubject.id);
        const responseForSave: ISaveSectionResponsesContract[] = [{
            assessmentId,
            parentAssessmentDetailId: null,
            sectionId,
            questionId: this.question.id,
            responses: this.assessmentDetailAction.formatQuestionResponses(currentlySelectedElements),
        }];
        this.assessmentDetailAction.saveDSEResponses(responseForSave);
    }

    private getInvalidResponses(question): void {
        const responses: IQuestionResponsesState<IQuestionModelResponse> = question.responses;
        this.invalidResponses = [];
        this.invalidDataAttributesMap = new Map<string, boolean>();

        if (!responses) return;

        forEach(responses.invalidResponsesMap, (invalidQuestion) => {
            forEach(invalidQuestion.categories, (category) => {
                forEach(category.dataElements, (dataElement) => {
                    const option = invalidQuestion.name + " | " + category.name + " | " + dataElement.name;
                    this.invalidResponses.push({
                        id: dataElement.id,
                        option,
                        optionDisplay: option,
                    });
                    this.invalidDataAttributesMap.set(dataElement.id, true);
                });

            });
        });
    }
}
