import { Directive, TemplateRef } from "@angular/core";

@Directive({ selector: "[questionDescriptionContent]" })
export class QuestionDescriptionContentDirective {
    constructor(public templateRef: TemplateRef<any>) {
    }
}
