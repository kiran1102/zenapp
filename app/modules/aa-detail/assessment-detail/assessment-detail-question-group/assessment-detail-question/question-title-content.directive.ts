import { Directive, TemplateRef } from "@angular/core";

@Directive({ selector: "[questionTitleContent]" })
export class QuestionTitleContentDirective {
    constructor(public templateRef: TemplateRef<any>) {
    }
}
