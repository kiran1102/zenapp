// External libraries
import { Component, Input, Inject, SimpleChanges, OnChanges } from "@angular/core";

// Enums
import { RiskLevels } from "enums/risk.enum";

// Interfaces
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IQuestionModel } from "interfaces/assessment/question-model.interface";
import { IRiskDetails, IRiskMetadata } from "interfaces/risk.interface";
import { IUser } from "interfaces/user.interface";

// Redux
import {
    getAssessmentDetailModel,
    getAssessmentRisksByQuestionId,
    getAssessmentDetailSelectedSectionId,
} from "modules/assessment/reducers/assessment-detail.reducer";
import { IStore } from "interfaces/redux.interface";
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { AssessmentAction } from "modules/assessment/reducers/assessment.reducer";

// Services
import { AssessmentRiskViewLogicService } from "modules/assessment/services/view-logic/assessment-risk-view-logic.service";
import { AssessmentRiskActionService } from "modules/assessment/services/action/assessment-risk-action.service";

// Constants
import { QuestionTypeNames } from "constants/question-types.constant";

@Component({
    selector: "assessment-detail-question-side-button",
    templateUrl: "./assessment-detail-question-side-button.component.html",
})
export class AssessmentDetailQuestionSideButtonComponent implements OnChanges {

    @Input() question: IQuestionModel;

    public assessment: IAssessmentModel;
    public canCreateRisk = false;
    public currentUser: IUser;
    public risk: IRiskDetails;
    public riskLevelClass: string;
    public risksMetadata: IRiskMetadata[];
    public sectionId: string;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private AssessmentRiskViewLogic: AssessmentRiskViewLogicService,
        private AssessmentRiskAction: AssessmentRiskActionService,
    ) { }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            this.componentWillReceiveState();
        }
    }

    setRiskLevelClass = (riskLevel: number): string => {
        switch (riskLevel) {
            case RiskLevels.Low:
                return "risk-low--bg";
            case RiskLevels.Medium:
                return "risk-medium--bg";
            case RiskLevels.High:
                return "risk-high--bg";
            case RiskLevels.VeryHigh:
                return "risk-very-high--bg";
            default:
                return "";
        }
    }

    public flagRisk = (metadata: IRiskMetadata = null): void => {
        if (metadata && metadata.riskId) {
            this.store.dispatch({ type: AssessmentAction.SET_PRESELECTED_RISKID, preSelectedRiskId: metadata.riskId });
        } else {
            this.AssessmentRiskAction.openRiskModal(metadata, this.assessment.assessmentId, this.sectionId, this.question.id);
        }
    }

    public trackByFn(index: number, risk: IRiskMetadata): string {
        return risk.riskId;
    }

    private componentWillReceiveState = (): void => {
        const state = this.store.getState();
        this.currentUser = getCurrentUser(state);
        this.assessment = getAssessmentDetailModel(state);
        this.risksMetadata = getAssessmentRisksByQuestionId(this.question.id)(state);
        this.sectionId = getAssessmentDetailSelectedSectionId(state);
        this.canCreateRisk = this.setRiskVisibility();
    }

    private setRiskVisibility(): boolean {
        return this.question.questionType !== QuestionTypeNames.Statement
            && this.question.questionType !== QuestionTypeNames.Incident
            && this.AssessmentRiskViewLogic.hasCreateRiskPermission(this.currentUser, this.assessment, null);
    }
}
