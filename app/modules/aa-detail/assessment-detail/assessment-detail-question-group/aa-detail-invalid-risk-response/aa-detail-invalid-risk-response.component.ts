import {
    Component,
    Inject,
    Input,
    OnInit,
} from "@angular/core";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAssessmentTemplateSection } from "modules/template/interfaces/template.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { INameId } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { AssessmentRiskApiService } from "modules/assessment/services/api/assessment-risk-api.service";
import { AssessmentRiskActionService } from "modules/assessment/services/action/assessment-risk-action.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";
import { AaCollaborationPaneCountsService } from "modules/assessment/services/messaging/aa-section-detail-header-helper.service";

// Constants
import { UserRoles } from "constants/user-roles.constant";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    AssessmentDetailAction,
    getAssessmentSectionByQuestionId,
    getAssessmentDetailAssessmentId,
    getAssessmentDetailModel,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Enum
import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

@Component({
    selector: "aa-detail-invalid-risk-response",
    template: `
        <div
            *ngIf="showRiskBanner"
            class="aa-question__invalid-response border-radius padding-all-1 {{wrapperClass}}">
            <div class="row-space-between-center">
                <div>
                    <i class="ot ot-exclamation-circle"></i>
                    <span class="aa-question__invalid-response-text">
                        {{"RisksDeleteConfirmationText" | otTranslate}}
                    </span>
                </div>
                <button
                    otButton
                    neutral
                    [attr.ot-auto-id]="'AAInvalidRiskResponseRetainId'"
                    (click)="onRetain()"
                >
                    {{"Retain" | otTranslate}}
                </button>
                <button
                    otButton
                    neutral
                    [attr.ot-auto-id]="'AAInvalidRiskResponseDeleteId'"
                    (click)="onDelete()"
                >
                    {{"Delete" | otTranslate}}
                </button>
            </div>
        </div>
    `,
})
export class AADetailInvalidRiskResponse implements OnInit {

    @Input() questionId;
    @Input() riskIds = [];
    @Input() wrapperClass: string;

    showRiskBanner = true;
    assessmentId: string;
    respondents: Array<INameId<string>>;

    constructor(
        @Inject(StoreToken) private readonly store: IStore,
        private translatePipe: TranslatePipe,
        private AssessmentRiskApi: AssessmentRiskApiService,
        private AssessmentRiskAction: AssessmentRiskActionService,
        private notificationService: NotificationService,
        private aaCollaborationPaneCountsService: AaCollaborationPaneCountsService,
    ) { }

    ngOnInit() {
        const state: IStoreState = this.store.getState();
        this.respondents = getAssessmentDetailModel(state).respondents;
        const currentUser: IUser = getCurrentUser(state);
        const isRespondent = this.respondents.find((respondent: INameId<string>) => respondent.id === currentUser.Id);
        this.assessmentId = getAssessmentDetailAssessmentId(state);
        if (isRespondent && currentUser.RoleName !== UserRoles.SiteAdmin) {
             this.showRiskBanner = false;
        }
    }

    onDelete(): void {
        if (this.questionId && this.riskIds && this.riskIds.length > 0) {
            this.AssessmentRiskApi.deleteAssessmentRisks(this.riskIds, this.assessmentId).then((response: IProtocolResponse<void>) => {
                    if (response.result) {
                        this.notificationService.alertSuccess(
                            this.translatePipe.transform("Success"),
                            this.translatePipe.transform("RiskDeleted"),
                        );
                        const state: IStoreState = this.store.getState();
                        const section: IAssessmentTemplateSection = getAssessmentSectionByQuestionId(this.questionId)(state);
                        this.AssessmentRiskAction.deleteRiskCallback(this.riskIds, this.questionId, section.id, this.assessmentId);
                        this.aaCollaborationPaneCountsService.getCollaborationPaneCounts(this.assessmentId, AACollaborationCountsTypes.Risks);
                    }
                });
        }
        this.showRiskBanner = true;
    }

    onRetain(): void {
        this.store.dispatch({ type: AssessmentDetailAction.REMOVE_INVALID_RISK_RESPONSE, questionId: this.questionId });
        this.showRiskBanner = true;
    }

}
