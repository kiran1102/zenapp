// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { VitreusModule } from "@onetrust/vitreus";
import { SharedModule } from "./../shared/shared.module";
import { OTPipesModule } from "modules/pipes/pipes.module";
import { AAPipesModule } from "modules/assessment/pipes/aa-pipes.module";
import { AASectionModule } from "modules/aa-section/aa-section.module";
import { AASectionFooterModule } from "modules/aa-section-footer/aa-section-footer.module";
import { AACollaborationModule } from "modules/aa-collaboration/aa-collaboration.module";
import { AssessmentDetailSharedModule } from "./shared/assessment-detail-shared.module";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { DndListModule } from "ngx-drag-and-drop-lists";
import { VirtualScrollModule } from "od-virtualscroll";
import { OneVideoModule } from "modules/video/video.module";

// Components
import { AAAssessmentDetailComponent } from "./assessment-detail/assessment-detail.component";
import { AssessmentDetailQuestionGroupComponent } from "./assessment-detail/assessment-detail-question-group/assessment-detail-question-group.component";
import { AADetailInvalidRiskResponse } from "./assessment-detail/assessment-detail-question-group/aa-detail-invalid-risk-response/aa-detail-invalid-risk-response.component";
import { AssessmentDetailQuestionComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/assessment-detail-question.component";
import { AssessmentDetailQuestionPersonalDataComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/assessment-detail-question-personal-data/assessment-detail-question-personal-data.component";
import { AssessmentDetailQuestionSideButtonComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/assessment-detail-question-side-button/assessment-detail-question-side-button.component";
import { AAAttributeQuestionComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-attribute-question/aa-attribute-question.component";
import { AAIncidentComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-incident/aa-incident.component";
import { AAInventoryDropdownComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-inventory-dropdown/aa-inventory-dropdown.component";
import { AAMultichoiceButtonsComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-multichoice-buttons/aa-multichoice-buttons.component";
import { AAMultichoiceDropdownComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-multichoice-dropdown/aa-multichoice-dropdown.component";
import { AATextboxComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/questions/aa-textbox/aa-textbox.component";
import { AAJurisdictionComponent } from "modules/assessment/components/questions/aa-jurisdiction/aa-jurisdiction.component";
import { AADetailQuestionNeedsMoreInfoComponent } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/aa-detail-question-needs-more-info/aa-detail-question-needs-more-info.component";
import { AAInvalidResponse } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/aa-invalid-response/aa-invalid-response.component";

// Directives
import { QuestionAttributeTitleContentDirective } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/question-attribute-title-content.directive";
import { QuestionDescriptionContentDirective } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/question-description-content.directive";
import { QuestionNumberContentDirective } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/question-number-content.directive";
import { QuestionTitleContentDirective } from "modules/aa-detail/assessment-detail/assessment-detail-question-group/assessment-detail-question/question-title-content.directive";

@NgModule({
    imports: [
        CommonModule,
        VitreusModule,
        SharedModule,
        OTPipesModule,
        AAPipesModule,
        AASectionModule,
        AASectionFooterModule,
        AACollaborationModule,
        AssessmentDetailSharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        DndListModule,
        OTPipesModule,
        VirtualScrollModule,
        OneVideoModule,
    ],
    exports: [
        AAAssessmentDetailComponent,
    ],
    entryComponents: [
        AAAssessmentDetailComponent,
    ],
    declarations: [
        AAAssessmentDetailComponent,
        AADetailInvalidRiskResponse,
        AssessmentDetailQuestionGroupComponent,
        AssessmentDetailQuestionComponent,
        AssessmentDetailQuestionPersonalDataComponent,
        AssessmentDetailQuestionSideButtonComponent,
        AAAttributeQuestionComponent,
        AAIncidentComponent,
        AAInventoryDropdownComponent,
        AAMultichoiceButtonsComponent,
        AAMultichoiceDropdownComponent,
        AATextboxComponent,
        AAJurisdictionComponent,
        AAInvalidResponse,
        AADetailQuestionNeedsMoreInfoComponent,
        QuestionAttributeTitleContentDirective,
        QuestionDescriptionContentDirective,
        QuestionNumberContentDirective,
        QuestionTitleContentDirective,
    ],
})
export class AADetailModule {}
