// 3rd Party
import {
    Component,
    Inject,
    Input,
    OnInit,
    EventEmitter,
    Output,
    SimpleChanges,
    OnChanges,
} from "@angular/core";
import {
    isNil,
    debounce,
} from "lodash";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import {
    getQuestionResponseIds,
    getQuestionResponseModels,
    getSubQuestionResponseModels,
    getSubQuestionResponseIds,
} from "modules/assessment/reducers/assessment-detail.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IQuestionModel,
    IQuestionModelSubQuestion,
} from "interfaces/assessment/question-model.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";
import { IMyDate } from "mydaterangepicker";

// Constants
import {
    QuestionResponseType,
    AttributeQuestionResponseType,
    QuestionTypeNames,
} from "constants/question-types.constant";
import { IQuestionResponseV2 } from "interfaces/assessment/question-response-v2.interface";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";

interface IQuestionDateEvent {
    questionId: string;
    responseId: string | null;
    response: string;
}

@Component({
    selector: "aa-date-picker",
    template: `
        <ot-datepicker
            *ngIf="isDateAttribute"
            [dateModel]="getDateOnlyForDisplay(model)"
            [placeholder]="placeholderKey | otTranslate"
            [format]="timeFormat"
            [disabled]="isDisabled"
            (onChange)="dateResponseSelected($event)"
        ></ot-datepicker>
        <ot-date-time-picker
            *ngIf="!isDateAttribute"
            [dateTimeModel]="model"
            [datePlaceholder]="placeholderKey | otTranslate"
            [dateDisableUntil]="(useMinDate ? minDate : '')"
            [dateFormat]="format"
            [hideTimePicker]="!enableTime"
            [is24Hour]="is24Hour"
            [timePlaceholder]="timePlaceholderKey | otTranslate"
            [dateDisabled]="isDisabled"
            [timeDisabled]="isDisabled"
            [dateDisableUntil]="(minDate ? minDate : '')"
            (onChange)="dateTimeResponseSelected($event)"
        ></ot-date-time-picker>`,
})
export class AADatePickerComponent implements OnInit, OnChanges {

    @Input() dateModel: Date;
    @Input() enableTime = false;
    @Input() isDisabled = false;
    @Input() isSubQuestion = false;
    @Input() placeholderKey = "ChooseDate";
    @Input() question: IQuestionModel;
    @Input() subQuestion: IQuestionModelSubQuestion;
    @Input() timePlaceholderKey = "ChooseTime";

    @Output() dateChanged = new EventEmitter();

    format = this.timeStamp.getDatePickerDateFormat();
    is24Hour = this.timeStamp.getIs24Hour();
    minDate = this.timeStamp.getDisableUntilDate();
    model = "";
    responseId = "";
    useMinDate = false;
    isDateAttribute = false;
    timeFormat: string;

    dateTimeResponseSelected = debounce((event: IDateTimeOutputModel): void => {
        if (event && !event.hasError) {
            this.dateChanged.emit({ value: event.jsdate, responseId: this.responseId });
        }
    }, 500);

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly timeStamp: TimeStamp,
    ) {}

    ngOnInit(): void {
        this.useMinDate = this.setMinDate(this.question, this.isSubQuestion);
        this.timeFormat = this.timeStamp.getDatePickerDateFormat();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.question) {
            if (!this.enableTime) {
                if ((this.question.attributes
                    && this.question.attributes.responseType === AttributeQuestionResponseType.DATE)
                    || (this.question.questionType === QuestionTypeNames.Date)) {
                        this.isDateAttribute = true;
                }
            }
            this.findWhichResponseIsSelected();
        }
    }

    dateResponseSelected(event): void {
        if (event && event.date) {
            const retDate = event.formatted ? this.getDateOnlyForUpdate(event.date) : "";
            this.dateChanged.emit({ value: retDate, responseId: this.responseId });
        }
    }

    getDateOnlyForDisplay(date: string): string {
        return date ? this.timeStamp.formatDateWithoutTimezone(date) : "";
    }

    getDateOnlyForUpdate(date: IMyDate): string {
        return this.timeStamp.formatDateToIso(date);
    }

    private findWhichResponseIsSelected(): void {
        let responseIds: string[];
        let responseModels: Partial<Map<string, IQuestionResponseV2>>;
        this.model = "";

        if (this.isSubQuestion) {
            responseIds = getSubQuestionResponseIds(this.question.id, this.subQuestion.parentAssessmentDetailId)(this.store.getState());
            responseModels = getSubQuestionResponseModels(this.question.id, this.subQuestion.parentAssessmentDetailId)(this.store.getState());
        } else {
            responseIds = getQuestionResponseIds(this.question.id)(this.store.getState());
            responseModels = getQuestionResponseModels(this.question.id)(this.store.getState());
        }
        for (const id of responseIds) {
            const hasValidResponse = Boolean(responseModels[id]
                && (responseModels[id].type === QuestionResponseType.Default));
            if (hasValidResponse) {
                this.model = this.enableTime ? new Date(responseModels[id].response) : responseModels[id].response;
                this.responseId = responseModels[id].responseId;
            } else {
                this.model = "";
            }
        }
    }

    private setMinDate(question: IQuestionModel, isSubQuestion: boolean): boolean {
        return this.question && this.isSubQuestion && this.question.attributes.fieldName === "deadline";
    }
}
