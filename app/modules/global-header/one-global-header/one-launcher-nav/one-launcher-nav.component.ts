import { isFunction } from "lodash";

// Angular
import {
    Component,
    EventEmitter,
    OnInit,
    Output,
} from "@angular/core";

// Services
import { StateService } from "@uirouter/core";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Interfaces
import { INavMenuItem } from "modules/global-sidebar/interfaces/nav-menu-item.interface";

// Enums
import { InventoryTableIds } from "enums/inventory.enum";

// Constants
import { InventorySchemaIds } from "constants/inventory-config.constant";

// Launcher Icons
import "images/launcher/AA.svg";
import "images/launcher/CM.svg";
import "images/launcher/Cookies.svg";
import "images/launcher/DM.svg";
import "images/launcher/Doc.svg";
import "images/launcher/DSAR.svg";
import "images/launcher/IBM.svg";
import "images/launcher/INTG.svg";
import "images/launcher/none.svg";
import "images/launcher/notice.svg";
import "images/launcher/RA.svg";
import "images/launcher/Reports.svg";
import "images/launcher/Risk.svg";
import "images/launcher/SSP.svg";
import "images/launcher/VM.svg";
import "images/launcher/users.svg";
import "images/launcher/DG.svg";

@Component({
    selector: "launcher-nav",
    templateUrl: "./one-launcher-nav.component.html",
})

export class LauncherNav implements OnInit {
    @Output() closed = new EventEmitter();

    menus: INavMenuItem[][] = [];

    constructor(
        private stateService: StateService,
        private permissions: Permissions,
        private mcmModalService: McmModalService,
    ) {}

    close() {
        this.closed.emit(true);
    }

    ngOnInit() {
        this.buildMenus();
        this.buildUpgradeMenus();
    }

    isActive(item: INavMenuItem): boolean {
        if (!(item.activeRoute || item.route) && !item.activeRouteFn)
            return false;
        if (isFunction(item.activeRouteFn)) {
            return item.activeRouteFn(this.stateService);
        } else {
            return this.stateService.includes(
                item.activeRoute || item.route,
                item.params,
            );
        }
    }

    triggerItemAction(item: INavMenuItem) {
        if (isFunction(item.action)) {
            item.action(item);
        }
    }

    private pushToMenu(item: INavMenuItem) {
        const maxMenuLen = 4;
        const len = this.menus.length;
        if (item.legacy === false && !item.params) {
            item.params = {};
        }
        if (!len || (len && this.menus[len - 1].length > maxMenuLen)) {
            this.menus.push([item]);
        } else {
            this.menus[len - 1].push(item);
        }
    }

    private pushEmptyColumn() {
        const len = this.menus.length;
        if (!len || this.menus[len - 1].length) {
            this.menus.push([]);
        }
    }

    private buildMenus() {

        if (this.permissions.canShow("ReadinessAssessmentV2")) {
            this.pushToMenu({
                id: "LauncherGlobalReadinessMenuItem",
                title: "MaturityBenchmarking",
                route: "zen.app.pia.module.globalreadiness.views.welcome",
                activeRoute: "zen.app.pia.module.globalreadiness",
                icon: "images/launcher/RA.svg",
            });
        }

        if (this.permissions.canShow("Assessments")) {
            this.pushToMenu({
                id: "LauncherAssessmentAutomationMenuItem",
                title: "AssessmentAutomation",
                route: "zen.app.pia.module.assessment.list",
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.assessment")
                        || stateService.includes("zen.app.pia.module.templatesv2")
                        || stateService.includes("zen.app.pia.module.dashboardV2");
                },
                icon: "images/launcher/AA.svg",
            });
        }

        if (this.permissions.canShow("Projects") && !this.permissions.canShow("Assessments")) {
            this.pushToMenu({
                id: "LauncherAssessmentAutomationMenuItem",
                title: "AssessmentAutomation",
                route: "zen.app.pia.module.projects.list",
                activeRouteFn: (stateService: StateService) => {
                    return stateService.includes("zen.app.pia.module.projects")
                        || stateService.includes("zen.app.pia.module.templates")
                        || stateService.includes("zen.app.pia.module.dashboard");
                },
                icon: "images/launcher/AA.svg",
            });
        }

        if (this.permissions.canShow("DataMapping") || this.permissions.canShow("DataMappingNew")) {
            let route;
            let params;

            if (this.permissions.canShow("DataMappingAssessments")) {
                route = "zen.app.pia.module.datamap.views.assessment-old.list";
            } else if (this.permissions.canShow("DataMappingAssessmentsList") && !this.permissions.canShow("DMAssessmentV2")) {
                route = "zen.app.pia.module.datamap.views.assessment.list";
            } else if (this.permissions.canShow("DMAssessmentV2") && this.permissions.canShow("DataMappingInventoryNew")) {
                if (this.permissions.canShow("DataMappingInventoryAssets")) {
                    route = "zen.app.pia.module.datamap.views.inventory.list";
                    params = { Id: InventorySchemaIds.Assets };
                } else if (this.permissions.canShow("DataMappingInventoryProcessingActivity")) {
                    route = "zen.app.pia.module.datamap.views.inventory.list";
                    params = { Id: InventorySchemaIds.Processes };
                } else {
                    route = "zen.app.pia.module.assessment.list";
                }
            } else {
                route = "zen.app.pia.module.datamap.views.assessmentV2";
            }

            this.pushToMenu({
                id: "LauncherDataMappingMenuItem",
                title: "DataMapping",
                route,
                params,
                activeRoute: "zen.app.pia.module.datamap",
                icon: "images/launcher/DM.svg",
            });
        }

        if (this.permissions.canShow("VendorRiskManagement")) {
            let route;
            let params;
            if (this.permissions.canShow("VRMDashboard")) {
                route = "zen.app.pia.module.vendor.dashboard";
            } else {
                route = "zen.app.pia.module.vendor.inventory.list";
                params = { Id: InventoryTableIds.Vendors };
            }
            this.pushToMenu({
                id: "permissionGroup.name.LauncherVendorManagementMenuItem",
                title: "VendorManagement",
                route,
                params,
                activeRoute: "zen.app.pia.module.vendor",
                icon: "images/launcher/VM.svg",
            });
        }

        if (this.permissions.canShow("IncidentBreachManagement")) {
            this.pushToMenu({
                id: "LauncherIncidentResponseMenuItem",
                title: "IncidentResponse",
                route: "zen.app.pia.module.incident.views.incident-register",
                activeRoute: "zen.app.pia.module.incident",
                icon: "images/launcher/IBM.svg",
            });
        }

        this.pushEmptyColumn();

        if (this.permissions.canShow("ConsentReceipt")) {
            this.pushToMenu({
                id: "LauncherConsentManagementAbbrMenuItem",
                title: "ConsentManagementAbbr",
                route: "zen.app.pia.module.consentreceipt.views.dashboard",
                activeRoute: "zen.app.pia.module.consentreceipt",
                icon: "images/launcher/CM.svg",
            });
        }

        if (this.permissions.canShow("Cookie")) {
            this.pushToMenu({
                id: "LauncherCookieConsentMenuItem",
                title: "CookieConsent",
                route: "zen.app.pia.module.cookiecompliance.views.websites",
                activeRoute: "zen.app.pia.module.cookiecompliance",
                icon: "images/launcher/Cookies.svg",
            });
        }

        if (this.permissions.canShow("DSARView")) {
            let route;
            if (this.permissions.canShow("DSARDashboard")) {
                route = "zen.app.pia.module.dsar.views.dashboard";
            } else if (this.permissions.canShow("DSARRequestQueue")) {
                route = "zen.app.pia.module.dsar.views.queue";
            } else {
                route = "";
            }

            this.pushToMenu({
                id: "LauncherDataSubjectRequestsMenuItem",
                title: "DataSubjectRequests",
                route,
                activeRoute: "zen.app.pia.module.dsar",
                icon: "images/launcher/DSAR.svg",
            });
        }

        if (this.permissions.canShow("DataGuidance")) {
            this.pushToMenu({
                id: "LauncherDGBetaMenuItem",
                title: "DataGuidance",
                route: "/module/data-guidance",
                activeRoute: "/module/data-guidance",
                icon: "images/launcher/DG.svg",
                legacy: false,
            });
        }

        if (this.permissions.canShow("PolicyManagement")) {
            this.pushToMenu({
                id: "LauncherPolicyManagementMenuItem",
                title: "PolicyAndNoticeManagement",
                route: "zen.app.pia.module.policy.privacynotices.list",
                activeRoute: "zen.app.pia.module.policy",
                icon: "images/launcher/CM.svg",
            });
        }

        this.pushEmptyColumn();

        if (this.permissions.canShow("ReportsNew")) {
            this.pushToMenu({
                id: "LauncherReportsMenuItem",
                title: "Reports",
                route: "zen.app.pia.module.reports-new.list",
                activeRoute: "zen.app.pia.module.reports-new",
                icon: "images/launcher/Reports.svg",
            });
        }

        if (this.permissions.canShow("Risks")) {
            this.pushToMenu({
                id: "LauncherRisksMenuItem",
                title: "Risks",
                route: "zen.app.pia.module.risk.list",
                activeRoute: "zen.app.pia.module.risk",
                icon: "images/launcher/Risk.svg",
            });
        }

        if (this.permissions.canShow("NewRiskRepositoryView") && !this.permissions.canShow("Projects")) {
            this.pushToMenu({
                id: "LauncherNewRisksMenuItem",
                title: "Risks",
                route: "zen.app.pia.module.risks.views.riskrepo",
                activeRoute: "zen.app.pia.module.risks",
                icon: "images/launcher/Risk.svg",
            });
        }

        if (this.permissions.canShow("Documents")) {
            this.pushToMenu({
                id: "LauncherDocumentsMenuItem",
                title: "Documents",
                route: "zen.app.pia.module.documents.repository.main",
                activeRoute: "zen.app.pia.module.documents",
                icon: "images/launcher/Doc.svg",
            });
        }

        this.pushEmptyColumn();

        if (this.permissions.canShow("SelfServicePortal")) {
            this.pushToMenu({
                id: "LauncherSelfServicePortalMenuItem",
                title: "SelfServicePortal",
                route: "zen.app.pia.module.ssp.list",
                activeRoute: "zen.app.pia.module.ssp",
                icon: "images/launcher/SSP.svg",
            });
        }

        if (this.permissions.canShow("IntegrationPageView")) {
            this.pushToMenu({
                id: "LauncherIntegrationsMenuItem",
                title: "Integrations",
                route: "zen.app.pia.module.intg.marketplace.list",
                activeRoute: "zen.app.pia.module.intg",
                icon: "images/launcher/INTG.svg",
            });
        }

        if (this.permissions.canShow("MenuAdmin")) {
            this.pushToMenu({
                id: "LauncherUsersAndGroupsMenuItem",
                title: "UsersAndGroups",
                route: "zen.app.pia.module.admin.users.list",
                activeRoute: "zen.app.pia.module.admin",
                icon: "images/launcher/users.svg",
            });
        }

        // EXAMPLE: Use below sample to add menu item using Angular Router.
        // this.pushToMenu({
        //     id: "LauncherWelcomeMenuItem",
        //     title: "Welcome",
        //     route: "/module/welcome",
        //     activeRoute: "/module/welcome",
        //     icon: "images/favicon.png",
        //     legacy: false,
        // });
    }

    private buildUpgradeMenus() {
        this.pushEmptyColumn();

        if (this.permissions.canShow("ReadinessUpgrade")) {
            this.pushToMenu({
                id: "LauncherGlobalReadinessMenuItem",
                title: "GlobalReadiness",
                icon: "images/launcher/RA.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("Upgrade"),
            });
        }

        if (this.permissions.canShow("AssessmentsUpgrade")) {
            this.pushToMenu({
                id: "LauncherAssessmentAutomationMenuItem",
                title: "AssessmentAutomation",
                icon: "images/launcher/AA.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("Upgrade"),
            });
        }

        if (this.permissions.canShow("ProjectsUpgrade")) {
            this.pushToMenu({
                id: "LauncherAssessmentAutomationMenuItem",
                title: "AssessmentAutomation",
                icon: "images/launcher/AA.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("Upgrade"),
            });
        }

        if (this.permissions.canShow("DataMappingUpgrade")) {
            this.pushToMenu({
                id: "LauncherDataMappingMenuItem",
                title: "DataMapping",
                activeRoute: "zen.app.pia.module.datamap",
                icon: "images/launcher/DM.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("SettingsDataMappingUpgrade"),
            });
        }

        if (this.permissions.canShow("VendorRiskManagementUpgrade")) {
            this.pushToMenu({
                id: "permissionGroup.name.LauncherVendorManagementMenuItem",
                title: "VendorManagement",
                icon: "images/launcher/VM.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("VendorRiskManagementUpgrade"),
            });
        }

        if (this.permissions.canShow("IncidentBreachManagementUpgrade")) {
            this.pushToMenu({
                id: "LauncherIncidentResponseMenuItem",
                title: "Incidents",
                icon: "images/launcher/IBM.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("IncidentBreachManagementUpgrade"),
            });
        }

        if (this.permissions.canShow("ConsentReceiptUpgrade")) {
            this.pushToMenu({
                id: "LauncherConsentManagementAbbrMenuItem",
                title: "ConsentManagementAbbr",
                icon: "images/launcher/CM.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("ConsentReceiptUpgrade"),
            });
        }

        if (this.permissions.canShow("CookieUpgrade")) {
            this.pushToMenu({
                id: "LauncherCookieConsentMenuItem",
                title: "CookieConsent",
                icon: "images/launcher/Cookies.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("Upgrade"),
            });
        }

        if (this.permissions.canShow("DSARUpgrade")) {
            this.pushToMenu({
                id: "LauncherDataSubjectRequestsMenuItem",
                title: "DataSubjectRequests",
                icon: "images/launcher/DSAR.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("DSARUpgrade"),
            });
        }

        if (this.permissions.canShow("ReportsUpgrade")) {
            this.pushToMenu({
                id: "LauncherReportsMenuItem",
                title: "Reports",
                icon: "images/launcher/Reports.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("ReportsUpgrade"),
            });
        }

        if (this.permissions.canShow("RisksUpgrade")) {
            this.pushToMenu({
                id: "LauncherRisksMenuItem",
                title: "Risks",
                icon: "images/launcher/Risk.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("SettingsRiskUpgrade"),
            });
        }

        if (this.permissions.canShow("DocumentsUpgrade")) {
            this.pushToMenu({
                id: "LauncherDocumentsMenuItem",
                title: "Documents",
                icon: "images/launcher/Doc.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("DocumentsUpgrade"),
            });
        }

        if (this.permissions.canShow("SelfServicePortalUpgrade")) {
            this.pushToMenu({
                id: "LauncherSelfServicePortalMenuItem",
                title: "SelfServicePortal",
                icon: "images/launcher/SSP.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("SelfServicePortalUpgrade"),
            });
        }

        if (this.permissions.canShow("IntegrationPageUpgrade")) {
            this.pushToMenu({
                id: "LauncherIntegrationsMenuItem",
                title: "Integrations",
                icon: "images/launcher/INTG.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("IntegrationPageUpgrade"),
            });
        }

        if (this.permissions.canShow("MenuAdminUpgrade")) {
            this.pushToMenu({
                id: "LauncherUsersAndGroupsMenuItem",
                title: "UsersAndGroups",
                icon: "images/launcher/users.svg",
                upgrade: true,
                action: () => this.mcmModalService.openMcmModal("UsersUpgrade"),
            });
        }

    }

}
