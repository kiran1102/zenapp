import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import {
    RawParams,
    StateOrName,
    StateService,
    TransitionOptions,
} from "@uirouter/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
    take,
} from "rxjs/operators";

// 3rd party
import Utilities from "Utilities";
import {
    capitalize,
    head,
    join,
    map,
    split,
} from "lodash";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import {
    getCurrentOrgId,
    getOrgById,
} from "oneRedux/reducers/org.reducer";
import { getHeaderBranding } from "oneRedux/reducers/branding.reducer";

// Services
import { GlobalHeaderStateService } from "modules/global-header/services/global-header-state.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { Accounts } from "modules/shared/services/api/accounts.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { Content } from "modules/shared/services/provider/content.service";
import { HelpCenter } from "modules/shared/services/provider/help-center.service";
import { LoadingService } from "modules/core/services/loading.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import { SkuManagementModalService } from "modules/sku/shared/sku-management-modal.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";
import {
    OtModalService,
    ModalSize,
} from "@onetrust/vitreus";

// Constants
import { DEFAULT_HEADER_LOGO } from "constants/branding.constant";
import {
    GUID,
    ALPHA_NUMERIC,
} from "constants/regex.constant";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";
import {
    ITokenIdentity,
    IAccessToken,
} from "interfaces/token.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";
import { IOrganization } from "interfaces/org.interface";
import { IAccessLevel } from "interfaces/user-access-level.interface";
import { IBrandingHeader } from "interfaces/branding.interface";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Components
import { SwitchTenantModal } from "sharedModules/components/switch-tenant-modal/switch-tenant-modal.component";
import { ChangeLanguageModal } from "sharedModules/components/change-language-modal/change-language-modal.component";
import { HelpModal } from "sharedModules/components/help-modal/help-modal.component";
import { AboutOnetrustModal } from "sharedModules/components/about-onetrust-modal/about-onetrust-modal.component";
import { SystemTrayModalComponent } from "sharedModules/modules/system-tray/system-tray-modal/system-tray-modal.component";

@Component({
    selector: "one-global-header",
    templateUrl: "./one-global-header.component.html",
})
export class OneGlobalHeader implements OnInit {
    token: IAccessToken;

    showBorders = true;
    branding: IBrandingHeader;
    brandingLogoUrl: string;

    rootOrgId: string;
    currentUser: IUser;
    currentOrgId: string;
    currentOrg: IOrganization;
    currentRoleId: string;
    currentRoleName: string;
    settingsRoute: string;

    canChangeLanguages = false;
    canShowHelpAndPrivacy = false;
    canShowHelpAbout = false;
    canViewHelpCenter = false;
    canShowHome = false;
    canShowProductLauncher = false;
    canShowSearch = false;
    canShowUserProfile = false;
    canShowSettings = false;
    canSwitchOrgs = false;
    canSwitchRoles = false;
    canSwitchTenants = false;
    canViewSupportPortal = false;
    supportPortalLink = "";
    activeItem: string;
    defaultDropdownDelays = [100, 100];
    helpSearchModel: string;
    searchingHelp: boolean;
    launcher = false;
    toggled = false;
    privacyPolicyLink = "";
    canShowSubscriptionSettings = false;
    canSettingsUpgrade = false;

    get currentAccessLevels() {
        return this.globalHeaderState.accessLevels$.value;
    }

    get helpCenterActive() {
        return this.globalHeaderState.helpCenterActive$.value;
    }

    set helpCenterActive(value: boolean) {
        this.globalHeaderState.helpCenterActive$.next(value);
    }

    get tasksCount() {
        return this.globalHeaderState.tasksCount$.value;
    }

    private destroy$ = new Subject();

    constructor(
        @Inject("$rootScope") public $rootScope: IExtendedRootScopeService,
        @Inject(StoreToken) private store: IStore,
        public stateService: StateService,
        public permissions: Permissions,
        private globalHeaderState: GlobalHeaderStateService,
        private principal: Principal,
        private authHandlerService: AuthHandlerService,
        private accounts: Accounts,
        private settingAction: SettingActionService,
        private content: Content,
        private helpCenter: HelpCenter,
        private loadingService: LoadingService,
        private skuManagementModalService: SkuManagementModalService,
        private mcmModalService: McmModalService,
        private otModalService: OtModalService,
    ) {}

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));

        this.globalHeaderState.init();
        this.globalHeaderState.helpCenterActive$
            .pipe(takeUntil(this.destroy$))
            .subscribe((active: boolean) => {
                if (active) {
                    this.launcher = false;
                }
            });

    }

    ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    toggleActiveItem(item: string, value: boolean) {
        if (value && this.activeItem !== item) {
            this.activeItem = item;

            if (this.helpCenterActive) {
                this.helpCenterActive = false;
                this.helpCenter.close();
            }

            if (item === "Launcher") {
                this.toggleLauncher(null, null);
            } else {
                this.closeLauncher();
            }

        } else if (!value && this.activeItem === item || this.helpCenterActive) {
            this.activeItem = "";
        }
    }

    buyNow() {
        this.mcmModalService.openMcmModal("CookiesPaidBuyNow");
    }

    navigateTo(route: StateOrName, params?: RawParams, options?: TransitionOptions) {
        this.launcher = false;
        this.helpCenter.close();
        this.stateService.go(route, params, options);
    }

    settingsNavigate(route: StateOrName) {
        if (this.canSettingsUpgrade) {
            this.mcmModalService.openMcmModal("SettingsUpgrade");
        } else {
            this.stateService.go(route);
        }
    }

    navigateToLanding() {
        this.navigateTo("landing", null, { reload: true });
        this.loadingService.set(true);
    }

    selectAccessLevel(accessLevel: IAccessLevel) {
        if (this.canSwitchRoles && !(accessLevel.organizationId === this.rootOrgId && accessLevel.roleId === this.currentRoleId)) {
            this.currentRoleId = accessLevel.roleId;
            this.$rootScope.contextSwitch(() => {
                this.accounts.refresh(null, accessLevel.organizationId, accessLevel.roleId).then(
                    (response: IProtocolResponse<ITokenIdentity>) => {
                        if (!response.result) return;
                        this.resetAppState();
                        this.navigateToLanding();
                    },
                );
            });
        }
    }

    selectOrganization(organizationId: string) {
        if (this.canSwitchOrgs && organizationId && organizationId !== this.currentOrgId) {
            this.currentOrgId = organizationId;
            this.currentOrg = getOrgById(this.currentOrgId)(this.store.getState());
            this.$rootScope.contextSwitch(() => {
                this.accounts.refresh(null, organizationId).then(
                    (response: IProtocolResponse<ITokenIdentity>) => {
                        if (!response.result) return;
                        this.resetAppState();
                        this.reloadCurrentPage();
                    },
                );
            });
        }
    }

    openTenantSwitchModal() {
        this.otModalService.create(
            SwitchTenantModal,
            {
                isComponent: true,
            },
        ).pipe(
            take(1),
        ).subscribe((id) => {
            if (id) {
                if (Utilities.matchRegex(id, GUID) && id !== this.currentUser.CurrentTenant) {
                    this.$rootScope.contextSwitch(() => {
                        this.loadingService.set(true);
                        this.accounts.refresh(id).then((res) => {
                            if (!res.result) {
                                this.loadingService.set(false);
                                return;
                            }
                            this.resetAppState();
                            this.navigateToLanding();
                        });
                    });
                }
            }
        });
    }

    openLanguageSwitchModal() {
        this.otModalService
            .create(
                ChangeLanguageModal,
                {
                    isComponent: true,
                },
            ).pipe(
                take(1),
            ).subscribe((success) => {
                if (success) {
                    this.resetAppState();
                    this.reloadCurrentPage();
                }
            });
    }

    openHelpModal() {
        this.launcher = false;
        this.toggleActiveItem("help", true);
        this.otModalService
            .create(
                HelpModal,
                {
                    isComponent: true,
                },
            );
    }

    openAboutOnetrustModal() {
        this.launcher = false;
        this.otModalService
            .create(
                AboutOnetrustModal,
                {
                    isComponent: true,
                },
            );
    }

    openTasksModal() {
        this.launcher = false;
        this.otModalService
            .create(
                SystemTrayModalComponent,
                {
                    isComponent: true,
                    size: ModalSize.MEDIUM,
                },
            );
    }

    toggleZenModal() {
        if (!this.helpCenter.helpCenterIsLive) return;
        if (!this.helpCenterActive) {
            this.helpCenterActive = true;
            this.launcher = false;
            this.helpCenter.open();
        } else {
            this.helpCenterActive = false;
            this.helpCenter.close();
        }
    }

    logout() {
        this.$rootScope.contextSwitch(() => {
            this.resetAppState();
            this.authHandlerService.logout();
        });
    }

    getUserInitials(fullName: string): string {
        return join(map(split(fullName, " "), (name) => capitalize(head(name))), "").replace(ALPHA_NUMERIC, "");
    }

    handleSearch(model: string) {
        this.helpSearchModel = model;
    }

    handleHelpSearch() {
        this.searchingHelp = true;
        this.helpCenter.open(this.helpSearchModel || "%");
        this.helpSearchModel = "";
        this.activeItem = "";
        this.searchingHelp = false;
    }

    toggleLauncher(route, params?) {
        this.activeItem = "Launch";
        if (!this.launcher) {
            this.launcher = true;
            this.toggled = true;
        } else {
            this.launcher = false;
        }
        if (route) this.navigateTo(route, params);
    }

    closeLauncher() {
        this.launcher = false;
    }

    openSkuManagementModal() {
        this.skuManagementModalService.openSkuManagementModal(this.currentUser);
    }

    get canViewHelpSearch() {
        return this.canViewHelpCenter && this.helpCenter.helpCenterIsLive;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.rootOrgId = this.principal.getRootOrg();
        this.token = this.principal.getDecodedToken();
        this.currentRoleName = this.token ? this.token.role : "";
        this.currentUser = getCurrentUser(state) || null;
        if (this.currentUser) {
            this.currentOrgId = getCurrentOrgId(state);
            this.currentOrg = this.currentOrgId ? getOrgById(this.currentOrgId)(state) : null;
            this.currentUser.OrgGroupName = this.currentOrg ? this.currentOrg.Name : "";
            this.currentRoleId = this.currentUser.RoleId;
        }
        this.canShowHome = this.permissions.canShow("Welcome");

        this.branding = getHeaderBranding(state);
        this.brandingLogoUrl = this.branding.icon && this.branding.icon.url ? this.branding.icon.url : this.content.GetKey("GlobalHeaderLogo") || DEFAULT_HEADER_LOGO;

        this.canSwitchOrgs = this.permissions.canShow("OrgGroupsSwitch");
        this.canSwitchRoles = this.permissions.canShow("RolesSwitch");

        const tenantCount = this.currentUser && this.currentUser.Tenants ? this.currentUser.Tenants.length : 0;
        const hasMultipleTenants = tenantCount > 1;
        this.canSwitchTenants = this.permissions.canShow("TenantsSwitch") && hasMultipleTenants;

        this.canChangeLanguages = this.$rootScope.languages && this.$rootScope.languages.length > 1;

        this.canShowSettings = this.permissions.canShow("Settings");
        this.settingsRoute = this.getSettingsRoute();
        this.canShowUserProfile = this.permissions.canShow("Profile");
        this.canShowHelpAndPrivacy = this.permissions.canShow("HelpAndPrivacy");
        this.privacyPolicyLink = this.content.GetKey("privacyPolicy");
        this.canShowHelpAbout = this.permissions.canShow("HelpAbout");
        this.canViewSupportPortal = this.permissions.canShow("SupportPortal");
        this.supportPortalLink = this.content.GetKey("SupportPortalLink");
        this.canViewHelpCenter = this.permissions.canShow("HelpCenter");
        this.canShowSubscriptionSettings = this.permissions.canShow("ManageSubscriptions");
        this.canSettingsUpgrade = this.permissions.canShow("SettingsUpgrade");

        this.canShowProductLauncher = false;
        this.canShowSearch = false;
    }

    private reloadCurrentPage() {
        this.loadingService.set(true);
        location.reload();
    }

    private resetAppState() {
        this.loadingService.set(true);
        this.globalHeaderState.reset();
        this.settingAction.resetSettings();
        this.permissions.logout();
        this.$rootScope.SaveReturnUrl = false;
    }

    private getSettingsRoute(): string {
        if (this.permissions.canShow("SettingsHelp")) {
            return "zen.app.pia.module.settings.help-and-privacy";
        } else {
            return "zen.app.pia.module.settings.manage-subscriptions";
        }
    }

}
