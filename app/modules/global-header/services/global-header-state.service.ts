// Angular
import { Inject, Injectable } from "@angular/core";

// rxjs
import { BehaviorSubject } from "rxjs";

// Services
import { NgxPermissionsService } from "ngx-permissions";
import { Principal } from "modules/shared/services/helper/principal.service";
import { AccessLevelsService } from "modules/shared/services/api/access-levels.service";
import { HelpCenter } from "modules/shared/services/provider/help-center.service";
import { Userlane } from "modules/shared/services/provider/userlane.service";
import { Wootric } from "modules/shared/services/provider/wootric.service";
import { TasksService } from "sharedServices/tasks.service";

// Interfaces
import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITaskListRequest } from "interfaces/tasks.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IAccessLevel } from "interfaces/user-access-level.interface";

@Injectable()
export class GlobalHeaderStateService {

    helpCenterActive$ = new BehaviorSubject(false);
    tasksCount$ = new BehaviorSubject(0);
    accessLevels$ = new BehaviorSubject([]);

    private initWidget$ = new BehaviorSubject(false);

    private tasksUpdatedListener = this.$rootScope.$on(
        "tasks-updated",
        (event: ng.IAngularEvent, count: number) => {
            this.tasksCount$.next(count);
        },
    );

    constructor(
        @Inject("$rootScope") private $rootScope,
        private permissions: NgxPermissionsService,
        private principal: Principal,
        private accessLevelsService: AccessLevelsService,
        private helpCenter: HelpCenter,
        private userlane: Userlane,
        private wootric: Wootric,
        private tasksService: TasksService,
    ) {}

    init() {
        this.fetchAccessLevels();
        this.initializeTasks();
        this.initializeWidgets();
    }

    fetchAccessLevels() {
        if (this.accessLevels$.value.length) return;
        if (this.permissions.getPermission("RolesSwitch")) {
            this.accessLevelsService.getCurrentAccess().then((response: IProtocolResponse<IAccessLevel[]>) => {
                const levels = response.result && response.data ? response.data : [];
                this.accessLevels$.next(levels);
            });
        }
    }

    initializeWidgets() {
        if (this.initWidget$.value) return;
        if (this.permissions.getPermission("HelpCenter")) {
            this.initializeHelpCenter();
        }

        if (this.permissions.getPermission("UserGuidance")) {
            this.initializeUserlane();
        }

        if (this.permissions.getPermission("UserFeedBack")) {
            this.initializeWootric();
        }
        this.initWidget$.next(true);
    }

    reset() {
        this.helpCenterActive$.next(false);
        this.tasksCount$.next(0);
        this.accessLevels$.next([]);
        this.initWidget$.next(false);
        this.tasksUpdatedListener();

        if (this.helpCenter.helpCenterIsLive) {
            this.helpCenter.logout();
        }
        if (this.userlane.isActive) {
            this.userlane.logout();
        }
        if (this.wootric.isActive) {
            this.wootric.logout();
        }
    }

    initializeTasks() {
        if (this.initWidget$.value) return;
        const tasksRequest: ITaskListRequest = {
            filterOptions: [],
            pageNumber: 0,
            pageSize: 10000,
            sortOptions: [],
            user: this.principal.getCurrentUserId(),
        };
        this.tasksService.listTasks(tasksRequest).then(
            (response: IProtocolResponse<IPageable>) => {
                const count = response.result && response.data && response.data.content
                    ? this.tasksService.filterTaskCount(response.data.content)
                    : 0;
                this.tasksCount$.next(count);
            },
        );
    }

    toggleZenModal() {
        if (!this.helpCenter.helpCenterIsLive) return;
        if (!this.helpCenterActive$.value) {
            this.helpCenterActive$.next(true);
            this.helpCenter.open();
        } else {
            this.helpCenterActive$.next(false);
            this.helpCenter.close();
        }
    }

    private initializeHelpCenter(): ng.IPromise<void> {
        return this.helpCenter.initialize(() =>
            this.toggleZenModal(),
        );
    }

    private initializeUserlane(): Promise<void> {
        return this.userlane.initialize();
    }

    private initializeWootric(): Promise<void> {
        return this.wootric.initialize();
    }
}
