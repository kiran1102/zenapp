// Angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

// Modules
import { UIRouterModule } from "@uirouter/angular";
import { TranslateModule } from "@ngx-translate/core";
import { NgxPermissionsModule } from "ngx-permissions";
import {
    OtPopoverModule,
    OtButtonModule,
    OtGlobalHeaderModule,
    OtSearchModule,
} from "@onetrust/vitreus";
import { SharedModule } from "modules/shared/shared.module";
import { SkuModule } from "modules/sku/sku.module";
import { SystemTrayModule } from "sharedModules/modules/system-tray/system-tray.module";

// Components
import { OneGlobalHeader } from "./one-global-header/one-global-header.component";
import { LauncherNav } from "./one-global-header/one-launcher-nav/one-launcher-nav.component";

// Services
import { GlobalHeaderStateService } from "./services/global-header-state.service";
import { TasksService } from "sharedServices/tasks.service";

// Legacy Provider Injections
export function getLegacyTasksService(injector) {
    return injector.get("TASKS");
}

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RouterModule,
        UIRouterModule,
        TranslateModule,
        NgxPermissionsModule.forChild(),
        OtGlobalHeaderModule,
        OtPopoverModule,
        OtButtonModule,
        OtSearchModule,
        SkuModule,
        SystemTrayModule,
    ],
    providers: [
        GlobalHeaderStateService,
        {
            provide: TasksService,
            deps: ["$injector"],
            useFactory: getLegacyTasksService,
        },
    ],
    declarations: [
        OneGlobalHeader,
        LauncherNav,
    ],
    entryComponents: [
        OneGlobalHeader,
    ],
    exports: [
        OneGlobalHeader,
    ],
})
export class GlobalHeaderModule {}
