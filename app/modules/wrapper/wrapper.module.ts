import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { GlobalHeaderModule } from "modules/global-header/global-header.module";
import { GlobalSidebarModule } from "modules/global-sidebar/global-sidebar.module";

import { WrapperComponent } from "./wrapper/wrapper.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        GlobalHeaderModule,
        GlobalSidebarModule,
    ],
    declarations: [WrapperComponent],
})
export class WrapperModule {}
