// Core
import { Component } from "@angular/core";

// Services
import { AuthEntryService } from "modules/core/services/auth-entry.service";

@Component({
    template: `
        <one-global-header class="static-vertical"></one-global-header>
        <div class="stretch-vertical row-nowrap">
            <one-global-sidebar></one-global-sidebar>
            <div id="ot-page-wrapper" class="stretch-horizontal position-relative z-index-0">
                <router-outlet></router-outlet>
            </div>
        </div>
    `,
    host: { class: "flex-full-height" },
})
export class WrapperComponent {
    constructor(
        private authEntry: AuthEntryService,
    ) {}

    ngOnInit() {
        this.authEntry.init();
    }
}
