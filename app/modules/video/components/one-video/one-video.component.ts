// Core
import {
    Component,
    Input,
    OnInit,
    OnDestroy,
    AfterViewInit,
} from "@angular/core";
import { SafeUrl } from "@angular/platform-browser";

import { isUndefined } from "lodash";
import Utilities from "Utilities";

// Redux
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { VideoTypes } from "sharedModules/enums/video-types.enum";

declare var Vimeo: any;

@Component({
    selector: "one-video",
    template: `
        <section
            class="one-video full-width position-relative"
            *ngIf="ready"
            [id]="conatinerId"
            [ngClass]="videoClass"
            >
            <div *ngIf="isIframe" class="position-absolute position-absolute-full z-index-2">
                <iframe
                    class="one-video__player full-width"
                    [src]="trustedUrl"
                    frameborder="0"
                    webkitallowfullscreen
                    mozallowfullscreen
                    allowfullscreen
                ></iframe>
            </div>
            <div *ngIf="!isIframe" [id]="iFrameId" class="position-absolute position-absolute-full z-index-2">
                <div
                    class="one-video__player full-width"
                    [ngClass]="playerClass"
                ></div>
            </div>
            <ot-loading class="z-index-1" otPageLoading></ot-loading>
        </section>
    `,
})
export class OneVideoComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() url: string;
    @Input() videoClass: string;

    videoTypes = VideoTypes;
    uniqueId = Utilities.uuid();
    conatinerId = `one-video__${this.uniqueId}`;
    iFrameId = `one-video__iframe__${this.uniqueId}`;
    videoType: number;
    trustedUrl: SafeUrl;
    videoId = "";
    player: any;
    playerClass: string;
    isIframe = true;
    ready = false;
    destroy$ = new Subject();

    ngOnInit() {
        this.setupVideo(this.url);
    }

    ngAfterViewInit() {
        if (this.videoType === this.videoTypes.WISTIA) {
            const containerElement = document.createElement("script");
            containerElement.setAttribute("src", "//fast.wistia.com/assets/external/E-v1.js");
            document.getElementById(this.conatinerId).appendChild(containerElement);
        }

        if (!this.isIframe) {
            const iFrameElement = document.createElement("script");
            iFrameElement.setAttribute("src", this.trustedUrl as string);
            document.getElementById(this.iFrameId).appendChild(iFrameElement);
        }
    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    private setupVideo(url: string): void {
        this.ready = false;
        this.videoType = this.getVideoType(url);
        this.configVideoByType(this.videoType, url);
        this.trustedUrl = this.getTrustedUrl(url);
        this.setupDestroy(this.videoType, this.videoId);
        this.ready = Boolean(!isUndefined(this.videoType) && this.trustedUrl);
    }

    private getVideoType(url: string): number {
        if (!url) return this.videoTypes.OTHER;
        else if (url.indexOf("vimeo") > -1) return this.videoTypes.VIMEO;
        else if (url.indexOf("wistia") > -1) return this.videoTypes.WISTIA;
        else if (url.indexOf("youtube") > -1) return this.videoTypes.YOUTUBE;
        else return this.videoTypes.OTHER;
    }

    private configVideoByType(videoType: number, url: string): void | undefined {
        if (!videoType || !url) return;
        if (videoType === this.videoTypes.VIMEO) {
            setTimeout(() => {
                const iframe: HTMLIFrameElement = document.querySelector("iframe");
                this.player = new Vimeo.Player(iframe);
            }, 0);
        } else if (videoType === this.videoTypes.WISTIA) {
            this.isIframe = false;
            const indexOfMedias: number = url.indexOf("medias/");
            const indexOfJsonp: number = url.indexOf(" async");
            const startIndex: number = indexOfMedias > -1 ? indexOfMedias + 7 : 0;
            const endIndex: number = indexOfJsonp > -1 ? indexOfJsonp : url.length;
            this.videoId = url.substring(startIndex, endIndex);
            this.playerClass = `wistia_embed wistia_async_${this.videoId} videoFoam=true`;
        }

    }

    private getTrustedUrl(url: string): SafeUrl {
        // TODO find new way to generate this
        return url;
    }

    private setupDestroy(videoType: number, videoId?: string): void | undefined {
        if (!videoType) return;
        if (videoType === this.videoTypes.WISTIA) {
            (window as any)._wq = (window as any)._wq || [];
            (window as any)._wq.push({
                id: videoId,
                onReady: (player: any): void => {
                    this.destroy$.pipe(
                        takeUntil(this.destroy$),
                    ).subscribe(() => {
                        player.remove();
                    });
                },
            });
        }
    }
}
