// Modules
import {
    NgModule,
    CUSTOM_ELEMENTS_SCHEMA,
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { OtLoadingModule } from "@onetrust/vitreus";

import { OneVideoComponent } from "./components/one-video/one-video.component";

@NgModule({
    imports: [
        CommonModule,
        OtLoadingModule,
    ],
    exports: [
        OneVideoComponent,
    ],
    declarations: [
        OneVideoComponent,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class OneVideoModule { }
