// Angular
import {
    Component,
    OnInit,
    ViewChild,
} from "@angular/core";

// Common
import {
    authConfig,
    OAuthenticationService,
} from "@onetrust/common";

// Vitreus
import {
    OtModalHostDirective,
    OtModalService,
} from "@onetrust/vitreus";

// Services
import { LoadingService } from "modules/core/services/loading.service";
import { Principal } from "modules/shared/services/helper/principal.service";

@Component({
    selector: "app-root",
    template: `
        <ng-container *ngIf="isLoading">
            <ot-loading otPageLoading class="app-loading flex-full-height"></ot-loading>
        </ng-container>
        <div [ngClass]="{'display-none': isLoading, 'flex-full-height full-size stretch-vertical': !isLoading}">
            <router-outlet></router-outlet>
            <div ui-view class="full-size"></div>
        </div>
        <ng-template otModalHost></ng-template>
    `,
    host: {
        class: "flex-full-height column-nowrap full-size",
    },
})
export class AppComponent implements OnInit {
    @ViewChild(OtModalHostDirective) otModalHost: OtModalHostDirective;

    isLoading: boolean;
    private initialLoadingEl = document.querySelector(".initial-app-loading");

    constructor(
        loadingService: LoadingService,
        private principal: Principal,
        private otModalService: OtModalService,
        private oauthService: OAuthenticationService,
    ) {
        this.initialLoadingEl.parentNode.removeChild(this.initialLoadingEl);
        this.initialLoadingEl = null;
        loadingService.active$.subscribe((isActive: boolean) => {
            this.isLoading = isActive;
        });
        this.configureAuth();
    }

    ngOnInit() {
        this.otModalService.hostElement = this.otModalHost;
    }

    private configureAuth() {
        this.principal.identity(true);
        this.oauthService.oauthService.configure(authConfig);
        this.oauthService.oauthService.setStorage(localStorage);
    }
}
