export enum AssesmentTemplateTypes {
    PIA = 10,
    Asset = 20,
    ProcessingActivity = 30,
}
