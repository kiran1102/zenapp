export enum ProjectModes {
    AssessmentMode = 1,
    ReviewMode = 2,
    ReadOnlyMode = 3,
    SuspendMode = 4,
}

export enum AssessmentTypes {
    PIA = 1,
    DM = 2,
    PIAV2 = 3,
}

export enum DMAssessmentStates {
    All = 0,
    NeedsAttention = 1,
    NotStarted = 10,
    InProgress = 20,
    Completed = 30,
    Archived = 40,
    UnderReview = 50,
}
