export enum TemplateTypes {
    Custom = 10,
    Readiness = 20,
    RequestForm = 30,
    Datamapping = 40,
    SelfService = 50,
    NewDatamapping = 60,
}
