export enum TasksViews {
    Create = "createView",
    Update = "updateView",
    List = "listView",
}
