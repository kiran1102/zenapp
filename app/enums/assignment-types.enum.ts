export enum AssignmentTypes {
    Lead = 10,
    Approver = 20,
    ProjectViewer = 30,
}
