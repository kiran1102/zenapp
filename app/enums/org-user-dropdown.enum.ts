export enum OrgUserDropdownTypes {
    Default = 0,
    RespondentPia = 1,
    ApproverPia = 2,
    RespondentDm = 3,
    AssignRespondentDm = 4,
    ApproverDm = 5,
    RiskOwner = 6,
    HelpContact = 7,
    DSARSubTaskAssignee = 8,
    DSARWebFromApprover = 9,
    DSARRequestAssignee = 10,
    OrgGroupDefaultApprover = 11,
    DMDetails = 12,
}

export enum OrgUserDropdownValidations {
    checkInvitedUser = 1,
    checkProjectViewer = 2,
    checkDisabledUser = 3,
}

export enum OrgUserTraversal {
    Up = "UP",              // Current Org + Up the tree
    Down = "DOWN",          // Current Org + Down the tree
    Branch = "BRANCH",      // Current Org + Up and Down the tree
    Current = "CURRENT",    // Current Org Only
    All = "ALL",            // All Orgs
}
