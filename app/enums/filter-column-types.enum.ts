export enum FilterColumnTypes {
    Text = 10,
    Number = 20,
    Multi = 30,
    MultiInt = 31,
    DateTime = 40,
    DateRange = 41,
    Date = 42,
    User = 50,
    Link = 60,
}
