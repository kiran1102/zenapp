export enum AAErrorCode {
    UNAUTHORIZED_INVENTORY_ACTION = "ERROR_ASSESSMENT-V2_INVENTORY_ACTION_UNAUTHORIZED",
    INVITED_USER_CREATE_FAILED = "ERROR_ASSESSMENT-V2_INVITED_USER_CREATION_FAILED",
    INVALID_SEARCH_CRITERIA = "ERROR_ASSESSMENT-V2_INVALID_SEARCH_CRITERIA",
    INVALID_APPROVER_RESPONDENT_CHANGE_AFTER_DEADLINE = "ERROR_ASSESSMENT-V2_INVALID_APPROVER_RESPONDENT_CHANGE_AFTER_DEADLINE",
}
