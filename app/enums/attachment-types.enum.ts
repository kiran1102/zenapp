export enum AttachmentTypes {
    Question = 10,
    LogoBranding = 20,
    TitleBranding = 21,
    DSAR = 30,
    Inventory = 50,
}
