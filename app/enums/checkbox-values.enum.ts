export enum CheckboxValues {
    Check = 10,
    Cross = 20,
    Minus = 30,
    Question = 40,
}
