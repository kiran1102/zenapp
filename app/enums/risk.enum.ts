export enum RiskLevels {
    None = 0,
    Low = 1,
    Medium = 2,
    High = 3,
    VeryHigh = 4,
}

export const RiskPrettyNames = {
    0: "None",
    1: "Low",
    2: "Medium",
    3: "High",
    4: "Very High",
};

export enum RiskLabel {
    "RISK_LOW" = "risk-low",
    "RISK_MEDIUM" = "risk-medium",
    "RISK_HIGH" = "risk-high",
    "RISK_VERY_HIGH" = "risk-very-high",
}

export enum RiskLabelBgClasses {
    "risk-low--bg" = 1,
    "risk-medium--bg" = 2,
    "risk-high--bg" = 3,
    "risk-very-high--bg" = 4,
}

export enum RiskLabelBgClassesV2 {
    "LOW" = "risk-low--bg",
    "MEDIUM" = "risk-medium--bg",
    "HIGH" = "risk-high--bg",
    "VERY_HIGH" = "risk-very-high--bg",
}

export enum RiskTypes {
    Question = 10,
    Inventory = 50,
}

export enum RiskSourceTypes {
    PIA = 10,
    Readiness = 20,
    RequestForm = 30,
    DataMapping = 40,
    Inventory = 50,
}

export enum RiskReferenceTypes {
    Question = 10,
    Section = 20,
    Assessment = 30,
}

export enum RiskCharacterLimit {
    Limit = 4000,
}

export enum RiskTranslations {
    "LOW" = "Low",
    "MEDIUM" = "Medium",
    "HIGH" = "High",
    "VERY_HIGH" = "VeryHigh",
}

export const RiskLevelTranslations = {
    0: "None",
    1: "Low",
    2: "Medium",
    3: "High",
    4: "VeryHigh",
};
