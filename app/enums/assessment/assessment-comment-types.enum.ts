export enum AANotesTypes {
    AllNote = "ALL",
    OnlyMeNote = "ONLY_ME",
    ApproversNote = "APPROVER",
    EveryoneNote = "EVERYONE",
}
