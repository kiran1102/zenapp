export enum QuestionResponseType {
    Justification = "JUSTIFICATION",
    NotApplicable = "NOT_APPLICABLE",
    NotSure = "NOT_SURE",
    Others = "OTHERS",
    Default = "DEFAULT",
}

export enum QuestionLockReason {
    LaunchFromInventory = "LAUNCH_FROM_INVENTORY",
}

export enum AAQuestionErrorCode {
    ATTRIBUTE_DISABLED = "ATTRIBUTE_DISABLED",
    ATTRIBUTE_OPTION_DISABLED = "ATTRIBUTE_OPTION_DISABLED",
    INVENTORY_NOT_EXISTS = "INVENTORY_NOT_EXISTS",
    RELATED_INVENTORY_ATTRIBUTE_DISABLED = "RELATED_INVENTORY_ATTRIBUTE_DISABLED",
    DATA_ELEMENT_NOT_EXISTS = "DATA_ELEMENT_NOT_EXISTS",
    DUPLICATE_INVENTORY = "DUPLICATE_INVENTORY",
}

export enum AAQuestionNotification {
    OTHER_RESPONSE_NOT_ALLOWED = "OtherResponseNotAllowedQuestionNotification",
    NOT_SURE_NOT_ALLOWED = "NotSureNotAllowedQuestionNotification",
    NOT_APPLICABLE_NOT_ALLOWED = "NotApplicableNotAllowedQuestionNotification",
    NEW_QUESTION = "NewQuestionQuestionNotification",
    MULTI_SELECT_NOT_ALLOWED = "MultiSelectNotAllowedQuestionNotification",
    OPTION_REMOVED = "OptionRemovedQuestionNotification",
}
