export enum AssessmentStatus {
    Completed = "COMPLETED",
    In_Progress = "IN_PROGRESS",
    Not_Started = "NOT_STARTED",
    Under_Review = "UNDER_REVIEW",
    Read_Only = "READ_ONLY",
}

export enum SendBackType {
    SendBackToInProgress = "SEND_BACK_TO_IN_PROGRESS",
    SendBackToUnderReview = "SEND_BACK_TO_UNDER_REVIEW",
}

export enum AssessmentActivity {
    ALL = "ALL",
    STATE_CHANGED = "STATE_CHANGED",
    NAME_CHANGED = "NAME_CHANGED",
    DEADLINE_CHANGED = "DEADLINE_CHANGED",
    APPROVER_CHANGED = "APPROVER_CHANGED",
    RESPONDENT_CHANGED = "RESPONDENT_CHANGED",
    VIEWER_CHANGED = "VIEWER_CHANGED",
    TEMPLATE_CHANGED = "TEMPLATE_CHANGED",
}

export enum ApprovalStatus {
    Open = "OPEN",
    Approved = "APPROVED",
    Rejected = "REJECTED",
}
