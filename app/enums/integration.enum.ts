export enum ConnectionManageTabs {
    AUTHENTICATION = "AUTHENTICATION",
    ADVANCED = "ADVANCED",
    API_DEFINITION = "API_DEFINITION",
    DATA_SUBJECT_ELEMENTS = "DATA_SUBJECT_ELEMENTS",
    DATA_SUBJECT_PROFILE = "DATA_SUBJECT_PROFILE",
    ADVANCED_PREFERENCE = "ADVANCED_PREFERENCE",
    CONTACT_MAPPING = "CONTACT_MAPPING",

}

export enum ConnectionMappingPrefixes {
    ELOQUA = "Purpose:",
    ELOQUA_CONTACT_MAPPING = "DSElementMapping",
    ELOQUA_DATASUBJECT_PROFILE = "DSProfileMapping:Purpose:",
    ELOQUA_PREFERENCE_PURPOSE = "DSPreferenceMapping:Purpose:",
    ELOQUA_PREFERENCE_CDO = ":CDO:",
}

export enum ConnectionManageMode {
    SYSTEMS = 10,
    CREATE = 20,
    EDIT = 30,
}

export enum WorkflowBuilderSteps {
    TRIGGER_STEP = 1,
    CREDENTIAL_STEP = 2,
    REQUEST_RESPONSE_STEP = 3,
    DELAY_LOGIC = 4,
    APPLY_EACH = 5,
}
