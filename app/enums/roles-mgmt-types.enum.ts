export enum TemplateModes {
    EditRole = "ROLE_EDIT",
    CopyRole = "ROLE_COPY",
    DeleteRole = "ROLE_DELETE",
    ExportRole = "EXPORT_DELETE",
    ReplaceRole = "REPLACE_DELETE",
}
