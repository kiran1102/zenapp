export enum TransitionTimes {
    AssessmentSlide = 1000,
    SectionNavFade = 300,
    PopupDelay = 1000,
    TooltipDelay = 500,
}

export enum SortTypes {
    None = 0,
    Ascending = 1,
    Descending = 2,
}

export enum FilterDisplayTypes {
    CollectionPicker = 1,
    Select = 2,
    Toggle = 3,
}

export enum CellTypes {
    text = 1,
    link = 2,
    date = 3,
}

export enum LanguageIds {
    English = 1,
}
