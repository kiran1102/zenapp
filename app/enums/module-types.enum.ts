export enum ModuleTypes {
    PIA = 10,  // Old PIA
    AE = 15,   // Assessment Export (temporary)
    DM = 20,   // Data Mapping
    VRM = 30,  // Vendor Risk Management
    VRME = 35, // Vendor Export (temporary)
    GRAR = 37, // Only used for Reports
    CM = 40,   // Consent Management
    RISK = 50, // New Risks
    INV = 60,  // Inventory
    GRA = 70,  // Global Readiness Assessment
    INC = 80,  // Incident
}

export enum ModuleTranslationKeys {
    AssessmentAutomation = 10,
    DataMapping = 20,
    VendorRiskManagement = 30,
    ConsentManager = 40,
    Risk = 50,
    DataMappingInventory = 60,
    GlobalReadinessAssessment = 70,
    Incident = 80,
}
