export enum InventoryTableIds {
    Elements = 10,
    Assets = 20,
    Processes = 30,
    Vendors = 50,
    Entities = 60,
    Incidents = 90,
}

export enum TableInputTypes {
    Text = 10,
    Typeahead = 20,
    List = 30,
    User = 40,
    Date = 50,
    Link = 60,
    Popup = 70,
    PopupList = 71,
    PopupTable = 72,
    PopupCard = 73,
    ProjectState = 80,
    Actions = 90,
    Tooltip = 100,
}

export enum InputTypes {
    Text = 10,
    Select = 20,
    List = 25,
    Multiselect = 30,
    Date = 40,
    Modal = 50,
}

export enum OptionTypes {
    Custom = 0,
    DataElements = 10,
    Assets = 20,
    Processes = 30,
    Users = 300,
    Orgs = 310,
    Locations = 400,
    Subjects = 410,
    Categories = 420,
}

export enum InventoryContexts {
    List = 10,
    Details = 20,
    Add = 30,
    Sidebar = 40,
    EditSidebar = 50,
    Related = 60,
    Lineage = 70,
    LineageSidebar = 80,
}

export enum OptionStates {
    Active = 10,
    Archived = 20,
}

export enum InventoryStatuses {
    Approved = 10,
    Pending = 20,
    Disabled = 30,
    Archived = 40,
}

export enum InventoryFieldNames {
    RiskLevel = "riskLevel",
    RiskScore = "riskScore",
    Status = "status",
}

export enum RelationshipLinkType {
    Destination = "DestinationAccessLinkType",
    Source = "SourceCollectionLinkType",
    Related = "RelatedLinkType",
    Processing = "StorageProcessingLinkType",
}

export enum InventoryRiskStates {
    Identified = "10",
    RecommendationAdded = "20",
    RecommendationSent = "25",
    RemediationProposed = "30",
    ExceptionRequested = "40",
    Reduced = "45",
    Retained = "55",
}

export enum InventoryRiskType {
    Question = "10",
    Inventory = "50",
}

export enum InventoryStatusOptions {
    Pending = "pending",
    Active = "active",
    Archived = "archived",
}

export enum DefaultTableConfig {
    defaultSize = 5,
    expandedSize = 20,
}

export enum InventorySettingValueTypes {
    Boolean = "boolean",
    String = "string",
}

export enum InventoryRiskControlsTabColumnNames {
    CONTROL_ID = "identifier",
    NAME = "name",
    FRAMEWORK = "frameworkName",
    CATEGORY = "categoryName",
    STATUS = "status",
}

export enum InventoryRiskControlStatusNames {
    Pending = "Pending",
    Implemented =  "Implemented",
    NotDoing = "NotDoing",
}

export enum InventoryRiskControlsFilterOperator {
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
    EQUAL_TO = "EQUAL_TO",
}

export enum ChangeInventoryStatusCloseAction {
    RELOAD_LIST = "RELOAD_LIST",
    SET_RECORD = "SET_RECORD",
}
