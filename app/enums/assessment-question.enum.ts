export enum QuestionTypes {
    Content = 1,
    TextBox = 2,
    Multichoice = 3,
    YesNo = 4,
    DateTime = 5,
    DataElement = 110,
    Application = 1000,
    Location = 1010,
    User = 1020,
    Provider = 1030,
    ApplicationGroup = 1040,
    PlaceholderGroup = 1050,
}

export enum QuestionStates {
    Unanswered = 10,
    Answered = 20,
    Accepted = 30,
    InfoRequested = 40,
    InfoAdded = 45,
    Skipped = 50,
}

export enum QuestionResponseTypes {
    Value = 1,
    Reference = 2,
    Other = 3,
    NotSure = 4,
    NotApplicable = 5,
    Justification = 6,
}

export enum QuestionMultiChoiceTypes {
    Button = 10,
    Dropdown = 20,
}

export enum QuestionDataElementStringKeys {
    Header = 10,
    Subheader = 20,
}

export enum QuestionInventoryTableIds {
    Elements = "10",
    Applications = "20",
    Assets = "20",
    Processes = "30",
}

export enum AssessmentSubmitStates {
    Submit = 10,
    SendBack = 20,
    Approve = 30,
    GoToAnalsis = 40,
    NextSteps = 50,
    SendRecommendations = 60,
    SendMitigations = 70,
    CompleteAnalysis = 80,
}
