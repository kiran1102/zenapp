export enum RiskStateTransitionTypes {
    Add = 10,
    Edit = 20,
    Revert = 30,
    Approve = 40,
    Reject = 50,
}
