export enum RiskV2Levels {
    NONE = 0,
    LOW = 1,
    MEDIUM = 2,
    HIGH = 3,
    VERY_HIGH = 4,
}

export enum RiskV2SourceTypes {
    PIA = 10,
    READINESS_ASSESSMENT = 20,
    REQUEST_FORM = 30,
    DM = 40,
    INVENTORY = 50,
}

export enum RiskV2ReferenceTypes {
    QUESTION = 10,
    ASSESSMENT = 30,
    INVENTORY = 50,
}

export enum RiskV2States {
    NONE = "NONE",
    IDENTIFIED = "IDENTIFIED",                    // when risk is first created
    RECOMMENDATION_ADDED = "RECOMMENDATION_ADDED",          // when risk has recommendation
    RECOMMENDATION_SENT = "RECOMMENDATION_SENT",
    REMEDIATION_PROPOSED = "REMEDIATION_PROPOSED",
    EXCEPTION_REQUESTED = "EXCEPTION_REQUESTED",
    REDUCED = "REDUCED",
    RETAINED = "RETAINED",
    ARCHIVED_IN_VERSION = "ARCHIVED_IN_VERSION",
    RECOMMENDATION_REMOVED = "RECOMMENDATION_REMOVED",
    REMEDIATION_REMOVED = "REMEDIATION_REMOVED",
    EXCEPTION_REMOVED = "EXCEPTION_REMOVED",
}

export enum RiskV2Types {
    QUESTION = 10,
    INVENTORY = 50,
}

export enum RiskV2ColumnTypes {
    Link = "link",
    TextBox = "textbox",
    Date = "date",
    DateTime = "dateTime",
    Org = "org",
    Status = "status",
    Icon = "icon",
    ControlsCount = "controlsCount",
}

export enum InventoryRiskFilterTypes {
    RiskLevel = 10,
    RiskState = 20,
    Type = 30,
    Organization = 40,
    RiskOwner = 50,
    Category = 60,
}
