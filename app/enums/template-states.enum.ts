export enum TemplateStates {
    Draft = "DRAFT",
    Locked = "LOCKED",
    Published = "PUBLISHED",
}

export enum DMTemplateStates {
    Draft = 10,
    Published = 20,
}
