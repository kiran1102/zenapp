export enum NeedsMoreInfoFilterState {
    All = "ALL",
    Open = "OPEN",
    Close = "CLOSED",
}

export enum NeedsMoreInfoState {
    Open = 10,
    Close = 20,
}

export const NeedsMoreInfoResponseStateKeys = {
    CLOSED: {
        text: "Closed",
        type: "success",
    },
    OPEN: {
        text: "Open",
        type: "",
    },
};
