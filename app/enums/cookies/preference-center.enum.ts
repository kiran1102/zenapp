export enum PrefrenceCenterDetailsToggle {
    ShowCookiesList,
    ShowLinkToCookiepedia,
    ShowSubGroupDescription,
    ShowSubGroupCookies,
    SubGroupOptOut,
    ShowCloseButton,
}

export enum EditablePreferenceItems {
    TITLE,
    YOUR_PRIVACY_TITLE,
    YOUR_PRIVACY_TEXT,
    COOKIE_POLICY_LINK,
    ALLOW_ALL_COOKIES_BUTTON,
    SAVE_SETTINGS_BUTTON,
    ACTIVE_LABEL,
    INACTIVE_LABEL,
    ALWAYS_ACTIVE_LABEL,
    COOKIES_USED_LABEL,
    LINK_TO_COOKIEPEDIA,
    SHOW_COOKIES_LIST,
    SHOW_SUBGROUP_DESC,
    SHOW_SUBGROUP_COOKIES,
    SUBGROUP_OPT_OUT,
    SHOW_CLOSE_BUTTON,
    PRIMARY_COLOR,
    BUTTON_COLOR,
    MENU_COLOR,
    MENU_HIGHLIGHT_COLOR,
    YOUR_LOGO,
}
