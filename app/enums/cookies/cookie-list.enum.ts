export enum CookieGroupStatus {
    Active = 1,
    Inactive = 2,
    AlwaysActive = 3,
    DoNotTrack = 4,
    InactiveLandingPage = 5,
}

export enum CookieConsentModel {
    InformationOnly = 1,
    ImpliedConsent = 2,
    ExplicitConsent = 3,
    OwnerDefined = 4,
    SoftOptIn = 5,
}

export enum BannerLanguageStatus {
    COMPLETED = "COMPLETED",
    INPROGRESS = "IN PROGRESS",
    FAILED = "FAILED",
}

export enum LanguageRequestMethod {
    ADD = "ADD",
    DELETE = "DELETE",
    UPDATE = "UPDATE",
}

export enum CookiePageName {
    ccScriptIntegration,
    ccBannerPage,
    ccPolicyPage,
    ccPreferenceCenter,
}

export enum CookieConsentPolicyConsentModel {
    NoticeOnly = 1,
    OptOut = 2,
    OptIn = 3,
    ImpliedConsent = 5,
}
