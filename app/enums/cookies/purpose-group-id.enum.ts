export enum PurposeGroupId {
    UnAssignedCookies = 0,
    StrictlyNecessaryCookies = 1,
    PerformanceCookies = 2,
    FunctionalCookies = 3,
    TargettingCookies = 4,
    SocialMediaCookies = 8,
}
