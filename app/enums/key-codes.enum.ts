export enum KeyCodes {
    Enter = 13,
    Esc = 27,
    Space = 32,
    Tab = 9,
    Key_0 = 48,
    Key_9 = 57,
    Period = 190,
    Backspace = 8,
    Delete = 46,
    Left_arrow = 37,
    Right_arrow = 39,
}
