export enum ComparisonOperators {
    EQUAL_TO = "EQUAL_TO",
    NOT_EQUAL_TO = "NOT_EQUAL_TO",
}

export enum LogicalOperators {
    AND = "AND",
    OR = "OR",
}
