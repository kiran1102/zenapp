export enum OptionTypes {
    Custom = 0,
    DataElements = 10,
    Assets = 20,
    Processes = 30,
    Users = 300,
    Orgs = 310,
    Locations = 400,
    Subjects = 410,
    Categories = 420,
}
