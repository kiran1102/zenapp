export enum ContextTypes {
    Inventory = 10,
    Assessment = 20,
    DataMapping = 30,
    DMAssessment = 40,
    DMTemplate = 50,
}
