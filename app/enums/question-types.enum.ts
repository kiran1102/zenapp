export enum DMQuestionTypes {
    Welcome = 10,
    Info = 20,
    AppInfo = 30,
    MultiSelect = 40,
    SingleSelect = 50,
    Text = 60,
    Bumper = 70,
    MultiSelectLoop = 80,
    TypeaheadMultiselectLoop = 90,
    DataSubject = 100,
    Asset = 110,
    AssetSingleSelect = 115,
    Process = 120,
    ProcessSingleSelect = 125,
    SingleSelectLoop = 130,
}

export enum DMQuestionDisplayTypes {
    Buttons = 10,
    Typeahead = 20,
    Dropdown = 30,
    CheckList = 40,
}

export enum QuestionAttributeTypes {
    Text = 10,
    Select = 20,
    MultiSelect = 30,
}

export enum IQuestionTypes {
    Content = 1,
    TextBox = 2,
    Multichoice = 3,
    YesNo = 4,
    DateTime = 5,
    DataElement = 110,
    Application = 1000,
    Location = 1010,
    User = 1020,
    Provider = 1030,
    ApplicationGroup = 1040,
    PlaceholderGroup = 1050,
    AssetDiscovery = 1060,
}

export enum MultiChoiceTypes {
    Multichoice = 10,
    AssetDiscovery = 20,
}

export enum QuestionAnswerTypes {
    Button = "button",
    Dropdown = "dropdown",
}
