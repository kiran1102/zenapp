export enum RiskStates {
    Ignored = 0, // added to skip RiskState comparison in risk-permission.service
    Identified = 10, // when risk is first created
    Analysis = 20, // when risk has recommendation
    Addressed = 30, // when risk has mitigation
    Exception = 40, // when risk has exceptionRequest
    Reduced = 45, // when risk was in Addressed state, then was accepted
    Retained = 55, // when risk was in Exception state, then was accepted
    Archived = 60, // unused
}
