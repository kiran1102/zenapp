export enum StatusCodes {
    Success = 200,
    Created = 201,
    BadRequest = 400,
    Forbidden = 403,
    NotFound = 404,
    Conflict = 409,
    ServerError = 500,
}
