export enum ExportTypes {
    PDF = 1,
    CSV = 2,
    JSON = 3,
    XLSX = 4,
    ZIP = 5,
    XLS = 6,
    SVG = 7,
}
