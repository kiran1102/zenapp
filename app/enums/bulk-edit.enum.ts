export enum BulkEditFilterTypes {
    RiskLevel = 1,
    Organization = 2,
    Search = 3,
    QuestionResponseState = 4,
}
