export enum TemplateModes {
    Create = "CREATE",
    Edit = "EDIT",
    Readonly = "READONLY",
}
