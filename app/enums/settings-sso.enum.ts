export enum BindingTypes {
    Post = 1,
    Redirect = 2,
    Artifact = 3,
}

export enum SamlAttributeType {
    Organization = 5,
}
