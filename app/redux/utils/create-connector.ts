import invariant from "invariant";
import getStateSlice from "./get-state-slice";
import updateTarget from "./update-target";
import shallowEqual from "./shallow-equal";
import wrapActionCreators from "./wrap-action-creators";
import * as _ from "lodash";

const defaultMapStateToTarget = () => ({});
const defaultMapDispatchToTarget = (dispatch) => ({ dispatch });

export default function createConnect(store) {
    return (mapStateToTarget, mapDispatchToTarget) => {

        let finalMapStateToTarget = mapStateToTarget || defaultMapStateToTarget;

        const finalMapDispatchToTarget = _.isPlainObject(mapDispatchToTarget) ?
            wrapActionCreators(mapDispatchToTarget) :
            mapDispatchToTarget || defaultMapDispatchToTarget;

        invariant(
            _.isFunction(finalMapStateToTarget),
            "mapStateToTarget must be a Function. Instead received %s.", finalMapStateToTarget,
        );

        invariant(
            _.isPlainObject(finalMapDispatchToTarget) || _.isFunction(finalMapDispatchToTarget),
            "mapDispatchToTarget must be a plain Object or a Function. Instead received %s.", finalMapDispatchToTarget,
        );

        let slice = getStateSlice(store.getState(), finalMapStateToTarget, false);
        const isFactory = _.isFunction(slice);

        if (isFactory) {
            finalMapStateToTarget = slice;
            slice = getStateSlice(store.getState(), finalMapStateToTarget);
        }

        const boundActionCreators = finalMapDispatchToTarget(store.dispatch);

        return (target) => {

            invariant(
                _.isFunction(target) || _.isObject(target),
                "The target parameter passed to connect must be a Function or a object.",
            );

            updateTarget(target, slice, boundActionCreators);

            const unsubscribe = store.subscribe(() => {
                const nextSlice = getStateSlice(store.getState(), finalMapStateToTarget);
                if (!shallowEqual(slice, nextSlice)) {
                    slice = nextSlice;
                    updateTarget(target, slice, boundActionCreators);
                }
            });
            return unsubscribe;
        };

    };
}
