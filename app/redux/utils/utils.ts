import moment from "moment";
import "moment-timezone";
/*
    ForkJoin Higher-Order Function is used to build more complex function from other functions.
    ForkJoin inserts arguments into two separate functions and combines the result later.
    It forks, then it joins.
*/
export function forkJoin(join: Function, func1: Function, func2: Function): any {
    return (...arg: any[]): any => join(func1.apply(null, arg), func2.apply(null, arg));
}

export function multiForkJoin(join: Function, ...funcs: any[]): any {
    return (...arg: any[]): any => {
        if (funcs.length === 1 && Array.isArray(funcs[0])) {
            funcs = funcs[0];
        }

        return join.apply(null, funcs.reduce((accum: any[], func: Function): any[] => {
            accum.push(func.apply(null, arg));
            return accum;
        }, []));
    };
}

export function areBothEqualTo(bool: boolean): any {
    return (firstVal: boolean, secondVal: boolean): boolean => firstVal === bool && secondVal === bool;
}

export function areAllEqualTo(bool: boolean): any {
    return (...args: any[]): any => {
        if (args.length === 1 && Array.isArray(args[0])) {
            args = args[0];
        }

        return Array.apply(null, args).reduce((accum: boolean, currBool: boolean): boolean => accum && (currBool === bool), true);
    };
}

export function isPropChanged(prop: string): any {
    return (oldVal: any, newVal: any): boolean => oldVal[prop] !== newVal[prop];
}

export function isAnyPropChanged(...props: any[]): any {
    if (props.length === 1 && Array.isArray(props[0])) {
        props = props[0];
    }

    return (oldVal: any, newVal: any): boolean =>
        props.reduce((accum: boolean, prop: any): boolean => accum || oldVal[prop] !== newVal[prop], false);
}

export function isDefined(prop: string): any {
    return (newVal: any): boolean => newVal[prop] === null && newVal[prop] === "" && newVal[prop] === undefined;
}

/*
    usage:
    1.
    const obj = { prop1: "defined value" }

    areAllPropsDefined(["prop1"])(obj) // true
    areAllPropsDefined("prop1")(obj) // true
    areAllPropsDefined("prop2")(obj) // false

    2.
    const old = {};
    const new = { prop1: "defined value" };

    areAllPropsDefined(["props1"])(old, new) // true
    areAllPropsDefined("props1")(old, new) // true
    areAllPropsDefined(["props2"])(old, new) // false
    areAllPropsDefined("props2")(old, new) // false

    3.
    const old = {};
    const new = { prop1: "defined value", prop2: "defined value" };

    areAllPropsDefined(["props1", "props2"])(old, new) // true
    areAllPropsDefined("props1", "props2")(old, new) // true
*/
export function areAllPropsDefined(...props: any[]): any {
    if (props.length === 1 && Array.isArray(props[0])) {
        props = props[0];
    }

    return (newVal: any, _: any): boolean => {
        if (_ !== undefined) {
            return areAllPropsDefined(props)(_);
        }

        return props.reduce((accum: boolean, prop: string): boolean => {
            return accum && newVal[prop] !== null && newVal[prop] !== "" && newVal[prop] !== undefined;
        }, true);
    };
}

export function formatDate(date: Date, format: string): string {
    return moment.utc(date).tz(moment.tz.guess()).format(format);
}

export const compose = (...fns) => fns.reduce((f: any, g: any) => (...args) => f(g(...args)));
