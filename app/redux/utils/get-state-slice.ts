import invariant from "invariant";
import * as _ from "lodash";

export default function getStateSlice(state, mapStateToScope, shouldReturnObject = true) {
    const slice = mapStateToScope(state);

    if (shouldReturnObject) {
        invariant(
            _.isPlainObject(slice),
            "`mapStateToScope` must return an object. Instead received %s.",
            slice,
        );
    } else {
        invariant(
            _.isPlainObject(slice) || _.isFunction(slice),
            "`mapStateToScope` must return an object or a function. Instead received %s.",
            slice,
        );
    }

    return slice;
}
