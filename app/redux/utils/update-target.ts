import * as _ from "lodash";

export default function updateTarget(target: (state, dispatch) => any, StateSlice, dispatch) {
    if (_.isFunction(target)) {
        target(StateSlice, dispatch);
    } else {
        _.assign(target, StateSlice, dispatch);
    }
}
