import {
    Observable,
    Observer,
} from "rxjs";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";

export default function observableFromStore(store: IStore): Observable<IStoreState> {
    return Observable.create((observer: Observer<any>) =>
        store.subscribe(() => observer.next(store.getState())) );
}
