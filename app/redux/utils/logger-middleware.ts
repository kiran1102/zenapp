export default function logger(store) {
    return (next) => (action) => {
        console.log("dispatching", action);
        const result = next(action);
        console.log("next state", store.getState());
        return result;
    };
}
