import { IStoreState, IStore } from "interfaces/redux.interface";
import { createStore, applyMiddleware, compose, Store, StoreEnhancer, AnyAction, Middleware } from "redux";
import createConnect from "./utils/create-connector";
import { assign } from "lodash";
import rootReducer from "./reducers/index.reducer";
import logger from "./utils/logger-middleware";

interface IReduxWindow extends Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
}

declare let window: IReduxWindow;

export function ReduxStoreFactory(): IStore {
    const composeEnhancers: <Function>(fn: Function) => Function = process.env.NODE_ENV !== "production" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ maxAge: 200, latency: 0 })
    : compose;

    const middlewares: Middleware[] = [];
    const enhancer: StoreEnhancer<IStoreState> = composeEnhancers(applyMiddleware(...middlewares));
    const store: Store<IStoreState> = createStore(rootReducer, {} as IStoreState, enhancer);

    return assign(store, { connect: createConnect(store) });
}

ReduxStoreFactory.$inject = [];
