import { AnyAction } from "redux";
import {
    IRisk,
    IRiskDetails,
    IRiskPaneFilter,
} from "interfaces/risk.interface";
import {
    createSelector,
    Selector,
} from "reselect";
import _, { xor } from "lodash";
import {
    IStoreState,
    IStringToBooleanMap,
} from "interfaces/redux.interface";
import {
    IRiskMap,
    IRiskState,
    IRiskByIdSelector, IRiskByIdSelectorFactory,
    ISelectedRisksMapSelector,
    ISelectedRiskIdsSelector,
    IAllRiskIdsSelector,
    IAllRisksSelector,
    IAllRisksInOrderSelector,
    IPendingRiskModelIdsSelector,
    IPendingRiskModelsSelector,
    INonPendingRiskModelIdsSelector,
    INonPendingRiskModelsSelector,
} from "interfaces/risk-reducer.interface";

export const RiskActions = {
    RISKS_FETCHED: "RISKS_FETCHED",
    RISK_RELOADED: "RISK_RELOADED",
    ADD_RISK: "ADD_RISK",
    ADD_BLANK_RISK: "ADD_BLANK_RISK",
    ADD_TEMP_RISK: "ADD_TEMP_RISK",
    UPDATE_RISK: "UPDATE_RISK",
    UPDATE_RISK_WITH_TEMP_PROP: "UPDATE_RISK_WITH_TEMP_PROP",
    UPDATE_TEMP_RISK: "UPDATE_TEMP_RISK",
    RISK_SAVING: "RISK_SAVING",
    RISK_SAVED: "RISK_SAVED",
    DELETE_RISK: "DELETE_RISK",
    DELETE_TEMP_RISK: "DELETE_TEMP_RISK",
    SELECT_RISK: "SELECT_RISK",
    UNSELECT_RISK: "UNSELECT_RISK",
    SELECT_ALL_RISKS: "SELECT_ALL_RISKS",
    UNSELECT_ALL_RISKS: "UNSELECT_ALL_RISKS",
    SET_RISK_FILTER: "SET_RISK_FILTER",
};

const initialState: IRiskState = {
    allIds: [],
    byId: {},
    tempModel: {},
    selectedRiskIds: {},
    pendingRiskIds: [],
    riskFilter: null,
};

export default function riskReducer(state = initialState, action: AnyAction): IRiskState {
    switch (action.type) {
        case RiskActions.RISKS_FETCHED:
            const { allIds, byId } = action;
            return {
                ...state,
                allIds,
                byId,
                selectedRiskIds: {},
            };
        case RiskActions.RISK_RELOADED:
            const { riskModelToReload, identifier = "Id" } = action;
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [riskModelToReload[identifier]]: riskModelToReload,
                },
            };
        case RiskActions.ADD_RISK: {
            const { riskToAdd, key = "Id" } = action;
            return {
                ...state,
                allIds: _.concat(state.allIds, riskToAdd[key]),
                byId: {
                    ...state.byId,
                    [riskToAdd[key]]: riskToAdd,
                },
            };
        }
        case RiskActions.ADD_BLANK_RISK:
        case RiskActions.ADD_TEMP_RISK:
            const { tempRiskToAdd, idKey = "Id" } = action;
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    [tempRiskToAdd[idKey]]: tempRiskToAdd,
                },
            };
        case RiskActions.UPDATE_RISK:
        case RiskActions.UPDATE_RISK_WITH_TEMP_PROP:
            const { riskId, keyValueToMerge } = action;
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [riskId]: _.assign({}, state.byId[riskId], keyValueToMerge),
                },
            };
        case RiskActions.UPDATE_TEMP_RISK:
            const { riskIdTemp, keyValueToMergeTemp } = action;
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    [riskIdTemp]: _.assign({}, state.tempModel[riskIdTemp], keyValueToMergeTemp),
                },
            };
        case RiskActions.RISK_SAVING:
            const { riskIdSaving } = action;
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [riskIdSaving]: _.assign({}, state.byId[riskIdSaving], { isSaving: true }),
                },
            };
        case RiskActions.RISK_SAVED:
            const { riskIdSaved } = action;
            return {
                ...state,
                byId: {
                    ...state.byId,
                    [riskIdSaved]: _.assign({}, state.byId[riskIdSaved], { isSaving: false, isDirty: false }),
                },
            };
        case RiskActions.DELETE_RISK:
            const { riskToDelete } = action;
            return {
                ...state,
                allIds: _.filter(state.allIds, (id: string): boolean => id !== riskToDelete),
                byId: _.omit(state.byId, riskToDelete),
                tempModel: _.omit(state.tempModel, riskToDelete),
            };
        case RiskActions.DELETE_TEMP_RISK:
            const { tempRiskToDelete } = action;
            return {
                ...state,
                tempModel: _.omit(state.tempModel, tempRiskToDelete),
            };
        case RiskActions.SELECT_RISK:
            const { riskIdToSelect } = action;
            return {
                ...state,
                selectedRiskIds: {
                    ...state.selectedRiskIds,
                    [riskIdToSelect]: true,
                },
            };
        case RiskActions.UNSELECT_RISK:
            const { riskIdToUnSelect } = action;
            return {
                ...state,
                selectedRiskIds: _.omit(state.selectedRiskIds, riskIdToUnSelect),
            };
        case RiskActions.SELECT_ALL_RISKS:
            return {
                ...state,
                selectedRiskIds: _.keys(state.byId).reduce((collector, key: string): IStringToBooleanMap => {
                    collector[key] = true;
                    return collector;
                }, {}),
            };
        case RiskActions.UNSELECT_ALL_RISKS:
            return {
                ...state,
                selectedRiskIds: {},
            };
        case RiskActions.SET_RISK_FILTER:
            return {
                ...state,
                riskFilter: action.riskFilter,
            };
        default:
            return state;
    }
}

export const riskState: Selector<IStoreState, IRiskState> = (state: IStoreState): IRiskState => state.risks;
export const getRiskById: IRiskByIdSelectorFactory = (id: string): IRiskByIdSelector =>
    createSelector(riskState, (state: IRiskState): IRisk | IRiskDetails => state.byId[id]);
export const getTempRiskById: IRiskByIdSelectorFactory = (id: string): IRiskByIdSelector =>
    createSelector(riskState, (state: IRiskState): IRisk | IRiskDetails => state.tempModel[id]);
export const getSelectedRisksMap: ISelectedRisksMapSelector = createSelector(riskState, (state: IRiskState): IStringToBooleanMap => state.selectedRiskIds);
export const getSelectedRiskIds: ISelectedRiskIdsSelector = createSelector(getSelectedRisksMap, (idMap: IStringToBooleanMap): string[] => _.keys(idMap));
export const getAllRiskIds: IAllRiskIdsSelector = createSelector(riskState, (state: IRiskState): string[] => state.allIds);
export const getAllRisks: IAllRisksSelector = createSelector(riskState, (state: IRiskState): IRiskMap => state.byId);
export const getAllRisksInOrder: IAllRisksInOrderSelector = createSelector(getAllRisks, getAllRiskIds, (allModels: IRiskMap, allIds: string[]): Array<IRisk | IRiskDetails> =>
    allIds.map((id: string): IRisk | IRiskDetails => allModels[id]));
export const getPendingRiskModels: IPendingRiskModelsSelector = createSelector(getAllRisksInOrder, (allRisks: IRisk[]): IRisk[] =>
    allRisks.filter((riskModel: IRisk): boolean => riskModel.PendingAssessment));
export const getPendingRiskIds: IPendingRiskModelIdsSelector = createSelector(getPendingRiskModels, (pendingRisks: IRisk[]): string[] =>
    pendingRisks.map((risk: IRisk): string => risk.Id));
export const getNonPendingRiskModels: INonPendingRiskModelsSelector = createSelector(getAllRisksInOrder, (allRisks: IRisk[]): IRisk[] =>
    allRisks.filter((riskModel: IRisk): boolean => !riskModel.PendingAssessment));
export const getNonPendingRiskIds: INonPendingRiskModelIdsSelector = createSelector(getNonPendingRiskModels, (pendingRisks: IRisk[]): string[] =>
    pendingRisks.map((risk: IRisk): string => risk.Id));
export const getRiskFilter: Selector<IStoreState, IRiskPaneFilter> = (state: IStoreState): IRiskPaneFilter => state.risks.riskFilter;
