import { assign, cloneDeep, find } from "lodash";
// Redux
import { AnyAction } from "redux";
import { Selector, createSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IPreferenceCenterState,
    IPreferenceCenterSelector,
    IPreferenceCenterStatusSelector,
    IPreferenceCenterLanguageSelector,
    IPreferenceCenterLanguagesSelector,
    IPreferenceCenterSelectedPurposesSelector,
} from "interfaces/preference-center-reducer.interface";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Constants
import crConstants from "consentModule/shared/cr-constants";
import { IDropDownElement } from "crservice/consent-base.service";

export const PreferenceCenterActions = {
    SELECT_LANGUAGE: "SELECT_LANGUAGE",
    ADD_PREFERENCE_CENTER: "ADD_PREFERENCE_CENTER",
    UPDATE_PREFERENCE_CENTER: "UPDATE_PREFERENCE_CENTER",
    UPDATE_PREFERENCE_CENTER_PREVIEW: "UPDATE_PREFERENCE_CENTER_PREVIEW",
    FETCHING_PREFERENCE_CENTER: "FETCHING_PREFERENCE_CENTER",
    FETCHED_PREFERENCE_CENTER: "FETCHED_PREFERENCE_CENTER",
    SAVING_PREFERENCE_CENTER: "SAVING_PREFERENCE_CENTER",
    SAVED_PREFERENCE_CENTER: "SAVED_PREFERENCE_CENTER",
    DRAG_PURPOSES: "DRAG_PURPOSES",
    DROPPING_PURPOSES: "DROPPING_PURPOSES",
    DROPPED_PURPOSES: "DROPPED_PURPOSES",
    PREFERENCE_CENTER_ERROR: "PREFERENCE_CENTER_ERROR",
    FETCHING_LANGUAGES: "FETCHING_LANGUAGES",
    FETCHED_LANGUAGES: "FETCHED_LANGUAGES",
    RESET_STATE: "RESET_STATE",
    RESET_CHANGES: "RESET_CHANGES",
    TOGGLE_DISABLE_LANGUAGES: "TOGGLE_DISABLE_LANGUAGES",
};

const initialState: IPreferenceCenterState = {
    isLoaded: false,
    isLoading: false,
    isModified: false,
    isSaving: false,
    isDropping: false,
    isValid: true,
    isLoadingLanguages: false,
    isDisabledLanguages: false,
    selectedPurposes: [],
};

export function preferenceCenter(state: IPreferenceCenterState = initialState, action: AnyAction): IPreferenceCenterState {
    switch (action.type) {
        case PreferenceCenterActions.SELECT_LANGUAGE:
            const { language } = action;
            return { ...state, language };
        case PreferenceCenterActions.UPDATE_PREFERENCE_CENTER:
            const { updatedPreferenceCenter } = action;
            return {
                ...state,
                preferenceCenter: assign({}, state.preferenceCenter, updatedPreferenceCenter),
                isLoaded: true,
            };
        case PreferenceCenterActions.UPDATE_PREFERENCE_CENTER_PREVIEW:
            const { updatedPreferenceCenterPreview, isModified, isValid } = action;
            return {
                ...state,
                preferenceCenterPreview: assign({}, state.preferenceCenterPreview, updatedPreferenceCenterPreview),
                isModified,
                isValid,
            };
        case PreferenceCenterActions.FETCHING_PREFERENCE_CENTER:
            return {
                ...state,
                isLoading: true,
                isLoaded: false,
            };
        case PreferenceCenterActions.FETCHED_PREFERENCE_CENTER:
            return {
                ...state,
                isLoading: false,
                language: getDefaultLanguage(state.preferenceCenterPreview, state.allLanguages),
            };
        case PreferenceCenterActions.SAVING_PREFERENCE_CENTER:
            return {
                ...state,
                isSaving: true,
            };
        case PreferenceCenterActions.PREFERENCE_CENTER_ERROR:
            return {
                ...state,
                isSaving: false,
                isLoading: false,
                isDropping: false,
            };
        case PreferenceCenterActions.SAVED_PREFERENCE_CENTER:
            return {
                ...state,
                preferenceCenter: assign({}, state.preferenceCenterPreview),
                isSaving: false,
                isModified: false,
            };
        case PreferenceCenterActions.DRAG_PURPOSES:
            const { selectedPurposes } = action;
            return {
                ...state,
                selectedPurposes: cloneDeep(selectedPurposes),
            };
        case PreferenceCenterActions.DROPPING_PURPOSES:
            return {
                ...state,
                isDropping: true,
            };
        case PreferenceCenterActions.DROPPED_PURPOSES:
            return {
                ...state,
                isDropping: false,
                selectedPurposes: [],
            };
        case PreferenceCenterActions.FETCHING_LANGUAGES:
            return {
                ...state,
                isLoadingLanguages: true,
            };
        case PreferenceCenterActions.FETCHED_LANGUAGES:
            const { allLanguages } = action;
            return {
                ...state,
                isLoadingLanguages: false,
                allLanguages: cloneDeep(allLanguages),
                language: getDefaultLanguage(state.preferenceCenterPreview, allLanguages),
            };
        case PreferenceCenterActions.RESET_STATE:
            return cloneDeep(initialState);
        case PreferenceCenterActions.RESET_CHANGES:
            return {
                ...state,
                preferenceCenterPreview: cloneDeep(state.preferenceCenter),
                isModified: false,
                isValid: true,
            };
        case PreferenceCenterActions.TOGGLE_DISABLE_LANGUAGES:
            return {
                ...state,
                isDisabledLanguages: action.isDisabledLanguages,
            };
        default:
            return state;
    }
}

export function getDefaultLanguage(prefCenter: PreferenceCenter, languages: IGetAllLanguageResponse[]): IGetAllLanguageResponse {
    if (!prefCenter || !languages) {
        return null;
    }
    return find(languages, (lang: IGetAllLanguageResponse): boolean => lang.Code === prefCenter.Language);
}

export const getPreferenceCenterState: Selector<IStoreState, IPreferenceCenterState> = (state: IStoreState): IPreferenceCenterState => state.preferenceCenter;

export const getPreferenceCenterLoadedStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isLoaded);
export const getPreferenceCenterLoadingStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isLoading);
export const getPreferenceCenterModifiedStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isModified);
export const getPreferenceCenterSavingStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isSaving);
export const getPreferenceCenterDisabledLanguagesStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isDisabledLanguages);
export const getPreferenceCenterDroppingStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isDropping);
export const getPreferenceCenterLoadingLanguagesStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isLoadingLanguages);
export const getPreferenceCenterActionStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isDropping || preferenceCenterState.isLoading || preferenceCenterState.isSaving || preferenceCenterState.isLoadingLanguages);
export const getPreferenceCenterInactiveStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.preferenceCenterPreview && preferenceCenterState.preferenceCenterPreview.Status === crConstants.CRPreferenceCenterStatus.Inactive);
export const getPreferenceCenterValidStatus: IPreferenceCenterStatusSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): boolean => preferenceCenterState.isValid);

export const getPreferenceCenter: IPreferenceCenterSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): PreferenceCenter => cloneDeep(preferenceCenterState.preferenceCenter));
export const getPreferenceCenterPreview: IPreferenceCenterSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): PreferenceCenter => cloneDeep(preferenceCenterState.preferenceCenterPreview));
export const getPreferenceCenterLanguage: IPreferenceCenterLanguageSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): IGetAllLanguageResponse => cloneDeep(preferenceCenterState.language));
export const getPreferenceCenterSelectedPurposes: IPreferenceCenterSelectedPurposesSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): IDropDownElement[] => cloneDeep(preferenceCenterState.selectedPurposes));

export const getPreferenceCenterAllLanguages: IPreferenceCenterLanguagesSelector = createSelector(getPreferenceCenterState, (preferenceCenterState: IPreferenceCenterState): IGetAllLanguageResponse[] => preferenceCenterState.allLanguages);
