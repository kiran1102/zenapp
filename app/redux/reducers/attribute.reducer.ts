import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IAttributeDetailV2,
    IAttributeV2,
} from "interfaces/inventory.interface";
import {
    IAttributeManagerState,
    ICurrentAttributeSelector,
    IIsLoadingCurrentAttributeSelector,
    IAttributeListSelector,
    IIsLoadingAttributeListSelector,
} from "interfaces/attribute-reducer.interface";

export const AttributeActions = {
    ATTRIBUTES_SET_CURRENT: "ATTRIBUTES_SET_CURRENT",
    ATTRIBUTES_START_LOADING_CURRENT: "ATTRIBUTES_START_LOADING_CURRENT",
    ATTRIBUTES_STOP_LOADING_CURRENT: "ATTRIBUTES_START_LOADING_CURRENT",
    ATTRIBUTES_SET_LIST: "ATTRIBUTES_SET_LIST",
    ATTRIBUTES_START_LOADING_LIST: "ATTRIBUTES_START_LOADING_LIST",
    ATTRIBUTES_STOP_LOADING_LIST: "ATTRIBUTES_STOP_LOADING_LIST",
};

const initialState: IAttributeManagerState = {
    isLoadingCurrentAttribute: false,
    isLoadingAttributeList: false,
    currentAttribute: null,
    attributeList: [],
};

export function attributeManager(state = initialState, action: AnyAction): IAttributeManagerState {
    switch (action.type) {
        case AttributeActions.ATTRIBUTES_START_LOADING_CURRENT: {
            return {
                ...state,
                isLoadingCurrentAttribute: true,
            };
        }
        case AttributeActions.ATTRIBUTES_STOP_LOADING_CURRENT: {
            return {
                ...state,
                isLoadingCurrentAttribute: false,
            };
        }
        case AttributeActions.ATTRIBUTES_SET_CURRENT: {
            const { currentAttribute } = action;
            return {
                ...state,
                currentAttribute,
                isLoadingCurrentAttribute: false,
            };
        }
        case AttributeActions.ATTRIBUTES_START_LOADING_LIST: {
            return {
                ...state,
                isLoadingAttributeList: true,
            };
        }
        case AttributeActions.ATTRIBUTES_STOP_LOADING_CURRENT: {
            return {
                ...state,
                isLoadingAttributeList: false,
            };
        }
        case AttributeActions.ATTRIBUTES_SET_LIST: {
            const { attributeList } = action;
            return {
                ...state,
                attributeList,
                isLoadingAttributeList: false,
            };
        }
        default:
            return state;
    }
}

export const attributeState: Selector<IStoreState, IAttributeManagerState> = (state: IStoreState): IAttributeManagerState => state.attributeManager;
export const getCurrentAttribute: ICurrentAttributeSelector = createSelector(attributeState, (state: IAttributeManagerState): IAttributeDetailV2 => state.currentAttribute);
export const getAttributeList: IAttributeListSelector = createSelector(attributeState, (state: IAttributeManagerState): IAttributeV2[] => state.attributeList);
export const getIsLoadingCurrentAttribute: IIsLoadingCurrentAttributeSelector = createSelector(attributeState, (state: IAttributeManagerState): boolean => state.isLoadingCurrentAttribute);
export const getIsLoadingAttributeList: IIsLoadingAttributeListSelector = createSelector(attributeState, (state: IAttributeManagerState): boolean => state.isLoadingAttributeList);
