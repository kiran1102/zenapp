import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IModalState,
    IModalStateSelector,
    IModalStatusSelector,
    IModalNameSelector,
    IModalDataSelector,
} from "interfaces/modal-reducer.interface";

export const ModalActions = {
    OPEN_MODAL: "OPEN_MODAL",
    CLOSE_MODAL: "CLOSE_MODAL",
    SET_MODAL_DATA: "SET_MODAL_DATA",
    UPDATE_MODAL_DATA: "UPDATE_MODAL_DATA",
    CLEAR_MODAL_DATA: "CLEAR_MODAL_DATA",
};

const initialState: IModalState = {
    modalStatus: "closed",
    modalName: "",
    modalData: null,
};

export default function modalReducer(state = initialState, action: AnyAction): IModalState {
    switch (action.type) {
        case ModalActions.OPEN_MODAL:
            const { modalName } = action;
            return {
                ...state,
                modalStatus: "opened",
                modalName,
            };
        case ModalActions.CLOSE_MODAL:
            return {
                ...state,
                modalStatus: "closed",
                modalName: "",
            };
        case ModalActions.SET_MODAL_DATA:
            const { modalData } = action;
            return {
                ...state,
                modalData,
            };
        case ModalActions.UPDATE_MODAL_DATA:
            const { modalDataToMerge } = action;
            return {
                ...state,
                modalData: {
                    ...state.modalData,
                    ...modalDataToMerge,
                },
            };
        case ModalActions.CLEAR_MODAL_DATA:
            return {
                ...state,
                modalData: null,
            };
        default:
            return state;
    }
}

export const modalState: Selector<IStoreState, IModalState> = (state: IStoreState): IModalState => state.modals;
export const getModalStatus: IModalStatusSelector = createSelector(modalState, (state: IModalState): string => state.modalStatus);
export const getModalName: IModalNameSelector = createSelector(modalState, (state: IModalState): string => state.modalName);
export const getModalData: IModalDataSelector = createSelector(modalState, (state: IModalState): any => state.modalData);
