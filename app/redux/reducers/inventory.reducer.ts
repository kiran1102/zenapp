// 3rd Party
import { filter, forIn, includes } from "lodash";
import { createSelector, Selector } from "reselect";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IFilterTree } from "interfaces/filter.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";
import {
    IRecordMap,
    IAttributeMap,
    IInventoryState,
    IRecordByIdSelector,
    IRecordByIdSelectorFactory,
    IInventoryListFetchStatusSelector,
    IAllRecordIdsSelector,
    IAllRecordMapSelector,
    IAllAttributeMapSelector,
    IRecordListInOrderSelector,
    IAttributeListInOrderSelector,
    IInventoryPaginationMetadata,
    IInventoryStateInvalidSelector,
    ISavingRecord,
    IFetchingRecord,
    IExportingInventory,
    IFetchingRecordDetails,
    IRecordEditEnabled,
    IGetInitialRecord,
    IGetCurrentRecord,
    IRequiredFieldsValid,
    IStatusMapSelector,
    ISelectedRecordSelector,
    IInventorySearchTermSelector,
    IInventoryIdSelector,
    IInventoryPageSelector,
    IAttributeDrawerState,
    IFilterDrawerState,
    IFilterDataSelector,
    IAllAssessmentIdsSelector,
    IAllAssessmentsMapSelector,
    IAssessmentListSelector,
    IIsLoadingAssessmentsSelector,
    IIsShowingFilteredResultsSelector,
    IIsFilteringInventorySelector,
    IIsLoadingRisksSelector,
    IIsLoadingRelatedSelector,
    IInventoryRelatedSelector,
    IGetCurrentRecordOrgId,
    IGetUsersValid,
    IGetAllUsersValid,
} from "interfaces/inventory-reducer.interface";
import {
    IInventoryRecord,
    IAttribute,
    IAssessmentStatusMap,
    IInventoryAssessment,
    IRelatedTable,
} from "interfaces/inventory.interface";
import { IAuditHistoryItem } from "interfaces/audit-history.interface";

// Enum + Const
import { InventoryTableIds } from "enums/inventory.enum";

export const InventoryActions = {
    FETCHING_INVENTORY_LIST: "FETCHING_INVENTORY_LIST",
    FETCHING_INVENTORY_PAGE: "FETCHING_INVENTORY_PAGE",
    INVENTORY_LIST_FETCHED: "INVENTORY_LIST_FETCHED",
    RECEIVE_INVENTORY_LIST: "RECEIVE_INVENTORY_LIST",
    SET_RECORD_DEFAULT_STATE: "SET_RECORD_DEFAULT_STATE",
    FETCHING_RECORD: "FETCHING_RECORD",
    RECORD_FETCHED: "RECORD_FETCHED",
    RECORD_EDITED: "RECORD_EDITED",
    RECORD_SAVING: "RECORD_SAVING",
    RECORD_SAVED: "RECORD_SAVED",
    FETCHING_RECORD_DETAILS: "FETCHING_RECORD_DETAILS",
    RECORD_DETAILS_FETCHED: "RECORD_DETAILS_FETCHED",
    EXPORTING_INVENTORY: "EXPORTING_INVENTORY",
    INVENTORY_EXPORTED: "INVENTORY_EXPORTED",
    VALIDATE_RECORD_STATE: "VALIDATE_RECORD_STATE",
    INVALIDATE_RECORD_STATE: "INVALIDATE_RECORD_STATE",
    RECORD_EDIT_ENABLED: "RECORD_EDIT_ENABLED",
    RECORD_EDIT_DISABLED: "RECORD_EDIT_DISABLED",
    RECORD_EDIT_CANCELLED: "RECORD_EDIT_CANCELLED",
    REQUIRED_FIELDS_POPULATED: "REQUIRED_FIELDS_POPULATED",
    REQUIRED_FIELDS_NOT_POPULATED: "REQUIRED_FIELDS_NOT_POPULATED",
    SET_CURRENT_RECORD: "SET_CURRENT_RECORD",
    RECEIVE_INVENTORY_PAGE: "RECEIVE_INVENTORY_PAGE",
    SET_RELATED_INVENTORY_PAGE: "SET_RELATED_INVENTORY_PAGE",
    TOGGLE_RECORD_SELECTION: "TOGGLE_RECORD_SELECTION",
    TOGGLE_ALL_RECORDS_SELECTION: "TOGGLE_ALL_RECORDS_SELECTION",
    UPDATED_STATUSES: "UPDATED_STATUSES",
    SEARCH_INVENTORY: "SEARCH_INVENTORY",
    SET_INVENTORY_DEPENDENCIES: "SET_INVENTORY_DEPENDENCIES",
    SET_SEARCH_TERM: "SET_SEARCH_TERM",
    SET_INVENTORY_ID: "SET_INVENTORY_ID",
    SET_PAGE_NUMBER: "SET_PAGE_NUMBER",
    RECORD_NOT_SAVING: "RECORD_NOT_SAVING",
    OPEN_ATTRIBUTE_DRAWER: "OPEN_ATTRIBUTE_DRAWER",
    CLOSE_ATTRIBUTE_DRAWER: "CLOSE_ATTRIBUTE_DRAWER",
    OPEN_FILTER_DRAWER: "OPEN_FILTER_DRAWER",
    CLOSE_FILTER_DRAWER: "CLOSE_FILTER_DRAWER",
    CLOSE_ALL_DRAWERS: "CLOSE_ALL_DRAWERS",
    SET_FILTER_DATA: "SET_FILTER_DATA",
    SET_DEFAULT_FILTER_DATA: "SET_DEFAULT_FILTER_DATA",
    FETCHING_INVENTORY_ASSESSMENT_LIST: "FETCHING_INVENTORY_ASSESSMENT_LIST",
    SET_INVENTORY_ASSESSMENT_LIST: "SET_INVENTORY_ASSESSMENT_LIST",
    FETCHING_ALL_INVENTORY_RECORD_DETAILS: "FETCHING_ALL_INVENTORY_RECORD_DETAILS",
    UPDATE_INVENTORY_RECORD_MODEL: "UPDATE_INVENTORY_RECORD_MODEL",
    SHOWING_FILTERED_RESULTS: "SHOWING_FILTERED_RESULTS",
    FILTERING_INVENTORY_LIST: "FILTERING_INVENTORY_LIST",
    INVENTORY_LIST_FILTERED: "INVENTORY_LIST_FILTERED",
    FETCHING_INVENTORY_RISK_LIST: "FETCHING_INVENTORY_RISK_LIST",
    FETCHED_INVENTORY_RISK_LIST: "FETCHED_INVENTORY_RISK_LIST",
    FETCHING_INVENTORY_RELATED_LIST: "FETCHING_INVENTORY_RELATED_LIST",
    FETCHED_INVENTORY_RELATED_LIST: "FETCHED_INVENTORY_RELATED_LIST",
    SET_RELATED_INVENTORY: "SET_RELATED_INVENTORY",
    SET_DEFAULT_RELATED_INVENTORIES: "SET_DEFAULT_RELATED_INVENTORIES",
    SET_INVENTORY_RELATED_LIST: "SET_INVENTORY_RELATED_LIST",
    SET_USER_VALIDITY: "SET_USER_VALIDITY",
};

const initialState: IInventoryState = {
    allRecordIds: [],
    allAttributeIds: [],
    byRecordId: null,
    byAttributeId: null,
    selectedInventoryIds: null,
    isFetchingInventoryList: false,
    paginationMetadata: null,
    initialRecordModel: null,
    currentRecordModel: null,
    isSavingRecord: false,
    isFetchingRecord: false,
    isFetchingRecordDetails: false,
    isExportingInventory: false,
    recordEdited: false,
    didRecordInvalidate: false,
    didInventoryInvalidate: false,
    isRecordEditEnabled: false,
    requiredFieldsValid: true,
    statusMap: null,
    searchTerm: "",
    inventoryId: null,
    currentPageNumber: 0,
    relatedInventoryCurrentPageNumber: 0,
    relatedInventoryCurrentPageSize: null,
    isAttributeDrawerOpen: false,
    isFilterDrawerOpen: false,
    filterData: null,
    byAssessmentId: null,
    allAssessmentIds: [],
    isFetchingAssessmentList: false,
    isShowingFilteredResults: false,
    isFilteringInventory: false,
    isFetchingRiskList: false,
    isFetchingRelatedList: false,
    relatedInventories: {
        [InventoryTableIds.Elements]: null,
        [InventoryTableIds.Assets]: null,
        [InventoryTableIds.Processes]: null,
        [InventoryTableIds.Vendors]: null,
    },
    currentRecordOrgId: "",
    usersValid: {},
};

export default function inventoryReducer(state = initialState, action) {
    switch (action.type) {
        case InventoryActions.FETCHING_INVENTORY_LIST:
            return { ...state, isFetchingInventoryList: true };
        case InventoryActions.INVENTORY_LIST_FETCHED:
            return { ...state, isFetchingInventoryList: false };
        case InventoryActions.RECEIVE_INVENTORY_LIST: {
            const { allRecordIds, allAttributeIds, byRecordId, byAttributeId, paginationMetadata, statusMap } = action;
            return {
                ...state,
                allRecordIds,
                allAttributeIds,
                byRecordId,
                byAttributeId,
                paginationMetadata,
                statusMap,
            };
        }
        case InventoryActions.SHOWING_FILTERED_RESULTS:
            const { showingFilteredResults } = action;
            return { ...state, isShowingFilteredResults: showingFilteredResults };
        case InventoryActions.SET_FILTER_DATA:
            const { filters } = action;
            return { ...state, filterData: filters };
        case InventoryActions.SET_DEFAULT_FILTER_DATA:
            return { ...state, filterData: null };
        case InventoryActions.OPEN_ATTRIBUTE_DRAWER:
            return { ...state, isAttributeDrawerOpen: true };
        case InventoryActions.CLOSE_ATTRIBUTE_DRAWER:
            return { ...state, isAttributeDrawerOpen: false };
        case InventoryActions.OPEN_FILTER_DRAWER:
            return { ...state, isFilterDrawerOpen: true };
        case InventoryActions.CLOSE_FILTER_DRAWER:
            return { ...state, isFilterDrawerOpen: false };
        case InventoryActions.CLOSE_ALL_DRAWERS:
            return { ...state, isFilterDrawerOpen: false, isAttributeDrawerOpen: false };
        case InventoryActions.FETCHING_RECORD:
            return { ...state, isFetchingRecord: true };
        case InventoryActions.RECORD_FETCHED: {
            const { currentRecordModel, allAttributeIds, byAttributeId, statusMap, currentRecordOrgId } = action;
            return {
                ...state,
                isFetchingRecord:
                false,
                currentRecordModel,
                allAttributeIds,
                byAttributeId,
                statusMap,
                currentRecordOrgId,
            };
        }
        case InventoryActions.RECORD_SAVING:
            return { ...state, isSavingRecord: true };
        case InventoryActions.RECORD_NOT_SAVING:
            return { ...state, isSavingRecord: false };
        case InventoryActions.RECORD_SAVED:
            return { ...state, isSavingRecord: false, recordEdited: false, initialRecordModel: null };
        case InventoryActions.EXPORTING_INVENTORY:
            return { ...state, isExportingInventory: true };
        case InventoryActions.INVENTORY_EXPORTED:
            return { ...state, isExportingInventory: false };
        case InventoryActions.VALIDATE_RECORD_STATE:
            return { ...state, didRecordInvalidate: false };
        case InventoryActions.INVALIDATE_RECORD_STATE:
            return { ...state, didRecordInvalidate: true };
        case InventoryActions.FETCHING_RECORD_DETAILS:
            return { ...state, isFetchingRecordDetails: true };
        case InventoryActions.RECORD_DETAILS_FETCHED:
            return { ...state, isFetchingRecordDetails: false };
        case InventoryActions.FILTERING_INVENTORY_LIST:
            return { ...state, isFilteringInventory: true };
        case InventoryActions.INVENTORY_LIST_FILTERED:
            return { ...state, isFilteringInventory: false };
        case InventoryActions.SET_USER_VALIDITY:
            const { attributeId, isValid } = action;
            return {
                ...state,
                usersValid: {
                    ...state.usersValid,
                    [attributeId]: isValid,
                },
            };
        case InventoryActions.SET_RECORD_DEFAULT_STATE:
            return {
                ...state,
                isRecordEditEnabled: false,
                requiredFieldsValid: true,
                initialRecordModel: null,
                currentRecordModel: null,
                recordEdited: false,
                usersValid: {},
            };
        case InventoryActions.RECORD_EDITED: {
            const { updatedRecordModel, byAttributeId, updatedOrgId } = action;
            return {
                ...state,
                recordEdited: true,
                currentRecordModel: updatedRecordModel,
                byAttributeId,
                currentRecordOrgId: updatedOrgId || state.currentRecordOrgId,
            };
        }
        case InventoryActions.RECORD_EDIT_ENABLED:
            const { initialRecordModel } = action;
            return { ...state, isRecordEditEnabled: true, initialRecordModel };
        case InventoryActions.RECORD_EDIT_DISABLED:
            return { ...state, isRecordEditEnabled: false, recordEdited: false };
        case InventoryActions.RECORD_EDIT_CANCELLED: {
            const { savedRecord, updatedOrgId } = action;
            return {
                ...state,
                isRecordEditEnabled: false,
                recordEdited: false,
                currentRecordModel: savedRecord,
                requiredFieldsValid: true,
                initialRecordModel: null,
                currentRecordOrgId: updatedOrgId,
            };
        }
        case InventoryActions.REQUIRED_FIELDS_POPULATED:
            return { ...state, requiredFieldsValid: true };
        case InventoryActions.REQUIRED_FIELDS_NOT_POPULATED:
            return { ...state, requiredFieldsValid: false };
        case InventoryActions.SET_CURRENT_RECORD: {
            const { currentRecordModel } = action;
            return {
                ...state,
                currentRecordModel,
            };
        }
        case InventoryActions.SET_RELATED_INVENTORY_PAGE: {
            const { page, size } = action;
            return {
                ...state,
                relatedInventoryCurrentPageNumber: page,
                relatedInventoryCurrentPageSize: size,
            };
        }
        case InventoryActions.RECEIVE_INVENTORY_PAGE: {
            const { allRecordIds, byRecordId, paginationMetadata, searchTerm } = action;
            return {
                ...state,
                allRecordIds,
                byRecordId,
                paginationMetadata,
                searchTerm,
            };
        }
        case InventoryActions.TOGGLE_RECORD_SELECTION: {
            const { record, isSelected } = action;
            record.isSelected = isSelected;
            return {
                ...state,
                byRecordId: {
                    ...state.byRecordId,
                    [record.id]: record,
                },
            };
        }
        case InventoryActions.TOGGLE_ALL_RECORDS_SELECTION: {
            const { isSelected } = action;
            forIn(state.byRecordId, (record: IInventoryRecord): void => {
                record.isSelected = (!record.canEdit || record.assessmentOutstanding) ? record.isSelected : isSelected;
            });
            return {
                ...state,
                byRecordId: { ...state.byRecordId },
            };
        }
        case InventoryActions.UPDATED_STATUSES: {
            const { byRecordId, statusMap } = action;
            return {
                ...state,
                byRecordId,
                statusMap,
            };
        }
        case InventoryActions.SET_SEARCH_TERM: {
            const { searchTerm } = action;
            return { ...state, searchTerm, currentPageNumber: 0 };
        }
        case InventoryActions.SET_PAGE_NUMBER: {
            const { page } = action;
            return { ...state, currentPageNumber: page };
        }
        case InventoryActions.SET_INVENTORY_ID: {
            const { inventoryId } = action;
            return { ...state, inventoryId, searchTerm: "", currentPageNumber: 0 };
        }
        case InventoryActions.SET_INVENTORY_DEPENDENCIES: {
            const { statusMap } = action;
            return { ...state, statusMap };
        }
        case InventoryActions.FETCHING_INVENTORY_ASSESSMENT_LIST: {
            return { ...state, isFetchingAssessmentList: true };
        }
        case InventoryActions.SET_INVENTORY_ASSESSMENT_LIST: {
            const { byAssessmentId, allAssessmentIds } = action;
            return {
                ...state,
                isFetchingAssessmentList: false,
                byAssessmentId,
                allAssessmentIds,
            };
        }
        case InventoryActions.FETCHING_ALL_INVENTORY_RECORD_DETAILS: {
            return {
                ...state,
                isFetchingRecord: true,
                isFetchingRelatedList: true,
            };
        }
        case InventoryActions.UPDATE_INVENTORY_RECORD_MODEL: {
            const { record } = action;
            return {
                ...state,
                byRecordId: {
                    ...state.byRecordId,
                    [record.id]: record,
                },
            };
        }
        case InventoryActions.FETCHING_INVENTORY_RISK_LIST: {
            return {
                ...state,
                isFetchingRiskList: true,
            };
        }
        case InventoryActions.FETCHED_INVENTORY_RISK_LIST: {
            return {
                ...state,
                isFetchingRiskList: false,
            };
        }
        case InventoryActions.FETCHING_INVENTORY_RELATED_LIST: {
            return {
                ...state,
                isFetchingRelatedList: true,
            };
        }
        case InventoryActions.FETCHED_INVENTORY_RELATED_LIST: {
            return {
                ...state,
                isFetchingRelatedList: false,
            };
        }
        case InventoryActions.SET_RELATED_INVENTORY: {
            const { relatedInventoryData, inventoryType } = action;
            return {
                ...state,
                relatedInventories: {
                    ...state.relatedInventories,
                    [inventoryType]: relatedInventoryData,
                },
            };
        }
        case InventoryActions.SET_DEFAULT_RELATED_INVENTORIES: {
            return {
                ...state,
                relatedInventories: {
                    [InventoryTableIds.Elements]: null,
                    [InventoryTableIds.Assets]: null,
                    [InventoryTableIds.Processes]: null,
                    [InventoryTableIds.Vendors]: null,
                },
            };
        }
        default:
            return state;
    }
}

export const inventoryState: Selector<IStoreState, IInventoryState> = (state: IStoreState): IInventoryState => state.inventory;
export const getRecordById: IRecordByIdSelectorFactory = (id: string): IRecordByIdSelector =>
    createSelector(inventoryState, (selectorInventoryState: IInventoryState): IInventoryRecord => selectorInventoryState.byRecordId ? selectorInventoryState.byRecordId[id] : null);
export const isFetchingInventoryList: IInventoryListFetchStatusSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingInventoryList);
export const getAllRecordIds: IAllRecordIdsSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): string[] => selectorInventoryState.allRecordIds);
export const getAllRecordsMap: IAllRecordMapSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IRecordMap => selectorInventoryState.byRecordId);
export const getAllAttributeIds: IAllRecordIdsSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): string[] => selectorInventoryState.allAttributeIds);
export const getAttributeMap: IAllAttributeMapSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IAttributeMap => selectorInventoryState.byAttributeId);
export const getRecordList: IRecordListInOrderSelector = createSelector(getAllRecordsMap, getAllRecordIds, (allModels: IRecordMap, ids: string[]): IInventoryRecord[] =>
    ids.map((id: string): IInventoryRecord => allModels[id]));
export const getAttributeList: IAttributeListInOrderSelector = createSelector(getAttributeMap, getAllAttributeIds, (allModels: IAttributeMap, ids: string[]): IAttribute[] =>
    ids.map((id: string): IAttribute => allModels[id]));
export const getInventoryListPaginationMetadata: IInventoryPaginationMetadata = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IPageable => selectorInventoryState.paginationMetadata);
export const isInventoryListStateInvalidated: IInventoryStateInvalidSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.didRecordInvalidate);
export const isSavingRecord: ISavingRecord = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isSavingRecord);
export const isFetchingRecord: IFetchingRecord = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingRecord);
export const isFetchingRecordDetails: IFetchingRecordDetails = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingRecordDetails);
export const recordEdited: IFetchingRecord = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.recordEdited);
export const recordEditEnabled: IRecordEditEnabled = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isRecordEditEnabled);
export const isExportingInventory: IExportingInventory = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isExportingInventory);
export const getInitialRecord: IGetInitialRecord = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IInventoryRecord => selectorInventoryState.initialRecordModel);
export const getCurrentRecord: IGetCurrentRecord = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IInventoryRecord => selectorInventoryState.currentRecordModel);
export const requiredFieldsValid: IRequiredFieldsValid = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.requiredFieldsValid);
export const getStatusMap: IStatusMapSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IAssessmentStatusMap => selectorInventoryState.statusMap);
export const getSelectedRecords: ISelectedRecordSelector = createSelector(getRecordList, (recordList: IInventoryRecord[]): IInventoryRecord[] =>
    filter(recordList, (record: IInventoryRecord): boolean => record.isSelected));
export const getSearchTerm: IInventorySearchTermSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): string => selectorInventoryState.searchTerm);
export const getInventoryId: IInventoryIdSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): number => selectorInventoryState.inventoryId);
export const getPageNumber: IInventoryPageSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): number => selectorInventoryState.currentPageNumber);
export const getRelatedInventoryPageNumber: IInventoryPageSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): number => selectorInventoryState.relatedInventoryCurrentPageNumber);
export const getRelatedInventoryPageSize: IInventoryPageSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): number => selectorInventoryState.relatedInventoryCurrentPageSize);
export const getAttributeDrawerState: IAttributeDrawerState = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isAttributeDrawerOpen);
export const getFilterDrawerState: IFilterDrawerState = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFilterDrawerOpen);
export const getFilterData: IFilterDataSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IFilterTree => selectorInventoryState.filterData);
export const getAllAssessmentIds: IAllAssessmentIdsSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): string[] => selectorInventoryState.allAssessmentIds);
export const getAllAssessmentsMap: IAllAssessmentsMapSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IStringMap<IInventoryAssessment> => selectorInventoryState.byAssessmentId);
export const getAssessmentList: IAssessmentListSelector = createSelector(getAllAssessmentsMap, getAllAssessmentIds, (allModels: IStringMap<IInventoryAssessment>, ids: string[]): IInventoryAssessment[] =>
    ids.map((id: string): IInventoryAssessment => allModels[id]));
export const getIsLoadingAssessments: IIsLoadingAssessmentsSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingAssessmentList);
export const isShowingFilteredResults: IIsShowingFilteredResultsSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isShowingFilteredResults);
export const isFilteringInventory: IIsFilteringInventorySelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFilteringInventory);
export const getIsLoadingRisks: IIsLoadingRisksSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingRiskList);
export const getIsLoadingRelated: IIsLoadingRelatedSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): boolean => selectorInventoryState.isFetchingRelatedList);
export const getRelatedInventories: IInventoryRelatedSelector = createSelector(inventoryState, (selectorInventoryState: IInventoryState): INumberMap<IRelatedTable> => selectorInventoryState.relatedInventories);
export const getCurrentRecordOrgId: IGetCurrentRecordOrgId = createSelector(inventoryState, (selectorInventoryState: IInventoryState): string => selectorInventoryState.currentRecordOrgId);
export const getUsersValidMap: IGetUsersValid = createSelector(inventoryState, (selectorInventoryState: IInventoryState): IStringMap<boolean> => selectorInventoryState.usersValid);
export const getAllUsersValid: IGetAllUsersValid = createSelector(getUsersValidMap, (usersValidMap: IStringMap<boolean>): boolean => !includes(usersValidMap, false));
