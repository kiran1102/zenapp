import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector } from "reselect";

import { IStringMap } from "interfaces/generic.interface";
import { IStoreState } from "interfaces/redux.interface";
import {
    IConfigurationSelector,
    IConfigurationSelectorFactory,
    IConfigurationState,
    IConfigurationStateSelector,
} from "interfaces/configuration-reducer.interface";

export const ConfigurationActions = {
    SET_CONFIGURATION: "SET_CONFIGURATION",
};

const initialState: IConfigurationState = {
    configurations: {},
};

export function configuration(state = initialState, action: AnyAction): IConfigurationState {
    switch (action.type) {
        case ConfigurationActions.SET_CONFIGURATION:
            return {
                ...state,
                configurations: {
                    ...state.configurations,
                    [action.payload.type]: action.payload.config,
                },
            };
        default:
            return state;
    }
}

export const confugurationsState: Selector<IStoreState, IConfigurationState> = (state: IStoreState): IConfigurationState => state.configuration;
export const getConfigurationState: IConfigurationStateSelector = createSelector(confugurationsState, (state: IConfigurationState): IStringMap<any> => state.configurations);
export const getConfigurationByType: IConfigurationSelectorFactory = (type: string): IConfigurationSelector => createSelector(confugurationsState, (state: IConfigurationState): IStringMap<any> => state.configurations[type]);
