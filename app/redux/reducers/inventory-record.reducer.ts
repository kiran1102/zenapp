// Redux
import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector } from "reselect";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IInventoryRecordV2,
    IAttributeOptionV2,
    IAttributeDetailV2,
} from "interfaces/inventory.interface";
import {
    IInventoryRecordState,
    IRecordSelector,
    ISchemaSelector,
    IDisplayValuesSelector,
    IIsLoadingRecordSelector,
    IRecordChangesSelector,
    IRequiredHasValuesSelector,
    IHasChangesSelector,
    IHasErrorsSelector,
    IEditingRecordSelector,
    ICurrentNameSelector,
    IAttributeMapSelector,
    IInventoryIdSelector,
    IInventoryOrgGroupSelector,
} from "interfaces/inventory-record-reducer.interface";

// constants
import { AttributeFieldNames } from "constants/inventory.constant";

export const RecordActions = {
    SET_INVENTORY_RECORD: "SET_INVENTORY_RECORD",
    START_LOADING_INVENTORY_RECORD: "START_LOADING_INVENTORY_RECORD",
    START_LOADING_SECONDARY_INVENTORY_RECORD: "START_LOADING_SECONDARY_INVENTORY_RECORD",
    FINISH_LOADING_INVENTORY_RECORD: "FINISH_LOADING_INVENTORY_RECORD",
    SET_INVENTORY_RECORD_AND_SCHEMA: "SET_INVENTORY_RECORD_AND_SCHEMA",
    SET_SECONDARY_INVENTORY_RECORD_AND_SCHEMA: "SET_SECONDARY_INVENTORY_RECORD_AND_SCHEMA",
    SET_INVENTORY_RECORD_CHANGE: "SET_INVENTORY_RECORD_CHANGE",
    SET_INVENTORY_RECORD_ERROR: "SET_INVENTORY_RECORD_ERROR",
    CLEAR_INVENTORY_RECORD_CHANGES: "CLEAR_INVENTORY_RECORD_CHANGES",
    TOGGLE_INVENTORY_RECORD_EDITING: "TOGGLE_INVENTORY_RECORD_EDITING",
    SAVE_INVENTORY_RECORD_CHANGES: "SAVE_INVENTORY_RECORD_CHANGES",
    SET_SCHEMA: "SET_SCHEMA",
};

const initialState: IInventoryRecordState = {
    record: {},
    isLoadingRecord: false,
    isLoadingSecondaryRecord: false,
    displayValues: {},
    schema: [],
    changes: {},
    errors: {},
    isEditing: false,
    inventoryId: null,
};

export function inventoryRecord(state = initialState, action: AnyAction): IInventoryRecordState {
    switch (action.type) {
        case RecordActions.START_LOADING_INVENTORY_RECORD: {
            return {
                ...state,
                isLoadingRecord: true,
            };
        }
        case RecordActions.START_LOADING_SECONDARY_INVENTORY_RECORD: {
            return {
                ...state,
                isLoadingSecondaryRecord: true,
            };
        }
        case RecordActions.FINISH_LOADING_INVENTORY_RECORD: {
            const { inventoryId, isEditing } = action;
            return {
                ...state,
                isLoadingRecord: false,
                inventoryId,
                isEditing,
            };
        }
        case RecordActions.SET_INVENTORY_RECORD_AND_SCHEMA: {
            const { record, schema, displayValues, isEditing, inventoryId } = action;
            return {
                ...state,
                record,
                displayValues,
                schema: schema || state.schema,
                isLoadingRecord: false,
                changes: {},
                errors: {},
                isEditing,
                inventoryId,
            };
        }
        case RecordActions.SET_SECONDARY_INVENTORY_RECORD_AND_SCHEMA: {
            const { record, schema, displayValues, isEditing, inventoryId } = action;
            return {
                ...state,
                record,
                displayValues,
                schema: schema || state.schema,
                isLoadingSecondaryRecord: false,
                changes: {},
                errors: {},
                isEditing,
                inventoryId,
            };
        }
        case RecordActions.SET_INVENTORY_RECORD: {
            const { record, displayValues, isEditing } = action;
            return {
                ...state,
                record,
                displayValues,
                changes: {},
                errors: {},
                isLoadingRecord: false,
                isEditing,
            };
        }
        case RecordActions.SET_SCHEMA: {
            const { schema, inventoryId } = action;
            return {
                ...state,
                schema,
                inventoryId,
            };
        }
        case RecordActions.SET_INVENTORY_RECORD_CHANGE: {
            const { fieldName, change, hasError, displayValue } = action;
            return {
                ...state,
                changes: {
                    ...state.changes,
                    [fieldName]: change,
                },
                displayValues: {
                    ...state.displayValues,
                    [fieldName]: displayValue,
                },
                errors: {
                    ...state.errors,
                    [fieldName]: hasError,
                },
            };
        }
        case RecordActions.SET_INVENTORY_RECORD_ERROR: {
            const { fieldName, hasError } = action;
            return {
                ...state,
                errors: {
                    ...state.errors,
                    [fieldName]: hasError,
                },
            };
        }
        case RecordActions.CLEAR_INVENTORY_RECORD_CHANGES: {
            const { displayValues } = action;
            return {
                ...state,
                displayValues,
                changes: {},
            };
        }
        case RecordActions.TOGGLE_INVENTORY_RECORD_EDITING: {
            const { isEditing, displayValues } = action;
            return {
                ...state,
                isEditing,
                displayValues: !isEditing && displayValues ? displayValues : state.displayValues,
                changes: isEditing ? state.changes : {},
            };
        }
        case RecordActions.SAVE_INVENTORY_RECORD_CHANGES: {
            const { isEditing } = action;
            return {
                ...state,
                record: { ...state.record, ...state.changes },
                changes: {},
                errors: {},
                isLoadingRecord: false,
                isEditing,
            };
        }
        default:
            return state;
    }
}

export const recordState: Selector<IStoreState, IInventoryRecordState> = (state: IStoreState): IInventoryRecordState => state.inventoryRecord;
export const getRecord: IRecordSelector = createSelector(recordState, (state: IInventoryRecordState): IInventoryRecordV2 => state.record);
export const getSchema: ISchemaSelector = createSelector(recordState, (state: IInventoryRecordState): IAttributeDetailV2[] => state.schema);
export const getDisplayValuesMap: IDisplayValuesSelector = createSelector(recordState, (state: IInventoryRecordState): IStringMap<string> => state.displayValues);
export const getIsLoadingRecord: IIsLoadingRecordSelector = createSelector(recordState, (state: IInventoryRecordState): boolean => state.isLoadingRecord);
export const getIsLoadingSecondaryRecord: IIsLoadingRecordSelector = createSelector(recordState, (state: IInventoryRecordState): boolean => state.isLoadingSecondaryRecord);
export const getRecordChanges: IRecordChangesSelector = createSelector(recordState, (state: IInventoryRecordState): IInventoryRecordV2 => state.changes);
export const getHasRequiredValues: IRequiredHasValuesSelector = createSelector(getRecord, getRecordChanges, getSchema,
    (record: IInventoryRecordV2, changes: IInventoryRecordV2, schema: IAttributeDetailV2[]): boolean => {
        const requiredAttributes = schema.filter((attribute) => attribute.required);
        return requiredAttributes.reduce((hasRequiredValues: boolean, attribute): boolean => {
            if (!hasRequiredValues) return false;
            if (attribute.fieldName === AttributeFieldNames.Location || attribute.fieldName === AttributeFieldNames.Type) {
                const value = { ...record, ...changes }[attribute.fieldName];
                return value && (value as IAttributeOptionV2[]).length !== 0;
            }
            return Boolean({...record, ...changes}[attribute.fieldName]);
        }, true);
    },
);
export const getAttributeMap: IAttributeMapSelector = createSelector(recordState, (state: IInventoryRecordState): IStringMap<IAttributeDetailV2> => {
    const attributeMap: IStringMap<IAttributeDetailV2> = {};
    state.schema.forEach((attribute: IAttributeDetailV2) => {
        attributeMap[attribute.fieldName] = attribute;
    });
    return attributeMap;
});
export const getHasChanges: IHasChangesSelector = createSelector(recordState, (state: IInventoryRecordState): boolean => Object.keys(state.changes).length > 0);
export const getHasErrors: IHasErrorsSelector = createSelector(recordState, (state: IInventoryRecordState): boolean => Object.keys(state.errors).filter((fieldName) => state.errors[fieldName]).length > 0);
export const getIsEditingRecord: IEditingRecordSelector = createSelector(recordState, (state: IInventoryRecordState): boolean => state.isEditing);
export const getCurrentRecordName: ICurrentNameSelector = createSelector(recordState, (state: IInventoryRecordState): string => ({ ...state.record, ...state.changes }).name as string || "" );
export const getCurrentInventoryId: IInventoryIdSelector = createSelector(recordState, (state: IInventoryRecordState): string => state.inventoryId);
export const getCurrentInventoryOrgGroupId: IInventoryOrgGroupSelector = createSelector(recordState, (state: IInventoryRecordState): string => state.record["organization"]["id"]);
