import { AnyAction } from "redux";
import { createSelector, Selector } from "reselect";
import { forkJoin, multiForkJoin, areBothEqualTo, compose, areAllEqualTo } from "oneRedux/utils/utils";
import { IStoreState, IStringToBooleanMap } from "interfaces/redux.interface";
import { isArray } from "lodash";
import { IUser } from "interfaces/user.interface";
import { IFilterIteratee } from "interfaces/redux.interface";
import {
    IUserMap,
    IUserState,
    IUserByIdSelector,
    IUserByIdSelectorFactory,
    IAllUserIdsSelector,
    IAllUserModelsSelector,
    IAllUsersSelector,
    ISelectedUserIdsSelector,
    ICurriedFilterUsersSelector,
    ICurriedFilterUsersSelectorFactory,
    ICurrentUserSelector,
} from "interfaces/user-reducer.interface";
import { UserRoles } from "constants/user-roles.constant";
import { getCurrentRelatedOrgIdsMap } from "oneRedux/reducers/org.reducer";

export const UserActions = {
    ADD_USER_LIST: "ADD_USER_LIST",
    SET_CURRENT_USER: "SET_CURRENT_USER",
};

const initialState: IUserState = {
    allIds: [],
    byId: {},
    selectedUserIds: {},
    currentUser: null,
};

export default function userReducer(state = initialState, action: AnyAction): IUserState {
    switch (action.type) {
        case UserActions.ADD_USER_LIST:
            const { allIds, byId } = action;
            return {...state, allIds, byId };
        case UserActions.SET_CURRENT_USER:
            const { currentUser } = action;
            return {...state, currentUser };
        default:
            return state;
    }
}

export const userState: Selector<IStoreState, IUserState> = (state: IStoreState): IUserState => state.users;
export const getCurrentUser: ICurrentUserSelector = createSelector(userState, (selectorUserState: IUserState): IUser => selectorUserState.currentUser);
export const getUserById: IUserByIdSelectorFactory = (id: string): IUserByIdSelector => createSelector(userState, (selectorUserState: IUserState): IUser => selectorUserState.byId[id]);
export const getAllUserIds: IAllUserIdsSelector = createSelector(userState, (selectorUserState: IUserState): string[] => selectorUserState.allIds);
export const getAllUserModels: IAllUserModelsSelector = createSelector(userState, (selectorUserState: IUserState): IUserMap => selectorUserState.byId);
export const getAllUsers: IAllUsersSelector = createSelector(getAllUserModels, getAllUserIds, (allModels: IUserMap, ids: string[]): IUser[] => ids.map((id: string): IUser => allModels[id]));
export const getFilteredUsers: ICurriedFilterUsersSelectorFactory = (iteratee: IFilterIteratee<IUser>): ICurriedFilterUsersSelector =>
    createSelector(getAllUsers, (users: IUser[]): IUser[] => users.filter(iteratee));
export const getMultiFilteredUsers = (...iteratees: any[]) => createSelector(
    getAllUsers,
    (userList: IUser[]) => {
        if (iteratees.length === 1 && isArray(iteratees[0])) {
            iteratees = iteratees[0];
        }
        return userList.filter(
            multiForkJoin(areAllEqualTo(true),
            ...iteratees as Array<IFilterIteratee<IUser>>,
        ));
    },
);
export const getUserDisplayName = (user: IUser): string => (user.FirstName || user.LastName) ? `${user.FirstName} ${user.LastName}`.trim() : user.Email;

export const isActive: IFilterIteratee<IUser> = (user: IUser): boolean => user.IsActive;
export const getActiveUsers: ICurriedFilterUsersSelector = getFilteredUsers(isActive);
export const isDisabled: IFilterIteratee<IUser> = (user: IUser): boolean => !user.IsActive;
export const getDisabledUsers: ICurriedFilterUsersSelector = getFilteredUsers(isDisabled);
export const isProjectViewer: IFilterIteratee<IUser> = (user: IUser): boolean => user.RoleName === UserRoles.ProjectViewer;
export const isNotProjectViewer: IFilterIteratee<IUser> = (user: IUser): boolean => user.RoleName !== UserRoles.ProjectViewer;
export const getProjectViewers: ICurriedFilterUsersSelector = getFilteredUsers(isProjectViewer);
export const isInvitedUser: IFilterIteratee<IUser> = (user: IUser): boolean => user.RoleName === UserRoles.Invited;
export const isNotInvitedUser: IFilterIteratee<IUser> = (user: IUser): boolean => user.RoleName !== UserRoles.Invited;
export const isNotViewerOrInvited = forkJoin(areBothEqualTo(true), isNotProjectViewer, isNotInvitedUser);
export const isUserInOrgIdsMap = (orgIdsMap: IStringToBooleanMap): any => (user: IUser): boolean => Boolean(orgIdsMap[user.OrgGroupId]);
export const getAllRelatedUsersThatAreViewer = createSelector(
    getCurrentRelatedOrgIdsMap,
    getAllUsers,
    (orgIdsMap: IStringToBooleanMap, userList: IUser[]): IUser[] => userList.filter(
        forkJoin(areBothEqualTo(true), isProjectViewer, isUserInOrgIdsMap(orgIdsMap)),
    ),
);

// Deprecating once getUsersByOrgAndPermission API call is updated
export const isSiteAdminOrPrivacyOfficer: IFilterIteratee<IUser> = (user: IUser): boolean => user.RoleName === UserRoles.SiteAdmin || user.RoleName === UserRoles.PrivacyOfficer;
