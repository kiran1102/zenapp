// External Libraries
import {
    omit,
    filter,
    map,
    isArray,
    memoize,
    forEach,
    find,
    mapValues,
    last,
    some,
} from "lodash";
import { AnyAction } from "redux";
import {
    createSelector,
    Selector,
} from "reselect";

// Interfaces
import {
    IGetTemplateTempModelState,
    ITemplateBuilderSelector,
    ITemplateBuilderState,
    ITemplateState,
    ITemplateSelector,
    ITemplateWelcomeMessageSelector,
    ITemplateUpdateSelector,
} from "interfaces/template-reducer.interface";
import { IStoreState } from "interfaces/redux.interface";
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";
import {
    IAssessmentTemplateSection,
} from "interfaces/assessment-template-section.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";
import {
    TemplateStates,
    TemplateStateProperty,
} from "constants/template-states.constant";
import { ICustomTemplate } from "modules/template/interfaces/custom-templates.interface";
import {
    ITemplateUpdateWelcomeText,
    ITemplateUpdateRequest,
    ITemplateUpdateSection,
    ITemplateUpdateQuestion,
} from "modules/template/interfaces/template-update-request.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { ITemplateRule } from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.interface";
import { IAssessmentNavigation } from "interfaces/assessment/assessment-navigation.interface";

// Enums
import { TemplateModes } from "enums/template-modes.enum";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

export const TemplateActions: IStringMap<string> = {
    ADD_QUESTION: "ADD_QUESTION",
    ADD_SECTION: "ADD_SECTION",
    DISCARD_TEMPLATE: "DISCARD_TEMPLATE",
    DELETE_TEMP_NAVIGATION: "DELETE_TEMP_NAVIGATION",
    DELETE_TEMPLATE_ACTION_RULE: "DELETE_TEMPLATE_ACTION_RULE",
    DISCARD_DRAFT: "DISCARD_DRAFT",
    DRAG_QUESTION_END: "DRAG_QUESTION_END",
    DRAG_QUESTION_START: "DRAG_QUESTION_START",
    DRAG_SECTION_END: "DRAG_SECTION_END",
    DRAG_SECTION_START: "DRAG_SECTION_START",
    FETCH_TEMPLATE: "FETCHING_TEMPLATE",
    FETCH_PUBLISHED_TEMPLATE_LIST: "FETCH_PUBLISHED_TEMPLATE_LIST",
    FETCH_BUILDER: "FETCH_BUILDER",
    PUBLISH_DRAFT: "PUBLISH_DRAFT",
    RESET_ACTION_TEMP_MODEL: "RESET_ACTION_TEMP_MODEL",
    RESET_BUILDER_VIEW: "RESET_BUILDER_VIEW",
    RESET_TEMP_MODEL_WITH_CURRENT: "RESET_TEMP_MODEL_WITH_CURRENT",
    RESET_TEMP_NAVIGATION: "RESET_TEMP_NAVIGATION",
    SET_BUILDER_VIEW: "SET_BUILDER_VIEW",
    SET_OPTIONS_BY_QUESTION_ID: "SET_OPTIONS_BY_QUESTION_ID",
    DELETE_OPTIONS_BY_QUESTION_ID: "DELETE_OPTIONS_BY_QUESTION_ID",
    SET_TEMPLATES: "SET_TEMPLATES",
    SET_PUBLISHED_TEMPLATE_LIST: "SET_PUBLISHED_TEMPLATE_LIST",
    TEMPLATE_BUILDER_LOADER_HIDE: "TEMPLATE_BUILDER_LOADER_HIDE",
    TEMPLATE_BUILDER_LOADER_SHOW: "TEMPLATE_BUILDER_LOADER_SHOW",
    TEMPLATE_FETCHED: "TEMPLATE_FETCHED",
    UPDATE_ACTION_TEMP_MODEL: "UPDATE_ACTION_TEMP_MODEL",
    UPDATE_NAVIGATIONS: "UPDATE_NAVIGATIONS",
    UPDATE_QUESTION: "UPDATE_QUESTION",
    UPDATE_SECTION: "UPDATE_SECTION",
    UPDATE_SECTION_ALL_IDS: "UPDATE_SECTION_ALL_IDS",
    UPDATE_TEMPLATE: "UPDATE_TEMPLATE",
    UPDATE_TEMPLATE_ACTION_RULE: "UPDATE_TEMPLATE_ACTION_RULE",
    UPDATE_TEMPLATE_METADATA: "UPDATE_TEMPLATE_METADATA",
    UPDATE_TEMPLATE_MODE: "UPDATE_TEMPLATE_MODE",
    UPDATE_TEMPLATE_SETTINGS: "UPDATE_TEMPLATE_SETTINGS",
    UPDATE_TEMPLATE_WELCOME_TEXT: "UPDATE_TEMPLATE_WELCOME_TEXT",
    UPDATE_TEMP_MODEL_QUESTION: "UPDATE_TEMP_MODEL_QUESTION",
    UPDATE_TEMP_MODEL_SECTION: "UPDATE_TEMP_MODEL_SECTION",
    UPDATE_TEMP_MODEL_METADATA_AND_SETTINGS: "UPDATE_TEMP_MODEL_METADATA_AND_SETTINGS",
    UPDATE_TEMP_MODEL_WELCOME_TEXT: "UPDATE_TEMP_MODEL_WELCOME_TEXT",
    UPDATE_TEMP_MODEL_INVENTORY_RELATIONS: "UPDATE_TEMP_MODEL_INVENTORY_RELATIONS",
    UPDATE_TEMP_NAVIGATION: "UPDATE_TEMP_NAVIGATION",
    UPDATE_QUESTION_RELATIONSHIP_CONFIGURATION: "UPDATE_QUESTION_RELATIONSHIP_CONFIGURATION",
    ADD_TEMPLATE_RULE_COMBINATION: "ADD_TEMPLATE_RULE_COMBINATION",
    UPDATE_SELECTED_TEMPLATE_RULE_COMBINATION: "UPDATE_SELECTED_TEMPLATE_RULE_COMBINATION",
    REMOVE_SELECTED_TEMPLATE_RULE_COMBINATION: "REMOVE_SELECTED_TEMPLATE_RULE_COMBINATION",
    RESET_SELECTED_TEMPLATE_RULE_COMBINATION: "RESET_SELECTED_TEMPLATE_RULE_COMBINATION",
    ADD_SKIP_CONDITION_COMBINATION: "ADD_SKIP_CONDITION_COMBINATION",
    UPDATE_SELECTED_SKIP_CONDITION_COMBINATION: "UPDATE_SELECTED_SKIP_CONDITION_COMBINATION",
    REMOVE_SELECTED_SKIP_CONDITION_COMBINATION: "REMOVE_SELECTED_SKIP_CONDITION_COMBINATION",
    TEMPLATE_VERSION_HISTORY_PANE_TOGGLE: "TEMPLATE_VERSION_HISTORY_PANE_TOGGLE",
    DISABLE_HEADER_ACTIONS: "DISABLE_HEADER_ACTIONS",
    UPDATE_TEMPLATE_BY_ID: "UPDATE_TEMPLATE_BY_ID",
    UPDATE_TEMPLATE_CARD_ACTIVE_STATE: "UPDATE_TEMPLATE_CARD_ACTIVE_STATE",
    COPY_TEMPLATE: "COPY_TEMPLATE",
    LOCK_TEMPLATE: "LOCK_TEMPLATE",
};

function getInitialBuilderState(): ITemplateBuilderState {
    return {
        questionById: {},
        sectionIdQuestionIdsMap: {},
        sectionById: {},
        sectionAllIds: [],
        navigationById: {},
        questionIdNavigationIdsMap: null,
        navigationTempModel: null,
        actionById: {},
        actionAllIds: null,
        actionTempModel: null,
        templateRuleCombinations: [],
        skipConditionCombinations: {},
    };
}

const initialState: ITemplateState = {
    current: null,
    isTempModelStateChanged: false,
    tempModel: null,
    state: null,
    isFetchingTemplate: false,
    isFetchingPublishedTemplate: false,
    isFetchingBuilder: false,
    isTemplateVersionHistoryPaneOpen: false,
    builder: getInitialBuilderState(),
    dragQuestion: false,
    dragSection: false,
    loaderText: "",
    templateMode: TemplateModes.Readonly,
    templateIds: null,
    templateByIds: null,
    publishedTemplateList: [],
    optionsByQuestionIdsMap: {},
    disableHeaderActions: false,
    inventorySettings: {
        prePopulateResponse: {},
    },
    isTemplateCopied: false,
    orgGroupId: null,
};

function flattenTemplatesStructure(input: ICustomTemplate[]): { templateIds: string[], templateByIds: { [key: string]: ICustomTemplate } } {
    const output = { templateIds: [], templateByIds: {} };

    forEach(input, (template: ICustomTemplate): void => {
        if (template.published) {
            template.activeState = TemplateStateProperty.Published;
        } else if (template.draft) {
            template.activeState = TemplateStateProperty.Draft;
        }
        output.templateIds.push(template.rootVersionId);
        output.templateByIds[template.rootVersionId] = template;
    });

    return output;
}

function getTemplateMode(status: string): TemplateModes {
    return status === TemplateStates.DRAFT.toUpperCase()
        ? TemplateModes.Create
        : TemplateModes.Readonly;
}

function getTempModel(currentModel: IAssessmentTemplateDetail): ITemplateUpdateRequest {
    return {
        welcomeText: {
            welcomeText: currentModel.welcomeText,
        },
        metadata: {
            description: currentModel.description,
            friendlyName: currentModel.friendlyName,
            icon: currentModel.icon,
            name: currentModel.name,
            orgGroupId: currentModel.orgGroupId,
        },
        settings: {
            prepopulateAttributeQuestions: currentModel.prepopulateAttributeQuestions,
        },
        inventoryRelations: [],
    };
}

function getUpdatedNavigations(state: ITemplateState, deletedNavigationIds: string[]): {
    navigationById: IStringMap<IAssessmentNavigation>,
    questionIdNavigationIdsMap: IStringMap<string[]>,
} {
    const newNavigations = { ...state.builder.navigationById };
    const navigationIdsByQuestionToMerge = mapValues(state.builder.questionIdNavigationIdsMap, (navIdsByQues: string[]) => {
        forEach(deletedNavigationIds, (deletedNav: string) => {
            const index = navIdsByQues.indexOf(deletedNav);
            if (index > -1) {
                navIdsByQues.splice(index, 1);
            }
        });
        return navIdsByQues;
    });

    return {
        navigationById: omit(newNavigations, deletedNavigationIds),
        questionIdNavigationIdsMap: navigationIdsByQuestionToMerge,
    };
}

// the cyclomatic-complexity of this function is increased due to many switch cases and other control flows in between
// tslint:disable-next-line:cyclomatic-complexity
export default function templateReducer(state: ITemplateState = initialState, action: AnyAction): ITemplateState {
    switch (action.type) {
        case TemplateActions.FETCH_TEMPLATE:
            return { ...state, isFetchingTemplate: true };
        case TemplateActions.TEMPLATE_FETCHED:
            return {
                ...state,
                isFetchingTemplate: false,
                current: action.current,
                tempModel: getTempModel(action.current),
                templateMode: getTemplateMode(action.current.status),
            };
        case TemplateActions.RESET_TEMP_MODEL_WITH_CURRENT:
            const { questionIds } = action;
            forEach(state.builder.questionById, (questionObj: IAssessmentTemplateQuestion) => {
                if (some(questionIds, (questionId: string): boolean => questionId === questionObj.id)) {
                    questionObj.destinationInventoryQuestion = false;
                }
            });
            return {
                ...state,
                isTempModelStateChanged: false,
                tempModel: getTempModel(state.current),
                templateMode: getTemplateMode(state.current.status),
                builder: {
                    ...state.builder,
                    questionById: {
                        ...state.builder.questionById,
                    },
                },
            };
        case TemplateActions.UPDATE_TEMPLATE:
            const newCurrentTemplate = {
                ...state.current,
                // metadata
                name: action.template.metadata.name,
                description: action.template.metadata.description,
                friendlyName: action.template.metadata.friendlyName,
                icon: action.template.metadata.icon,
                orgGroupId: action.template.metadata.orgGroupId,
                // settings
                directLaunchEnabled: action.template.settings.directLaunchEnabled,
                launchFromInventory: action.template.settings.launchFromInventory,
                orgGroupSelectionAllowed: action.template.settings.orgGroupSelectionAllowed,
                prepopulateAttributeQuestions: action.template.settings.prepopulateAttributeQuestions,
                primaryAssetQuestionId: action.template.settings.primaryAssetQuestionId,
                primaryProcessingActivityQuestionId: action.template.settings.primaryProcessingActivityQuestionId,
                primaryVendorQuestionId: action.template.settings.primaryVendorQuestionId,
                respondentSelectionAllowed: action.template.settings.respondentSelectionAllowed,
                selfServiceEnabled: action.template.settings.selfServiceEnabled,
                // welcome text
                welcomeText: action.template.welcomeText.welcomeText,
            };

            const newSectionByIds = { ...state.builder.sectionById };
            const newQuestionByIds = { ...state.builder.questionById };

            // update the sections/questions if any
            if (isArray(action.template.sections) && action.template.sections.length > 0) {

                action.template.sections.forEach((section: ITemplateUpdateSection) => {
                    if (section.name) {
                        newSectionByIds[section.sectionId] = {
                            ...newSectionByIds[section.sectionId],
                            name: section.name,
                        };
                    }
                    if (isArray(section.questions) && section.questions.length > 0) {
                        section.questions.forEach((question: ITemplateUpdateQuestion) => {
                            newQuestionByIds[question.questionId] = {
                                ...newQuestionByIds[question.questionId],
                                content: question.content,
                                friendlyName: question.friendlyName,
                            };
                        });
                    }
                });

            }
            return {
                ...state,
                current: newCurrentTemplate,
                isTempModelStateChanged: false,
                tempModel: getTempModel(newCurrentTemplate),
                builder: {
                    ...state.builder,
                    sectionById: newSectionByIds,
                    questionById: newQuestionByIds,
                },
                inventorySettings: { prePopulateResponse: {} },
            };
        case TemplateActions.UPDATE_TEMPLATE_METADATA: {
            const { metadata } = action;
            return {
                ...state,
                current: {
                    ...state.current,
                    description: metadata.description,
                    friendlyName: metadata.friendlyName,
                    icon: metadata.icon,
                    name: metadata.name,
                    orgGroupId: metadata.orgGroupId,
                },
                tempModel: {
                    ...state.tempModel,
                    metadata: {
                        description: metadata.description,
                        friendlyName: metadata.friendlyName,
                        icon: metadata.icon,
                        name: metadata.name,
                        orgGroupId: metadata.orgGroupId,
                    },
                },
                isTempModelStateChanged: false,
                disableHeaderActions: false,
                templateMode: getTemplateMode(state.current.status),
            };
        }
        case TemplateActions.UPDATE_TEMPLATE_SETTINGS:
            return {
                ...state,
                current: {
                    ...state.current,
                    directLaunchEnabled: action.settings.directLaunchEnabled,
                    launchFromInventory: action.settings.launchFromInventory,
                    orgGroupSelectionAllowed: action.settings.orgGroupSelectionAllowed,
                    prepopulateAttributeQuestions: action.settings.prepopulateAttributeQuestions,
                    primaryAssetQuestionId: action.settings.primaryAssetQuestionId,
                    primaryProcessingActivityQuestionId: action.settings.primaryProcessingActivityQuestionId,
                    primaryVendorQuestionId: action.settings.primaryVendorQuestionId,
                    respondentSelectionAllowed: action.settings.respondentSelectionAllowed,
                    selfServiceEnabled: action.settings.selfServiceEnabled,
                },
                isTempModelStateChanged: false,
                templateMode: getTemplateMode(state.current.status),
            };
        case TemplateActions.UPDATE_TEMPLATE_WELCOME_TEXT:
            const { keyValueToMerge } = action;
            return {
                ...state,
                current: {
                    ...state.current,
                    welcomeText: action.welcomeText.welcomeText,
                },
                tempModel: {
                    ...state.tempModel,
                    welcomeText: action.welcomeText,
                },
                isTempModelStateChanged: false,
                templateMode: getTemplateMode(state.current.status),
            };
        case TemplateActions.RESET_BUILDER_VIEW:
            return {
                ...state,
                builder: getInitialBuilderState(),
                isFetchingPublishedTemplate: false,
                publishedTemplateList: [],
                optionsByQuestionIdsMap: {},
            };
        case TemplateActions.SET_BUILDER_VIEW:
            return {
                ...state,
                builder: { ...state.builder, ...action.builderStructure },
                loaderText: "",
            };
        case TemplateActions.FETCH_BUILDER:
            return {
                ...state,
                isFetchingBuilder: action.isFetching,
            };
        case TemplateActions.ADD_SECTION:
            const sectionAllIds = [...state.builder.sectionAllIds];
            sectionAllIds.splice(action.section.sequence - 1, 0, action.section.id);
            const newState = {
                ...state,
                builder: {
                    ...state.builder,
                    ...{
                        sectionAllIds,
                        sectionById: { ...state.builder.sectionById, ...{ [action.section.id]: action.section } },
                    },
                },
            };
            return newState;
        case TemplateActions.ADD_QUESTION:
            const sectionIdQuestionIdsMap = { ...state.builder.sectionIdQuestionIdsMap };
            if (!sectionIdQuestionIdsMap[action.sectionId]) {
                sectionIdQuestionIdsMap[action.sectionId] = [];
            }
            sectionIdQuestionIdsMap[action.sectionId].splice(action.question.sequence - 1, 0, action.questionId);
            // re-order question sequence in the action section, while adding a new question
            map(state.builder.questionById, (question: IAssessmentTemplateQuestion): IAssessmentTemplateQuestion => {
                const questionBelongsToActionSection = sectionIdQuestionIdsMap[action.sectionId].indexOf(question.id) >= 0;
                question.sequence = (questionBelongsToActionSection && (question.sequence > action.question.sequence - 1)) ? question.sequence += 1 : question.sequence;
                return question;
            });
            return {
                ...state,
                builder: {
                    ...state.builder,
                    sectionIdQuestionIdsMap,
                    questionById: { ...state.builder.questionById, [action.questionId]: action.question },
                },
            };
        case TemplateActions.UPDATE_QUESTION:
            if (action.deletedNavigationIds && action.deletedNavigationIds.length > 0) {
                const navigationChanges = getUpdatedNavigations(state, action.deletedNavigationIds);
                return {
                    ...state,
                    builder: {
                        ...state.builder,
                        ...navigationChanges,
                        questionById: { ...state.builder.questionById, [action.question.id]: { ...state.builder.questionById[action.question.id], ...action.question } },
                    },
                };
            } else {
                return {
                    ...state,
                    builder: {
                        ...state.builder,
                        questionById: { ...state.builder.questionById, [action.question.id]: { ...state.builder.questionById[action.question.id], ...action.question } },
                    },
                };
            }
        case TemplateActions.UPDATE_SECTION:
            return {
                ...state,
                builder: {
                    ...state.builder,
                    sectionById: {
                        ...state.builder.sectionById,
                        [action.sectionId]: {
                            ...state.builder.sectionById[action.sectionId],
                            name: action.sectionName,
                        },
                    },
                },
            };
        case TemplateActions.UPDATE_SECTION_ALL_IDS:
            return {
                ...state,
                builder: {
                    ...state.builder,
                    sectionAllIds: action.sectionAllIds,
                },
            };
        case TemplateActions.DRAG_QUESTION_START:
            return {
                ...state,
                dragQuestion: true,
            };
        case TemplateActions.DRAG_QUESTION_END:
            return {
                ...state,
                dragQuestion: false,
            };
        case TemplateActions.TEMPLATE_BUILDER_LOADER_SHOW:
            return {
                ...state,
                loaderText: action.text,
            };
        case TemplateActions.TEMPLATE_BUILDER_LOADER_HIDE:
            return {
                ...state,
                loaderText: "",
            };
        case TemplateActions.DRAG_SECTION_START:
            return {
                ...state,
                dragSection: true,
            };
        case TemplateActions.DRAG_SECTION_END:
            return {
                ...state,
                dragSection: false,
            };
        case TemplateActions.UPDATE_TEMPLATE_MODE:
            return {
                ...state,
                templateMode: action.templateMode,
            };
        case TemplateActions.SET_TEMPLATES:
            return {
                ...state,
                ...flattenTemplatesStructure(action.templates),
            };
        case TemplateActions.DISCARD_DRAFT:
            let templateByIds = { ...state.templateByIds };
            let templateIds = [...state.templateIds];
            if (templateByIds[action.templateRootId]) {
                const currentTemplate = templateByIds[action.templateRootId];
                currentTemplate.draft = null;
                if (state.current) {
                    state.current.isDraft = false;
                }
                if (currentTemplate.published) {
                    currentTemplate.activeState = TemplateStateProperty.Published;
                } else {
                    templateIds = filter(templateIds, (templateId: string) => templateId !== action.templateRootId);
                    templateByIds = omit(state.templateByIds, action.templateRootId);
                }
            }

            return {
                ...state,
                templateIds,
                templateByIds,
            };
        case TemplateActions.DISCARD_TEMPLATE:
            let templateByIdsList = { ...state.templateByIds };
            let templateIdsList = [...state.templateIds];
            if (templateByIdsList[action.templateRootId]) {
                templateIdsList = filter(templateIdsList, (templateId: string) => templateId !== action.templateRootId);
                templateByIdsList = omit(state.templateByIds, action.templateRootId);
            }

            return {
                ...state,
                templateIds: templateIdsList,
                templateByIds: templateByIdsList,
            };
        case TemplateActions.PUBLISH_DRAFT:
            const template = { ...state.templateByIds[action.templateRootId] };
            if (template) {
                template.published = template.draft;
                template.published.status = TemplateStates.DRAFT.toUpperCase();
                template.draft = null;
                template.activeState = TemplateStateProperty.Published;
            }
            return {
                ...state,
                templateByIds: {
                    ...state.templateByIds,
                    [action.templateRootId]: template,
                },
            };
        case TemplateActions.UPDATE_TEMPLATE_BY_ID:
            const templateById = { ...state.templateByIds[action.templateRootId] };
            if (templateById.draft && templateById.published) {
                templateById.draft.isLocked = templateById.published.isLocked = action.isLocked;
            } else if (action.templateStatus === TemplateStates.DRAFT.toUpperCase()) {
                templateById.draft.isLocked = action.isLocked;
            } else {
                templateById.published.isLocked = action.isLocked;
            }
            return {
                ...state,
                templateByIds: {
                    ...state.templateByIds,
                    [action.templateId]: templateById,
                },
            };
        case TemplateActions.UPDATE_TEMP_MODEL_METADATA_AND_SETTINGS:
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    metadata: {
                        name: action.template.metadata.name,
                        description: action.template.metadata.description,
                        friendlyName: action.template.metadata.friendlyName,
                        icon: action.template.metadata.icon,
                        orgGroupId: action.template.metadata.orgGroupId,
                    },
                    settings: {
                        prepopulateAttributeQuestions: action.template.settings.prepopulateAttributeQuestions,
                    },
                },
                isTempModelStateChanged: true,
            };
        case TemplateActions.UPDATE_TEMP_MODEL_SECTION:
            // update the section
            let sectionUpdated = false;
            const sections = map(state.tempModel.sections, (section: ITemplateUpdateSection): ITemplateUpdateSection => {
                if (section.sectionId === action.sectionId) {
                    sectionUpdated = true;
                    return {
                        ...section,
                        name: action.sectionName,
                    };
                }
                return section;
            });
            // insert the section if not already in the list of updated ones
            if (!sectionUpdated) {
                sections.push({
                    sectionId: action.sectionId,
                    name: action.sectionName,
                    questions: null,
                });
            }
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    sections,
                },
            };
        case TemplateActions.UPDATE_TEMP_MODEL_WELCOME_TEXT:
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    welcomeText: action.welcomeText,
                },
            };
        case TemplateActions.UPDATE_TEMP_MODEL_QUESTION:
            // update the questin in the section
            let isSectionUpdated = false;
            const updatedSections = map(state.tempModel.sections, (section: ITemplateUpdateSection): ITemplateUpdateSection => {
                let isQuestionUpdated = false;
                const updatedQuestions = map(section.questions, (question: ITemplateUpdateQuestion): ITemplateUpdateQuestion => {
                    if (question.questionId === action.question.questionId) {
                        isQuestionUpdated = true;
                        return { ...action.question };
                    } else if (question.prePopulateResponse && action.question.prePopulateResponse) {
                        const inventoryType = state.builder.questionById[question.questionId].attributeJson.inventoryType;
                        if (inventoryType === action.inventoryType) {
                            return { ...question, prePopulateResponse: false };
                        }
                    }
                    return question;
                });
                if (!isQuestionUpdated && section.sectionId === action.sectionId) {
                    isSectionUpdated = true;
                    updatedQuestions.push({ ...action.question });
                }
                return {
                    ...section,
                    questions: updatedQuestions,
                };
            });
            if (!isSectionUpdated) {
                updatedSections.push({
                    sectionId: action.sectionId,
                    name: state.builder.sectionById[action.sectionId].name,
                    questions: [{ ...action.question }],

                });
            }
            if (action.question.prePopulateResponse) {
                state.inventorySettings.prePopulateResponse[action.inventoryType] = action.question.questionId;
            } else if (action.inventoryType && state.inventorySettings.prePopulateResponse[action.inventoryType] === action.question.questionId) {
                state.inventorySettings.prePopulateResponse[action.inventoryType] = null;
            }

            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    sections: updatedSections,
                },
            };
        case TemplateActions.UPDATE_TEMP_MODEL_INVENTORY_RELATIONS:
            return {
                ...state,
                tempModel: {
                    ...state.tempModel,
                    inventoryRelations: action.payload,
                },
            };
        case TemplateActions.UPDATE_TEMP_NAVIGATION:
            return {
                ...state,
                builder: {
                    ...state.builder,
                    navigationTempModel: {
                        ...state.builder.navigationTempModel,
                        [action.navigation.id]: action.navigation,
                    },
                },
            };
        case TemplateActions.RESET_TEMP_NAVIGATION:
            return {
                ...state,
                builder: {
                    ...state.builder,
                    navigationTempModel: null,
                },
            };
        case TemplateActions.UPDATE_NAVIGATIONS:
            const navigationToMerge: IStringMap<IAssessmentNavigation> = { ...state.builder.navigationById };
            const navigationIdsByQuestion = [];
            if (isArray(action.navigations) && action.navigations.length > 0) {
                action.navigations.forEach((navigation: IAssessmentNavigation, index: number) => {
                    navigationToMerge[navigation.id] = navigation;
                    navigationIdsByQuestion.push(navigation.id);
                });
            }
            return {
                ...state,
                builder: {
                    ...state.builder,
                    navigationById: navigationToMerge,
                    questionIdNavigationIdsMap: {
                        ...state.builder.questionIdNavigationIdsMap,
                        [action.questionId]: navigationIdsByQuestion,
                    },
                },
            };
        case TemplateActions.DELETE_TEMP_NAVIGATION: {
            const { navigationId } = action;
            return {
                ...state,
                builder: {
                    ...state.builder,
                    navigationTempModel: omit(state.builder.navigationTempModel, navigationId),
                },
            };
        }
        case TemplateActions.UPDATE_ACTION_TEMP_MODEL: {
            const { actionRule } = action;
            return {
                ...state,
                builder: {
                    ...state.builder,
                    actionTempModel: actionRule,
                },
            };
        }
        case TemplateActions.RESET_ACTION_TEMP_MODEL: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    actionTempModel: null,
                },
            };
        }
        case TemplateActions.SET_PUBLISHED_TEMPLATE_LIST: {
            return {
                ...state,
                isFetchingPublishedTemplate: false,
                publishedTemplateList: action.templates,
            };
        }
        case TemplateActions.DELETE_TEMPLATE_ACTION_RULE: {
            const actionAllIds = [...state.builder.actionAllIds];
            actionAllIds.splice(actionAllIds.indexOf(action.actionId), 1);
            return {
                ...state,
                builder: {
                    ...state.builder,
                    actionById: omit(state.builder.actionById, action.actionId),
                    actionAllIds,
                },
            };
        }
        case TemplateActions.FETCH_PUBLISHED_TEMPLATE_LIST: {
            return {
                ...state,
                isFetchingPublishedTemplate: true,
            };
        }
        case TemplateActions.UPDATE_TEMPLATE_ACTION_RULE: {
            const { actionRules } = action;
            const actionByIdToMerge = {};
            const actionAllIdsToMerge = [...state.builder.actionAllIds || []];
            actionRules.forEach((rule: ITemplateRule): void => {
                actionByIdToMerge[rule.id] = rule;
                if (actionAllIdsToMerge.findIndex((ruleId: string): boolean => ruleId === rule.id) === -1) {
                    actionAllIdsToMerge.push(rule.id);
                }
            });

            return {
                ...state,
                builder: {
                    ...state.builder,
                    actionAllIds: actionAllIdsToMerge,
                    actionById: {
                        ...state.builder.actionById,
                        ...actionByIdToMerge,
                    },
                },
            };
        }
        case TemplateActions.SET_OPTIONS_BY_QUESTION_ID: {
            return {
                ...state,
                optionsByQuestionIdsMap: {
                    ...state.optionsByQuestionIdsMap,
                    [action.questionId]: action.options,
                },
            };
        }
        case TemplateActions.DELETE_OPTIONS_BY_QUESTION_ID: {
            const { questionId } = action;
            return {
                ...state,
                optionsByQuestionIdsMap: omit(state.optionsByQuestionIdsMap, questionId),
            };
        }
        case TemplateActions.ADD_TEMPLATE_RULE_COMBINATION: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    templateRuleCombinations: action.combinations,
                },
            };
        }
        case TemplateActions.UPDATE_SELECTED_TEMPLATE_RULE_COMBINATION: {
            const combinations = state.builder.templateRuleCombinations;
            combinations.splice(action.index, 1, action.id);
            return {
                ...state,
                builder: {
                    ...state.builder,
                    templateRuleCombinations: combinations,
                },
            };
        }
        case TemplateActions.REMOVE_SELECTED_TEMPLATE_RULE_COMBINATION: {
            const combinations = state.builder.templateRuleCombinations;
            combinations.splice(action.index, 1);
            return {
                ...state,
                builder: {
                    ...state.builder,
                    templateRuleCombinations: combinations,
                },
            };
        }
        case TemplateActions.RESET_SELECTED_TEMPLATE_RULE_COMBINATION: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    templateRuleCombinations: [],
                },
            };
        }
        case TemplateActions.ADD_SKIP_CONDITION_COMBINATION: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    skipConditionCombinations: action.combinations,
                },
            };
        }
        case TemplateActions.UPDATE_SELECTED_SKIP_CONDITION_COMBINATION: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    skipConditionCombinations: {
                        ...state.builder.skipConditionCombinations,
                        [action.conditionId]: { ...action.condition },
                    },
                },
            };
        }
        case TemplateActions.REMOVE_SELECTED_SKIP_CONDITION_COMBINATION: {
            return {
                ...state,
                builder: {
                    ...state.builder,
                    skipConditionCombinations: omit(state.builder.skipConditionCombinations, action.conditionId),
                },
            };
        }
        case TemplateActions.TEMPLATE_VERSION_HISTORY_PANE_TOGGLE: {
            return {
                ...state,
                isTemplateVersionHistoryPaneOpen: !action.isTemplateVersionHistoryPaneOpen,
            };
        }
        case TemplateActions.DISABLE_HEADER_ACTIONS: {
            const { disableHeaderActions } = action;
            return {
                ...state,
                disableHeaderActions,
            };
        }
        case TemplateActions.UPDATE_QUESTION_RELATIONSHIP_CONFIGURATION: {
            const { questionId, destinationInventoryQuestion } = action.payload;
            return {
                ...state,
                builder: {
                    ...state.builder,
                    questionById: {
                        ...state.builder.questionById,
                        [questionId]: {
                            ...state.builder.questionById[questionId],
                            destinationInventoryQuestion,
                        },
                    },
                },
            };
        }
        case TemplateActions.UPDATE_TEMPLATE_CARD_ACTIVE_STATE: {
            const updatedStateTemplate = { ...state.templateByIds[action.templateRootId] };
            return {
                ...state,
                templateByIds: {
                    ...state.templateByIds,
                    [action.templateRootId]: {
                        ...updatedStateTemplate,
                        activeState: action.activeState,
                    },
                },
            };
        }
        case TemplateActions.COPY_TEMPLATE: {
            return {
                ...state,
                isTemplateCopied: action.isTemplateCopied,
            };
        }
        case TemplateActions.LOCK_TEMPLATE: {
            return {
                ...state,
                current: {
                    ...state.current,
                    isLocked: action.isLocked,
                },
                templateMode: state.current.status === TemplateStates.DRAFT.toUpperCase() ? TemplateModes.Create : TemplateModes.Readonly,
            };
        }
        default:
            return state;
    }
}

export const getTemplateState: Selector<IStoreState, ITemplateState> =
    (state: IStoreState): ITemplateState => state.template;
export const getCurrentTemplate: ITemplateSelector =
    createSelector(getTemplateState, (templateState: ITemplateState): IAssessmentTemplateDetail => templateState.current);
export const getTemplateTempModel: ITemplateUpdateSelector =
    createSelector(getTemplateState, (templateState: ITemplateState): ITemplateUpdateRequest => templateState.tempModel);
export const getTemplateTempModelWelcomeMessage: ITemplateWelcomeMessageSelector =
    createSelector(getTemplateTempModel, (tempModel: ITemplateUpdateRequest): ITemplateUpdateWelcomeText => tempModel.welcomeText);
export const getTemplateTempModelState: IGetTemplateTempModelState =
    createSelector(getTemplateState, (templateState: ITemplateState): boolean => templateState.isTempModelStateChanged);
export const getTemplateVersionHistoryDrawerState =
    createSelector(getTemplateState, (templateState: ITemplateState): boolean => templateState.isTemplateVersionHistoryPaneOpen);
export const getBuilderView: ITemplateBuilderSelector =
    createSelector(getTemplateState, (templateState: ITemplateState): ITemplateBuilderState => templateState.builder);
export const getSections: Selector<IStoreState, ((sectionId: string) => IAssessmentTemplateSection)> =
    createSelector(getBuilderView, (builderState: ITemplateBuilderState) => memoize((sectionId: string) => builderState.sectionById[sectionId]));
export const getSectionById: (state: IStoreState, sectionId: string) => IAssessmentTemplateSection =
    (state: IStoreState, sectionId: string): IAssessmentTemplateSection => getSections(state)(sectionId);
export const sectionHasInvalidQuestions: (state: IStoreState, sectionId: string) => boolean =
    (state: IStoreState, sectionId: string): boolean => !!(getSections(state)(sectionId).invalidQuestionCount > 0);
export const getTempSections: Selector<IStoreState, ((sectionId: string) => ITemplateUpdateSection)> =
    createSelector(getTemplateTempModel, (tempModel: ITemplateUpdateRequest) => memoize((sectionId: string): ITemplateUpdateSection => {
        return find(tempModel.sections, (section: ITemplateUpdateSection): boolean => {
            return section.sectionId === sectionId;
        });
    }));
export const getTempSectionNameById: (state: IStoreState, sectionId: string) => string =
    (state: IStoreState, sectionId: string): string => {
        const section = getTempSections(state)(sectionId);
        return section ? section.name : "";
    };
export const getTempQuestionById: (state: IStoreState, sectionId: string, questionId: string) => ITemplateUpdateQuestion =
    (state: IStoreState, sectionId: string, questionId: string): ITemplateUpdateQuestion => {
        const section = getTempSections(state)(sectionId);
        if (section && section.questions) {
            return find(section.questions, (question: ITemplateUpdateQuestion): boolean => {
                return question.questionId === questionId;
            });
        } else {
            return null;
        }
    };
export const getNavigationById = (navigationId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        return state.navigationById ? { ...state.navigationById[navigationId] } : {} as IAssessmentNavigation;
    });
export const getTempNavigationById = (navigationId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        return { ...state.navigationTempModel[navigationId] };
    });
export const getNavigationsByQuestion = (questionId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        const navigationIds = state.questionIdNavigationIdsMap
            ? (state.questionIdNavigationIdsMap[questionId] || [])
            : [];
        return navigationIds.map((navigationId: string) => {
            return { ...state.navigationById[navigationId] };
        });
    });
export const getTempNavigationsByQuestion = (questionId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        return state.navigationTempModel;
    });
export const hasQuestionNavigation = (questionId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        const navigationIds = state.questionIdNavigationIdsMap ? state.questionIdNavigationIdsMap[questionId] : [];
        return navigationIds && navigationIds.length > 0;
    });
export const getQuestionById = (questionId: string) =>
    createSelector(getBuilderView, (builder: ITemplateBuilderState) => {
        return { ...builder.questionById[questionId] };
    });
export const allTempNavigationValid = (navigationIds: string[]) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        const navigationTempModel = state.navigationTempModel || {};
        return navigationIds.every((navigationId: string) => navigationTempModel[navigationId] ? !navigationTempModel[navigationId].invalid : true);
    });
export const getTemplateActionRuleById = (actionRuleId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => state.actionById ? { ...state.actionById[actionRuleId] } : null);
export const getTemplateActionRuleTempModel =
    createSelector(getBuilderView, (state: ITemplateBuilderState) => state.actionTempModel);
export const getPublishedTemplates =
    createSelector(getTemplateState, (templateState: ITemplateState): ITemplateNameValue[] => templateState.publishedTemplateList);
export const isFetchingPublishedTemplates =
    createSelector(getTemplateState, (templateState: ITemplateState): boolean => templateState.isFetchingPublishedTemplate);
export const getTemplateRuleNextSequence =
    createSelector(getBuilderView, (state: ITemplateBuilderState): number => {
        const lastRule = last(state.actionAllIds);
        return state.actionById && state.actionById[lastRule] ? state.actionById[lastRule].sequence + 1 : 1;
    });
export const getTemplateQuestionOptions = (questionId: string) =>
    createSelector(getTemplateState, (state: ITemplateState) => state.optionsByQuestionIdsMap[questionId]);
export const getTemplateRuleCombinations =
    createSelector(getBuilderView, (state: ITemplateBuilderState) => state.templateRuleCombinations);
export const getSkipConditionCombinations =
    createSelector(getBuilderView, (state: ITemplateBuilderState) => state.skipConditionCombinations);
export const getSectionByQuestionId = (questionId: string) =>
    createSelector(getBuilderView, (state: ITemplateBuilderState) => {
        for (const key in state.sectionIdQuestionIdsMap) {
            if (state.sectionIdQuestionIdsMap.hasOwnProperty(key)) {
                const questionIds = state.sectionIdQuestionIdsMap[key];
                if (questionIds.indexOf(questionId) > -1) {
                    return state.sectionById[key];
                }
            }
        }
    });
export const getTempPrePopulateResponseQuestionIds =
    createSelector(getTemplateState, (state: ITemplateState) => state.inventorySettings.prePopulateResponse);
export const getTempPrePopulateResponseQuestionIdByInventoryType = (type: string) =>
    createSelector(getTemplateState, (state: ITemplateState) => state.inventorySettings.prePopulateResponse[type]);
export const getTemplateById = (templateId: string) =>
    createSelector(getTemplateState, (state: ITemplateState) => state.templateByIds[templateId]);

export const isTemplateModelValid = createSelector(getTemplateState, (state: ITemplateState): boolean => {
    return Boolean(state.tempModel.metadata.name.length && state.tempModel.metadata.orgGroupId.length);
});
