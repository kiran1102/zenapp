import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IDrawerState,
    IDrawerStateSelector,
} from "interfaces/drawer-reducer.interface";

export const DrawerActions = {
    OPEN_DRAWER: "OPEN_DRAWER",
    CLOSE_DRAWER: "CLOSE_DRAWER",
};

const initialState: IDrawerState = {
    drawerIsOpen: false,
};

export default function drawerReducer(state = initialState, action: AnyAction): IDrawerState {
    switch (action.type) {
        case DrawerActions.OPEN_DRAWER:
            return {
                ...state,
                drawerIsOpen: true,
            };
        case DrawerActions.CLOSE_DRAWER:
            return {
                ...state,
                drawerIsOpen: false,
            };
        default:
            return state;
    }
}

export const drawerState: Selector<IStoreState, IDrawerState> = (state: IStoreState): IDrawerState => state.drawer;
export const getDrawerState: IDrawerStateSelector = createSelector(drawerState, (state: IDrawerState): boolean => state.drawerIsOpen);
