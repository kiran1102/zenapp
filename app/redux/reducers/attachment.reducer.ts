// Redux
import { AnyAction } from "redux";
import { createSelector } from "reselect";
import { Selector } from "reselect";

// Interfaces
import { IStoreState } from "interfaces/redux.interface";
import {
    IAttachmentState,
    IAttachmentListSelector,
    IAttachmentRefIdSelector,
    IFetchingAttachmentListSelector,
} from "interfaces/attachment-reducer.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IPageableOf } from "interfaces/pagination.interface";

export const AttachmentActions = {
    FETCHING_ATTACHMENT_LIST: "FETCHING_ATTACHMENT_LIST",
    ATTACHMENT_LIST_FETCHED: "ATTACHMENT_LIST_FETCHED",
    SAVING_ATTACHMENT: "SAVING_ATTACHMENT",
    DONE_SAVING_ATTACHMENT: "DONE_SAVING_ATTACHMENT",
    DOWNLOADING_ATTACHMENT: "DOWNLOADING_ATTACHMENT",
    DONE_DOWNLOADING_ATTACHMENT: "DONE_DOWNLOADING_ATTACHMENT",
    DELETING_ATTACHMENT: "DELETING_ATTACHMENT",
    DONE_DELETING_ATTACHMENT: "DONE_DELETING_ATTACHMENT",
    RECEIVE_ATTACHMENT_LIST: "RECEIVE_ATTACHMENT_LIST",
    SET_REF_ID: "SET_REF_ID",
    SET_DEFAULT_LIST: "SET_DEFAULT_LIST",
};

const initialState: IAttachmentState = {
    isFetchingAttachments: false,
    isSavingAttachment: false,
    isEditingAttachment: false,
    isDeletingAttachment: false,
    isDownloadingAttachment: false,
    attachmentListData: null,
    currentRefId: "",
};

export default function attachmentReducer(state = initialState, action: AnyAction): IAttachmentState {
    switch (action.type) {
        case AttachmentActions.FETCHING_ATTACHMENT_LIST:
            return {
                ...state,
                isFetchingAttachments: true,
            };
        case AttachmentActions.SET_DEFAULT_LIST:
            return {
                ...state,
                attachmentListData: null,
                currentRefId: "",
            };
        case AttachmentActions.ATTACHMENT_LIST_FETCHED:
            return {
                ...state,
                isFetchingAttachments: false,
            };
        case AttachmentActions.RECEIVE_ATTACHMENT_LIST:
            const { attachmentListData } = action;
            return {
                ...state,
                attachmentListData,
            };
        case AttachmentActions.SET_REF_ID:
            const { currentRefId } = action;
            return {
                ...state,
                currentRefId,
            };
        case AttachmentActions.SAVING_ATTACHMENT:
            return {
                ...state,
                isSavingAttachment: true,
            };
        case AttachmentActions.DONE_SAVING_ATTACHMENT:
            return {
                ...state,
                isSavingAttachment: false,
            };
        case AttachmentActions.DOWNLOADING_ATTACHMENT:
            return {
                ...state,
                isDownloadingAttachment: true,
            };
        case AttachmentActions.DONE_DOWNLOADING_ATTACHMENT:
            return {
                ...state,
                isDownloadingAttachment: false,
            };
        case AttachmentActions.DELETING_ATTACHMENT:
            return {
                ...state,
                isDeletingAttachment: true,
            };
        case AttachmentActions.DONE_DELETING_ATTACHMENT:
            return {
                ...state,
                isDeletingAttachment: false,
            };
        default:
            return state;
    }
}

export const attachmentState: Selector<IStoreState, IAttachmentState> = (state: IStoreState): IAttachmentState => state.attachment;
export const getAttachmentList: IAttachmentListSelector = createSelector(attachmentState, (state: IAttachmentState): IPageableOf<IAttachmentFileResponse> => state.attachmentListData);
export const getIsLoadingAttachments: IFetchingAttachmentListSelector = createSelector(attachmentState, (state: IAttachmentState): boolean => state.isFetchingAttachments);
export const getRefId: IAttachmentRefIdSelector = createSelector(attachmentState, (state: IAttachmentState): string => state.currentRefId);
