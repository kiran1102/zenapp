import { AnyAction } from "redux";
import {
    IRiskSettings,
    IProjectSettings,
    IHelpSettings,
} from "interfaces/setting.interface";
import { createSelector, Selector } from "reselect";
import assign from "lodash/assign";
import { IStoreState } from "interfaces/redux.interface";
import {
    ISettingState,
    IRiskSettingSelector,
    IRiskSettingsFetchStatus,
    IProjectSettingSelector,
    IProjectSettingsFetchStatus,
    ISmtpSettingSelector,
    ISmtpSettingFetchStatus,
    IHeatMapEnabledSelector,
    IRiskSummaryEnabledSelector,
    IQuestionApprovalEnabledSelector,
    ISmtpConnectionEnabledSelector,
    ISmtpConnectionToggleSelector,
    IHelpSettingSelector,
    IHelpSettingsFetchStatus,
    ISmtpSettingHasPendingChangeSelector,
    IIsIE11Selector,
    IConsentSettingsSelector,
    IConsentSettingsHasPendingChangeSelector,
    IConsentSettingsFetchStatus,
} from "interfaces/setting-reducer.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";
import { IConsentSettings } from "interfaces/consent-settings.interface";

export const SettingActions = {
    ADD_PROJECT_SETTINGS: "ADD_PROJECT_SETTINGS",
    FETCHING_PROJECT_SETTINGS: "FETCHING_PROJECT_SETTINGS",
    FETCHED_PROJECT_SETTINGS: "FETCHED_PROJECT_SETTINGS",
    ADD_RISK_SETTINGS: "ADD_RISK_SETTINGS",
    FETCHING_RISK_SETTINGS: "FETCHING_RISK_SETTINGS",
    FETCHED_RISK_SETTINGS: "FETCHED_RISK_SETTINGS",
    RESET_SETTINGS: "RESET_SETTINGS",
    TOGGLE_SMTP_CONNECTION_IN_PROGRESS: "TOGGLE_SMTP_CONNECTION_IN_PROGRESS",
    TOGGLE_SMTP_CONNECTION_COMPLETE: "TOGGLE_SMTP_CONNECTION_COMPLETE",
    TOGGLE_SMTP_CONNECTION: "TOGGLE_SMTP_CONNECTION",
    FETCHING_SMTP_SETTING: "FETCHING_SMTP_SETTING",
    FETCHED_SMTP_SETTING: "FETCHED_SMTP_SETTING",
    ADD_SMTP_SETTING: "ADD_SMTP_SETTING",
    ADD_TEMP_SMTP_SETTING: "ADD_TEMP_SMTP_SETTING",
    UPDATE_TEMP_SMTP_SETTING: "UPDATE_TEMP_SMTP_SETTING",
    ADD_HELP_SETTINGS: "ADD_HELP_SETTINGS",
    FETCHING_HELP_SETTINGS: "FETCHING_HELP_SETTINGS",
    FETCHED_HELP_SETTINGS: "FETCHED_HELP_SETTINGS",
    SET_SMTP_SETTING_PENDING_CHANGES_TO_FALSE: "SET_SMTP_SETTING_PENDING_CHANGES_TO_FALSE",
    SET_IS_IE_11: "SET_IS_IE_11",
    ADD_CONSENT_SETTINGS: "ADD_CONSENT_SETTINGS",
    UPDATE_CONSENT_SETTINGS: "UPDATE_CONSENT_SETTINGS",
    FETCHING_CONSENT_SETTINGS: "FETCHING_CONSENT_SETTINGS",
    FETCHED_CONSENT_SETTINGS: "FETCHED_CONSENT_SETTINGS",
    SAVED_CONSENT_SETTINGS: "SAVED_CONSENT_SETTINGS",
};

const initialState: ISettingState = {
    projectSettings: null,
    isFetchingProjectSettings: false,
    riskSettings: null,
    isFetchingRiskSettings: false,
    smtpSetting: null,
    tempSmtpSetting: null,
    isFetchingSmtpSetting: false,
    isTogglingSmtpConnection: false,
    helpSettings: null,
    isFetchingHelpSettings: false,
    hasPendingSmtpSettingChange: false,
    isIE11: false,
    consentSettings: null,
    hasPendingConsentSettingsChange: false,
    isFetchingConsentSettings: false,
};

export default function userReducer(state: ISettingState = initialState, action: AnyAction): ISettingState {
    switch (action.type) {
        case SettingActions.ADD_PROJECT_SETTINGS:
            const { projectSettings } = action;
            return {
                ...state,
                projectSettings: { ...projectSettings },
            };
        case SettingActions.FETCHING_PROJECT_SETTINGS:
            return {
                ...state,
                isFetchingProjectSettings: true,
            };
        case SettingActions.FETCHED_PROJECT_SETTINGS:
            return {
                ...state,
                isFetchingProjectSettings: false,
            };
        case SettingActions.ADD_RISK_SETTINGS:
            const { riskSettings } = action;
            return {...state,
                riskSettings: { ...riskSettings },
            };
        case SettingActions.FETCHING_RISK_SETTINGS:
            return {
                ...state,
                isFetchingRiskSettings: true,
            };
        case SettingActions.FETCHED_RISK_SETTINGS:
            return {
                ...state,
                isFetchingRiskSettings: false,
            };
        case SettingActions.RESET_SETTINGS:
            return initialState;
        case SettingActions.TOGGLE_SMTP_CONNECTION_IN_PROGRESS:
            return {
                ...state,
                isTogglingSmtpConnection: true,
            };
        case SettingActions.TOGGLE_SMTP_CONNECTION_COMPLETE:
            return {
                ...state,
                isTogglingSmtpConnection: false,
            };
        case SettingActions.TOGGLE_SMTP_CONNECTION:
            const { smtpEnabled } = action;
            return {
                ...state,
                smtpSetting: assign({}, state.smtpSetting, { smtpEnabled }),
                tempSmtpSetting: assign({}, state.tempSmtpSetting, { smtpEnabled }),
                hasPendingSmtpSettingChange: state.hasPendingSmtpSettingChange && smtpEnabled,
            };
        case SettingActions.FETCHING_SMTP_SETTING:
            return {
                ...state,
                isFetchingSmtpSetting: true,
            };
        case SettingActions.FETCHED_SMTP_SETTING:
            return {
                ...state,
                isFetchingSmtpSetting: false,
            };
        case SettingActions.ADD_SMTP_SETTING:
            const { smtpSetting } = action;
            return {
                ...state,
                smtpSetting: { ...smtpSetting },
                hasPendingSmtpSettingChange: false,
            };
        case SettingActions.ADD_TEMP_SMTP_SETTING:
            const { tempSmtpSetting } = action;
            return {
                ...state,
                tempSmtpSetting: { ...tempSmtpSetting },
                hasPendingSmtpSettingChange: false,
            };
        case SettingActions.UPDATE_TEMP_SMTP_SETTING:
            const { keyValueToMerge } = action;
            let hasPendingSmtpSettingChange: boolean;

            if (keyValueToMerge.validate !== undefined) {
                hasPendingSmtpSettingChange = state.hasPendingSmtpSettingChange;
            } else {
                hasPendingSmtpSettingChange = true;
            }

            return {
                ...state,
                tempSmtpSetting: assign({}, state.tempSmtpSetting, keyValueToMerge),
                hasPendingSmtpSettingChange,
            };
        case SettingActions.ADD_HELP_SETTINGS:
            const { helpSettings } = action;
            return {
                ...state,
                helpSettings: { ...helpSettings },
            };
        case SettingActions.FETCHING_HELP_SETTINGS:
            return {
                ...state,
                isFetchingHelpSettings: true,
            };
        case SettingActions.FETCHED_HELP_SETTINGS:
            return {
                ...state,
                isFetchingHelpSettings: false,
            };
        case SettingActions.SET_SMTP_SETTING_PENDING_CHANGES_TO_FALSE:
            return {
                ...state,
                hasPendingSmtpSettingChange: false,
            };
        case SettingActions.SET_IS_IE_11:
            const { isIE11 } = action;
            return {
                ...state,
                isIE11,
            };
        case SettingActions.ADD_CONSENT_SETTINGS:
            const { consentSettings } = action;
            return {
                ...state,
                hasPendingConsentSettingsChange: false,
                consentSettings: { ...consentSettings },
            };
        case SettingActions.UPDATE_CONSENT_SETTINGS:
            const { consentKeyToMerge } = action;
            return {
                ...state,
                consentSettings: assign({}, state.consentSettings, consentKeyToMerge),
                hasPendingConsentSettingsChange: true,
            };
        case SettingActions.FETCHING_CONSENT_SETTINGS:
            return {
                ...state,
                isFetchingConsentSettings: true,
            };
        case SettingActions.FETCHED_CONSENT_SETTINGS:
            return {
                ...state,
                isFetchingConsentSettings: false,
            };
        case SettingActions.SAVED_CONSENT_SETTINGS:
            return {
                ...state,
                hasPendingConsentSettingsChange: false,
            };
        default:
            return state;
    }
}

export const settingState: Selector<IStoreState, ISettingState> = (state: IStoreState): ISettingState => state.settings;
export const getRiskSettings: IRiskSettingSelector = createSelector(settingState, (settingState: ISettingState): IRiskSettings => settingState.riskSettings);
export const getRiskSettingsFetchStatus: IRiskSettingsFetchStatus = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isFetchingRiskSettings);
export const getProjectSettings: IProjectSettingSelector = createSelector(settingState, (settingState: ISettingState): IProjectSettings => settingState.projectSettings);
export const getProjectSettingsFetchStatus: IProjectSettingsFetchStatus = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isFetchingProjectSettings);
export const isHeatMapEnabled: IHeatMapEnabledSelector = createSelector(getRiskSettings, (riskSettings: IRiskSettings): boolean => riskSettings.Enabled);
export const isRiskSummaryEnabled: IRiskSummaryEnabledSelector = createSelector(getProjectSettings, (projectSettings: IProjectSettings): boolean => projectSettings.EnableRiskAnalysis);
export const getQuestionApprovalEnabled: IQuestionApprovalEnabledSelector = createSelector(settingState, (settingState: ISettingState): boolean => settingState.projectSettings && !settingState.projectSettings.ProjectLevelApproval);
export const getSmtpSetting: ISmtpSettingSelector = createSelector(settingState, (settingState: ISettingState): ISmtpSetting => settingState.smtpSetting);
export const getTempSmtpSetting: ISmtpSettingSelector = createSelector(settingState, (settingState: ISettingState): ISmtpSetting => settingState.tempSmtpSetting);
export const getSmtpSettingFetchStatus: ISmtpSettingFetchStatus = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isFetchingSmtpSetting);
export const isSmtpConnectionEnabled: ISmtpConnectionEnabledSelector = createSelector(getSmtpSetting, (smtpSettings: ISmtpSetting): boolean => smtpSettings ? smtpSettings.smtpEnabled : null);
export const isTogglingSmtpConnection: ISmtpConnectionToggleSelector = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isTogglingSmtpConnection);
export const getHelpSettings: IHelpSettingSelector = createSelector(settingState, (settingState: ISettingState): IHelpSettings => settingState.helpSettings);
export const getHelpSettingsFetchStatus: IHelpSettingsFetchStatus = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isFetchingHelpSettings);
export const hasPendingSmtpSettingChange: ISmtpSettingHasPendingChangeSelector = createSelector(settingState, (settingState: ISettingState): boolean => settingState.hasPendingSmtpSettingChange);
export const getIsIE11: IIsIE11Selector = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isIE11);
export const getConsentSettings: IConsentSettingsSelector = createSelector(settingState, (settingState: ISettingState): IConsentSettings => settingState.consentSettings);
export const hasPendingConsentSettingsChange: IConsentSettingsHasPendingChangeSelector = createSelector(settingState, (settingState: ISettingState): boolean => settingState.hasPendingConsentSettingsChange);
export const getConsentSettingsFetchStatus: IConsentSettingsFetchStatus = createSelector(settingState, (settingState: ISettingState): boolean => settingState.isFetchingConsentSettings);
