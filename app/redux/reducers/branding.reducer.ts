import { AnyAction } from "redux";
import { createSelector, Selector } from "reselect";

import {
    DEFAULT_BACKGROUND_COLOR,
    DEFAULT_BRAND_LOGO,
    DEFAULT_HEADER_LOGO,
    DEFAULT_TEXT_COLOR,
} from "constants/branding.constant";

import { IStoreState } from "interfaces/redux.interface";
import {
    IBrandingState,
    IBrandingSelector,
    IBrandingHeaderSelector,
    IBrandingFetchStatus,
} from "interfaces/branding-reducer.interface";
import { IBranding, IBrandingHeader } from "interfaces/branding.interface";

export const BrandingActions = {
    FETCHING_BRANDING: "FETCHING_BRANDING",
    FETCHED_BRANDING: "FETCHED_BRANDING",
    SET_DEFAULT_BRANDING_DATA: "SET_DEFAULT_BRANDING_DATA",
    SET_BRANDING_DATA: "SET_BRANDING_DATA",
    SET_BRANDING_DATA_AND_FETCHED: "SET_BRANDING_DATA_AND_FETCHED",
};

const defaultBranding: IBranding = {
    logo: {
        url: DEFAULT_BRAND_LOGO,
        name: "",
    },
    header: {
        backgroundColor: DEFAULT_BACKGROUND_COLOR,
        textColor: DEFAULT_TEXT_COLOR,
        title: "",
        icon: {
            name: "",
            url: DEFAULT_HEADER_LOGO,
        },
    },
};

const initialState: IBrandingState = {
    branding: null,
    isFetchingBranding: false,
    defaultBranding,
};

export function branding(state: IBrandingState = initialState, action: AnyAction): IBrandingState {
    switch (action.type) {
        case BrandingActions.FETCHING_BRANDING:
            return {
                ...state,
                isFetchingBranding: true,
            };
        case BrandingActions.FETCHED_BRANDING:
            return {
                ...state,
                isFetchingBranding: false,
            };
        case BrandingActions.SET_DEFAULT_BRANDING_DATA:
            return {
                ...state,
                defaultBranding: {
                    logo: { ...defaultBranding.logo, ...action.defaultBranding.logo },
                    header: { ...defaultBranding.header, ...action.defaultBranding.header },
                },
            };
        case BrandingActions.SET_BRANDING_DATA:
            return {
                ...state,
                branding: { ...action.branding },
            };
        case BrandingActions.SET_BRANDING_DATA_AND_FETCHED:
            return {
                ...state,
                isFetchingBranding: false,
                branding: { ...action.branding },
            };
        default:
            return state;
    }
}

export function mergeDefaultBrandingWithCurrent(defaults: IBranding, currentBranding: IBranding): IBranding {
    const defaultHeader = defaults.header;
    const currentHeader = currentBranding.header || defaultHeader;
    currentHeader.backgroundColor = !currentHeader.backgroundColor || currentHeader.backgroundColor === DEFAULT_BACKGROUND_COLOR
        ? defaultHeader.backgroundColor
        : currentHeader.backgroundColor;
    currentHeader.textColor = !currentHeader.textColor || currentHeader.textColor === DEFAULT_TEXT_COLOR
        ? defaultHeader.textColor
        : currentHeader.textColor;
    return {
        logo: { ...defaults.logo, ...currentBranding.logo },
        header: { ...defaultHeader, ...currentHeader },
    };
}

export const brandingStoreState: Selector<IStoreState, IBrandingState> = (state: IStoreState): IBrandingState => state.branding;
export const getBranding: IBrandingSelector = createSelector(brandingStoreState, (brandingState: IBrandingState): IBranding => mergeDefaultBrandingWithCurrent(brandingState.defaultBranding, brandingState.branding));
export const getHeaderBranding: IBrandingHeaderSelector = createSelector(brandingStoreState, (brandingState: IBrandingState): IBrandingHeader => mergeDefaultBrandingWithCurrent(brandingState.defaultBranding, brandingState.branding).header);
export const getBrandingFetchStatus: IBrandingFetchStatus = createSelector(brandingStoreState, (brandingState: IBrandingState): boolean => brandingState.isFetchingBranding);
