import { AnyAction } from "redux";
import { createSelector, Selector, OutputSelector } from "reselect";
import { omit } from "lodash";
import { IStoreState, IStoreStateSelector, IStringToBooleanMap } from "interfaces/redux.interface";
import { IOrganization, IOrganizationMap } from "interfaces/org.interface";
import {
    IOrgState,
    IRootOrgTreeSelector,
    IRootOrgListSelector,
    IRootOrgTableSelector,
    ICurrentOrgIdSelector,
    ISelectedOrgIdsSelector,
    IOrgByIdSelector,
    IOrgByIdSelectorFactory,
    IOrgNameByIdSelectorFactory,
    IOrgNameByIdSelector,
    IOrgListByIdSelector,
    IOrgListByIdSelectorFactory,
    IParentOrgsSelector,
    IParentOrgsSelectorFactory,
    IRelatedOrgsSelector,
    IRelatedOrgsSelectorFactory,
    IRelatedOrgIdsSelector,
    IRelatedOrgIdsSelectorFactory,
    ICurrentOrgListSelector,
    ICurrentParentListSelector,
    ICurrentRelatedOrgsSelector,
    ICurrentRelatedOrgIdsSelector,
    ICurrentRelatedOrgIdsMapSelector,
 } from "interfaces/org-reducer.interface";

export const OrgActions = {
    SET_ROOT_ORG_TREE: "SET_ROOT_ORG_TREE",
    SET_ROOT_ORG_LIST: "SET_ROOT_ORG_LIST",
    SET_ROOT_ORG_TABLE: "SET_ROOT_ORG_TABLE",
    SET_CURRENT_ORG: "SET_CURRENT_ORG",
    SELECT_ORG: "SELECT_ORG",
    UNSELECT_ORG: "UNSELECT_ORG",
    UNSELECT_ALL_ORGS: "UNSELECT_ALL_ORGS",
};

const initialState: IOrgState = {
    rootOrgTree: null,
    rootOrgList: [],
    rootOrgTable: {},
    orgList: [],
    currentOrgId: "",
    selectedOrgIds: {},
};

export default function orgReducer(state = initialState, action: AnyAction): IOrgState {
    switch (action.type) {
        case OrgActions.SET_ROOT_ORG_TREE:
            const { rootOrgTreeToSet } = action;
            return { ...state, rootOrgTree: rootOrgTreeToSet };
        case OrgActions.SET_ROOT_ORG_LIST:
            const { rootOrgListToSet } = action;
            return { ...state, rootOrgList: rootOrgListToSet };
        case OrgActions.SET_ROOT_ORG_TABLE:
            const { rootOrgTableToSet } = action;
            return { ...state, rootOrgTable: rootOrgTableToSet };
        case OrgActions.SET_CURRENT_ORG:
            const { currentOrgId } = action;
            return {
                ...state,
                currentOrgId,
            };
        case OrgActions.SELECT_ORG:
            const { orgIdToSelect } = action;
            return {
                ...state,
                selectedOrgIds: {
                    ...state.selectedOrgIds,
                    [orgIdToSelect]: true,
                },
            };
        case OrgActions.UNSELECT_ORG:
            const { orgIdToUnSelect } = action;
            return {
                ...state,
                selectedOrgIds: omit(state.selectedOrgIds, orgIdToUnSelect),
            };
        case OrgActions.UNSELECT_ALL_ORGS:
            return {
                ...state,
                selectedOrgIds: {},
            };
        default:
            return state;
    }
}

export const storeState: IStoreStateSelector = (state: IStoreState): IStoreState => state;
export const orgState: Selector<IStoreState, IOrgState> = (state: IStoreState): IOrgState => state.orgs;
export const getRootOrgTree: IRootOrgTreeSelector = createSelector(orgState, (orgState: IOrgState): IOrganization => orgState.rootOrgTree);
export const getRootOrgList: IRootOrgListSelector = createSelector(orgState, (orgState: IOrgState): IOrganization[] => orgState.rootOrgList);
export const getRootOrgTable: IRootOrgTableSelector = createSelector(orgState, (orgState: IOrgState): IOrganizationMap => orgState.rootOrgTable);
export const getCurrentOrgId: ICurrentOrgIdSelector = createSelector(orgState, (orgState: IOrgState): string => orgState.currentOrgId);
export const getSelectedOrgIds: ISelectedOrgIdsSelector = createSelector(orgState, (orgState: IOrgState): IStringToBooleanMap => orgState.selectedOrgIds);
export const getCurrentOrgTree: IOrgByIdSelector = createSelector(orgState, (orgState: IOrgState): IOrganization => findInTree(orgState.rootOrgTree, orgState.currentOrgId));
export const getOrgTreeById: IOrgByIdSelectorFactory = (Id: string): IRootOrgTreeSelector => createSelector(orgState, (orgState: IOrgState): IOrganization => findInTree(orgState.rootOrgTree, Id));
export const getOrgById: IOrgByIdSelectorFactory = (id: string): IOrgByIdSelector => createSelector(orgState, (orgState: IOrgState): IOrganization => orgState.rootOrgTable[id]);
export const getOrgNameById: IOrgNameByIdSelectorFactory = (id: string): IOrgNameByIdSelector => createSelector(orgState, (orgState: IOrgState): string => orgState.rootOrgTable[id].Name);
export const getOrgList: IOrgListByIdSelectorFactory = (id: string): IOrgListByIdSelector => createSelector(
    getRootOrgList,
    getOrgById(id),
    (rootOrgList: IOrganization[], selectedOrg: IOrganization): IOrganization[] => {
        if (!selectedOrg) return [];
        if (selectedOrg.Index === 0) {
            return rootOrgList;
        }
        const startInd = selectedOrg.Index;
        const listLen = rootOrgList.length;
        const orgList = [];
        for (let i = startInd; i < listLen; i++) {
            if (selectedOrg.Id === rootOrgList[i].Id) {
                orgList.push(rootOrgList[i]);
            } else if (selectedOrg.Level < rootOrgList[i].Level) {
                orgList.push(rootOrgList[i]);
            } else {
                break;
            }
        }
        return orgList;
    },
);
export const getParentOrgs: IParentOrgsSelectorFactory = (id: string): IParentOrgsSelector => createSelector(
    getRootOrgList,
    getOrgById(id),
    (rootOrgList: IOrganization[], selectedOrg: IOrganization): IOrganization[] => {
        if (!selectedOrg) return [];
        if (selectedOrg.Index === 0) {
            return [];
        }
        const startInd = selectedOrg.Index - 1;
        let currLevel = selectedOrg.Level - 1;
        const parentOrgs = [];
        for (let i = startInd; currLevel >= 0; i--) {
            if (rootOrgList[i].Level === currLevel) {
                parentOrgs.unshift(rootOrgList[i]);
                currLevel--;
            }
        }
        return parentOrgs;
    },
);
export const getRelatedOrgs: IRelatedOrgsSelectorFactory = (id: string): IRelatedOrgsSelector => createSelector(
    getParentOrgs(id),
    getOrgList(id),
    (parentOrgs: IOrganization[], orgList: IOrganization[]): IOrganization[] => parentOrgs.concat(orgList),
);
export const getRelatedOrgsById: IOrgListByIdSelectorFactory = (id: string): IOrgListByIdSelector => createSelector(
    getParentOrgs(id),
    getOrgById(id),
    (parentOrgs: IOrganization[], orgList: IOrganization): IOrganization[] => parentOrgs.concat(orgList),
);
export const getRelatedOrgIds: IRelatedOrgIdsSelectorFactory = (id: string): IRelatedOrgIdsSelector => createSelector(
    getRelatedOrgs(id),
    (relatedOrgs: IOrganization[]): string[] => relatedOrgs.map((org: IOrganization): string => org.Id),
);
export const getCurrentOrgList: ICurrentOrgListSelector = createSelector(
    storeState,
    getCurrentOrgId,
    (storeState: IStoreState, currentOrgId: string): IOrganization[] => getOrgList(currentOrgId)(storeState),
);
export const getCurrentParentList: ICurrentParentListSelector = createSelector(
    storeState,
    getCurrentOrgId,
    (storeState: IStoreState, currentOrgId: string): IOrganization[] => getParentOrgs(currentOrgId)(storeState),
);
export const getCurrentRelatedOrgs: ICurrentRelatedOrgsSelector = createSelector(
    getCurrentParentList,
    getCurrentOrgList,
    (parentOrgs: IOrganization[], orgList: IOrganization[]): IOrganization[] => parentOrgs.concat(orgList),
);
export const getCurrentRelatedOrgIds: ICurrentRelatedOrgIdsSelector = createSelector(
    getCurrentRelatedOrgs,
    (relatedOrgs: IOrganization[]): string[] => relatedOrgs.map((org: IOrganization): string => org.Id),
);
export const getCurrentRelatedOrgIdsMap: ICurrentRelatedOrgIdsMapSelector = createSelector(
    getCurrentRelatedOrgs,
    (relatedOrgs: IOrganization[]): IStringToBooleanMap => relatedOrgs.reduce((collector: IStringToBooleanMap, org: IOrganization): IStringToBooleanMap => {
        collector[org.Id] = true;
        return collector;
    }, {}),
);
export const findInTree = (org: IOrganization, Id: string) => {
    if (org.Id === Id) return org;
    else if (org.Children && org.Children.length) {
        const foundSubOrg = null;
        for (let i = 0; i < org.Children.length; i++) {
            const found = findInTree(org.Children[i], Id);
            if (found) return found;
        }
    }
    return null;
};
