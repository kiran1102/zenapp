import { combineReducers, Reducer } from "redux";
import { IStoreState } from "interfaces/redux.interface";
import attachment from "./attachment.reducer";
import drawer from "./drawer.reducer";
import inventory from "./inventory.reducer";
import localization from "settingsReducers/localization.reducer";
import modals from "./modal.reducer";
import orgs from "./org.reducer";
import risks from "./risk.reducer";
import settings from "./setting.reducer";
import template from "./template.reducer";
import users from "./user.reducer";
import { assessment } from "modules/assessment/reducers/assessment.reducer";
import { attributeManager } from "./attribute.reducer";
import { branding } from "./branding.reducer";
import { configuration } from "./configuration.reducer";
import { integration } from "oneRedux/reducers/integration.reducer";
import { preferenceCenter } from "oneRedux/reducers/preference-center.reducer";
import { purpose } from "oneRedux/reducers/purpose.reducer";
import { inventoryRecord } from "oneRedux/reducers/inventory-record.reducer";
import { vendor } from "modules/vendor/shared/reducers/vendor.reducer";

const rootReducer: Reducer<IStoreState> = combineReducers<IStoreState>({
    assessment,
    attachment,
    attributeManager,
    branding,
    drawer,
    configuration,
    integration,
    inventory,
    inventoryRecord,
    localization,
    modals,
    orgs,
    preferenceCenter,
    purpose,
    risks,
    settings,
    template,
    users,
    vendor,
});

export default rootReducer;
