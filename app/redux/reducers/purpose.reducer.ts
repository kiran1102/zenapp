import {
    assign,
    cloneDeep,
    find,
} from "lodash";
// Redux
import { AnyAction } from "redux";
import {
    Selector,
    createSelector,
} from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IPurposeState,
    IPurposeSelector,
    IPurposeStatusSelector,
} from "interfaces/purpose-reducer.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";
import { PurposeLanguage } from "crmodel/purpose-list";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

// Constants
import crConstants from "consentModule/shared/cr-constants";

export const PurposeActions = {
    UPDATE_PURPOSE: "UPDATE_PURPOSE",
    UPDATE_PURPOSE_DRAFT: "UPDATE_PURPOSE_DRAFT",
    FETCHING_PURPOSE: "FETCHING_PURPOSE",
    FETCHED_PURPOSE: "FETCHED_PURPOSE",
    SAVING_PURPOSE: "SAVING_PURPOSE",
    PURPOSE_ERROR: "PURPOSE_ERROR",
    SAVED_PURPOSE: "SAVED_PURPOSE",
    PURPOSE_VALID: "PURPOSE_VALID",
    PURPOSE_INVALID: "PURPOSE_INVALID",
};

const initialState: IPurposeState = {
    isLoading: false,
    isModified: false,
    isSaving: false,
    isValid: true,
    isLoadingLanguages: false,
};

export function purpose(state: IPurposeState = initialState, action: AnyAction): IPurposeState {
    switch (action.type) {
        case PurposeActions.UPDATE_PURPOSE:
            const { updatedPurpose } = action;
            return {
                ...state,
                purpose: updatedPurpose,
            };
        case PurposeActions.UPDATE_PURPOSE_DRAFT:
            const { updatedPurposeDraft, isModified } = action;
            return {
                ...state,
                purposeDraft: assign({}, state.purposeDraft, updatedPurposeDraft),
                isModified,
            };
        case PurposeActions.FETCHING_PURPOSE:
            return {
                ...initialState,
                isLoading: true,
            };
        case PurposeActions.FETCHED_PURPOSE:
            return {
                ...state,
                isLoading: false,
                language: getDefaultLanguage(state.purpose, state.allLanguages),
            };
        case PurposeActions.SAVING_PURPOSE:
            return {
                ...state,
                isSaving: true,
            };
        case PurposeActions.PURPOSE_ERROR:
            return {
                ...state,
                isSaving: false,
                isLoading: false,
            };
        case PurposeActions.SAVED_PURPOSE:
            return {
                ...state,
                purpose: assign({}, state.purposeDraft),
                isSaving: false,
                isModified: false,
            };
        case PurposeActions.PURPOSE_VALID:
            return {
                ...state,
                isValid: true,
            };
        case PurposeActions.PURPOSE_INVALID:
            return {
                ...state,
                isValid: false,
            };
        default:
            return state;
    }
}

export function getDefaultLanguage(purposeDetails: PurposeDetails, languages: IGetAllLanguageResponse[]): IGetAllLanguageResponse {
    if (!purposeDetails || !languages) {
        return null;
    }
    const purposeDefaultLanguage = purposeDetails.Languages.find((purposeLanguage: PurposeLanguage): boolean => {
        return purposeLanguage.Default;
    });
    return find(languages, (lang: IGetAllLanguageResponse): boolean => lang.Code === purposeDefaultLanguage.Language);
}

export const getPurposeState: Selector<IStoreState, IPurposeState> = (state: IStoreState): IPurposeState => state.purpose;

export const getPurposeLoadingStatus: IPurposeStatusSelector = createSelector(getPurposeState, (purposeState: IPurposeState): boolean => purposeState.isLoading);
export const getPurposeSavingStatus: IPurposeStatusSelector = createSelector(getPurposeState, (purposeState: IPurposeState): boolean => purposeState.isSaving);
export const getPurposeValidStatus: IPurposeStatusSelector = createSelector(getPurposeState, (purposeState: IPurposeState): boolean => purposeState.isValid);

export const getPurpose: IPurposeSelector = createSelector(getPurposeState, (purposeState: IPurposeState): PurposeDetails => cloneDeep(purposeState.purpose));
export const getPurposeDraft: IPurposeSelector = createSelector(getPurposeState, (purposeState: IPurposeState): PurposeDetails => cloneDeep(purposeState.purposeDraft));
