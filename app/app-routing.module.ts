import { NgModule } from "@angular/core";
import { APP_BASE_HREF } from "@angular/common";
import {
    ExtraOptions,
    PreloadAllModules,
    RouterModule,
    Routes,
} from "@angular/router";

// Components
import { WrapperComponent } from "./modules/wrapper/wrapper/wrapper.component";

// Modules
// Do not delete the below lines until we have completely migrated off of using Webpack directly to bundle our application.
// In order for Webpack to chunk modules, they must be imported somewhere. Then, in order for Angular to set them as
// lazy loaded chunks, they must be loaded as children based on path references.
import { LandingModule } from "./modules/landing/landing.module";
import { NotFoundModule } from "./modules/not-found/not-found.module";
import { WelcomeModule } from "./modules/welcome/welcome.module";
import { ErrorModule } from "./modules/error/error.module";
import { DGBetaModule } from "./modules/dg-beta/dg-beta.module";

// Guards
import { AuthGuard } from "./modules/core/guards/auth.guard";

// Resolves
import { OmniResolver } from "modules/core/resolves/omni-resolve.service";
import { ContentResolver } from "modules/core/resolves/content-resolve.service";
import { LocalBrandingResolver } from "modules/core/resolves/local-branding-resolve.service";
import { TranslationsResolver } from "modules/core/resolves/translations-resolve.service";
import { LoadingResolver } from "modules/core/resolves/loading-resolve.service";

const basicModuleResolves = {
    contentResolve: ContentResolver,
    localBrandingResolver: LocalBrandingResolver,
    translationsResolve: TranslationsResolver,
    loadingResolve: LoadingResolver,
};

const disableLoading = {
    loadingResolve: LoadingResolver,
};

const routes: Routes = [
    {
        path: "landing",
        loadChildren: "./modules/landing/landing.module#LandingModule",
        resolve: { omniResolver: OmniResolver },
        canActivate: [ AuthGuard ],
    },
    {
        path: "module",
        component: WrapperComponent,
        children: [
            {
                path: "",
                redirectTo: "welcome",
                pathMatch: "full",
            },
            {
                path: "welcome",
                loadChildren: "./modules/welcome/welcome.module#WelcomeModule",
                resolve: disableLoading,
            },
            {
                path: "data-guidance",
                loadChildren: "./modules/dg-beta/dg-beta.module#DGBetaModule",
                resolve: disableLoading,
            },
        ],
        resolve: { omniResolver: OmniResolver },
        canActivate: [ AuthGuard ],
    },
    {
        path: "error",
        loadChildren: "./modules/error/error.module#ErrorModule",
        resolve: basicModuleResolves,
    },
    {
        path: "404",
        loadChildren: "./modules/not-found/not-found.module#NotFoundModule",
        resolve: basicModuleResolves,
    },
    { path: "", redirectTo: "/landing", pathMatch: "full" },
    { path: "**", redirectTo: "/404" },
];

const extraOptions: ExtraOptions = {
    useHash: true,
    preloadingStrategy: PreloadAllModules,
    initialNavigation: false,
};

if (process.env.NODE_ENV === "development") {
    extraOptions.enableTracing = false;
}

@NgModule({
    imports: [RouterModule.forRoot(routes, extraOptions)],
    exports: [RouterModule],
    providers: [{ provide: APP_BASE_HREF, useValue: "/" }],
})
export class AppRoutingModule {}
