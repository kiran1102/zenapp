import "core-js/es7/reflect";
import "core-js/client/shim";
import "zone.js/dist/zone";

/* TODO: Remove of below remove polyfill.
** Bug no. AA-5361:Template view is broken in IE 11
** Currently ngx-drag-and-drop-list api using remove(), but remove() will not work in IE11
** Note: It was working fine earlier, needs some time to investigate on this issue
** Because to unblock the testing currently adding polyfill.
*/
import "modules/template/components/template-page/template-builder/template-selection-list/remove-polyfill.ts";

if (!Element.prototype.matches) {
    const el: any = Element.prototype;
    Element.prototype.matches = (
        el.matches
        || el.matchesSelector
        || el.msMatchesSelector
        || el.mozMatchesSelector
        || el.webkitMatchesSelector
        || el.oMatchesSelector
    );
}

/** IE10 and IE11 requires the following for NgClass support on SVG elements */
import "classlist.js";  // Run `npm install --save classlist.js`.
