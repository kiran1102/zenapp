declare var angular: angular.IAngularStatic;

import { upgradeModule } from "@uirouter/angular-hybrid";

export const Ng1Module = angular
    .module("legacy", [
        "zen",
        upgradeModule.name,
    ])
;
