declare var angular: angular.IAngularStatic;

import { downgradeComponent, downgradeInjectable } from "@angular/upgrade/static";

// Library
import { AlertBannerService } from "@onetrust/vitreus";

// Router
import { HybridRouter } from "modules/shared/services/helper/hybrid-router.service";

// Common
import { OmniService } from "modules/core/services/omni.service";
import { AuthEntryService } from "modules/core/services/auth-entry.service";
import { ProtocolService } from "modules/core/services/protocol.service";
import { Favicons } from "modules/shared/services/provider/favicon.service";
import { GlobalSidebarService } from "modules/global-sidebar/services/one-global-sidebar.service";
import { LoadingService } from "modules/core/services/loading.service";
import { AuthHandlerService } from "modules/shared/services/helper/auth-handler.service";
import { StorageProxy } from "modules/shared/services/helper/storage.service";
import { OneGlobalHeader } from "modules/global-header/one-global-header/one-global-header.component";
import { OneGlobalSidebar } from "modules/global-sidebar/one-global-sidebar/one-global-sidebar.component";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { Principal } from "modules/shared/services/helper/principal.service";
import { SessionService } from "modules/shared/services/helper/session.service";
import { LocalizationApiService } from "modules/settings/services/apis/localization-api.service";
import { MessageApiService } from "modules/settings/services/apis/message-api.service";
import { SettingSmtpApiService } from "./modules/settings/services/apis/setting-smtp-api.service";
import { SettingSmtpAdapterService } from "./modules/settings/services/adapters/setting-smtp-adapter.service";
import { UsersTableComponent } from "adminComponent/users/table/users-table.component";
import { BulkImportTableComponent } from "adminComponent/bulk-import/table/bulk-import-table.component";
import { BulkImportTemplatesComponent } from "adminComponent/bulk-import/templates/bulk-import-templates.component";
import { ExternalUserEnableModal } from "adminComponent/external-user-enable-modal/external-user-enable-modal.component";
import { OrgGroupEdit } from "adminComponent/orgs/org-group-edit.component";
import { OrgGroupCreate } from "adminComponent/orgs/org-group-create.component";
import { OrgTreeManageViewComponent } from "adminComponent/orgs/org-tree-manage-view.component";
import { OrgTreeViewComponent } from "adminComponent/orgs/org-tree-view.component";
import { UserGroupsListComponent } from "adminComponent/user-groups/list/user-groups-list.component";
import { UserGroupsCreateModal } from "adminComponent/user-groups/manage/user-groups-create-modal.component";
import { UserGroupsAddUsersModal } from "adminComponent/user-groups/manage/user-groups-add-users-modal.component";
import { UserGroupsManageUsersList } from "adminComponent/user-groups/manage/user-groups-manage-users-list.component";
import { RolesMgmtListComponent } from "adminComponent/roles/list/roles-mgmt-list.component";
import { RolesImportModal } from "adminComponent/roles/roles-import-modal/roles-import-modal.component";
import { RolesReplaceModal } from "adminComponent/roles/roles-replace-modal/roles-replace-modal.component";
import { Wootric } from "modules/shared/services/provider/wootric.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// VRM
import { VendorUploadDocumentComponent } from "modules/vendor/shared/components/vendor-upload-document/vendor-upload-document.component";
import { VendorSelectServicesComponent } from "modules/vendor/shared/components/vendor-select-services/vendor-select-services.component";
import { VendorInformationComponent } from "modules/vendor/shared/components/vendor-information/vendor-information.component";
import { VendorDashboardComponent } from "modules/vendor/shared/components/vendor-dashboard/vendor-dashboard.component";
import { VendorControlsListDetailsComponent } from "modules/vendor/vendor-controls/vendor-controls-list/vendor-controls-list-details/vendor-controls-list-details.component";
import { VendorRulesComponent } from "modules/vendor/rules/vendor-rules.component";
import { ContractListComponent } from "modules/vendor/contracts/contract-list/contract-list.component";
import { EngagementListComponent } from "modules/vendor/engagements/engagement-list/engagement-list.component";
import { AddEngagementWizardComponent } from "modules/vendor/engagements/add-engagement-wizard/add-engagement-wizard.component";
import { EngagementDetailComponent } from "modules/vendor/engagements/engagement-detail/engagement-detail.component";

// Link-controls
import { LinkControlsListDetailComponent } from "modules/link-controls/link-controls-detail/components/link-controls-list-detail/link-controls-list-detail.component";

// Contracts
import { ContractDetailsComponent } from "contracts/contract-details/contract-details.component";
import { ContractRelatedListComponent } from "contracts/contract-details/contract-related-list/contract-related-list.component";
import { AddContractComponent } from "contracts/contract-list/add-contract/add-contract.component";

// Vendorpedia Exchange
import { VendorpediaExchangeComponent } from "vendorpediaExchange/vendorpedia-exchange.component";
import { VendorpediaPreviewComponent } from "vendorpediaExchange/vendorpedia-preview/vendorpedia-preview.component";

// Vendorpedia Requests
import { VendorpediaRequestComponent } from "vendorpediaRequest/vendorpedia-request.component";
import { VendorpediaRequestFormComponent } from "vendorpediaRequest/vendorpedia-request-form/vendorpedia-request-form.component";

// Controls
import { ControlsLibraryComponent } from "controls/controls-library/controls-library.component";
import { ControlPageComponent } from "controls/control-page/control-page.component";

import { LaunchAssessmentModalComponent } from "generalcomponent/Modals/launch-assessment-modal/launch-assessment-modal.component";
import { OneHorseshoeGraph } from "sharedModules/components/one-horseshoe-graph/one-horseshoe-graph.component";
import { OneModalBase } from "sharedModules/components/one-modal-base/one-modal-base.component";
import { OneAnchorButton } from "sharedModules/components/one-anchor-button/one-anchor-button.component";
import { OrgSelect } from "sharedModules/components/org-select/org-select.component";
import { OneEmptyState } from "sharedModules/components/one-empty-state/one-empty-state.component";
import { OneNotificationModalComponent } from "sharedModules/components/one-notification-modal/one-notification-modal.component";
import { OneButtonToggleComponent } from "sharedModules/components/one-button-toggle/one-button-toggle.component";
import { ColumnSelectorModalComponent } from "sharedModules/components/column-selector/column-selector-modal.component";
import { OtDeleteConfirmationModal } from "generalcomponent/Modals/ot-delete-confirmation-modal/ot-delete-confirmation-modal.component";
import { AppMaintenanceAlertsModal } from "sharedModules/components/app-maintenance-alerts-modal/app-maintenance-alerts-modal.component";
import { RiskSelectorComponent } from "sharedModules/components/risk-selector/risk-selector.component";
import { AppMaintenanceAlertsService } from "sharedModules/services/api/app-maintenance-alerts.service";
import { BannerAlertService } from "sharedModules/services/api/banner-alert.service";
import { HelpDialogModal } from "sharedModules/components/help-dialog-modal/help-dialog-modal.component";
import { OneRulesHelperService } from "modules/dsar/components/webforms/shared/services/one-rules-helper.service";
import { Content } from "sharedModules/services/provider/content.service";
import { DefaultContentService } from "sharedModules/services/provider/default-content.service";
import { OneVideoComponent } from "modules/video/components/one-video/one-video.component";
import { SwitchToggleComponent } from "sharedModules/components/switch-toggle/switch-toggle.component";
import { OneDateTimePickerComponent } from "sharedModules/components/one-date-time-picker/one-date-time-picker.component";
import { NotificationService } from "sharedModules/services/provider/notification.service";
import { SupportService } from "oneServices/support.service";
import { DownloadLandingComponent } from "sharedModules/components/download-landing/download-landing.component";

// Intg
import { IntgCredentialsComponent } from "intsystemcomponent/intg-credentials/intg-credentials.component";
import { IntgMarketplaceDetailsComponent } from "intsystemcomponent/Marketplace/Details/intg-marketplace-details.component";
import { IntgConnectionManageComponent } from "intsystemcomponent/Connections/Manage/intg-connection-manage/intg-connection-manage.component";
import { IntgMarketplaceComponent } from "intsystemcomponent/Marketplace/List/intg-marketplace/intg-marketplace.component";
import { IntgWorkflowsListComponent } from "intsystemcomponent/workflows/list/intg-workflows-list.component";
import { IntgWorkflowBuilderComponent } from "intsystemcomponent/workflows/builder/intg-workflow-builder.component";
import { IntgWorkFlowCreateModal } from "intsystemcomponent/workflows/addWorkflow/intg-workflow-create-modal.component";
import { IntgPreferencesModalComponent } from "intsystemcomponent/intg-preferences/preference-modal/intg-preference-modal.component";
import { IntgPreferencesDeleteModalComponent } from "intsystemcomponent/intg-preferences/preference-delete-modal/intg-preference-delete-modal.component";
import { IntgCustomSystemAddModal } from "intsystemcomponent/intg-add-custom-connector-modal/intg-add-custom-connector-modal.component";
import { IntgCredentialsAddModal } from "intsystemcomponent/intg-credentials-add-modal/intg-credentials-add-modal.component";
import { IntgSystemActionsAddModal } from "intsystemcomponent/intg-system-actions-add-modal/intg-system-actions-add-modal.component";
import { IntgCustomValueListComponent } from "intsystemcomponent/custom-values/list/intg-custom-value-list.component";

// Dashboard
import { DashboardWrapperComponent } from "modules/dashboard/components/dashboard-wrapper/dashboard-wrapper.component";

// Incidents Module
import { IncidentRegisterComponent } from "incidentModule/incident-register/incident-register.component";
import { IncidentDataBreachPediaComponent } from "incidentModule/incident-databreachpedia/incident-databreachpedia.component";
import { IncidentDetailComponent } from "incidentModule/incident-detail/incident-detail.component";
import { IncidentDatabreachpediaViewComponent } from "incidentModule/incident-databreachpedia/incident-databreachpedia-view/incident-databreachpedia-view.component";
import { IncidentWorkflowComponent } from "incidentModule/incident-workflow/incident-workflow.component";
import { IncidentTypesComponent } from "incidentModule/incident-types/incident-types.component";

// Settings Module
import { LocalizationEditorTableComponent } from "settingsComponents/localization-editor/localization-editor-table.component";
import { SettingsConsentComponent } from "settingsComponents/settings-consent/settings-consent.component";
import { SettingsInventoryComponent } from "settingsComponents/settings-inventory/settings-inventory.component";
import { EmailLanguageActivatorComponent } from "settingsComponents/email-language-activator/email-language-activator.component";
import { RiskSettingsPageComponent } from "modules/settings/settings-risks/risk-settings-page/risk-settings-page.component";
import { RiskHeatmapPageComponent } from "modules/settings/settings-risks/heatmap-page/risk-heatmap-page.component";
import { SettingsHelpComponent } from "settingsComponents/settings-help/settings-help.component";
import { EmailTemplateListComponent } from "settingsComponents/email-template-list/email-template-list.component";
import { EmailTemplateEditComponent } from "settingsComponents/email-template-edit/email-template-edit.component";
import { EmailTemplatePreviewModal } from "settingsComponents/email-template-preview-modal/email-template-preview-modal.component";
import { LocalizationEditorComponent } from "settingsComponents/localization-editor/localization-editor.component";
import { SettingsSmtpComponent } from "settingsComponents/settings-smtp/settings-smtp.component";
import { TimestampSettingsComponent } from "settingsComponents/settings-timestamp/timestamp-settings.component";
import { LoginHistoryComponent } from "adminComponent/login-history/login-history.component";
import { SettingsManageSubscriptionsComponent } from "settingsComponents/settings-manage-subscriptions/settings-manage-subscriptions.component";
import { SettingsLearningAndFeedbackToolComponent } from "./modules/settings/components/settings-learning-and-feedback-tool/settings-learning-and-feedback-tool.component";
import { SettingSsoComponent } from "./modules/settings/components/settings-sso/settings-sso.component";
import { SettingSsoCertificateUploadModal } from "./modules/settings/components/settings-sso/settings-sso-certificate-upload-modal.component";
import { SettingScimcomponent } from "./modules/settings/components/setting-scim/setting-scim.component";
import { SettingScimGenerateCredsModalComponent } from "settingsComponents/setting-scim/setting-scim-generate-creds-modal.component";
import { SettingsVendorManagementComponent } from "settingsComponents/settings-vendor-management/settings-vendor-management.component";

// Eula.module.ts
import { EulaModal } from "modules/eula/eula-modal/eula-modal.component";
import { EulaService } from "modules/eula/shared/eula.service";

// DSAR
import { DSARPublishFormConfirmationModal } from "modules/dsar/components/webforms/publish-confirmation-modal/dsar-publish-form-confirmation-modal.component";
import { WebFormSidebarComponent } from "dsarModule/components/webforms/sidebar/webform-sidebar.component.ts";
import { WebformApiService } from "./modules/dsar/services/webform-api.service";
import { RequestQueueStatusBar } from "modules/dsar/components/request-queue/detail-status-bar/request-queue-status-bar.component";
import { SubTasksTable } from "modules/dsar/components/sub-tasks/sub-tasks-table/sub-tasks-table.component";
import { WebformEditLanguageComponent } from "modules/dsar/components/webforms/webform-edit-language/webform-edit-language.component";
import { GrantRequestAccessComponent } from "modules/dsar/components/request-queue/grant-access/grant-request-access.component";
import { DSARRequestQueueComponent } from "modules/dsar/components/request-queue/dsar-request-queue.component";
import { DsarSaveSystemTaskModal } from "dsarModule/components/sub-tasks/dsar-save-system-task-modal/dsar-save-system-task-modal.component";
import { RequestQueueHistoryComponent } from "modules/dsar/components/request-queue/detail-history/request-queue-history.component";
import { RequestDeadlineChangeComponent } from "modules/dsar/components/request-queue/deadline-change-modal/request-deadline-change.component";
import { WorkflowsListTable } from "modules/dsar/components/workflows/list-table/dsar-workflows-list-table.component";
import { WorkflowsList } from "modules/dsar/components/workflows/list/dsar-workflows-list.component";
import { DSARWorkflowDetailComponent } from "modules/dsar/components/workflows/detail/dsar-workflow-detail.component";
import { ResponseTemplateTabs } from "modules/dsar/components/response-templates/tabs/dsar-response-templates-tabs.component";
import { DsarWorkFlowSettings } from "dsarModule/components/workflows/settings/dsar-workflow-settings.component";
import { DsarEditWorkflowComponent } from "dsarModule/components/workflows/edit-workflow/dsar-edit-workflow.component";
import { DsarResponseSubTaskModal } from "dsarModule/components/response-templates/dsar-response-sub-task-modal/dsar-response-sub-task-modal.component";
import { DsarWebformAutoPublishModal } from "dsarModule/components/webforms/dsar-webform-auto-publish-modal/dsar-webform-auto-publish-modal.component";
import { DSARWebformSettingsComponent } from "modules/dsar/components/webforms/webform-settings/dsar-webform-settings.component";
import { DsarWebformRulesComponent } from "modules/dsar/components/webforms/webform-rules/dsar-webform-rules.component";
import { RequestQueueRejectModal } from "dsarModule/components/request-queue/reject-modal/request-queue-reject-modal.component";
import { RequestQueueCompleteModal } from "dsarModule/components/request-queue/complete-modal/request-queue-complete-modal.component";
import { RequestQueueAssignModal } from "dsarModule/components/request-queue/assign-modal/request-queue-assign-modal.component";
import { RequestQueueExtendModal } from "dsarModule/components/request-queue/extend-modal/request-queue-extend-modal.component";
import { GenerateScriptModal } from "dsarModule/components/webforms/generate-script-modal/generate-script-modal.component";
import { RequestQueueSidebarDetails } from "dsarModule/components/request-queue/detail-sidebar/dsar-request-queue-detail-sidebar.component.ts";
import { RequestQueueChangeOrgModal } from "dsarModule/components/request-queue/change-org/request-queue-change-org-modal.component";
import { RequestQueueDetailPanelComponent } from "modules/dsar/components/request-queue/detail-panel/dsar-request-queue-detail-panel.component";
import { DSARRequestQueueDetailComponent } from "modules/dsar/components/request-queue/detail/dsar-request-queue-details.component";
import { WebformPreviewComponent } from "dsarModule/components/webforms/webform-preview/webform-preview.component";
import { FormFieldTypeConfigureComponent } from "dsarModule/components/webforms/sidebar/sidebar-edit/form-field-type-configure.component";
import { MFAPasswordModal } from "dsarModule/components/request-queue/mfa-password-modal/mfa-password-modal.component";
import { RequestQueueHistoryService } from "dsarservice/request-queue-history.service";
import { DsarWebformRulesApiService } from "dsarModule/services/dsar-webform-rules-api.service";
import { RequestQueueDetailService } from "dsarservice/request-queue-detail-service";
import { FormFieldConfigureComponent } from "dsarModule/components/webforms/sidebar/sidebar-edit/form-field-configure.component";
import { DSARWebFormsComponent } from "dsarModule/components/webforms/dsar-webform/dsar-webforms.component";
import { DSARSaveResponseTemplateModalComponent } from "dsarModule/components/response-templates/save-modal/dsar-response-templates-save-modal.component";
import { DsarWorkflowSubTasksComponent } from "dsarModule/components/workflows/sub-tasks/dsar-workflow-sub-tasks.component";
import { DSARRequestQueueWrapperComponent } from "dsarModule/components/request-queue/request-queue-wrapper/dsar-request-queue-wrapper.component";
import { DSARAddNewWebformComponent } from "modules/dsar/components/webforms/add-new-webform/dsar-add-new-webform.component";
import { DSARDashboardComponent } from "modules/dsar/dsar-dashboard/dashboard/dsar-dashboard.component";
import { WebformSidebarEditComponent } from "dsarModule/components/webforms/sidebar/sidebar-edit/webform-sidebar-edit.component.ts";
import { DSARWorkflowService } from "dsarservice/dsar-workflows.service";
import { WebformStore } from "modules/dsar/webform.store";
import { RequestQueueService } from "dsarservice/request-queue.service";

// Assessment Automation
import { AAAssessmentDetailComponent } from "modules/aa-detail/assessment-detail/assessment-detail.component";
import { AAListComponent } from "modules/assessment/components/aa-list/aa-list.component";
import { AABulkLaunchComponent } from "modules/assessment/components/aa-bulk-launch/aa-bulk-launch.component";
import { AATemplateSelectionComponent } from "modules/assessment/components/aa-template-selection/aa-template-selection.component";
import { AACopyAssessmentComponent } from "modules/assessment/components/aa-copy-assessment/aa-copy-assessment.component";
import { AANeedsMoreInfoModalComponent } from "modules/assessment/components/aa-needs-more-info-modal/aa-needs-more-info-modal.component";
import { AssessmentDetailRelatedDataSubjectModalComponent } from "modules/assessment/components/assessment-detail-question-related-data-subject-modal/assessment-detail-question-related-data-subject-modal.component";
import { AACommentsModalComponent } from "modules/assessment/components/aa-comments-modal/aa-comments-modal.component";
import { AAAttachmentsModalComponent } from "modules/assessment/components/aa-attachments-modal/aa-attachments-modal.component";
import { AAUnapprovalModal } from "modules/assessment/components/aa-unapproval-modal/aa-unapproval-modal.component";
import { AASendBackModalComponent } from "modules/assessment/components/aa-send-back-modal/aa-send-back-modal.component";
import { AARiskModalComponent } from "modules/assessment/components/aa-risk-modal/aa-risk-modal.component";
import { AAMetadataModalComponent } from "modules/assessment/components/aa-metadata-modal/aa-metadata-modal.component";
import { ShareAssessmentModal } from "sharedModules/components/share-assessment-modal/share-assessment-modal.component";
import { AAQuestionActivityStreamModalComponent } from "modules/assessment/components/aa-question-activity-stream/aa-question-activity-stream.component";
import { AATemplateSettingsComponent } from "settingsComponents/aa-settings/aa-template-settings/aa-template-settings.component";
import { AssessmentListApiService } from "modules/assessment/services/api/assessment-list-api.service";
import { AssessmentDetailApiService } from "modules/assessment/services/api/assessment-detail-api.service";
import { SettingsSspComponent } from "settingsComponents/settings-ssp/settings-ssp.component";
import { AAAssessmentSettingsComponent } from "settingsComponents/aa-settings/aa-assessment-settings/aa-assessment-settings.component";

// Risks
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import { RiskAdapterService } from "oneServices/adapters/risk-adapter.service";

// Consent
import { CrCollectionPointListComponent } from "consentModule/collection-points/collection-point-list/collection-point-list.component";
import { CrCollectionPointUpdatesComponent } from "consentModule/collection-points/collection-point-updates/collection-point-updates.component";
import { CrDashboardComponent } from "consentModule/dashboard/dashboard.component";
import { CrPreferenceCenterDetailsComponent } from "consentModule/preferences/preference-center-details/preference-center-details.component";
import { CrPreferenceCenterListComponent } from "consentModule/preferences/preference-center-list/preference-center-list.component";
import { CrPurposeDetailsComponent } from "consentModule/purposes/purpose-details/purpose-details.component";
import { CrPurposeCreateComponent } from "consentModule/purposes/purpose-create/purpose-create.component";
import { CrTopicListComponent } from "consentModule/topics/topic-list/topic-list.component";
import { CrTopicDetailsComponent } from "consentModule/topics/topic-details/topic-details.component";
import { CrDataSubjectDetailsComponent } from "consentModule/data-subjects/data-subject-details/data-subject-details.component";
import { CrPurposeListComponent } from "consentModule/purposes/purpose-list/purpose-list.component";
import { CrDataSubjectListComponent } from "consentModule/data-subjects/data-subject-list/data-subject-list.component";
import { CrTransactionListComponent } from "consentModule/transactions/transaction-list/transaction-list.component";
import { CrCollectionPointDetailsComponent } from "consentModule/collection-points/collection-point-details/collection-point-details.component";
import { CrCollectionPointCreateComponent } from "consentModule/collection-points/collection-point-create/collection-point-create.component";
import { CrCollectionPointTypeComponent } from "consentModule/collection-points/collection-point-type/collection-point-type.component";
import { CrWrapperComponent } from "consentModule/shared/cr-wrapper/cr-wrapper.component";
import { CrTransactionDetailsComponent } from "consentModule/transactions/transaction-details/transaction-details.component";
import { CrCustomPreferenceListComponent } from "consentModule/custom-preferences/custom-preference-list/custom-preference-list.component";
import { CrCustomPreferenceDetailsComponent } from "consentModule/custom-preferences/custom-preference-details/custom-preference-details.component";

import { SelfServicePortalComponent } from "modules/self-service-portal/ssp-page/self-service-portal/self-service-portal.component";
import { SspConfigComponent } from "modules/self-service-portal/ssp-config/ssp-config/ssp-config.component";
import { SspTemplateModalComponent } from "modules/self-service-portal/ssp-page/ssp-template-modal/ssp-template-modal.component";
import { SspConfigTemplateCardComponent } from "modules/self-service-portal/ssp-config/ssp-config-template-card/ssp-config-template-card.component";
import { SspTemplateCardComponent } from "modules/self-service-portal/ssp-page/ssp-template-card/ssp-template-card.component";
import { SspLaunchAssessmentModalComponent } from "modules/self-service-portal/ssp-page/ssp-launch-assessment-modal/ssp-launch-assessment-modal.component";
import { SSPApiService } from "modules/self-service-portal/shared/ssp-api.service";

// Risks Module
import { RiskRepoComponent } from "modules/risks/risk-repo/risk-repo.component";
import { RiskRegisterRiskDetailsComponent } from "modules/risks/risk-register-risk-details/risk-register-risk-details.component";
import { RisksWrapperComponent } from "modules/risks/risks-wrapper/risks-wrapper.component";
import { RiskCategoryTableComponent } from "modules/risks/risk-category/risk-category-table.component";

// Cookie module
import { WebsiteListTableComponent } from "generalcomponent/Cookies/website-list-table/website-list-table.component";
import { CCIABVendorDetails } from "generalcomponent/Cookies/IABVendorsList/cc-iab-vendor-details.component";
import { CCIABVendorsList } from "generalcomponent/Cookies/IABVendorsList/cc-iab-vendors-list.component";
import { TemplatesTableComponent } from "cookiesV2Module/components/templates/cc-templates-table/cc-templates-table.component";
import { AddTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/add-template-modal.component";
import { PublishTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/publish-template-modal.component";
import { AssignTemplateComponent } from "generalcomponent/Cookies/Shared/Modal/cc-assign-template-modal/assign-template.component";
import { TemplatesTabComponent } from "cookiesV2Module/components/templates/cc-template-tab/cc-template-tab.component";
import { EditTemplateModalController } from "generalcomponent/Cookies/Shared/Modal/add-template-modal/edit-template-modal.component";
import { TemplateNewLanguageComponent } from "generalcomponent/Cookies/Shared/Modal/template-new-language/template-new-language.component";
import { ScanBehindTutorialComponent } from "generalcomponent/Cookies/Shared/Modal/cc-scan-login-modal/scan-behind-tutorial-modal.component";
import { CcPreferenceCenterIabEditDetails } from "generalcomponent/Cookies/PreferenceCenter/cc-preference-center-iab-edit-details.component";
import { AddCustomCookieModal } from "modules/cookies/components/policy/add-custom-cookie-modal/add-custom-cookie-modal.component";
import { OrgGroupReAssignModal } from "modules/cookies/components/website-list/org-group-re-assign-modal/org-group-re-assign-modal.component";
import { CCGroupModal } from "modules/cookies/components/policy/cc-group-modal/cc-group-modal.component";
import { RemoveCookieModal } from "modules/cookies/components/policy/remove-cookie-modal/remove-cookie-modal.component";
import { CookieDeleteConfirmationComponent } from "modules/cookies/components/policy/cookie-delete-confirmation-modal/cookie-delete-confirmation-modal.component";
import { ScriptIntegrationSettings } from "modules/cookies/components/script-integration/script-integration-settings/script-integration-settings-component";
import { PolicyConfiguration } from "modules/cookies/components/policy/policy-configuration/policy-configuration.component";
import { CcLanguageSelectionComponent } from "generalcomponent/Cookies/Shared/cc-upgrade-language-selection-component";
import { CcLanguageModal } from "generalcomponent/Cookies/Shared/Modal/cc-language-modal.component";
import { AddVendorListModal } from "modules/cookies/components/iab/add-vendor-modal/cc-add-vendor-list-modal.component";
import { CookiePublishScriptModal } from "cookiesV2Module/components/shared/cookie-publish-script-modal/cookie-publish-script-modal.component";
import { RestoreScriptModal } from "cookiesV2Module/components/script-archive/restore-script-modal/restore-script-modal.component";
import { ScheduleAuditModal } from "modules/cookies/components/website-list/schedule-audit/cc-schedule-audit-modal.component";
import { ConsentPolicyList } from "modules/cookies/components/consent-policy/consent-policy-list/consent-policy-list.component";
import { ConsentPolicyDetailModal } from "modules/cookies/components/consent-policy/consent-policy-detail-modal/consent-policy-detail-modal.component";
import { ConsentPolicyDetailWrapper } from "modules/cookies/components/consent-policy/consent-policy-detail-wrapper/consent-policy-detail-wrapper.component";
import { ConsentPolicyStatusModal } from "modules/cookies/components/consent-policy/consent-status-modal/consent-status-modal.component";
import { CookieEmptyState } from "modules/cookies/components/shared/cookie-empty-state/cookie-empty-state.component";
import { CookieLimitModal } from "modules/cookies/components/policy/cookie-limit-modal/cookie-limit-modal.component";
import { ScriptArchiveWrapper } from "cookiesV2Module/components/script-archive/script-archive-wrapper/script-archive-wrapper.component";
import { ThirdPartyBannerNotification } from "modules/cookies/components/shared/notification/thirdparty-banner-notification.component";
import { ScanLoginDetail } from "modules/cookies/components/website-list/scan-login/scan-login-detail/scan-login-detail.component";
import { PreferenceCenterPreviewComponent } from "modules/cookies/components/preference-center/preference-center-preview/preference-center-preview.component";
import { CcBannerWrapperComponent } from "cookiesV2Module/components/banner/cc-banner-wrapper/cc-banner-wrapper.component";

// Cookies- V2 components
import { CCCategorizationsWrapper } from "cookiesV2Module/components/cc-categorizations/cc-categorizations-wrapper/cc-categorizations-wrapper.component";

// Cookie services
import { CookieApiService } from "modules/cookies/shared/services/cookie-api.service";
import { CookieSideMenuHelperService } from "modules/cookies/services/helper/cookie-side-menu-helper.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

// Inventory Module
import { InventoryListWrapper } from "modules/inventory/components/inventory-list-wrapper/inventory-list-wrapper.component";
import { InventoryDetails } from "modules/inventory/components/inventory-details/inventory-details.component";
import { RelateInventoryModalComponent } from "generalcomponent/Modals/relate-inventory-modal/relate-inventory-modal.component";
import { LinkInventoryModalComponent } from "generalcomponent/Modals/link-inventory-modal/link-inventory-modal.component";
import { RelateDataSubjectModalComponent } from "generalcomponent/Modals/relate-data-subject-modal/relate-data-subject-modal.component";
import { LinkDataSubjectModalComponent } from "generalcomponent/Modals/link-data-subject-modal/link-data-subject-modal.component";
import { AddInventoryModalComponent } from "generalcomponent/Modals/add-inventory-modal/add-inventory-modal.component";
import { CopyInventoryModalComponent } from "generalcomponent/Modals/copy-inventory-modal/copy-inventory-modal.component";
import { CopyInventoryV2ModalComponent } from "generalcomponent/Modals/copy-inventory-v2-modal/copy-inventory-v2-modal.component";
import { AttributeManagerComponent } from "modules/attribute-manager/components/attribute-manager/attribute-manager.component";
import { AttributeManagerEntryComponent } from "modules/attribute-manager/components/attribute-manager-entry/attribute-manager-entry.component";
import { InventoryListManagerEntryComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager-entry/inventory-list-manager-entry.component";
import { InventoryListManagerDetailsComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager-details/inventory-list-manager-details.component";
import { InventoryListManagerComponent } from "inventoryModule/inventory-list-manager/shared/components/inventory-list-manager/inventory-list-manager.component";
import { InventoryManageVisibilityWrapperComponent } from "inventoryModule/inventory-manage-visibility/shared/components/inventory-manage-visibility-wrapper/inventory-manage-visibility-wrapper.component";
import { AttributeManagerDetailsComponent } from "modules/attribute-manager/components/attribute-manager-details/attribute-manager-details.component";
import { AttributeManagerModalComponent } from "modules/attribute-manager/components/attribute-manager-modal/attribute-manager-modal.component";
import { EditAttributeOptionModalComponent } from "modules/attribute-manager/components/edit-attribute-option-modal/edit-attribute-option-modal.component";
import { AttributeOptionStatusModalComponent } from "modules/attribute-manager/components/attribute-option-status-modal/attribute-option-status-modal.component";
import { InventoryBulkEditComponent } from "modules/inventory/components/inventory-bulk-edit/inventory-bulk-edit.component";
import { AttributeStatusModalComponent } from "modules/attribute-manager/components/attribute-status-modal/attribute-status-modal.component";
import { BulkEditConfirmationModalComponent } from "modules/inventory/components/bulk-edit-confirmation-modal/bulk-edit-confirmation-modal.component";
import { AACreateFormComponent } from "modules/assessment/components/aa-create-form/aa-create-form.component";
import { AttributeGroupModal } from "modules/attribute-manager/components/attribute-group-modal/attribute-group-modal.component";
import { AttributePickerModal } from "modules/attribute-manager/components/attribute-picker-modal/attribute-picker-modal.component";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryRiskDetailsComponent } from "modules/inventory/inventory-risk-details/inventory-risk-details.component";

// Datamapping Module
import { DMDataMaps } from "modules/data-mapping/components/data-maps/dm-data-maps.component";
import { DMWelcome } from "modules/data-mapping/components/welcome/dm-welcome.component";
import { DMOneDataMaps } from "modules/data-mapping/services/dm-one-data-maps.service";
import { SectionEditModalComponent } from "generalcomponent/Modals/section-edit-modal/section-edit-modal.component";
import { DMTemplateService } from "datamappingservice/Template/dm-template.service";
import { InventoryRiskModalViewLogicService } from "oneServices/view-logic/inventory-risk-modal-view-logic.service";
import { SendBackModalComponent } from "modules/data-mapping/components/send-back-modal/send-back-modal.component";

// Templates Module
import { EditTemplateRuleModalComponent, TemplateRuleService } from "modules/template/components/template-page/template-rule-list/template-rule";
import { TemplateRelatedInventoryModalComponent } from "modules/template/components/template-related-inventory-modal/template-related-inventory-modal.component";
import { TemplateQuestionBuilderModalComponent } from "modules/template/components/question-builder-modal/template-question-builder-modal.component";
import { EditTemplateSectionModalComponent } from "modules/template/components/edit-template-section-modal/edit-template-section-modal.component";
import { EditTemplateWelcomeMessage } from "modules/template/components/edit-template-welcome-message-modal/edit-template-welcome-message-modal.component";
import { TemplateConditionsModalComponent } from "modules/template/components/template-conditions-modal/template-conditions-modal.component";
import { QuestionAttributeModalComponent } from "modules/template/components/question-attribute-modal/question-attribute-modal.component";
import { ConfigureRelationshipModalComponent } from "modules/template/components/configure-relationship-modal/configure-relationship-modal.component";
import { TemplateCreateFormComponent } from "modules/template/components/template-create-form/template-create-form.component";
import { DataDiscoveryComponent } from "modules/intg-shared/components/intg-data-discovery.component";
import { QuestionTypeService } from "modules/template/services/question-type.service";
import { IncidentSchemaApiService } from "modules/template/services/incident-schema-api.service";
import { BulkImportService } from "modules/admin/services/bulk-import/bulk-import.service";

// Reports Module
import { PDFReportComponent } from "reportsModule/pdf-reports/components/pdf-report/pdf-report.component";
import { ReportsListComponent } from "reportsModule/reports-list/components/reports-list/reports-list.component";
import { ColumnReportComponent } from "reportsModule/column-reports/components/column-report/column-report.component";
import { ReportsTemplateSelectComponent } from "reportsModule/reports-list/components/reports-template-select/reports-template-select.component";
import { DashboardListComponent } from "reportsModule/dashboard/dashboard-list/components/dashboard-list/dashboard-list.component";
import { DashboardEditComponent } from "reportsModule/dashboard/dashboard-edit/components/dashboard-edit/dashboard-edit.component";
import { ReportAction } from "reportsModule/reports-list/services/report-action.service";
import { DashboardSwitcherComponent } from "reportsModule/dashboard/dashboard-view/components/dashboard-switcher/dashboard-switcher.component";

// Report Schedule Module
import { ReportScheduleListComponent } from "reportScheduleModule/components/report-schedule-list/report-schedule-list.component";

// global-readiness-legacy-module
import { GlobalReadinessAssessmentsListComponent } from "modules/global-readiness/components/assessments-list/global-readiness-assessments-list.component";
import { GlobalReadinessDashboardComponent } from "modules/global-readiness/components/dashboard/global-readiness-dashboard.component";
import { GlobalReadinessWelcomeComponent } from "modules/global-readiness/components/welcome/global-readiness-welcome.component";
import { GlobalReadinessLaunchComponent } from "modules/global-readiness/components/global-readiness-launch/global-readiness-launch.component";
import { GlobalReadinessAssessmentReport } from "modules/global-readiness/components/assessment-report/global-readiness-assessment-report.component";
import { GRAStartAssessmentModalComponent } from "modules/global-readiness/components/start-assessment-modal/gra-start-assessment-modal.component";
import { GRAShowGuidanceModalComponent } from "modules/global-readiness/components/gra-show-guidance-modal/gra-show-guidance-modal.component";

import { PreferenceCenterEditDetails } from "modules/cookies/components/preference-center/preference-center-details/preference-center-details.component";
import { PreferenceCenterStyling } from "modules/cookies/components/preference-center/preference-center-styling/preference-center-styling.component";
import { TemplatePageComponent } from "modules/template/components/template-page/template-page.component";
import { CustomTemplatesComponent } from "modules/template/components/custom-templates/custom-templates.component";
import { GalleryTemplatesComponent } from "modules/template/components/gallery-templates/gallery-templates.component";

// Onboarding
import { OnboardingModal } from "modules/onboarding/components/onboarding-modal/onboarding-modal.component";
import { OnboardingService } from "modules/onboarding/services/onboarding.service";
import { IntgConnectionsListComponent } from "intsystemcomponent/Connections/List/intg-connections-list.component";

// Welcome
import { WelcomePageComponent } from "modules/welcome/welcome-page/welcome-page.component";
import { WelcomeDetailComponent } from "modules/welcome/welcome-detail/welcome-detail.component";

// Preference
import { PreferencesChangePasswordComponent } from "modules/preferences/components/preferences-change-password/preferences-change-password.component";
import { PreferencesLearningAndFeedbackTools } from "modules/preferences/components/preferences-learning-and-feedback-tools/preferences-learning-and-feedback-tools.component";

// Policy Module
import { PolicyWrapperComponent } from "modules/policy/shared/policy-wrapper/policy-wrapper.component";
import { PolicyTable } from "modules/policy/shared/policy-table/policy-table.component";
import { PrivacyNoticeListComponent } from "modules/policy/privacy-notice/privacy-notice-list/privacy-notice-list.component";
import { CreatePrivacyNoticeModalComponent } from "modules/policy/privacy-notice/privacy-notice-list/create-privacy-notice-modal/create-privacy-notice-modal.component";
import { PrivacyNoticeDetailsComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-details.component";
import { PrivacyNoticeHeaderComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-header/privacy-notice-header.component";
import { PrivacyNoticeBuilderComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-builder/privacy-notice-builder.component";
import { PrivacyNoticeBuilderSidebarComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-builder/privacy-notice-builder-sidebar/privacy-notice-builder-sidebar.component";
import { PrivacyNoticeIntegrationsComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-integrations/privacy-notice-integrations.component";
import { PrivacyNoticePublishScriptModalComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-publish-script-modal/privacy-notice-publish-script-modal.component";
import { PrivacyNoticeVersionsComponent } from "modules/policy/privacy-notice/privacy-notice-details/privacy-notice-versions/privacy-notice-versions.component";

export function downgrade() {
    angular.module("zen")

        // Router
        .service("OmniService", downgradeInjectable(OmniService))
        .service("AuthEntryService", downgradeInjectable(AuthEntryService))
        .service("OneProtocol", downgradeInjectable(ProtocolService))
        .service("HybridRouter", downgradeInjectable(HybridRouter))

        // global
        .directive("oneGlobalHeader", downgradeComponent({ component: OneGlobalHeader }))
        .directive("oneGlobalSidebar", downgradeComponent({ component: OneGlobalSidebar }))
        .service("GlobalSidebarService", downgradeInjectable(GlobalSidebarService))
        .service("Favicons", downgradeInjectable(Favicons))

        // Welcome
        .directive("downgradedWelcomePage", downgradeComponent({ component: WelcomePageComponent }))
        .directive("downgradedWelcomeDetail", downgradeComponent({ component: WelcomeDetailComponent }))

        // Preference
        .directive("downgradePreferencesChangePassword", downgradeComponent({ component: PreferencesChangePasswordComponent }))
        .directive("downgradePreferencesLearningAndFeedbackTools", downgradeComponent({ component: PreferencesLearningAndFeedbackTools }))

        // admin-legacy.module
        .directive("usersTable", downgradeComponent({ component: UsersTableComponent }) as angular.IDirectiveFactory)
        .directive("externalUserEnableModal", downgradeComponent({ component: ExternalUserEnableModal }) as angular.IDirectiveFactory)
        .directive("orgGroupEdit", downgradeComponent({ component: OrgGroupEdit }) as angular.IDirectiveFactory)
        .directive("orgGroupCreate", downgradeComponent({ component: OrgGroupCreate }) as angular.IDirectiveFactory)
        .directive("orgTreeManageView", downgradeComponent({ component: OrgTreeManageViewComponent }) as angular.IDirectiveFactory)
        .directive("orgTreeView", downgradeComponent({ component: OrgTreeViewComponent }) as angular.IDirectiveFactory)
        .directive("userGroupsList", downgradeComponent({ component: UserGroupsListComponent }) as angular.IDirectiveFactory)
        .directive("userGroupsCreateModal", downgradeComponent({ component: UserGroupsCreateModal }) as angular.IDirectiveFactory)
        .directive("userGroupsAddUsersModal", downgradeComponent({ component: UserGroupsAddUsersModal }) as angular.IDirectiveFactory)
        .directive("userGroupsManageUsersList", downgradeComponent({ component: UserGroupsManageUsersList }) as angular.IDirectiveFactory)
        .directive("downgradeBulkImportTable", downgradeComponent({ component: BulkImportTableComponent }) as angular.IDirectiveFactory)
        .directive("downgradeBulkImportTemplates", downgradeComponent({ component: BulkImportTemplatesComponent }) as angular.IDirectiveFactory)
        .service("bulkImportService", downgradeInjectable(BulkImportService))

        // vendor-legacy.module
        .directive("downgradeVendorpediaExchange", downgradeComponent({ component: VendorpediaExchangeComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorUploadDocument", downgradeComponent({ component: VendorUploadDocumentComponent }) as angular.IDirectiveFactory)
        .directive("downgradeContractDetails", downgradeComponent({ component: ContractDetailsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeContractRelatedListComponent", downgradeComponent({ component: ContractRelatedListComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorSelectServices", downgradeComponent({ component: VendorSelectServicesComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorInformation", downgradeComponent({ component: VendorInformationComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorpediaPreview", downgradeComponent({ component: VendorpediaPreviewComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorControlsListDetails", downgradeComponent({ component: VendorControlsListDetailsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorDashboard", downgradeComponent({ component: VendorDashboardComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorpediaRequest", downgradeComponent({ component: VendorpediaRequestComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorpediaRequestNew", downgradeComponent({ component: VendorpediaRequestFormComponent }) as angular.IDirectiveFactory)
        .directive("downgradeVendorRules", downgradeComponent({ component: VendorRulesComponent }) as angular.IDirectiveFactory)
        .directive("downgradeContractList", downgradeComponent({ component: ContractListComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAddContract", downgradeComponent({ component: AddContractComponent }) as angular.IDirectiveFactory)
        .directive("downgradeEngagementList", downgradeComponent({ component: EngagementListComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAddEngagementWizard", downgradeComponent({ component: AddEngagementWizardComponent }) as angular.IDirectiveFactory)
        .directive("downgradeEngagementDetail", downgradeComponent({ component: EngagementDetailComponent }) as angular.IDirectiveFactory)

        // link-controls-legacy.module
        .directive("linkControlsListDetail", downgradeComponent({ component: LinkControlsListDetailComponent }) as angular.IDirectiveFactory)

        // controls-legacy.module
        .directive("downgradeControlsLibrary", downgradeComponent({ component: ControlsLibraryComponent }) as angular.IDirectiveFactory)
        .directive("downgradeControlPage", downgradeComponent({ component: ControlPageComponent }) as angular.IDirectiveFactory)

        // main app.js
        .directive("launchAssessmentModal", downgradeComponent({ component: LaunchAssessmentModalComponent }))
        .directive("oneHorseshoeGraph", downgradeComponent({ component: OneHorseshoeGraph }))
        .directive("oneModalBase", downgradeComponent({ component: OneModalBase }))
        .directive("orgSelect", downgradeComponent({ component: OrgSelect }))
        .directive("oneEmptyState", downgradeComponent({ component: OneEmptyState }))
        .directive("downgradeOneAnchorButton", downgradeComponent({ component: OneAnchorButton }))
        .directive("oneNotificationModalComponent", downgradeComponent({ component: OneNotificationModalComponent }))
        .directive("oneButtonToggle", downgradeComponent({ component: OneButtonToggleComponent }))
        .directive("columnSelectorModal", downgradeComponent({ component: ColumnSelectorModalComponent }))
        .directive("otDeleteConfirmationModal", downgradeComponent({ component: OtDeleteConfirmationModal }))
        .directive("appMaintenanceAlertsModal", downgradeComponent({ component: AppMaintenanceAlertsModal }))
        .directive("riskSelector", downgradeComponent({ component: RiskSelectorComponent }))
        .directive("downgradeOneVideo", downgradeComponent({ component: OneVideoComponent }))
        .directive("downgradeSwitchToggle", downgradeComponent({ component: SwitchToggleComponent }))
        .directive("downgradeOneDateTimePicker", downgradeComponent({ component: OneDateTimePickerComponent }))
        .directive("rolesMgmtList", downgradeComponent({ component: RolesMgmtListComponent }))
        .directive("rolesImportModal", downgradeComponent({ component: RolesImportModal }))
        .directive("rolesReplaceModal", downgradeComponent({ component: RolesReplaceModal }))
        .directive("helpDialogModal", downgradeComponent({ component: HelpDialogModal }))
        .directive("downloadLanding", downgradeComponent({ component: DownloadLandingComponent }))
        .service("oneRulesHelperService", downgradeInjectable(OneRulesHelperService))
        .service("AlertBannerService", downgradeInjectable(AlertBannerService))
        .service("BannerAlertService", downgradeInjectable(BannerAlertService))
        .service("AppMaintenanceAlertsService", downgradeInjectable(AppMaintenanceAlertsService))
        .service("Content", downgradeInjectable(Content))
        .service("DefaultContent", downgradeInjectable(DefaultContentService))
        .service("NotificationService", downgradeInjectable(NotificationService))
        .service("Permissions", downgradeInjectable(Permissions))
        .service("PRINCIPAL", downgradeInjectable(Principal))
        .service("Session", downgradeInjectable(SessionService))
        .service("AuthHandlerService", downgradeInjectable(AuthHandlerService))
        .service("StorageProxy", downgradeInjectable(StorageProxy))
        .service("LoadingService", downgradeInjectable(LoadingService))
        .service("Wootric", downgradeInjectable(Wootric))
        .service("SupportService", downgradeInjectable(SupportService))
        .service("McmModalService", downgradeInjectable(McmModalService))
        .service("LocalizationApiService", downgradeInjectable(LocalizationApiService))
        .service("EulaService", downgradeInjectable(EulaService))
        .service("MessageApiService", downgradeInjectable(MessageApiService))
        .service("SettingSmtpAdapterService", downgradeInjectable(SettingSmtpAdapterService))
        .service("SettingSmtpApi", downgradeInjectable(SettingSmtpApiService))

        // intg-legacy.module
        .directive("intgMarketplace", downgradeComponent({ component: IntgMarketplaceComponent }))
        .directive("intgMarketplaceDetails", downgradeComponent({ component: IntgMarketplaceDetailsComponent }))
        .directive("downgradeIntgConnectionsList", downgradeComponent({ component: IntgConnectionsListComponent }))
        .directive("intgConnectionManage", downgradeComponent({ component: IntgConnectionManageComponent }))
        .directive("intgWorkflowsList", downgradeComponent({ component: IntgWorkflowsListComponent }))
        .directive("intgWorkflowBuilder", downgradeComponent({ component: IntgWorkflowBuilderComponent }))
        .directive("intgCredentials", downgradeComponent({ component: IntgCredentialsComponent }))
        .directive("intgWorkFlowCreateModal", downgradeComponent({ component: IntgWorkFlowCreateModal }))
        .directive("intgPreferencesModalComponent", downgradeComponent({ component: IntgPreferencesModalComponent }))
        .directive("intgPreferencesDeleteModalComponent", downgradeComponent({ component: IntgPreferencesDeleteModalComponent }))
        .directive("intgCustomSystemAddModal", downgradeComponent({ component: IntgCustomSystemAddModal }) as angular.IDirectiveFactory)
        .directive("intgCredentialsAddModal", downgradeComponent({ component: IntgCredentialsAddModal }))
        .directive("intgSystemActionsAddModal", downgradeComponent({ component: IntgSystemActionsAddModal }))
        .directive("intgCustomValuesList", downgradeComponent({ component: IntgCustomValueListComponent }))

        // dashboard-legacy.module
        .directive("downgradeDashboardWrapper", downgradeComponent({ component: DashboardWrapperComponent }) as angular.IDirectiveFactory)

        // incident-legacy.module
        .directive("downgradeIncidentRegister", downgradeComponent({ component: IncidentRegisterComponent }) as angular.IDirectiveFactory)
        .directive("downgradeIncidentDatabreachpedia", downgradeComponent({ component: IncidentDataBreachPediaComponent }) as angular.IDirectiveFactory)
        .directive("downgradeIncidentDatabreachpediaView", downgradeComponent({ component: IncidentDatabreachpediaViewComponent }) as angular.IDirectiveFactory)
        .directive("downgradeIncidentDetail", downgradeComponent({ component: IncidentDetailComponent }) as angular.IDirectiveFactory)
        .directive("downgradeIncidentWorkflow", downgradeComponent({ component: IncidentWorkflowComponent }) as angular.IDirectiveFactory)
        .directive("downgradeIncidentTypes", downgradeComponent({ component: IncidentTypesComponent }) as angular.IDirectiveFactory)

        // eula-legacy.module
        .directive("eulaModal", downgradeComponent({ component: EulaModal }) as angular.IDirectiveFactory)

        // settings-legacy.module
        .directive("localizationEditorTable", downgradeComponent({ component: LocalizationEditorTableComponent }) as angular.IDirectiveFactory)
        .directive("settingsConsent", downgradeComponent({ component: SettingsConsentComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingsInventory", downgradeComponent({ component: SettingsInventoryComponent }) as angular.IDirectiveFactory)
        .directive("emailLanguageActivator", downgradeComponent({ component: EmailLanguageActivatorComponent }) as angular.IDirectiveFactory)
        .directive("riskSettingsPage", downgradeComponent({ component: RiskSettingsPageComponent }) as angular.IDirectiveFactory)
        .directive("riskHeatmapPage", downgradeComponent({ component: RiskHeatmapPageComponent }) as angular.IDirectiveFactory)
        .directive("emailTemplateList", downgradeComponent({ component: EmailTemplateListComponent }) as angular.IDirectiveFactory)
        .directive("emailTemplateEdit", downgradeComponent({ component: EmailTemplateEditComponent }) as angular.IDirectiveFactory)
        .directive("emailTemplatePreviewModal", downgradeComponent({ component: EmailTemplatePreviewModal }) as angular.IDirectiveFactory)
        .directive("localizationEditor", downgradeComponent({ component: LocalizationEditorComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingsHelp", downgradeComponent({ component: SettingsHelpComponent }) as angular.IDirectiveFactory)
        .directive("downgradeTimestampSettings", downgradeComponent({ component: TimestampSettingsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeLoginHistoryComponent", downgradeComponent({ component: LoginHistoryComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingsSmtp", downgradeComponent({ component: SettingsSmtpComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingsManageSubscriptions", downgradeComponent({ component: SettingsManageSubscriptionsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingsLearningAndFeedbackTool", downgradeComponent({ component: SettingsLearningAndFeedbackToolComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingSso", downgradeComponent({ component: SettingSsoComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingSsoCertificateModal", downgradeComponent({ component: SettingSsoCertificateUploadModal }) as angular.IDirectiveFactory)
        .directive("downgradeSettingScim", downgradeComponent({ component: SettingScimcomponent }) as angular.IDirectiveFactory)
        .directive("downgradeSettingScimGenerateCredsModal", downgradeComponent({ component: SettingScimGenerateCredsModalComponent }))
        .directive("downgradeSettingsVendorManagement", downgradeComponent({ component: SettingsVendorManagementComponent }))

        // dsar-legacy.module.ts
        .directive("dsarPublishFormConfirmationModal", downgradeComponent({ component: DSARPublishFormConfirmationModal }) as angular.IDirectiveFactory)
        .directive("webformSidebar", downgradeComponent({ component: WebFormSidebarComponent }))
        .directive("dsarRequestQueueStatusBar", downgradeComponent({ component: RequestQueueStatusBar }) as angular.IDirectiveFactory)
        .directive("dsarSubTasksTable", downgradeComponent({ component: SubTasksTable }) as angular.IDirectiveFactory)
        .directive("webformEditLanguageBody", downgradeComponent({ component: WebformEditLanguageComponent }) as angular.IDirectiveFactory)
        .directive("grantRequestAccess", downgradeComponent({ component: GrantRequestAccessComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDsarRequestQueue", downgradeComponent({ component: DSARRequestQueueComponent }) as angular.IDirectiveFactory)
        .directive("requestQueueHistory", downgradeComponent({ component: RequestQueueHistoryComponent }) as angular.IDirectiveFactory)
        .directive("requestDeadlineChange", downgradeComponent({ component: RequestDeadlineChangeComponent }) as angular.IDirectiveFactory)
        .directive("dsarWorkflowsListTable", downgradeComponent({ component: WorkflowsListTable }) as angular.IDirectiveFactory)
        .directive("dsarWorkflowsList", downgradeComponent({ component: WorkflowsList }) as angular.IDirectiveFactory)
        .directive("downgradeDsarWorkflowDetail", downgradeComponent({ component: DSARWorkflowDetailComponent }) as angular.IDirectiveFactory)
        .directive("downgradeResponseTemplatesTabs", downgradeComponent({ component: ResponseTemplateTabs }) as angular.IDirectiveFactory)
        .directive("dsarSaveResponseTemplateModal", downgradeComponent({ component: DSARSaveResponseTemplateModalComponent }) as angular.IDirectiveFactory)
        .directive("dsarWorkflowSettings", downgradeComponent({ component: DsarWorkFlowSettings }) as angular.IDirectiveFactory)
        .directive("dsarEditWorkflow", downgradeComponent({ component: DsarEditWorkflowComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDsarResponseSubTaskModal", downgradeComponent({ component: DsarResponseSubTaskModal }) as angular.IDirectiveFactory)
        .directive("downgradeDsarWebformAutoPublishModal", downgradeComponent({ component: DsarWebformAutoPublishModal }) as angular.IDirectiveFactory)
        .directive("downgradeDsarWebformSettings", downgradeComponent({ component: DSARWebformSettingsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDsarSaveSystemTaskModal", downgradeComponent({ component: DsarSaveSystemTaskModal }) as angular.IDirectiveFactory)
        .directive("downgradeDsarWorkflowSubTasks", downgradeComponent({ component: DsarWorkflowSubTasksComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDsarWebformRules", downgradeComponent({ component: DsarWebformRulesComponent }) as angular.IDirectiveFactory)
        .directive("downgradeRequestQueueRejectModal", downgradeComponent({ component: RequestQueueRejectModal }) as angular.IDirectiveFactory)
        .directive("downgradeRequestQueueCompleteModal", downgradeComponent({ component: RequestQueueCompleteModal }) as angular.IDirectiveFactory)
        .directive("downgradeRequestQueueAssignModal", downgradeComponent({ component: RequestQueueAssignModal }) as angular.IDirectiveFactory)
        .directive("downgradeGenerateScriptModal", downgradeComponent({ component: GenerateScriptModal }) as angular.IDirectiveFactory)
        .directive("downgradeRequestQueueDetailSidebar", downgradeComponent({ component: RequestQueueSidebarDetails }) as angular.IDirectiveFactory)
        .directive("requestQueueExtendModal", downgradeComponent({ component: RequestQueueExtendModal }))
        .directive("requestQueueChangeOrgModal", downgradeComponent({ component: RequestQueueChangeOrgModal }) as angular.IDirectiveFactory)
        .directive("downgradeRequestQueueDetailPanel", downgradeComponent({ component: RequestQueueDetailPanelComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDsarRequestQueueDetail", downgradeComponent({ component: DSARRequestQueueDetailComponent }) as angular.IDirectiveFactory)
        .directive("webformPreview", downgradeComponent({ component: WebformPreviewComponent }) as angular.IDirectiveFactory)
        .directive("formFieldTypeConfigure", downgradeComponent({ component: FormFieldTypeConfigureComponent }))
        .directive("downgradeMfaPasswordModal", downgradeComponent({ component: MFAPasswordModal }) as angular.IDirectiveFactory)
        .directive("formFieldConfigure", downgradeComponent({ component: FormFieldConfigureComponent }))
        .directive("downgradeDsarWebforms", downgradeComponent({ component: DSARWebFormsComponent }))
        .directive("downgradeDsarRequestQueueWrapper", downgradeComponent({ component: DSARRequestQueueWrapperComponent }))
        .directive("downgradeDsarAddNewWebform", downgradeComponent({ component: DSARAddNewWebformComponent }))
        .directive("downgradeDsarDashboard", downgradeComponent({ component: DSARDashboardComponent }))
        .directive("downgradeWebformSidebarEdit", downgradeComponent({ component: WebformSidebarEditComponent }))
        .factory("historyServiceDowngraded", downgradeInjectable(RequestQueueHistoryService))
        .factory("dsarRulesServiceDowngraded", downgradeInjectable(DsarWebformRulesApiService))
        .factory("requestQueueDetailServiceDowngraded", downgradeInjectable(RequestQueueDetailService))
        .factory("requestQueueServiceDowngraded", downgradeInjectable(RequestQueueService))
        .factory("dsarWorkflowServiceDowngraded", downgradeInjectable(DSARWorkflowService))
        .factory("webformApiService", downgradeInjectable(WebformApiService))
        .factory("webformStore", downgradeInjectable(WebformStore))

        // assessment-legacy.module
        .directive("downgradeAssessmentDetailRelatedDataSubjectModal", downgradeComponent({ component: AssessmentDetailRelatedDataSubjectModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAaNeedsMoreInfoModal", downgradeComponent({ component: AANeedsMoreInfoModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAACommentsModal", downgradeComponent({ component: AACommentsModalComponent }))
        .directive("downgradeAAAttachmentsModal", downgradeComponent({ component: AAAttachmentsModalComponent }))
        .directive("downgradeAAUnapprovalModal", downgradeComponent({ component: AAUnapprovalModal }))
        .directive("downgradeAaSendBackModal", downgradeComponent({ component: AASendBackModalComponent }))
        .directive("downgradeAAMetadataModal", downgradeComponent({ component: AAMetadataModalComponent }))
        .directive("downgradeCopyAssessment", downgradeComponent({ component: AACopyAssessmentComponent }))
        .directive("downgradeAaRiskModalComponent", downgradeComponent({ component: AARiskModalComponent }))
        .directive("downgradeShareAssessmentModal", downgradeComponent({ component: ShareAssessmentModal }))
        .directive("downgradeAaList", downgradeComponent({ component: AAListComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAaTemplateSelection", downgradeComponent({ component: AATemplateSelectionComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAAQuestionActivityStream", downgradeComponent({ component: AAQuestionActivityStreamModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAaCreateForm", downgradeComponent({ component: AACreateFormComponent }))
        .directive("downgradeAaBulkLaunch", downgradeComponent({ component: AABulkLaunchComponent }) as angular.IDirectiveFactory)
        .directive("downgradeAssessmentDetail", downgradeComponent({ component: AAAssessmentDetailComponent }))
        .directive("downgradeCopyAssessment", downgradeComponent({ component: AACopyAssessmentComponent }))
        .directive("downgradeAaTemplateSettings", downgradeComponent({ component: AATemplateSettingsComponent }))
        .directive("downgradeSettingsSsp", downgradeComponent({ component: SettingsSspComponent }))
        .directive("downgradeAaAssessmentSettings", downgradeComponent({ component: AAAssessmentSettingsComponent }))
        .service("AssessmentDetailApi", downgradeInjectable(AssessmentDetailApiService))
        .service("AssessmentListApi", downgradeInjectable(AssessmentListApiService))

        // consentreceipt-legacy.module
        .directive("crWrapper", downgradeComponent({ component: CrWrapperComponent }) as angular.IDirectiveFactory)
        .directive("crDashboard", downgradeComponent({ component: CrDashboardComponent }) as angular.IDirectiveFactory)
        .directive("crCollectionPointList", downgradeComponent({ component: CrCollectionPointListComponent }) as angular.IDirectiveFactory)
        .directive("crCollectionPointDetails", downgradeComponent({ component: CrCollectionPointDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crCollectionPointCreate", downgradeComponent({ component: CrCollectionPointCreateComponent }) as angular.IDirectiveFactory)
        .directive("crCollectionPointType", downgradeComponent({ component: CrCollectionPointTypeComponent }) as angular.IDirectiveFactory)
        .directive("crCollectionPointUpdates", downgradeComponent({ component: CrCollectionPointUpdatesComponent }) as angular.IDirectiveFactory)
        .directive("crPurposeList", downgradeComponent({ component: CrPurposeListComponent }) as angular.IDirectiveFactory)
        .directive("crPurposeDetails", downgradeComponent({ component: CrPurposeDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crPurposeCreate", downgradeComponent({ component: CrPurposeCreateComponent }) as angular.IDirectiveFactory)
        .directive("crTransactionList", downgradeComponent({ component: CrTransactionListComponent }) as angular.IDirectiveFactory)
        .directive("crTransactionDetails", downgradeComponent({ component: CrTransactionDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crDataSubjectList", downgradeComponent({ component: CrDataSubjectListComponent }) as angular.IDirectiveFactory)
        .directive("crDataSubjectDetails", downgradeComponent({ component: CrDataSubjectDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crTopicList", downgradeComponent({ component: CrTopicListComponent }) as angular.IDirectiveFactory)
        .directive("crTopicDetails", downgradeComponent({ component: CrTopicDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crPreferenceCenterList", downgradeComponent({ component: CrPreferenceCenterListComponent }) as angular.IDirectiveFactory)
        .directive("crPreferenceCenterDetails", downgradeComponent({ component: CrPreferenceCenterDetailsComponent }) as angular.IDirectiveFactory)
        .directive("crCustomPreferenceList", downgradeComponent({ component: CrCustomPreferenceListComponent }) as angular.IDirectiveFactory)
        .directive("crCustomPreferenceDetails", downgradeComponent({ component: CrCustomPreferenceDetailsComponent }) as angular.IDirectiveFactory)

        // ssp-legacy.module
        .directive("downgradeSelfServicePortal", downgradeComponent({ component: SelfServicePortalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSspConfig", downgradeComponent({ component: SspConfigComponent }) as angular.IDirectiveFactory)
        .directive("downgradedSspTemplateModal", downgradeComponent({ component: SspTemplateModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSspConfigTemplateCard", downgradeComponent({ component: SspConfigTemplateCardComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSspTemplateCard", downgradeComponent({ component: SspTemplateCardComponent }) as angular.IDirectiveFactory)
        .directive("downgradeSspLaunchAssessmentModal", downgradeComponent({ component: SspLaunchAssessmentModalComponent }) as angular.IDirectiveFactory)
        .service("SSPApiService", downgradeInjectable(SSPApiService))

        // risks-legacy.module
        .directive("downgradeRiskRepo", downgradeComponent({ component: RiskRepoComponent }) as angular.IDirectiveFactory)
        .directive("downgradeRiskRegisterRiskDetails", downgradeComponent({ component: RiskRegisterRiskDetailsComponent }) as angular.IDirectiveFactory)
        .directive("downgradeRisksWrapper", downgradeComponent({ component: RisksWrapperComponent }) as angular.IDirectiveFactory)
        .directive("downgradeRiskCategoryTable", downgradeComponent({ component: RiskCategoryTableComponent }) as angular.IDirectiveFactory)

        // Cookies-legacy.module
        .directive("websiteListTable", downgradeComponent({ component: WebsiteListTableComponent }) as angular.IDirectiveFactory)
        .directive("thirdPartyBannerNotification", downgradeComponent({ component: ThirdPartyBannerNotification }) as angular.IDirectiveFactory)
        .directive("ccIabVendorDetails", downgradeComponent({ component: CCIABVendorDetails }) as angular.IDirectiveFactory)
        .directive("ccIabVendorsList", downgradeComponent({ component: CCIABVendorsList }) as angular.IDirectiveFactory)
        .directive("ccTemplatesTable", downgradeComponent({ component: TemplatesTableComponent }) as angular.IDirectiveFactory)
        .directive("addTemplateModal", downgradeComponent({ component: AddTemplateModalController }) as angular.IDirectiveFactory)
        .directive("publishTemplateModal", downgradeComponent({ component: PublishTemplateModalController }) as angular.IDirectiveFactory)
        .directive("assignTemplate", downgradeComponent({ component: AssignTemplateComponent }) as angular.IDirectiveFactory)
        .directive("ccTemplateTab", downgradeComponent({ component: TemplatesTabComponent }) as angular.IDirectiveFactory)
        .directive("editTemplateModal", downgradeComponent({ component: EditTemplateModalController }) as angular.IDirectiveFactory)
        .directive("templateNewLanguage", downgradeComponent({ component: TemplateNewLanguageComponent }) as angular.IDirectiveFactory)
        .directive("scanBehindTutorial", downgradeComponent({ component: ScanBehindTutorialComponent }) as angular.IDirectiveFactory)
        .directive("ccPreferenceCenterIabEditDetails", downgradeComponent({ component: CcPreferenceCenterIabEditDetails }) as angular.IDirectiveFactory)
        .directive("addCustomCookieModal", downgradeComponent({ component: AddCustomCookieModal }) as angular.IDirectiveFactory)
        .directive("orgGroupReAssignModal", downgradeComponent({ component: OrgGroupReAssignModal }) as angular.IDirectiveFactory)
        .directive("ccGroupModal", downgradeComponent({ component: CCGroupModal }) as angular.IDirectiveFactory)
        .directive("removeCookieModal", downgradeComponent({ component: RemoveCookieModal }) as angular.IDirectiveFactory)
        .directive("cookieDeleteConfirmationComponent", downgradeComponent({ component: CookieDeleteConfirmationComponent }) as angular.IDirectiveFactory)
        .directive("scriptIntegrationSettings", downgradeComponent({ component: ScriptIntegrationSettings }) as angular.IDirectiveFactory)
        .directive("policyConfiguration", downgradeComponent({ component: PolicyConfiguration }) as angular.IDirectiveFactory)
        .directive("ccLanguageSelectionComponent", downgradeComponent({ component: CcLanguageSelectionComponent }) as angular.IDirectiveFactory)
        .directive("ccLanguageModal", downgradeComponent({ component: CcLanguageModal }) as angular.IDirectiveFactory)
        .directive("downgradePreferenceCenterEditDetails", downgradeComponent({ component: PreferenceCenterEditDetails }) as angular.IDirectiveFactory)
        .directive("downgradePreferenceCenterStyling", downgradeComponent({ component: PreferenceCenterStyling }) as angular.IDirectiveFactory)
        .directive("addVendorListModal", downgradeComponent({ component: AddVendorListModal }) as angular.IDirectiveFactory)
        .directive("downgradeCookiePublishScriptModal", downgradeComponent({ component: CookiePublishScriptModal }) as angular.IDirectiveFactory)
        .directive("scheduleAuditModal", downgradeComponent({ component: ScheduleAuditModal }) as angular.IDirectiveFactory)
        .directive("downgradeRestoreScriptModal", downgradeComponent({ component: RestoreScriptModal }) as angular.IDirectiveFactory)
        .directive("downgradeConsentPolicyList", downgradeComponent({ component: ConsentPolicyList }) as angular.IDirectiveFactory)
        .directive("downgradeCookieEmptyState", downgradeComponent({ component: CookieEmptyState }) as angular.IDirectiveFactory)
        .directive("cookieLimitModal", downgradeComponent({ component: CookieLimitModal }) as angular.IDirectiveFactory)
        .directive("consentPolicyDetailModal", downgradeComponent({ component: ConsentPolicyDetailModal }) as angular.IDirectiveFactory)
        .directive("downgradeConsentPolicyDetailWrapper", downgradeComponent({ component: ConsentPolicyDetailWrapper }) as angular.IDirectiveFactory)
        .directive("downgradeConsentPolicyStatusModal", downgradeComponent({ component: ConsentPolicyStatusModal }) as angular.IDirectiveFactory)
        .directive("downgradeScriptArchiveWrapper", downgradeComponent({ component: ScriptArchiveWrapper }) as angular.IDirectiveFactory)
        .directive("downgradeScanLoginDetail", downgradeComponent({ component: ScanLoginDetail }) as angular.IDirectiveFactory)
        .directive("downgradeCcPreferenceCenterPreview", downgradeComponent({ component: PreferenceCenterPreviewComponent }) as angular.IDirectiveFactory)
        .directive("downgradeCcBannerWrapper", downgradeComponent({ component: CcBannerWrapperComponent }) as angular.IDirectiveFactory)
        .directive("downgradeCcCategorizationsWrapper", downgradeComponent({ component: CCCategorizationsWrapper }) as angular.IDirectiveFactory)
        .service("CookieApiService", downgradeInjectable(CookieApiService))
        .service("CookieSideMenuHelperService", downgradeInjectable(CookieSideMenuHelperService))
        .service("CookiesStorageService", downgradeInjectable(CookiesStorageService))

        // inventory-legacy.module
        .directive("attributeManager", downgradeComponent({ component: AttributeManagerComponent }) as angular.IDirectiveFactory)
        .directive("attributeManagerDetails", downgradeComponent({ component: AttributeManagerDetailsComponent }) as angular.IDirectiveFactory)
        .directive("inventoryListWrapper", downgradeComponent({ component: InventoryListWrapper }) as angular.IDirectiveFactory)
        .directive("inventoryDetails", downgradeComponent({ component: InventoryDetails }) as angular.IDirectiveFactory)
        .directive("inventoryBulkEdit", downgradeComponent({ component: InventoryBulkEditComponent }) as angular.IDirectiveFactory)
        .directive("relateInventoryModal", downgradeComponent({ component: RelateInventoryModalComponent }) as angular.IDirectiveFactory)
        .directive("linkInventoryModal", downgradeComponent({ component: LinkInventoryModalComponent }) as angular.IDirectiveFactory)
        .directive("relateDataSubjectModal", downgradeComponent({ component: RelateDataSubjectModalComponent }) as angular.IDirectiveFactory)
        .directive("linkDataSubjectModal", downgradeComponent({ component: LinkDataSubjectModalComponent }) as angular.IDirectiveFactory)
        .directive("addInventoryModal", downgradeComponent({ component: AddInventoryModalComponent }) as angular.IDirectiveFactory)
        .directive("copyInventoryModal", downgradeComponent({ component: CopyInventoryModalComponent }) as angular.IDirectiveFactory)
        .directive("copyInventoryV2Modal", downgradeComponent({ component: CopyInventoryV2ModalComponent }) as angular.IDirectiveFactory)
        .directive("attributeGroupModal", downgradeComponent({ component: AttributeGroupModal }))
        .directive("attributePickerModal", downgradeComponent({ component: AttributePickerModal }))
        .directive("attributeManagerEntry", downgradeComponent({ component: AttributeManagerEntryComponent }) as angular.IDirectiveFactory)
        .directive("inventoryListManagerEntry", downgradeComponent({ component: InventoryListManagerEntryComponent }) as angular.IDirectiveFactory)
        .directive("inventoryListManagerDetails", downgradeComponent({ component: InventoryListManagerDetailsComponent }) as angular.IDirectiveFactory)
        .directive("inventoryListManager", downgradeComponent({ component: InventoryListManagerComponent }) as angular.IDirectiveFactory)
        .directive("inventoryManageVisibilityWrapper", downgradeComponent({ component: InventoryManageVisibilityWrapperComponent }) as angular.IDirectiveFactory)
        .directive("attributeManagerModal", downgradeComponent({ component: AttributeManagerModalComponent }) as angular.IDirectiveFactory)
        .directive("attributeManagerModalSmall", downgradeComponent({ component: AttributeManagerModalComponent }) as angular.IDirectiveFactory)
        .directive("editAttributeOptionModal", downgradeComponent({ component: EditAttributeOptionModalComponent }) as angular.IDirectiveFactory)
        .directive("attributeOptionStatusModal", downgradeComponent({ component: AttributeOptionStatusModalComponent }) as angular.IDirectiveFactory)
        .directive("attributeStatusModal", downgradeComponent({ component: AttributeStatusModalComponent }) as angular.IDirectiveFactory)
        .directive("bulkEditConfirmationModal", downgradeComponent({ component: BulkEditConfirmationModalComponent }) as angular.IDirectiveFactory)
        .factory("Inventory", downgradeInjectable(InventoryService))
        .directive("dmDataMaps", downgradeComponent({ component: DMDataMaps }) as angular.IDirectiveFactory)
        .directive("dmWelcome", downgradeComponent({ component: DMWelcome }) as angular.IDirectiveFactory)
        .factory("dmOneDataMaps", downgradeInjectable(DMOneDataMaps))
        .factory("DMTemplate", downgradeInjectable(DMTemplateService))
        .factory("InventoryRiskModalViewLogic", downgradeInjectable(InventoryRiskModalViewLogicService))
        .directive("sectionEditModal", downgradeComponent({ component: SectionEditModalComponent }) as angular.IDirectiveFactory)
        .directive("sendBackModalComponent", downgradeComponent({ component: SendBackModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeDataDiscovery", downgradeComponent({ component: DataDiscoveryComponent }))
        .directive("downgradeInventoryRiskDetails", downgradeComponent({ component: InventoryRiskDetailsComponent }) as angular.IDirectiveFactory)

        // template.module
        .directive("downgradeCustomTemplatesComponent", downgradeComponent({ component: CustomTemplatesComponent }))
        .directive("downgradeGalleryTemplatesComponent", downgradeComponent({ component: GalleryTemplatesComponent }))
        .directive("downgradeTemplateRelatedInventoryModal", downgradeComponent({ component: TemplateRelatedInventoryModalComponent }))
        .directive("downgradeTemplateQuestionBuilderModal", downgradeComponent({ component: TemplateQuestionBuilderModalComponent }))
        .directive("downgradeEditTemplateSectionModalComponent", downgradeComponent({ component: EditTemplateSectionModalComponent }))
        .directive("downgradeEditTemplateWelcomeMessage", downgradeComponent({ component: EditTemplateWelcomeMessage }))
        .directive("downgradeTemplateConditionsModal", downgradeComponent({ component: TemplateConditionsModalComponent }))
        .directive("questionAttributeModal", downgradeComponent({ component: QuestionAttributeModalComponent }))
        .directive("downgradeTemplatePage", downgradeComponent({ component: TemplatePageComponent }))
        .directive("downgradeEditTemplateRuleModal", downgradeComponent({ component: EditTemplateRuleModalComponent }))
        .directive("downgradeConfigureRelationshipModal", downgradeComponent({ component: ConfigureRelationshipModalComponent }))
        .directive("downgradeTemplateCreateModal", downgradeComponent({ component: TemplateCreateFormComponent }))
        .factory("TemplateRule", downgradeInjectable(TemplateRuleService))
        .factory("QuestionType", downgradeInjectable(QuestionTypeService))
        .factory("IncidentSchemas", downgradeInjectable(IncidentSchemaApiService))

        // global-rediness-legacy.module
        .directive("downgradeGlobalReadinessWelcome", downgradeComponent({ component: GlobalReadinessWelcomeComponent }) as angular.IDirectiveFactory)
        .directive("downgradeGlobalReadinessLaunch", downgradeComponent({ component: GlobalReadinessLaunchComponent }) as angular.IDirectiveFactory)
        .directive("downgradeGlobalReadinessDashboard", downgradeComponent({ component: GlobalReadinessDashboardComponent }) as angular.IDirectiveFactory)
        .directive("downgradeGlobalReadinessAssessments", downgradeComponent({ component: GlobalReadinessAssessmentsListComponent }) as angular.IDirectiveFactory)
        .directive("downgradeGlobalReadinessAssessmentReport", downgradeComponent({ component: GlobalReadinessAssessmentReport }) as angular.IDirectiveFactory)
        .directive("downgradeStartAssessmentModal", downgradeComponent({ component: GRAStartAssessmentModalComponent }) as angular.IDirectiveFactory)
        .directive("downgradeShowGuidanceModal", downgradeComponent({ component: GRAShowGuidanceModalComponent }) as angular.IDirectiveFactory)

        // reports.module
        .directive("reportsList", downgradeComponent({ component: ReportsListComponent }) as angular.IDirectiveFactory)
        .directive("reportsTemplateSelect", downgradeComponent({ component: ReportsTemplateSelectComponent }) as angular.IDirectiveFactory)
        .directive("columnReport", downgradeComponent({ component: ColumnReportComponent }) as angular.IDirectiveFactory)
        .directive("pdfReport", downgradeComponent({ component: PDFReportComponent }) as angular.IDirectiveFactory)
        .directive("dashboardList", downgradeComponent({ component: DashboardListComponent }) as angular.IDirectiveFactory)
        .directive("dashboardEdit", downgradeComponent({ component: DashboardEditComponent }) as angular.IDirectiveFactory)
        .factory("ReportAction", downgradeInjectable(ReportAction))
        .directive("dashboardSwitcher", downgradeComponent({ component: DashboardSwitcherComponent }) as angular.IDirectiveFactory)

        // reports-schedule.module
        .directive("reportScheduleList", downgradeComponent({ component: ReportScheduleListComponent }) as angular.IDirectiveFactory)

        // risks
        .factory("RiskAction", downgradeInjectable(RiskActionService))
        .factory("RiskAPI", downgradeInjectable(RiskAPIService))
        .factory("RiskAdapter", downgradeInjectable(RiskAdapterService))

        // onboarding.module
        .factory("OnboardingService", downgradeInjectable(OnboardingService))
        .directive("onboardingModal", downgradeComponent({ component: OnboardingModal }) as angular.IDirectiveFactory)

        // privacy module
        .directive("policyWrapper", downgradeComponent({ component: PolicyWrapperComponent }) as angular.IDirectiveFactory)
        .directive("policyTable", downgradeComponent({ component: PolicyTable }) as angular.IDirectiveFactory)
        .directive("privacyNoticeList", downgradeComponent({ component: PrivacyNoticeListComponent }) as angular.IDirectiveFactory)
        .directive("createPrivacyNoticeModal", downgradeComponent({ component: CreatePrivacyNoticeModalComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeDetails", downgradeComponent({ component: PrivacyNoticeDetailsComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeHeader", downgradeComponent({ component: PrivacyNoticeHeaderComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeBuilder", downgradeComponent({ component: PrivacyNoticeBuilderComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeBuilderSidebar", downgradeComponent({ component: PrivacyNoticeBuilderSidebarComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeIntegrations", downgradeComponent({ component: PrivacyNoticeIntegrationsComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticePublishScriptModal", downgradeComponent({ component: PrivacyNoticePublishScriptModalComponent }) as angular.IDirectiveFactory)
        .directive("privacyNoticeVersions", downgradeComponent({ component: PrivacyNoticeVersionsComponent }) as angular.IDirectiveFactory)
        ;
}
