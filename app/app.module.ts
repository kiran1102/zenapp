// Angular
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Base Modules
import { CoreModule } from "modules/core/core.module";
import { AppRoutingModule } from "./app-routing.module";

// Root Modules
import {
    OtAlertBannerModule,
    OtModalModule,
    OtToastModule,
    OtLoadingModule,
} from "@onetrust/vitreus";

// Modules
import { AdminModule } from "./modules/admin/admin.module";
import { AssessmentModule } from "./modules/assessment/assessment.module";
import { AttributeManagerModule } from "./modules/attribute-manager/attribute-manager.module";
import { ConsentModule } from "modules/consent/consent.module";
import { CookiesModule } from "./modules/cookies/cookies.module";
import { CookiesV2Module } from "cookiesV2Module/cookies-v2.module";
import { ControlsModule } from "controls/controls.module";
import { DashboardModule } from "./modules/dashboard/dashboard.module";
import { DataMappingModule } from "./modules/data-mapping/data-mapping.module";
import { DSARModule } from "./modules/dsar/dsar.module";
import { EulaModule } from "./modules/eula/eula.module";
import { GlobalReadinessModule } from "./modules/global-readiness/global-readiness.module";
import { GlobalSidebarModule } from "./modules/global-sidebar/global-sidebar.module";
import { IncidentModule } from "incidentModule/incident.module";
import { IntgModule } from "./modules/intg/intg.module";
import { InventoryModule } from "./modules/inventory/inventory.module";
import { LinkControlsModule } from "./modules/link-controls/link-controls.module";
import { McmModalModule } from "./modules/mcm/mcm-modal.module";
import { PolicyModule } from "./modules/policy/policy.module";
import { OnboardingModule } from "./modules/onboarding/onboarding.module";
import { PreferencesModule } from "modules/preferences/preferences.module";
import { ReportsModule } from "./modules/reports/reports.module";
import { RisksModule } from "./modules/risks/risks.module";
import { SettingsModule } from "./modules/settings/settings.module";
import { SspModule } from "./modules/self-service-portal/ssp.module";
import { TemplateModule } from "./modules/template/template.module";
import { VendorModule } from "./modules/vendor/vendor.module";
import { WrapperModule } from "./modules/wrapper/wrapper.module";
import { DGBetaModule } from "./modules/dg-beta/dg-beta.module";

// Components
import { AppComponent } from "./app.component";

// For now we are loading AngularJs main component and injecting Angular components. Later on we will bootstrap to Angular component.
// Loading up Angular component will be needed if we were to handle routing correctly.
// As for routing, we can use two strategies: 1. Overriding UrlHandlingStrategy 2. Using sink route
// We will use one of the strategies later

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CoreModule,
        AppRoutingModule,

        OtAlertBannerModule.forRoot(),
        OtModalModule.forRoot(),
        OtToastModule.forRoot(),
        OtLoadingModule,

        AdminModule,
        AssessmentModule,
        AttributeManagerModule,
        ConsentModule,
        ControlsModule,
        CookiesModule,
        CookiesV2Module,
        DashboardModule,
        DataMappingModule,
        DSARModule,
        EulaModule,
        GlobalReadinessModule,
        GlobalSidebarModule,
        IncidentModule,
        IntgModule,
        InventoryModule,
        LinkControlsModule,
        McmModalModule,
        OnboardingModule,
        PolicyModule,
        PreferencesModule,
        ReportsModule,
        RisksModule,
        SettingsModule,
        SspModule,
        TemplateModule,
        VendorModule,
        WrapperModule,
        DGBetaModule,
    ],
    declarations: [
        AppComponent,
    ],
})
export class AppModule {}
