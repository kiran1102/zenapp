export interface ITab {
    id: string;
    text: string;
    otAutoId: string;
}