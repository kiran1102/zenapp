import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

import { IBranding, IBrandingHeader } from "interfaces/branding.interface";

export interface IBrandingState {
    branding: IBranding;
    defaultBranding: IBranding;
    isFetchingBranding: boolean;
}

export type IBrandingSelector = OutputSelector<IStoreState, IBranding, (brandingState: IBrandingState) => IBranding>;
export type IBrandingHeaderSelector = OutputSelector<IStoreState, IBrandingHeader, (brandingState: IBrandingState) => IBrandingHeader>;
export type IBrandingFetchStatus = OutputSelector<IStoreState, boolean, (brandingState: IBrandingState) => boolean>;
