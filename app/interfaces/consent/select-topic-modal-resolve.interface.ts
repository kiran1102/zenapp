import { PurposeDetails } from "crmodel/purpose-details";

export interface ISelectTopicModalResolve {
    callback?: () => void;
    purpose: PurposeDetails;
}
