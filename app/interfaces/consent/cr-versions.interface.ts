export interface IVersionListItem {
    Id: string;
    Label: string;
    Status: string;
    Version: number;
    CreatedBy: string;
    CreatedDate: Date;
    UpdatedBy: string;
    UpdatedDate: Date;
    IsExpanded?: boolean;
}

export interface IVersionGroup {
    Title: string;
    Versions: IVersionListItem[];
}

export interface IVersionDetails {
    show: Function;
    label: string;
    type: string;
    getValue: Function;
}
