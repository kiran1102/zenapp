export interface IEntityTranslation {
    Language: string;
    Name: string;
    Description?: string;
    Default?: boolean;
    IsNew?: boolean;
}
