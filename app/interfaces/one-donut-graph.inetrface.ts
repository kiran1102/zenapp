export interface IOneDonutGraphData {
    color: string;
    count: number;
    legendLabel: string;
    magnitude: number;
    percentage: number;
    status?: string;
}
