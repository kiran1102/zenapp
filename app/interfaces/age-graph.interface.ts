export interface IAgeDate {
    Day: number;
    DaysAgo?: number;
    HoursAgo?: number;
    Id: string;
    MinutesAgo?: number;
    Name: string;
    OrgGroup: string;
    StateDesc: string;
    StateName: string;
    Time: number;
    Version: number;
    TimeSinceCreated: string;
}
