export interface IRelateInventoryModalResolve {
    modalTitle?: string;
    howIsInventoryRelatedText?: string;
    chooseInventoryItemText?: string;
    submitButtonText?: string;
    cancelButtonText?: string;
    inventoryType: number;
    linkedType?: string;
    recordId?: string;
    recordName?: string;
    orgGroupId?: string;
    callback?: () => void;
}
