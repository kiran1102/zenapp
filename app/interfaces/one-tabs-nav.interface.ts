export interface ITabsNav {
    readonly id: string;
    readonly text: string;
    action?: (option: ITabsNav) => void;
    route?: string;
    otAutoId?: string;
}

export interface ITabsMeta<T> {
    readonly id: T;
    readonly text: string;
    action?: (option: ITabsMeta<T>) => void;
    route?: string;
}
