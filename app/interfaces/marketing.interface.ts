export interface IMarketingDefault {
    backgroundImage: string;
    marketingLogo: string;
    loginLogo: string;
    headline: string;
    copy: string;
    buttonText: string;
    marketingUrl: string;
    registerUrl: string;
}
