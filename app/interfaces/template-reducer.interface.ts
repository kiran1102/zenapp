// External Libraries
import { OutputSelector } from "reselect";

// Redux
import { IStoreState } from "interfaces/redux.interface";

// Interfaces
import { IAssessmentTemplateDetail } from "interfaces/assessment-template-detail.interface";
import { IAssessmentTemplateSection } from "interfaces/assessment-template-section.interface";
import {
    IAssessmentTemplateQuestion,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import { ICustomTemplate } from "modules/template/interfaces/custom-templates.interface";
import {
    ITemplateUpdateWelcomeText,
    ITemplateUpdateRequest,
} from "modules/template/interfaces/template-update-request.interface";
import { IAssessmentNavigation } from "interfaces/assessment/assessment-navigation.interface";
import { ITemplateRule } from "modules/template/components/template-page/template-rule-list/template-rule/template-rule.interface";
import { ITemplateNameValue } from "modules/template/interfaces/template.interface";
import { ITemplateSkipConditionCombination } from "interfaces/template-skip-condition-combination.interface";

// Enums
import { TemplateStates } from "enums/template-states.enum";
import { TemplateModes } from "enums/template-modes.enum";
import { IStringMap } from "interfaces/generic.interface";

export interface ITemplateBuilderState {
    actionAllIds: string[];
    actionById: IStringMap<ITemplateRule>;
    actionTempModel: ITemplateRule;
    navigationById: IStringMap<IAssessmentNavigation>;
    navigationTempModel?: IStringMap<IAssessmentNavigation>;
    questionById: IStringMap<IAssessmentTemplateQuestion>;
    questionIdNavigationIdsMap: IStringMap<string[]>;
    sectionAllIds: string[];
    sectionById: IStringMap<IAssessmentTemplateSection>;
    sectionIdQuestionIdsMap: IStringMap<string[]>;
    skipConditionCombinations: IStringMap<ITemplateSkipConditionCombination>;
    templateRuleCombinations: string[];
}

export interface ITemplateState {
    builder: ITemplateBuilderState;
    current: IAssessmentTemplateDetail;
    disableHeaderActions: boolean;
    dragQuestion: boolean;
    dragSection: boolean;
    inventorySettings: { prePopulateResponse: IStringMap<string>};
    isFetchingBuilder: boolean;
    isFetchingPublishedTemplate: boolean;
    isFetchingTemplate: boolean;
    isTemplateCopied: boolean;
    isTemplateVersionHistoryPaneOpen: boolean;
    isTempModelStateChanged: boolean;
    loaderText: string;
    optionsByQuestionIdsMap: IStringMap<IQuestionOptionDetails[]>;
    orgGroupId: string;
    publishedTemplateList: ITemplateNameValue[];
    state: TemplateStates;
    templateByIds: { [key: string]: ICustomTemplate };
    templateIds: string[];
    templateMode: TemplateModes;
    tempModel: ITemplateUpdateRequest | null;
}

export type IGetTemplateTempModelState = OutputSelector<IStoreState, boolean, (templateState: ITemplateState) => boolean>;
export type ITemplateUpdateSelector = OutputSelector<IStoreState, ITemplateUpdateRequest, (templateState: ITemplateState) => ITemplateUpdateRequest>;
export type ITemplateSelector = OutputSelector<IStoreState, IAssessmentTemplateDetail, (templateState: ITemplateState) => IAssessmentTemplateDetail>;
export type ITemplateWelcomeMessageSelector = OutputSelector<IStoreState, ITemplateUpdateWelcomeText, (template: ITemplateUpdateRequest) => ITemplateUpdateWelcomeText>;
export type ITemplateBuilderSelector = OutputSelector<IStoreState, ITemplateBuilderState, (templateState: ITemplateState) => ITemplateBuilderState>;
export type ITemplateSectionSelector = OutputSelector<ITemplateBuilderSelector, { [key: string]: IAssessmentTemplateSection }, (builderState: ITemplateBuilderState, sectionId: string) => { [key: string]: IAssessmentTemplateSection }>;
