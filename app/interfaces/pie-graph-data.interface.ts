export interface IPieGraphData {
    color: string;
    legendLabel: string;
    label?: string;
    magnitude: number;
    percentage?: string;
    show: boolean;
}
