export interface ISaveChangesModalResolve {
    cancelButtonText: string;
    closeCallback?: () => any;
    confirmationText: string;
    discardCallback: () => any;
    discardChangesButtonText: string;
    iconClass?: string;
    modalTitle: string;
    promiseToResolve: () => ng.IPromise<any>;
    saveButtonText: string;
    showDiscard?: boolean;
    showSave?: boolean;
}
