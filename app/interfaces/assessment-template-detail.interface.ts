import { ITemplateUpdateMetadata } from "modules/template/interfaces/template-update-request.interface";

export interface IAssessmentTemplateDetail {
    canEdit: boolean;
    createdDate: Date;
    description: string;
    directLaunchEnabled: boolean;
    friendlyName?: string | null;
    icon: string;
    id: string;
    isActive: boolean;
    isDraft: boolean;
    isLocked: boolean;
    launchFromInventory: boolean;
    name: string;
    nameKey: string;
    orgGroupId: string;
    orgGroupSelectionAllowed: boolean;
    parentId?: string | null;
    prepopulateAttributeQuestions: boolean;
    primaryAssetQuestionId?: string;
    primaryProcessingActivityQuestionId?: string;
    primaryVendorQuestionId?: string;
    publishDate?: Date;
    respondentSelectionAllowed: boolean;
    rootVersionId: string;
    selfServiceEnabled: boolean;
    status: string;
    templateType: string;
    templateVersion: number;
    updatedDate?: Date;
    welcomeText?: string;
}

export interface IAssessmentTemplateDetailUpdateParams {
    metadata: ITemplateUpdateMetadata;
    settings: {
        prepopulateAttributeQuestions: boolean;
    };
}

export interface IAssessmentTemplateOptionSettings {
    directLaunchEnabled?: boolean;
    isLocked?: boolean;
    orgGroupSelectionAllowed?: boolean;
    prepopulateAttributeQuestions?: boolean;
    respondentSelectionAllowed?: boolean;
    selfServiceEnabled?: boolean;
    launchFromInventory?: boolean;
    primaryAssetQuestionId?: string;
    primaryProcessingActivityQuestionId?: string;
    primaryVendorQuestionId?: string;
}

export interface IAssessmentTemplateOptionSettingsInitParams {
    prepopulateAttributeQuestions: IAssessmentTemplateOptionSettingsDetails;
}

export interface IAssessmentTemplateOptionSettingsDetails {
    title: string;
    description: string;
    showSelectableOptions?: boolean;
    key?: string;
}

export interface ITemplateWelcomeTextUpdateParams {
    welcomeText: string;
}

export interface ICreateTemplateVersionResponse {
    id: string;
    rootVersionId: string;
    templateVersion: number;
}

export interface ICopyTemplateDetails {
    name: string;
    description: string;
    icon: string;
    templateId: string;
    orgGroupId: string;
}
export interface IPublishTemplatePayload {
    pushTemplateChanges: boolean;
    notifyRespondents: boolean;
}
export interface IPublishTemplateModalData extends IPublishTemplatePayload {
    count: number;
}
