// Interfaces
import { IStringMap } from "./generic.interface";

// Enums
import { TableColumnTypes } from "enums/data-table.enum";

export interface IGridColumn {
    name: string;
    nameKey?: string;
    sortKey?: string;
    style?: IStringMap<string>;
    type?: TableColumnTypes;
    valueKey: string;
    isMandatory?: boolean;
    id?: string;
}

export interface IGridPaginationParams {
    page: any;
    size?: number;
    sort?: string | string[] | IGridSort;
}

export interface IGridSort {
    sortedKey: string;
    sortOrder: "desc" | "asc";
}
