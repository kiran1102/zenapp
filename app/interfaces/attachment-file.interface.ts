import { IAttachment } from "./attachment.interface";
import { IPageableMeta } from "interfaces/pagination.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

export interface IAttachmentFile {
    FileName: string;
    Blob?: Blob;
    Extension: string;
    Comments?: string;
    RefIds?: string[] | string;
    RefId?: string;
    IsInternal?: boolean | string;
    AttachmentRefId?: string;
    Name?: string;
    Type?: number | string;
    isEncrypted?: boolean | string;
    RequestId?: string;
    Id?: string;
    Location?: string;
    FileSize?: string | number;
    FileType?: string;
    CreatedById?: string;
    CreatedBy?: string;
    CreatedDT?: string;
}

export interface IAttachmentFileResponse extends IAttachment {
    Id: string;
    FileName: string;
    Name?: string;
    StatusId?: number;
    Location?: string;
    Comments?: string;
    FileSize?: string | number;
    FileType?: string;
    CreatedById?: string;
    CreatedBy?: string;
    CreatedDT?: string | Date;
    Type?: string | number;
    IsInternal?: boolean;
    AttachmentRefId?: string;
    RefIds?: any;
    RefId?: string;
    isInternal?: boolean;
    dropDownMenu?: IDropdownOption[];
}

export interface IAttachmentFileType {
    FileTypeExtension: string;
    FileTypeName: string;
}
