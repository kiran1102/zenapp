export interface IOrgGroupState {
    tenantNormalizer: any;
    rootNormalizer: any;
    normalizer: any;
    tenantOrgTree: any;
    orgTree: any;
    selectedOrg: any;
}

export interface IOrganization {
    Id: string;
    Name: string;
    PrivacyOfficerId: string;
    PrivacyOfficerName: string;
    LanguageId?: number;
    CanDelete: boolean;
    ParentId: string;
    Children?: IOrganization[];
    ChildrenIds?: string[];
    Level?: number;
    Index?: number;
    Visible?: boolean;
    PathName?: string[];
}

export interface IOrganizationMap {
    rootId?: string;
    id?: IOrganization;
}

// deprecate below - IOrg, IOrgList, and IOrgTable.
// change camelCase back to PascalCase because converting everything to camelCase kills productivity.
export interface IOrg {
    id: string;
    name: string;
    privacyOfficerId: string;
    privacyOfficerName: string;
    canDelete: boolean;
    parentId: string;
    childrenIds: string[];
    level: number;
}

export interface IOrgList extends Array<IOrg> {
    [index: number]: IOrg;
}

export interface IOrgTable {
    rootId: string;
    id?: IOrg;
}
