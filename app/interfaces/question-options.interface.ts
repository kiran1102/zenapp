export interface IQuestionOption {
    key?: string;
    value?: string;
    hint?: string;
    isSelected?: boolean;
}
