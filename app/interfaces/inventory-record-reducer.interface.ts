import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import { IInventoryRecordV2, IAttributeDetailV2 } from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface IInventoryRecordState {
    record: IInventoryRecordV2;
    isLoadingRecord: boolean;
    isLoadingSecondaryRecord: boolean;
    schema: IAttributeDetailV2[];
    displayValues: IStringMap<string>;
    changes: IInventoryRecordV2;
    errors: IStringMap<boolean>;
    isEditing: boolean;
    inventoryId: string;
}

export type IRecordSelector = OutputSelector<IStoreState, IInventoryRecordV2, (recordState: IInventoryRecordState) => IInventoryRecordV2>;
export type ISchemaSelector = OutputSelector<IStoreState, IAttributeDetailV2[], (recordState: IInventoryRecordState) => IAttributeDetailV2[]>;
export type IDisplayValuesSelector = OutputSelector<IStoreState, IStringMap<string>, (recordState: IInventoryRecordState) => IStringMap<string>>;
export type IIsLoadingRecordSelector = OutputSelector<IStoreState, boolean, (recordState: IInventoryRecordState) => boolean>;
export type IRecordChangesSelector = OutputSelector<IStoreState, IInventoryRecordV2, (recordState: IInventoryRecordState) => IInventoryRecordV2>;
export type IRequiredHasValuesSelector = OutputSelector<IStoreState, boolean, (getRecord: IInventoryRecordV2, getRecordChanges: IInventoryRecordV2, getSchema: IAttributeDetailV2[]) => boolean>;
export type IHasChangesSelector = OutputSelector<IStoreState, boolean, (recordState: IInventoryRecordState) => boolean>;
export type IHasErrorsSelector = OutputSelector<IStoreState, boolean, (recordState: IInventoryRecordState) => boolean>;
export type IEditingRecordSelector = OutputSelector<IStoreState, boolean, (recordState: IInventoryRecordState) => boolean>;
export type ICurrentNameSelector = OutputSelector<IStoreState, string, (recordState: IInventoryRecordState) => string>;
export type IAttributeMapSelector = OutputSelector<IStoreState, IStringMap<IAttributeDetailV2>, (recordState: IInventoryRecordState) => IStringMap<IAttributeDetailV2>>;
export type IInventoryIdSelector = OutputSelector<IStoreState, string, (recordState: IInventoryRecordState) => string>;
export type IInventoryOrgGroupSelector = OutputSelector<IStoreState, string, (recordState: IInventoryRecordState) => string>;
