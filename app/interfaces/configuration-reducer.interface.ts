import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface IConfigurationState {
    configurations: IStringMap<any>;
}

export type IConfigurationStateSelector = OutputSelector<IStoreState, IStringMap<any>, (configurationState: IConfigurationState) => IStringMap<any>>;
export type IConfigurationSelector = OutputSelector<IStoreState, IStringMap<any>, (configurationState: IConfigurationState) => IStringMap<any>>;
export type IConfigurationSelectorFactory = (type: string) => IConfigurationSelector;
