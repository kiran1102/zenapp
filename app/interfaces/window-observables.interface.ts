import { Observable } from "rxjs";

export interface IWindowObservablesService {
    get: (observerKey: string) => Observable<EventListener>;
}
