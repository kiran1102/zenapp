export interface IExpandableLinkItem {
    text?: string;
    textKey?: string;
    bullet?: boolean;
}

export interface IExpandableLink {
    title?: string;
    titleKey?: string;
    items: IExpandableLinkItem[];
}
