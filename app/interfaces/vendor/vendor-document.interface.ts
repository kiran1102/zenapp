import { IPageableMeta } from "interfaces/pagination.interface";

export interface IDocumentFileResponse extends IPageableMeta {
    content: IContractRowContent[];

}
export interface IContractRowContent {
    name: string;
    id: string;
    vendorId: string;
    vendorName?: string;
    attachments: IDocumentAttachment[];
    status: string;
    agreementCreatedDate: string;
    contractTypeId: string;
    contractTypeName: string;
    contractTypeNameKey: string;
    approvedDate: string;
    expirationDate: string;
    contractOwnerId: string;
    contractOwner?: string;
    vendorContact: string;
    approvers: string;
    note: string;
    cost: string;
    currency: string;
    viewOnly?: boolean;
    orgGroupId?: string;
    isSelected?: boolean;
}

export interface IDocumentAttachment {
    id: string;
    attachmentId: string;
    attachmentDescription: string;
    fileName: string;
}
