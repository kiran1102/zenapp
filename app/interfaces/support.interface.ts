export interface ISupportTicket {
    comment: string;
}

export interface ISupportUpgrade {
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
    comment?: string;
    module: string;
}

export interface IContactOneTrustModalResolve {
    module: string;
}
