export interface IOrganization {
    canDelete: boolean;
    children?: IOrganization[];
    id: string;
    name: string;
    parentId: string | null;
    privacyOfficerId: string;
    privacyOfficerName: string;
}
