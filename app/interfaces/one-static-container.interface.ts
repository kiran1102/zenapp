import { IAnchorButton } from "interfaces/one-anchor-button.interface";
import { IExpandableLink } from "interfaces/one-expandable-links.interface";

export interface IStaticContainerContent {
    title?: string;
    titleKey?: string;
    copy?: string;
    copyKey?: string;
    link?: IExpandableLink[];
}

export interface IStaticContainer {
    index?: number | string;
    content: IStaticContainerContent;
    action: IAnchorButton;
    permissionKeys?: string[];
    negativePermissionKeys?: string[];
}
