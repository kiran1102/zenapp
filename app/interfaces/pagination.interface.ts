export interface IPageable extends IPageableMeta {
    content: any[];
}

export interface IPageableOf<T> extends IPageableMeta {
    content: T[];
}

export interface IPageableMeta {
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort?: any;
    totalElements: number;
    totalPages: number;
    itemCount?: number;
}

export interface IPaginationParams<T> {
    page?: number;
    filters?: T;
    size?: number;
    sort?: { [key: string]: string };
    search?: string;
}

export interface IPaginationFilter {
    name: string;
    value: string[];
}

export interface IPageableResponse {
    PageNumber: number;
    TotalPageCount: number;
    ItemCount: number;
    PageSize: number;
    TotalItemCount: number;
}

export interface IPaginatedResponse<T> {
    data?: T[];
    meta: {
        page: IPageableMeta;
    };
}
