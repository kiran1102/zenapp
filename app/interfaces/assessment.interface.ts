import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IQuestion } from "interfaces/question.interface";
import { ISection, ISectionZenApi } from "interfaces/section.interface";
import { IAssignment } from "interfaces/assignment.interface";
import { INote } from "interfaces/note.interface";

export interface IAssessmentScope extends ng.IScope {
    goToQuestion: any;
    goToSection: any;
    loading: boolean;
    NeedInfoCount: number;
    permissions: any;
    submitState: any;
}

export interface IProjectSectionDirections {
    Backwards: number;
    NotMoving: number;
    Forwards: number;
}

export interface IHeaderOption {
    id?: string;
    action: () => any;
    text: string;
}

export interface IAssessmentSubmitButtonConfig {
    submitButtonVisible: boolean;
    submitButtonEnabled: boolean;
    submitButtonAction: string;
    submitButtonText: string;
}

export interface IAssessmentQuestionPermission {
    allowedPermission: string;
    allowedRoles: string[];
    allowedAssessmentStates: string[];
    allowedSectionStates?: string[];
}

export interface IAssessmentModalData {
    currentUser: IUser;
    project: IProject;
    question: IQuestion;
    callback?: () => any;
}

export interface IViewInfoAddedModalData extends IAssessmentModalData {
    reviewerName: string;
}

export interface INeedMoreInfoModalData extends IAssessmentModalData {
    isFromViewInfoAddedModal: boolean | undefined;
}

// ======================================================

interface IProjectGetPageMetadata {
    ItemCount: number;
    PageNumber: number;
    PageSize: number;
    TotalItemCount: number;
    TotalPageCount: number;
}

export interface IProjectGetPageResponse {
    Metadata: IProjectGetPageMetadata;
    Content: IProjectPageResponse[];
    IsSuccess: boolean;
    StatusCode: number;
}

export interface IProjectGetPageResponseNormalized {
    metadata: IProjectGetPageMetadata;
    page: IProjectPageResponse[];
}

export interface IProjectPageResponse {
    Assignment: IAssignment | null;
    Assignments: IAssignment[] | null;
    CompletedDT: string | null;
    CreatedDT: string | null;
    Creator: string;
    Deadline: string | null;
    Description: string;
    Id: string;
    IsDeletable: boolean;
    IsLatestVersion: boolean;
    IsLinked: boolean;
    Lead: string;
    LeadId: string;
    Name: string;
    Number: number;
    OrgGroup: string;
    OrgGroupId: string;
    OwnerId: string;
    ProjectRisk: number | null;
    ProjectRiskState: string | null;
    Readonly: boolean;
    Reviewer: string;
    ReviewerId: string;
    StateDesc: string;
    Tags: any[] | null;
    TemplateId: string;
    TemplateName: string;
    TemplateState: string;
    TemplateType: number;
    TemplateVersion: number;
    Version: number;
}

export interface IPiaAssessmentSelfService extends IProjectPageResponse {
    StateLabel?: string;
    StateName?: string;
    localeCreatedDT: string | null;
    localeDeadline?: string | null;
    CreatedDTtooltip: string | null;
    Deadlinetooltip?: string | null;
}

export interface IProjectGetResponse extends IProjectPageResponse {
    HasComments?: boolean;
    HasRisks?: boolean;
    NextTemplateName?: string;
    Notes?: INote[];
    ReminderDays?: number | null;
    Sections?: ISectionZenApi[];
}

export interface IPiaAssessment extends IProjectGetResponse {
    canBeDuplicated: boolean;
    CreatedDTtooltip: string | null;
    Deadlinetooltip: string | null;
    hasCrossSectionConditions?: boolean;
    isShared: boolean;
    localeCreatedDT: string | null;
    localeDeadline: string | null;
    Remaining: any[];
    ReminderShow?: boolean;
    Sections: ISection[];
    riskClass: string;
    riskWord: string;
    StateLabel: string;
    StateName: string;
    time: number;
}
