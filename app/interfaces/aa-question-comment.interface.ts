export interface IQuestionComment {
    id: string;
    comment: string;
    createdBy: string;
    createdDate: string;
    parentAssessmentDetailId?: string;
}

export interface IQuestionCommentCreateRequest {
    assessmentId: string;
    comment: string;
    parentAssessmentDetailId?: string;
    questionId: string;
    sectionId: string;
    callback?: (commentsCount: number) => void;
}

export interface IAssessmentQuestionComments {
    assessmentId: string;
    sectionId: string;
    questionId: string;
    commentDetails: IQuestionComment[];
}

export interface IAssessmentAllComments extends IQuestionComment {
    assessmentId: string;
    sectionId: string;
    questionId: string;
}

export interface ICommentCollaborationPaneResponse {
    content: IAssessmentAllComments[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: ICommentCollaborationPaneSort[];
    totalElements: number;
    totalPages: number;
}

export interface ICommentCollaborationPaneSort {
    ascending: boolean;
    descending: boolean;
    direction: string;
    ignoreCase: boolean;
    nullHandling: string;
    property: string;
}
