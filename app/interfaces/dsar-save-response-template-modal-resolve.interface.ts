import { IResponseTemplate } from "dsarModule/interfaces/response-templates.interface";
import { IGetWebformLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

export interface IDSARSaveResponseTemplateModalResolve {
    isEditModal: boolean;
    title?: string;
    row?: IResponseTemplate;
    languages?: IGetWebformLanguageResponse[];
}
