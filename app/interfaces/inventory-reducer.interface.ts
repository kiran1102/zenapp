import { OutputSelector } from "reselect";

// Interfaces
import {
    IStoreState,
    IStringToBooleanMap,
} from "interfaces/redux.interface";
import {
    IInventoryRecord,
    IAttribute,
    IAssessmentStatusMap,
    IRelatedTable,
} from "interfaces/inventory.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IFilterTree } from "interfaces/filter.interface";
import {
    IStringMap,
    INumberMap,
} from "interfaces/generic.interface";
import {
    IInventoryAssessment,
    IPageableInventory,
} from "interfaces/inventory.interface";

export type IAttributeMap = IStringMap<IAttribute>;
export type IRecordMap = IStringMap<IInventoryRecord>;

export interface IInventoryState {
    allRecordIds: string[];
    allAttributeIds: string[];
    byRecordId: IRecordMap;
    byAttributeId: IAttributeMap;
    selectedInventoryIds: IStringToBooleanMap;
    isFetchingInventoryList: boolean;
    paginationMetadata?: IPageableInventory;
    relatedInventoryCurrentPageNumber?: number;
    relatedInventoryCurrentPageSize?: number;
    initialRecordModel: IInventoryRecord;
    currentRecordModel: IInventoryRecord;
    isSavingRecord: boolean;
    isFetchingRecord: boolean;
    isFetchingRecordDetails: boolean;
    isExportingInventory: boolean;
    recordEdited: boolean;
    didRecordInvalidate: boolean;
    didInventoryInvalidate: boolean;
    isRecordEditEnabled: boolean;
    requiredFieldsValid: boolean;
    statusMap: IAssessmentStatusMap;
    searchTerm: string;
    inventoryId: number;
    currentPageNumber: number;
    isAttributeDrawerOpen: boolean;
    isFilterDrawerOpen: boolean;
    filterData: IFilterTree;
    byAssessmentId: IStringMap<IInventoryAssessment>;
    allAssessmentIds: string[];
    isFetchingAssessmentList: boolean;
    isShowingFilteredResults: boolean;
    isFilteringInventory: boolean;
    isFetchingRiskList: boolean;
    isFetchingRelatedList: boolean;
    relatedInventories: INumberMap<IRelatedTable>;
    currentRecordOrgId: string;
    usersValid: IStringMap<boolean>;
}

export type IRecordByIdSelector = OutputSelector<IStoreState, IInventoryRecord, (inventoryState: IInventoryState) => IInventoryRecord>;
export type IRecordByIdSelectorFactory = (id: string) => IRecordByIdSelector;
export type ISelectedRecordsMapSelector = OutputSelector<IStoreState, IStringToBooleanMap, (inventoryState: IInventoryState) => IStringToBooleanMap>;
export type IInventoryListFetchStatusSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IAllRecordIdsSelector = OutputSelector<IStoreState, string[], (inventoryState: IInventoryState) => string[]>;
export type IAllRecordMapSelector = OutputSelector<IStoreState, IRecordMap, (inventoryState: IInventoryState) => IRecordMap>;
export type IAllAttributeMapSelector = OutputSelector<IStoreState, IAttributeMap, (inventoryState: IInventoryState) => IAttributeMap>;
export type IRecordListInOrderSelector = OutputSelector<IStoreState, IInventoryRecord[], (allModels: IRecordMap, ids: string[]) => IInventoryRecord[]>;
export type IAttributeListInOrderSelector = OutputSelector<IStoreState, IAttribute[], (allModels: IAttributeMap, ids: string[]) => IAttribute[]>;
export type IInventoryPaginationMetadata = OutputSelector<IStoreState, IPageable, (inventoryState: IInventoryState) => IPageable>;
export type IInventoryStateInvalidSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type ISavingRecord = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IFetchingRecord = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IRecordEditEnabled = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IFetchingRecordDetails = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IExportingInventory = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IGetInitialRecord = OutputSelector<IStoreState, IInventoryRecord, (inventoryState: IInventoryState) => IInventoryRecord>;
export type IGetCurrentRecord = OutputSelector<IStoreState, IInventoryRecord, (inventoryState: IInventoryState) => IInventoryRecord>;
export type IGetRecordDetails = OutputSelector<IStoreState, IRecordMap, (inventoryState: IInventoryState) => IRecordMap>;
export type IEditCancelled = OutputSelector<IStoreState, IRecordMap, (inventoryState: IInventoryState) => IRecordMap>;
export type IRequiredFieldsValid = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IStatusMapSelector = OutputSelector<IStoreState, IAssessmentStatusMap, (inventoryState: IInventoryState) => IAssessmentStatusMap>;
export type ISelectedRecordSelector = OutputSelector<IStoreState, IInventoryRecord[], (recordList: IInventoryRecord[]) => IInventoryRecord[]>;
export type IInventorySearchTermSelector = OutputSelector<IStoreState, string, (inventoryState: IInventoryState) => string>;
export type IInventoryIdSelector = OutputSelector<IStoreState, number, (inventoryState: IInventoryState) => number>;
export type IInventoryPageSelector = OutputSelector<IStoreState, number, (inventoryState: IInventoryState) => number>;
export type IAttributeDrawerState = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IFilterDrawerState = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IFilterDataSelector = OutputSelector<IStoreState, IFilterTree, (inventoryState: IInventoryState) => IFilterTree>;
export type IAllAssessmentIdsSelector = OutputSelector<IStoreState, string[], (inventoryState: IInventoryState) => string[]>;
export type IAllAssessmentsMapSelector = OutputSelector<IStoreState, IStringMap<IInventoryAssessment>, (inventoryState: IInventoryState) => IStringMap<IInventoryAssessment>>;
export type IAssessmentListSelector = OutputSelector<IStoreState, IInventoryAssessment[], (allModels: IStringMap<IInventoryAssessment>, ids: string[]) => IInventoryAssessment[]>;
export type IIsLoadingAssessmentsSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IIsShowingFilteredResultsSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IIsFilteringInventorySelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IIsLoadingRisksSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IIsLoadingRelatedSelector = OutputSelector<IStoreState, boolean, (inventoryState: IInventoryState) => boolean>;
export type IInventoryRelatedSelector = OutputSelector<IStoreState, INumberMap<IRelatedTable>, (inventoryState: IInventoryState) => INumberMap<IRelatedTable>>;
export type IGetCurrentRecordOrgId = OutputSelector<IStoreState, string, (inventoryState: IInventoryState) => string>;
export type IGetUsersValid = OutputSelector<IStoreState, IStringMap<boolean>, (inventoryState: IInventoryState) => IStringMap<boolean>>;
export type IGetAllUsersValid = OutputSelector<IStoreState, boolean, (usersValidMap: IStringMap<boolean>) => boolean>;
