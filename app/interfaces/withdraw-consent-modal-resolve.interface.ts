export interface IWithdrawConsentModalResolve {
    transactionId: string;
    result?: boolean;
}
