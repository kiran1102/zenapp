export interface IHorseshoeGraphData {
    series: IHorseShoeSeries[];
    legend?: Array<{
        class: string,
        label: string;
    }>;
    title?: number | string;
    titleClass?: string;
}

export interface IHorseShoeSeries {
    graphClass: string;
    label: string;
    value: number;
    dashOffset?: number;
    dashArray?: string;
}
