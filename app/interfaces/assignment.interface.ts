export interface IAssignment {
    Assignee?: string;
    AssigneeId: string;
    Id?: string;
    ProjectId?: string;
    SectionId?: string;
    ProjectVersion?: number;
    Comment?: string;
}

export interface IShareProjectParams {
    ProjectId: string;
    ProjectVersion: number;
    Type: number;
    AssigneeIds: Object[];
}
