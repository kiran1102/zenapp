export interface ITaskConfirmationModalResolve {
    modalTitle?: string;
    closeButtonText?: string;
    confirmationText: string;
    imgSrc?: string;
}
