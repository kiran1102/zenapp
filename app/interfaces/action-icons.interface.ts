export interface IActionIcon {
    key?: string;
    handleClick?: Function;
    isLoading?: boolean;
    isDisabled?: boolean;
    iconClass?: string;
    isHidden?: boolean;
    showPill?: boolean;
    hasTooltip?: boolean;
    tooltipPlacement?: string;
    tooltipCloseDelay?: number;
    tooltipTrigger?: string;
    tooltipTranslationKey?: string;
    buttonClass?: string;
}
