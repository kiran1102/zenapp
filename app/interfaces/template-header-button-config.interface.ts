export interface ITemplateHeaderButtonConfig {
    buttonId: string;
    identifier?: string;
    isVisible: boolean;
    text: string;
    isDisabled?: boolean;
    toolTip?: string;
    isLoading?: boolean;
}
