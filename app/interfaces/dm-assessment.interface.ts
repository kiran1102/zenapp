import { IPageableMeta } from "interfaces/pagination.interface";
import { IUser } from "interfaces/user.interface";

export interface IDMAssessment {
    assessmentId: string;
    assessmentRefId: number;
    name: string;
    approver: IUser;
    asset: { name: string, id: string };
    createdBy: IUser;
    createdDate: Date;
    lastModifiedDate: Date;
    completedDate?: Date;
    orgGroup: { name: string, id: string };
    respondent: IUser;
    statusId: number;
    templateType: number;
    isDisabled: boolean;
}

export interface IDMStateFilters {
    [index: number]: {label: string, value: number};
}

export interface IDMAssessmentListResponse extends IPageableMeta {
    content: IDMAssessment[];
}

export interface IDMSendBackModal {
    updateCallback: (comment: string) => ng.IPromise<{result: boolean}>;
    callback: (result: boolean) => void;
}
