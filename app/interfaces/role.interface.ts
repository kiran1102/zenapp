import { IStringMap } from "interfaces/generic.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IRole {
    Assignable?: boolean;
    Description?: string;
    Name: string;
    Id: string;
    RoleId?: number;
    orgGroupId?: string;
}

export interface IRoleDetails extends IRole {
    CanEdit?: boolean;
    CanDelete?: boolean;
    CanReplace?: boolean;
    IsSeeded?: boolean;
    OrgGroupName?: string;
    lastModifiedBy?: string;
    dropDownMenu?: IDropdownOption[];
}

export interface IRoleRequest {
    description?: string;
    id?: string;
    name: string;
    orgGroupId?: string;
    permissions?: string[];
}

export interface IPermissionHierarchyNode extends INameDescriptionTranslations {
    id?: string;
    name: string;
    exists?: boolean;
    children?: IPermissionHierarchyNode[];
}

interface INameTranslation {
    nameTranslation?: string;
}

interface INameDescriptionTranslations extends INameTranslation {
    descriptionTranslation?: string;
}

export interface IRoleColumnType {
    Name?: string;
}

export interface IPageableRoles {
    content: IRoleDetails[];
    page: IPageableMeta;
}

export interface IRolesListParams {
    isAssignable?: boolean;
    page?: number;
    size?: number;
    sort?: string;
    organizationId?: string;
    roleName?: string;
    orgGroupId?: string;
}

export interface IRolesImportModalResolve {
    modalTitle: string;
    closeCallback: () => void;
}

export interface IRolesReplaceModalResolve {
    modalTitle: string;
    closeCallback: () => void;
    orgGroupId: string;
    importName: string;
    description?: string;
    isReplace?: boolean;
}

export interface IRoleAttachmentFile {
    blob: Blob;
    fileName: string;
    name: string;
    extension: string;
    description: string;
    orgGroupId: string;
    importName: string;
    isReplace?: boolean;
}
