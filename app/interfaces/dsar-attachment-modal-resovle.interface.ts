export interface IDSARAttachmentModalResolve {
    isInternal: boolean;
    requestId: string;
    callback?: any;
}
