export interface IBulkImportStatus {
    CreatedBy: string;
    CreatedDate: string;
    ErrorCount: number;
    FileName: string;
    Id: string;
    ImportId: string;
    Name: string;
    Status: string;
    StatusId: number;
    TypeId: string;
}
