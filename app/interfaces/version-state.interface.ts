export interface IVersionState {
    state: number;
    isDisabled?: boolean;
}
