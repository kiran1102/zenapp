import { IStringMap } from "interfaces/generic.interface";
import { IAssignment } from "interfaces/assignment.interface";
import { IRisk } from "interfaces/risk.interface";
import { IVersionState } from "interfaces/version-state.interface";

export interface IQuestionZenApi {
    AllowJustification?: boolean;
    AllowNotApplicable?: boolean;
    AllowNotSure?: boolean;
    Assignment: IAssignment | null;
    AttachmentCount: number;
    CommentCount: number;
    ConditionCount: number;
    GroupId: number | null;
    HasAttachments: boolean;
    HasComments: boolean;
    HasConditions: boolean;
    HasVersionResponseChanged: boolean | null;
    Hint: string | null;
    Id: string;
    InfoAddedComment: string | null;
    IsFromCurrentVersion: boolean;
    IsVersionResponseReviewed: boolean;
    MoreInfoComment: string | null;
    Name: string | null;
    NextQuestionId: number | null;
    Required?: boolean;
    RequireJustification?: boolean;
    Responses: IQuestionResponse[];
    ReportFriendlyName: string | null;
    Risk: IRisk | null;
    RiskLevel: number | null;
    SectionId: string;
    State: number;
    Tags: any[] | null; // TODO: Tags Interface
    TemplateQuestionId: string;
    Text: string | null;
    Type: number;
    OptionListTypeId?: number;
}

export interface IQuestion extends IQuestionZenApi {
    AllowOther?: boolean;
    Answer?: any;
    AnswerName?: string;
    AssetDiscoveryName: string | null;
    Content: string;
    DefaultResponseType: number;
    inventoryError?: string | null;
    isAnswered?: boolean;
    isShowingSubmitBtn?: boolean;
    IsSkipped: boolean;
    Justification?: string;
    MultiSelect: boolean | null;
    NotApplicable: boolean;
    Number: string;
    Options?: IAssessmentDataElement[];
    OtherAnswer?: any;
    QuestionType?: number;
    ResponseType?: number;
    versionState?: IVersionState;
    AssetDiscovery: boolean | null;
}

export interface IDMAssessmentQuestion {
    allowNotSure: boolean;
    allowOther: boolean;
    attributeId: string;
    attributeCode?: string;
    attributeReadValues: any[];
    dataSubjectType?: string;
    description: string;
    disabled: boolean;
    displayType?: number;
    disabledOptions: any[];
    followUpQuestions: IStringMap<any>;
    id: number;
    isOpen?: boolean;
    justificationType: any;
    modified: boolean;
    name: string;
    number: number;
    questionType: number;
    reportFriendlyName: string;
    required: boolean;
    responseType: number;
    sectionId: string;
    seeded: boolean;
    templateQuestionUniqueId: string;
}

export interface IQuestionSerialized {
    Id: string;
    State: number;
    Comment?: string;
}

export interface IAssessmentQuestionAction {
    question?: IQuestion;
    type: string;
    value?: any;
    name?: string;
}

export interface IAssessmentDataElement {
    Answer: boolean;
    Description: string | null;
    Group: string;
    GroupId: string;
    Hint: string | null;
    Name: string;
    Value: string;
    Type: number;
    IsPlaceholder?: boolean;
    InputValue?: string;
}

export interface ICreateQuestionResponseContract {
    HasVersionResponseChanged: boolean;
    QuestionId: string;
    Responses: IQuestionResponse[];
}

export interface IQuestionResponse {
    Type: number;
    Value: any;
    InventoryId?: string | null;
}

export interface IDMQuestionCondition {
    conditionalOperator: string;
    relationalOperator: number;
    value: string;
}

export interface IDMConditions {
    conditions: IDMQuestionCondition[];
    targetQuestionId: string;
}

export interface IMultiChoiceDataTypes {
    Multichoice: number;
    AssetDiscovery: number;
}
