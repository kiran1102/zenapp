import { IQuestion, IQuestionZenApi, IDMAssessmentQuestion } from "interfaces/question.interface";
import { IAssignment } from "interfaces/assignment.interface";

export interface ISectionZenApi {
    Assignment: IAssignment | null;
    Id: string;
    IsLocked: boolean;
    Name: string;
    NextSectionId: string;
    Questions: IQuestionZenApi[];
    StateDesc: string | null;
    TemplateId: string;
    TemplateName: string;
    TemplateSectionId: string;
}

export interface ISection extends ISectionZenApi {
    Completed: number;
    FilteredQuestions: any; // TODO: Make Interface
    Questions: IQuestion[];
    IsSkipped: boolean;
    Remaining: any[]; // TODO: Make Interface
    StateLabel: string;
    StateName: string | null;
    Total: number;
    isWelcomeSection: boolean;
    questionToggle?: boolean;
    totalIndex: number;
    totalRemaining: number;
    canShowNoAdditionalChangesButton?: boolean;
}

export interface IPhase {
    Id?: string;
    Sections: ISection[];
    TemplateName?: string;
    expanded: boolean;
    locked: boolean;
}

export interface IDMAssessmentSection {
    description: string;
    id: string;
    incompleteQuestionCount: number;
    isActive?: boolean;
    isReadyForApproval: boolean;
    name: string;
    nextQuestions: IDMAssessmentQuestion[];
    sectionSequenceId: number;
    statusId: number;
}
