import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

export interface ISetLanguageModalData {
    masterLanguagesList: any;
    webformLanguagesList: any;
    templateId: string;
    bodyText?: string;
    headerTitle?: string;
    promiseToResolve: (changes: ISetLanguagePutData[], defaultLanguage?: string) => ng.IPromise<boolean>;
    promiseArray: () => Promise<[IProtocolResponse<IGetWebformLanguageResponse[]>, IProtocolResponse<IGetAllLanguageResponse[]>]>;
}
export interface IGetWebformLanguageResponse {
    Id: number;
    Code: string;
    Name: string;
    Status: number;
    IsDefault: boolean;
}
export interface IGetAllLanguageResponse {
    Id: number;
    Code: string;
    Name: string;
    Translated: boolean;
}
export interface ILanguageSelection extends IGetAllLanguageResponse {
    enabled?: boolean;
    isDefault?: boolean;
}
export interface ISetLanguageTableData {
    textdata: string;
    id: number;
    enabled: boolean;
    code: string;
    isDefault: boolean;
    dirty: boolean;
}
export interface ISetLanguagePutData {
    id: number;
    name: string;
    status: number;
    code: string;
    isDefault: boolean;
}

export interface ISetLanguageRadio {
    code: string;
    id: number;
}

export interface ITenantTranslations {
    FallbackToNotFound: boolean;
    LanguageCode: string;
    Translations: IStringMap<string>;
}
export enum Status {
    Enabled = 10,
    Disabled = 20,
}
