import { IProject } from "interfaces/project.interface";
import { IQuestion } from "interfaces/question.interface";

export interface IAssessmentQuestionModalData {
    Project: IProject;
    Question: IQuestion;
}
