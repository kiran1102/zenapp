import { IAssessmentTemplateQuestion } from "interfaces/assessment-template-question.interface";

export interface IAssessmentTemplateSection {
    attributes: string;
    description: string;
    id: string;
    invalidQuestionCount: number;
    name: string;
    questions?: IAssessmentTemplateQuestion[] | null;
    sequence: number;
}

export interface IAssessmentTemplateSectionCreateParam {
    nextSectionId: string;
    section: {
        description: null;
        name: string;
    };
}

export interface IEditTemplateSectionResolve {
    modalTitle: string;
    templateId: string;
    sectionId?: string;
    sectionName?: string;
}
