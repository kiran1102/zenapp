import { IStringMap } from "interfaces/generic.interface";
export interface IFormField {
    dataType: string;
    inputType?: string;
    descriptionKey?: string;
    fieldClass?: string;
    errors?: IFormFieldErrors;
}

export interface IFormFieldInput extends IFormField {
    fieldKey: string;
    fieldNameKey?: string;
    pValueKey?: string;
    placeholderKey?: string;
    required?: boolean;
    validationKey?: string;
    inputValue?: string;
}

export interface IFormFieldBtn extends IFormField {
    textKey: string;
    text?: string;
    btnType?: string;
    requireFields?: string[];
    isLoading?: boolean;
}

export interface IFormFieldErrors {
    hasErrors: boolean;
    list: string[];
    map?: IStringMap<any>;
}
