export interface IGeneralModalData {
    Title: string;
    Content: string;
    ShowClose: boolean;
}
