import { IProject } from "interfaces/project.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IProjectReviewerModalData {
    project: IProject;
    Team: IOrgUserAdapted[];
}
