// Interfaces
import { IStringMap, IValueId } from "interfaces/generic.interface";
import {
    IPageableMeta,
    IPageable,
} from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IRiskDetails,
    IAdditionalAttributes,
} from "interfaces/risk.interface";
import { INameId } from "interfaces/generic.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Enums
import { CellTypes } from "enums/general.enum";
import {
    RelationshipLinkType,
    InventoryRiskControlsFilterOperator,
} from "enums/inventory.enum";
import { IFilterTree } from "modules/reports/reports-shared/interfaces/report.interface";
import { InventoryStatusOptions } from "enums/inventory.enum";
import { TableColumnTypes } from "enums/data-table.enum";

export interface IPageableInventory extends IPageable {
    recordId: string;
}

export interface IAttribute {
    allowOther: boolean;
    attributeStatusId: number;
    description: string;
    descriptionKey?: string;
    editable: boolean;
    fieldAttributeCode: string;
    fieldAttributeTypeId: number;
    fieldResponseType: string;
    fieldResponseTypeId: number;
    id: string;
    inventoryTypeId: number;
    name: string;
    nameKey?: string;
    order: number;
    required: boolean;
    values: IAttributeValue[];
    version: number;
}

export interface IAttributeValue {
    attributeValueStatusId?: number;
    description?: string;
    id?: string;
    name: string;
    value?: string;
    version?: number;
    active?: boolean;
    nameKey?: string;
}

export interface IAttributeDetails {
    icon: string;
    items: object[];
    text: string;
}

export interface IInventoryRecord {
    id: string;
    cells: IInventoryCell[];
    canEdit: boolean;
    assessmentOutstanding: boolean;
    isSelected?: boolean;
}

export interface IRecordResponse {
    canEdit: boolean;
    id: string;
    map: IStringMap<IInventoryCellValue[]>;
}

export interface IInventoryAddModalResolve {
    modalTitle: string;
    inventoryId: number;
    callback: () => void;
}

export interface IInventoryCopyModalResolve {
    recordId: string;
    callback: () => void;
}

export interface IInventoryCopyV2ModalResolve {
    record: IInventoryRecordV2;
    inventoryId: number;
    callback: () => void;
}

export interface IInventoryCell {
    displayValue: string;
    values: IInventoryCellValue[];
    attributeId: string;
    attributeCode: string;
    fieldResponseTypeId?: number;
    fieldAttributeTypeId?: number;
    route?: string;
    routeParams?: IStringMap<string>;
}

export interface IInventoryCellValue {
    value: string;
    valueId?: string;
    valueKey?: string;
}

export interface IInventorySchema {
    description: string;
    id: string;
    name: string;
    nameKey: string;
    attributes: IAttribute[];
}

export interface IInventorySchemaV2 {
    description: string;
    id: string;
    name: string;
    nameKey: string;
    uri: string;
}

export interface IInventorySchemaV1 {
    description: string;
    id: number;
    name: string;
    nameKey: string;
}

export interface IRecordsResponse extends IPageableMeta {
    content: IRecordResponse[];
}

export interface IInventoryDetails extends IInventorySchema {
    record: IInventoryRecord;
    statusMap: IAssessmentStatusMap;
    currentRecordOrgId: string;
}

export interface IInventoryTable extends IPageableMeta {
    records: IInventoryRecord[];
}

export interface IInventoryTableView extends IInventoryTable, IInventorySchema {
    statusMap: IAssessmentStatusMap;
}

export interface IAssessmentStatusMap {
    [key: string]: IAssessmentStatus;
}

export interface IAssessmentStatus {
    assessmentId: string;
    status: string;
}

export interface IHeaderOption {
    id?: string;
    action: () => any;
    text: string;
}

export interface ILaunchModalResolve {
    modalTitle: string;
    Processes: IRecordAssignment[];
    TemplateType: number;
    TableName: string;
    callback?: (response: IProtocolResponse<ILaunchResponse[]>) => void;
}

export interface ILaunchResponse {
    assessmentId: string;
    name: string;
}

export interface IRecordAssignment {
    InventoryId: string;
    Name?: string;
    OrgGroupId?: string;
    defaultRespondent?: string;
    inventoryNumber?: string;
}

export interface IInventoryDependencyMap {
    assessmentStatus: IAssessmentStatusMap;
}

export interface IInventoryParams {
    page: number;
    size: number;
    searchTerm: string;
}

export interface IInventoryAttachment {
    Id: string;
    Name: string;
    Comments: string;
    CreatedBy: string;
    CreatedDT: Date | string;
}

export interface IInventoryAssessment {
    id: string;
    name: string;
    templateName: string;
    status: string;
    orgName: string;
    completedDate: Date | string;
    route: string;
    routeParams: IInventoryAssessmentParams;
    type: number;
}

export interface IInventoryAssessmentParams {
    Id?: string;
    Version?: number;
    assessmentId?: string;
    launchAssessmentDetails?: IAssessmentLaunchDetails;
}

export interface IAssessmentLaunchDetails {
    inventoryId: string;
    inventoryTypeId: number;
    inventoryName: string;
}

export interface ILinkAssessmentModalData {
    recordId: string;
    body: string;
    typeId: number;
    recordName: string;
    callback?: () => void;
}

export interface IRelatedRecord {
    key: string;
    name: string;
    organization: string;
}

export interface IRelatedTableRow {
    id: string;
    name: string;
    organization: string;
    attributeId: string;
    inventoryId?: string;
    inventoryType: number | string;
    inbound: boolean;
    canEdit: boolean;
    inventoryAssociationTypeKey?: string;
    subject?: string;
    subjectKey?: string;
    nameKey?: string;
    location?: string;
    category?: string;
    relation?: string;
    type?: string;
}

export interface ILinkedInventoryRow {
    anchorInventoryId?: string;
    editable?: boolean;
    inventoryAssociationId: string;
    inventoryAssociationType: string;
    inventoryAssociationTypeKey?: string;
    inventoryId: string;
    inventoryType: string;
    location?: string;
    name: string;
}

export interface IInventoryListItem {
    id: string;
    name: string;
    nameKey: string;
    description: string;
    descriptionKey: string;
    selected?: boolean;
    dataElements?: IInventoryListItem[];
}

export interface IFormattedListItem extends IInventoryListItem {
    allSelected?: boolean;
    hasSelections?: boolean;
}

export interface IInventoryElementListItem extends IInventoryListItem {
    classifications: IInventoryListItem[];
    categories: IInventoryListItem[];
    categoryNames?: string;
    classificationNames?: string;
    flagged?: boolean;
}

export interface IPersonalDataItem {
    dataCategories: IInventoryListItem[];
    dataElement: IInventoryListItem;
    dataSubjectType: IInventoryListItem;
    dataClassifications?: IInventoryListItem[];
}

export interface IFormattedPersonalDataTableRow extends IPersonalDataItem {
    category: string;
    categoryKey: string;
    name: string;
    nameKey: string;
    subject: string;
    subjectKey: string;
}

export interface IInventoryListOption extends IInventoryListItem {
    displayName?: string;
}

export interface IRelatedTable {
    name: string;
    number: number;
    first: boolean;
    last: boolean;
    id: number;
    numberOfElements: number;
    size: number;
    totalElements: number;
    totalPages: number;
    columns: IInventoryTableColumn[];
    rows: IRelatedTableRow[] | ILinkedInventoryRow[] | IContractRelatedRow[] | IFormattedPersonalDataTableRow[];
}

export interface IInventoryTableColumn {
    label: string;
    dataKey?: string;
    rowTranslationKey?: string;
    type: CellTypes;
    dataObjRef?: string;
    keyObjRef?: string;
    tooltipKey?: string;
}

export interface ISectionEditModalResolve {
    isNewSection: boolean;
    templateId: string;
    section: any;
    previousSectionId: string;
    modalTitle: string;
    sectionId: string;
    sectionName: string;
    callback?: (response) => void;
}

export interface INewAttributeV2 {
    name: string;
    description: string;
    responseType: string;
    allowOther: boolean;
    attributeType: string;
}

export interface IAttributeV2 {
    description: string;
    descriptionKey?: string;
    fieldName: string;
    id: string;
    name: string;
    nameKey?: string;
    required: boolean;
    responseType: string;
    attributeType: string;
    status: string;
    readOnly?: boolean;
}

export interface IAttributeDetailV2 extends IAttributeV2 {
    allowOther: boolean;
    values: IAttributeOptionV2[];
    filteredValues?: IFormattedAttributeOption[];
}

export interface IInventoryListV2Column extends IAttributeDetailV2 {
    cellType?: TableColumnTypes;
}

export interface IAttributeOptionV2 {
    id?: string;
    value: string;
    valueKey?: string;
    status?: string;
    label?: string;
}

export interface IRelatedListOption {
    id: string;
    name: string;
    label: string;
    organization: IValueId<string>;
    location?: IValueId<string>;
    type?: IValueId<string>;
    primaryOperatingLocation?: IValueId<string>;
}

export interface IDataCategory {
    categoryId: string;
    categoryValue: string;
    elements: IInventoryCellValue[];
    categoryKey?: string;
    displayCategoryValue?: string;
}

export interface IAttributeModalResolve {
    type: string;
    attribute?: IAttributeDetailV2;
    getAttribute?: () => ng.IPromise<IAttributeDetailV2>;
    saveAttribute: (attribute: IAttributeDetailV2) => ng.IPromise<IProtocolResponse<IAttributeDetailV2>>;
    callback: (attribute: IAttributeDetailV2) => void;
    schemaId: string;
}

export interface IAttributeTypeOption {
    name: string;
    type: string;
}

export interface IAttributeEditOptionModalResolve {
    option: IAttributeOptionV2;
    attributeId: string;
    callback: (attributeId: string) => void;
}

export interface IAttributeOptionStatusModalResolve {
    title: string;
    warningMessage: string;
    option: IAttributeOptionV2;
    attributeId: string;
    newStatus: string;
    callback: (attributeId: string) => void;
}

export interface IAttributeStatusModalResolve {
    title: string;
    warningMessage: string;
    attributeId: string;
    schemaId: string;
    newStatus: string;
    callback: () => void;
}

export interface ILinkOption {
    name: string;
    id: string;
}

export interface IListOption {
    Id: string;
    Name: string;
    OrgGroup: string;
    OrgGroupId: string;
}

export interface ILinkedElement {
    inventoryId: string;
    inventoryAssociationId: string;
}

export interface ILinkPersonalData {
    dataElement: { id: string };
    dataSubjectType: { id: string };
}
export interface IInventoryAttributeV2 {
    [key: string]: string | Date | IAttributeOptionV2 | IAttributeOptionV2[] | IValueId<string> | boolean | IInventoryStatusOption;
}

export interface IInventoryRecordV2 extends IInventoryAttributeV2 {
    id?: string;
    number?: string;
}

export interface IInventoryRecordResponseV2 {
    data: IInventoryRecordV2;
}

export interface IFormattedAttribute extends IAttributeDetailV2 {
    values: IFormattedAttributeOption[];
}

export interface IFormattedAttributeOption extends IAttributeOptionV2 {
    label?: string;
}

export interface IAttributeLink {
    route: string;
    params: IAttributeParams[];
}

export interface IAttributeParams {
    id: string;
    inventoryId: number;
}

export interface IAssetRelationshipTypeCheckbox {
    label: string;
    selected: boolean;
    id: RelationshipLinkType;
}

export interface IRelationshipTypeParams {
    description: string;
    descriptionKey: string;
    name: string;
    nameKey: string;
    selected?: boolean;
}

export interface IControlsTableRow {
    framework: string;
    controlId: string;
    description: string;
    dateAdded: string;
    verified: boolean;
}

export interface IInventoryListAction {
    buttonType: string;
    labelKey?: string;
    options?: IHeaderOption[];
    action?: () => void;
}

export interface IInventoryListManagerViewState {
    rows: any[];
    columns: IInventoryTableColumn[];
    listId: string;
    associatedListId: string;
    listReady: boolean;
}

export interface IInventoryListViewState {
    filterData: IFilterTree;
    filterDrawerOpen: boolean;
    loadingFilterMetaData: boolean;
    currentRecordId: string;
    currentPageNumber: number;
    inventoryId: number;
    attributeDrawerOpen: boolean;
    selectedRecords: IStringMap<boolean>;
    listReady: boolean;
    table: IInventoryTableV2;
    isExporting: boolean;
    isShowingSearchResults: boolean;
    isShowingFilteredResults: boolean;
    searchTerm: string;
}

export interface IInventoryTableV2 {
    rows: IInventoryRecordV2[];
    columns: IInventoryListV2Column[];
    metaData: IPageableMeta;
}

export interface IInventoryRiskTableRow extends IRiskDetails {
    actionId: number;
    justification: string;
    createdUTCDateTime: Date;
    riskScore: number;
    levelId: number;
    levelDisplayName: string;
    riskApproversId: string[];
    orgGroup: INameId<string>;
    source: IInventoryRiskReference;
    categoryNames?: string[];
    categoriesCsv?: string;
}

export interface IInventoryRiskReference {
    id: string;
    type: string;
    name: string;
    additionalAttributes: IAdditionalAttributes;
}

export interface IInventoryRiskTable extends IPageable {
    content: IInventoryRiskTableRow[];
}
export interface IContractRelatedRow {
    inventoryId: string;
    inventoryName: string;
    inventoryType: string;
    location: IContractLocationResponse;
    id: string;
    value: string;
    valueKey: string;
    orgGroupId: string;
    orgGroupName: string;
    viewOnly: boolean;
    inventoryAssociationTypeKey?: string;
}

interface IContractLocationResponse {
    id: string;
    value: string;
    valueKey: string;
}

export interface IInventorySetting {
    name: string;
    value: string | boolean;
    valueType: string;
    options?: Array<ILabelValue<any>>;
    selectedOption?: ILabelValue<any>;
}

export interface IInventorySettingList {
    data: IInventorySetting[];
}

export interface IChangeInventoryStatusModalInputData {
    inventoryName?: string;
    recordIds: string[];
    selectedInventoryStatus?: InventoryStatusOptions;
    inventoryId: string;
}

export interface IInventoryStatus {
    key: InventoryStatusOptions;
}

export interface IInventoryStatusOption extends IInventoryStatus {
    name?: string;
    description?: string;
}

export interface IViewMoreParams {
    tableParam: IFetchDetailsParams;
    showContract?: boolean;
}

export interface IFetchDetailsParams {
    type?: number;
    page: number;
    size: number;
    sort?: string;
}

export interface IAttributeColumn<T> {
    label: string;
    getCellValue: (row: T) => string;
    isDetailLink?: boolean;
    tooltipKey?: string;
}

export interface IInventoryControlsTableRow {
    categoryId: string;
    categoryName: string;
    categoryNameKey: string;
    description: string;
    frameworkId: string;
    frameworkName: string;
    id: string;
    identifier: string;
    name: string;
}

export interface IInventoryRiskControlsFilter {
    field: string;
    operator: InventoryRiskControlsFilterOperator;
    value: string[];
}

export interface IInventoryRiskDetail {
    riskScore: string;
    riskLevel: string;
    record?: IInventoryRecordV2;
}
