export interface IRiskActionButtonConfig {
    name: string;
    identifier: string;
    isDisabled: boolean;
    isLoading: boolean;
    id: string;
}
