import { IStringMap } from "interfaces/generic.interface";
import { IPageableMeta } from "interfaces/pagination.interface";

export interface IFormattedActivity {
    id: string;
    title: string;
    description: string;
    timeStamp: string;
    icon: string;
    iconColor: string;
    iconBackgroundColor: string;
    changes: IAuditChange[];
    associations?: IAuditAssociation[];
    fieldLabel?: string;
    showOldNewValueField?: boolean;
    oldFieldLabel?: string;
    newFieldLabel?: string;
}

export interface IAuditHistoryUser {
    email: string;
    fullName: string;
    userId: string;
    internal: boolean;
    disabled: boolean;
}

export interface IAuditHistoryItem {
    changeDateTime: string;
    description: string;
    objectId: string;
    recordedDateTime: string;
    userDetail: IAuditHistoryUser;
    userId?: string;
    userName?: string;
    changes: IAuditChange[];
    associations?: IAuditAssociation[];
    source?: string;
}

export interface IAuditAssociation {
    description: string;
    action: string;
    objects: IAuditPersonalData[];
}

export interface IAuditPersonalData {
    id: string;
    name: string;
    type: string;
}

export interface IAuditChange {
    description: string;
    fieldLabel?: string;
    fieldName: string;
    newValue: IStringMap<string>;
    oldValue: IStringMap<string>;
}

export interface IGetPageableAudit extends IPageableMeta {
    content: IAuditHistoryItem[];
}
