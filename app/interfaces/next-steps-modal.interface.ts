import { IProject } from "interfaces/project.interface";

export interface INextStepsModalData {
    Project: IProject;
    EnableRiskAnalysis: boolean;
}
