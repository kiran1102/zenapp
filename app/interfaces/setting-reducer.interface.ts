import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IRiskSettings,
    IProjectSettings,
    IHelpSettings,
} from "interfaces/setting.interface";
import { ISmtpSetting } from "interfaces/smtp-settings.interface";
import { IConsentSettings } from "interfaces/consent-settings.interface";

export interface ISettingState {
    projectSettings: IProjectSettings;
    isFetchingProjectSettings: boolean;
    riskSettings: IRiskSettings;
    isFetchingRiskSettings: boolean;
    smtpSetting: ISmtpSetting;
    tempSmtpSetting: ISmtpSetting;
    isFetchingSmtpSetting: boolean;
    isTogglingSmtpConnection: boolean;
    helpSettings: IHelpSettings;
    isFetchingHelpSettings: boolean;
    hasPendingSmtpSettingChange: boolean;
    isIE11: boolean;
    consentSettings: IConsentSettings;
    hasPendingConsentSettingsChange: boolean;
    isFetchingConsentSettings: boolean;
}

export type IRiskSettingSelector = OutputSelector<IStoreState, IRiskSettings, (settingState: ISettingState) => IRiskSettings>;
export type IRiskSettingsFetchStatus = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IProjectSettingSelector = OutputSelector<IStoreState, IProjectSettings, (settingState: ISettingState) => IProjectSettings>;
export type IProjectSettingsFetchStatus = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IHeatMapEnabledSelector = OutputSelector<IStoreState, boolean, (riskSettings: IRiskSettings) => boolean>;
export type IRiskSummaryEnabledSelector = OutputSelector<IStoreState, boolean, (projectSettings: IProjectSettings) => boolean>;
export type IQuestionApprovalEnabledSelector = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type ISmtpSettingSelector = OutputSelector<IStoreState, ISmtpSetting, (settingState: ISettingState) => ISmtpSetting>;
export type ISmtpSettingFetchStatus = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type ISmtpConnectionEnabledSelector = OutputSelector<IStoreState, boolean, (smtpSettings: ISmtpSetting) => boolean>;
export type ISmtpConnectionToggleSelector = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IHelpSettingSelector = OutputSelector<IStoreState, IHelpSettings, (settingState: ISettingState) => IHelpSettings>;
export type IHelpSettingsFetchStatus = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type ISmtpSettingHasPendingChangeSelector = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IIsIE11Selector = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IConsentSettingsSelector = OutputSelector<IStoreState, IConsentSettings, (settingState: ISettingState) => IConsentSettings>;
export type IConsentSettingsHasPendingChangeSelector = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
export type IConsentSettingsFetchStatus = OutputSelector<IStoreState, boolean, (settingState: ISettingState) => boolean>;
