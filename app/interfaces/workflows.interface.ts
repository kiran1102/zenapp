import { WorkflowsStatus } from "dsarModule/enums/workflows.enum";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IKeyValue } from "interfaces/generic.interface";

export interface IWorkflow {
    name: string;
    workflowStatus: WorkflowsStatus;
    referenceId: string;
    id: string;
    version: number;
    workflowDetailDtos: IWorkflowItem[];
    mfaEnabled?: boolean;
}

export interface IWorkflowItem {
    id?: string;
    stage: string;
    guidanceText?: string;
    status?: number;
    order?: number;
    hidden?: boolean;
    language?: string;
    canDelete?: boolean;
    labelColor?: string;
    allowRequestQueueUpdate?: boolean;
    beginDeadlineCalc: boolean;
    allowDeadlineEdit?: boolean;
    workFlowSettingsOutput?: Array<IKeyValue<boolean>>;
}

export interface IWorkflowRequest {
    name: string;
    currentStatus: WorkflowsStatus;
    nextStatus: WorkflowsStatus;
    id: string;
    deletedStages?: string[];
    language: string;
    version: number;
    workflowDetailRequest?: IWorkflowItem[];
    mfaEnabled?: boolean;
}

export interface IWorkflowPutData {
    name: string;
    workflowStatus: WorkflowsStatus;
    id: string;
    language: string;
}

export interface ICreateWorkflow extends IWorkflowPutData {
    referenceId: string;
}

export interface IEditTitle {
    workflowName: string;
    workflowId?: string;
    workflowReferenceId?: string;
}

export interface IPublishedWorkflow extends IEditTitle {
    referenceId: string;
}

export interface ICreateWorkflowModalResolve {
    promiseToResolve: (workflow: ICreateWorkflow) => ng.IPromise<IProtocolResponse<IWorkflow>>;
    modalTitle: string;
    confirmationText: string;
    submitButtonText: string;
    cancelButtonText: string;
}

export interface IWebformPutWorkflow {
    workflowName: string;
    workflowRefId: string;
}
