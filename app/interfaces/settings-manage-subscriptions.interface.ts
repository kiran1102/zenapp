export interface ISubscriptionManager {
    UserId: string;
    Email: string;
    FirstName: string;
    LastName: string;
    FullName: string;
}
