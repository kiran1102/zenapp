export interface ISelfServiceConfig {
    TemplateId: string;
    TemplateVersion: number;
    method?: string;
    url?: string;
}
