export interface IAttachment {
    name?: string;
    url?: string;
    type?: string;
    icon?: string;
    showDeleteIcon?: boolean;
    isDeleted?: boolean;
    deleteDescriptionText?: string;
}
