export interface IAccessLevel {
    id: string;
    isDefault: boolean;
    organizationId: string;
    organizationName?: string;
    roleId: string;
    roleName?: string;
}

export interface IAccessLevelLegacy {
    Id: string;
    IsDefault: boolean;
    OrganizationId: string;
    RoleId: string;
}
