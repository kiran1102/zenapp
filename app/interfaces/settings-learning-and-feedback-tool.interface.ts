export interface ILearningAndFeedbackSetting {
    enabled: boolean;
    toolName: string;
    id: string;
}

export interface ILearningAndFeedbackUserSetting {
    enabled: boolean;
    toolName: string;
    id: string;
}
