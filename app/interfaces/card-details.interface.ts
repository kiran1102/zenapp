export interface ICardDetails {
    cardHeaderStyle: string;
    cardHeaderLogoUrl: string;
    cardName: string;
}
