import { IUser } from "interfaces/user.interface.ts";

export interface ISalesforceInfo {
    orgid?: string;
    salesforceUrl?: string;
}

export interface ISkuManagementModalInfo extends ISalesforceInfo {
    title?: string;
    licenseModalContent?: any;
    buttonType?: string;
    buttonLink?: string;
    action?: string;
    button?: string;
    buttonAction?: string;
    modalType?: string;
    pricingLink?: string;
    currentUser?: IUser;
    successType?: string;
}

export interface ISkuManagementContactSalesInfo extends ISalesforceInfo {
    tenantId: string;
    company: string;
    fullName?: string;
    firstName?: string;
    lastName?: string;
    email: string;
    interestedDesc?: string;
    contactNumber?: number;
}
