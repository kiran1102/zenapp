import { OutputSelector } from "reselect";
import {
    IStoreState,
    IStringToBooleanMap,
} from "interfaces/redux.interface";
import {
    IRisk,
    IRiskDetails,
    IRiskPaneFilter,
} from "interfaces/risk.interface";

export type IRiskMap = {} | Map<string, IRisk | IRiskDetails>;

export interface IRiskState {
    allIds: string[];
    byId: IRiskMap;
    tempModel: IRiskMap;
    selectedRiskIds: IStringToBooleanMap;
    pendingRiskIds: string[];
    riskFilter: IRiskPaneFilter;
}

export type IRiskByIdSelector = OutputSelector<IStoreState, IRisk | IRiskDetails, (riskState: IRiskState) => IRisk | IRiskDetails>;
export type IRiskByIdSelectorFactory = (id: string) => IRiskByIdSelector;
export type ISelectedRisksMapSelector = OutputSelector<IStoreState, IStringToBooleanMap, (riskState: IRiskState) => IStringToBooleanMap>;
export type ISelectedRiskIdsSelector = OutputSelector<IStoreState, string[], (idMap: IStringToBooleanMap) => string[]>;
export type IAllRiskIdsSelector = OutputSelector<IStoreState, string[], (riskState: IRiskState) => string[]>;
export type IAllRisksSelector = OutputSelector<IStoreState, IRiskMap, (riskState: IRiskState) => IRiskMap>;
export type IAllRisksInOrderSelector = OutputSelector<IStoreState, Array<IRisk | IRiskDetails>, (allModels: IRiskMap, allIds: string[]) => Array<IRisk | IRiskDetails>>;
export type IPendingRiskModelsSelector = OutputSelector<IStoreState, IRisk[], (allRisks: IRisk[]) => IRisk[]>;
export type IPendingRiskModelIdsSelector = OutputSelector<IStoreState, string[], (pendingRisks: IRisk[]) => string[]>;
export type INonPendingRiskModelsSelector = OutputSelector<IStoreState, IRisk[], (allRisks: IRisk[]) => IRisk[]>;
export type INonPendingRiskModelIdsSelector = OutputSelector<IStoreState, string[], (pendingRisks: IRisk[]) => string[]>;
