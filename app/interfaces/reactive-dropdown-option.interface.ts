export interface IReactiveDropdownOption {
    readonly text?: string;
    readonly textKey?: string;
    readonly icon?: string;
    readonly route?: string;
    readonly [index: string]: any;
    readonly isDisabled?: boolean;
    readonly toolTip?: string;
    readonly toolTipDirection?: string;
    readonly value?: any;
}
