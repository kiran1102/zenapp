import { IFilterIteratee } from "interfaces/redux.interface";
import { Selector, OutputSelector } from "reselect";
import { IStoreState, IStringToBooleanMap } from "interfaces/redux.interface";
import { IOrganization, IOrganizationMap } from "interfaces/org.interface";
import { IUser } from "interfaces/user.interface";

export type IUserMap = {} | Map<string, IUser>;

export interface IUserState {
    allIds: string[];
    byId: IUserMap;
    selectedUserIds: IStringToBooleanMap;
    currentUser: IUser;
}

export type ICurrentUserSelector = OutputSelector<IStoreState, IUser, (userState: IUserState) => IUser>;
export type IUserByIdSelector = OutputSelector<IStoreState, IUser, (userState: IUserState) => IUser>;
export type IUserByIdSelectorFactory = (id: string) => IUserByIdSelector;
export type IAllUserIdsSelector = OutputSelector<IStoreState, string[], (userState: IUserState) => string[]>;
export type IAllUserModelsSelector = OutputSelector<IStoreState, IUserMap, (userState: IUserState) => IUserMap>;
export type IAllUsersSelector = OutputSelector<IStoreState, IUser[], (allModels: IUserMap, ids: string[]) => IUser[]>;
export type ISelectedUserIdsSelector = OutputSelector<IStoreState, IStringToBooleanMap, (userState: IUserState) => IStringToBooleanMap>;
export type ICurriedFilterUsersSelector = OutputSelector<IStoreState, IUser[], (users: IUser[]) => IUser[]>;
export type ICurriedFilterUsersSelectorFactory = (iteratee: IFilterIteratee<IUser>) => ICurriedFilterUsersSelector;
