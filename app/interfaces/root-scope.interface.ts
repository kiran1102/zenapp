import { IStringMap } from "interfaces/generic.interface";
import { ILanguage } from "interfaces/localization.interface";
export interface IExtendedRootScopeService extends ng.IRootScopeService, ITranslateFunction {
    APICache: any;
    customTerms: any;
    eulaModalOpen: boolean;
    tenantLanguage: string;
    languages: ILanguage[];

    // Routing Properties
    contextSwitch: (cb: () => any) => void;
    $state: ng.ui.IStateService;
    SaveReturnUrl: boolean;
    origin: string;

    // Invite Module Properties
    GetModule: (module: string) => IStringMap<any>;

}

export interface ITranslateFunction {
    t: (key: string, data?) => string;
}
