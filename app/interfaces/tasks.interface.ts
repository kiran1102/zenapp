import { IDropdownOption } from "interfaces/one-dropdown.interface";

export interface ITaskSettings {
    Enabled: boolean;
}

export interface ITaskListRequest {
    filterOptions: any[];
    pageNumber: number;
    pageSize: number;
    sortOptions: any[];
    user: string;
}

export interface ITask {
    assignee?: string;
    canComplete?: boolean;
    canDelete?: boolean;
    canModify?: boolean;
    canReopen?: boolean;
    createdBy?: string;
    createdDate?: string;
    creator?: string;
    description: string;
    details?: ITaskDetail[];
    downloadId?: string;
    dueDate: string;
    dueDateToolTip?: string;
    expirationJobId?: number;
    id?: number;
    isDataDiscoveryLink?: boolean;
    isFailed?: boolean;
    isFileDownload?: boolean;
    lastModifiedBy?: string | null;
    lastModifiedDate?: string;
    localeDueDate?: string;
    name: string;
    reminderDate?: string;
    reminderJobId?: number;
    reportId?: string;
    reportName?: string;
    stateLabel?: string;
    stateName?: string;
    taskStatusId?: number;
    type?: string;
    updatedDate?: Date | null;
    linkPressed?: boolean;
    isAssessment?: boolean;
    assessmentId?: string;
    dropDownMenu?: IDropdownOption[];
}

export interface ITaskDetail {
    type: string;
    value: string;
}

export interface ITaskFilterOption {
    filterType: string;
    filterValue: string;
}
