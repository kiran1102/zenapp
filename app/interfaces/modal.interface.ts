export interface IModalConfig {
    backdrop?: string;
    controller?: string;
    keyboard?: boolean;
    resolve?: any;
    size?: string;
    template?: string;
    templateUrl?: string;
    windowClass?: string;
}

export interface IModalData {
    promiseToResolve: () => void;
    discardCallback: () => void;
    modalTitle?: string;
    saveButton?: string;
    descText?: string;
    cancelButtonText?: string;
}
