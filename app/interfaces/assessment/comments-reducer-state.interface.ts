// Interface
import { IQuestionComment } from "interfaces/aa-question-comment.interface";

export interface ICommentReducerState {
    allIds: string[];
    byId: {} | Map<string, IQuestionComment>;
    byQuestionId: Map<string, IQuestionComment[]>;
}
