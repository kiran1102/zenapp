import { INeedsMoreInfoState } from "interfaces/assessment/needs-more-info.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IAssessmentDetailState } from "interfaces/assessment/assessment-detail-state.interface";
import { IAssessmentCommentState } from "interfaces/assessment/assessment-comment-reducer.interface";
import { INoteState } from "interfaces/note.interface";

export type IAssessmentMap = {} | Map<string, IAssessmentModel>;

export interface IAssessmentState {
    allIds: string[];
    byId: IAssessmentMap;
    selectedAssessmentIds: IAssessmentMap;
    aaNeedsMoreInfoDetail: INeedsMoreInfoState;
    assessmentDetail: IAssessmentDetailState;
    assessmentComment: IAssessmentCommentState;
    preSelectedRiskId: string;
    preSelectedRootRequestId: string;
    preSelectedCommentQuestionId: string;
    notes: INoteState;
}
