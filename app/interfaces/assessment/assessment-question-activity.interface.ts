export interface IQuestionActivityFormatted {
    id: number;
    title: string;
    timeStamp: string;
    icon: string;
    iconColor: string;
    iconBackgroundColor: string;
    changes: IQuestionActivityStreamActivity[];
}

export interface IQuestionActivityStreamActivity {
    activityType: string;
    fieldName: string;
    newValue: string;
    oldValue: string;
}

export interface IQuestionActivityContent {
    activities: IQuestionActivityStreamActivity[];
    timeStamp: string;
    userId: string;
    userName: string;
}

export interface IQuestionActivityParams {
    page: number;
    size: number;
    filter: string;
    text: string;
    sort: string;
}

export interface IQuestionActivityModalData {
    assessmentId: string;
    questionId: string;
}
