import { IRiskStatistics } from "interfaces/risk.interface";

export interface ISectionHeaderInfo {
    description: string | null;
    hidden: boolean;
    name: string;
    invalidQuestionIds: string[];
    requiredQuestionIds: string[];
    requiredUnansweredQuestionIds: string[];
    unansweredQuestionIds: string[];
    respondent: {
        id: string,
        name: string,
    };
    riskStatistics?: IRiskStatistics;
    rootRequestInfoCount: number;
    sectionId: string;
}
