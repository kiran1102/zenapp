export interface IAssessmentQuestionViewState {
    canViewQuestion: boolean;
    isQuestionDisabled: boolean;
}
