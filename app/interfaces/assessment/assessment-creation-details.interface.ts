import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IAssessmentCreationDetails {
    approverId?: string;
    approverName?: string;
    comments?: string;
    deadline?: string;
    description?: string;
    inventoryDetails?: IInventoryDetails;
    incidentDetails?: IIncidentDetails;
    isValidReminder?: boolean;
    name: string;
    orgGroupId: string;
    orgGroupName: string;
    reminder?: number;
    respondentInvitedUser?: string;
    respondentModel?: string;
    approvers?: IAssessmentApproverDetails[];
    respondents: IAssessmentRespondentDetails[];
    approversOrgUserList?: IOrgUserAdapted[];
    respondentsOrgUserList?: IOrgUserAdapted[];
    showDetails?: boolean;
    templateId: string;
    templateName?: string;
    showDuplicateWarning?: boolean;
    tags?: string[];
}

export interface IInventoryDetails {
    inventoryId: string;
    inventoryTypeId: number;
    inventoryName: string;
    inventoryNumber?: string;
}

export interface IIncidentDetails {
    incidentId: string;
    incidentName: string;
}

export interface IAssessmentRespondentDetails {
    comment?: string;
    respondentId: string;
    respondentName: string;
}
export interface IAssessmentApproverDetails {
    comment?: string;
    approverId: string;
    approverName: string;
}

export interface IAssessmentApproverDetails {
    approverId: string;
    approverName: string;
    comment?: string;
}
