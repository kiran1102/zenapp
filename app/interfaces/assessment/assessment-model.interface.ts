import { AssessmentStatus } from "enums/assessment/assessment-status.enum";
import { ISectionModel } from "interfaces/assessment/section-model.interface";
import { ISectionHeaderReducerState } from "interfaces/assessment/section-header-reducer-state.interface";
import { IFlattenedState } from "interfaces/redux.interface";
import {
    IQuestionModel,
    IQuestionModelOption,
} from "interfaces/assessment/question-model.interface";
import {
    IStringMap,
    INameId,
    ICommentNameId,
} from "interfaces/generic.interface";
import { IAssessmentApproverDetails, IAssessmentRespondentDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IAssessmentModelBase {
    assessmentId: string;
    canApproveAssessment: boolean;
    canSubmitAssessment: boolean;
    createdBy: {
        id: string,
        name: string,
    };
    createdDT: string;
    displayState: "REFRESH_ALL" | "REFRESH_NONE" | "REFRESH_PARTIAL";
    deadline: string | null;
    description: string;
    firstSection: boolean;
    lastSection: boolean;
    laws: string[];
    hasRequestInformation: boolean;
    invalidQuestionToRiskIdMap: IStringMap<string[]>;
    inventoryLinks: IAssessmentInventoryLink[];
    name: string;
    orgGroup: {
        id: string,
        name: string,
    };
    questions: IFlattenedState<IQuestionModel>;
    // TODO: remove once multiple a/r is done
    respondent: ICommentNameId<string>;
    section: ISectionModel;
    sectionHeaders: ISectionHeaderReducerState;
    status: AssessmentStatus;
    template: IAssessmentTemplate;
    welcomeSection: boolean;
    welcomeText: string | null;
    reminder?: number;
    comment?: string;
    isReminderValid?: boolean;
    showDetails?: boolean;
    latestTemplateVersion?: boolean;
    tags?: IQuestionModelOption[];
    viewer?: boolean;
    reviewChangedResponses?: boolean;
    parentAssessmentId?: string;
    firstAssignedTemplateId?: string;
    editAllResponsesWhenInProgress: boolean;
}

export interface IAssessmentModel extends IAssessmentModelBase {
    approvers: IAssessmentApprover[];
    respondents: Array<ICommentNameId<string>>;
}

export interface IAssessmentCopyModel extends IAssessmentModelBase {
    approvers: IOrgUserAdapted[];
    respondents: IOrgUserAdapted[];
}

export interface IAssessmentInventoryLink {
    type: string;
    link: string;
}

export interface IAssessmentTemplate {
    id: string;
    name: string;
    templateType: string;
}

export interface IAssessmentApprover {
    id: string;
    name: string;
    comment?: string;
    approvedOn?: Date;
    approvalState?: string;
}

export interface IAssessmentApproverFooterViewModel extends IAssessmentApprover {
    approved?: boolean;
    stateIconClass?: string;
}
export interface IAssessmentCopyPayload {
    approvers: IAssessmentApproverDetails[] | IAssessmentApprover[] | IOrgUserAdapted[];
    comment?: string;
    description?: string;
    deadline: string;
    name?: string;
    orgGroupId: string;
    orgGroupName: string;
    reminder?: number;
    reviewChangedResponses: boolean;
    respondents: IAssessmentRespondentDetails[] | Array<INameId<string>> | IOrgUserAdapted[];
    latestTemplateVersion: boolean;
    sourceAssessmentId?: string;
}
export interface IAsessmentCopyContent {
    copyResponses: boolean;
    copyComments: boolean;
    copyAttachments: boolean;
    copyNotes: boolean;
}
export interface IChangedQuestion {
    content: string;
    description: string;
    friendlyName: string;
    hint: string;
    questionId: string;
    sectionId: string;
    questionIndex: number;
    sectionIndex: number;
}

export interface IChangedQuestionResponse {
    questionsChanged: IChangedQuestion[];
    totalQuestionCount: number;
    changedQuestionCount: number;
}

export interface IChangedResponse {
    originalResponse: string;
    currentResponse: string;
    originalJustification: string;
    currentJustification: string;
}

export interface IUpdatedTemplateQuestions extends IChangedQuestion {
    changeTypes: string[];
    deleted: boolean;
    disabled?: boolean;
}
export interface IUpdatedTemplateQuestionsResponse {
    questionsChanged: IUpdatedTemplateQuestions[];
}
