import { AssessmentStatus } from "enums/assessment/assessment-status.enum";

export interface IAssessmentRiskHeaderModel {
    name: string;
    status: AssessmentStatus;
    totalRiskCount: number;
    approvers: Array<{
        id: string,
        name: string,
    }>;
}
