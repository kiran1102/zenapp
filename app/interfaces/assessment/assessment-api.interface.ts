import {
    IQuestionResponseV2,
    IAssessementDataElementResponse,
    IPersonalDataQuestionResponseV2,
} from "interfaces/assessment/question-response-v2.interface";
import { IValueId } from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface InventoryOptionsResponse {
    id: string;
    name: string;
}

export interface IProcessingActivityInventoryOption extends InventoryOptionsResponse {
    organization: IValueId<string>;
}

export interface IAssetsInventoryOption extends InventoryOptionsResponse {
    location: IValueId<string>;
    organization: IValueId<string>;
}

export interface IEntitiesInventoryOption extends InventoryOptionsResponse {
    primaryOperatingLocation: IValueId<string>;
    organization: IValueId<string>;
}

export interface IVendorInventoryOption extends InventoryOptionsResponse {
    location: IValueId<string>;
    organization: IValueId<string>;
    type: IValueId<string>;
}
// TBD: Update when Data Elements Inventory API becomes available
export interface IDataElementsInventoryOption extends InventoryOptionsResponse {
    location: IValueId<string>;
    organization: IValueId<string>;
}

export interface IAssessmentMetadataRequest {
    name: string;
    description: string;
    orgGroupId: string;
    orgGroupName: string;
    deadline: string;
    reminder: number;
    updateApproverRequests: IAssessmentUpdateApproverRequest[];
    updateRespondentRequests: IAssessmentUpdateRespondentRequest[];
    tags: string[];
}

export interface IAssessmentUpdateApproverRequest {
    approverId: string;
    approverName: string;
    comment: string;
}

export interface IAssessmentUpdateRespondentRequest {
    comment?: string;
    respondentId?: string;
    respondentName: string;
    sectionId?: string;
}

export interface ISaveSectionResponsesContract {
    assessmentId: string;
    parentAssessmentDetailId: string;
    questionId: string;
    responses: IQuestionResponseV2[] | IPersonalDataQuestionResponseV2[];
    sectionId: string;
    responseMap?: IStringMap<IStringMap<IAssessementDataElementResponse[]>>;
}
