import { IRiskStatistics } from "interfaces/risk.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";

export interface ISectionHeaderReducerState {
    allIds: string[];
    byIds: Map<string, ISectionHeaderInfo>;
    riskStatisticsBySectionIds: Map<string, IRiskStatistics>;
}
