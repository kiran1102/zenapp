import { IStringMap } from "interfaces/generic.interface";
import { QuestionResponseType } from "enums/assessment/assessment-question.enum";
import { INameId } from "interfaces/generic.interface";

export interface IQuestionResponseV2 {
    response: string;
    responseId: string;
    type: QuestionResponseType;
    valid?: boolean;
}

export interface IQuestionModelResponse extends IQuestionResponseV2 {
    categoryById: IStringMap<string>;
    dataSubjectById: IStringMap<string>;
    responseMap: IStringMap<IStringMap<IAssessementDataElementResponse[]>>;
}

export interface IQuestionPersonalDataResponse extends IQuestionResponseV2 {
    responseMap: IPersonalDataUpdateResponseMap;
}

export interface IAssessementDataElementResponse {
    id: string;
    name: string;
    type: QuestionResponseType.Default | QuestionResponseType.Others;
}

export interface IPersonalDataQuestionResponse {
    categories: IPersonalDataCategory[];
    id: string;
    name: string;
}

export interface IPersonalDataCategory {
    categoryId: string;
    categoryValue: string;
    elementResponses: IAssessementDataElementResponse[];
}

export interface IPersonalDataUpdateResponseMap {
    DATA_SUBJECTS: INameId<string>;
    DATA_ELEMENTS: INameId<string>;
    DATA_CATEGORIES: INameId<string>;
}

export interface IPersonalDataElement {
    value: string;
    valueId?: string;
    valueKey?: string;
    displayValue?: string;
}

export interface IAssessementInvalidResponseMap {
    id: string;
    name: string;
    categories: IStringMap<{
        id: string;
        name: string;
        dataElements: IAssessementDataElementResponse[];
    }>;
}

export interface IPersonalDataQuestionResponseV2 extends IQuestionResponseV2 {
    responseMap: IStringMap<IStringMap<IAssessementDataElementResponse[]>>;
}
