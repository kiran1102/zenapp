export interface IAssessmentConditionsModalResolve {
    callback?: () => void;
    questionId: string;
    sectionId: string;
    previewMode: boolean;
}
