import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";

export interface IAssessmentCommentModel {
    commenter: string;
    commenterId: string;
    createdDT: string| Date;
    editable: boolean;
    id: string;
    refId: string;
    type: AANotesTypes;
    value: string;
}

export interface IAssessmentCommentCreationReq {
    type: AANotesTypes;
    value: string;
}

export interface IAssessmentCommentModaldata {
    id: string;
    type: AANotesTypes;
    value: string;
    isReadOnly: boolean;
    editMode: boolean;
    commenterId: string;

}
