export interface IApproverTextConfig {
    approverNMI: boolean;
    isMultiApprover: boolean;
    hasApprover: boolean;
    forceApprove: boolean;
    bypassOtherApproversEnabled: boolean;
    needsConfirmation: boolean;
}

export interface IApproverModalResolve {
    promiseToResolve: () => ng.IPromise<boolean>;
    modalTitle: string;
    approvalTextConfig: IApproverTextConfig;
}

export interface IUnapproveModalResolve {
    promiseToResolve: () => ng.IPromise<boolean>;
    modalTitle: string;
    unapproveConfirmationText: string;
}
