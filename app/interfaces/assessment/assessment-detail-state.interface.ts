import { IFlattenedState } from "interfaces/redux.interface";
import { IAssessmentModel } from "interfaces/assessment/assessment-model.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import { IAssessmentTemplate } from "modules/template/interfaces/template.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IAssessmentDetailState {
    assessmentModel: IAssessmentModel;
    expandedSectionAccordion: {} | Map<string, boolean>;
    inventoryOptions: IStringMap<IFlattenedState<IQuestionModelOption>>;
    inventoryUserOptions: IOrgUserAdapted[];
    isLoadingAssessment: boolean;
    isLoadingSection: boolean;
    isLoadingMessage: string;
    isSaveInProgress: boolean;
    isSubmittingAssessment: boolean;
    questionIdsBeingSaved: string[];
    questionIdsToSave: string[];
    selectedSectionId: string;
    subQuestionIdsBeingSaved: string[];
    subQuestionIdsToSave: string[];
    tempAssessmentModel: IAssessmentModel;
    bulkAssessmentDetailsList: IAssessmentCreationDetails[];
    templateMetadata: IAssessmentTemplate;
    invalidQuestionRisksIdMap: IStringMap<string[]>;
}
