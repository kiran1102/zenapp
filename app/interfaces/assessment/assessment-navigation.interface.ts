import { TemplateRuleActions } from "modules/template/enums/template-rule.enums";
import { LogicalOperators, ComparisonOperators } from "enums/operators.enum";

export interface IAssessmentNavigation {
    conditionType: TemplateRuleActions.SKIP;
    conditions: IAssessmentNavigationCondition[];
    destinationQuestionId: string;
    destinationSectionId: string;
    id: string;
    sourceQuestionId: string;
    sourceSectionId: string;
    // UI specific fields
    added: boolean; // specifies that the condition is newly added on UI, and will have a generated id
    invalid?: boolean;
    errorMessage?: string;
}

export interface IAssessmentNavigationCondition {
    logicalOperator?: LogicalOperators.AND | LogicalOperators.OR;
    comparisonOperator: ComparisonOperators.EQUAL_TO | ComparisonOperators.NOT_EQUAL_TO;
    optionId: string;
    optionType?: string;
}

export interface IAssessmentNavigationExpressionGroup {
    expressions: string[];
    logicalOperator: LogicalOperators.AND | LogicalOperators.OR;
    expressionType: "JSONPATH";
}
