// Interfaces
import { IPageableMeta } from "interfaces/pagination.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IGridColumn } from "interfaces/grid.interface";
import { ISharedAssessmentUsersContractById } from "interfaces/user.interface";
import { IAddedFilter } from "@onetrust/vitreus";
import {
    IFilterColumn,
    IFilter,
} from "interfaces/filter.interface";

// Enum
import { TableColumnTypes } from "enums/data-table.enum";

export interface IAssessmentTableRow {
    approver: string;
    assessmentId: string;
    assessmentRiskLevelId?: number;
    assessmentRiskLevelName?: string;
    assessmentRiskScore?: number;
    completedOn?: Date;
    createDT: Date;
    creator: string;
    deadline: Date;
    maxRiskLevelId?: number;  // will be deprecated soon replaced by assessmentRiskLevelId
    name: string;
    number: number;
    openInfoRequestCount: number;
    openRiskCount: number;
    orgGroup: string;
    orgGroupId: string;
    orgGroupName?: string;
    respondent: string;
    respondentIds?: string;
    status: string;
    templateId: string;
    templateName: string;
    templateRootVersionId: string;
    tags: string;
}

export interface IAssessmentTable extends IPageableMeta {
    content: IAssessmentTableRow[];
}

export interface IAssessmentFilterModel {
    statusFilter: string;
    templateFilter: string;
}

export interface IAssessmentFilterParams {
    field: string;
    operation: string;
    value: string | string[];
    toValue?: string | string[];
}

export interface IAAListFilter {
    preSelectedFilters: IAssessmentFilterParams[];
    activeAssessmentFilters: IFilter[];
}

export interface IAssessmentRelatedInventoryModal {
    modalTitle?: string;
    questionId?: string;
    isEditMode?: boolean;
    currentOrg: IStringMap<string>;
    submitButtonText?: string;
    cancelButtonText?: string;
    callback?: () => void;
}

export interface IAssessmentExportModel {
    name: string;
    entityId: string;
    reportExportFormat: string;
    reportEntityType: number;
    reportExportLayoutInfo: {};
}

export interface IShareAssessmentModalData {
    assessmentId: string;
    usersToWhichAssessmentsShared: ISharedAssessmentUsersContractById;
}

export interface IAssessmentColumns {
    activeColumns: IGridColumn[];
    disabledColumns: IGridColumn[];
}

export interface IAssessmentExportCriteria {
    reportExportFormat: string;
    savedReportView: {
        name: string;
        type: number;
        viewType: string;
        filterCriteria: IStringMap<IStringMap<IFilter[]> | any[]>,
        sortInfo: IAssessmentSort,
        visibleColumns: IFilterColumn[],
    };
}

export interface IAssessmentSort {
    columnName: string;
    ascending: boolean;
}
