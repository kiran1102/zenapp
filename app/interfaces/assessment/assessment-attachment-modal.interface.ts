export interface IAAAttachmentDetails {
    assessmentId: string;
    sectionId: string;
    questionId: string;
    attachmentId: string;
    fileName: string;
    fileDescription: string;
}

export interface IAAAttachmentModalData {
    assessmentId: string;
    questionId: string;
    sectionId: string;
    callback?: (attachmentsCount: number) => void;
}

export interface IAAAttachmentModalResponse {
    attachmentId: string;
    createdDate: string;
    fileDescription: string;
    fileName: string;
    creator: IAAAttachmentModalCreator;
}

export interface IAAAttachmentModalCreator {
    name: string;
    id: string;
}

export interface IUploadFile {
    lastModified: string;
    lastModifiedDate: string;
    name: string;
    size: number;
    type: string;
}
