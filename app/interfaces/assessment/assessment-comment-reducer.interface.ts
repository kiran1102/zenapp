import { IAssessmentCommentModel } from "interfaces/assessment/assessment-comment-model.interface";

export interface IAssessmentCommentState {
    commentModel: IAssessmentCommentModel[];
}
