import { IRiskReducerState } from "interfaces/assessment/risk-reducer-state.interface";
import { ISectionHeaderInfo } from "interfaces/assessment/section-header-info.interface";
import { IRiskStatistics } from "interfaces/risk.interface";
import { ICommentReducerState } from "interfaces/assessment/comments-reducer-state.interface";
import {
    IAssessmentApprover,
    IAssessmentTemplate,
} from "interfaces/assessment/assessment-model.interface";
import {
    INameId,
    IStringMap,
} from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";

export interface ISectionModel {
    description: string | null;
    hidden: boolean;
    name: string;
    requiredUnansweredQuestionIds: string[];
    // TODO: remove once multiple a/r is done
    respondent: {
        id: string,
        name: string,
    };
    respondents: [{
        id: string,
        name: string,
    }];
    header: ISectionHeaderInfo;
    riskStatistics: IRiskStatistics;
    sectionId: string;
    risks?: IRiskReducerState;  // computed by assessment-detail-adapter.service
}

export interface ISectionHeaderModel {
    name: string;
    org: {
        id: string,
        name: string,
    };
    // TODO: remove once multiple a/r is done
    respondent: {
        id: string,
        name: string,
    };
    respondents: Array<INameId<string>>;
    approverNames: string;
    approvers?: IAssessmentApprover[];
    // TODO: remove once multiple a/r is done
    creatorName: string;
    deadline: string;
    description: string;
    respondentNames?: string;
    reminder?: number;
    formattedDisplayDate?: string;
    viewState?: IStringMap<boolean>;
    selectedTags?: IQuestionModelOption[];
    template?: IAssessmentTemplate;
    closeCallback?: () => void;
}
export interface ICommentSectionModel extends ISectionModel {
    comments: ICommentReducerState;
}

export interface ISectionHeaderMetaDataModel {
    name: string;
    deadline: string;
    orgGroupId: string;
    orgGroupName: string;
    description: string;
    reminder: number;
    approvers: IAssessmentApprover[];
    respondents: Array<Array<INameId<string>>>;
}

export interface ISectionRespondentMetaDataModel {
    id: string;
    name: string;
}

export interface ISectionApproverMetaDataModel {
    approvalState: string;
    approvedOn: string;
    id: string;
    name: string;
}

export interface ISectionOrgUserAdapted {
    id: string;
    FullName: string;
    comment?: string;
}
