export interface IAssessmentValidApproverRespondentResolve {
    validApproverIds: string[];
    validRespondentIds: string[];
}
