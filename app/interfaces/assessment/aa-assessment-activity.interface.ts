export interface IActivityHistoryItem {
    timeStamp: string;
    description: string;
    id: string;
    recordedDateTime: string;
    userId: string;
    userName: string;
    activities: IActivityChange[];
}

export interface IActivityChange {
    fieldName: string;
    newValue: string;
    oldValue: string;
    activityType: string;
    assessmentName?: string;
}

export interface IFormattedActivity {
    id: string;
    title: string;
    timeStamp: string;
    icon: string;
    iconColor: string;
    iconBackgroundColor: string;
    activities: IActivityChange[];
    isOldNewLabelEmpty: boolean;
    fieldLabel: string;
    oldFieldLabel: string;
    newFieldLabel: string;
}

export interface IActivityMetaData {
    icon: string;
    iconHeader: string;
    iconColor: string;
    iconBackgroundColor: string;
    isOldNewLabelEmpty: boolean;
    fieldLabel: string;
    oldFieldLabel: string;
    newFieldLabel: string;
}

export interface IAssessmentActivityParams {
    page: number;
    size: number;
    filter: string;
    text: string;
    sort: string;
}
