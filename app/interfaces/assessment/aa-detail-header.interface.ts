import { AACollaborationCountsTypes } from "modules/assessment/enums/aa-detail-header.enum";

export interface ICollaborationPaneCounts {
    type: AACollaborationCountsTypes;
    count: number;
}
