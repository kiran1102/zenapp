import { ILabelValue } from "interfaces/generic.interface";

export interface INeedsMoreInfoModel {
    approverSendBackFlow: boolean;
    openInfoRequestCount: number;
    rootRequestInformationResponses: INeedsMoreInfo[];
}

export interface INeedsMoreInfo {
    rootRequestId: string;
    responseState: string;
    assessmentId: string;
    response: string;
    sectionId: string;
    questionId: string;
    canReopenWithAllowEditOption: boolean;
}

export interface INeedsMoreInfoState {
    needsMoreInfoModel: INeedsMoreInfoModel;
    filterState: ILabelValue<string>;
}

export interface INeedsMoreInfoResponse {
    id: string;
    rootRequestId: string;
    requestState: string;
    response: string;
    createdDt: string;
    creator: INeedsMoreInfoCreator;
}

export interface INeedsMoreInfoCreator {
    id: string;
    name: string;
}

export interface INeedsMoreInfoCreationDetails {
    assessmentId: string;
    sectionId: string;
    questionId: string;
    response: string;
    isResponseEditable?: boolean;
}

export interface INeedsMoreInfoApiResponse {
    id: string;
    createdBy: string;
    createdDate: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
    version: number;
    assessmentId: string;
    sectionId: string;
    questionId: string;
    response: string;
    requestState: string;
    rootRequestId: string;
    new: boolean;
}
