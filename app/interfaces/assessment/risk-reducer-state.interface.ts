import { IRiskMetadata } from "interfaces/risk.interface";

export interface IRiskReducerState {
    allIds: string[];
    byId: {} | Map<string, IRiskMetadata>;
    byQuestionId: Map<string, IRiskMetadata[]>;
}
