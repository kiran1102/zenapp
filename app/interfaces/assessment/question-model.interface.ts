// Interfaces
import { IFlattenedState } from "interfaces/redux.interface";
import {
    IQuestionAttributes,
    IQuestionDetails,
} from "interfaces/assessment-template-question.interface";
import {
    IQuestionResponseV2,
    IQuestionModelResponse,
    IAssessementInvalidResponseMap,
} from "interfaces/assessment/question-response-v2.interface";
import { IQuestionOptionAttributes } from "interfaces/gallery-templates.interface";
import { IRiskMetadata } from "interfaces/risk.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums
import { QuestionResponseType,
    QuestionLockReason,
    AAQuestionErrorCode,
} from "enums/assessment/assessment-question.enum";
import { AAParentQuestionType } from "modules/assessment/enums/aa-question.enums";

export interface IQuestionModel {
    attributes: IQuestionAttributes;
    canReopenWithAllowEditOption: boolean;
    content: string;
    description: string;
    errorCode: AAQuestionErrorCode;
    friendlyName: string;
    hidden: boolean;
    hint: string;
    id: string;
    justification: string;
    lockReason: QuestionLockReason;
    options: IQuestionModelOption[];
    parentQuestionId?: string;
    questionType: string;
    required: boolean;
    responseEditableWhileUnderReview: boolean;
    responses: IQuestionResponsesState<IQuestionModelResponse>;
    risks: IRiskMetadata[];
    sequence: number;
    subQuestions: IFlattenedState<IQuestionModelSubQuestion>;
    valid: boolean;
    totalComments: number;
    totalAttachments?: number;
    copyErrors?: string[];
}

export interface IQuestionModelSubQuestion {
    displayLabel: string;
    errorCode: AAQuestionErrorCode;
    justification: string;
    parentAssessmentDetailId: string;
    parentQuestionType: AAParentQuestionType;
    responses: IQuestionResponsesState<IQuestionModelResponse>;
    totalComments: number;
}

export interface IQuestionResponsesState<T> extends IFlattenedState<T> {
    invalidQuestionIds: string[];
    invalidResponsesMap?:  IStringMap<IAssessementInvalidResponseMap>;
}

export interface IQuestionModelOption {
    attributes?: IQuestionOptionAttributes;
    hint?: string;
    id: string;
    option: string;
    optionDisplay?: string;
    optionType?: QuestionResponseType;
    optionKey?: string;
    selected?: boolean;
    sequence?: number;
    valid?: boolean;
    translatedOption?: string;
}

export interface IQuestionHavingRisk {
    hidden?: boolean;
    justification?: string;
    question: IQuestionDetails;
    responses?: IQuestionResponseV2;
    risk?: IRiskMetadata;
}
