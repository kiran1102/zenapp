import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

export interface IModalState {
    modalStatus: string;
    modalName: string;
    modalData: any;
}

export type IModalStateSelector = OutputSelector<IStoreState, IModalState, (state: IModalState) => string>;
export type IModalStatusSelector = OutputSelector<IStoreState, string, (state: IModalState) => string>;
export type IModalNameSelector = OutputSelector<IStoreState, string, (state: IModalState) => string>;
export type IModalDataSelector = OutputSelector<IStoreState, any, (state: IModalState) => any>;
