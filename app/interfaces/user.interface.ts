import {
    IPaginationParams,
    IPaginationFilter,
} from "interfaces/pagination.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { ITenant } from "interfaces/tenant.interface";
import { IAccessLevel, IAccessLevelLegacy } from "interfaces/user-access-level.interface";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface IUserHandlerData {
    canBeDisabled?: boolean;
    canBeEdited?: boolean;
    canBeEnabled?: boolean;
    canResendEmail?: boolean;
}

export interface IUser extends IUserHandlerData {
    AccessLevels?: IAccessLevel[] | IAccessLevelLegacy[];
    CanDelete?: boolean;
    CanEdit?: boolean;
    CurrentTenant?: string;
    DisabledBy?: string | null;
    DisabledDT?: string | null;
    Email: string;
    EmailConfirmed?: boolean;
    ExpirationDT?: string | null;
    FirstName?: string;
    FromCache?: boolean;
    FullName: string;
    Id: string;
    IsActive: boolean;
    IsDirectoryUser?: boolean;
    IsInternal: boolean;
    IsLocked?: boolean;
    IsPasswordExpired?: boolean;
    IsReadOnly?: boolean;
    LanguageId?: number;
    LastName?: string;
    Onboarded?: boolean;
    OrgGroupId?: string;
    OrgGroupName?: string;
    RoleId?: string;
    RoleName?: string;
    RootOrgGroupId?: string;
    Tenants?: ITenant[] | null;
    EmailHash?: string;
    FirstNameLastName?: string;
}

export interface IUserManageContract {
    Id?: string;
    Email: string;
    UserName: string;
    FirstName: string;
    LastName: string;
    OrgGroupId?: string;
    RoleId?: string | null;
    AccessLevels?: IAccessLevel[] | IAccessLevelLegacy[];
    IsDirectoryUser: boolean;
    IsInternal: boolean;
    ExpirationDT?: Date | null;
}

export interface IUserActivateContract {
    UserId: string;
    ExpirationDT?: Date;
}

export interface IUserActivateModalData extends IConfirmationModalResolve {
    promiseToResolve: (date?: Date) => ng.IPromise<boolean>;
}

export interface IUserPaginationParams extends IPaginationParams<IPaginationFilter[]> {
    seedFilters?: IUserSeedFilters;
    tenantFilters?: IUserTenantFilters;
}

export interface IUserSeedFilters {
    enabled?: boolean;
    internal?: boolean;
    external?: boolean;
    beginDate?: string;
    endDate?: string;
    search?: string;
}

export interface IUserTenantFilters {
    organizationId?: string;
    roleId?: string[];
}

export interface IUserType {
    Email: string;
}

export interface IModifiedUserMetaData {
    user: IUserMetaData;
    checked: boolean;
}

export interface IUserRequestParams {
    userId: string;
    userName: string;
}

export interface ISharedAssessmentUsersContractById {
    userById: IStringMap<ISharedAssessmentUsersContract>;
}

export interface ISharedAssessmentUsersContract {
    UserId: string;
    UserFullName: string;
    OrgGroupId: string;
    OrgGroupName: string;
    RoleId: string;
    RoleName: string;
    Email: string;
}

export interface IUserMetaData extends IUserRequestParams {
    email: string;
}

export interface IUserDetail {
    email: string;
    fullName: string;
    userId: string;
    internal: boolean;
    disabled: boolean;
}
