export interface IDeleteConfirmationModalResolve {
    promiseToResolve?: () => ng.IPromise<any>;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText?: string;
    submitButtonText?: string;
    customConfirmationText?: string;
    customKeyText?: string;
    cancelIdentifier?: string;
    deleteIdentifier?: string;
    modalIcon?: string;
    modalIconTextClass?: string;
    callback?: () => void;
}
