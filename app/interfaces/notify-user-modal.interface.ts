export interface INotifyUserModal {
    modalTitle: string;
    notificationText: string;
    closeButtonText?: string;
}
