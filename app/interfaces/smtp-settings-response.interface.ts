export interface ISmtpSettingsResponse {
    links: any[];
    content: Array<{
        name: string,
        value: number | string | boolean,
        links: any[],
    }>;
    page: {
        size: number;
        totalElements: number;
        totalPages: number;
        number: number;
    };
}
