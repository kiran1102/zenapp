export interface IUpgradeModule {
    Name: string;
    UpgradeVideo: string;
}

export interface IShowModalData {
    name: string;
    video: string;
    state: number;
    contactSales?: boolean;
    modalOptions?: IUpgradeModalOptions | null | undefined;
}

export interface IModuleStates {
    Message: number;
    Video: number;
    Form: number;
    Submitted: number;
}

export interface IUpgradeModalOptions {
    modalTitle: string;
    modalText?: string;
}
