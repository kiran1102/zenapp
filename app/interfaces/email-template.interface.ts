export interface ITinymceOptions {
    entity_encoding: "named" | "numeric" | "raw";
    fullpage_default_title: string;
    fullpage_hide_in_source_view: boolean;
    menu: any;
    plugins: string;
    textcolor_cols: string;
    textcolor_rows: string;
    toolbar: boolean;
}

export interface ITemplate {
    id: string;
    guid: string;
    subject: string;
    bodyText: string;
    messageStateId: number;
    messageTypeId: number;
    version: string;
    parentMessageTypeId: string;
    languageId?: number;
}

export interface IButtonColors {
    buttonBgColor: string;
    buttonFgColor: string;
}

export interface IButtonColorStyle {
    "background-color": string;
    "border-color": string;
    "color": string;
    "font-weight": string;
}

export interface IMinicolorOptions {
    position: string;
    defaultValue: string;
    animationSpeed: number;
    animationEasing: string;
    change: string;
    changeDelay: number;
    control: string;
    hide: string;
    hideSpeed: number;
    inline: boolean;
    letterCase: string;
    opacity: boolean;
    show: string;
    showSpeed: number;
}

export interface ISettingsTestEmailModalResolve {
    messageTypeId: number;
    modalTitle: string;
    submitEmailButtonText: string;
    cancelButtonText?: string;
    languageId?: number;
}

export interface ISaveMessageTemplate {
    result: boolean;
    data: IBindToTemplateScopeResponse;
}

export interface IBindToTemplateScopeResponse {
    bodyText: string;
    channelId?: number;
    id?: number;
    languageId?: number;
    messageStateId: number;
    messageTypeId: number;
    orgGroupId?: string;
    parentMessageTypeId: number;
    subject: string;
    templateDetailTranslationKey?: string;
    templateNameTranslationKey?: string;
    version: number;
    disabled?: boolean;
    guid?: string;
}

export interface IMessageTemplate {
    data: IMessageTemplateData;
    result: boolean;
}

export interface IMessageTemplateData {
    content: IBindToTemplateScopeResponse[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: IMessageTemplateDataSort[];
    totalElements: number;
    totalPages: number;
    name?: string;
}

export interface IMessageTemplateDataSort {
    ascending: boolean;
    descending: boolean;
    direction: string;
    ignoreCase: boolean;
    nullHandling: string;
    property: string;
}

export interface IResetEmailData {
    data: IBindToTemplateScopeResponse;
    result: boolean;
    status: number;
    time: number;
}

export interface IMessageTemplateRequestParams {
    languageCode: string;
    typeId: number;
    page: number;
    size: number;
    sort: string;
    messageStateId: number;
}

export interface IBrandingLanguageDataContent {
    name: string;
    orgGroupId: string;
    value: string;
    version: number;
}

export interface IUpdateBrandingLanguage {
    settings: { DEFAULTLANGUAGE: number };
    validate: boolean;
}

export interface ILanguageActivatorData {
    Activated: boolean;
    Code: string;
    Id: number;
    Name: string;
    Translated: boolean;
}
