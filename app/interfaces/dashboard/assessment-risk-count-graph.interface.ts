import { IAssessmentStatusCount } from "interfaces/dashboard/assessment-status-count.interface";

export interface IAssessmentRiskCountResponse {
    assessments: IAssessmentStatusCount[];
    orgGroupId: string;
    templateId: string;
    totalCount: number;
}
