export interface IAgingAssessmentDetailResponse {
    totalCount: number;
    content: IAgingAssessmentDetail[];
}

export interface IAgingAssessmentDetail {
    createDate: string;
    id: string;
    name: string;
    orgGroupId: string;
    status: string;
}

export interface IAssessmentAgingGraphData {
    Day: number;
    Id: string;
    Name: string;
    OrgGroup: string;
    StateDesc: string;
    StateName: string;
    Time: number;
}
