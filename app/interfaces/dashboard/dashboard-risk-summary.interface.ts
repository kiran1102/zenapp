
export interface IDashboardRiskSummaryResponse {
    totalCount: number;
    orgGroupId: string;
    risks: IAssessmentRiskSummaryDetails[];
}

export interface IAssessmentRiskSummaryDetails {
    level: string;
    count: number;
}

export interface IRiskSummaryGraphData {
    id: number;
    level: string;
    label: string;
    count: number;
    percentage: number;
    magnitude: number;
}
