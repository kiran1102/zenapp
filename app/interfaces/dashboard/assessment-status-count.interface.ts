export interface IAssessmentStatusCountResponse {
    assessments: IAssessmentStatusCount[];
    orgGroupId: string;
    totalCount: number;
}

export interface IAssessmentStatusCount {
    count: number;
    status: string;
}
