export interface ISspTemplateDetails extends ISspTemplate {
    directLaunchEnabled: boolean;
    icon: string;
    name: string;
    publishDate: Date;
    version: number;
}

export interface ISspTemplate {
    id?: string;
    rootVersionId: string;

}
export interface ISspTemplatesConfiguration extends ISspTemplate {
    directLaunchEnabled: boolean;
    approverSelectionAllowed: boolean;
    orgGroupSelectionAllowed: boolean;
    respondentSelectionAllowed: boolean;
    selfServiceEnabled?: boolean;
}

export interface ISspTemplatesRestrictView {
    restrictProjectOwnerView: boolean;
    restrictInvitedUserAttachmentAction?: boolean;
    restrictAssessmentCompletionWithOpenRisk?: boolean;
    infoRequestBulkEmailAllowed?: boolean;
}
