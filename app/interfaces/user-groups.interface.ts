export interface IUserGroup {
    id?: string;
    name: string;
    description?: string;
    createdBy?: string;
    createdDate?: string;
    lastModifiedBy?: string;
    lastModifiedDate?: string;
}

export interface IUserGroupDisplay extends IUserGroup {
    createdByDisplay?: string;
    lastModifiedByDisplay?: string;
}

export interface IUserGroupsModal {
    modalTitle: string;
    userGroupActions?: string;
    userGroupData?: IUserGroup;
    callback: Function;
    groupId?: string;
}

export interface IUserGroupsListRequest {
    filterOptions: IUserGroupsFilterOption[];
    pageNumber: number;
    pageSize: number;
    sortOptions: any[];
}
export interface IUserGroupsFilterOption {
    filterType: string;
    filterValue: string;
}