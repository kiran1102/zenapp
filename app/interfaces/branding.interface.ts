export interface IBrandingModel {
    BackgroundColor: string;
    IconData: string;
    IconFileName?: string;
    InheritsFrom: string;
    IsInherited: boolean;
    LogoData: string;
    LogoFileName?: string;
    PrimaryColor: string;
    SecondaryColor: string;
    TextColor: string;
    Title: string;
}

export interface IBrandingData {
    InheritsFrom: string | null;
    IsInherited: boolean;
    branding: IBranding;
}

export interface ICustomBranding {
    branding: IBranding;
}

export interface IBranding {
    logo?: ILogo;
    header?: IBrandingHeader;
}
export interface ILogo {
    name?: string;
    url: string;
}
export interface IBrandingHeader {
    icon?: ILogo;
    backgroundColor?: string;
    textColor?: string;
    title?: string;
}
