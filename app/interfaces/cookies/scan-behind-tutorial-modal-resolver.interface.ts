export interface IScanBehindTutorialModalResolver {
    modalTitle: string;
    closeButtonText: string;
    altText: string;
    note: string;
}
