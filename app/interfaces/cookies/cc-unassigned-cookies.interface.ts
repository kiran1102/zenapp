export interface IUnassignedCookies {
    name: string;
    description: string;
    canDelete: boolean;
    cookies: Object[];
    groupId: number;
    hosts: Object[];
    orderPreview: number;
    purposeGroupId: number;
    subGroups: Object[];
}
