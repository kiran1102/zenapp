import { ICookieNameId } from "interfaces/cookies/cookie-generic.interface";
import { IConsentPolicyVendorDetail } from "interfaces/cookies/consent-policy.interface";

export interface IRegionRuleDetail {
    groupStatuses: IConsentPolicyGroupDetail[];
    id: number;
    isDefault: boolean;
    name: string;
    regions: ICookieNameId[];
    settingDetails: IConsentPolicySetting;
}

export interface IConsentPolicyGroupDetail {
    id: number;
    isDntEnabled: boolean;
    name: string;
    optanonGroupId: number;
    status?: ICookieNameId;
}

export interface IConsentModelSelection extends ICookieNameId {
    impliedBehaviors: ICookieNameId;
}

export interface IConsentPolicySetting {
    ClickAcceptAll: string;
    ConsentModel: string;
    EnabledConsentLogging: string;
    EnableIAB: string;
    ExcludePages: string;
    NextPage: string;
    ScrollAcceptAll: string;
    ShowBanner: string;
    VendorVersionList: string;
    ConsentCookie: string;
}

export interface IRegionRulePayload {
    id?: number;
    name: string;
    regions: number[];
    otherSettings: Array<{ key: string; value: string }>;
    groupStatuses: Array<{ id: number; isDntEnabled: boolean }>;
}

export interface IRegionRuleSetting {
    consentModel: ICookieNameId;
    editMode?: boolean;
    enabledConsentLogging: boolean;
    enableIAB: boolean;
    groupStatuses: IConsentPolicyGroupDetail[];
    id: number;
    impliedBehaviors: IImpliedOptionDetail;
    isDefault: boolean;
    isToggled?: boolean;
    name: string;
    showBanner: boolean;
    showImpliedBehaviors: boolean;
    siteVisitorId: ICookieNameId;
    thirdPartyEuConsentCookie: boolean;
    vendorVersion: IConsentPolicyVendorDetail;
    regions: ICookieNameId[];
}

export interface IImpliedOptionDetail {
    id: number;
    key: string;
    name: string;
}

export interface IdomainGroupTableColumns {
    labelKey: string;
    cellValueKey?: string;
    tooltipDescriptionKey?: string;
}

export interface IRegionSavePayload {
    regionsToCreate: IRegionRulePayload[];
    regionsToUpdate: IRegionRulePayload[];
}
