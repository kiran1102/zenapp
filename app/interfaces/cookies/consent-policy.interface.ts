export interface IConsentPolicyMetadata {
    description: string;
    name: string;
    orgGroupName: string;
    orgId: string;
    isDefault: boolean;
}

export interface IConsentPolicyVendorDetail {
    ListId: number;
    ListName: string;
}
