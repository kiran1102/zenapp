import {
    BannerLanguageStatus,
    LanguageRequestMethod,
} from "enums/cookies/cookie-list.enum";

export interface IBannerLanguage {
    domainId: number;
    language: ILanguage;
    englishName: string;
    status: BannerLanguageStatus;
}

export interface IBannerLangReponse {
    languages: IBannerLanguage[];
    failedLanguges: IBannerLanguage[];
    refreshNeeded: boolean;
}

export interface ILanguage {
    culture: string;
    deleted: boolean;
    id: number;
    name: string;
    new: boolean;
    englishName: string;
    status?: string;
}

export interface IBannerLanguageRequestData {
    id?: number;
    method: LanguageRequestMethod;
    url?: string;
    domainId: number;
    culture: string;
    defaultLanguage: boolean;
    languageId: number;
}

export interface IBannerLanguageTableData {
    id: number;
    textdata: string;
    enabled: boolean;
    code: number;
    clang: string;
    name: string;
    isDefault: boolean;
    dirty: boolean;
}
export interface IBannnerLanguageModalData {
    masterLanguagesList: IBannerLanguageResponse[];
    webformLanguagesList: IBannerLanguageData[];
    templateId: string;
    bodyText?: string;
    domainId?: number;
    headerTitle?: string;
    onModalClose: any;
}

export interface IBannerLanguageData {
    id: number;
    name: string;
    culture: string;
    deleted?: boolean;
    englishName: string;
    new?: boolean;
}

export interface IBannerLanguageResponse {
    checked: boolean;
    defaultLanguage: boolean;
    language: IBannerLanguageData;
}

export interface IBannerLanguageRadio {
    code: number;
    id: number;
}
