export interface IPreviewScript {
    scriptData: string;
}

export interface IPreviewStyle {
    cssURL: string;
    logoURL: string;
}
