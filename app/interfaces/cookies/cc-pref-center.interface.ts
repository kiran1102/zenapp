export interface IPrefCenterPreviewCategory {
    StrictlyNecessaryCookies: string;
    PerformanceCookies: string;
}

export interface IPrefCenterData {
    details: IPrefCenterDetails;
    styling: IPrefCenterStyling;
    consentSettings: IPrefCenterConsentSettings;
    consentModels: IPrefCenterConsentModels;
    defaultStatuses: IPrefCenterDefaultStatus[];
    languageResources: IPrefCenterLanguageResources[];
    IsIABEnabled?: boolean;
    isDntEnabled?: boolean;
    IsConsentLoggingEnabled?: boolean;
}

export interface IPrefCenterDetails {
    title: string;
    privacyTitle: string;
    privacyText: string;
    cookiePolicyLink: string;
    cookiePolicyURL: string;
    allowAllCookiesButtonLabel: string;
    saveSettingsButtonLabel: string;
    activeLabelText: string;
    alwaysActiveLabelText: string;
    cookiesUsedLabelText: string;
    showCookiesList: boolean;
    linkToCookiepedia: boolean;
    inactiveLabelText: string;
    showSubGroupDescription: boolean;
    showSubGroupCookies: boolean;
    showPreferenceCenterCloseButton: boolean;
    vendorLevelOptOut: boolean;
    vendorLevelOptOutPreview: boolean;
    viewVendorConsent: string;
    vewVendorConsentPreview: string;
    iABVendorsLink: string;
    iABVendorsLinkPreview: string;
    backButton: string;
    backButtonPreview: string;
}

export interface IPrefCenterStyling {
    style: string;
    primaryColor: string;
    buttonColor: string;
    menu: string;
    menuHighlightColor: string;
    customCSS: string;
    logoFileName: string;
    logoFileData: string;
}

export interface IPrefCenterConsentSettings {
    consentModel: string;
    groupStatuses: IConsentSettingsGroupStatus;
    showSubgroupToggles: boolean;
}

export interface IConsentSettingsGroupStatus {
    groupName: string;
    status: number;
    groupLanguagePropertiesSetId: number;
    purposeGroupId: number;
    subGroups: ISubGroupsDetails;
    isExpanded?: boolean;
    orderPreview: number;
}

export interface ISubGroupsDetails {
    groupName: string;
    id: number;
    orderPreview: number;
}

export interface IPrefCenterConsentModels {
    id: number;
    deleted: boolean;
    new: boolean;
    Name: string;
}

export interface IPrefCenterDefaultStatus {
    id: number;
    deleted: boolean;
    new: boolean;
    Text: string;
}

export interface IPrefCenterLanguageResources {
    id: number;
    key: string;
    language: string;
    value: string;
    new: boolean;
}

export interface IPolicyData {
    consentSettings: IPrefCenterConsentSettings;
    consentModels: IPrefCenterConsentModels;
    defaultStatuses: IPrefCenterDefaultStatus[];
    languageResources: IPrefCenterLanguageResources[];
    CookiesText: string;
    CategoriesText: string;
    LifespanText: string;
    IsLifespanEnabled: boolean;
    isDntEnabled?: boolean;
}

export interface IPolicyConsentsetting {
    consentSettings: IPrefCenterConsentSettings;
    consentModels: IPrefCenterConsentModels;
    defaultStatuses: IPrefCenterDefaultStatus[];
    languageResources: IPrefCenterLanguageResources[];
    isDntEnabled: boolean;
}

export interface IValueId {
    id: number;
    value: string;
}

export interface IPreferenceCenterEmitData {
    toggleStatus: boolean;
    isToggleTrigger: boolean;
}

export interface IPreferenceCenterMinicolorOptions {
    position: string;
    defaultValue: string;
    animationSpeed: number;
    animationEasing: string;
    change: string;
    changeDelay: number;
    control: string;
    hide: string;
    hideSpeed: number;
    inline: boolean;
    letterCase: string;
    opacity: boolean;
    show: string;
    showSpeed: number;
}

export interface IPreferenceCenterDetailsFormInput {
    id: string;
    label: string;
    name: string;
    placeholder: string;
    model: string;
    required: boolean;
    errorMessage: string;
    type: string;
    visible: boolean;
}

export interface IPreferenceCenterDetailsFormToggle {
    visible: boolean;
    label: string;
    tooltip: boolean;
    tooltipText: string;
    id: string;
    isChecked: () => boolean;
    isDisabled: () => boolean;
}
