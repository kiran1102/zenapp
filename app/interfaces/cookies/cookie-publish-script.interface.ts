import { LicenseType } from "enums/cookies/cookie-limit.enum";

export interface IAggregator {
    original: string;
    originalFileBlobUrl: string;
    originalFileName: string;
    revised: string;
    revisedFileBlobUrl: string;
    revisedFileName: string;
}

export interface ICookieScriptVersionDiffResponse {
    cssFilesAggregator: IAggregator;
    jsFilesAggregator: IAggregator;
    languageDetectionToggle: boolean;
    useOneTrustJQuery: boolean;
}

export interface IJqueryVersionDetail {
    jqueryVersion: string;
    jqueryVersionId: number;
    selected: boolean;
}

export interface IPublishScriptPayload {
    DomainIds?: number[];
    DomainId?: number;
    RequireReConsent: boolean;
    JQueryVersionId: number;
}

export interface ICookieLimitResponse {
    Publish: boolean;
    IsAdmin: boolean;
    LicenseType: LicenseType;
    UpgradeEcommerce: string;
    UpgradeOther: string;
    SubcriptionURL: string;
    Email: string;
}
