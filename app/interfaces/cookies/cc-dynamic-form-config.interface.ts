import { ValidatorFn } from "@angular/forms";

export interface ICCDynamicFormConfig {
    controlName: string;
    controlType?: string;
    errorMessage?: string;
    hasError?: boolean;
    inputType?: string;
    isDisabled?: boolean;
    isRequired?: boolean;
    label?: string;
    labelKey?: string;
    options?: any[];
    otAutoId: string;
    placeholder?: string;
    placeholderKey?: string;
    validation?: ValidatorFn[];
    valueKey: string;
}
