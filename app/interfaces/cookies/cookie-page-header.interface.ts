export interface ICookiePageHeaderBreadcrumbItem {
    label?: string;
    labelKey?: string;
    otAutoId: string;
    params?: any;
    path?: string;
}

export interface ICookiePageHeaderButtonConfiguration {
    action?: () => void;
    isDisabled?: boolean;
    isLoading?: boolean;
    labelKey: string;
    otAutoId: string;
}

export interface ICookiePageHeaderBadgeConfiguration {
    text: string;
    type: string;
}

export interface ICookiePageHeaderContextMenuConfiguration {
    action: () => any;
    text: string;
}
