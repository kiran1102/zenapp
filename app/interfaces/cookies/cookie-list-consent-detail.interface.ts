import { IDropdownOption } from "interfaces/one-dropdown.interface";
export interface ICookieListConsentDetail {
    CategoriesText: string;
    consentModels: IConsentModelDetail[];
    consentSettings: IConsentSettingDetail;
    CookiesText: string;
    defaultStatuses: IDefaultStatusDetail[];
    isDntEnabled: boolean;
    IsIABEnabled: boolean;
    IsLifespanEnabled: boolean;
    languageResources: ILanguageResourceDetail[];
    LifespanDurationText: string;
    LifespanText: string;
    LifespanTypeText: string;
    SelectedList: number;
    selectedVendor?: ICookieVendorDetail;
    VendorLists: ICookieVendorDetail[];
    IsIabThirdPartyCookieEnabled: boolean;
    IsConsentLoggingEnabled: boolean;
}

export interface ICookieVendorDetail {
    description: string;
    id: number;
    name: string;
    new: boolean;
}

export interface IConsentModelDetail {
    deleted: boolean;
    id: number;
    Name: string;
    new: boolean;
}

export interface ILanguageResourceDetail {
    id: number;
    key: string;
    language: string;
    new: boolean;
    value: string;
}

export interface IDefaultStatusDetail {
    Text: string;
    deleted: boolean;
    id: number;
    new: boolean;
}

export interface IConsentSettingDetail {
    consentModel: number;
    showSubgroupToggles: boolean;
    groupStatuses: IConsentGroupStatusDetail[];
}

export interface IConsentGroupStatusDetail {
    customGroupId: string;
    groupId: number;
    groupLanguagePropertiesSetId: number;
    groupName: string;
    isExpanded?: boolean;
    orderPreview: number;
    purposeGroupId: number;
    status: number;
    selectedStatus?: IDefaultStatusDetail;
    subGroups: ISubGroupDetail[];
}

export interface ISubGroupDetail {
    customGroupId: string;
    groupName: string;
    id: number;
    orderPreview: number;
}

export interface ICreatePolicyRequestBody {
    PolicyName: string;
    OrgId: string;
    OrgName: string;
    Description: string;
    Default: boolean;
}

export interface IConsentPolicy {
    ConsentPolicyName: string;
    DefaultConsentModel: string;
    DefaultConsentPolicy: boolean;
    Description: string;
    id: number;
    NumberOfDomains: number;
    NumberOfRegion: number;
    OrgId: string;
    OrgName: string;
    Status: string;
    contextMenuOptions?: IDropdownOption[];
}
