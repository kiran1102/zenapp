export interface IPublishTemplateModalResolver {
    modalTitle: string;
    publishConfirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    templateId: number;
    callback?: () => void;
}

export interface IPublishTemplateDomain {
    title: string;
    id: number;
}

export interface ITemplateMultipleSelectPayload {
    previousValue: IPublishTemplateDomain[];
    currentValue: IPublishTemplateDomain[];
    change: IPublishTemplateDomain;
}

export interface ITemplateDomainDetails {
    DomainId: number;
    DomainName: string;
    HasScriptArchive: boolean;
    TemplateId: number;
    TemplateName: string;
}
