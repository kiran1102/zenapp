export interface ICookies {
    id: number;
    name: string;
    host: string;
    purpose: string;
    lifeSpan: string;
    session: boolean;
    thirdParty: boolean;
    isParentGroup?: boolean;
}

export interface IHosts {
    groupId: number;
    hostName: string;
    numberOfCookies: number;
    purpose: string;
}
