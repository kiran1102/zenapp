export interface IDomainScanSchedule {
    domain: string;
    canTenantLevelScheduledScan: boolean;
    emailRecipients: string;
    nextAudit: Date | null;
    periodInMonths: number;
}
