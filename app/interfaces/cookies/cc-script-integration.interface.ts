export interface IDomainScript {
    cdnScriptUrl: string;
    domainId: number;
    previewScriptUrl: string;
    publishedScriptUrl: string;
    testScriptUrl: string;
    useNoJQueryScript: boolean;
}
