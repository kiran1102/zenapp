export interface IScanLoginFormConfig {
    errorMessage?: string;
    hasError?: boolean;
    labelKey: string;
    name: string;
    otAutoId: string;
    placeholder?: string;
    placeholderKey?: string;
    type: string;
    valueKey: string;
}
