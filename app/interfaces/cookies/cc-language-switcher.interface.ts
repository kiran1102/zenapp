export interface ILanguageSwitcher {
    domainId: number;
    isEnabled: boolean;
    defaultLanguage: string;
    productionCDNUrl?: string;
    productionSingleLocationUrl?: string;
};
