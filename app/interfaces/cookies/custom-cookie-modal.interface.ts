export interface ICustomCookieModal {
    description: string;
    groupId?: number;
    host: string;
    isSession: boolean;
    isThirdParty: boolean;
    lifeSpan?: number;
    name: string;
    purpose: string;
    value: string;
};
