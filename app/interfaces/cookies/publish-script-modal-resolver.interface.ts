import {
    ICookieScriptVersionDiffResponse,
    IJqueryVersionDetail,
} from "interfaces/cookies/cookie-publish-script.interface";
import { IBannerLanguage } from "interfaces/cookies/cc-banner-languages.interface";

export interface IPublishScriptModalResolver {
    bannerLanguages: IBannerLanguage[];
    currentDomainId: number;
    jqueryVersions: IJqueryVersionDetail[];
    scriptData: ICookieScriptVersionDiffResponse;
    promiseToResolve: (response: boolean, jqueryVersionId: number, domainIds: number[]) => void;
}
