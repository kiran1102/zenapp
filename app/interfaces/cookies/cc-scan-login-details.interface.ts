export interface IScanLoginDetails {
    domainName: string;
    loginPageUrl: string;
    loginFormId: string;
    submitButtonId: string;
    usernameFieldId: string;
    usernameValue: string;
    passwordFieldId: string;
    passwordValue: string;
    otherLoginInfo: string;
    isEnabled: boolean;
    loginError?: string;
    cookiesJsonData: string;
}

export interface IAuthenticationCookiesList {
    cookiesName: string;
    cookiesValue: string;
}
