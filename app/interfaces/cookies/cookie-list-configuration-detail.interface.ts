export interface ICookieListConfigurationDetail {
    CategoriesText: string;
    CookiesText: string;
    IsLifespanEnabled: boolean;
    LifespanDurationText: string;
    LifespanText: string;
    LifespanTypeText: string;
    IsRestoreRequest?: boolean;
};
