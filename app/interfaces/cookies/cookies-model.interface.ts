export class CookiesModel {
    websiteListLoaded = false;
    domainList = [];
    domainSelectionList = [];
    reportPartsSelectionList = [];
    summaryReport = [];
    domainHistorySelectionList = [];
    selectedWebsite = "";
    websitesFilter = "";
    selectedPart = "";
    selectedRowKey = "";
    pagination = [];
}
