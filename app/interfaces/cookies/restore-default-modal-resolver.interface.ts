export interface IRestoreDefaultModalResolve {
    promiseToResolve: () => ng.IPromise<any>;
    modalTitle: string;
    confirmationText: string;
    warningText: string;
    cancelButtonText: string;
    submitButtonText: string;
}
