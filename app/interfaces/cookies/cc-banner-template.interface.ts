import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

export interface IBannerTemplate {
    createdBy?: string;
    createdDate?: Date;
    defaultLanguage?: ILanguage;
    languages?: ILanguage;
    lastModifiedBy?: string | null;
    lastModifiedDate?: Date | null;
    organization?: string;
    status?: IBannerTemplateStatus;
    templateId: number;
    templateName: string;
    tenantGuid?: string;
}

export interface IBannerTemplatePublish {
    templateId: number;
    domainNames: string[];
}

export interface IBannerLayoutProperties {
    imageSrc: string;
    label: string;
    position: string;
    selected: boolean;
    theme: string;
    bannerColor: string;
    textColor: string;
    top: string;
    bottom: string;
    left: string;
    right: string;
    width: string;
    height: string;
    transform: string;
    transformOrigin: string;
}

export interface  IBannerModel {
    domainId: number;
    noticeText: string;
    showAddPolicyLink: boolean;
    cookiePolicyLinkText: string;
    cookiePolicyUrl: string;
    showCookieSettings: boolean;
    cookieSettings: string;
    showAcceptCookies: boolean;
    acceptCookies: string;
    bannerStyle: IBannerModelBannerStyle;
    textStyle: IBannerModelTextStyle;
    buttonStyle: IBannerModelButtonStyle;
    closeAcceptsAllCookies: boolean;
    onClickAcceptAllCookies: boolean;
    scrollAcceptsCookies: boolean;
    scrollAcceptsAllCookies: boolean;
    showOnlyInEU: boolean;
    hideForAll: boolean;
    skinName: string;
    isPublished: boolean;
    containsUnpublishedChanges: boolean;
    forceConsent: boolean;
    bannerPushesDown: boolean;
    bannerTitle: string;
    showBannerCloseButton?: boolean;
    footerDescriptionText?: string;
    bannerCloseButtonText: string;
    externalCssBaseUrl: string;
}

export interface  IBannerModelBannerStyle {
    backgroundColor: string;
    transform: string;
    transformOrigin: string;
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
    width?: string;
    height?: string;
}

export interface  IBannerModelTextStyle {
    color: string;
}

export interface  IBannerModelButtonStyle {
    backgroundColor: string;
    border: string;
    color?: string;
}
export interface IBannerTemplateCreate {
    templateName: string;
    orgGroupId: string;
    defaultLanguage: number;
}
export interface IBannerTemplateStatus {
    id: number;
    description: string;
    name: string;
    new: boolean;
}

export interface IBannerTemplateColumns {
    columnHeader: string;
    columnTitleKey: string;
    cellValueKey: string | null;
    sortable?: boolean | false;
    id?: string;
}

export interface ITemplateModalResolver {
    modalTitle: string;
    createTemplateButton: string;
    cancelButtonText: string;
    templateNameLabel: string;
    organizationNameLabel: string;
    defaultLanguageLabel: string;
    bannerTemplate: IBannerTemplate | null;
}

export interface IAssignTemplateModalResolver {
    domainId: number;
    callback?: () => void;
}

export interface ITemplateAddLanguageModalResolver {
    templateId: number;
    templateJsonData: ITemplateJson;
    callback?: (language: ILanguage) => void;
}
