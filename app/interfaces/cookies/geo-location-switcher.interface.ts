export interface IGeoLocationSwitcherRequest {
    isEnabled: boolean;
    euLanguage: string;
    nonEuLanguage: string;
};

export interface IGeoLocationSwitcher extends IGeoLocationSwitcherRequest {
    productionCDNUrl: string;
    productionSingleLocationUrl: string;
};
