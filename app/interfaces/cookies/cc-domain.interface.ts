export interface IDomain {
    auditDomain: string;
    domainId: number;
    auditedPages: number;
    cookiesFound: number;
    hasPrivacyPolicy: boolean;
    hasCookiePolicy: boolean;
    hasCookieNotice: boolean;
    hasLoginForm: boolean;
    status: string;
    pageLimit: number;
    lastCookiesFound: number;
    isNew: boolean;
    scanError: string | null;
    isCookieDetectVersion: boolean;
    cookiePolicyUrl: string | null;
    completedTime: Date;
    organizationUUID: string;
    lastSuccessfulScanDate: Date | null;
    lastSuccessfulScanRowKey: string | null;
    scheduledDateOfNextScan: Date | null;
    includedQueryParams: string;
    secondaryUris: string;
    cookiesChangedBy: number;
    lastScanDate: Date | null;
    domainReadyForBanner: boolean;
    hasScriptArchive: boolean;
    hasBanner: boolean;
    behindLoginCookiesFound: number | null;
    behindLoginPagesScanned: number | null;
    scanLoginInfoStatus: string | null;
    id?: string;
    templateId?: number;
    templateName?: string;
    isThirdPartyBannerEnabled?: boolean;
    isBannerWarningClicked?: boolean;
    isBannerWarningClosed?: boolean;
    showThirdPartyBannerWarning?: boolean;
}

export interface IDomainColumns {
  columnHeader: string;
  columnTitleKey: string;
  cellValueKey: string | null;
  sortable?: boolean | false;
}

export interface IDomainSort {
    sortKey: string;
    sortOrder: "desc" | "asc";
    page: number;
}
