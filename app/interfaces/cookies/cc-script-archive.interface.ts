export interface IScriptArchiveDetail {
    archiveScriptId: number;
    createDate: Date;
    createDateString: string;
    scriptURL: string;
    creator: string;
    creatorFullName: string;
    domainCctId: string;
    domainJson: any;
    requireReConsent: boolean;
    culture?: string;
    consentModel?: string;
    restoreInProgress?: boolean;
}
