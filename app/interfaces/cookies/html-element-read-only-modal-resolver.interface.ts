export interface IHtmlElementReadOnlyModalResolver {
    modalTitle: string;
    closeButtonText: string;
}
