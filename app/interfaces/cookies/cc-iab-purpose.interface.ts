export interface IPurpose {
    purposeId: number;
    purposeName?: string;
    description?: string;
}

export type IPurposes = IPurpose[];

export interface IVendor {
    vendorId: number;
    vendorName: string;
    policyURL: string;
    isSelected?: boolean;
}

export type IVendors = IVendor[];

export interface IVersion {
    ListId: number;
    ListName: string;
    OrgName: string;
}
export interface IAddVendorRequest {
    Description: string;
    ListName: string;
    OrgId: string;
    OrgName: string;
}
export interface IAddVendorRequestHeader {
    TenantId: string;
}

interface Ifeature {
    id: number;
    name?: string;
    description?: string;
}

type Ifeatures = Ifeature[];

export interface IVersionData {
    content: IVersion[];
    totalPages: number;
    totalElements: number;
    last: boolean;
    size: number;
    number: number;
    sort: null;
    numberOfElements: number;
    first: boolean;
}
export interface IVendorTableRow {
    id: string;
    name: string;
    purposes: IPurposes;
    features: Ifeatures;
    policyURL: string;
}

export interface IfilterMeta {
    name: string;
    valueOptions: IfilterOptions[];
}

export interface IfilterOptions {
    value: string;
    id: number;
}

export interface IcustomFilter {
    filterType: string;
    selectionIds: number[];
}
export interface IVendorColumns {
    columnHeader: string;
    columnTitleKey: string;
    dataValueKey: string | null;
    clickable?: boolean;
    sortable?: boolean | false;
    selectable?: boolean | false;
}
export interface IselectedFiltersList {
    [key: string]: IfilterOptions[];
}

interface IvendorRecords {
    content: IvendorRecord[];
    first?: boolean;
    last?: boolean;
    numberOfElements?: number;
    size?: number;
    sort?: string;
    totalElements?: number;
    totalPages?: number;
    number?: number;
}

export interface IvendorRecord {
    id: number;
    name: string;
    policyUrl: string;
    features: Ifeature[];
    purposes: IPurpose[];
    active: boolean;
    firstPurposeName?: string;
    featureNames?: string;
    selected?: boolean;
    whiteListed: boolean;
}
export interface IvendorTableData {
    rows: IvendorRecord[];
    name?: string;
    totalElements?: number;
    size?: number;
    number?: number;
}
export interface IVendorsResponse {
    activeVendors?: IvendorRecords;
    inactiveVendors?: IvendorRecords;
    listId: number;
    listName: string;
}

export interface IFilterParams {
    page: number;
    filterMetaData: IselectedFiltersList[];
    filterData: IcustomFilter[];
    searchText: string;
}

export interface IVendorMobile {
    ListDesc: string;
    ListId: number;
    ListName: string;
    OrgName: string;
}
