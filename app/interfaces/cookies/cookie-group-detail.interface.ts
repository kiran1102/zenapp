import { IPurpose } from "interfaces/cookies/cc-iab-purpose.interface";
import {
    ICookies,
    IHosts,
} from "interfaces/cookies/cc-cookie-category.interface";

export interface ICookieGroupDetail {
    canDelete: boolean;
    cookies: ICookies[];
    customGroupId: string;
    description: string;
    groupId: number;
    hosts: IHosts[];
    isDntEnabled?: boolean;
    isExpanded?: boolean;
    isParentGroup?: boolean;
    name: string;
    orderPreview: number;
    purposeGroupId: number;
    purposes: IPurpose[];
    show?: boolean;
    showIabSettings?: boolean;
    subGroups: ICookieSubGroup[];
    vendors: any;
}

export interface ICookieSubGroup {
    customGroupId: string;
    description: string;
    id: number;
    isDntEnabled?: boolean;
    name: string;
    numberOfCookies: number;
    orderPreview: number;
    parentId: number;
}

export interface ICookieGroupRequest {
    groupName: string;
    groupDescription: string;
    isDntEnabled: boolean;
    customGroupId: string;
}

export interface ICookieCategoryOptions {
    id: number;
    name: string;
}

export interface ICookieGroupMetaDetail {
    name: string;
    customGroupId: string;
    description: string;
    isDntEnabled: boolean;
}
