import { IPrefCenterData } from "interfaces/cookies/cc-pref-center.interface";
import {
    IBannerModel,
    IBannerTemplate,
} from "interfaces/cookies/cc-banner-template.interface";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";

export interface ITemplateJson {
    bannerModel: IBannerModel;
    prefCenterData: IPrefCenterData;
    defaultLanguage?: ILanguage;
};

export interface ITemplateBannerPrefCenter {
    cookieBanner?: IBannerModel;
    allTabs?: IPrefCenterData;
    bannerModel?: IBannerModel;
    prefCenterData?: IPrefCenterData;
    templateJson?: string;
    defaultLanguage?: ILanguage;
    customSkin?: ITemplateSkinDetail;
    skinDirectoryName?: string;
};

export interface ITemplateSkinDetail {
    bannerBackgroundColor: string;
    bannerTextColor: string;
    customCss: string;
    id: string;
    logoFileName: string;
    menuColor: string;
    menuHighlightColor: string;
    new: boolean;
    primaryColor: string;
    secondaryColor: string;
};

export interface ICookieTemplateDetail extends IBannerTemplate {
    templateJson: string;
};

export interface ICookieTemplate extends IBannerTemplate, ITemplateJson {
};
