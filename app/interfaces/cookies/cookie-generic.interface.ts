export interface ICookieNameId {
    id: number;
    name: string;
}
