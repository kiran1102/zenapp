import { IStringMap } from "interfaces/generic.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

export interface ICertificateFileUpload {
    FileName: string;
    Extension: string;
    Data: string;
    Password: string;
}

export interface IAddSamlSettings {
    AttributeMappings?: ISamlAttributeMapping[];
    Hosts?: ISamlHost[];
    Id: string;
    IdentityProviderName: string;
    IdentityProviderSignOnUrl: string;
    IdentityProviderSignOutUrl: string;
    IdpCertificate?: string | ISamlIdpCertificate;
    IsEnabled: boolean;
    RequestBindingTypeId: number | string;
    ResponseBindingTypeId: number | string;
    SericeProviderName?: string;
    SpCertificate?: string;
    Provider?: number;
    SpAssertionUrl?: string;
    IdpCertificateId?: string;
    SpCertificateId?: string;
    WantRequestsSign?: boolean;
    IsLegacy?: boolean;
}

export interface ISamlIdpCertificate {
    Id?: string;
    IsRevoked?: boolean;
    IssuerName?: string;
    SerialNumber?: string;
    SubjectName?: string;
    Thumbprint?: string;
}

export interface ISamlHost {
    Id?: string;
    Description: string;
    Name: string;
}

export interface ISamlResponse {
    Content: IAddSamlSettings;
}

export interface ISamlSettingsFormFields {
  dataType: string;
  inputType: string;
  fieldKey: string;
  fieldNameKey: string;
  validationKey?: string;
  required?: boolean;
  placeholderKey?: string;
  tooltip?: string;
}

export interface ISamlAttributeMapping {
    Id: string;
    DefaultValue?: string;
    Description: string;
    Lookup?: string;
    MapKey: string;
    Name: string;
    SamlAttributeTypeId: number;
}

export interface ISamlOrgGroupMapping {
    Guid?: string;
    SamlAttributeType: number;
    Key: string;
    Value: string;
}

export interface ISamlOrgMappingResponse extends IPageable {
    content: ISamlOrgGroupMapping[];
}

export interface ISamlOrgGroupMappingModalResolve {
    inputName: string;
    showOrgDropdown: boolean;
    title?: string;
    isEditOrgMappingAttr: boolean;
    addNewAttribute?: boolean;
    orgName?: string;
    samlConfigValues?: IAddSamlSettings;
    samlOrgMapping?: ISamlOrgGroupMapping[];
    row?: ISamlOrgGroupMapping;
    index?: number;
    pageNumber?: number;
    sort?: string;
    search?: string;
}

export interface ISamlOrgGroupMappingTable {
    page: number;
    search?: number;
    size: number;
    sort: string;
    attributeValue?: string;
}

export interface ISamlOrgMappingTableSort {
    sortKey: string;
    sortOrder: string;
}

export interface ISamlCertificateResponse {
    result: boolean;
    data: ISamlIdpCertificate;
}
