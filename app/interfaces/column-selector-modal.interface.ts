import { IListItem, IListState } from "@onetrust/vitreus";
export interface IColumnSelectorModalResolve {
    promiseToResolve?: (columnSelectionMap: IListState) => Promise<boolean>;
    cancelButtonText?: string;
    saveButtonText?: string;
    modalTitle: string;
    leftList?: IListItem[];
    rightList?: IListItem[];
    leftHeaderLabel?: string;
    rightHeaderLabel?: string;
    labelKey?: string;
}
