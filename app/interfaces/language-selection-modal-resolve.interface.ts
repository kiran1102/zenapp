import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

export interface ILanguageSelectionModalResolve {
    allLanguages: IGetAllLanguageResponse[];
    defaultLanguage: string;
    selectedLanguages: string[];
    bodyTextKey?: string;
    showBodyText?: boolean;
}
