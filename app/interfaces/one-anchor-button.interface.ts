export interface IAnchorButton {
    anchorLink?: string;
    anchorRoute?: string;
    buttonClass?: string;
    icon?: string;
    iconClass?: string;
    identifier: string;
    newTab?: boolean;
    rightSideIcon?: string;
    rightSideIconClass?: string;
    isRound?: boolean;
    isFloating?: boolean;
    text?: string;
    textKey?: string;
    type?: string;
    wrapText?: boolean;
}
