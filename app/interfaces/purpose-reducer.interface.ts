// Redux
import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

// Models
import { PurposeDetails } from "crmodel/purpose-details";
import { EntityItem } from "crmodel/entity-item";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

export interface IPurposeState {
    isLoading: boolean;
    isModified: boolean;
    isSaving: boolean;
    isLoadingLanguages: boolean;
    isValid: boolean;
    language?: IGetAllLanguageResponse;
    allLanguages?: IGetAllLanguageResponse[];
    purpose?: PurposeDetails;
    purposeDraft?: PurposeDetails;
    selectedPurposes?: EntityItem[];
}

export type IPurposeSelector = OutputSelector<IStoreState, PurposeDetails, (purposeState: IPurposeState) => PurposeDetails>;
export type IPurposeStatusSelector = OutputSelector<IStoreState, boolean, (purposeState: IPurposeState) => boolean>;
