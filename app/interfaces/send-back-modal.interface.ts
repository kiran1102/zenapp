import { IProject, IProjectLabels } from "interfaces/project.interface";

export interface ISendBackModalData {
    Project: IProject;
    Labels: IProjectLabels;
    TemplateType: number;
}
