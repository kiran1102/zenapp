export interface IProjectSettings {
    DefaultReminderDays?: number;
    DefaultReviewer?: boolean;
    EnableRiskAnalysis: boolean;
    EnableThresholdTemplates: boolean;
    FromCache?: boolean;
    HasSeeded?: boolean;
    IsVersioningEnabled: boolean;
    ProjectLevelApproval: boolean;
    ProjectTaggingEnabled?: boolean;
    QuestionTaggingEnabled?: boolean;
    ReadinessSamplesHidden?: boolean;
    SectionAssignment: boolean;
    SelfServiceEnabled?: boolean;
    TaggingEnabled: boolean;
    Reminder?: IProjectSettingsReminder;
    MinDate?: Date;
}

export interface IProjectSettingsReminder {
    Show: boolean;
    Value: number;
}

export interface ITagSettings {
    ProjectTaggingEnabled: boolean;
    QuestionTaggingEnabled: boolean;
    FromCache: boolean;
}

export interface ISettingsUpdateBrandingContract {
    LogoData: string;
    LogoFileName: string;
    IconData: string;
    IconFileName: string;
    PrimaryColor: string;
    SecondaryColor: string;
    TertiaryColor: string;
    Title: string;
}

export interface ISettingsUpdateTagContract {
    QuestionTaggingEnabled: boolean;
    ProjectTaggingEnabled: boolean;
}

export interface ISamlSettings {
    AttributeMappings: string | null;
    Hosts: any[]; // TODO: IHosts
    Id: string;
    IdentityProviderName: string;
    IdentityProviderSignOnUrl: string;
    IdentityProviderSignOutUrl: string;
    IdpCertificate: string;
    IdpCertificateId: string;
    IsEnabled: boolean;
    RequestBindingTypeId: string;
    ResponseBindingTypeId: string;
    SericeProviderName: string;
    SpAssertionUrl: string;
    SpCertificate: string;
    SpCertificateId: string;
    IsLegacy?: boolean;
}

export interface ISamlSettingsResponse {
    Content: ISamlSettings;
    Error?: string;
    StatusCode: number;
}

export interface IRiskSettings {
    Enabled: boolean;
    GraphSettings: { Axes: [{ Label: string, Points: Array<{ Label: string, Value: number}> }, { Label: string, Points: Array<{ Label: string, Value: number}> }] };
    RiskMap: Array<{ RiskLevel: number, Axis1Value: number, Axis2Value: number }>;
    FromCache?: boolean;
}

export interface IHelpSettings {
    Enabled: boolean;
    ContactId: string;
    DefaultComment: string;
}
