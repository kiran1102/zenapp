export interface IRiskSetting {
    enableRiskHeatMap: boolean;
}

export interface IRiskHeatmap {
    impactLevel: string;
    impactLevelId: number;
    levelId: number;
    probabilityLevel: string;
    probabilityLevelId: number;
    riskScore: number;
}

export interface IRiskLevel {
    id: number;
    displayName: string;
    name: string;
    score: number;
    maxScore: number;
    minScore: number;
    riskFlagBgClass?: string;
}
