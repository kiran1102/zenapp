export interface IBannerAlertData {
    alertScheduleId: string;
    alertType: string;
    alertTypeId: number;
    bannerMessage?: IBannerMessageData;
    displayAttributes?: IDisplayAttributeData;
    maintenanceScheduleAttributes?: IInAppMaintenanceDataDates;
}

export interface IInAppMaintenanceDataDates {
    MaintenanceEndTime: Date;
    MaintenanceStartTime: Date;
}

export interface IBannerMessageData {
    id: string;
    languageCode: string;
    message: string;
}

export interface IDisplayAttributeData {
    attributes?: IBannerMessageOptionData;
    alertTypeId: number;
}

export interface IBannerMessageOptionData {
    IconName: string;
}
