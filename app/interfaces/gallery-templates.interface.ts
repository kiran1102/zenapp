// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";
import {
    ICardTitle,
    ICardHeader,
    ICardImage,
 } from "@onetrust/vitreus";
import { ICustomTemplateDetails } from "modules/template/interfaces/custom-templates.interface";

export interface IGalleryTemplate {
    description: string;
    id: string;
    icon: string;
    name: string;
    status: string;
}

export interface IGalleryTemplateResponse {
    description: string;
    friendlyName: string;
    icon: string;
    id: string;
    name: string;
    sections: IGalleryTemplateSection[];
    status: string;
    templateType: string;
    templateVersion: number;
    welcomeText: string;
}

export interface IGalleryTemplateSection {
    attributes?: string;
    description: string;
    id: string;
    name: string;
    questions: IGalleryTemplateQuestion[];
    sequence: number;
}

export interface IGalleryTemplateQuestion {
    attributes?: string | null;
    attributeJson?: IQuestionAttributes;
    content: string;
    description?: string;
    friendlyName: string;
    id: string;
    options?: IQuestionOptionDetails[] | null;
    questionType: string;
    required: boolean;
    sequence: number;
    isPreviewShown?: boolean;
}

export interface IQuestionOptionDetails {
    attributes?: string | null;
    attributeJson?: IQuestionOptionAttributes;
    id: string;
    option: string;
    sequence: number;
}

export interface IQuestionAttributes {
    allowJustification?: boolean;
    allowNotApplicable?: boolean;
    allowNotSure?: boolean;
    allowOther?: boolean;
    displayAnswerType?: QuestionAnswerTypes;
    isMultiSelect?: boolean;
    optionHintEnabled?: boolean;
    required?: boolean;
    requireJustification?: boolean;
}

export interface IQuestionOptionAttributes {
    hint?: string;
}

export interface IGalleryTemplateCardAttributes {
    title?: ICardTitle;
    image?: ICardImage;
    header?: ICardHeader;
    content?: string;
    button?: ICardHeader;
    locked?: boolean;
    id?: string;
    status?: string;
    name?: string;
    draft?: ICustomTemplateDetails;
    published?: ICustomTemplateDetails;
    publishButtonText?: string;
    version?: number;
    contextMenuOpen?: boolean;
}
