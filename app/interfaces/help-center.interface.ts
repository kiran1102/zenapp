import { IStringMap } from "./generic.interface";

export interface IHelpCenterWindow extends Window {
    zE: (cb: (zE: IZendeskWidget) => void) => void;
    zEACLoaded: boolean;
    zESettings: IZendeskSettingsWidget;
    zEmbed: () => void;
}

export interface IZendeskWidget {
    activate: (props: IZendeskWidgetActivate) => void;
    hide: () => void;
    identify: (identity: IZendeskWidgetIdentity) => void;
    logout: () => void;
    setHelpCenterSuggestions: (search: IZendeskWidgetSearch) => void;
    setLocale: (languageCode: string) => void;
    show: () => void;
}

export interface IZendeskWidgetActivate {
    hideOnClose: boolean;
}

export interface IZendeskWidgetIdentity {
    name: string;
    email: string;
    organization: string;
}

export interface IZendeskWidgetSearch {
    search?: string;
}

export interface IZendeskSettingsWidget {
    authenticate?: IZendeskSettingsAuthenticate;
    webWidget?: IZendeskSettingsWebWidget;
}

export interface IZendeskSettingsAuthenticate {
    jwt: string;
}

export interface IZendeskSettingsWebWidget {
    authenticate?: IZendeskSettingsAuthenticate;
    chat?: IZendeskSuppresable;
    color?: IZendeskSettingsWebWidgetColor;
    contactForm?: IZendeskSettingsWebWidgetContactForm;
    contactOptions?: IZendeskSettingsWebWidgetContactOptions;
    helpCenter?: IZendeskSettingsWebWidgetHelpCenter;
    launcher?: IZendeskWebWidgetLauncher;
    offset?: IZendeskSettingsWebWidgetOffset;
    position?: IZendeskSettingsWebWidgetPosition;
    talk?: IZendeskWebWidgetTalk;
    zIndex?: number;
}

export interface IZendeskSettingsWebWidgetColor {
    articleLinks?: string;
    button?: string;
    header?: string;
    launcher?: string;
    resultLists?: string;
    theme?: string;
}

export interface IZendeskSettingsWebWidgetContactForm extends IZendeskSuppresable {
    attachments?: boolean;
    fields?: IZendeskSettingsField;
    selectTicketForm?: LocalizationValueMap;
    subject?: string;
    tags?: string[];
    ticketForms?: IZendeskSettingsTicket[];
    title?: LocalizationValueMap;
}

export interface IZendeskSettingsField {
    id?: string | number;
    prefill?: LocalizationValueMap;
}

export interface IZendeskSettingsTicket {
    id?: string | number;
    fields?: IZendeskSettingsField;
}

export interface IZendeskSettingsWebWidgetContactOptions {
    chatLabelOffline?: LocalizationValueMap;
    chatLabelOnline?: LocalizationValueMap;
    contactButton?: LocalizationValueMap;
    contactFormLabel?: LocalizationValueMap;
    enabled?: boolean;
}

export interface IZendeskSettingsOffset {
    horizontal?: string;
    vertical?: string;
}

export interface IZendeskSettingsWebWidgetOffset extends IZendeskSettingsOffset {
    mobile?: IZendeskSettingsOffset;
}

export interface IZendeskSettingsWebWidgetPosition {
    horizontal?: "left" | "right";
    vertical?: "top" | "bottom";
}

export interface IZendeskSuppresable {
    suppress?: boolean;
}

export interface IZendeskSettingsWebWidgetHelpCenter extends IZendeskSuppresable {
    chatButton?: LocalizationValueMap;
    filter?: IZendeskSettingsWebWidgetHelpCenterFilter;
    messageButton?: LocalizationValueMap;
    originalArticleButton?: boolean;
    searchPlaceholder?: LocalizationValueMap;
    title?: LocalizationValueMap;
}

export interface IZendeskSettingsWebWidgetHelpCenterFilter {
    category?: string;
    label_names?: string;
    section?: string;
}

export interface IZendeskWebWidgetLauncher extends IZendeskSuppresable {
    chatLabel?: LocalizationValueMap;
    label?: LocalizationValueMap;
}

export interface IZendeskWebWidgetTalk extends IZendeskSuppresable {
    nickname?: string;
}

export type LocalizationValueMap = IStringMap<string>;
