export interface IConsentSettings {
    magicLinkEnabled: boolean;
    magicLinkTimeSpan: number;
    magicLinkKillSwitchEnabled?: boolean;
    noConsentTransactions?: boolean;
    consentAcknowledgementEmail?: boolean;
}
