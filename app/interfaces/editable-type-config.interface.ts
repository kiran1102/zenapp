export interface IEditableTypeConfig {
    clearOnSelect?: boolean;
    isButton?: boolean;
    isEditable: boolean;
    inputType: number;
}
