import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IPageableOf } from "interfaces/pagination.interface";

export interface IAttachmentState {
    isFetchingAttachments: boolean;
    isSavingAttachment: boolean;
    isEditingAttachment: boolean;
    isDeletingAttachment: boolean;
    isDownloadingAttachment: boolean;
    attachmentListData: IPageableOf<IAttachmentFileResponse>;
    currentRefId: string;
}

export type IAttachmentStateSelector = OutputSelector<IStoreState, boolean, (attachmentState: IAttachmentState) => boolean>;
export type IAttachmentListSelector = OutputSelector<IStoreState, IPageableOf<IAttachmentFileResponse>, (attachmentState: IAttachmentState) => IPageableOf<IAttachmentFileResponse>>;
export type IFetchingAttachmentListSelector = OutputSelector<IStoreState, boolean, (attachmentState: IAttachmentState) => boolean>;
export type IAttachmentRefIdSelector = OutputSelector<IStoreState, string, (attachmentState: IAttachmentState) => string>;
