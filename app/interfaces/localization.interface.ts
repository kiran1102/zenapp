import { IProtocolResponse } from "interfaces/protocol.interface";
import { ITableColumnConfig } from "interfaces/table-config.interface";
import { IBasicTable, IBasicTableCell } from "interfaces/tables.interface";
import { IPageable } from "interfaces/pagination.interface";
import { IKeyValue, IStringMap } from "interfaces/generic.interface";

export interface ILanguage {
    Id: number;
    Name: string;
    Code: string;
    Translated?: boolean;
    Activated?: boolean;
    sortOrder?: number;
}

export interface ITranslationsTable extends IBasicTable {
    rows: ITranslationCell[];
    columns: ITableColumnConfig[];
    search?: string;
}

export interface ITranslationsPage extends IPageable {
    content: ITranslation[];
}

export interface ITranslation extends IKeyValue<string> {
    defaultValue?: string;
    isCustomTerm?: boolean;
    language?: string;
    languageCode: string;
    customizedValue: boolean;
}

export interface ITranslationCell extends ITranslation {
    cells: IBasicTableCell[];
}

export interface ILocalizationParams {
    language?: string;
    page?: number;
    searchKey?: string;
    size?: number;
    tenantId?: string;
    module?: string;
}

export interface ILocalizationEditModalData {
    termKey: string;
    termIndex: number;
    lastTermIndex: number;
    searchKey?: string;
    tabId: string;
}

export interface ITenantTranslations {
    FallbackToNotFound: boolean;
    LanguageCode: string;
    Translations: IStringMap<string>;
}
