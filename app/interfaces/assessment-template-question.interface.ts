// Interface
import { ILabelValue } from "interfaces/generic.interface";
import { IQuestionModelOption } from "interfaces/assessment/question-model.interface";

// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";

export interface IAssessmentTemplateQuestion {
    attributeJson?: IQuestionAttributes;
    attributes?: string | null;
    content: string;
    friendlyName: string;
    destinationInventoryQuestion: boolean;
    sourceInventoryQuestion: boolean;
    id: string;
    linkAssessmentToInventory: boolean;
    options?: IQuestionOptionDetails[];
    parentQuestionId?: string;
    prePopulateResponse?: boolean;
    questionSequence?: number;
    questionType: string;
    required: boolean;
    sectionSequence?: number;
    sequence?: number;
    valid?: boolean;
}

export interface IAssessmentTemplateAttribute {
    attributeType: string;
    description: string;
    descriptionKey: string;
    fieldName: string;
    id: string;
    name: string;
    nameKey: string;
    required: boolean;
    responseType: string;
}

export interface ICreateQuestionParams {
    nextQuestionId: string | null;
    question: IQuestionDetails;
}

export interface IQuestionOptionDetails {
    Hint?: string;
    attributeJson?: IQuestionOptionAttributes;
    attributes?: string | null;
    focusEnabled?: boolean;
    id?: string;
    isSelected?: boolean;
    option: string;
    optionType: string;
    sequence?: number;
    tooltipHintEnabled?: boolean;
    orgGroupId?: string;
}

export interface IQuestionOptionAttributes {
    hint?: string;
}

export interface IQuestionDetails {
    attributeJson?: IQuestionAttributes;
    attributes?: string | null;
    content?: string;
    deletedNavigationIds?: string[];
    description?: string;
    friendlyName?: string;
    destinationInventoryQuestion: boolean;
    sourceInventoryQuestion: boolean;
    hint?: string;
    id?: string;
    prePopulateResponse?: boolean;
    linkAssessmentToInventory?: boolean;
    options?: IQuestionOptionDetails[] | null;
    parentQuestionId?: string;
    questionType: string;
    required?: boolean;
    response?: string;
    responseIds?: string[];
    sequence?: number;
    valid?: boolean;
}

export interface IQuestionAttributes {
    allowJustification?: boolean;
    allowNotApplicable?: boolean;
    allowNotSure?: boolean;
    allowOther?: boolean;
    attributeId?: string;
    attributeType?: string;
    displayAnswerType?: QuestionAnswerTypes;
    fieldName?: string;
    inventoryType?: string;
    isMultiSelect?: boolean;
    isQuestionHintEnabled?: boolean;
    link?: string;
    name?: string;
    nameKey?: string;
    optionHintEnabled?: boolean;
    required?: boolean;
    requireJustification?: boolean;
    responseType?: string;
    text?: boolean;
}

export interface IQuestionBuilderModalData {
    nextQuestionId?: string;
    question?: Partial<IQuestionDetails>;
    sectionId: string;
    templateId: string;
    modalTitle?: string;
    submitButtonText?: string;
    cancelButtonText?: string;
}

export interface IQuestionAnswer {
    answer: string;
    answerId?: string;
    answerType: string;
}

export interface ITemplateQuestionReorderRequest {
    nextQuestionId: string;
    nextSectionId: string;
    questionId: string;
    sectionId: string;
}

export interface IBuilderQuestionDragItem {
    index: number;
    questionId: string;
    sectionId: string;
}

export interface IInventoryQuestionOption {
    id: string;
    content: string;
    sectionSequence: number;
    questionSequence: number;
}

export interface IAdditionalAttribute extends ILabelValue<string> {
    tooltip: string;
}

export interface IMultipleSelectPayload {
    previousValue: IQuestionOptionDetails[];
    currentValue: IQuestionOptionDetails[];
    change: IQuestionOptionDetails;
}

export interface ISingleSelectPayload {
    currentValue: IQuestionOptionDetails;
    change: IQuestionOptionDetails;
}

export interface IMultichoiceDropdownSelection {
    previousValue: IQuestionModelOption[];
    currentValue: IQuestionModelOption[];
    change: IQuestionModelOption;
}
