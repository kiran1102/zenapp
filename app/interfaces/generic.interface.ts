export interface IStringMap<T> {
    [index: string]: T;
}

export interface INumberMap<T> {
    [index: number]: T;
}

export interface IKeyValue<T> {
    key: string;
    value: T;
}

export interface ILabelValue<T> {
    label: string;
    value: T;
}

export interface IValueId<T> {
    id: string;
    value: T;
}

export interface INameId<T> {
    id: string;
    name: T;
}

export interface ICommentNameId<T> extends INameId<T> {
    comment?: string;
}

export interface ILabelId {
    id: string;
    label: string;
}

export interface ISelection<T> {
    previousValue: T;
    currentValue: T;
    change: T;
}
