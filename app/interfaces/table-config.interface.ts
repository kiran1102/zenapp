import { IStringMap } from "interfaces/generic.interface";
import { TableColumnTypes } from "enums/data-table.enum";

// TODO: Consolidate all table configs to ITableConfig
export interface ITableConfig {
    component?: string;
    sortable?: boolean;
    sortKey?: string | number;
}

export interface ITableColumnConfig extends ITableConfig {
    columnKey?: string;
    cellValueKey?: string;
    name: string;
    style?: IStringMap<string | number>;
    type?: TableColumnTypes;
    canFilter?: boolean;
    filterKey?: string;
    isChildObject?: boolean;
    childObjectRef?: string;
}

export interface IIGenericTableConfig extends ITableConfig {
    headerText: string;
    id?: string;
    modelPropKey?: string;
    multiLine?: boolean;
    style?: IStringMap<string | number>;
}
