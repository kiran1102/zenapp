import { IListItem, IListState } from "@onetrust/vitreus";

export declare class OtDuelingPicklistComponent {
    optionsLeft: IListItem[];
    optionsRight: IListItem[];
    labelLeft: string;
    labelRight: string;
    labelKey: string;
    getListState(): IListState;
    moveRight(): void;
    moveLeft(): void;
    moveTop(): void;
    moveUp(): void;
    moveDown(): void;
    moveBottom(): void;
    private findSelected(listId);
}
