import { IProtocolResponse } from "./protocol.interface";
export interface IConfirmationModalResolve {
    promiseToResolve?: () => ng.IPromise<any>;
    callback?: (response?: IProtocolResponse<any>) => void;
    successCallback?: () => void;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText?: string;
    submitButtonText?: string;
    iconClass?: string;
    iconTemplate?: string;
    hideCancelButton?: boolean;
    hideSubmitButton?: boolean;
    confirmationRowKey?: string;
}
