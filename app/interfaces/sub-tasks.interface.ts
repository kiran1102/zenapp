import { IPageableMeta } from "interfaces/pagination.interface";
import { SubTaskCommentType } from "dsarModule/enums/sub-task.enum";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { Subject } from "rxjs";

export interface ISubTaskRowTable extends IPageableMeta {
    content: ISubTask[];
}
export interface ISubTask {
    userId: string;
    taskName: string;
    comment?: string;
    type?: number;
    typeLabel?: string;
    deadline?: number | string;
    reminder?: number | string;
    isRequired?: boolean;
    isGroup?: boolean;
    isManualRun?: boolean;
    id?: string;
    requestQueueId?: string;
    status?: number;
    sequence?: null | string;
    comments?: any;
    statusLabel?: string;
    statusColor?: string;
    assignee?: string;
    deadlineDate?: string;
    reminderDate?: string;
    workflowDetailId?: string;
    workflowStatus?: number;
    workflowStage?: string;
    systemName?: string;
    actionName?: string;
    integrationSystemId?: string;
    integrationWorkflowId?: string;
    taskDescription?: string;
    dropDownMenu?: IDropdownOption[];
}

export interface IDSARSaveSubTaskModalResolve {
    isEditModal: boolean;
    callback?: any;
    row?: ISubTask;
    requestId?: string;
    workflowId?: string;
    workflowSubtaskModal?: boolean;
    type?: number;
}
export interface ISubTaskPaginationParams {
    page: number;
    size?: number;
    sort?: string | string[];
}
export interface IDSARRespondSubTaskModalResolve {
    callback?: (res: IProtocolResponse<ISubTask>) => void;
    row: ISubTask | ISubtaskResponseModalData;
    requestId?: string;
    disableFields?: boolean;
}

export interface ISubtaskResponseModalData {
    id: string;
    taskName: string;
    type?: number;
    taskDescription: string;
    requestId: string;
    status: number;
    assignee: string;
    comments: string[];
    deadline: string;
    integrationSystemId?: string;
    integrationWorkflowId?: string;
}

export interface IStatusLabel {
    label: string;
    id: number;
    color: string;
}

export interface ITypeLabel {
    label: string;
    id: number;
}

export interface ISubTaskAttachment {
    fileName: string;
    fileId: string;
    isInternal: boolean;
}

export interface ISubTaskComment {
    comment: string;
    files?: ISubTaskAttachment[];
    id?: string;
    isInternal?: boolean;
    taskId?: string;
    type?: SubTaskCommentType;
    isDataDiscovery?: boolean;
}
export interface ISubTaskData {
    comment: string;
    markAsComplete: boolean;
    files: ISubTaskAttachment[];
    isInternal: boolean;
}
export interface IFileInput {
    Data: string;
    FileName: string;
    Extension: string;
    Blob?: Blob;
}

export interface ISubTaskModalData {
    requestId?: string;
    workflowId?: string;
    title?: string;
    workflowSubtaskModal?: boolean;
    isEditModal?: boolean;
    type?: number;
    row?: {
        id: string;
        assignee?: string;
        taskName: string;
        isRequired: boolean;
        userId: string;
        deadline: number | Date;
        reminder: number | Date;
        comment: string;
        taskDescription: string;
        integrationSystemId?: string;
        integrationWorkflowId?: string;
        type?: number;
        isManualRun?: boolean;
        isGroup?: boolean;
    };
    updateSubtask$?: Subject<ISubTask | null>;
    viewMode?: boolean;
}
