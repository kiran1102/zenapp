// Enums
import { QuestionAnswerTypes } from "enums/question-types.enum";

export interface IQuestionTypeAttributesList {
    allowJustification?: boolean;
    allowNotApplicable: boolean;
    allowNotSure: boolean;
    allowOther?: boolean;
    displayAnswerType?: QuestionAnswerTypes;
    isMultiSelect?: boolean;
    optionHintEnabled?: boolean;
    required?: boolean;
    requireJustification?: boolean;
    link?: string;
    inventoryType?: string;
}

export interface IQuestionTypeAttributes {
    DATE: IQuestionTypeAttributesList;
    MULTICHOICE: IQuestionTypeAttributesList;
    TEXTBOX: IQuestionTypeAttributesList;
    YESNO: IQuestionTypeAttributesList;
    INVENTORY: IQuestionTypeAttributesList;
    PERSONALDATA: IQuestionTypeAttributesList;
    INCIDENT: IQuestionTypeAttributesList;
}

export interface IQuestionType {
    label: string;
    value: string;
    attributes?: IQuestionTypeAttributesList;
}

export interface IQuestionTypeList {
    ATTRIBUTE?: IQuestionType;
    DATE?: IQuestionType;
    MULTICHOICE?: IQuestionType;
    STATEMENT?: IQuestionType;
    TEXTBOX?: IQuestionType;
    YESNO?: IQuestionType;
    INVENTORY?: IQuestionType;
    INCIDENT?: IQuestionType;
}
