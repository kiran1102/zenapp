export interface IMenuGroup {
    name?: string;
    nameKey?: string;
    links: IMenuLink[];
    canToggle?: boolean;
    isToggled?: boolean;
}

export interface IMenuLink {
    name?: string;
    nameKey?: string;
    route?: string;
    params?: any;
    options?: ng.ui.IStateOptions;
    activeRoute?: string;
    action?: (item: IMenuLink) => void;
    icon?: string;
    isUpgrade?: boolean;
    getIsActive?: () => boolean;
}

export interface IMenuConfig {
    betaUI?: boolean;
    isOpen?: boolean;
    toggleMenu?: () => void;
    menuClass?: string;
    menuTitleClass?: string;
}
