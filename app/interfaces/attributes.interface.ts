import { IStringMap } from "interfaces/generic.interface";

export interface IAttributeGroup {
    id?: string;
    name: string;
    nameKey?: string;
    attributes?: string[];
}

export interface IAttributeGroupModalResolve {
    title: string;
    group: IAttributeGroup;
    isNew: boolean;
    inventoryId: string;
}

export interface IAttributePickerModalResolve {
    inventoryId: string;
    group: IAttributeGroup;
    ungroupedAttributes: string[];
}

export interface IAttributeGroupsState {
    isSaving: boolean;
    isLoadingGroups: boolean;
    openGroupMap: IStringMap<boolean>;
}
