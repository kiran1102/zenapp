export interface ITemplateSkipConditionCombination {
    defaultValue: boolean;
    logicalOperator: string;
    combinations: string[];
}
