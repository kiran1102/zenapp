export interface ITemplateCreateModalData {
    name: string;
    description: string;
    icon: string;
    id: string;
    orgGroupId?: string;
    copying?: boolean;
}
