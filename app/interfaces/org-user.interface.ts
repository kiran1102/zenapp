import { OrgUserDropdownTypes, OrgUserTraversal } from "enums/org-user-dropdown.enum";

export interface IOrgUser {
    id: string;
    fullName: string;
}

export interface IOrgUserAdapted {
    Id: string;
    FullName: string;
    errorMessage?: string;
}

export interface IUserDropdownConfig {
    allowOther?: boolean;
    doValidate?: boolean;
    errorText?: string;
    filterCurrentUser: boolean;
    hideLabel?: boolean;
    label?: string;
    labelKey?: string;
    labelDescription?: string;
    orgId?: string;
    otherOptionPrependText?: string;
    permissionKeys?: string[];
    placeholder?: string;
    traversal?: OrgUserTraversal;
    type?: OrgUserDropdownTypes;
    validations?: IUserDropdownValidation[];
    allowEmpty?: boolean;
    emptyText?: string;
    disableIfNoOptions?: boolean;
}

export interface IUserDropdownValidation {
    errorMessage: string;
    id: number;
    validate: (selection: any) => boolean;
}

export interface IUserDropdownValidationResult {
    isValid: boolean;
    errors: string[];
}
