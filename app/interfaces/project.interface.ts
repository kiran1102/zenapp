import { IAssignment } from "interfaces/assignment.interface";
import { ISection } from "interfaces/section.interface";
import { INote } from "interfaces/note.interface";

export interface IProject {
    Assignment: IAssignment | null;
    Assignments: IAssignment[] | null;
    CompletedDT: string | null;
    CreatedDT: string | null;
    Creator: string;
    Deadline: string | null;
    Description: string;
    HasComments: boolean;
    HasRisks: boolean;
    Id: string;
    IsDeletable: boolean;
    IsLatestVersion: boolean;
    IsLinked: boolean;
    Lead: string;
    LeadId: string;
    Name: string;
    NextTemplateName: string;
    Notes: INote[];
    Number: number;
    OrgGroup: string;
    OrgGroupId: string;
    OwnerId: string;
    ProjectRisk: number | null;
    ProjectRiskState: string | null;
    Readonly: boolean;
    Remaining: any[];
    ReminderShow?: boolean;
    ReminderDays: number | null;
    Reviewer: string;
    ReviewerId: string;
    Sections: ISection[];
    StateDesc: string;
    StateLabel: string;
    StateName: string;
    Tags: any[] | null;
    TemplateId: string;
    TemplateName: string;
    TemplateState: string;
    TemplateType: number;
    TemplateVersion: number;
    Version: number;
    time: number;
    localeCreatedDT: string | null;
    CreatedDTtooltip: string | null;
    localeDeadline: string | null;
    Deadlinetooltip: string | null;
    isShared: boolean;
    riskClass: string;
    riskWord: string;
    canBeDuplicated: boolean;
    hasCrossSectionConditions?: boolean;
}

export interface IProjectCreateParams {
    Comment?: string | null;
    Deadline?: string | null;
    Description: string;
    LeadId: string;
    Name: string;
    OrgGroupId: string;
    ProjectGuid?: string;
    ReminderDays?: string | null;
    SectionAssignments?: any[];
    Tags?: string[];
    TemplateId?: string;
    TemplateType?: number;
    TemplateVersion?: string;
}

export interface IUpdateProjectPayload {
    Deadline: string | null;
    Description: string;
    Id: string;
    LeadId: string;
    Name: string;
    OrgGroupId: string;
    OwnerId: string;
    ProjectVersion: number;
    TemplateId: string;
}

export interface ISubmitStates {
    Submit: number;
    SendBack: number;
    Approve: number;
    GoToAnalsis: number;
    NextSteps: number;
    SendRecommendations: number;
    SendMitigations: number;
    CompleteAnalysis: number;
}

export interface ICreateNextProjectParams {
    Comment: string;
    Id: string;
    LeadId: string;
    ProjectVersion: number;
}

export interface ICreateProjectNotePayload {
    ProjectId: string;
    ProjectVersion: string;
    Type: string;
    Value: string;
}

export interface IProjectStatusCount {
    projectsTotal: number;
    projectsInProgress: number;
    projectsUnderReview: number;
    projectsRiskAssessment: number;
    projectsCompleted: number;
    projectsRiskTreatment: number;
    projectsInfoNeeded: number;
    projectsNotStarted: number;
}

export interface IProjectLabels {
    approvedMessage: string;
    breadcrumb: string;
    deadlinesHeadline: string;
    loadingMessage: string;
    noLongerAvailable: string;
    projectApprover: string;
    readOnlyMessage: string;
    riskHeadline: string;
    sendBackTooltip: string;
    submitForReviewTooltip: string;
    successModalMessage: string;
    versionWillLock: string;
}

export interface IProjectForm {
    sourceAssessment: string;
    projectName: string;
    comments: string;
    hasReminder: boolean;
    formName: string;
    reminderDays: number | null;
    deadline: Date;
    minDate: Date | null;
    orgGroupId: string | null;
    respondentId: string;
    allowMultipleRespondent: boolean;
    isMultiRespondent: boolean;
    isShowingDetails: boolean;
    isValidReminder: boolean;
    isRespondentValid: boolean;
    sectionAssignments: IAssignment[] | null;
}

export interface IDuplicateProjectContract {
    Name: string;
    Description: string;
    OrgGroupId: string;
    LeadId: string | null;
    Deadline: Date | null;
    ReminderDays: number | null;
    Tags: null;  // TODO: include tagging
    ProjectId: string;
    ProjectVersion: number;
    TemplateType: string;
    SectionAssignments: IAssignment[] | null;
}
