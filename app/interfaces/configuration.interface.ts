export interface IConfigurationSource {
    id: string;
    src?: string;
    getSrc?: (config) => string;
}

export interface IHelpCenterConfiguration {
    token: string;
    organization: string;
    widgetId?: string;
}

export interface IUserlaneConfiguration {
    token: number;
}

export interface IWootricConfiguration {
    token?: string;
}
