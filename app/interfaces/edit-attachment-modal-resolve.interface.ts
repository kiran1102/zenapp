export interface IEditAttachmentModalResolve {
    modalTitle?: string;
    cancelButtonText?: string;
    submitButtonText?: string;
    attachmentName: string;
    attachmentComment: string;
    requestId: string;
    inventoryId: number;
    callback?: () => void;
}
