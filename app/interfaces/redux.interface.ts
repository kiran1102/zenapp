import { Store } from "redux";
import { Selector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

import { IAssessmentCommentState } from "interfaces/assessment/assessment-comment-reducer.interface";
import { IAssessmentState } from "interfaces/assessment/assessment-reducer.interface";
import { IAttachmentState } from "interfaces/attachment-reducer.interface";
import { IAttributeManagerState } from "interfaces/attribute-reducer.interface";
import { IBrandingState } from "./branding-reducer.interface";
import { IConfigurationState } from "./configuration-reducer.interface";
import { IDrawerState } from "interfaces/drawer-reducer.interface";
import { IInventoryState } from "interfaces/inventory-reducer.interface";
import { ILocalizationState } from "interfaces/localization-reducer.interface";
import { IModalState } from "interfaces/modal-reducer.interface";
import { INeedsMoreInfoState } from "interfaces/assessment/needs-more-info.interface";
import { IOrgState } from "./org-reducer.interface";
import { IOrgUserState } from "interfaces/org-user-reducer.interface";
import { IPreferenceCenterState } from "interfaces/preference-center-reducer.interface";
import { IPurposeState } from "interfaces/purpose-reducer.interface";
import { IRiskState } from "interfaces/risk-reducer.interface";
import { ISettingState } from "interfaces/setting-reducer.interface";
import { ITemplateState } from "interfaces/template-reducer.interface";
import { IUserState } from "interfaces/user-reducer.interface";
import { IInventoryRecordState } from "interfaces/inventory-record-reducer.interface";
import { IVendorState } from "modules/vendor/shared/interfaces/vendorpedia-reducer.interface";

export type IObserver = () => void;

export type IUnsubscribeCallback = () => void;

export interface IAction {
    type: string;
    payload: any;
}

export interface IStoreState {
    aaNeedsMoreInfo: INeedsMoreInfoState;
    assessment: IAssessmentState;
    assessmentComment: IAssessmentCommentState;
    attachment: IAttachmentState;
    attributeManager: IAttributeManagerState;
    branding: IBrandingState;
    configuration: IConfigurationState;
    drawer: IDrawerState;
    inventory: IInventoryState;
    inventoryRecord: IInventoryRecordState;
    localization: ILocalizationState;
    modals: IModalState;
    orgs: IOrgState;
    orgUsers: IOrgUserState;
    preferenceCenter: IPreferenceCenterState;
    purpose: IPurposeState;
    risks: IRiskState;
    settings: ISettingState;
    template: ITemplateState;
    users: IUserState;
    vendor: IVendorState;
}

export interface IStore extends Store<IStoreState> {
    connect(
        mapStateToTarget: (state: any) => object,
        mapDispatchToTarget?: object | ((dispatch: () => void) => object),
    ): (target: () => void | object) => () => void;
}

export interface IFlattenedState<T> {
    allIds: string[];
    byId: Partial<Map<string, T>>;
}

export type IStoreStateSelector = Selector<IStoreState, IStoreState>;

export type IStringToBooleanMap = {} | Map<string, boolean>;

export type IFilterIteratee<T> = (value: T, index?: number, array?: T[]) => boolean;

export type IMapIteratee<T> = (value: T, index?: number, array?: T[]) => any;
