export interface IColorOptions {
    rgb: string;
    code: string;
}
