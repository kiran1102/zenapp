import { IUser } from "interfaces/user.interface.ts";

export interface IToken {
    ".expires"?: string;
    ".issued"?: string;
    access_token: string;
    expiredPassword?: string;
    expires_in?: number;
    orggroup_id?: string;
    refresh_token?: string;
    role_id?: string;
    roles?: string[];
    tenant_id?: string;
    token_type: string;
    userName?: string;
}

export interface ITokenIdentity extends IToken {
    user?: IUser;
}

export interface ITokenRequest {
    client_id: string;
    grant_type: string;
    organization_id?: string;
    orggroup_id?: string;
    role_id?: string;
    tenant_id?: string;
}

export interface ITokenPasswordRequest extends ITokenRequest {
    password: string;
    username: string;
}

export interface ITokenRefreshRequest extends ITokenRequest {
    refresh_token: string;
}

export interface ILoginRequest {
    Email: string;
    Password: string;
}

export interface ITokenError {
    disabled?: boolean;
    error_description: string;
    error: string;
    expired?: boolean;
    invalidPassword?: boolean;
}

export interface IAccessToken {
    authorities: string[];
    client_id: string;
    email: string;
    exp: number;
    guid: string;
    jti: string;
    languageId: number;
    orgGroupGuid: string;
    orgGroupId: string;
    role: string;
    root: string;
    sessionId: string;
    tenantGuid: string;
    tenantId: number;
    user_name: string;
    virtualRole: boolean;
}
