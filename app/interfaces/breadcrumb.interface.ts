export interface IBreadcrumb {
    text: string;
    stateName: string;
    identifier?: string;
}
