import { IPageable } from "interfaces/pagination.interface";

export interface IBasicTable {
    sort?: IBasicTableSort;
    rows: IBasicTableRow[];
    columns: any[];
    metaData: IPageable;
}

export interface IBasicTableSort {
    column: string;
    ascending: boolean;
}

export interface IBasicTableColumn {
    [key: string]: any;
}

export interface IBasicTableRow {
    [key: string]: any;
    cells: IBasicTableCell[];
}

export interface IBasicTableCell {
    [key: string]: any;
}

export interface IDataTableColumn {
    id: string;
    labelKey: string;
    rowTextKey?: string;
    rowTranslationKey?: string;
    type: number;
}
