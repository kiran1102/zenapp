// Redux
import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

// Models
import { PreferenceCenter } from "crmodel/preference-center-list";
import { IDropDownElement } from "crservice/consent-base.service";

// Interfaces
import { IGetAllLanguageResponse } from "interfaces/dsar/dsar-edit-language.interface";

export interface IPreferenceCenterState {
    isDropping: boolean;
    isLoaded: boolean;
    isLoading: boolean;
    isModified: boolean;
    isSaving: boolean;
    isLoadingLanguages: boolean;
    isValid: boolean;
    language?: IGetAllLanguageResponse;
    allLanguages?: IGetAllLanguageResponse[];
    preferenceCenter?: PreferenceCenter;
    preferenceCenterPreview?: PreferenceCenter;
    isDisabledLanguages: boolean;
    selectedPurposes?: IDropDownElement[];
}

export type IPreferenceCenterSelector = OutputSelector<IStoreState, PreferenceCenter, (preferenceCenterState: IPreferenceCenterState) => PreferenceCenter>;
export type IPreferenceCenterStatusSelector = OutputSelector<IStoreState, boolean, (preferenceCenterState: IPreferenceCenterState) => boolean>;
export type IPreferenceCenterLanguageSelector = OutputSelector<IStoreState, IGetAllLanguageResponse, (preferenceCenterState: IPreferenceCenterState) => IGetAllLanguageResponse>;
export type IPreferenceCenterLanguagesSelector = OutputSelector<IStoreState, IGetAllLanguageResponse[], (preferenceCenterState: IPreferenceCenterState) => IGetAllLanguageResponse[]>;
export type IPreferenceCenterSelectedPurposesSelector = OutputSelector<IStoreState, IDropDownElement[], (preferenceCenterState: IPreferenceCenterState) => IDropDownElement[]>;
