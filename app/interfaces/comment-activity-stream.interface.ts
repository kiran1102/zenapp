export interface ICommentActivityStream {
    styleModifier?: string;
    date?: string;
    content?: string;
    prefixText?: string;
    icon?: string;
    name: string;
    email: string;
    showOnlyPrefix?: boolean;
    displayString?: string;
    showStatus: boolean;
    statusText: string;
    statusColor: string;
}
