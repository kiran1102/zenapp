import { IPageableMeta } from "interfaces/pagination.interface";
import { EmailTemplateGroupId } from "modules/settings/enums/email-template.enums";

export interface ISmtpSetting {
    smtpEnabled: boolean;
    smtpHostname: string;
    smtpInstalled: boolean;
    smtpPassword: string;
    smtpPort?: number;
    smtpSenderEmail: string;
    smtpSenderName: string;
    smtpSslTls?: boolean;
    smtpTested: boolean;
    smtpUsername: string;
    htmlButtonBgColor?: string;
    htmlButtonFgColor?: string;
    validate?: boolean;
}

export interface ITableData extends IPageableMeta {
    content: ITableDataContent[];
}

export interface ITableDataContent {
    bodyText: string;
    channelId: number;
    languageId: number;
    messageTypeId: number;
    orgGroupId: string;
    organization: string;
    parentMessageTypeId: number;
    subject: string;
    templateDetail: string;
    templateName: string;
    version?: number;
    currentUser?: string;
    lastModifiedBy: string;
    lastModifiedDate: Date | string;
    templateNameTranslationKey?: string;
}

export interface ILanguage {
    Id: number;
    Code: string;
    Translated: boolean;
    Next: any;
}

export interface ITableColumnList {
    columnType: string;
    columnName: string;
    cellValueKey: string;
    columnIndex: boolean;
}

export interface IGroupFilter {
    description: string;
    id: EmailTemplateGroupId;
    name: string;
    permissionName: string;
    translatedName?: string;
}
