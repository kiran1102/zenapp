import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import { IOrganization } from "interfaces/org.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

export interface IOrgUserState {
    providedOptions: any[];
    users: IOrgUserAdapted[];
}

export type IOrgUserSelector = OutputSelector<IStoreState, IOrganization, (orgState: IOrgUserState) => IOrgUserAdapted[]>;
