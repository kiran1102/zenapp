import { IProject, IProjectLabels } from "interfaces/project.interface";

export interface IProjectDeadlinesModalData {
    Project: IProject;
    Labels: IProjectLabels;
}
