// enum
import { AANotesTypes } from "enums/assessment/assessment-comment-types.enum";

// Interfaces
import { ILabelValue } from "interfaces/generic.interface";

export interface INote {
    Type: number;
    Value: string;
}

export interface INoteState {
    filter: ILabelValue<AANotesTypes>;
}
