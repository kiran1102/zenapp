export interface IFilter {
    columnId?: string;
    attributeKey?: string;
    entityType?: string;
    dataType?: number;
    operator?: string;
    labels?: Array<string | number>;
    values?: Array<string | number>;
}

export interface IFilterTree extends IFilter {
    AND?: IFilter[];
    OR?: IFilter[];
}

export interface ISort {
    columnName: string;
    ascending: boolean;
}

export interface IFilterColumn {
    columnId: string;
    columnName: string;
    entityType: string;
    dataType: number;
    translationKey?: string;
    group?: string;
    groupName?: string;
    groupKey?: string[];
    groupLabel?: string;
    sortable?: boolean;
}

export interface IFilterColumnOption {
    name: string;
    translationKey?: string;
    id: string;
    type: number;
    entityType?: string;
    valueOptions?: IFilterValueOption[];
    valueOptionLink?: IFilterValueOptionLink;
    groupName?: string;
    group?: string;
    groupKey?: string[];
    groupLabel?: string;
    comparatorOptions?: number[];
    sortable?: boolean;
    filterOnKey?: boolean;
}

export interface IFilterValueOptionLink {
    url: string;
}

export interface IFilterValueOptionResponse {
    id: string;
    name: string;
    orgGroupId: string;
}

export interface IFilterValueOption {
    key: string | number;
    value: string | number;
    translationKey?: string;
}
