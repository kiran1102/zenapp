export interface IHelpDialogModal {
    modalTitle: string;
    content: string;
    hideClose?: boolean;
}
