import { IOtHttpRequest } from "modules/core/interfaces/http.interface";
import { IStringMap } from "interfaces/generic.interface";

export interface IProtocolMessage {
    readonly object?: string;
    readonly action?: string;
    readonly custom?: string;
    readonly Hide?: boolean;
}

export interface IProtocolMessages {
    readonly Success?: IProtocolMessage;
    readonly Error?: IProtocolMessage;
}

export type IProtocolConfig = Partial<IOtHttpRequest<any>>;

// Deprecating... Use IProtocolResponse<T> going forward... allows for typing response data instead of any
export interface IProtocolPacket {
    data: any;
    errorCode?: number | string;
    result: boolean;
    errors?: IProtocolError;
    statusText?: string;
    readonly headers?: IStringMap<string>;
    readonly status?: number;
    readonly time?: number;
}

export interface IProtocolResponse<T> {
    data: T;
    errorCode?: number | string;
    result: boolean;
    errors?: IProtocolError;
    statusText?: string;
    readonly headers?: IStringMap<string>;
    readonly status?: number;
    readonly time?: number;
}

export interface IProtocolStatus {
    status: number;
    value?: string;
}

export interface IProtocolError {
    title: string;
    detail: string;
    code: string;
    source: string;
}
