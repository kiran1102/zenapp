export interface ILoginHistoryData {
    content: ILoginHistoryContent[];
    totalElements?: number;
    last?: boolean;
    totalPages?: number;
    sort?: ILoginHistorySort[];
    first?: boolean;
    numberOfElements?: number;
    size?: number;
    number?: number;
}

export interface ILoginHistoryContent {
    userName: string;
    ipAddress: string;
    userAgentString: any;
    createDT: string;
    status: boolean;
}

export interface ILoginHistorySort {
    direction: string;
    property: string;
    ignoreCase: boolean;
    nullHandling: string;
    ascending: boolean;
}
