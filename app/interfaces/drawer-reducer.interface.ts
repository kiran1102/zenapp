import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";

export interface IDrawerState {
    drawerIsOpen: boolean;
}

export type IDrawerStateSelector = OutputSelector<IStoreState, boolean, (drawerState: IDrawerState) => boolean>;
