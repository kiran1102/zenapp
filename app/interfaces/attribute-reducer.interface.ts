import { Selector, OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import {
    IAttributeDetailV2,
    IAttributeV2,
} from "interfaces/inventory.interface";

export interface IAttributeManagerState {
    isLoadingCurrentAttribute: boolean;
    isLoadingAttributeList: boolean;
    currentAttribute: IAttributeDetailV2;
    attributeList: IAttributeV2[];
}

export type ICurrentAttributeSelector = OutputSelector<IStoreState, IAttributeDetailV2, (attributeState: IAttributeManagerState) => IAttributeDetailV2>;
export type IAttributeListSelector = OutputSelector<IStoreState, IAttributeV2[], (attributeState: IAttributeManagerState) => IAttributeV2[]>;
export type IIsLoadingCurrentAttributeSelector = OutputSelector<IStoreState, boolean, (attributeState: IAttributeManagerState) => boolean>;
export type IIsLoadingAttributeListSelector = OutputSelector<IStoreState, boolean, (attributeState: IAttributeManagerState) => boolean>;
