export interface ITenant {
    Id: string;
    LicenseType: number;
    Name: string;
    TenantId: number;
    IsOffline: boolean;
}
