import { Selector, OutputSelector } from "reselect";
import { IStoreState, IStringToBooleanMap } from "interfaces/redux.interface";
import { IOrganization, IOrganizationMap } from "interfaces/org.interface";

export interface IOrgState {
    rootOrgTree: IOrganization | null;
    rootOrgList: IOrganization[];
    rootOrgTable: IOrganizationMap;
    orgList: IOrganization[];
    currentOrgId: string;
    selectedOrgIds: IStringToBooleanMap;
}

export type IRootOrgTreeSelector = OutputSelector<IStoreState, IOrganization, (orgState: IOrgState) => IOrganization>;
export type IRootOrgListSelector = OutputSelector<IStoreState, IOrganization[], (orgState: IOrgState) => IOrganization[]>;
export type IRootOrgTableSelector = OutputSelector<IStoreState, IOrganizationMap, (orgState: IOrgState) => IOrganizationMap>;
export type ICurrentOrgIdSelector = OutputSelector<IStoreState, string, (orgState: IOrgState) => string>;
export type ISelectedOrgIdsSelector = OutputSelector<IStoreState, IStringToBooleanMap, (orgState: IOrgState) => IStringToBooleanMap>;
export type IOrgByIdSelector = OutputSelector<IStoreState, IOrganization, (orgState: IOrgState) => IOrganization>;
export type IOrgByIdSelectorFactory = (id: string) => IOrgByIdSelector;
export type IOrgNameByIdSelector = OutputSelector<IStoreState, string, (orgState: IOrgState) => string>;
export type IOrgNameByIdSelectorFactory = (id: string) => IOrgNameByIdSelector;
export type IOrgListByIdSelector = OutputSelector<IStoreState, IOrganization[], (rootOrgList: IOrganization[], selectedOrg: IOrganization) => IOrganization[]>;
export type IOrgListByIdSelectorFactory = (id: string) => IOrgListByIdSelector;
export type IParentOrgsSelector = OutputSelector<IStoreState, IOrganization[], (rootOrgList: IOrganization[], selectedOrg: IOrganization) => IOrganization[]>;
export type IParentOrgsSelectorFactory = (id: string) => IOrgListByIdSelector;
export type IRelatedOrgsSelector = OutputSelector<IStoreState, IOrganization[], (parentOrgs: IOrganization[], orgList: IOrganization[]) => IOrganization[]>;
export type IRelatedOrgsSelectorFactory = (id: string) => IRelatedOrgsSelector;
export type IRelatedOrgIdsSelector = OutputSelector<IStoreState, string[], (relatedOrgs: IOrganization[]) => string[]>;
export type IRelatedOrgIdsSelectorFactory = (id: string) => IRelatedOrgIdsSelector;
export type ICurrentOrgListSelector = OutputSelector<IStoreState, IOrganization[], (storeState: IStoreState, currentOrgId: string) => IOrganization[]>;
export type ICurrentParentListSelector = OutputSelector<IStoreState, IOrganization[], (storeState: IStoreState, currentOrgId: string) => IOrganization[]>;
export type ICurrentRelatedOrgsSelector = OutputSelector<IStoreState, IOrganization[], (parentOrgs: IOrganization[], orgList: IOrganization[]) => IOrganization[]>;
export type ICurrentRelatedOrgIdsSelector = OutputSelector<IStoreState, string[], (orgList: IOrganization[]) => string[]>;
export type ICurrentRelatedOrgIdsMapSelector = OutputSelector<IStoreState, IStringToBooleanMap, (orgList: IOrganization[]) => IStringToBooleanMap>;
