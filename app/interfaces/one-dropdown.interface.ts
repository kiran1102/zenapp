export interface IDropdownOption {
    readonly bolderOption?: boolean;
    text?: string;
    textKey?: string;
    readonly icon?: string;
    action?: Function;
    readonly route?: string;
    [index: string]: any;
    readonly isDisabled?: boolean;
    readonly toolTip?: string;
    readonly toolTipDirection?: string;
    readonly value?: any;
    rowRefId?: string;
}
