export interface ISmtpSettingRequest {
    validate: boolean;
    settings: {
        [key: string]: string | number;
    };
}
