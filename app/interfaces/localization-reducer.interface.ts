import { OutputSelector } from "reselect";
import { IStoreState } from "interfaces/redux.interface";
import { ITranslationsPage, ITranslation } from "interfaces/localization.interface";

export interface ILocalizationState {
    translationsPage: ITranslationsPage;
    customTranslationsPage: ITranslationsPage;
    isFetchingPage: boolean;
    isFetchingCustomPage: boolean;
}

export type IGetInitialState = () => ILocalizationState;
export type ITranslationsPageSelector = OutputSelector<IStoreState, ITranslationsPage, (localizationState: ILocalizationState) => ITranslationsPage>;
export type ITranslationsPageFetchStatus = OutputSelector<IStoreState, boolean, (localizationState: ILocalizationState) => boolean>;
