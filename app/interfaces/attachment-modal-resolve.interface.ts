export interface IAttachmentModalResolve {
    modalTitle?: string;
    attachmentType: number;
    cancelButtonText?: string;
    submitButtonText?: string;
    allowMultipleUploads?: boolean;
    acceptedFileTypes?: string;
    showFileList?: boolean;
    allowAttachmentNaming?: boolean;
    allowComments?: boolean;
    isInternal?: boolean;
    requestId: string;
    inventoryId: number;
    callback?: () => void;
}
