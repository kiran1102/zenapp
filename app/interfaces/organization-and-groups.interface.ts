import { IProtocolResponse } from "interfaces/protocol.interface";

export interface IOrganizationResponse {
    Id?: string;
    Name: string;
    ParentId?: string;
    LanguageId?: number;
    PrivacyOfficerId: string;
}

export interface IOrganizationModal {
    parentId?: string;
    isAddNext: boolean;
    isEditOrg: boolean;
    modalTitle: string;
    rootId?: string;
    callback: (res: IProtocolResponse<void>) => boolean;
}

export interface ISelectedUser {
    Id: string;
    FullName: string;
}

export interface ISelectUserOrGroup {
    id: string;
    FullName: string;
    type?: string;
}

export interface IOrganizationOrgUserDropdown {
    isValid: boolean;
    selection: ISelectedUser;
}

export interface IOrganizationOrgUserOrGroupDropdown  {
    isValid: boolean;
    selection: ISelectUserOrGroup;
}
