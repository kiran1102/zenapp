import { IProtocolResponse } from "./protocol.interface";

export interface IWebformTextModalResolve {
    typeOfModal: string;
    title?: string;
    callback: (res: IProtocolResponse<string>) => void;
    text: string;
}
