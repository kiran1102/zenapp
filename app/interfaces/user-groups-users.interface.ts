export interface IUserGroupUser {
    memberId?: string;
    fullName: string;
    email?: string;
    createdBy?: string;
    createdDate?: string;
}

export interface IUserGroupUserDisplay extends IUserGroupUser {
    createdByDisplay?: string;
}

export interface ISelectedUsers {
    members: string[];
}

export interface IUserGroupUserSearch {
    search: string;
    value: string;
}

export interface IModifiedUser {
    user?: IUserGroupUser;
    checked?: boolean;
}
