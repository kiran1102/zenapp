import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IUser } from "interfaces/user.interface";
import { RiskV2UpdateActions } from "modules/assessment/types/risk-v2.types";
import { ILabelValue, INameId } from "interfaces/generic.interface";
import { IRiskControlInformation } from "modules/assessment/interfaces/aa-risk-control.interface";

// Constants & Enums
import { InventoryTableIds } from "enums/inventory.enum";
import {
    RiskV2Levels,
    RiskV2States,
} from "enums/riskV2.enum";

export interface IRisk {
    ConditionGroupId: string | null;
    CurrentProjectVersion: number | null;
    Deadline: Date | null;
    Description: string;
    Id: string;
    Level: number | null;
    MitigatedDt: Date | null;
    Mitigation: string | null;
    OriginalProjectVersion: number | null;
    ProjectId: string;
    // ProjectQuestionGuid: string;
    Question?: IRiskQuestion | null;
    QuestionId: string;
    Recommendation: string;
    ReminderDays: number | null;
    RequestedException?: string | null;
    RiskImpactLevel: number | null;
    RiskOwnerId: string;
    RiskProbabilityLevel: number | null;
    PreviousState?: number | null;
    State: number | null;
    isSaving?: boolean;
    editingDescription?: boolean;
    editingRecommendation?: boolean;
    editingMitigation?: boolean;
    editingException?: boolean;
    showingException?: boolean;
    showingMitigation?: boolean;
    PendingAssessment?: boolean;
    orgGroupId?: string;
}

export interface IRiskQuestion {
    IsShowing: boolean;
    SectionId?: string;
    Id: string;
}

export interface IRiskSerialized {
    QuestionId: string;
    RiskLevel: number;
    Description: string;
    Recommendation: string;
    RiskImpactLevel: number;
    RiskProbabilityLevel: number;
    Deadline: Date;
    ReminderDays: number;
    RiskOwnerId: string;
    Mitigation: string;
    RequestedException: string;
}

export interface IRiskSerializedV2 {
    levelId: number | string;
    typeId: number;
    type: string;
    orgGroupId: string;
    references: IRiskReference[];
    sourceTypeId: number;
    sourceType: string;
    description?: string;
    recommendation?: string;
    impactLevel?: number | string;
    probabilityLevel?: number | string;
    deadline?: Date;
    reminderDays?: number;
    riskOwnerId?: string;
    mitigation?: string;
    requestedException?: string;
    conditionGroupGuid?: string;
    action?: RiskV2UpdateActions;
}

export interface IRiskSerializedV2New {
    levelId: number | string;
    typeId: number;
    type: string;
    orgGroupId: string;
    references: IRiskReference[];
    sourceTypeId: number;
    sourceType: string;
    description?: string;
    recommendation?: string;
    impactLevel?: number | string;
    probabilityLevel?: number | string;
    deadline?: Date;
    reminderDays?: number;
    riskOwnerId?: string;
    mitigation?: string;
    requestedException?: string;
    conditionGroupGuid?: string;
    action?: RiskV2UpdateActions;
    riskScore: number;
}

export interface IFetchingRisksResponse {
    AllIds: string[];
    ById: Map<string, IRisk>;
    NeedsAttention: string[];
    Project?: IProject;
    SelectedRiskIds?: Map<string, boolean>;
}

export interface IRiskSummaryPermissionConfig {
    heatmap: boolean;
    canViewProjectList: boolean;
}

export interface IRiskModalPermissionConfig {
    riskLevel: boolean;
    heatmap: boolean;
    deadline: boolean;
    owner: boolean;
    description: boolean;
    recommendation: boolean;
    mitigation: boolean;
    requestedException: boolean;
}

export interface IRiskStatusFilter {
    Filtered: boolean;
    value: string;
    label: any;
    lookupKey: number;
}

export interface IRiskStatusFilters {
    filters: IRiskStatusFilter[];
    allStatesEnabled: boolean;
}

export interface IHeatMapTile {
    xValue: number;
    yValue: number;
    riskLevel: number;
    htmlClass: string;
}

export interface IRiskHeatMapLabels {
    xAxis: string[];
    xAxisLabel: string;
    yAxis: string[];
    yAxisLabel: string;
}

export interface IGraphSettings {
    Axes: IRiskAxis[];
}

export interface IRiskAxis {
    Label: string;
    Points: IRiskAxisPoint[];
}

export interface IRiskAxisPoint {
    Label: string;
    Value: number;
}

export interface IRiskMapItem {
    RiskLevel: number;
    Axis1Value: number;
    Axis2Value: number;
}

export interface IRiskPrettyNames {
    [index: number]: string;
}

export interface IAssessmentRiskModalData {
    reminderDays: number;
    projectModel: IProject;
    section: ISection;
    callback: () => void;
}

export interface IRiskColumnTypes {
    ProjectRisk: number;
    ProjectName: number;
    ProjectState: number;
    Description: number;
    Recommendation: number;
    Status: number;
    SectionName: number;
    Question: number;
    QuestionFriendlyName: number;
    QuestionAnswer: number;
    QuestionJustification: number;
    RiskDeadline: number;
    MitigatedDt: number;
}

export interface IRiskStates {
    Identified: number;
    Analysis: number;
    Addressed: number;
    Exception: number;
    Accepted: number;
}

export interface IRiskGetPageParams {
    page: number;
    size?: number;
    filters: IRiskGetPageParamsFilter[];
}

export interface IRiskGetPageParamsFilter {
    name: string;
    value: number[];
}

export interface IRiskStateFilter {
    Filtered: boolean;
    label: string;
    lookupKey: number;
    value: string;
}

export interface IRiskSummaryContentViewConfig {
    riskStatusIconClass: string;
    riskStatusText: string;
    riskLevelSymbolClass: string;
    riskLevelText: string;
    flagRiskButtonEnabled: boolean;
    openModalButtonEnabled: boolean;
    deleteRiskButtonEnabled: boolean;
    canShowDetails: boolean;
    button1Enabled: boolean;
    button1Text: string;
    button1Action: string;
    button2Enabled: boolean;
    button2Text: string;
    button2Action: string;
    button2Type: string;
    treatmentLabelText: string;
    treatmentModelProp: string;
    textareaLabel: string;
    textareaPlaceholder: string;
    textareaModelProp: string;
    textareaSaveButtonText: string;
    textareaSubmitAction: string;
    contextMenuEnabled: boolean;
    contextMenuOptions: IDropdownOption;
}

export interface IRiskModalViewConfig {
    riskLevelShown: boolean;
    riskLevelEnabled: boolean;
    riskHeatmapShown: boolean;
    riskHeatmapEnabled: boolean;
    riskDeadlineShown: boolean;
    riskDeadlineEnabled: boolean;
    riskOwnerShown: boolean;
    riskOwnerEnabled: boolean;
    riskSummaryShown: boolean;
    riskSummaryEnabled: boolean;
    riskRecommendationShown: boolean;
    riskRecommendationEnabled: boolean;
    riskRemediationShown: boolean;
    riskRemediationEnabled: boolean;
    riskExceptionShown: boolean;
    riskExceptionEnabled: boolean;
}

export interface IRiskDetails {
    id: string;
    level: RiskV2Levels | string;
    probabilityLevel?: string;
    probabilityLevelId?: number;
    impactLevel?: string;
    impactLevelId?: number;
    state: RiskV2States;
    previousState: string;
    typeId: number;
    type?: string;
    typeRefId?: string;
    sourceType?: string;
    sourceTypeId: number;
    description?: string;
    recommendation?: string;
    conditionGroupUuid?: string;
    requestedException?: string;
    mitigation?: string;
    deadline?: Date;
    reminderDays?: number;
    mitigatedDate?: Date;
    orgGroupId?: string;
    references?: IRiskReference[];
    riskOwner?: string;
    riskOwnerId?: string;
    conditionGroupGuid?: string;
    action?: RiskV2UpdateActions;
    actionId?: number;
    justification?: string;
    controlsIdentifier?: string[];
    riskControls?: IRiskControlInformation[];
    createdUTCDateTime?: Date;
    riskScore?: number;
    levelId?: number;
    levelDisplayName?: string;
    riskApproversId?: string[];
    orgGroup?: INameId<string>;
    source?: IRiskReference;
    viewOnly?: boolean;
    number?: number;
    categories?: IRiskCategoryBasicInfo[];
}

export interface IRiskCreateRequest {
    id: string;
    probabilityLevel?: string;
    probabilityLevelId?: number;
    impactLevel?: string;
    impactLevelId?: number;
    level: RiskV2Levels | string;
    levelId?: number;
    state: RiskV2States;
    previousState: string;
    typeId: number;
    sourceTypeId: number;
    description?: string;
    recommendation?: string;
    conditionGroupUuid?: string;
    requestedException?: string;
    mitigation?: string;
    deadline?: Date;
    reminderDays?: number;
    mitigatedDate?: Date;
    orgGroupId?: string;
    references?: IRiskReference[];
    riskScore?: number;
    riskOwner?: string;
    riskOwnerId?: string;
    orgGroup?: INameId<string>;
    type?: string;
    sourceType?: string;
    source?: IRiskReference;
    categoryIds?: string[];
}

export interface IRiskDetailsRegisterView extends IRiskDetails {
    controlsCount?: number;
    categoriesCsv?: string;
}

export interface IRiskMetadata {
    riskId: string;
    levelId: number;
    questionId: string;
    riskResponse?: IRiskDetails;
}

export interface IRiskCreateRequestModel {
    sectionId: string;
    questionId: string;
    riskCreateRequest: IRiskCreateRequest;
}

export interface IRiskUpdateRequest {
    action: RiskV2UpdateActions;
    deadline: Date;
    reminderDays?: number;
    sourceType?: string;
    description: string;
    id: string;
    impactLevelId: number;
    levelId: number;
    mitigation: string;
    orgGroupId: string;
    probabilityLevelId: number;
    recommendation: string;
    references: IRiskReference[];
    requestedException: string;
    riskApproversId: string[];
    riskOwner: string;
    riskOwnerId: string;
    riskScore: number;
    state: string;
    categoryIds?: string[];
}

export interface IRiskReference {
    id: string;
    version?: number;
    typeId?: number;
    type?: string;
    name?: string;
    additionalAttributes?: IAdditionalAttributes;
}

export interface IAdditionalAttributes {
    inventoryType: InventoryTableIds;
}

export interface IRiskFilter {
    name: string;
    values: Array<string | number>;
}

export interface IRiskModalData {
    viewService: string;
    inventoryName?: string;
    userList?: IUser[];
    inventoryOrgId?: string;
    saveRisk: (string, IRiskDetails) => ng.IPromise<IRiskDetails | boolean>;
    callback?: (IRiskDetails) => void;
    setRisk?: () => ng.IPromise<boolean>;
    selectedModelId?: string;
}

export interface IRiskModelState {
    riskStatusIconClass: string;
    riskStatusText: string;
    treatmentLabelText: string;
}

export interface IRiskStatistics {
    sectionId: string;
    riskCount: number;
    maxRiskLevel: number;
}

export interface IRiskPaneFilter {
    riskFilter: ILabelValue<number>;
}

export interface IRiskRepoTable {
    cellValueKey: string;
    columnName: string;
    columnNameTranslationKey?: string;
    columnType: string;
    isMandatory?: boolean;
    name: string;
    sortKey?: string;
    style?: {
        width: string;
    };
}

export interface IUnlinkRiskReference {
    id: string;
    type: string;
    name: string;
}

export interface IAssessmentRiskUnlinkRequest {
    riskIds: string[];
    reference: IUnlinkRiskReference;
}

export interface IRiskCategoryBasicInfo {
    id: string;
    name: string;
    nameKey?: string;
}

export interface IRiskCategoryDetail extends IRiskCategoryBasicInfo {
    seeded: boolean;
    archived: boolean;
    description?: string;
    descriptionKey?: string;
}
