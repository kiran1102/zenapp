import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader } from "@angular/core";
import { UpgradeModule } from "@angular/upgrade/static";
import { UIRouterModule } from "@uirouter/angular";
import { UIRouterUpgradeModule } from "@uirouter/angular-hybrid";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";
import {
    getAngularJsHttp,
    getAngularJsQ,
    getAngularJsScope,
} from "./legacy.providers";
import { UrlHandlingStrategy } from "@angular/router";
import { CustomUrlHandlingStrategy } from "./custom-url-handling-strategy";

// Two ways to handle URLs:
// 1. Overriding UrlHandlingStrategy
// 2. Empty path sink route
// tell Angular to only process custom route
// class CustomUrlHandlingStrategy implements UrlHandlingStrategy {
//     shouldProcessUrl(url) {
//         return url.toString().startsWith("/feature1") || url.toString() === "/";
//     }
//     extract(url) { return url; }
//     merge(url, whole) { return url; }
// }

// import modules and declare providers that will go away once migration is complete
@NgModule({
    imports: [
        UpgradeModule,
        AppModule,
        UIRouterUpgradeModule,
        UIRouterModule,
    ],
    providers: [
        { provide: UrlHandlingStrategy, useClass: CustomUrlHandlingStrategy }, // tell Angular to only process custom route
        { provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader },
        { provide: "$rootScope", deps: ["$injector"], useFactory: getAngularJsScope },
        { provide: "$q", deps: ["$injector"], useFactory: getAngularJsQ },
        { provide: "$http", deps: ["$injector"], useFactory: getAngularJsHttp },
        { provide: "local", useValue: false },
    ],
    bootstrap: [AppComponent], // used when loading Angular first. disable this and uncomment <ui-view></ui-view> in index.html to load AngularJs first
})
export class Ng2Module {
    ngDoBootstrap() {
        /* no body: this disables normal (non-hybrid) Angular bootstrapping */
    }
}
