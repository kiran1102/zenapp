import $ from "jquery";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OneSelectCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    model: string;

    private translate: Function;
    private translationKey: string;
    private labelKey: string;
    private resetToFirstSelection: boolean;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    private getLabel(option: any): string {
        return option ? (option[this.translationKey] ? this.translate(option[this.translationKey]) : option[this.labelKey]) : option;
    }

    private resetToFirstSelectionFn(): void {
        if (this.resetToFirstSelection) {
            this.model = null;
            $("#oneSelectDropdown option:first").attr("selected", "selected");
        }
    }

}

export default {
    bindings: {
        name: "@?",
        model: "=",
        onChange: "&?",
        disablePlaceholderOption: "<?",
        isDisabled: "<?",
        isRequired: "@?",
        options: "<",
        labelKey: "@?",
        translationKey: "@?",
        valueKey: "@?",
        placeholderText: "@?",
        selectClass: "@?",
        resetToFirstSelection: "<?",
        identifier: "@",
        modifierClass: "@?",
    },
    controller: OneSelectCtrl,
    template: `
            <div id="oneSelectDropdown" class="one-select {{$ctrl.modifierClass}}">
                <select
                    data-id="{{::$ctrl.identifier}}"
                    ng-if="$ctrl.labelKey && $ctrl.valueKey"
                    class="one-select__select {{$ctrl.selectClass}}"
                    name="$ctrl.name"
                    ng-model="$ctrl.model"
                    ng-change="$ctrl.onChange({selection: $ctrl.model}); $ctrl.resetToFirstSelectionFn()"
                    ng-disabled="$ctrl.isDisabled"
                    ng-required="$ctrl.isRequired"
                    ng-options="option[$ctrl.valueKey] as $ctrl.getLabel(option) for option in $ctrl.options">
                        <option
                            ng-if="$ctrl.placeholderText"
                            ng-disabled="$ctrl.disablePlaceholderOption"
                            value=""
                            selected>
                                {{$ctrl.placeholderText}}
                        </option>
                </select>
                <select
                    data-id="{{::$ctrl.identifier}}"
                    ng-if="$ctrl.labelKey && !$ctrl.valueKey"
                    class="one-select__select {{$ctrl.selectClass}}"
                    name="$ctrl.name"
                    ng-model="$ctrl.model"
                    ng-change="$ctrl.onChange({selection: $ctrl.model})"
                    ng-disabled="$ctrl.isDisabled"
                    ng-required="$ctrl.isRequired"
                    ng-options="$ctrl.getLabel(option) for option in $ctrl.options">
                        <option
                            ng-if="$ctrl.placeholderText"
                            ng-disabled="$ctrl.disablePlaceholderOption"
                            value=""
                            selected>
                                {{$ctrl.placeholderText}}
                        </option>
                </select>
                <select
                    data-id="{{::$ctrl.identifier}}"
                    ng-if="!($ctrl.labelKey || $ctrl.valueKey)"
                    class="one-select__select {{$ctrl.selectClass}}"
                    name="$ctrl.name"
                    ng-model="$ctrl.model"
                    ng-change="$ctrl.onChange({selection: $ctrl.model})"
                    ng-disabled="$ctrl.isDisabled"
                    ng-required="$ctrl.isRequired"
                    ng-options="option for option in $ctrl.options">
                        <option
                            ng-if="$ctrl.placeholderText"
                            ng-disabled="$ctrl.disablePlaceholderOption"
                            value=""
                            selected>
                                {{$ctrl.placeholderText}}
                        </option>
                </select>
                <i class="one-select__arrow fa fa-caret-down"></i>
            </div>
        `,
};
