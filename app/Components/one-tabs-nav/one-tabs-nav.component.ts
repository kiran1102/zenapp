import {
    isFunction,
    isObject,
} from "lodash";
import { Subscription } from "rxjs";
import { IWindowObservablesService } from "interfaces/window-observables.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";

class TabsNavController implements ng.IComponentController {
    static $inject: string[] = ["$timeout", "WindowObservables"];

    private selectedTab: any;
    private options: ITabsNav[];
    private resizeSubscription: Subscription;
    private resolveBeforeTabChange: () => ng.IPromise<boolean> | boolean;

    constructor(readonly $timeout: ng.ITimeoutService, readonly WindowObservables: IWindowObservablesService) {}

    $onInit() {
        this.resizeSubscription = this.WindowObservables.get("RESIZE").subscribe(() => setTimeout((): void => this.setSliderCss(), 400));

        this.$timeout((): void => {
            this.setSliderCss();
        }, 500, true);
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.selectedTab) {
            this.setSliderCss();
        }
    }

    public $onDestroy(): void {
        if (this.resizeSubscription) {
            this.resizeSubscription.unsubscribe();
        }
    }

    private changeTab(option: ITabsNav): void {
        if (isFunction(this.resolveBeforeTabChange)) {
            const resolve = this.resolveBeforeTabChange();
            if (isObject(resolve)) {
                (resolve as ng.IPromise<boolean>).then((continueChange: boolean) => {
                    if (continueChange) {
                        this.startTabChange(option);
                    }
                });
            } else {
                this.startTabChange(option);
            }
        } else {
           this.startTabChange(option);
        }
    }

    private startTabChange(option: ITabsNav) {
        this.selectedTab = option.id;
        setTimeout((): void => this.setSliderCss(), 400);
        if (isFunction(option.action)) {
            option.action(option);
        }
    }

    private setSliderCss(): void {
        const selectedId: string = "tab-" + this.selectedTab;
        const element: HTMLElement = document.getElementById(selectedId);
        const tabElement: HTMLElement = document.getElementById("tab");
        if (element && tabElement) {
            tabElement.style.width = `${element.offsetWidth}px`;
            tabElement.style.left = `${element.offsetLeft}px`;
        }
    }

}

const OneTabsNav: ng.IComponentOptions = {
    bindings: {
        selectedTab: "<",
        options: "<",
        customClass: "<?",
        resolveBeforeTabChange: "&?",
    },
    controller: TabsNavController,
    template: `
        <div class="one-tabs-nav position-relative" ng-class="$ctrl.customClass">
            <div class="one-tabs-nav__container row-horizontal-center">
                <div class="one-tabs-nav__item margin-left-right-2 margin-bottom-half text-center"
                     ng-repeat="option in $ctrl.options track by option.id"
                     ng-init="$ctrl.setSliderCss()"
                    >
                    <span
                        ot-auto-id="{{'OneTabActionNavigation'+option.text}}"
                        class="padding-bottom-1"
                        ng-if="option.action"
                        ng-click="$ctrl.changeTab(option)"
                        ng-class="{'one-tabs-nav__item--active one-tabs-nav__item--active-{{::$ctrl.tabHighlightClass}}': $ctrl.selectedTab===option.id}"
                        id="{{'tab-' + option.id}}"
                        >
                        {{::option.text}}
                    </span>
                    <a
                        ot-auto-id="{{'OneTabRouteNavigation'+option.text}}"
                        class="padding-bottom-1"
                        ng-if="option.route"
                        ui-sref="{{option.route}}"
                        ng-class="{'one-tabs-nav__item--active one-tabs-nav__item--active-{{::$ctrl.tabHighlightClass}}': $ctrl.selectedTab===option.id}"
                        id="{{'tab-' + option.id}}"
                        >
                        {{::option.text}}
                    </a>
                </div>
            </div>
            <span class="one-tabs-nav__item--tab-highlight position-absolute" id="tab"></span>
            <div class="one-tabs-nav__separator full-width margin-top-half"></div>
        </div>
    `,
};

export default OneTabsNav;
