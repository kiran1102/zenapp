// 3rd Party
import {
    assign,
    map,
    filter,
} from "lodash";

// Services
import Utilities from "Utilities";
import { ProjectService } from "oneServices/project.service";
import SettingStore from "oneServices/setting-store.service";
import TemplateService from "oneServices/templates.service";
import Sections from "oneServices/section.service";

// Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { TemplateTypes } from "enums/template-type.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IOrganization } from "interfaces/organization.interface";
import {
    IProject,
    IProjectForm,
    IDuplicateProjectContract,
} from "interfaces/project.interface";
import { IAssignment } from "interfaces/assignment.interface";
import { IProjectSettings } from "interfaces/setting.interface";
import { ITemplateAPIParams } from "modules/template/interfaces/template.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStringMap } from "interfaces/generic.interface";

class CreateProjectFormController {
    static $inject: string[] = [
        "$rootScope",
        "$q",
        "$state",
        "OrgGroupStoreNew",
        "SettingStore",
        "Projects",
        "Permissions",
        "Sections",
        "Templates",
    ];

    isMakingRequest: boolean;
    orgId: string;
    orgList: IOrganization[];
    permissions: IStringMap<boolean>;
    projectForm: IProjectForm;
    projectSource: IProject;
    routes: any;
    selectedOrg: IOrganization;
    showMoreDetails: boolean;
    translations: IStringMap<string>;
    userDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.RespondentPia;

    private isDataMappingTemplate: boolean;
    private settings: IProjectSettings;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $q: ng.IQService,
        private $state: ng.ui.IStateService,
        private OrgGroupStoreNew: any,
        private settingStore: SettingStore,
        private Projects: ProjectService,
        private permissionsService: Permissions,
        private sections: Sections,
        private Templates: TemplateService,
    ) { }

    $onInit() {

        const term: any = {
            singular: this.$rootScope.customTerms.Project,
            plural: this.$rootScope.customTerms.Projects,
        };

        this.translations = {
            AddMultipleRespondents: this.$rootScope.t("AddMultipleRespondents"),
            Me: this.$rootScope.t("Me"),
            Cancel: this.$rootScope.t("Cancel"),
            Comments: this.$rootScope.t("Comments"),
            Deadline: this.$rootScope.t("Deadline"),
            EnterDeadlineDate: this.$rootScope.t("EnterDeadlineDate"),
            DeadlineWillBeSelectedDateAt1159PmUtc: this.$rootScope.t("DeadlineWillBeSelectedDateAt1159PmUtc"),
            DuplicateProject: this.$rootScope.t("DuplicateProject", { Project: term.singular }),
            Projects: this.$rootScope.t("Projects"),
            Name: this.$rootScope.t("Name"),
            Organization: this.$rootScope.t("Organization"),
            Reminders: this.$rootScope.t("Reminders"),
            Respondent: this.$rootScope.t("Respondent"),
            Send: this.$rootScope.t("Send"),
            SourceAssessment: this.$rootScope.t("SourceAssessment"),
            User: this.$rootScope.t("User"),
            ShowMoreDetails: this.$rootScope.t("ShowMoreDetails"),
            ShowFewerDetails: this.$rootScope.t("ShowFewerDetails"),
            ReminderMustBeAtLeastOneDayInTheFuture: this.$rootScope.t("ReminderMustBeAtLeastOneDayInTheFuture"),
            EnterProjectName: this.$rootScope.t("EnterProjectNameDuplicate", { ProjectTerm: term.singular }),
            ErrorEnterProjectName: this.$rootScope.t("ErrorEnterProjectNameDuplicate", { ProjectTerm: term.singular }),
            ExceedCharacterLengthName: this.$rootScope.t("ExceedCharacterLengthProjectNameDuplicate", { ProjectTerm: term.singular }),
            EnterComments: this.$rootScope.t("EnterComments"),
            SelectOrganizationGroup: this.$rootScope.t("SelectOrganizationGroup"),
        };

        if (this.$state.includes("zen.app.pia.module.projects.duplicate")) {
            this.translations.headline = this.$rootScope.t("DuplicateProject", { Project: this.$rootScope.customTerms.Project });
        }

        this.routes = {
            projects: "zen.app.pia.module.projects.list",
            oldDataMapping: "zen.app.pia.module.datamap.views.assessment.list",
        };

        this.permissions = {
            projectsDeadline: this.permissionsService.canShow("ProjectsDeadline"),
        };

        const promises: any = [
            this.getProjectSource(),
            this.getSettings(),
        ];

        this.$q.all(promises).then((project: any[]): any => {
            this.orgList = this.OrgGroupStoreNew.orgList;
            const projectSource: IProject = assign({}, this.projectSource);
            this.orgId = projectSource.OrgGroupId;
            this.isDataMappingTemplate = this.projectSource.TemplateType === TemplateTypes.Datamapping;
            this.projectForm = this.Projects.setDuplicateProjectForm(projectSource, this.settings);
            this.selectedOrg = this.OrgGroupStoreNew.orgList[0];
            this.isMakingRequest = false;
        });
    }

    updateDeadline(deadline: Date) {
        this.projectForm.deadline = deadline;
        this.validateReminder();
    }

    cancel(): void {
        const route: string = this.isDataMappingTemplate ? this.routes.oldDataMapping : this.routes.projects;
        this.$state.go(route);
    }

    handleAction(action: string, payload: any): void {
        switch (action) {
            case "ORG_SELECTED":
                this.selectedOrg = payload.org;
                this.projectForm = assign({}, this.projectForm, { orgGroupId: payload.org.id });
                break;
            case "TOGGLE_SHOW_MORE_DETAILS":
                this.showMoreDetails = !this.showMoreDetails;
                this.projectForm = assign({}, this.projectForm, { isShowingDetails: this.showMoreDetails });
                break;
            case "PROJECT_RESPONDENT_SELECTED":
                this.projectForm = assign({}, this.projectForm, { respondentId: payload.respondentId });
                this.projectForm.sectionAssignments = map(this.projectForm.sectionAssignments, (assignment: IAssignment): IAssignment => {
                    return assign({}, assignment, { AssigneeId: payload.respondentId });
                });
                break;
            case "TOGGLE_REMINDERS_CHECKBOX":
                this.projectForm = assign({}, this.projectForm, { hasReminder: this.projectForm.hasReminder });
                this.validateReminder();
                break;
            case "REMINDER_DAYS_CHANGED":
                this.projectForm = assign({}, this.projectForm, { reminderDays: this.projectForm.reminderDays });
                this.validateReminder();
                break;
            case "DEADLINE_CHANGED":
                this.projectForm = assign({}, this.projectForm, { deadline: this.projectForm.deadline });
                this.validateReminder();
                break;
            case "TOGGLE_MULTI_RESPONDENTS_CHECKBOX":
                this.projectForm = assign({}, this.projectForm, { isMultiRespondent: this.projectForm.isMultiRespondent });
                break;
            case "RESPONDENT_ADDED_TO_SECTION":
                this.projectForm = assign({}, this.projectForm, { respondentId: null });
                this.projectForm.sectionAssignments = map(this.projectForm.sectionAssignments, (assignment: IAssignment): IAssignment => {
                    if (assignment.SectionId !== payload.templateSectionId) return assignment;
                    return assign({}, assignment, { AssigneeId: payload.respondentId });
                });
                break;
        }
    }

    respondentSelected = (respondent: any, isValid: boolean, templateSectionId?: string): void => {
        this.projectForm.isRespondentValid = isValid;
        const respondentId: string = respondent.Id || respondent;
        if (!this.projectForm.isMultiRespondent) {
            this.handleAction("PROJECT_RESPONDENT_SELECTED", { respondentId });
        } else {
            this.handleAction("RESPONDENT_ADDED_TO_SECTION", { respondentId, templateSectionId });
        }
    }

    duplicateProject() {
        this.isMakingRequest = true;
        const contract: IDuplicateProjectContract = this.Projects.composeDuplicateProjectContract(this.projectForm, this.projectSource);
        return this.Projects.duplicateProject(contract).then((res: IProtocolPacket): void => {
            if (res.result) {
                const route: string = this.isDataMappingTemplate ? this.routes.oldDataMapping : this.routes.projects;
                this.$state.go(route);
            } else {
                this.isMakingRequest = false;
            }
        });
    }

    private getSettings(): ng.IPromise<IProjectSettings> {
        return this.settingStore.fetchProjectSettings().then((result: IProjectSettings) => {
            this.settings = result;
            return this.$q.when(this.settings);
        });
    }

    private getProjectSource = (): ng.IPromise<IProject | void> => {
        this.isMakingRequest = true;
        const id: string = this.$state.params.id;
        const version: string = this.$state.params.version;
        return this.Projects.read(id, version).then((res: IProtocolPacket): ng.IPromise<IProject | void> => {
            if (res.result) {
                this.projectSource = res.data;
                this.projectSource.Sections = filter(this.projectSource.Sections, this.sections.excludeWelcomeSection);
                return this.getTemplate(this.projectSource.TemplateId, this.projectSource.TemplateVersion);
            } else {
                this.$state.go(this.routes.projects);
            }
        });
    }

    private getTemplate(id: string, version: number = 1): ng.IPromise<IProject> {
        const _version = version.toString();
        const params: ITemplateAPIParams = { id, version: _version };
        return this.Templates.readTemplate(params).then((response: IProtocolPacket) => {
            // Reason we need this call is to see if a Template HasCrossSectionConditions (hides MultiRespondent checkbox if true)
            this.projectSource.hasCrossSectionConditions = response.data.HasCrossSectionConditions;
            return this.$q.when(this.projectSource);
        });
    }

    private validateReminder = (): void => {
        const isValidReminder: boolean = Utilities.validateReminder(this.projectForm.deadline, this.projectForm.reminderDays);
        this.projectForm = assign({}, this.projectForm, { isValidReminder });
    }
}

const createProjectForm = {
    controller: CreateProjectFormController,
    template: `
        <loading ng-if="$ctrl.isMakingRequest"></loading>
        <section class="create-project-form create-project-form" ng-if="!$ctrl.isMakingRequest">
            <header class="old-one-header">
                <ol class="breadcrumb create-project-form__header">
                    <li>
                        <a ui-sref-active="active" ui-sref="{{$ctrl.routes.projects}}">
                            <i class="fa fa-chevron-left"></i>
                            <span>{{::$ctrl.translations.Projects}}&nbsp</span>
                        </a>
                    </li>
                    <li class="active">
                        <span>{{$ctrl.translations.headline}}</span>
                    </li>
                </ol>
            </header>
            <div class="create-project-form__container">
                <h2 class="text-center text-huge margin-top-bottom-2 color-green">{{$ctrl.projectForm.formName}}</h2>
                <form class="create-project-form__form padding-left-right-2 center-block" name="projectInfoForm">
                    <section class="row-vertical-center margin-top-bottom-2">
                        <label class="create-project-form__label text-right margin-right-2" for="sourceAssessment">{{::$ctrl.translations.SourceAssessment}}</label>
                        <div class="create-project-form__input-group">
                            <p class="create-project-form__text">{{::$ctrl.projectForm.sourceAssessment}}</p>
                        </div>
                    </section>
                    <section
                        class="row-vertical-center margin-bottom-half"
                        ng-class="{'has-error': projectInfoForm.Name.$invalid && (projectInfoForm.Name.$touched || projectInfoForm.Name.$dirty)}">
                        <label
                            class="create-project-form__label text-right margin-right-2"
                            for="newProjectName">
                            {{::$ctrl.translations.Name}}
                            <sup class="fa fa-asterisk"></sup>
                        </label>
                        <div class="create-project-form__input-group">
                            <input
                                class="create-project-form__input one-input full-width block"
                                id="newProjectName"
                                type="text"
                                name="Name"
                                required
                                ng-maxlength="100"
                                ng-model="$ctrl.projectForm.projectName"
                                placeholder="{{$ctrl.translations.EnterProjectName}}"/>
                            <p
                                class="text-error text-left text-small margin-all-half"
                                ng-show="(projectInfoForm.Name.$touched || projectInfoForm.Name.$dirty) && projectInfoForm.Name.$error.required">
                                {{$ctrl.translations.ErrorEnterProjectName}}
                            </p>
                            <p
                                class="text-error text-left text-small margin-all-half"
                                ng-show="(projectInfoForm.Name.$touched || projectInfoForm.Name.$dirty) && projectInfoForm.Name.$error.maxlength">
                                {{$ctrl.translations.ExceedCharacterLengthName}}
                            </p>
                        </div>
                    </section>
                    <section class="row-vertical-center margin-bottom-1">
                        <label
                            class="create-project-form__label text-right margin-right-2">
                        </label>
                        <div class="create-project-form__input-group padding-right-1 text-right">
                            <p
                                class="text-xsmall margin-top-bottom-0 inline-block"
                                ng-if="!$ctrl.showMoreDetails"
                                ng-click="$ctrl.handleAction('TOGGLE_SHOW_MORE_DETAILS')">
                                {{$ctrl.translations.ShowMoreDetails}}
                            </p>
                            <p
                                class="text-xsmall margin-top-bottom-0 inline-block"
                                ng-if="$ctrl.showMoreDetails"
                                ng-click="$ctrl.handleAction('TOGGLE_SHOW_MORE_DETAILS')">
                                {{$ctrl.translations.ShowFewerDetails}}
                            </p>
                            <i
                                class="fa text-xsmall"
                                ng-class="{'fa-caret-down': !$ctrl.showMoreDetails, 'fa-caret-up': $ctrl.showMoreDetails }"
                                ng-click="$ctrl.handleAction('TOGGLE_SHOW_MORE_DETAILS')">
                            </i>
                        </div>
                    </section>
                    <section ng-if="$ctrl.showMoreDetails" class="row-vertical-center margin-bottom-2">
                        <label class="create-project-form__label text-right margin-right-2" for="project-comments">{{::$ctrl.translations.Comments}}</label>
                        <div class="create-project-form__input-group">
                            <textarea
                                class="create-project-form__textarea resize-none"
                                id="project-comments"
                                type="text"
                                name="Comments"
                                ng-model="$ctrl.projectForm.comments"
                                placeholder="{{$ctrl.translations.EnterComments}}">
                            </textarea>
                        </div>
                    </section>
                    <section ng-if="$ctrl.showMoreDetails" class="row-vertical-center margin-bottom-2" ng-if="$ctrl.permissions.projectsDeadline">
                        <label class="create-project-form__label text-right margin-right-2">
                            {{::$ctrl.translations.Deadline}}
                            <i
                                class="fa fa-info-circle"
                                uib-tooltip="{{::$ctrl.translations.DeadlineWillBeSelectedDateAt1159PmUtc}}."
                                tooltip-placement="top"
                                tooltip-trigger="mouseenter">
                            </i>
                        </label>
                        <div class="create-project-form__input-group">
                            <div class="deadlinePicker">
                                <one-datepicker
                                    class="datePicker"
                                    date-model="$ctrl.projectForm.deadline"
                                    is-required="false"
                                    min-date="$ctrl.projectForm.minDate"
                                    on-change="$ctrl.updateDeadline(value)"
                                    placeholder="{{::$ctrl.translations.EnterDeadlineDate}}"
                                ></one-datepicker>
                            </div>
                        </div>
                    </section>
                    <section class="row-vertical-center margin-bottom-2" ng-if="$ctrl.showMoreDetails && $ctrl.projectForm.deadline">
                        <label class="create-project-form__label text-right margin-right-2">{{::$ctrl.translations.Reminders}}</label>
                        <div class="create-project-form__input-group">
                            <div class="create-project-form__reminder">
                                <button
                                    class="create-project-form__reminder-btn margin-right-1 one-checkbox"
                                    type="button"
                                    uib-btn-checkbox
                                    btn-checkbox-true="true"
                                    btn-checkbox-false="false"
                                    ng-model="$ctrl.projectForm.hasReminder"
                                    ng-change="$ctrl.handleAction('TOGGLE_REMINDERS_CHECKBOX')">
                                    <i ng-if="$ctrl.projectForm.hasReminder" class="ot ot-check"></i>
                                </button>
                                <input
                                    class="create-project-form__reminder-input animate-if one-input block"
                                    ng-if="$ctrl.projectForm.hasReminder"
                                    type="number"
                                    ng-model="$ctrl.projectForm.reminderDays"
                                    min="1"
                                    ng-change="$ctrl.handleAction('REMINDER_DAYS_CHANGED')"/>
                                <span class="create-project-form__reminder-message" ng-if="$ctrl.projectForm.reminderDays">
                                    {{::$ctrl.translations.DaysBeforeDeadline}} &nbsp
                                    <i
                                        class="fa fa-info-circle deadlineTooltip"
                                        ng-if="$ctrl.projectForm.IsDefault"
                                        uib-tooltip="{{::$ctrl.translations.TheseAreDefaultReminderSettings}}"
                                        tooltip-placement="right"
                                        tooltip-trigger="mouseenter">
                                    </i>
                                </span>
                                <span class="create-project-form__reminder-message animate-if" ng-if="!$ctrl.projectForm.reminderDays">
                                    {{::$ctrl.translations.AddReminder}}
                                </span>
                            </div>
                            <p
                                class="text-error text-left text-small margin-top-half"
                                ng-if="$ctrl.projectForm.hasReminder && !$ctrl.projectForm.isValidReminder">
                                {{::$ctrl.translations.ReminderMustBeAtLeastOneDayInTheFuture}}
                            </p>
                        </div>
                    </section>
                    <section
                        class="row-vertical-center margin-bottom-2"
                        ng-class="{'has-error': $ctrl.projectForm.OrgGroupId === '' && (projectInfoForm.OrganizationGroup.$touched || projectInfoForm.OrganizationGroup.$dirty)}">
                        <label class="create-project-form__label text-right margin-right-2">
                            {{::$ctrl.translations.Organization}}
                            <sup class="fa fa-asterisk"></sup>
                        </label>
                        <div class="create-project-form__input-group">
                            <one-org-input
                                org-list="$ctrl.orgList"
                                selected-org="$ctrl.selectedOrg"
                                action-callback="$ctrl.handleAction(action, payload)"
                                placeholder="{{::$ctrl.translations.SelectOrganizationGroup}}">
                            </one-org-input>
                            <p
                                class="text-error text-left text-small margin-all-half"
                                ng-show="(projectInfoForm.OrganizationGroup.touched || projectInfoForm.OrganizationGroup.$dirty) && (projectInfoForm.OrganizationGroup.$error.required || $ctrl.projectForm.OrgGroupId === '')">
                                {{::$ctrl.translations.ErrorSelectProjectOrganizationGroup}}
                            </p>
                        </div>
                    </section>
                    <org-user-dropdown
                        ng-if="!$ctrl.projectForm.isMultiRespondent"
                        body-class="margin-0"
                        input-class="create-project-form__input-group"
                        is-required="!$ctrl.projectForm.isMultiRespondent"
                        label-class="create-project-form__label margin-right-2 justify-end"
                        label-inline="true"
                        on-select="$ctrl.respondentSelected(selection, isValid)"
                        org-id="$ctrl.orgId"
                        type="$ctrl.userDropdownType"
                        wrapper-class="row-vertical-center margin-bottom-1"
                    ></org-user-dropdown>
                    <section
                        class="row-vertical-center margin-bottom-2"
                        ng-if="$ctrl.projectForm.allowMultipleRespondent"
                    >
                        <label class="create-project-form__label text-right margin-right-2"></label>
                        <div class="create-project-form__input-group row-vertical-center">
                            <button
                                class="create-project-form__reminder-btn margin-right-1 one-checkbox"
                                type="button"
                                uib-btn-checkbox
                                btn-checkbox-true="true"
                                btn-checkbox-false="false"
                                ng-model="$ctrl.projectForm.isMultiRespondent"
                                ng-change="$ctrl.handleAction('TOGGLE_MULTI_RESPONDENTS_CHECKBOX')">
                                <i ng-if="$ctrl.projectForm.isMultiRespondent" class="ot ot-check"></i>
                            </button>
                            <p class="margin-top-bottom-0">{{::$ctrl.translations.AddMultipleRespondents}}</p>
                        </div>
                    </section>
                    <org-user-dropdown
                        ng-if="$ctrl.projectForm.isMultiRespondent"
                        ng-repeat="section in $ctrl.projectSource.Sections track by section.Id"
                        body-class="margin-0"
                        input-class="create-project-form__input-group"
                        is-required="$ctrl.projectForm.isMultiRespondent"
                        label-class="create-project-form__label margin-right-2 justify-end"
                        label-inline="true"
                        on-select="$ctrl.respondentSelected(selection, isValid, section.TemplateSectionId)"
                        org-id="$ctrl.orgId"
                        type="$ctrl.userDropdownType"
                        wrapper-class="row-vertical-center margin-bottom-half"
                    ></org-user-dropdown>
                </form>
                <footer class="create-project-form__footer text-center margin-top-bottom-3">
                    <one-button
                        button-click="$ctrl.cancel()"
                        text="{{::$ctrl.translations.Cancel}}">
                    </one-button>
                    <one-button
                        button-click="$ctrl.duplicateProject()"
                        class="margin-left-1"
                        enable-loading="true"
                        is-loading="$ctrl.isMakingRequest"
                        is-disabled="ng-invalid.$invalid || \
                                    !$ctrl.projectForm.isRespondentValid || \
                                    !$ctrl.projectForm.isValidReminder || \
                                    $ctrl.isMakingRequest"
                        text="{{::$ctrl.translations.Send}}"
                        type="primary">
                    </one-button>
                </footer>
            </div>
        </section>
    `,
};

export default createProjectForm;
