export const AssessmentQuestionNotSureButton = {
    bindings: {
        isDisabled: "<",
        isSelected: "<",
        buttonClass: "@?",
        onClick: "&",
    },
    template: `
        <one-button
            button-class="assessment-question__not-sure {{$ctrl.buttonClass}}"
            button-click="$ctrl.onClick()"
            is-selected="$ctrl.isSelected"
            is-disabled="$ctrl.isDisabled"
            text="{{::$root.t('NotSure')}}"
            type="select"
        ></one-button>
    `,
};

export const AssessmentQuestionNotApplicableButton = {
    bindings: {
        buttonClass: "@?",
        isDisabled: "<",
        isSelected: "<",
        onClick: "&",
    },
    template: `
        <one-button
            button-class="assessment-question__not-applicable {{$ctrl.buttonClass}}"
            button-click="$ctrl.onClick()"
            is-selected="$ctrl.isSelected"
            is-disabled="$ctrl.isDisabled"
            text="{{::$root.t('NotApplicable')}}"
            type="select"
        ></one-button>
    `,
};

export const AssessmentQuestionOtherOptionButton = {
    bindings: {
        buttonClass: "@?",
        isDisabled: "<",
        optionText: "<",
        onAfterSave: "&",
    },
    template: `
        <button
            class="one-button one-button--select {{$ctrl.buttonClass}}"
            e-placeholder="{{::$root.t('EnterYourOtherAnswerHere')}}"
            editable-text="$ctrl.optionText"
            ng-class="{'selected': $ctrl.optionText }"
            ng-disabled="$ctrl.isDisabled"
            onaftersave="$ctrl.onAfterSave({ value: $ctrl.optionText })"
            oncancel="">
            {{ $ctrl.optionText || $root.t('Other') }}
        </button>
    `,
};
