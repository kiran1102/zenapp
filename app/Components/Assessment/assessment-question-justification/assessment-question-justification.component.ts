import _ from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { AssessmentStates } from "constants/assessment.constants";

class AssessmentQuestionJustificationController {

    static $inject: string[] = ["$rootScope"];

    private question: IQuestion;
    private tempModel: IQuestion;
    private projectState: string;
    private onUpdate: (IAssessmentQuestionAction) => IAssessmentQuestionAction;
    private showLabel: boolean;
    private labelText: string;
    private placeholderText: string;
    private isEditable: boolean;

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) {}

    public $onInit(): void {
        this.showLabel = this.projectState === AssessmentStates.InProgress || Boolean(this.tempModel.Justification);
        this.labelText = this.projectState === AssessmentStates.InProgress ? this.$rootScope.t("JustifyYourAnswerBelow") : this.$rootScope.t("Justification");
        this.placeholderText = this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() });
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.question) {
            this.tempModel = this.question;
        }
    }

    private sendAction(type: string): void {
        if (_.isFunction(this.onUpdate)) {
            const actionType: IAssessmentQuestionAction = { type, value: this.tempModel.Justification };
            this.onUpdate({ actionType });
        }
    }
}

export default {
    bindings: {
        isDisabled: "<",
        isRequired: "<",
        projectState: "<",
        question: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionJustificationController,
    template: `
        <label
            class="question__justification-label"
            ng-if="$ctrl.showLabel">
            {{::$ctrl.labelText}}
            <sup class="fa fa-asterisk question__required" ng-if="$ctrl.isRequired"></sup>
        </label>
        <textarea
            class="question__justification-text resize-none"
            ng-change="$ctrl.sendAction('JUSTIFICATION_UPDATED')"
            ng-if="!$ctrl.isDisabled"
            ng-model="$ctrl.tempModel.Justification"
            ng-required="$ctrl.isRequired"
            placeholder="{{::$ctrl.placeholderText}}"
            rows="3">
        </textarea>
        <div
            class="question__justification-text--readonly padding-all-1"
            ng-if="$ctrl.isDisabled && $ctrl.tempModel.Justification">
            {{::$ctrl.question.Justification}}
        </div>
    `,
};
