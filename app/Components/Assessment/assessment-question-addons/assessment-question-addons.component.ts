import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

class AssessmentQuestionAddonsController {

    static $inject: string[] = ["$rootScope"];

    private question: IQuestion;
    private onButtonClick: Function;

    constructor(public readonly $rootScope: IExtendedRootScopeService) {}

    private buttonClicked(type: string): void {
        const questionAction: IAssessmentQuestionAction = { type, question: this.question };
        this.onButtonClick({ questionAction });
    }
}

export default {
    bindings: {
        viewState: "<",
        question: "<",
        onButtonClick: "&",
    },
    controller: AssessmentQuestionAddonsController,
    template: `
        <button
            class="assessment-question__addon--button border-none padding-none margin-right-2 no-outline"
            ng-click="$ctrl.buttonClicked('OPEN_COMMENTS_MODAL')"
            ng-if="::$ctrl.viewState.canViewQuestionComments">
            <span class="fa-stack">
                <i class="fa fa-stack-1x fa-comment comment-icon assessment-question__addon--button-icon"></i>
                <i class="fa fa-stack-1x notification" ng-if="$ctrl.question.HasComments"></i>
            </span>
            {{::$root.t("Comments")}}
        </button>
        <button
            class="assessment-question__addon--button border-none padding-none margin-right-2 no-outline"
            ng-click="$ctrl.buttonClicked('OPEN_ATTACHMENT_MODAL')"
            ng-if="::$ctrl.viewState.canViewQuestionAttachments">
            <span class="fa-stack">
                <i class="fa fa-stack-1x fa-file assessment-question__addon--button-icon"></i>
                <i class="fa fa-stack-1x notification" ng-if="$ctrl.question.HasAttachments"></i>
            </span>
            {{::$root.t("Attachments")}}
        </button>
        <button
            class="assessment-question__addon--button border-none padding-none margin-right-2 no-outline"
            ng-click="$ctrl.buttonClicked('OPEN_HISTORY_MODAL')"
            ng-if="::$ctrl.viewState.canViewQuestionHistory">
            <span class="fa-stack">
                <i class="fa fa-stack-1x fa-history assessment-question__addon--button-icon"></i>
            </span>
            {{::$root.t("History")}}
        </button>
    `,
};
