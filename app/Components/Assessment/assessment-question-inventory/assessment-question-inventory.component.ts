import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionTypes, QuestionResponseTypes, QuestionInventoryTableIds } from "enums/assessment-question.enum";

class AssessmentQuestionInventoryController {

    static $inject: string[] = ["$rootScope", "Permissions"];

    private onUpdate: ({}) => void;

    private inputPlaceholders: any;
    private QuestionTypes: any;
    private QuestionResponseTypes: any;
    private QuestionInventoryTableIds: any;

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.QuestionTypes = QuestionTypes;
        this.QuestionResponseTypes = QuestionResponseTypes;
        this.QuestionInventoryTableIds = QuestionInventoryTableIds;

        this.inputPlaceholders = {};
        this.inputPlaceholders[QuestionTypes.Application] = this.$rootScope.t("EnterAnApplication");
        this.inputPlaceholders[QuestionTypes.User] = this.$rootScope.t("EnterAUser");
        this.inputPlaceholders[QuestionTypes.Location] = this.$rootScope.t("EnterALocation");
        this.inputPlaceholders[QuestionTypes.Provider] = this.$rootScope.t("EnterAProvider");
    }

    private sendAction(type: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        handleInventorySelect: "<",
        isDisabled: "<",
        question: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionInventoryController,
    template: `
        <form
            class="question__inventory-form"
            ng-submit="$ctrl.handleInventorySelect($ctrl.question.SectionId, $ctrl.question, $ctrl.question.Answer)">
            <input
                class="question__inventory-input one-input"
                placeholder="{{::$ctrl.inputPlaceholders[$ctrl.question.Type]}}"
                title="{{::$ctrl.inputPlaceholders[$ctrl.question.Type]}}"
                type="text"
                ng-model="$ctrl.question.Answer"
                ng-class="{'has-errors': $ctrl.question.inventoryError}"
                uib-typeahead="option as option.Name for option in $ctrl.question.Options | orderBy:'Name' | TypeaheadInventoryFilter:$viewValue:$ctrl.question.AllowOther:$ctrl.question.Type"
                typeahead-editable="false"
                typeahead-on-select="$ctrl.handleInventorySelect($ctrl.question.SectionId, $ctrl.question, $model)"
                typeahead-min-length="0"
                typeahead-focus-on-select="false"
                typeahead-template-url="inventoryItemTemplate.html"/>
            <p
                class="question__inventory-error"
                ng-if="$ctrl.question.inventoryError"
                ng-bind="$ctrl.question.inventoryError">
            </p>
        </form>

        <assessment-question-not-sure-button
            button-class="margin-right-1 margin-top-bottom-1"
            class="option-container"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
            on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
            ng-if="$ctrl.question.AllowNotSure">
        </assessment-question-not-sure-button>

        <assessment-question-not-applicable-button
            button-class="margin-right-1 margin-top-bottom-1"
            class="option-container"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
            on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
            ng-if="$ctrl.question.AllowNotApplicable">
        </assessment-question-not-applicable-button>

        <inventory-card
            ng-if="$ctrl.question.Type === $ctrl.QuestionTypes.Application && $ctrl.question.Answer && $ctrl.question.Answer.Value"
            table-id="::$ctrl.QuestionInventoryTableIds.Applications"
            inventory-id="$ctrl.question.Answer.Value"
            inventory-name="{{$ctrl.question.Answer.Name || $root.t('Application')}}"
            inventory-name-prefix="{{::$root.t('Details')}} /">
        </inventory-card>
    `,
};
