import * as _ from "lodash";

class DMAssessmentTableCell {
    static $inject: string[] = ["$state", "ENUMS"];

    private cellTypes: any;
    private cell: any;
    private assessment: any;

    constructor(
        readonly $state: ng.ui.IStateService,
        ENUMS: any,
    ) {
        this.cellTypes = ENUMS.TableInputTypes;
    }

    public goToLink(data, cell): void {
        const routeParams = {};
        if (_.isArray(this.cell.linkParams) && this.cell.linkParams.length) {
            _.forEach(this.cell.linkParams, (param: any): void => {
                routeParams[param.routeKey] = this.assessment[param.dataKey];
            });
        }
        this.$state.go(this.cell.link, routeParams);
    }
}

const dmAssessmentTableCellComponent = {
    bindings: {
        cell: "<",
        assessment: "<",
        disabled: "<",
    },
    controller: DMAssessmentTableCell,
    template: `
        <div
            class="dm-assessment-table-cell"
            >
            <span
                class="dm-assessment-table-cell__text overflow-ellipsis dm-assessment-table-cell__text--link text-link"
                ng-class="::$ctrl.cell.classes ? $ctrl.cell.classes : ''"
                ng-if="$ctrl.cell.type === $ctrl.cellTypes.Link && !$ctrl.disabled"
                ng-click="$ctrl.goToLink()"
                uib-tooltip="{{$ctrl.cell.getLabel($ctrl.assessment)}}"
                tooltip-append-to-body="true"
                >
                {{$ctrl.cell.getLabel($ctrl.assessment)}}
            </span>
            <span
                class="dm-assessment-table-cell__text overflow-ellipsis"
                ng-if="$ctrl.cell.type === $ctrl.cellTypes.Text || ($ctrl.cell.type === $ctrl.cellTypes.Link && $ctrl.disabled)"
                >
                {{$ctrl.cell.getLabel($ctrl.assessment)}}
            </span>
            <span
                class="dm-assessment-table-cell__text overflow-ellipsis text-bold {{$ctrl.cell.getColor($ctrl.assessment)}}"
                ng-if="$ctrl.cell.type === $ctrl.cellTypes.ProjectState"
                >
                {{$ctrl.cell.getLabel($ctrl.assessment)}}
            </span>
            <one-date
                ng-if="$ctrl.cell.type === $ctrl.cellTypes.Date"
                class="dm-assessment-table-cell__text overflow-ellipsis"
                date-to-format="$ctrl.cell.getLabel($ctrl.assessment)"
                enable-tooltip="true"
                >
            </one-date>
            <span
                class="dm-assessment-table-cell__text overflow-ellipsis"
                ng-if="$ctrl.cell.type === $ctrl.cellTypes.Actions"
                >
                <dm-assessment-list-actions
                    row-id="{{$ctrl.assessment.assessmentId}}"
                    assessment-status="$ctrl.assessment.statusId"
                    >
                </dm-assessment-list-actions>
            </span>
        </div>
    `,
};

export default dmAssessmentTableCellComponent;
