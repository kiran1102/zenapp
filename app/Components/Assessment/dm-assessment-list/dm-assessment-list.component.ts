// 3rd Party
import * as _ from "lodash";
import Utilities from "Utilities";
import { IScope } from "angular";

// Services
import {
    IAssessmentParams,
    IAssessmentFilters,
} from "app/scripts/DataMapping/Services/Assessment/dm-assessments.service";

// Enums
import { ExportTypes } from "enums/export-types.enum";
import { DMAssessmentStates } from "enums/assessment.enum.ts";
import { KeyCodes } from "enums/key-codes.enum";
import { AssesmentTemplateTypes } from "enums/assesment-template-types.enum.ts";

// Interfaces
import { IActionIcon } from "interfaces/action-icons.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IDMAssessmentListResponse,
    IDMStateFilters,
} from "interfaces/dm-assessment.interface";
import {
    ILabelValue,
    IStringMap,
} from "interfaces/generic.interface";

class DmAssessmentList {
    static $inject: string[] = [
        "$scope",
        "$rootScope",
        "$state",
        "OneAssessmentList",
        "Permissions",
    ];
    public ready = false;

    private stateFilters: IDMStateFilters;
    private translate: (key: string) => string;
    private templateType: Array<ILabelValue<number>>;
    private selectedState = 0;
    private iconActions: IActionIcon[];
    private openFilterOptions = false;
    private assessmentList: IDMAssessmentListResponse;
    private canAddAssets = false;
    private AssessmentLoadingText: IStringMap<string> = {
        loading: "Loading",
        sorting: "Sorting",
        filtering: "Filtering",
    };

    private showingFilteredResults = false;
    private loadingPrefix = this.AssessmentLoadingText.loading;
    private defaultAssessmentParams: IAssessmentParams = {
        page: 1,
        size: 10,
        sort: "assessmentRefId,DESC",
        filter: undefined, // { types:[], statuses:[], templates:[], searchTerm },
    };

    private params: IAssessmentParams =  this.getFilterParams();
    private filters: { selectedState?: number, templateType?: number, templates?: string, searchTerm?: string };

    constructor(readonly $scope: IScope, $rootScope: IExtendedRootScopeService, readonly $state: ng.ui.IStateService, readonly OneAssessmentList: any, Permissions: any) {
        this.canAddAssets = Permissions.canShow("DataMappingInventoryAssetsAdd");
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.stateFilters = this.getFilters();
        this.templateType = this.getTemplate();
        this.iconActions = this.setIconActions();
        this.init();
    }

    public goToAssessment(id: string): void {
        this.$state.go("zen.app.pia.module.datamap.views.assessment.single", { Id: id });
    }

    public handleSearchKeyPress = (event: JQueryKeyEventObject): void => {
        if (event.keyCode === KeyCodes.Enter) {
            this.$scope.$emit("FILTER_ASSESSMENT", this.filters.templateType, this.filters.selectedState, this.filters.templates, this.filters.searchTerm);
        }
    }

    public applyFilters(templateType: number, state: number): void {
        this.$scope.$emit("FILTER_ASSESSMENT", templateType, state, this.filters.templates, this.filters.searchTerm);
    }

    public clearFilters(): void {
        this.filters.selectedState = 0;
        this.filters.templateType = 0;
        this.applyFilters(this.filters.templateType, this.filters.selectedState);
    }

    private setIconActions(): IActionIcon[] {
        const iconActions: IActionIcon[] = [];
        if (this.showingFilteredResults) {
            iconActions.push({
                handleClick: this.toggleFilters,
                iconClass: "ot-filter color-white background-blue padding-all-1 border-round",
            });
        } else if (!this.showingFilteredResults) {
            iconActions.push({
                handleClick: this.toggleFilters,
                iconClass: "ot-filter",
            });
        }
        return iconActions;
    }

    private init(softReload: boolean = false): ng.IPromise<any> {
        this.ready = false;
        this.filters = {
            templateType: this.params.filter && this.params.filter.types ? this.params.filter.types[0] : 0,
            selectedState: this.params.filter && this.params.filter.statuses ? this.params.filter.statuses[0] : 0,
            searchTerm: this.params.filter && this.params.filter.name ? this.params.filter.name : "",
        };

        this.params.sort = this.normalizeSortKey(this.params.sort);
        return this.OneAssessmentList.init(this.params).then((res: any): undefined|void => {
            if (!res.result) {
                this.ready = true;
            } else if (res.data.number > 0 && !res.data.content.length) {
                this.softInit({page: 1});
            } else {
                this.assessmentList = res.data;
                if (!softReload) this.startEventWatchers();
                this.ready = true;
            }
        });
    }

    private softInit(newParams: IAssessmentParams = this.params): void {
        newParams = _.assign(this.params, newParams);
        const params: any = _.cloneDeep(newParams);
        params.filter = JSON.stringify(newParams.filter);
        params.sort = this.normalizeSortKey(this.params.sort, true);
        this.$state.go(".", params, {notify: false, reload: false}).then((): void => {
            this.init(true);
        });
    }

    private getFilterParams(): IAssessmentParams {
        const params: IAssessmentParams =   {
            page: parseInt(this.$state.params.page, 10) || this.defaultAssessmentParams.page,
            size: parseInt(this.$state.params.size, 10) || this.defaultAssessmentParams.size,
            sort: this.$state.params.sort || this.defaultAssessmentParams.sort,
            filter: undefined,
        };
        const cachedFilters = sessionStorage.getItem("DMAssesmentListFilterSettings");
        if (this.$state.params.filter ) {
            params.filter = JSON.parse(this.$state.params.filter);
            this.showingFilteredResults = true;
        } else if (cachedFilters) {
            params.filter = JSON.parse(sessionStorage.getItem("DMAssesmentListFilterSettings"));
            this.showingFilteredResults = true;
        }
        return params;
    }

    private startEventWatchers(): void {
        this.$scope.$on("CHANGE_PAGE", (event: ng.IAngularEvent, page: number = this.params.page, size: number = this.params.size): void => {
            this.loadingPrefix = this.AssessmentLoadingText.loading;
            this.softInit({ page: ++page, size });
        });
        this.$scope.$on("SORT_ASSESSMENT", (event: ng.IAngularEvent, sort: string = this.params.sort): void => {
            this.loadingPrefix = this.AssessmentLoadingText.sorting;
            this.softInit({ sort });
        });
        this.$scope.$on("FILTER_ASSESSMENT", (event: ng.IAngularEvent, type: number, status: number, template: string, searchTerm: string): void => {
            this.loadingPrefix = this.AssessmentLoadingText.filtering;
            let filter: IAssessmentFilters = {};
            if (!type && !status && !template && !searchTerm ) {
                filter = undefined;
                sessionStorage.removeItem("DMAssesmentListFilterSettings");
                this.showingFilteredResults = false;
            } else {
                if (type) filter.types = [type];
                if (status) filter.statuses = [status];
                if (template) filter.templates = [template];
                if (searchTerm) filter.name = searchTerm;
                sessionStorage.setItem("DMAssesmentListFilterSettings", JSON.stringify(filter));
                this.showingFilteredResults = true;

            }
            this.iconActions = this.setIconActions();
            this.softInit({ filter });
        });
        this.$scope.$on("DELETE_ASSESSMENT", (event: ng.IAngularEvent, assessmentId: string): void => {
            this.OneAssessmentList.deleteAssessment(assessmentId).then((res: any): void => {
                if (!res.result) {
                    const assessmentIndex: number = _.findIndex(this.assessmentList.content, ["assessmentId", assessmentId]);
                    if (assessmentIndex > -1) this.assessmentList.content[assessmentIndex].isDisabled = false;
                } else {
                    const page: number = !this.params.page || this.assessmentList.numberOfElements > 1 ? this.params.page : this.params.page - 1;
                    this.$scope.$emit("CHANGE_PAGE", page - 1);
                }
            });
        });
    }

    private toggleFilters = (): void => {
        this.openFilterOptions = !this.openFilterOptions;
    }

    private getFilters(): Array<ILabelValue<number>> {
        return [
            {
                label: this.translate("AllAssessments"),
                value: DMAssessmentStates.All,
            },
            {
                label: this.translate("NotStarted"),
                value: DMAssessmentStates.NotStarted,
            },
            {
                label: this.translate("InProgress"),
                value: DMAssessmentStates.InProgress,
            },
            {
                label: this.translate("UnderReview"),
                value: DMAssessmentStates.UnderReview,
            },
            {
                label: this.translate("Completed"),
                value: DMAssessmentStates.Completed,
            },
            {
                label: this.translate("NeedsAttention"),
                value: DMAssessmentStates.NeedsAttention,
            },
        ];
    }

    private getTemplate(): Array<ILabelValue<number>> {
        return [
          {
             label: this.translate("AllTemplates"),
             value: 0,
          },
          {
            label: this.translate("Asset"),
            value: AssesmentTemplateTypes.Asset,
          },
          {
            label: this.translate("ProcessingActivity"),
            value: AssesmentTemplateTypes.ProcessingActivity,
          },
        ];
      }

    private normalizeSortKey(sort: string, forUrl?: boolean): string {
        const sortArray: string[] = sort.split(",");
        if (!sortArray.length || sortArray.length < 1) {
            return sort;
        } else if (forUrl && sortArray[0] === "assessmentRefId") {
            sortArray[0] = "id";
        } else if (!forUrl && sortArray[0] === "id") {
            sortArray[0] = "assessmentRefId";
        }
        return sortArray.join(",");
    }

    private handleSearch(searchTerm: string): void {
        this.filters.searchTerm = searchTerm;
        this.$scope.$emit("FILTER_ASSESSMENT", this.filters.templateType, this.filters.selectedState, this.filters.templates, this.filters.searchTerm);
    }
}

const dmAssessmentListComponent: ng.IComponentOptions = {
    controller: DmAssessmentList,
    template: `
        <section class="dm-assessment-list flex-full-height" hide-shadow="$ctrl.openFilterOptions">
            <dm-assessments-header
                class="dm-assessment-list__header"
                filters="$ctrl.filters"
                >
            </dm-assessments-header>
            <div class="dm-assessments-list__sub-header background-grey border-bottom full-width padding-top-bottom-half padding-left-right-2">
                <div class="dm-assessments-list__sub-header--actions full-width row-nowrap">
                    <one-search-input
                        class="margin-left-auto"
                        input-class="block"
                        search-class="filter-item__search"
                        name="AssessmentSearch"
                        id="AssessmentSearch"
                        model="$ctrl.filters.searchTerm"
                        search-class="inventory-list__search pull-right"
                        placeholder="{{::$root.t('SearchByName')}}"
                        on-search="$ctrl.handleSearch(model, clear)"
                        on-keypress="$ctrl.handleSearchKeyPress(event)"
                        throttle="250"
                    ></one-search-input>
                    <action-icons
                        class="dm-assessments-filter row-flex-start"
                        actions="$ctrl.iconActions"
                        >
                    </action-icons>
                </div>
            </div>
            <span
                class="dm-assessments-filter shadow-header row-flex-start overflow-y-auto static-vertical"
                ng-if="$ctrl.openFilterOptions"
                >
                <div class="dm-assessments-list__status margin-left-2 margin-top-1">
                    <label>{{::$root.t('Status')}}</label>
                    <one-select
                        class="dm-assessments-list__group margin-left-1"
                        options="$ctrl.stateFilters"
                        model="$ctrl.filters.selectedState"
                        on-change="$ctrl.applyFilters($ctrl.filters.templateType, selection)"
                        label-key="label"
                        value-key="value"
                        select-class="dm-assessments-list_select"
                        >
                    </one-select>
                </div>
                <div class="dm-assessments-list__status margin-left-2 margin-top-1">
                    <label>{{::$root.t('AllTemplates')}}</label>
                    <one-select
                        class="dm-assessments-list__group margin-left-1"
                        options="$ctrl.templateType"
                        model="$ctrl.filters.templateType"
                        on-change="$ctrl.applyFilters(selection, $ctrl.filters.selectedState)"
                        label-key="label"
                        value-key="value"
                        select-class="dm-assessments-list__select"
                        >
                    </one-select>
                </div>
                <div class="dm-assessments-header__clearFilter margin-top-1">
                    <label> </label>
                    <one-button
                        class="dm-assessments-list__group margin-left-2"
                        button-click="$ctrl.clearFilters()"
                        icon='ot ot-times fa-fw'
                        text="{{::$root.t('ClearFilters')}}"
                        identifier="assessmentHeaderClearFilters"
                        >
                    </one-button>
                </div>
            </span>
            <section
                class="stretch-vertical flex-full-height column-nowrap"
                >
                <loading
                    class="dm-assessment-list__wrapper padding-all-1 flex-full-height"
                    ng-if="!$ctrl.ready"
                    loading-class="flex-full-height column-horizontal-vertical-center"
                    show-loading-prefix="false"
                    text="{{$root.t($ctrl.loadingPrefix)+' '+$root.t('Assessments')}}"
                    >
                </loading>
                <div
                    ng-if="$ctrl.ready"
                    class="dm-assessment-list__wrapper padding-all-1 flex-full-height column-nowrap"
                    >
                    <dm-assessment-table
                        ng-if="$ctrl.assessmentList.content.length"
                        class="dm-assessment-list__table flex-full-height"
                        table="$ctrl.assessmentList"
                        filter-open="$ctrl.openFilterOptions"
                        >
                    </dm-assessment-table>
                    <span
                        class="dm-assessment-list__empty flex-full-height column-horizontal-vertical-center"
                        ng-if="($ctrl.assessmentList.numberOfElements === 0) && $ctrl.canAddAssets"
                        >
                        {{::$root.t('NoExistingAssessmentsCanAdd')}}
                    </span>
                    <span
                        class="dm-assessment-list__empty flex-full-height column-horizontal-vertical-center"
                        ng-if="($ctrl.assessmentList.numberOfElements === 0) && !$ctrl.canAddAssets"
                        >
                        {{::$root.t('NoExistingAssessmentsNoAdd')}}
                    </span>
                </div>
                <one-pagination
                    class="static-vertical margin-top-bottom-2 margin-left-right-1"
                    ng-if="$ctrl.assessmentList.totalPages > 1"
                    list="$ctrl.assessmentList"
                    >
                </one-pagination>
            </section>
        </section>
    `,
};

export default dmAssessmentListComponent;
