import { isUndefined } from "lodash";

import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionResponseTypes } from "enums/assessment-question.enum";

import * as ACTIONS from "../assessment-actions.constants";

class AssessmentQuestionTextboxController {

    private isDisabled: boolean;
    private question: IQuestion;
    private onUpdate: ({}) => void;

    private QuestionResponseTypes: any;

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
    }

    private sendAction(type: string, value?: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        if (type === ACTIONS.TEXTBOX_UPDATED) {
            actionType.value = isUndefined(value) ? "" : value;
        }
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        isDisabled: "<",
        question: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionTextboxController,
    template: `
        <textarea
            class="form-control resize-none"
            ng-change="$ctrl.sendAction('TEXTBOX_UPDATED', $ctrl.question.Answer)"
            ng-if="!$ctrl.isDisabled"
            ng-model="$ctrl.question.Answer"
            ng-required="$ctrl.question.Required"
            placeholder="{{::$root.t('EnterYourAnswerHere')}}"
            rows="3"
        ></textarea>
        <div
            class="question__content question__content--readonly padding-all-1"
            ng-if="$ctrl.isDisabled">
            {{::$ctrl.question.Answer }}
        </div>
        <assessment-question-not-sure-button
            button-class="margin-right-1 margin-top-bottom-1"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
            on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
            ng-if="$ctrl.question.AllowNotSure"
        ></assessment-question-not-sure-button>
        <assessment-question-not-applicable-button
            button-class="margin-right-1 margin-top-bottom-1"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
            on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
            ng-if="$ctrl.question.AllowNotApplicable"
        ></assessment-question-not-applicable-button>
    `,
};
