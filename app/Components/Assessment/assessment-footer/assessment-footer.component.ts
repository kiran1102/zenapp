import { find, findIndex } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class AssessmentFooterController implements ng.IComponentController {

    static $inject: string[] = ["$scope", "ENUMS", "$rootScope", "Permissions", "OneAssessment"];

    private translate: any;
    private assessmentState: any;
    private AssessmentStates: any;
    private canSubmit = false;
    private canApprove = false;
    private canSendBack = false;
    private actionText: any;
    private readyForApproval: boolean;
    private submitInProgress = false;
    private waitingForResponse = false;
    private activeSection: any;
    private sections: any;
    private previousSectionDisable: boolean;
    private nextSectionDisable: boolean;
    private previousSectionLoading: boolean;
    private nextSectionLoading: boolean;

    constructor(
        readonly $scope: any,
        readonly ENUMS: any,
        readonly $rootScope: IExtendedRootScopeService,
        private Permissions: any,
        readonly OneAssessment: any) {
        this.AssessmentStates = ENUMS.DMAssessmentStates;
        this.translate = $rootScope.t;
    }

    public $onInit(): void {
        this.assessmentState = this.OneAssessment.getStatus();
        this.startListeners();
        this.sections = this.OneAssessment.getSections();
        this.updateStatus();
        this.$scope.$on("STATUS_UPDATED", (): void => {
            this.updateStatus();
        });
    }

    public updateStatus(): void {
        this.assessmentState = this.OneAssessment.getStatus();
        const isUnderReview: boolean = this.assessmentState === this.AssessmentStates.UnderReview;
        const isCompleted: boolean = this.assessmentState === this.AssessmentStates.Completed;
        this.canSubmit = this.Permissions.canShow("DataMappingAssessmentsRespond") && !(isUnderReview || isCompleted);
        this.canApprove = this.Permissions.canShow("DataMappingAssessmentsApprove") && isUnderReview;
        this.canSendBack = this.Permissions.canShow("DataMappingAssessmentsSendBack") && isUnderReview;
        this.actionText = this.translate(isUnderReview ? "Approve" : "Submit");
        this.readyForApproval = this.OneAssessment.getIsReadyForApproval();
        this.handleSectionChanged();
    }

    public sendBackAssessment(): void {
        this.$scope.$emit("LAUNCH_SEND_BACK");
    }

    public submitAssessment(): void {
        this.submitInProgress = true;
        if (!this.waitingForResponse) {
            this.$scope.$emit("SUBMIT_ASSESSMENT");
        }
    }

    public setSectionSequenceId(sequenceId: number): void | undefined {
        const newSection: any = find(this.sections, (section: any): boolean => section.sectionSequenceId === sequenceId);
        if (!newSection || !newSection.id) return;
        this.$scope.$emit("GO_TO_SECTION", newSection.id);
    }

    public previousSection(): void {
        this.previousSectionLoading = true;
        const currentSequenceId: number = findIndex(this.sections, { sectionSequenceId: this.activeSection.sectionSequenceId });
        const previousSectionSequenceId: number = this.sections[currentSequenceId - 1].sectionSequenceId;
        this.setSectionSequenceId(previousSectionSequenceId);
    }

    public nextSection(): void {
        this.nextSectionLoading = true;
        const currentSequenceId: number = findIndex(this.sections, { sectionSequenceId: this.activeSection.sectionSequenceId });
        const nextSectionSequenceId: number = this.sections[currentSequenceId + 1].sectionSequenceId;
        this.setSectionSequenceId(nextSectionSequenceId);
    }

    public handleSectionChanged(): void {
        this.previousSectionLoading = false;
        this.nextSectionLoading = false;
        this.activeSection = this.OneAssessment.getActiveSection();
        this.nextSectionDisable = this.activeSection.sectionSequenceId === this.sections[this.sections.length - 1].sectionSequenceId;
        this.previousSectionDisable = this.activeSection.sectionSequenceId === this.sections[0].sectionSequenceId;
    }

    private startListeners(): void {
        this.$scope.$on("ANSWER_STARTED", (): void => {
            this.readyForApproval = false;
            this.waitingForResponse = true;
        });
        this.$scope.$on("ANSWER_CHANGED", (): void => {
            this.readyForApproval = this.waitingForResponse ? this.readyForApproval : this.OneAssessment.getIsReadyForApproval();
        });
        this.$scope.$on("ANSWER_CHANGE_FINISHED", (): void => {
            this.readyForApproval = this.OneAssessment.getIsReadyForApproval();
            this.waitingForResponse = false;
        });
        this.$scope.$on("SECTION_CHANGED", (): void => {
            this.handleSectionChanged();
        });
        this.$scope.$on("APPROVE_CANCELLED", (): void => {
            this.waitingForResponse = false;
            this.submitInProgress = false;
        });
    }
}

const assessmentFooterComponent: ng.IComponentOptions = {
    controller: AssessmentFooterController,
    template: `
        <one-popup-footer
            visible="true"
            >
            <left-actions ng-if="$ctrl.canSendBack">
                <one-button
                   class="align-left"
                   type="primary"
                   text="{{::$ctrl.translate('SendBack')}}"
                   button-click="$ctrl.sendBackAssessment()"
                   icon="fa fa-angle-left"
                   >
               </one-button>
               <i
                   class="one-popup-footer__left-actions--icon fa fa-fw fa-info-circle inline-block align-left margin-left-1 padding-right-1"
                   uib-tooltip="{{::$ctrl.translate('SendBackToRespondent')}}"
                   >
               </i>
               <span class="one-popup-footer__left-actions--divider align-left"></span>
            </left-actions>
            <right-actions>
                <one-button
                    class="one-popup-footer__left-actions--section-item align-left"
                    type="default"
                    text="{{::$ctrl.translate('PreviousSection')}}"
                    button-click="$ctrl.previousSection()"
                    is-disabled="$ctrl.previousSectionDisable || $ctrl.previousSectionLoading"
                    icon="fa fa-angle-left"
                    identifier="previousSection"
                    enable-loading="true"
                    is-loading="$ctrl.previousSectionLoading"
                    >
                </one-button>
                <i
                    class="one-popup-footer__left-actions--section-icon one-popup-footer__left-actions--circular-btn fa inline-block align-left"
                    ng-class="{
                        'fa-spinner fa-pulse fa-fw one-popup-footer__loading-icon': $ctrl.previousSectionLoading,
                        'fa-angle-left': !$ctrl.previousSectionLoading
                    }"
                    uib-tooltip="{{::$ctrl.translate('PreviousSection')}}"
                    ng-click="$ctrl.previousSection()"
                    ng-disabled="$ctrl.previousSectionDisable || $ctrl.previousSectionLoading"
                    >
                </i>
                <one-button
                    class="one-popup-footer__left-actions--section-item align-left margin-left-1"
                    type="default"
                    text="{{::$ctrl.translate('NextSection')}}"
                    button-click="$ctrl.nextSection()"
                    is-disabled="$ctrl.nextSectionDisable || $ctrl.nextSectionLoading"
                    right-side-icon="fa fa-angle-right"
                    identifier="nextSection"
                    enable-loading="true"
                    is-loading="$ctrl.nextSectionLoading"
                    >
                </one-button>
                <i
                    class="one-popup-footer__left-actions--section-icon one-popup-footer__left-actions--circular-btn fa inline-block align-left"
                    ng-class="{
                        'fa-spinner fa-pulse fa-fw one-popup-footer__loading-icon': $ctrl.nextSectionLoading,
                        'fa-angle-right': !$ctrl.nextSectionLoading
                    }"
                    uib-tooltip="{{::$ctrl.translate('NextSection')}}"
                    ng-click="$ctrl.nextSection()"
                    ng-disabled="$ctrl.nextSectionDisable || $ctrl.nextSectionLoading"
                    >
                </i>
                <one-button
                    ng-if="$ctrl.canSubmit || $ctrl.canApprove"
                    class="align-right margin-left-1 padding-right-1"
                    type="primary"
                    text="{{$ctrl.actionText}}"
                    button-click="$ctrl.submitAssessment()"
                    is-disabled="$ctrl.submitInProgress || !$ctrl.readyForApproval || $ctrl.waitingForResponse"
                    enable-loading="true"
                    is-loading="$ctrl.submitInProgress || $ctrl.waitingForResponse"
                    >
                </one-button>
            </right-actions>
        </one-popup-footer>
    `,
};

export default assessmentFooterComponent;
