import {
    find,
    map,
    isArray,
    isEmpty,
} from "lodash";
import * as ENUMS from "enums/assessment-question.enum.ts";
import * as ACTIONS from "./assessment-actions.constants";

import {
    IQuestion,
    IAssessmentDataElement,
    IQuestionResponse,
} from "interfaces/question.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

export default class AssessmentQuestionValidationService {

    static $inject: string[] = ["$rootScope"];

    constructor( readonly $rootScope: IExtendedRootScopeService ) {}

    public isQuestionAnswered(question: IQuestion): boolean {
        const type: ENUMS.QuestionResponseTypes = question.ResponseType;
        return question.Answer ||
            type === ENUMS.QuestionResponseTypes.NotSure ||
            type === ENUMS.QuestionResponseTypes.NotApplicable ||
            question.Answer === false;
    }

    public validateYesNo = (question: IQuestion, value: boolean | null): IQuestion => {
        const isAnswered: boolean = this.isQuestionAnswered(question);
        return {
            ...question,
            Answer: value,
            isAnswered,
            NotApplicable: false,
            OtherAnswer: null,
            ResponseType: ENUMS.QuestionResponseTypes.Value,
        };
    }

    public validateDate = (question: IQuestion, value: string): IQuestion => {
        const isAnswered = this.isQuestionAnswered(question);
        return {
            ...question,
            Answer: value,
            isAnswered,
            NotApplicable: false,
            OtherAnswer: null,
            ResponseType: ENUMS.QuestionResponseTypes.Value,
        };
    }

    public toggleNotSure = (question: IQuestion): IQuestion => {
        return {
            ...question,
            Answer: null,
            isAnswered: this.isQuestionAnswered(question),
            NotApplicable: false,
            OtherAnswer: null,
            ResponseType: question.ResponseType === ENUMS.QuestionResponseTypes.NotSure ? null : ENUMS.QuestionResponseTypes.NotSure,
        };
    }

    public toggleNotApplicable = (question: IQuestion): IQuestion => {
        return {
            ...question,
            Answer: null,
            isAnswered: this.isQuestionAnswered(question),
            NotApplicable: !question.NotApplicable,
            OtherAnswer: null,
            ResponseType: question.ResponseType === ENUMS.QuestionResponseTypes.NotApplicable ? null : ENUMS.QuestionResponseTypes.NotApplicable,
        };
    }

    public updateResponseToMatchAnswer = (answer: any, responses: IQuestionResponse[], options: IAssessmentDataElement[]): IQuestionResponse[] => {
        let newResponses: IQuestionResponse[];
        let responseValuesArray: string[] = map(responses, (response: IQuestionResponse): string => {
            return response.Value;
        });

        if (answer !== responseValuesArray) {
            responseValuesArray = answer;
            newResponses = map(responseValuesArray, (value: string): IQuestionResponse => {
                const matchingOption = find(options, (opt: IAssessmentDataElement): boolean => opt.Value === value);
                if (matchingOption) {
                    return {
                        Type: matchingOption.Type,
                        Value: matchingOption.Value,
                    };
                }
            });
        }
        return !isEmpty(newResponses) ? newResponses : responses;
    }

    public validateMultiselect = (question: IQuestion, option: IAssessmentDataElement): IQuestion => {
        const { Answer, Responses, Options } = this.updateMultiselectAnswers(question, option);

        return {
            ...question,
            NotApplicable: false,
            ResponseType: question.DefaultResponseType,  // is this correct logic?
            Answer,
            Responses: this.updateResponseToMatchAnswer(Answer, Responses, Options),
            isAnswered: Boolean(Answer) || Answer === false,
        };
    }

    public updateMultiselectAnswers = (question: IQuestion, option: IAssessmentDataElement): IQuestion => {
        if (isArray(question.Answer) && question.Answer.length > 0) {
            const valueIndex: number = question.Answer.indexOf(option.Value);
            if (valueIndex < 0) {
                question.Answer.push(option.Value);
            } else {
                question.Answer.splice(valueIndex, 1);
            }
        } else {
            question.Answer = [option.Value];
        }

        return question;
    }

    public validateJustification = (question: IQuestion, value: string): IQuestion => {
        return {
            ...question,
            Justification: value,
        };
    }

    public validateTextbox = (question: IQuestion, value: string): IQuestion => {
        if (value) {
            return {
                ...question,
                Answer: value,
                ResponseType: ENUMS.QuestionResponseTypes.Value,
                isAnswered: true,
                NotApplicable: false,
                OtherAnswer: null,
            };
        } else {
            return {
                ...question,
                Answer: "",
                isAnswered: false,
            };
        }
    }

    public validateOtherOption = (question: IQuestion, value: string): IQuestion => {
        const nullifyAnswer: boolean = !question.MultiSelect && question.Type === ENUMS.QuestionTypes.Multichoice;
        return {
            ...question,
            ResponseType: value ? ENUMS.QuestionResponseTypes.Other : question.DefaultResponseType,
            OtherAnswer: value || "",
            Answer: nullifyAnswer ? null : question.Answer,
        };
    }

    public validateMultichoice = (question: IQuestion, value: any, name: string): IQuestion => {
        if (question.OptionListTypeId === 20) {
            return {
                ...question,
                ResponseType: ENUMS.QuestionResponseTypes.Reference,
                Answer: question.Answer === value.toString() ? "" : value,
                AnswerName: name,
                OtherAnswer: undefined,
            };
        } else {
            return {
                ...question,
                ResponseType: ENUMS.QuestionResponseTypes.Reference,
                Answer: question.Answer === value.toString() ? "" : value,
                OtherAnswer: undefined,
            };
        }
    }
}
