import AssessmentMenuViewLogic from "generalcomponent/Assessment/assessment-menu-view-logic.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { IUser } from "interfaces/user.interface";
import { IChangesObject } from "angular";

class AssessmentMenuCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "AssessmentMenuViewLogic"];

    private currentUser: IUser;
    private project: IPiaAssessment;

    private viewState: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly AssessmentMenuViewLogic: AssessmentMenuViewLogic,
    ) { }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.phases || changes.project) {
            this.viewState = this.AssessmentMenuViewLogic.calculateNewViewState(this.currentUser, this.project);
        }
    }
}

export default {
    bindings: {
        currentUser: "<",
        project: "<",
        phases: "<",
        goToSection: "<",
        goToQuestion: "<",
        goToRiskSummary: "<",
        togglePhaseExpand: "<",
        checkSidebarActive: "<",
        openCommentsModal: "<",
        toggleOpenQuestions: "<",
    },
    controller: AssessmentMenuCtrl,
    template: `
        <section class="assessment__phase" ng-repeat="phase in $ctrl.phases track by phase.Id" ng-class="{'locked': phase.locked}">
            <button
                class="assessment__phase-header"
                ng-if="phase.Sections.length"
                ng-click="$ctrl.togglePhaseExpand(phase)">
                {{phase.TemplateName}}
                <i
                    class="assessment__phase-header-lock ot ot-lock"
                    ng-if="phase.locked"
                    uib-tooltip="{{::$root.t('SectionIsReadOnly')}}"
                    tooltip-append-to-body="true"
                    tooltip-popup-close-delay="200">
                </i>
                <i class="assessment__phase-header-caret ot" ng-class="{'ot-caret-up': phase.expanded, 'ot-caret-down': !phase.expanded, }"></i>
            </button>
            <section class="assessment__phase-body" expand="phase.expanded" transition="300">
                <uib-accordion close-others="true">
                    <div
                        class="panel {{ $ctrl.checkSidebarActive(section) ? 'active' : '' }}"
                        uib-accordion-group=""
                        ng-if="phase.Sections.length && !section.IsSkipped"
                        ng-repeat="section in phase.Sections track by section.totalIndex"
                        ng-click="$ctrl.goToSection(section.totalIndex, section)">
                        <uib-accordion-heading ng-if="section.isWelcomeSection">{{::$root.t("Welcome")}}</uib-accordion-heading>
                        <uib-accordion-heading ng-if="!section.isWelcomeSection">
                            <div class="section-header">
                                <span class="section-name" ng-bind="section.Name"></span>
                                <div class="section-badges" ng-if="section.Questions">
                                    <span
                                        class="assessment__menu-pill assessment__menu-pill--unanswered-required"
                                        ng-if="section.Remaining.length"
                                        uib-tooltip-template="'unanswered-required-badges-tooltip.html'"
                                        tooltip-placement="top-left"
                                        tooltip-append-to-body="true"
                                        ng-click="$ctrl.goToQuestion(section.Id, section.Remaining[0])">
                                        <i class="ot ot-asterisk required"></i>
                                        <span class="value">{{section.Remaining.length}}</span>
                                    </span>
                                    <span
                                        class="assessment__menu-pill assessment__menu-pill--accepted"
                                        ng-if="$ctrl.viewState.canShowAcceptedPill && section.FilteredQuestions.Accepted.length"
                                        uib-tooltip-template="'accepted-badges-tooltip.html'"
                                        tooltip-placement="top-left"
                                        tooltip-append-to-body="true">
                                        <i class="ot ot-check"></i>
                                        <span class="value" ng-bind="section.FilteredQuestions.Accepted.length"></span>
                                    </span>
                                    <span
                                        class="assessment__menu-pill assessment__menu-pill--risk-{{section.FilteredQuestions.RiskLevel}}"
                                        ng-if="$ctrl.viewState.canShowRiskPill && section.FilteredQuestions.Risks"
                                        uib-tooltip-template="'flag-badges-tooltip.html'"
                                        tooltip-placement="top-left"
                                        tooltip-append-to-body="true">
                                        <i class="ot ot-flag"></i>
                                        <span class="value" ng-bind="section.FilteredQuestions.Risks"></span>
                                    </span>
                                    <span
                                        class="assessment__menu-pill assessment__menu-pill--added-info"
                                        ng-if="$ctrl.viewState.canShowInfoAddedPill && section.FilteredQuestions.InfoAdded.length"
                                        uib-tooltip-template="'info-added-badges-tooltip.html'"
                                        tooltip-placement="top-left"
                                        tooltip-append-to-body="true">
                                        <i class="ot ot-plus"></i>
                                        <span class="value" ng-bind="section.FilteredQuestions.InfoAdded.length"></span>
                                    </span>
                                    <span
                                        class="assessment__menu-pill assessment__menu-pill--info"
                                        ng-if="$ctrl.viewState.canShowInfoRequestedPill && section.FilteredQuestions.InfoRequested.length"
                                        uib-tooltip-template="'need-info-badges-tooltip.html'"
                                        tooltip-placement="top-left"
                                        tooltip-append-to-body="true">
                                        <i class="ot ot-question"></i>
                                        <span class="value" ng-bind="section.FilteredQuestions.InfoRequested.length"></span>
                                    </span>
                                </div>
                            </div>

                            <button class="toggle-questions" ng-click="$ctrl.toggleOpenQuestions(section.Id)">
                                <i class="ot ot-align-justify" ng-if="!questionsOpen(section)"></i>
                                <i class="ot ot-close" ng-if="questionsOpen(section)"></i>
                            </button>

                            <span class="section-scripts">
                                <script type="text/ng-template" id="required-badge-tooltip.html">
                                    <span ng-if="section.totalRemaining">
                                        <ng-pluralize
                                            count="section.totalRemaining"
                                            when="{ '1': $root.t('OneQuestionRemaining'), 'other': $root.t('ManyQuestionsRemaining', {Count: section.totalRemaining})}">
                                        </ng-pluralize>
                                    </span>
                                    <span ng-if="!section.totalRemaining">{{::$root.t("Done")}}&nbsp<i class="ot ot-check"></i></span>
                                </script>
                                <script type="text/ng-template" id="unanswered-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.Unanswered.length"
                                        when="{'0': $root.t('NoUnansweredQuestions'), '1': $root.t('OneUnansweredQuestion'), 'other': $root.t('ManyUnansweredQuestions', {Count: section.FilteredQuestions.Unanswered.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="unanswered-required-badges-tooltip.html"><span>
                                    <ng-pluralize
                                        count="section.Remaining.length"
                                        when="{'0': $root.t('NoRequiredQuestionsOrJustifications'), '1': $root.t('OneRequiredQuestionOrJustification'), 'other': $root.t('ManyRequiredQuestionsOrJustifications', {Count: section.Remaining.length})}">
                                    </ng-pluralize></span><span>&nbsp{{::$root.t("ClickToGoToQuestion")}}</span>
                                </script>
                                <script type="text/ng-template" id="answered-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.Answered.length"
                                        when="{'0': $root.t('NoAnsweredQuestions'), '1': $root.t('OneAnsweredQuestion'), 'other': $root.t('ManyAnsweredQuestions', {Count: section.FilteredQuestions.Answered.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="need-info-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.InfoRequested.length"
                                        when="{'0': $root.t('NoQuestionNeedsMoreInformation'), '1': $root.t('OneQuestionNeedsMoreInformation'), 'other': $root.t('ManyQuestionsNeedMoreInformation', {Count: section.FilteredQuestions.InfoRequested.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="info-added-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.InfoAdded.length"
                                        when="{'0': $root.t('NoQuestionsHaveInformationAdded'), '1': $root.t('OneQuestionHasInformationAdded'), 'other': $root.t('ManyQuestionsHaveInformationAdded', {Count: section.FilteredQuestions.InfoAdded.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="accepted-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.Accepted.length"
                                        when="{'0': $root.t('NoQuestionsAccepted'), '1': $root.t('OneQuestionAccepted'), 'other': $root.t('ManyQuestionsAccepted', {Count: section.FilteredQuestions.Accepted.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="needs-review-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.NeedsReview.length"
                                        when="{'0': $root.t('NoQuestionsNeedReview'), '1': $root.t('OneQuestionNeedsReview'), 'other': $root.t('ManyQuestionsNeedReview', {Count: section.FilteredQuestions.NeedsReview.length})}">
                                    </ng-pluralize>
                                </script>
                                <script type="text/ng-template" id="flag-badges-tooltip.html">
                                    <ng-pluralize
                                        count="section.FilteredQuestions.Risks"
                                        when="{'0': $root.t('NoFlaggedQuestions'), '1': $root.t('OneQuestionFlagged'), 'other': $root.t('ManyQuestionsFlagged', {Count: section.FilteredQuestions.Risks})}">
                                    </ng-pluralize>
                                </script>
                            </span>
                        </uib-accordion-heading>

                        <ul class="list-group" ng-if="section.Questions.length">
                            <li
                                class="list-group-item"
                                ng-repeat="Question in section.Questions track by Question.Id"
                                ng-click="$ctrl.goToQuestion(section.Id, Question.Id)"
                                ng-if="!section.isWelcomeSection && !Question.IsSkipped">
                                <span>{{Question.Name}}</span>
                                <span class="menu-question-actions btn-group-sm">
                                    <span class="action" ng-click="$ctrl.openCommentsModal(Question); $event.stopPropagation();">
                                        <span class="fa-stack" ng-if="!Question.HasComments">
                                            <i class="ot ot-comment fa-stack-1x comment-icon" ng-if="Question.Type!=1"></i>
                                        </span>
                                        <span class="fa-stack" ng-if="Question.HasComments">
                                            <i class="ot ot-comment fa-stack-1x comment-icon" ng-if="Question.Type!=1"></i>
                                            <i class="ot ot-circle fa-stack-1x notification"></i>
                                        </span>
                                    </span>
                                </span>
                            </li>
                        </ul>

                    </div>
                </uib-accordion>
            </section>
        </section>
        <section class="assessment__phase" ng-if="$ctrl.viewState.canShowRiskSummaryTab">
            <button class="assessment__phase-header assessment__phase-header--analysis" ng-click="$ctrl.goToRiskSummary()">{{::$root.t("RiskSummary")}}</button>
        </section>
    `,
};
