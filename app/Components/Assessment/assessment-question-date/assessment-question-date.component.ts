import {
    assign,
} from "lodash";

import moment from "moment";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionResponseTypes } from "enums/assessment-question.enum";

class AssessmentQuestionDateController {

    static $inject: string[] = ["$rootScope"];

    private dateVal: string;
    private question: IQuestion;
    private onUpdate: ({ actionType: IAssessmentQuestionAction}) => void;

    private QuestionResponseTypes: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
        if (this.question.Answer) {
            this.dateVal = moment(this.question.Answer).format("YYYY-MM-DD");
        }
    }

    private sendAction(type: string, value?: string): void {

        this.question.Answer = value ? moment(value).format("MM/DD/YYYY") : "";
        const actionType: IAssessmentQuestionAction = { type };
        if (type === "DATE_SELECTED") {
            assign(actionType, { value: this.question.Answer });
        }
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        isDisabled: "<",
        question: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionDateController,
    template: `
        <div>
            <downgrade-one-date-time-picker
                [disabled]="$ctrl.isDisabled"
                [date-placeholder]="$root.t('ChooseDate')"
                [date-time-model]="$ctrl.dateVal"
                (on-change)="$ctrl.sendAction('DATE_SELECTED', $event.jsdate)"
            ></downgrade-one-date-time-picker>
        </div>

        <assessment-question-not-sure-button
            button-class="margin-right-1 margin-top-bottom-1"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
            on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
            ng-if="$ctrl.question.AllowNotSure">
        </assessment-question-not-sure-button>

        <assessment-question-not-applicable-button
            button-class="margin-right-1 margin-top-bottom-1"
            class="option-container"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
            on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
            ng-if="$ctrl.question.AllowNotApplicable">
        </assessment-question-not-applicable-button>
    `,
};
