import { isRiskSummaryEnabled } from "oneRedux/reducers/setting.reducer";

import AssessmentFooterPermission from "./assessment-footer-permission.service";
import AssessmentBusinessLogic from "./assessment-business-logic.service";
import RiskBusinessLogic from "oneServices/logic/risk-business-logic.service";

import { AssessmentStates } from "constants/assessment.constants";
import * as ACTIONS from "./assessment-actions.constants";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IRisk } from "interfaces/risk.interface";
import { IAssessmentSubmitButtonConfig } from "interfaces/assessment.interface";
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ISection } from "interfaces/section.interface";

export default class AssessmentFooterViewLogicService {
    static $inject: string[] = ["$rootScope", "$state", "store", "AssessmentFooterPermission", "AssessmentBusinessLogic", "RiskBusinessLogic"];

    private hiddenButtonState: IAssessmentSubmitButtonConfig = {
        submitButtonVisible: false,
        submitButtonEnabled: false,
        submitButtonAction: "",
        submitButtonText: "Something went wrong.",
    };

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $state: ng.ui.IStateService,
        private readonly store: IStore,
        private readonly AssessmentFooterPermission: AssessmentFooterPermission,
        private readonly AssessmentBusinessLogic: AssessmentBusinessLogic,
        private readonly RiskBusinessLogic: RiskBusinessLogic,
    ) {}

    public calculateAssessmentFooterButtonState(currentUser: IUser, projectModel: IProject, riskModels?: IRisk[], currentSection?: ISection): IAssessmentSubmitButtonConfig {
        const { InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed } = AssessmentStates;
        switch (projectModel.StateDesc) {
            case InProgress:
                return this.getButtonStateForInProgress(currentUser, projectModel, currentSection);
            case UnderReview:
                return this.getButtonStateForUnderReview(currentUser, projectModel, riskModels);
            case InfoNeeded:
                return this.getButtonStateForNeedInfo(currentUser, projectModel, currentSection);
            case RiskAssessment:
                return this.getButtonStateForRiskAssessment(currentUser, projectModel, riskModels);
            case RiskTreatment:
                return this.getButtonStateForRiskTreatment(currentUser, projectModel, riskModels);
            case Completed:
                return this.hiddenButtonState;
        }
    }

    private getButtonStateForInProgress(currentUser: IUser, projectModel: IProject, currentSection: ISection): IAssessmentSubmitButtonConfig {
        if (!this.AssessmentFooterPermission.hasViewPermission("canViewSubmitButton", currentUser, projectModel)) {
            return this.hiddenButtonState;
        }

        const { SUBMIT_ASSESSMENT } = ACTIONS;
        const requiredQuestionsAnswered: boolean = this.AssessmentBusinessLogic.allRequiredQuestionsAreAnswered(projectModel);
        const requiredJustificationsAnswered: boolean = this.AssessmentBusinessLogic.allRequiredJustificationsAreAnswered(projectModel);
        const isMultiRespondent: boolean = this.AssessmentBusinessLogic.isMultiRespondent(projectModel);

        if (projectModel.Version > 1) {
            const changeOrNoChangeSelectedAllQuestions: boolean = this.AssessmentBusinessLogic.allQuestionsChangedOrNoChanged(projectModel);
            if (isMultiRespondent) {
                const changeOrNoChangeSelectedAllSectionQuestions: boolean = this.AssessmentBusinessLogic.allSectionQuestionsChangedOrNoChanged(currentSection);
                if (this.AssessmentBusinessLogic.isNotRespondentOfAnySection(projectModel)
                    && this.AssessmentBusinessLogic.isApproverOrHasFullAccess(projectModel)) {
                    if (requiredJustificationsAnswered && requiredQuestionsAnswered && changeOrNoChangeSelectedAllSectionQuestions) {
                        return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit"); // approver submits all sections
                    } else {
                        return this.showDisabledButton("Submit");
                    }
                } else if (!this.AssessmentBusinessLogic.isNotRespondentOfAnySection(projectModel)) {
                    if (this.AssessmentBusinessLogic.isCurrentUserAbleToSubmitCurrentSection(currentSection)
                        && changeOrNoChangeSelectedAllSectionQuestions) {
                            return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit"); // respondent submits one section
                        } else {
                        return this.showDisabledButton("Submit");
                    }
                } else {
                    return this.hiddenButtonState;
                }
            } else if (requiredQuestionsAnswered && requiredJustificationsAnswered && changeOrNoChangeSelectedAllQuestions) {
                return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit");
            } else {
                return this.showDisabledButton("Submit");
            }
        } else if (isMultiRespondent) {
            if (this.AssessmentBusinessLogic.isNotRespondentOfAnySection(projectModel)
            && this.AssessmentBusinessLogic.isApproverOrHasFullAccess(projectModel)) {
                if (requiredJustificationsAnswered && requiredQuestionsAnswered) {
                    return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit"); // approver submits all sections
                } else {
                    return this.showDisabledButton("Submit");
                }
            } else if (!this.AssessmentBusinessLogic.isNotRespondentOfAnySection(projectModel)) {
                if (this.AssessmentBusinessLogic.isCurrentUserAbleToSubmitCurrentSection(currentSection)) {
                    return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit"); // respondent submits one section
                } else {
                    return this.showDisabledButton("Submit");
                }
            } else {
                return this.hiddenButtonState;
            }
        } else if (requiredQuestionsAnswered && requiredJustificationsAnswered) {
            return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit"); // submit single respondent project
        } else {
            return this.showDisabledButton("Submit");
        }
    }

    private getButtonStateForUnderReview(currentUser: IUser, projectModel: IProject, riskModels?: IRisk[]): IAssessmentSubmitButtonConfig {
        if (!this.AssessmentFooterPermission.hasViewPermission("canViewSendBackButton", currentUser, projectModel)
            || !this.AssessmentFooterPermission.hasViewPermission("canViewNextStepsButton", currentUser, projectModel)
            || !this.AssessmentFooterPermission.hasViewPermission("canViewContinueToRiskTrackingButton", currentUser, projectModel)
            || !this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)
        ) {
            return this.hiddenButtonState;
        }

        const { SEND_BACK_MODAL, OPEN_ANALYSIS_MODAL, APPROVE_ASSESSMENT, OPEN_NEXT_STEPS_MODAL } = ACTIONS;
        const riskSummaryEnabled: boolean = isRiskSummaryEnabled(this.store.getState());
        const isThresholdTemplate: boolean = projectModel.IsLinked;

        if (this.AssessmentFooterPermission.hasViewPermission("canViewSendBackButton", currentUser, projectModel)
            && this.AssessmentBusinessLogic.someQuestionsNeedMoreInfo(projectModel)) {
            return this.showEnabledButton(SEND_BACK_MODAL, "SendBack");
        }

        const requiredQuestionsAnswered: boolean = this.AssessmentBusinessLogic.allRequiredQuestionsAreAnswered(projectModel);
        const requiredJustificationsAnswered: boolean = this.AssessmentBusinessLogic.allRequiredJustificationsAreAnswered(projectModel);
        // TODO remove the checks for states, once assessment is rebuilt with redux
        const isRiskSummaryState: boolean = this.$state.includes("zen.app.pia.module.projects.risk_summary");
        const isAssessmentState: boolean = this.$state.includes("zen.app.pia.module.datamap.views.assessment-old.single.module.questions")
            || this.$state.includes("zen.app.pia.module.projects.assessment.module.questions");

        if (!isThresholdTemplate && riskSummaryEnabled
            && this.AssessmentFooterPermission.hasViewPermission("canViewContinueToRiskTrackingButton", currentUser, projectModel)
            && (isRiskSummaryState && riskModels && riskModels.length
                || isAssessmentState && this.AssessmentBusinessLogic.flaggedRiskExists(projectModel))
            && this.AssessmentBusinessLogic.multiRespondentProjectsAllUnderReview(projectModel)) {
            if (requiredQuestionsAnswered && requiredJustificationsAnswered) {
                return this.showEnabledButton(OPEN_ANALYSIS_MODAL, "ContinueRiskTracking", { RiskTracking: this.$rootScope.customTerms.RiskTracking });
            } else {
                return this.showDisabledButton("ContinueRiskTracking", { RiskTracking: this.$rootScope.customTerms.RiskTracking });
            }
        } else if (!isThresholdTemplate && riskSummaryEnabled
            && this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)
            && this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
            return this.showEnabledButton(APPROVE_ASSESSMENT, "Approve");
        }

        if (this.AssessmentBusinessLogic.isThresholdAndFirstTemplate(projectModel)) {
            if (this.AssessmentFooterPermission.hasViewPermission("canViewNextStepsButton", currentUser, projectModel)
                && this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
                return this.showEnabledButton(OPEN_NEXT_STEPS_MODAL, "NextSteps");
            } else {
                return this.showDisabledButton("NextSteps");
            }
        } else if (this.AssessmentBusinessLogic.isThresholdAndSecondTemplate(projectModel)) {
            if (riskSummaryEnabled
                && this.AssessmentFooterPermission.hasViewPermission("canViewContinueToRiskTrackingButton", currentUser, projectModel)
                && (isRiskSummaryState && riskModels && riskModels.length
                    || isAssessmentState && this.AssessmentBusinessLogic.flaggedRiskExists(projectModel))) {
                return this.showEnabledButton(OPEN_ANALYSIS_MODAL, "ContinueRiskTracking", { RiskTracking: this.$rootScope.customTerms.RiskTracking });
            } else if (this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)
                && this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
                    return this.showEnabledButton(APPROVE_ASSESSMENT, "Approve");
            }
        }

        if (!riskSummaryEnabled
            && this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)
            && this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
            return this.showEnabledButton(APPROVE_ASSESSMENT, "Approve");
        }

        return this.showDisabledButton("Approve");
    }

    private getButtonStateForNeedInfo(currentUser: IUser, projectModel: IProject, currentSection: ISection): IAssessmentSubmitButtonConfig {
        const { SUBMIT_ASSESSMENT } = ACTIONS;
        const isMultiRespondent: boolean = this.AssessmentBusinessLogic.isMultiRespondent(projectModel);
        const isRiskSummaryState: boolean = this.$state.includes("zen.app.pia.module.projects.risk_summary");
        const isWelcomeSection: boolean = currentSection.isWelcomeSection;

        if (!this.AssessmentFooterPermission.hasViewPermission("canViewNeedsInfoSubmitButton", currentUser, projectModel)
            || isRiskSummaryState) {
                return this.hiddenButtonState;
        }
        if (isMultiRespondent) {
            if ((currentSection.StateDesc === AssessmentStates.InfoNeeded)
                && !this.AssessmentBusinessLogic.someSectionQuestionsNeedMoreInfo(currentSection)
                && this.AssessmentBusinessLogic.isApproverAndOrRespondent(projectModel, currentSection)) {
                    return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit");
            } else if (!isWelcomeSection
                && this.AssessmentBusinessLogic.isNotRespondentOfAnySection(projectModel)
                && this.AssessmentBusinessLogic.isApproverOrHasFullAccess(projectModel)
                && !this.AssessmentBusinessLogic.someQuestionsNeedMoreInfo(projectModel)) {
                    return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit");
            } else {
                return this.showDisabledButton("Submit");
            }
        } else if (this.AssessmentBusinessLogic.someQuestionsNeedMoreInfo(projectModel)) {
            return this.showDisabledButton("Submit");
        } else {
            return this.showEnabledButton(SUBMIT_ASSESSMENT, "Submit");
        }
    }

    private getButtonStateForRiskAssessment(currentUser: IUser, projectModel: IProject, riskModels: IRisk[]): IAssessmentSubmitButtonConfig {
        const { SEND_RECOMMENDATIONS, APPROVE_ASSESSMENT } = ACTIONS;
        if (!this.AssessmentFooterPermission.hasViewPermission("canViewSendRecommendationsButton", currentUser, projectModel)
            || !this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)
        ) {
            return this.hiddenButtonState;
        }

        if (isRiskSummaryEnabled(this.store.getState())) {
            // TODO remove the checks for states, once assessment is rebuilt with redux
            const isRiskSummaryState: boolean = this.$state.includes("zen.app.pia.module.projects.risk_summary");
            const isAssessmentState: boolean = this.$state.includes("zen.app.pia.module.datamap.views.assessment-old.single.module.questions")
                || this.$state.includes("zen.app.pia.module.projects.assessment.module.questions");

            if (isRiskSummaryState && riskModels && riskModels.length
                || isAssessmentState && this.AssessmentBusinessLogic.flaggedRiskExists(projectModel)
            ) {
                if (isRiskSummaryState && this.RiskBusinessLogic.allRisksHaveRecommendation(riskModels)
                    || isAssessmentState && this.AssessmentBusinessLogic.allRisksHaveRecommendation(projectModel)
                ) {
                    return this.showEnabledButton(SEND_RECOMMENDATIONS, "SendRecommendations", { Recommendations: this.$rootScope.customTerms.Recommendations });
                }
                return this.showDisabledButton("SendRecommendations");
            }

            return this.showEnabledButton(APPROVE_ASSESSMENT, "Approve");
        }

        if (this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
            return this.showEnabledButton(APPROVE_ASSESSMENT, "Approve");
        }

        return this.showDisabledButton("Approve");
    }

    private getButtonStateForRiskTreatment(currentUser: IUser, projectModel: IProject, riskModels: IRisk[]): IAssessmentSubmitButtonConfig {
        const { COMPLETE_ANALYSIS } = ACTIONS;
        if (!this.AssessmentFooterPermission.hasViewPermission("canViewApproveButton", currentUser, projectModel)) {
            return this.hiddenButtonState;
        }

        if (isRiskSummaryEnabled(this.store.getState())) {
            const isRiskSummaryState: boolean = this.$state.includes("zen.app.pia.module.projects.risk_summary");
            const isAssessmentState: boolean = this.$state.includes("zen.app.pia.module.datamap.views.assessment-old.single.module.questions")
                || this.$state.includes("zen.app.pia.module.projects.assessment.module.questions");

            if ((isRiskSummaryState && this.RiskBusinessLogic.allRisksAreReducedOrRetained(riskModels)
                || isAssessmentState && this.AssessmentBusinessLogic.allRisksAreReducedOrRetained(projectModel))
                && this.AssessmentBusinessLogic.isReadyToApprove(projectModel)
            ) {
                return this.showEnabledButton(COMPLETE_ANALYSIS, "Approve");
            }
            return this.showDisabledButton("Approve");
        }

        if (this.AssessmentBusinessLogic.isReadyToApprove(projectModel)) {
            return this.showEnabledButton(COMPLETE_ANALYSIS, "Approve");
        }

        return this.showDisabledButton("Approve");
    }

    private showEnabledButton(action: string, translationKey: string, customTerm?: IStringMap<string>): IAssessmentSubmitButtonConfig {
        return {
            submitButtonVisible: true,
            submitButtonEnabled: true,
            submitButtonAction: action,
            submitButtonText: this.$rootScope.t(translationKey, customTerm),
        };
    }

    private showDisabledButton(translationKey: string, customTerm?: IStringMap<string>): IAssessmentSubmitButtonConfig {
        return {
            submitButtonVisible: true,
            submitButtonEnabled: false,
            submitButtonAction: null,
            submitButtonText: this.$rootScope.t(translationKey, customTerm),
        };
    }
}
