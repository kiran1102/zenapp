import AssessmentMenuPermission from "./assessment-menu-permission.service";

import { IUser } from "interfaces/user.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { IStringMap } from "interfaces/generic.interface";

export default class AssessmentMenuViewLogicService {

    static $inject: string[] = ["AssessmentMenuPermission"];
    constructor(
        private readonly AssessmentMenuPermission: AssessmentMenuPermission,
    ) {}

    public calculateNewViewState(currentUser: IUser, assessment: IPiaAssessment): IStringMap<boolean> {
        return {
            canShowAcceptedPill:        this.hasViewPermission("canShowAcceptedPill", assessment),
            canShowInfoAddedPill:       this.hasViewPermission("canShowInfoAddedPill", assessment),
            canShowInfoRequestedPill:   this.hasViewPermission("canShowInfoRequestedPill", assessment),
            canShowRiskPill:            this.hasViewPermission("canShowRiskPill", assessment),
            canShowUnansweredPill:      this.hasViewPermission("canShowUnansweredPill", assessment),
            canShowRiskSummaryTab:      this.hasViewPermission("canShowRiskSummaryTab", assessment),
        };
    }

    private hasViewPermission(itemPermissionsKey: string, assessment: IPiaAssessment): boolean {
        return this.AssessmentMenuPermission.hasMenuPermission(itemPermissionsKey, assessment);
    }

}
