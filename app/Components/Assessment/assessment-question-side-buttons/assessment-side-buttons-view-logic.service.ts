import { getQuestionApprovalEnabled } from "oneRedux/reducers/setting.reducer";
import NeedsMoreInfoPermission from "generalcomponent/Assessment/assessment-nmi-permission.service";
import RiskPermissionService from "oneServices/permissions/risk-permission.service";

import { QuestionStates, QuestionTypes } from "enums/assessment-question.enum";
import { RiskLevels } from "enums/risk.enum";
import * as ACTIONS from "generalcomponent/Assessment/assessment-actions.constants";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IQuestion } from "interfaces/question.interface";
import { IStore } from "interfaces/redux.interface";
import { IRisk } from "interfaces/risk.interface";

export default class AssessmentSideButtonsViewLogicService {

    static $inject: string[] = ["store", "$rootScope", "NeedsMoreInfoPermission", "RiskPermission"];

    constructor(
        private readonly store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly NeedsMoreInfoPermission: NeedsMoreInfoPermission,
        private readonly RiskPermission: RiskPermissionService,
    ) {}

    public calculateNewViewState(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): any {
        return {
            canViewNeedsMoreInfoButton: this.getCanViewNeedsMoreInfoButton(currentUser, projectModel, questionModel),
            canViewAcceptQuestionButton: this.getCanViewAcceptQuestionButton(currentUser, projectModel, questionModel),
            isAcceptQuestionButtonDisabled: this.getIsAcceptQuestionButtonDisabled(questionModel),
            canViewAddInformationButton: this.getCanViewAddInformationButton(currentUser, projectModel, questionModel),
            canViewInfoAddedButton: this.getCanViewInfoAddedButton(currentUser, projectModel, questionModel),
            canViewFlagRisksButton: this.getCanViewFlagRisksButton(currentUser, projectModel, questionModel, questionModel.Risk),
            riskLevelClass: this.getRiskLevelClass(questionModel),
            acceptQuestionButtonClass: this.getAcceptQuestionButtonClass(questionModel),
            addInfoButtonClass: this.getAddInfoButtonClass(questionModel),
            addInfoButtonText: this.getAddInfoButtonText(questionModel),
            infoAddedButtonClass: this.getInfoAddedButtonClass(questionModel),
            needsMoreInfoButtonClass: this.getNeedsMoreInfoButtonClass(questionModel),
        };
    }

    private getCanViewNeedsMoreInfoButton(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.NeedsMoreInfoPermission.hasQuestionPermission("canViewNeedsMoreInfoButton", currentUser, projectModel, questionModel);
    }

    private getCanViewAcceptQuestionButton(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return getQuestionApprovalEnabled(this.store.getState()) && this.NeedsMoreInfoPermission.hasQuestionPermission("canViewAcceptQuestionButton", currentUser, projectModel, questionModel);
    }

    private getIsAcceptQuestionButtonDisabled(questionModel: IQuestion): boolean {
        return getQuestionApprovalEnabled(this.store.getState()) && (questionModel.Required && questionModel.State === QuestionStates.Unanswered);
    }

    private getCanViewAddInformationButton(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.NeedsMoreInfoPermission.hasQuestionPermission("canViewAddInformationButton", currentUser, projectModel, questionModel);
    }

    private getCanViewInfoAddedButton(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.NeedsMoreInfoPermission.hasQuestionPermission("canViewInfoAddedButton", currentUser, projectModel, questionModel);
    }

    private getCanViewFlagRisksButton(currentUser: IUser, projectModel: IProject, questionModel: IQuestion, riskModel: IRisk): boolean {
        return questionModel.Type !== QuestionTypes.Content
            && (this.RiskPermission.hasRiskPermission("canCreateRisk", currentUser, projectModel, riskModel)
            || this.RiskPermission.hasRiskPermission("canShowSingleRisk", currentUser, projectModel, riskModel));
    }

    private getRiskLevelClass(questionModel: IQuestion): string {
        if (!questionModel.Risk) return "";
        switch (questionModel.Risk.Level) {
            case RiskLevels.Low:
                return "low-risk-icon";
            case RiskLevels.Medium:
                return "medium-risk-icon";
            case RiskLevels.High:
                return "high-risk-icon";
            case RiskLevels.VeryHigh:
                return "very-high-risk-icon";
            default:
                return "";
        }
    }

    private getAcceptQuestionButtonClass(questionModel: IQuestion): string {
        if (questionModel.State === QuestionStates.Accepted) return "btn-success";
        return "btn-default";
    }

    private getAddInfoButtonClass(questionModel: IQuestion): string {
        if (questionModel.State === QuestionStates.InfoAdded) return "btn-info";
        return "btn-default";
    }

    private getInfoAddedButtonClass(questionModel: IQuestion): string {
        if (questionModel.State === QuestionStates.InfoAdded) return "btn-info";
        return "btn-default";
    }

    private getNeedsMoreInfoButtonClass(questionModel: IQuestion): string {
        if (questionModel.State === QuestionStates.InfoRequested) return "btn-warning";
        return "btn-default";
    }

    private getAddInfoButtonText(questionModel: IQuestion): string {
        return questionModel.State === QuestionStates.InfoAdded
            ? this.$rootScope.t("InformationAdded")
            : this.$rootScope.t("AddInformation");
    }
}

export interface ISideButtonViewState {
    canViewNeedsMoreInfoButton: boolean;
    canViewAcceptQuestionButton: boolean;
    isAcceptQuestionButtonDisabled: boolean;
    canViewAddInformationButton: boolean;
    canViewInfoAddedButton: boolean;
    canViewFlagRisksButton: boolean;
    riskLevelClass: string;
    acceptQuestionButtonClass: string;
    addInfoButtonClass: string;
    addInfoButtonText: string;
    infoAddedButtonClass: string;
    needsMoreInfoButtonClass: string;
}
