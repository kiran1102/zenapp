// External
import { unescape } from "lodash";
import { Subject } from "rxjs";
import { startWith, filter, takeUntil } from "rxjs/operators";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { IStore, IStoreState } from "interfaces/redux.interface";

// Services
import AssessmentSideButtonsViewLogic from "./assessment-side-buttons-view-logic.service";

// Interfaces
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";
import { IUser } from "interfaces/user.interface";

// Constants
import { AssessmentStates } from "constants/assessment.constants";

// Enums
import { QuestionTypes, QuestionStates } from "enums/assessment-question.enum";
import { RiskLevels } from "enums/risk.enum";

class AssessmentQuestionSideButtonsController {

    static $inject: string[] = ["store", "AssessmentSideButtonsViewLogic"];

    private currentUser: IUser;
    private project: IProject;
    private section: ISection;
    private question: IQuestion;
    private onButtonClick: ({ questionAction: IAssessmentQuestionAction}) => void;
    private viewState: any;
    private ProjectStates: any;
    private QuestionTypes: any;
    private QuestionStates: any;
    private RiskLevels: any;
    private destroy$ = new Subject();

    constructor(
        public readonly store: IStore,
        private readonly assessmentSideButtonsViewLogic: AssessmentSideButtonsViewLogic,
    ) {}

    public $onInit(): void {
        this.ProjectStates = AssessmentStates;
        this.QuestionTypes = QuestionTypes;
        this.QuestionStates = QuestionStates;
        this.RiskLevels = RiskLevels;
        this.question.MoreInfoComment = this.unescapeComment(this.question.MoreInfoComment);
        this.question.InfoAddedComment = this.unescapeComment(this.question.InfoAddedComment);
        observableFromStore(this.store)
            .pipe(
                startWith(this.store.getState()),
                filter((state: IStoreState) => Boolean(state.settings.projectSettings)),
                takeUntil(this.destroy$),
            )
            .subscribe(() => this.componentWillReceiveState());
        this.componentWillReceiveState();
    }

    public $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(): void {
        this.viewState = this.assessmentSideButtonsViewLogic.calculateNewViewState(this.currentUser, this.project, this.question);
    }

    private buttonClicked(type: string): void {
        const questionAction: IAssessmentQuestionAction = { type, question: this.question };
        this.onButtonClick({ questionAction });
    }

    private unescapeComment(comment: string): string {
        comment = unescape(comment)
                    .replace(/&#34;/g, "\"")
                    .replace(/&#61;/g, "=")
                    .replace(/&#43;/g, "+")
                    .replace(/&#64;/g, "@")
                    .replace(/&#96;/g, "`");
        return comment;
    }
}

export default {
    bindings: {
        currentUser: "<",
        project: "<",
        projectLevelApproval: "<",
        section: "<",
        question: "<",
        onButtonClick: "&",
    },
    controller: AssessmentQuestionSideButtonsController,
    template: `
    <div
        class="assessment-question__side-buttons"
        ng-if="$ctrl.viewState.canViewFlagRisksButton
            || $ctrl.viewState.canViewAcceptQuestionButton
            || $ctrl.viewState.canViewAddInformationButton
            || $ctrl.viewState.canViewInfoAddedButton
            || $ctrl.viewState.canViewNeedsMoreInfoButton">
        <button
            class="min-width-15"
            ng-click="$ctrl.buttonClicked('OPEN_EXCEPTIONS_MODAL')"
            ng-if="$ctrl.viewState.canViewFlagRisksButton">
            <i class="fa fa-flag {{$ctrl.viewState.riskLevelClass}}"></i>
            <span>{{::$root.t('FlagRisks')}}</span>
        </button>
        <button
            class="{{$ctrl.viewState.acceptQuestionButtonClass}} min-width-15"
            ng-click="$ctrl.buttonClicked('TOGGLE_QUESTION_ACCEPTANCE')"
            ng-if="$ctrl.viewState.canViewAcceptQuestionButton"
            ng-disabled="$ctrl.viewState.isAcceptQuestionButtonDisabled">
            <i class="ot ot-check"></i>
            <span>{{::$root.t('Accept')}}</span>
            <span
                class="assessment-question__side-buttons-tooltip"
                ng-if="$ctrl.viewState.isAcceptQuestionButtonDisabled"
                tooltip-append-to-body="true"
                tooltip-placement="right"
                tooltip-popup-close-delay="200"
                tooltip-trigger="mouseenter"
                uib-tooltip="{{::$root.t('QuestionRequiredAnsweredBeforeAcceptIt')}}">
            </span>
        </button>
        <button
            class="{{$ctrl.viewState.addInfoButtonClass}} min-width-15"
            ng-click="$ctrl.buttonClicked('OPEN_ADD_INFO_MODAL')"
            ng-if="$ctrl.viewState.canViewAddInformationButton">
            <i class="fa fa-plus"></i>
            {{$ctrl.viewState.addInfoButtonText}}
        </button>
        <button
            class="{{$ctrl.viewState.infoAddedButtonClass}} min-width-15"
            ng-click="$ctrl.buttonClicked('OPEN_VIEW_INFO_ADDED_MODAL')"
            ng-if="$ctrl.viewState.canViewInfoAddedButton">
            <i class="fa fa-plus"></i>
            {{::$root.t('InformationAdded')}}
        </button>
        <button
            class="{{$ctrl.viewState.needsMoreInfoButtonClass}} min-width-15"
            ng-click="$ctrl.buttonClicked('OPEN_NEED_INFO_MODAL')"
            ng-if="$ctrl.viewState.canViewNeedsMoreInfoButton">
            <i class="fa fa-question"></i>
            <span>{{::$root.t('NeedsMoreInfo')}}</span>
        </button>
	</div>
    `,
};
