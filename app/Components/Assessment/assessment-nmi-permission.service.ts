import { includes, find } from "lodash";

import { Permissions } from "modules/shared/services/helper/permissions.service";

import { AssessmentPermissions, AssessmentStates } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";
import { QuestionStates, QuestionTypes } from "enums/assessment-question.enum";

import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";

export default class NeedsMoreInfoPermissionService {
    static $inject: string[] = ["Permissions"];

    private needsMoreInfoPermissions;

    constructor(
        private permissions: Permissions,
    ) {
        const { InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment } = AssessmentStates;
        const { AssessmentRequestInfo, AssessmentViewAddedInfo, AssessmentProvideMoreInfo, AssessmentQuestionApprove } = AssessmentPermissions;
        const { Unanswered, Answered, Accepted, InfoRequested, InfoAdded } = QuestionStates;
        const { Content } = QuestionTypes;
        const { approver, respondent } = AssignmentTypes;

        this.needsMoreInfoPermissions = {
            canViewNeedsMoreInfoButton: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [Answered, InfoRequested, Accepted],
                disallowedQuestionTypes:    [Content],
                requiresLatestVersion:      true,
            },
            canEditNeedsMoreInfoComment: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [Answered, Accepted],
                requiresLatestVersion:      true,
            },
            canSubmitNeedsMoreInfoModal: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [Answered, InfoRequested, Accepted],
                requiresLatestVersion:      true,
            },
            canViewInfoRequestedPill: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
                allowedQuestionStates:      [InfoRequested],
            },
            canViewInfoRequestedBadge: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
                allowedQuestionStates:      [InfoRequested],
            },
            canViewAddInformationButton: {
                allowedPermission:          AssessmentProvideMoreInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InfoNeeded],
                allowedQuestionStates:      [InfoRequested, InfoAdded],
                disallowedQuestionTypes:    [Content],
                requiresLatestVersion:      true,
            },
            canSubmitAddInformationModal: {
                allowedPermission:          AssessmentProvideMoreInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InfoNeeded],
                allowedQuestionStates:      [InfoRequested, InfoAdded],
                requiresLatestVersion:      true,
            },
            canViewInfoAddedPill: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
                allowedQuestionStates:      [InfoAdded],
            },
            canViewInfoAddedBadge: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
                allowedQuestionStates:      [InfoAdded],
            },
            canViewInfoAddedButton: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [InfoAdded],
                disallowedQuestionTypes:    [Content],
            },
            canViewNeedMoreInfoButtonInfoAddedModal: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [InfoAdded],
                requiresLatestVersion:      true,
            },
            canViewAcceptButtonInfoAddedModal: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [InfoAdded],
                requiresLatestVersion:      true,
            },
            canRemoveInfoModal: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
                allowedQuestionStates:      [InfoRequested],
                requiresLatestVersion:      true,
            },
            canViewAcceptQuestionButton: {
                allowedPermission:          AssessmentQuestionApprove,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview, RiskAssessment, RiskTreatment],
                allowedQuestionStates:      [Unanswered, Answered, Accepted],
                disallowedQuestionTypes:    [Content],
                requiresLatestVersion:      true,
            },
        };
    }

    public hasQuestionPermission(permissionToCheck: string, currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        const config = this.needsMoreInfoPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAllowedUser(config.allowedRoles, currentUser, projectModel, questionModel))
            && this.isAllowedState(config.allowedAssessmentStates, projectModel.StateDesc)
            && this.isAllowedState(config.allowedQuestionStates, questionModel.State)
            && this.isAllowedQuestionType(config.disallowedQuestionTypes, questionModel.Type)
            && this.isLatestVersionRequired(config.requiresLatestVersion, projectModel);
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedQuestionType(questionTypeArray: Array<string | number>, type: QuestionTypes): boolean {
        return !(questionTypeArray && questionTypeArray.length && includes(questionTypeArray, type));
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return includes(stateArray, state);
    }

    private isAllowedUser(permissionArray: string[], user: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        let isApprover: boolean;
        let isRespondent: boolean;
        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId === user.Id;
            } else if (permission === AssignmentTypes.respondent) {
                const currentSection: ISection = find(projectModel.Sections, { Id: questionModel.SectionId });
                isRespondent = currentSection.Assignment.AssigneeId === user.Id;
            }
        }
        return isApprover || isRespondent;
    }

    private isLatestVersionRequired(requiresLatestVersion: boolean, projectModel: IProject): boolean {
        return !requiresLatestVersion || projectModel.IsLatestVersion;
    }

}
