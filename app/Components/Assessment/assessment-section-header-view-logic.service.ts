import { split } from "lodash";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import AssessmentSectionHeaderPermission from "./assessment-section-header-permission.service";

import { AssessmentSubmitStates } from "enums/assessment-question.enum";

import { EmptyGuid } from "constants/empty-guid.constant";

import { IUser } from "interfaces/user.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { IStringMap } from "interfaces/generic.interface";
import { ISection } from "interfaces/section.interface";
import { IStore } from "interfaces/redux.interface";

export default class AssessmentSectionHeaderViewLogicService {

    static $inject: string[] = ["store", "AssessmentSectionHeaderPermission"];
    constructor(
        private readonly store: IStore,
        private readonly AssessmentSectionHeaderPermission: AssessmentSectionHeaderPermission,
    ) {}

    public calculateNewViewState(assessment: IPiaAssessment, currentSection: ISection): IStringMap<boolean> {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        return {
            // Show/Hide
            canShowDescription:     this.hasViewPermission("canShowAssessmentDescription", currentUser, assessment, currentSection),
            canShowName:            this.hasViewPermission("canShowAssessmentName", currentUser, assessment, currentSection),
            canShowOrg:             this.hasViewPermission("canShowAssessmentOrg", currentUser, assessment, currentSection),
            canShowDeadline:        this.hasViewPermission("canShowAssessmentDeadline", currentUser, assessment, currentSection),
            canShowApprover:        this.hasViewPermission("canShowAssessmentApprover", currentUser, assessment, currentSection),
            canShowRespondent:      this.hasViewPermission("canShowAssessmentRespondent", currentUser, assessment, currentSection),
            canShowCreator:         this.hasViewPermission("canShowAssessmentCreator", currentUser, assessment, currentSection),
            canShowSectionStatus:   this.hasViewPermission("canShowSectionStatus", currentUser, assessment, currentSection),
            // Edit
            canEditDescription:     this.hasEditPermission("canEditAssessmentDescription", currentUser, assessment, currentSection),
            canEditName:            this.hasEditPermission("canEditAssessmentName", currentUser, assessment, currentSection),
            canEditOrg:             false, // in the backlog
            canEditDeadline:        this.hasEditPermission("canEditAssessmentDeadline", currentUser, assessment, currentSection),
            canEditApprover:        this.hasEditPermission("canReassignAssessmentApprover", currentUser, assessment, currentSection),
            canEditRespondent:      this.getCanEditRespondent("canReassignAssessmentRespondent", currentUser, assessment, currentSection),
        };
    }

    private hasViewPermission(itemPermissionsKey: string, currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        return this.AssessmentSectionHeaderPermission.hasViewPermission(itemPermissionsKey, currentUser, assessment, currentSection);
    }

    private hasEditPermission(itemPermissionsKey: string, currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        return this.AssessmentSectionHeaderPermission.hasEditPermission(itemPermissionsKey, currentUser, assessment, currentSection);
    }

    private getCanEditRespondent(itemPermissionsKey: string, currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        const respondents: string[] = split(assessment.Lead, ",");
        const isMultiRespondent: boolean = respondents.length > 1;

        return this.hasEditPermission(itemPermissionsKey, currentUser, assessment, currentSection)
            && !(isMultiRespondent && currentSection.Id === EmptyGuid);
    }

}
