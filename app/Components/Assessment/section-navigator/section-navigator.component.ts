class SectionNavigator implements ng.IComponentController {
    static $inject: string[] = ["$scope", "ENUMS", "OneAssessment"];

    private sections: any;

    constructor(readonly $scope: any, ENUMS: any, readonly OneAssessment: any) {}

    public $onInit(): void {
        this.startListeners();
        this.sections = this.OneAssessment.getSections();
    }

    private startListeners(): void {
        this.$scope.$on("SECTION_CHANGED", (): void => {
            this.sections = this.OneAssessment.getSections();
        });
        this.$scope.$on("ANSWER_CHANGED", (): void => {
            this.sections = this.OneAssessment.getSections();
        });
    }

}

const sectionNavigator: ng.IComponentOptions = {
    controller: SectionNavigator,
    bindings: {
        context: "<",
    },
    template: `
        <section class="section-navigator">
            <ul class="section-navigator__list row-space-between margin-top-bottom-1 margin-left-right-auto">
                <section-navigator-button
                    class="section-navigator__item row-vertical-center stretch-horizontal"
                    section="section"
                    ng-repeat="section in $ctrl.sections track by $index">
                </section-navigator-button>
            </ul>
        </section>
    `,
};

export default sectionNavigator;
