export default {
    bindings: {
        showHint: "<",
        question: "<",
    },
    template: `
        <h3 class="question__name">
            {{$ctrl.question.Name}}
            <sup class="fa fa-asterisk question__required" ng-if="$ctrl.question.Required"></sup>
            <i
                class="question__hint fa fa-info-circle"
                ng-if="$ctrl.showHint"
                tooltip-placement="top"
                tooltip-trigger="hover"
                uib-tooltip="{{$ctrl.question.Hint}}">
            </i>
        </h3>
    `,
};
