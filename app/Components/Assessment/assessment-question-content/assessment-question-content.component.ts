import invariant from "invariant";
declare const OneConfirm: any;
import { assign, map, find, isUndefined, isArray } from "lodash";

import AssessmentQuestionValidation from "../assessment-question-validation.service";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import * as ACTIONS from "../assessment-actions.constants";

import { QuestionTypes, QuestionResponseTypes, QuestionMultiChoiceTypes } from "enums/assessment-question.enum";

class AssessmentQuestionContentController {

    static $inject: string[] = ["$rootScope", "$filter", "AssessmentQuestionValidation"];

    private currentUser: IUser;
    private events: any;
    private question: IQuestion;
    private section: ISection;
    private project: IProject;
    private onQuestionUpdate: ({}) => void;

    private QuestionTypes: any;
    private QuestionMultiChoiceTypes: any;
    private viewState: any;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $filter: ng.IFilterService,
        private assessmentQuestionValidation: AssessmentQuestionValidation,
    ) { }

    public $onInit(): void {
        this.QuestionTypes = QuestionTypes;
        this.QuestionMultiChoiceTypes = QuestionMultiChoiceTypes;

        if (!this.events) {
            this.events = {
                multiChoiceGroupSelected: this.multiChoiceGroupSelected,
                multiSelectGroupChange: this.multiSelectGroupChange,
            };
        }
    }

    // We are doing this because of the two-way data bound nature of AssessmentsCtrl...
    // It's a temporary solution while AssessmentsCtrl continues to be componentized, refactored, and migrates to Redux state mgmt.
    public updateSectionsReference(updatedQuestion: IQuestion): void {
        this.section.Questions = map(this.section.Questions, (question: IQuestion): IQuestion => {
            if (question.Id === updatedQuestion.Id) {
                return updatedQuestion;
            } else {
                return question;
            }
        });
    }

    private multiChoiceGroupSelected = (x, y, z, value: number): boolean => {
        if (!isUndefined(value) && isArray(this.question.Answer)) {
            const filteredAnswers: any[] = this.$filter("filter")(this.question.Answer, value.toString(), true);
            return Boolean(filteredAnswers && filteredAnswers.length);
        }
        return false;
    }

    private multiSelectGroupChange = (x, y, z, value: number): void => {
        this.question.ResponseType = QuestionResponseTypes.Reference;
        if (isArray(this.question.Answer) && this.question.Answer.length > 0) {
            const valueIndex: number = this.question.Answer.indexOf(value.toString());
            if (valueIndex < 0) {
                this.question.Answer.push(value.toString());
            } else {
                this.question.Answer.splice(valueIndex, 1);
            }
        } else {
            this.question.Answer = [value.toString()];
        }
        this.question.NotApplicable = false;
        this.question.isAnswered = (this.question.Answer && this.question.Answer.length) || (this.question.OtherAnswer && this.question.OtherAnswer.length);
    }

    private resetOther = (): void => {
        this.question.OtherAnswer = null;
    }

    private handleInventorySelect = (sectionId: string, question: IQuestion, response: any): void => {
        if (response && response.IsPlaceholder) {
            question.Answer = response.InputValue;
        }
        if (response && response.ExistingMatch) {
            OneConfirm(this.$rootScope.t("CreatingNewApplication"), this.$rootScope.t("CreateNewAppWithSameName"), "Confirm", "Cancel", true, this.$rootScope.t("CreateNew"), this.$rootScope.t("SelectExisting"))
                .then((result: boolean) => {
                    if (isUndefined(result) || result === null) {
                        question.Answer = null;
                    } else if (!result) {
                        question.Answer = response.ExistingMatch;
                    }
                    this.events.inventorySelect(sectionId, question, question.Answer);
                });
        } else {
            this.events.inventorySelect(sectionId, question, question.Answer);
        }
    }

    private updateQuestion(action: IAssessmentQuestionAction): void {
        let questionAction: IAssessmentQuestionAction;
        switch (action.type) {
            case ACTIONS.YES_SELECTED:
            case ACTIONS.NO_SELECTED:
                this.question = this.assessmentQuestionValidation.validateYesNo(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.YES_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.NOT_SURE_SELECTED:
                this.question = this.assessmentQuestionValidation.toggleNotSure(this.question);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.NOT_SURE_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.NOT_APPLICABLE_SELECTED:
                this.question = this.assessmentQuestionValidation.toggleNotApplicable(this.question);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.NOT_APPLICABLE_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.JUSTIFICATION_UPDATED:
                this.question = this.assessmentQuestionValidation.validateJustification(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.JUSTIFICATION_UPDATED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.DATE_SELECTED:
                this.question = this.assessmentQuestionValidation.validateDate(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.DATE_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.MULTISELECT_OPTION_SELECTED:
                this.question = this.assessmentQuestionValidation.validateMultiselect(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.MULTISELECT_OPTION_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.MULTICHOICE_OPTION_SELECTED:
                this.question = this.assessmentQuestionValidation.validateMultichoice(this.question, action.value, action.name);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.MULTICHOICE_OPTION_SELECTED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.OTHER_OPTION_UPDATED:
                this.question = this.assessmentQuestionValidation.validateOtherOption(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.OTHER_OPTION_UPDATED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            case ACTIONS.TEXTBOX_UPDATED:
                this.question = this.assessmentQuestionValidation.validateTextbox(this.question, action.value);
                this.updateSectionsReference(this.question);
                questionAction = { type: ACTIONS.TEXTBOX_UPDATED, question: this.question };
                this.onQuestionUpdate({ questionAction });
                return;
            default:
                invariant(false, "assessment-question-content.component - Assessment question content action not handled.");
        }
    }
}

export default {
    bindings: {
        currentUser: "<",
        events: "<",
        isDisabled: "<",
        question: "<",
        section: "<",
        project: "<",
        questionUniqId: "<?",
        questionTextModifier: "@?",
        viewState: "<",
        onQuestionUpdate: "&",
    },
    controller: AssessmentQuestionContentController,
    template: `
        <div class="question" ng-class="{ 'disabled': $ctrl.isDisabled }">
            <assessment-question-header
                ng-if="$ctrl.viewState.canViewQuestionName"
                class="question__headline main"
                question="$ctrl.question"
                show-hint="$ctrl.viewState.canViewQuestionHint">
            </assessment-question-header>
            <p
                class="question__text ql-editor padding-all-0 quill-contents-container-{{$ctrl.questionUniqId}} question__text--{{$ctrl.questionTextModifier}}"
                ng-if="$ctrl.viewState.canViewQuestionDescription"
                ng-bind-html="$ctrl.question.Text">
            </p>
            <fieldset>
                <div
                    class="question__content ql-editor padding-all-0 margin-top-bottom-1"
                    ng-if="$ctrl.viewState.canViewQuestionStatement"
                    ng-bind-html="$ctrl.question.Content">
                </div>

                <assessment-question-textbox
                    class="question__content margin-bottom-1"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.viewState.canViewQuestionTextbox"
                    question="$ctrl.question"
                    on-update="$ctrl.updateQuestion(actionType)">
                </assessment-question-textbox>

                <div
                    class="question__multi"
                    ng-if="$ctrl.viewState.canViewQuestionMultichoice">

                    <assessment-question-multichoice
                        class="question__multichoice"
                        is-disabled="$ctrl.isDisabled"
                        ng-if="!$ctrl.question.MultiSelect"
                        ng-required="$ctrl.question.Required"
                        project="$ctrl.project"
                        question="$ctrl.question"
                        on-update="$ctrl.updateQuestion(actionType)">
                    </assessment-question-multichoice>

                    <assessment-question-multiselect
                        class="question__multiselect"
                        is-disabled="$ctrl.isDisabled"
                        ng-if="$ctrl.question.MultiSelect && $ctrl.question.DisplayTypeId !== $ctrl.QuestionMultiChoiceTypes.Dropdown"
                        project="$ctrl.project"
                        question="$ctrl.question"
                        on-update="$ctrl.updateQuestion(actionType)">
                    </assessment-question-multiselect>

                    <assessment-question-multiselect-dropdown
                        ng-if="$ctrl.question.MultiSelect && $ctrl.question.DisplayTypeId === $ctrl.QuestionMultiChoiceTypes.Dropdown"
                        question="$ctrl.question"
                        read-only="$ctrl.isDisabled"
                        callback="$ctrl.updateQuestion(actionType)">
                    </assessment-question-multiselect-dropdown>

                </div>

                <assessment-question-yes-no
                    class="question__yesNo"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.viewState.canViewQuestionYesNo"
                    question="$ctrl.question"
                    project="$ctrl.project"
                    on-update="$ctrl.updateQuestion(actionType)">
                </assessment-question-yes-no>

                <assessment-question-date
                    class="question__date"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.viewState.canViewQuestionDate"
                    project="$ctrl.project"
                    question="$ctrl.question"
                    on-update="$ctrl.updateQuestion(actionType)">
                </assessment-question-date>

                <assessment-question-inventory
                    class="question__inventory"
                    handle-inventory-select="$ctrl.handleInventorySelect"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.question.Type === $ctrl.QuestionTypes.Application || \
                            $ctrl.question.Type === $ctrl.QuestionTypes.Location || \
                            $ctrl.question.Type === $ctrl.QuestionTypes.User || \
                            $ctrl.question.Type === $ctrl.QuestionTypes.Provider"
                    project="$ctrl.project"
                    question="$ctrl.question"
                    on-update="$ctrl.updateQuestion(actionType)">
                </assessment-question-inventory>

                <assessment-question-data-element
                    class="question__multi"
                    events="$ctrl.events"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.question.Type === $ctrl.QuestionTypes.DataElement"
                    question="$ctrl.question"
                    reset-other="$ctrl.resetOther()"
                    section="$ctrl.section"
                    on-update="$ctrl.updateQuestion(actionType)">
                </assessment-question-data-element>

                <div class="margin-top-bottom-1" ng-if="$ctrl.viewState.canViewQuestionJustification">
                    <assessment-question-justification
                        class="question__justification"
                        is-disabled="$ctrl.isDisabled"
                        is-required="$ctrl.question.RequireJustification"
                        project-state="$ctrl.project.StateDesc"
                        question="$ctrl.question"
                        on-update="$ctrl.updateQuestion(actionType)">
                    </assessment-question-justification>
                </div>

                <script type="text/ng-template" id="inventoryItemTemplate.html">
                    <a class="question__inventory-item">
                        <span ng-bind-html="match.label || '(' + $root.t('NoAnswer') + ')' | uibTypeaheadHighlight:query"></span>
                    </a>
                </script>

            </fieldset>
        </div>
    `,
};
