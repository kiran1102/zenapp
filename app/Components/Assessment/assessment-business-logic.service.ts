import { isNil, split, isNull, filter, find } from "lodash";
import { getQuestionApprovalEnabled } from "oneRedux/reducers/setting.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

import { QuestionStates, QuestionTypes, QuestionResponseTypes } from "enums/assessment-question.enum";
import { RiskStates } from "enums/risk-states.enum";

import { EmptyGuid } from "constants/empty-guid.constant";
import { AssessmentStates } from "constants/assessment.constants";

import { IStore } from "interfaces/redux.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRisk } from "interfaces/risk.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";

export default class AssessmentBusinessLogic {

    static $inject: string[] = ["store", "$rootScope", "Permissions"];

    private questionPermissions;

    constructor(
        private store: IStore,
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
    ) {}

    public isApproverOrHasFullAccess(projectModel: IProject): boolean {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        return projectModel.ReviewerId === currentUser.Id || this.permissions.canShow("ProjectsFullAccess");
    }

    public isThresholdAndFirstTemplate(projectModel: IProject): boolean {
        return projectModel.IsLinked && !isNull(projectModel.NextTemplateName);
    }

    public isThresholdAndSecondTemplate(projectModel: IProject): boolean {
        return projectModel.IsLinked && isNull(projectModel.NextTemplateName);
    }

    public isMultiRespondent(projectModel: IPiaAssessment): boolean {
        const respondents: string[] = split(projectModel.Lead, ",");
        return respondents.length > 1;
    }

    public getAssignedSections(projectModel: IPiaAssessment): ISection[] {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const isMultiRespondent: boolean = this.isMultiRespondent(projectModel);
        const isApprover: boolean = currentUser.Id === projectModel.ReviewerId;
        const assignedSections = filter(projectModel.Sections, (section: ISection): boolean => {
            if (!section.isWelcomeSection && section.Assignment) {
                return section.Assignment.AssigneeId === currentUser.Id;
            }
            return false;
        });

        if (currentUser.Id === projectModel.ReviewerId || this.permissions.canShow("ProjectsFullAccess")) {
            return filter(projectModel.Sections, (section: ISection): boolean => section.Id !== EmptyGuid);
        } else {
            return filter(projectModel.Sections, (section: ISection): boolean => {
                if (!section.isWelcomeSection && section.Assignment) {
                    return section.Assignment.AssigneeId === currentUser.Id;
                }
                return false;
            });
        }
    }

    public getSections(projectModel: IPiaAssessment): {isMultiRespondent: boolean, assignedSections: ISection[], assignedSectionsLength: number} {
        const isMultiRespondent: boolean = this.isMultiRespondent(projectModel);
        const assignedSections: ISection[] = this.getAssignedSections(projectModel);
        const assignedSectionsLength: number = assignedSections.length;
        return { isMultiRespondent, assignedSections, assignedSectionsLength };
    }

    public allRequiredQuestionsAreAnswered(projectModel: IProject): boolean {
        const { Unanswered, Skipped } = QuestionStates;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const question: IQuestion = section.Questions[j];
                if (question.Required
                    && question.State !== Skipped
                    && (question.State === Unanswered || !question.State)) {
                    return false;
                }
            }
        }
        return true;
    }

    public allRequiredSectionQuestionsAnswered(currentSection: ISection): boolean {
        const { Unanswered, Skipped } = QuestionStates;
        const questionLength: number = currentSection.Questions.length;

        for (let j = 0; j < questionLength; j++) {
            const question: IQuestion = currentSection.Questions[j];
            if (question.Required
                && question.State !== Skipped
                && (question.State === Unanswered || !question.State)) {
                return false;
            }
        }
        return true;
    }

    public allRequiredSectionJustificationsAnswered(currentSection: ISection): boolean {
        const { Unanswered, Skipped } = QuestionStates;
        const questionLength: number = currentSection.Questions.length;

        for (let j = 0; j < questionLength; j++) {
            const question: IQuestion = currentSection.Questions[j];
            if (question.State !== Skipped
                && question.AllowJustification
                && question.RequireJustification
                && !this.questionHasJustification(question)) {
                return false;
            }
        }
        return true;
    }

    public isApproverAndOrRespondent(projectModel: IProject, currentSection: ISection): boolean {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const isApprover: boolean = this.isApproverOrHasFullAccess(projectModel);
        const isRespondent: boolean = currentSection.Assignment ? (currentUser.Id === currentSection.Assignment.AssigneeId) : false;
        return (isRespondent && isApprover) || isRespondent;
    }

    public isNotRespondentOfAnySection(projectModel: IProject) {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const sectionsLength: number = projectModel.Sections.length;

        for (let j = 0; j < sectionsLength; j++) {
            const section: ISection = projectModel.Sections[j];
            if ((section.Id !== EmptyGuid
                && currentUser.Id === section.Assignment.AssigneeId)) {
                return false;
            }
        }
        return true;
    }

    public allRequiredJustificationsAreAnswered(projectModel: IProject): boolean {
        const { Answered, Skipped } = QuestionStates;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const question: IQuestion = section.Questions[j];
                if (question.State !== Skipped
                    && question.AllowJustification
                    && question.RequireJustification
                    && !this.questionHasJustification(question)) {
                    return false;
                }
            }
        }
        return true;
    }

    public allQuestionsAreAccepted(projectModel: IProject): boolean {
        const { Accepted, Skipped } = QuestionStates;
        const { Content } = QuestionTypes;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const question: IQuestion = section.Questions[j];
                if (question.State !== Skipped
                    && question.Type !== Content
                    && question.State !== Accepted
                ) {
                    return false;
                }
            }
        }
        return true;
    }

    public allQuestionsChangedOrNoChanged(projectModel: IProject): boolean {
        const { Skipped } = QuestionStates;
        const { Content } = QuestionTypes;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const question: IQuestion = section.Questions[j];
                if (!question.IsFromCurrentVersion
                    && question.State !== Skipped
                    && question.Type !== Content
                    && isNil(question.HasVersionResponseChanged)
                ) {
                    return false;
                }
            }
        }
        return true;
    }

    public allSectionQuestionsChangedOrNoChanged(currentSection: ISection): boolean {
        const { Skipped } = QuestionStates;
        const { Content } = QuestionTypes;

        for (const question of currentSection.Questions) {
            if (!question.IsFromCurrentVersion
                && question.State !== Skipped
                && question.Type !== Content
                && isNil(question.HasVersionResponseChanged)
            ) {
                return false;
            }
        }
        return true;
    }

    public someQuestionsNeedMoreInfo(projectModel: IProject): boolean {
        const { InfoRequested } = QuestionStates;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const question: IQuestion = section.Questions[j];
                if (question.State === InfoRequested) {
                    return true;
                }
            }
        }
        return false;
    }

    public someSectionQuestionsNeedMoreInfo(currentSection: ISection): boolean {
        const { InfoRequested } = QuestionStates;
        return Boolean(find(currentSection.Questions, (question: IQuestion): boolean => {
            return question.State === InfoRequested;
        }));
    }

    public allSectionsAreUnderReview(projectModel: IProject): boolean {
        const { UnderReview } = AssessmentStates;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            if (section.StateDesc !== UnderReview) {
                return false;
            }
        }
        return true;
    }

    public multiRespondentProjectsAllUnderReview(projectModel: IProject): boolean {
        return this.isMultiRespondent(projectModel) ? this.allSectionsAreUnderReview(projectModel) : true;
    }

    public flaggedRiskExists(projectModel: IProject): boolean {
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                if (section.Questions[j].Risk) {
                    return true;
                }
            }
        }
        return false;
    }

    public questionApprovalOnAndQuestionsAccepted(projectModel: IProject): boolean {
        const questionApprovalEnabled: boolean = getQuestionApprovalEnabled(this.store.getState());
        if (!questionApprovalEnabled) {
            return true;
        } else {
            return questionApprovalEnabled && this.allQuestionsAreAccepted(projectModel);
        }
    }

    public isReadyToApprove(projectModel: IProject): boolean {
        return this.allRequiredQuestionsAreAnswered(projectModel)
            && this.allRequiredJustificationsAreAnswered(projectModel)
            && this.questionApprovalOnAndQuestionsAccepted(projectModel);
    }

    public allRisksHaveRecommendation(projectModel: IProject): boolean {
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const riskModel: IRisk = section.Questions[j].Risk;
                if (riskModel && riskModel.State !== RiskStates.Analysis) {
                    return false;
                }
            }
        }
        return true;
    }

    public allRisksAreReducedOrRetained(projectModel: IProject): boolean {
        const { Retained, Reduced } = RiskStates;
        const { assignedSections, assignedSectionsLength } = this.getSections(projectModel);

        for (let i = 0; i < assignedSectionsLength; i++) {
            const section: ISection = assignedSections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                const riskModel: IRisk = section.Questions[j].Risk;
                if (riskModel
                    && riskModel.State !== Reduced
                    && riskModel.State !== Retained) {
                    return false;
                }
            }
        }
        return true;
    }

    public isCurrentUserAbleToSubmitCurrentSection(currentSection: ISection): boolean {
        const currentUser: IUser = getCurrentUser(this.store.getState());

        return (currentSection.Id !== EmptyGuid
            && currentSection.Assignment.AssigneeId === currentUser.Id
            && currentSection.StateDesc === AssessmentStates.InProgress
            && this.allRequiredSectionQuestionsAnswered(currentSection)
            && this.allRequiredSectionJustificationsAnswered(currentSection));
    }

    public isRiskOwner(projectModel: IPiaAssessment): boolean {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        const sectionLength: number = projectModel.Sections.length;

        for (let i = 0; i < sectionLength; i++) {
            const section: ISection = projectModel.Sections[i];
            const questionLength: number = section.Questions.length;
            for (let j = 0; j < questionLength; j++) {
                if (section.Questions[j].Risk) {
                    const riskOwnerId: string = section.Questions[j].Risk.RiskOwnerId;
                    return currentUser.Id === riskOwnerId;
                }
            }
        }
    }

    private questionHasJustification(question: IQuestion): boolean {
        const responseLength: number = question.Responses.length;
        if (responseLength) {
            for (let i = 0; i < responseLength; i++) {
                if (question.Responses[i].Type === QuestionResponseTypes.Justification
                    && Boolean(question.Responses[i].Value)
                ) {
                    return true;
                }
            }
        }
        return false;
    }

}
