import { includes, find } from "lodash";

import { AssessmentStates, AssessmentPermissions } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";

import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IAssessmentQuestionPermission } from "interfaces/assessment.interface";

export default class QuestionPermissionService {
    static $inject: string[] = ["Permissions"];

    private assessmentQuestionPermissions;

    constructor(private permissions: Permissions) {

        const { InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment } = AssessmentStates;
        const { approver, respondent } = AssignmentTypes;

        this.assessmentQuestionPermissions = {
            canViewQuestion: {
                allowedPermission:          AssessmentPermissions.AssessmentQuestionView,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    ["all"],
            },
            canEditQuestion: {
                allowedPermission:          AssessmentPermissions.AssessmentQuestionEdit,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview],
            },
            canViewQuestionComments: {
                allowedPermission:          AssessmentPermissions.CommentsQuestionRead,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    ["all"],
            },
            canAddQuestionComments: {
                allowedPermission:          AssessmentPermissions.CommentsQuestionCreate,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    ["all"],
            },
            canViewQuestionAttachments: {
                allowedPermission:          AssessmentPermissions.AttachmentsRead,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    ["all"],
            },
            canAddQuestionAttachments: {
                allowedPermission:          AssessmentPermissions.AttachmentsCreate,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
            },
            canDeleteQuestionAttachments: {
                allowedPermission:          AssessmentPermissions.AttachmentsDelete,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
            },
            canViewQuestionHistory: {
                allowedPermission:          AssessmentPermissions.QuestionHistory,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    ["all"],
            },
        };
    }

    public hasQuestionEditPermission(permissionToCheck: string, currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        const config: IAssessmentQuestionPermission = this.assessmentQuestionPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAllowedUserEdit(config.allowedRoles, currentUser, projectModel, questionModel))
            && this.isAllowedState(config.allowedAssessmentStates, projectModel.StateDesc);
    }

    public hasQuestionViewPermission(permissionToCheck: string, currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        const config: IAssessmentQuestionPermission = this.assessmentQuestionPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAllowedUserView(config.allowedRoles, currentUser, projectModel, questionModel))
            && this.isAllowedState(config.allowedAssessmentStates, projectModel.StateDesc);
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return stateArray[0] === "all" || includes(stateArray, state);
    }

    private isAllowedUserView(permissionArray: string[], user: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        let isApprover = false;
        let isRespondent = false;

        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId === user.Id;
            } else if (permission === AssignmentTypes.respondent) {
                const currentSection: ISection = find(projectModel.Sections, { Id: questionModel.SectionId });
                isRespondent = currentSection.Assignment.AssigneeId === user.Id;
            }
        }
        return isApprover || isRespondent;
    }

    private isAllowedUserEdit(permissionArray: string[], user: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        let isApprover = false;
        let isRespondent = false;
        let currentSection: ISection;

        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId === user.Id;
            } else if (permission === AssignmentTypes.respondent) {
                currentSection = find(projectModel.Sections, { Id: questionModel.SectionId });
                isRespondent = currentSection.Assignment.AssigneeId === user.Id;
            }
        }

        if (projectModel.StateDesc === AssessmentStates.UnderReview
            || currentSection.StateDesc === AssessmentStates.UnderReview) {
            return isApprover;
        } else {
            return isApprover || isRespondent;
        }
    }

}
