import { includes, find, filter, split } from "lodash";
import Sections from "oneServices/section.service";
import AssessmentBusinessLogic from "generalcomponent/Assessment/assessment-business-logic.service";

import { AssessmentStates, AssessmentPermissions } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";

import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import { IAssessmentQuestionPermission } from "interfaces/assessment.interface";

export default class AssessmentFooterPermissionService {
    static $inject: string[] = ["Permissions", "AssessmentBusinessLogic", "Sections"];

    private assessmentFooterPermissions;

    constructor(
        private permissions: Permissions,
        private assessmentBusinessLogic: AssessmentBusinessLogic,
        private sections: Sections,
    ) {
        const { InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment } = AssessmentStates;
        const { AssessmentSubmit, AssessmentRequestInfo, AssessmentProvideMoreInfo, AssessmentNextSteps, AssessmentRiskManagement, AssessmentApprove } = AssessmentPermissions;
        const { approver, respondent } = AssignmentTypes;

        this.assessmentFooterPermissions = {
            canViewSubmitButton: {
                allowedPermission:          AssessmentSubmit,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InProgress],
            },
            canViewSendBackButton: {
                allowedPermission:          AssessmentRequestInfo,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
            },
            canViewNeedsInfoSubmitButton: {
                allowedPermission:          AssessmentProvideMoreInfo,
                allowedRoles:               [approver, respondent],
                allowedAssessmentStates:    [InfoNeeded],
            },
            canViewNextStepsButton: {
                allowedPermission:          AssessmentNextSteps,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
            },
            canViewContinueToRiskTrackingButton: {
                allowedPermission:          AssessmentRiskManagement,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview],
            },
            canViewSendRecommendationsButton: {
                allowedPermission:          AssessmentRiskManagement,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [RiskAssessment],
            },
            canViewApproveButton: {
                allowedPermission:          AssessmentApprove,
                allowedRoles:               [approver],
                allowedAssessmentStates:    [UnderReview, RiskAssessment, RiskTreatment],
            },
        };
    }

    public hasViewPermission(permissionToCheck: string, currentUser: IUser, projectModel: IProject): boolean {
        const config: IAssessmentQuestionPermission = this.assessmentFooterPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAllowedUser(config.allowedRoles, currentUser, projectModel))
            && this.isAllowedAssessmentState(config.allowedAssessmentStates, currentUser, projectModel)
            && this.isLatestVersion(projectModel);
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedAssessmentState(assessmentStateArray: string[], currentUser: IUser, projectModel: IProject): boolean {
        const { isMultiRespondent } = this.assessmentBusinessLogic.getSections(projectModel);
        if (isMultiRespondent
            && currentUser.Id !== projectModel.ReviewerId
            && !this.permissions.canShow("ProjectsFullAccess")
        ) {
            const { assignedSections, assignedSectionsLength } = this.assessmentBusinessLogic.getSections(projectModel);
            for (let i = 0; i < assignedSectionsLength; i++) {
                if (!includes(assessmentStateArray, assignedSections[i].StateDesc)) {
                    return false;
                }
            }
        }
        return includes(assessmentStateArray, projectModel.StateDesc);
    }

    private isAllowedUser(permissionArray: string[], user: IUser, projectModel: IProject): boolean {
        let isApprover: boolean;
        let isRespondent: boolean;
        const { assignedSections, assignedSectionsLength } = this.assessmentBusinessLogic.getSections(projectModel);

        for (const permission of permissionArray) {
            if (permission === AssignmentTypes.approver) {
                isApprover = projectModel.ReviewerId === user.Id;
            } else if (permission === AssignmentTypes.respondent) {
                for (let index = 0; index < assignedSectionsLength; index++) {
                    if (assignedSections[index].Assignment.AssigneeId === user.Id) {
                        isRespondent = true;
                    }
                }
            }
        }

        return isApprover || isRespondent;
    }

    private isLatestVersion(projectModel: IProject): boolean {
        return projectModel.IsLatestVersion;
    }

}
