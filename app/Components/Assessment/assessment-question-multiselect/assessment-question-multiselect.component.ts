import _ from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionResponseTypes } from "enums/assessment-question.enum";

class AssessmentQuestionMultiselectController {

    static $inject: string[] = ["$rootScope", "Permissions"];

    private question: IQuestion;
    private onUpdate: ({}) => void;

    private QuestionResponseTypes: any;

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.question) this.question = _.cloneDeep(this.question);
    }

    private sendAction(type: string, value?: any): void {
        let actionType: IAssessmentQuestionAction = { type };
        if (value) {
            actionType = {...actionType, value};
        }
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        question: "<",
        isDisabled: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionMultiselectController,
    template: `
    <div class="question__multiselect">
        <p ng-if="$ctrl.question.Options.length">{{::$root.t("SelectAllThatApply")}}</p>
        <div class="btn-group">
            <div
                class="option-container"
                ng-if="$ctrl.question.Options.length && option.Name"
                ng-repeat="option in $ctrl.question.Options track by $index">
                <button
                    class="one-button one-button--select"
                    ng-model="option.Answer"
                    ng-class="{'selected': $ctrl.question.Answer.indexOf(option.Value) >= 0}"
                    ng-click="$ctrl.sendAction('MULTISELECT_OPTION_SELECTED', option);"
                    ng-disabled="$ctrl.isDisabled"
                    tooltip-append-to-body="true"
                    tooltip-class="one-dropdown__tooltip--offset-left"
                    tooltip-enable="$ctrl.question.OptionHintEnabled"
                    tooltip-placement="top-left"
                    uib-btn-checkbox=""
                    uib-tooltip="{{option.Hint}}">
                    {{option.Name}}
                </button>
            </div>

            <assessment-question-other-option-button
                class="question__multiselect-other"
                is-disabled="$ctrl.isDisabled"
                ng-if="$ctrl.question.AllowOther"
                option-text="$ctrl.question.OtherAnswer"
                on-after-save="$ctrl.sendAction('OTHER_OPTION_UPDATED', value)">
            </assessment-question-other-option-button>

            <div class="option-container">
                <assessment-question-not-sure-button
                    button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                    is-disabled="$ctrl.isDisabled"
                    is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
                    on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                    ng-if="$ctrl.question.AllowNotSure">
                </assessment-question-not-sure-button>
            </div>
            <div class="option-container">
                <assessment-question-not-applicable-button
                    button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                    is-disabled="$ctrl.isDisabled"
                    is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
                    on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                    ng-if="$ctrl.question.AllowNotApplicable">
                </assessment-question-not-applicable-button>
            </div>
        </div>
    </div>
    `,
};
