import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentDataElement, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionTypes, QuestionResponseTypes, QuestionDataElementStringKeys } from "enums/assessment-question.enum";

class AssessmentQuestionDataElementController {

    static $inject: string[] = ["$rootScope", "$filter"];

    private events: any;
    private question: IQuestion;
    private onUpdate: ({}) => void;

    private QuestionResponseTypes: any;
    private QuestionDataElementStringKeys: any;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $filter: ng.IFilterService,
    ) { }

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
        this.QuestionDataElementStringKeys = QuestionDataElementStringKeys;
    }

    private getDataElementStrings(stringKey: number, name: string): string {
        if (stringKey === QuestionDataElementStringKeys.Header) {
            return this.$rootScope.t("PleaseSelectDataElementsGroupsBeingProcessed");
        } else if (stringKey === QuestionDataElementStringKeys.Subheader) {
            return this.$rootScope.t("WhichCategoryDataElementsAreProcessed", { Category: name });
        }
    }

    private getDefaultElementStrings(stringKey: number, name: string): string {
        if (stringKey === QuestionDataElementStringKeys.Header) {
            return this.$rootScope.t("SelectAsManyAsYouWant");
        } else if (stringKey === QuestionDataElementStringKeys.Subheader) {
            return this.$rootScope.t("WhichOptionsAreProcessedByTheName", { Name: name });
        }
    }

    private getGroupName(question: IQuestion, GroupId: string): string {
        if (question && question.Options) {
            const groupItems: IAssessmentDataElement[] = this.$filter("filter")(question.Options, { GroupId });
            if (groupItems && groupItems.length) {
                return groupItems[0].Group;
            }
        }
        return "";
    }

    private getQuestionString = (stringType: number, name: string): string => {
        if (this.question.Type === QuestionTypes.DataElement) {
            return this.getDataElementStrings(stringType, name);
        }
        return this.getDefaultElementStrings(stringType, name);
    }

    private sendAction(type: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        isDisabled: "<",
        events: "<",
        question: "<",
        section: "<",
        resetOther: "&",
        onUpdate: "&",
    },
    controller: AssessmentQuestionDataElementController,
    template: `
        <div
            class="question__multiselect"
            ng-if="$ctrl.question.Options"
            ng-repeat="(GroupId, GroupOptions) in Options = ($ctrl.question.Options | ItemsNotIn:'GroupId':$ctrl.question.Filters | groupBy: 'GroupId') track by $index"
            ng-required="$ctrl.question.Required">
            <div class="question__body">
                <div class="question__numbers">
                    <div class="question__numbers-number" ng-if="$ctrl.question.Number && !$ctrl.section.IsLocked">
                        {{$ctrl.question.Number}}.{{$index + 1}}
                    </div>
                    <div class="question__numbers-number" ng-if="!$ctrl.question.Number && !$ctrl.section.IsLocked">
                        {{$index + 1}}
                    </div>
                    <div
                        class="question__numbers-number"
                        ng-if="$ctrl.section.IsLocked"
                        tooltip-append-to-body="true"
                        tooltip-placement="top"
                        tooltip-popup-close-delay="200"
                        tooltip-trigger="mouseenter"
                        uib-tooltip="{{::$root.t('QuestionSubmittedNoLongerEditable')}}">
                        {{$ctrl.question.Number}}.{{$index + 1}}
                        <i class="fa fa-lock"></i>
                    </div>
                </div>

                <div class="question__detail">
                    <div class="question__headline">
                        <div class="question__headline--text">
                            {{::$ctrl.getGroupName($ctrl.question, GroupId)}}
                        </div>
                    </div>
                </div>
            </div>

            <p class="question__subheadline">
                {{::$ctrl.getQuestionString($ctrl.QuestionDataElementStringKeys.Subheader, $ctrl.getGroupName($ctrl.question, GroupId))}}
            </p>

            <div class="btn-group">
                <div
                    class="option-container"
                    ng-if="GroupOptions.length && option.Name"
                    ng-repeat="option in (GroupOptions | filter:{Value: ''}) track by $index">
                    <button
                        class="one-button one-button--select"
                        ng-class="{'selected': $ctrl.events.multiChoiceGroupSelected($ctrl.question, option)}"
                        ng-click="$ctrl.events.multiSelectGroupChange($ctrl.question, $ctrl.QuestionResponseTypes.Reference, option, GroupId);">
                        {{option.Name}}
                    </button>
                </div>

                <div
                    class="question__multiselect-other"
                    ng-if="$ctrl.question.AllowOther"
                    ng-repeat="OtherOption in ($ctrl.question.Responses | filter:{Type: $ctrl.QuestionResponseTypes.Other, FieldId: GroupId}) track by $index">
                    <button
                        class="one-button one-button--select"
                        e-placeholder="{{::$root.t('EnterYourOtherAnswerHere')}}"
                        editable-text="OtherOption.Value"
                        ng-class="{'selected': OtherOption.Value, 'review': OtherOption.Value}"
                        onaftersave="$ctrl.events.multiSelectGroupChange($ctrl.question, $ctrl.QuestionResponseTypes.Other, OtherOption, GroupId)"
                        oncancel="">
                        {{ OtherOption.Value || 'Other' }}
                    </button>
                </div>

                <div class="question__multiselect-other" ng-if="$ctrl.question.AllowOther">
                    <button
                        class="one-button one-button--select"
                        e-placeholder="{{::$root.t('EnterYourOtherAnswerHere')}}"
                        editable-text="$ctrl.question.OtherAnswer"
                        onaftersave="$ctrl.events.multiSelectGroupChange($ctrl.question, $ctrl.QuestionResponseTypes.Other, {Value: $ctrl.question.OtherAnswer}, GroupId); $ctrl.resetOther();"
                        oncancel="">
                        {{ $root.t('Other') }}
                    </button>
                </div>
            </div>
        </div>

        <div class="question__multiselect question__multiselect--separated">
            <div class="btn-group">
                <assessment-question-not-sure-button
                    button-class="margin-right-1 margin-top-bottom-1 option-container"
                    is-disabled="$ctrl.isDisabled"
                    is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
                    on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                    ng-if="$ctrl.question.AllowNotSure">
                </assessment-question-not-sure-button>
                <assessment-question-not-applicable-button
                    button-class="margin-right-1 margin-top-bottom-1 option-container"
                    is-disabled="$ctrl.isDisabled"
                    is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
                    on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                    ng-if="$ctrl.question.AllowNotApplicable">
                </assessment-question-not-applicable-button>
            </div>
        </div>
    `,
};
