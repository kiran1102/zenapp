import * as _ from "lodash";
import Utilities from "Utilities";

class DMAssessmentTableController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "ENUMS", "OneAssessmentList"];

    private table: any;
    private SortTypes: any;
    private settings: any;
    private sortColumn: string;
    private sortDirection: number;
    private sortString: string;
    private filterOpen: boolean;

    constructor(readonly $scope: any, ENUMS: any, readonly OneAssessmentList: any) {
        this.SortTypes = ENUMS.SortTypes;
    }

    $onInit() {
        this.startEventWatchers();
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (_.isArray(this.table.sort) && this.table.sort.length) {
            this.sortColumn = this.table.sort[0].property;
            this.sortDirection = this.table.sort[0].ascending ? this.SortTypes.Ascending : this.SortTypes.Descending;
        } else {
            this.sortColumn = "";
            this.sortDirection = null;
        }
    }

    toggleSortType(sortColumn: string, noToggle: boolean = false, page: number, size: number): void {
        if (sortColumn) {
            if (this.sortColumn !== sortColumn) {
                this.sortColumn = sortColumn;
                this.sortDirection = null;
            }
            this.sortDirection = this.getSortDir(this.sortDirection);
            const order: string = (this.sortDirection === this.SortTypes.Ascending) ? "ASC" : "DESC";
            this.sortString = `${this.sortColumn},${order}`;
            this.$scope.$emit("SORT_ASSESSMENT", this.sortString);
        }
    }

    private startEventWatchers(): void {
        this.$scope.$on("DELETE_ASSESSMENT", (event: ng.IAngularEvent, assessmentId: string): void => {
            const assessmentIndex: number = _.findIndex(this.table.content, ["assessmentId", assessmentId]);
            if (assessmentIndex > -1) this.table.content[assessmentIndex].isDisabled = true;
        });
    }

    private getSortDir(sortDirection: number): number {
        return (sortDirection === this.SortTypes.Ascending) ? this.SortTypes.Descending : this.SortTypes.Ascending;
    }
}

const dmAssessmentTableComponent: ng.IComponentOptions = {
    bindings: {
        table: "<",
        filterOpen: "<?",
    },
    controller: DMAssessmentTableController,
    template: `
        <section class="dm-assessment-table flex-full-height">
            <one-table
                class="flex-full-height"
                wrapper-class="overflow-x-auto block flex-shrink"
                >
                <one-table-header>
                    <one-table-row row-class="one-table-row--header">
                        <one-table-header-cell
                            ng-repeat="cell in $ctrl.table.tableConfig.cells track by $index"
                            size="cell.size"
                            is-required="cell.required"
                            is-sorted="cell.sortKey === $ctrl.sortColumn"
                            sort-direction="$ctrl.sortDirection"
                            ng-click="!cell.sortable || $ctrl.toggleSortType(cell.sortKey)"
                            wrapper-class="{{::cell.classes ? cell.classes : ''}}"
                            >
                                {{cell.text}}
                        </one-table-header-cell>
                    </one-table-row>
                </one-table-header>
                <one-table-body
                    class="dm-assessment-table__body block overflow-y-auto overflow-x-hidden"
                    ng-class="{'dm-assessment-table__body--filter-open' : $ctrl.filterOpen }"
                    >
                    <one-table-row
                        row-class="one-table-row"
                        is-disabled="assessment.isDisabled"
                        ng-repeat="assessment in $ctrl.table.content track by $index"
                        >
                        <one-table-cell
                            ng-repeat="cell in $ctrl.table.tableConfig.cells track by $index"
                            size="cell.size"
                            >
                            <one-table-cell-content>
                                <dm-assessment-table-cell
                                    cell="cell"
                                    assessment="assessment"
                                    disabled="assessment.isDisabled"
                                    >
                                </dm-assessment-table-cell>
                            </one-table-cell-content>
                        </one-table-cell>
                    </one-table-row>
                </one-table-body>
            </one-table>
        </section>
    `,
};

export default dmAssessmentTableComponent;
