import { includes, find } from "lodash";
import { getQuestionApprovalEnabled } from "oneRedux/reducers/setting.reducer";
import AssessmentBusinessLogic from "generalcomponent/Assessment/assessment-business-logic.service";

import { AssessmentPermissions, AssessmentStates } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";
import { UserRoles } from "constants/user-roles.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

import { IUser } from "interfaces/user.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { ISection } from "interfaces/section.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStore } from "interfaces/redux.interface";

export default class AssessmentSectionHeaderPermissionService {
    static $inject: string[] = ["store", "Permissions", "AssessmentBusinessLogic"];

    private assessmentSectionHeaderPermissions;

    constructor(
        private store: IStore,
        private permissions: Permissions,
        private assessmentBusinessLogic: AssessmentBusinessLogic,
    ) {
        const { InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed } = AssessmentStates;
        const {
            AssessmentDetailsView,
            AssessmentNameEdit,
            AssessmentOrgView,
            AssessmentDeadlineEdit,
            AssessmentDescriptionEdit,
            AssessmentApproverReassign,
            AssessmentRespondentReassign,
        } = AssessmentPermissions;
        const { approver, respondent } = AssignmentTypes;

        this.assessmentSectionHeaderPermissions = {
            canShowAssessmentName: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["welcome"],
            },
            canEditAssessmentName: {
                allowedPermission:          AssessmentNameEdit,
                allowedAssignments:         [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
                disallowedRole:             [UserRoles.Invited],
            },
            canShowAssessmentCreator: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["welcome"],
            },
            canShowAssessmentOrg: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["welcome"],
            },
            canShowAssessmentDeadline: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["welcome"],
            },
            canEditAssessmentDeadline: {
                allowedPermission:          AssessmentDeadlineEdit,
                allowedAssignments:         [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
                disallowedRole:             [UserRoles.Invited],
            },
            canShowAssessmentDescription: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["welcome"],
            },
            canEditAssessmentDescription: {
                allowedPermission:          AssessmentDescriptionEdit,
                allowedAssignments:         [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
                disallowedRole:             [UserRoles.Invited],
            },
            canShowAssessmentApprover: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["all"],
            },
            canReassignAssessmentApprover: {
                allowedPermission:          AssessmentApproverReassign,
                allowedAssignments:         [approver],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment],
                disallowedRole:             [UserRoles.Invited],
            },
            canShowAssessmentRespondent: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["all"],
            },
            canReassignAssessmentRespondent: {
                allowedPermission:          AssessmentRespondentReassign,
                allowedAssignments:         [approver, respondent],
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                disallowedRole:             [UserRoles.Invited],
            },
            canShowSectionStatus: {
                allowedPermission:          AssessmentDetailsView,
                allowedAssessmentStates:    [InProgress, UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
                allowedSections:            ["not welcome"],
            },
        };
    }

    public hasViewPermission(permissionToCheck: string, currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        const config = this.assessmentSectionHeaderPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAssigned(assessment, currentSection))
            && this.isAllowedState(config.allowedAssessmentStates, assessment.StateDesc)
            && this.isAllowedSection(config.allowedSections, currentSection);
    }

    public hasEditPermission(permissionToCheck: string, currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        const config = this.assessmentSectionHeaderPermissions[permissionToCheck];
        if (config.disallowedRole && includes(config.disallowedRole, currentUser.RoleName)) {
            return false;
        }

        return (this.hasPermission(config.allowedPermission)
            || this.isAssigneeAllowedToEdit(config.allowedAssignments, currentUser, assessment, currentSection))
            && this.isAllowedState(config.allowedAssessmentStates, assessment.StateDesc)
            && assessment.IsLatestVersion;
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return includes(stateArray, state);
    }

    private isAssignedToAtLeastOneSection(assessment: IPiaAssessment): boolean {
        const { assignedSectionsLength } = this.assessmentBusinessLogic.getSections(assessment);
        return assignedSectionsLength > 0;
    }

    private isWelcomeSectionAndAssigned(assessment: IPiaAssessment, currentSection: ISection): boolean {
        return currentSection.Id === EmptyGuid && this.isAssignedToAtLeastOneSection(assessment);
    }

    private isAssignedToCurrentSection(assessment: IPiaAssessment, currentSection: ISection): boolean {
        const { assignedSectionsLength, assignedSections } = this.assessmentBusinessLogic.getSections(assessment);
        return Boolean(find(assignedSections, (section) => section.Id === currentSection.Id));
    }

    private isQuestionApprovalSettingMatching(permissionToCompare: boolean | undefined, currentSetting: boolean): boolean {
        if (permissionToCompare === undefined) {
            return true;
        }
        return permissionToCompare === currentSetting;
    }

    private isQuestionApprovalSettingEnabled(): boolean {
        return getQuestionApprovalEnabled(this.store.getState());
    }

    private isAssigned(assessment: IPiaAssessment, currentSection: ISection): boolean {
        return this.isWelcomeSectionAndAssigned(assessment, currentSection)
            || this.isAssignedToCurrentSection(assessment, currentSection);
    }

    private isAllowedSection(allowedSection: string[], currentSection: ISection): boolean {
        if (allowedSection[0] === "all") return true;
        if (allowedSection[0] === "welcome") return currentSection.Id === EmptyGuid;
        if (allowedSection[0] === "not welcome") return currentSection.Id !== EmptyGuid;
    }

    private isAssigneeAllowedToEdit(allowedAssignmentTypes: string[], currentUser: IUser, assessment: IPiaAssessment, currentSection: ISection): boolean {
        let isApprover = false;
        let isRespondent = false;

        for (const allowedType of allowedAssignmentTypes) {
            if (allowedType === AssignmentTypes.approver) {
                isApprover = assessment.ReviewerId === currentUser.Id;
            } else if (allowedType === AssignmentTypes.respondent) {
                isRespondent = this.isAssigned(assessment, currentSection);
            }
        }

        if (assessment.StateDesc === AssessmentStates.UnderReview
            || currentSection.StateDesc === AssessmentStates.UnderReview) {
            return isApprover;
        } else {
            return isApprover || isRespondent;
        }
    }

}
