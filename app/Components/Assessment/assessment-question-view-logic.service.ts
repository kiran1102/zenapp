import { find } from "lodash";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import AssessmentQuestionPermission from "./assessment-question-permission.service";

import { QuestionStates, QuestionTypes } from "enums/assessment-question.enum";

import * as ACTIONS from "generalcomponent/Assessment/assessment-actions.constants";
import { AssessmentStates } from "constants/assessment.constants";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { IProject } from "interfaces/project.interface";
import { IQuestion } from "interfaces/question.interface";
import { IStore } from "interfaces/redux.interface";

export default class AssessmentQuestionViewLogicService {

    static $inject: string[] = ["store", "$rootScope", "AssessmentQuestionPermission"];

    private questionPermissions;

    constructor(
        private readonly store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly AssessmentQuestionPermission: AssessmentQuestionPermission,
    ) {}

    public getCanAddQuestionAttachments(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canAddQuestionAttachments", currentUser, projectModel, questionModel)
            && projectModel.IsLatestVersion;
    }

    public getCanDeleteQuestionAttachments(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canDeleteQuestionAttachments", currentUser, projectModel, questionModel)
            && projectModel.IsLatestVersion;
    }

    public calculateNewViewState(projectModel: IProject, questionModel: IQuestion): any {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        return {
            canViewQuestionNumber: this.getCanViewQuestionNumber(currentUser, projectModel, questionModel),
            canViewQuestionName: this.getCanViewQuestionName(currentUser, projectModel, questionModel),
            canViewQuestionFriendlyName: this.getCanViewQuestionFriendlyName(currentUser, projectModel, questionModel),
            canViewQuestionDescription: this.getCanViewQuestionDescription(currentUser, projectModel, questionModel),
            canViewQuestionHint: this.getCanViewQuestionHint(currentUser, projectModel, questionModel),
            canViewQuestionStatement: this.getCanViewQuestionStatement(currentUser, projectModel, questionModel),
            canViewQuestionTag: this.getCanViewQuestionTag(questionModel),
            canViewQuestionYesNo: this.getCanViewQuestionYesNo(currentUser, projectModel, questionModel),
            canViewQuestionMultichoice: this.getCanViewQuestionMultichoice(currentUser, projectModel, questionModel),
            canViewQuestionTextbox: this.getCanViewQuestionTextbox(currentUser, projectModel, questionModel),
            canViewQuestionDate: this.getCanViewQuestionDate(currentUser, projectModel, questionModel),
            canViewQuestionJustification: this.getCanViewQuestionJustification(currentUser, projectModel, questionModel),
            canViewQuestionVersionLabel: this.getCanViewQuestionVersionLabel(projectModel, questionModel),
            canViewQuestionChangeToggle: this.getCanViewQuestionChangeToggle(projectModel, questionModel),
            questionVersionLabelClass: this.getQuestionVersionLabelClass(questionModel),
            questionVersionLabelText: this.getQuestionVersionLabelTranslationText(questionModel),
            questionTag: this.getQuestionTag(questionModel),
            canViewQuestionActions: this.getCanViewQuestionActions(questionModel),
            isQuestionDisabled: this.getIsQuestionDisabled(currentUser, projectModel, questionModel),
            questionLabelClass: this.getQuestionLabelClass(projectModel, questionModel),
            canViewQuestionComments: this.getCanViewQuestionComments(currentUser, projectModel, questionModel),
            canAddQuestionComments: this.getCanAddQuestionComments(currentUser, projectModel, questionModel),
            canViewQuestionAttachments: this.getCanViewQuestionAttachments(currentUser, projectModel, questionModel),
            canAddQuestionAttachments: this.getCanAddQuestionAttachments(currentUser, projectModel, questionModel),
            canDeleteQuestionAttachments: this.getCanDeleteQuestionAttachments(currentUser, projectModel, questionModel),
            canViewQuestionHistory: this.getCanViewQuestionHistory(currentUser, projectModel, questionModel),
        };
    }

    private getCanViewQuestionNumber(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionName(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionFriendlyName(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return true;
    }

    private getCanViewQuestionDescription(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return Boolean(questionModel.Text)
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionHint(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return Boolean(questionModel.Hint)
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionStatement(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return Boolean(questionModel.Content)
            && questionModel.Type === QuestionTypes.Content
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionTag(questionModel: IQuestion): boolean {
        return questionModel.State === QuestionStates.InfoRequested
            || questionModel.State === QuestionStates.InfoAdded;
    }

    private getQuestionTag(questionModel: IQuestion): any {
        if (questionModel.State !== QuestionStates.InfoRequested
            && questionModel.State !== QuestionStates.InfoAdded) {
            return undefined;
        }
        if (questionModel.State === QuestionStates.InfoRequested) {
            return {
                text: "InformationRequested",
                labelClass: "pia-question__tag--need-info",
            };
        } else if (questionModel.State === QuestionStates.InfoAdded) {
            return {
                text: "InformationAdded",
                labelClass: "pia-question__tag--info-added",
            };
        }
    }

    private getCanViewQuestionYesNo(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return questionModel.Type === QuestionTypes.YesNo
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionMultichoice(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return questionModel.Type === QuestionTypes.Multichoice
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionTextbox(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return questionModel.Type === QuestionTypes.TextBox
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionDate(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return questionModel.Type === QuestionTypes.DateTime
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionJustification(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return (questionModel.AllowJustification || Boolean(questionModel.Justification))
            && this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestion", currentUser, projectModel, questionModel);
    }

    private getIsQuestionDisabled(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return !this.AssessmentQuestionPermission.hasQuestionEditPermission("canEditQuestion", currentUser, projectModel, questionModel)
            || !projectModel.IsLatestVersion
            || find(projectModel.Sections, {Id: questionModel.SectionId}).IsLocked
            || (Boolean(questionModel.versionState) && questionModel.versionState.isDisabled);
    }

    private getCanViewQuestionVersionLabel(projectModel: IProject, questionModel: IQuestion): boolean {
        return questionModel.State === QuestionStates.Answered
            && projectModel.Version > 1
            && questionModel.Type !== QuestionTypes.Content;
    }

    private getQuestionVersionLabelClass(questionModel: IQuestion): string {
        if (questionModel.HasVersionResponseChanged || questionModel.IsFromCurrentVersion) {
            return "needs-attention";
        } else {
            return "";
        }
    }

    private getQuestionVersionLabelTranslationText(questionModel: IQuestion): string {
        if (!questionModel.HasVersionResponseChanged && !questionModel.IsFromCurrentVersion) {
            return this.$rootScope.t("AnswerNotChangedSincePreviousVersion");
        } else if (questionModel.IsFromCurrentVersion) {
            return this.$rootScope.t("QuestionNotAppearOnThePreviousVersion");
        } else if (questionModel.HasVersionResponseChanged && !questionModel.IsFromCurrentVersion) {
            return this.$rootScope.t("AnswerChangedSincePreviousVersion");
        } else {
            return "";
        }
    }

    private getQuestionLabelClass(projectModel: IProject, questionModel: IQuestion): string {
        if (!((questionModel.versionState && questionModel.versionState.isDisabled)
            || (!questionModel.HasVersionResponseChanged && !questionModel.IsFromCurrentVersion))
            && projectModel.StateDesc !== AssessmentStates.InProgress
            && projectModel.Version > 1) {
                return "needs-review";
        }
    }

    private getCanViewQuestionChangeToggle(projectModel: IProject, questionModel: IQuestion): boolean {
        return projectModel.Version > 1
            && projectModel.StateDesc === AssessmentStates.InProgress
            && projectModel.IsLatestVersion
            && questionModel.Type !== QuestionTypes.Content;
    }

    private getCanViewQuestionComments(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestionComments", currentUser, projectModel, questionModel);
    }

    private getCanAddQuestionComments(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canAddQuestionComments", currentUser, projectModel, questionModel)
            && projectModel.IsLatestVersion;
    }

    private getCanViewQuestionAttachments(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestionAttachments", currentUser, projectModel, questionModel);
    }

    private getCanViewQuestionHistory(currentUser: IUser, projectModel: IProject, questionModel: IQuestion): boolean {
        return this.AssessmentQuestionPermission.hasQuestionViewPermission("canViewQuestionHistory", currentUser, projectModel, questionModel)
            && questionModel.Type !== QuestionTypes.DataElement
            && questionModel.GroupId === null;
    }

    private getCanViewQuestionActions(questionModel: IQuestion): boolean {
        return questionModel.Type !== QuestionTypes.Content;
    }

}
