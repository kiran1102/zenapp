// External
import { Subject } from "rxjs";
import { startWith, takeUntil, filter } from "rxjs/operators";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getAllRisksInOrder } from "oneRedux/reducers/risk.reducer";
import { IStore, IStoreState } from "interfaces/redux.interface";

// Services
import AssessmentFooterViewLogic from "../assessment-footer-view-logic.service";

// Interfaces
import { IProject } from "interfaces/project.interface";
import { IUser } from "interfaces/user.interface";
import { IAssessmentSubmitButtonConfig } from "interfaces/assessment.interface";
import { IAssessmentQuestionAction } from "interfaces/question.interface";
import { ISection } from "interfaces/section.interface";

class AssessmentFooterNewController {

    static $inject: string[] = ["store", "AssessmentFooterViewLogic"];

    private project: IProject;
    private currentUser: IUser;
    private onButtonClick: ({ actionType: IAssessmentQuestionAction}) => void;
    private selectedIndex: number;
    private submitButtonState: IAssessmentSubmitButtonConfig;
    private disablePreviousButton: boolean;
    private disableNextButton: boolean;
    private responseIsUpdating: boolean;
    private riskModels: any[];
    private currentSection: ISection;
    private destroy$ = new Subject();

    constructor(
        public readonly store: IStore,
        public readonly assessmentFooterViewLogic: AssessmentFooterViewLogic,
    ) { }

    public $onInit(): void {
        observableFromStore(this.store)
            .pipe(
                startWith(this.store.getState()),
                filter((state) => Boolean(state.settings.projectSettings)),
                takeUntil(this.destroy$),
            ).
            subscribe((state: IStoreState) => this.componentWillReceiveState(state));
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.selectedIndex) {
            this.disablePreviousButton = this.selectedIndex === 0;
            this.disableNextButton = (this.project.Sections.length === this.selectedIndex + 1) && this.selectedIndex !== 0;
            this.componentWillReceiveState(this.store.getState());
        }
    }

    public $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.riskModels = getAllRisksInOrder(state);
        this.submitButtonState = this.assessmentFooterViewLogic.calculateAssessmentFooterButtonState(this.currentUser, this.project, this.riskModels, this.currentSection);
    }

    private buttonClicked(type: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        this.onButtonClick({ actionType });
    }
}

export default {
    bindings: {
        currentSection: "<?",
        currentUser: "<",
        labels: "<",
        project: "<",
        requestInProgress: "<",
        responseIsUpdating: "<",
        selectedIndex: "<",
        showNextSection: "<",
        onButtonClick: "&",
        hidePreviousButton: "<?",
        hideNextButton: "<?",
    },
    controller: AssessmentFooterNewController,
    template: `
        <footer class="assessment-footer__wrapper">
            <div class="assessment-footer">
                <one-button
                    ng-if="!$ctrl.hidePreviousButton"
                    class="assessment-footer__btn assessment-footer__btn--left"
                    button-click="$ctrl.buttonClicked('INDEX_BACKWARD')"
                    icon="fa"
                    icon-class="fa-chevron-left"
                    is-disabled="$ctrl.disablePreviousButton"
                    identifier="previousSection"
                    text="{{::$root.t('PreviousSection')}}">
                </one-button>
                <one-button
                    ng-if="!$ctrl.hideNextButton"
                    class="assessment-footer__btn assessment-footer__btn--right"
                    button-click="$ctrl.buttonClicked('INDEX_FORWARD')"
                    identifier="nextSection"
                    is-disabled="$ctrl.disableNextButton"
                    right-side-icon="fa"
                    right-side-icon-class="fa-chevron-right"
                    text="{{::$root.t('NextSection')}}">
                </one-button>
                <div class="assessment-footer__btn-container" ng-if="$ctrl.project.IsLatestVersion">
                    <one-button
                        button-class="margin-left-right-1 cell-min-width-medium"
                        button-click="$ctrl.buttonClicked($ctrl.submitButtonState.submitButtonAction)"
                        enable-loading="true"
                        is-loading="$ctrl.requestInProgress"
                        identifier="assessment-footer-submit-button"
                        is-disabled="!$ctrl.submitButtonState.submitButtonEnabled || $ctrl.requestInProgress || $ctrl.responseIsUpdating"
                        ng-if="$ctrl.submitButtonState.submitButtonVisible"
                        text="{{$ctrl.responseIsUpdating ? $root.t('UpdatingUpper') : $ctrl.submitButtonState.submitButtonText}}"
                        type="primary">
                    </one-button>

                    <script type="text/ng-template" id="submit_tooltip.html">
                        <div class="tooltip-submit-enabled" ng-if="!$ctrl.project.Remaining.length">
                            <p>{{::$ctrl.labels.submitForReviewTooltip}}</p>
                        </div>
                        <div class="tooltip-submit-disabled" ng-if="$ctrl.project.Remaining.length">
                            <p>{{::$root.t("RequiredAnswerBeforeYouSubmit")}}</p>
                        </div>
                    </script>

                    <script type="text/ng-template" id="approve_tooltip.html">
                        <div ng-if="$ctrl.approveStatus">
                            <p>
                                <span ng-if="!$ctrl.project.NextTemplateName">
                                    {{::$root.t("QuestionsApprovedCompleteProject")}}
                                </span>
                                <span ng-if="$ctrl.project.NextTemplateName">
                                    {{::$root.t("QuestionsApprovedCompleteNextStep")}}
                                </span></p>
                        </div>
                        <div ng-if="!$ctrl.approveStatus">
                            <p>{{::$root.t("QuestionsMustBeApprovedBeforeComplete")}}</p>
                        </div>
                    </script>

                    <script type="text/ng-template" id="complete_analysis.html">
                        <p>{{::$root.t("AllRisksRecommendationsMitigated", {Recommendations: $root.customTerms.Recommendations}) }}</p>
                    </script>

                    <script type="text/ng-template" id="send_recommendations.html">
                        <p>{{::$root.t("SubmitRecommendationsToRespondent", {Recommendations: $root.customTerms.Recommendations}) }}</p>
                    </script>
                </div>
            </div>
        </footer>
    `,
};
