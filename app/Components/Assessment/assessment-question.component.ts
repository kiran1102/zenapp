import _, { assign } from "lodash";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

import AssessmentQuestionViewLogic from "./assessment-question-view-logic.service";

import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";
import { IStore } from "interfaces/redux.interface";

import { AssessmentStates } from "constants/assessment.constants";

import { QuestionStates, QuestionTypes, QuestionMultiChoiceTypes } from "enums/assessment-question.enum";

class AssessmentQuestionController {

    static $inject: string[] = ["store", "AssessmentQuestionViewLogic"];

    private currentUser: IUser;
    private events: any;
    private question: IQuestion;
    private section: ISection;
    private project: IProject;
    private onQuestionAction: ({}) => void;

    private unsubscribe: () => void;
    private QuestionTypes: any;
    private QuestionMultiChoiceTypes: any;
    private QuestionStates: any;
    private AssessmentStates: any;

    private viewState: any;
    private viewStateOverride: any;

    constructor(
        private store: IStore,
        private assessmentQuestionViewLogic: AssessmentQuestionViewLogic,
    ) { }

    public handleQuestionAction(questionAction: IAssessmentQuestionAction) {
        this.onQuestionAction({ questionAction });
    }

    public $onInit(): void {
        this.unsubscribe = this.store.subscribe(() => this.componentWillReceiveState());
        this.componentWillReceiveState();

        this.currentUser = getCurrentUser(this.store.getState());
        this.QuestionTypes = QuestionTypes;
        this.QuestionMultiChoiceTypes = QuestionMultiChoiceTypes;
        this.QuestionStates = QuestionStates;
        this.AssessmentStates = AssessmentStates;
    }

    private componentWillReceiveState(): void {
        this.viewState = this.assessmentQuestionViewLogic.calculateNewViewState(this.project, this.question);
        if (this.viewStateOverride) {
            assign(this.viewState, this.viewStateOverride);
        }
    }

}

const AssessmentQuestion = {
    bindings: {
        events: "<",
        getRemainingQuestions: "&",
        getProjectProgress: "<",
        question: "<",
        project: "<",
        section: "<",
        onQuestionAction: "&",
        viewStateOverride: "<?",
    },
    controller: AssessmentQuestionController,
    template: `
        <div class="pia-question">
            <div class="pia-question__container">
                <div class="pia-question__note {{$ctrl.viewState.questionVersionLabelClass}}" ng-if="$ctrl.viewState.canViewQuestionVersionLabel">
                    <p class="pia-question__note-text">
                        {{::$ctrl.viewState.questionVersionLabelText}}
                    </p>
                </div>
                <div class="tags" ng-if="$ctrl.viewState.canViewQuestionTag">
                    <one-tag translated-text="{{$ctrl.viewState.questionTag.text}}" label-class="{{$ctrl.viewState.questionTag.labelClass}}"></one-tag>
                </div>
                <div class="question-body">
                    <assessment-question-number
                        ng-if="$ctrl.viewState.canViewQuestionNumber"
                        is-locked="$ctrl.section.IsLocked"
                        question-label-class="$ctrl.viewState.questionLabelClass"
                        question-number="$ctrl.question.Number"
                        question-type="$ctrl.question.Type">
                    </assessment-question-number>
                    <div class="section__detail">
                        <assessment-question-content
                            current-user="$ctrl.currentUser"
                            question="$ctrl.question"
                            section="$ctrl.section"
                            project="$ctrl.project"
                            events="$ctrl.events"
                            is-disabled="$ctrl.viewState.isQuestionDisabled"
                            question-text-modifier="section-detail"
                            view-state="$ctrl.viewState"
                            on-question-update="$ctrl.handleQuestionAction(questionAction)">
                        </assessment-question-content>
                        <assessment-question-addons
                            class="margin-top-bottom-1 flex-full-width"
                            question="$ctrl.question"
                            ng-if="::$ctrl.viewState.canViewQuestionActions"
                            view-state="$ctrl.viewState"
                            on-button-click="$ctrl.handleQuestionAction(questionAction)">
                        </assessment-question-addons>
                    </div>
                    <question-change-toggle
                        question="$ctrl.question"
                        project="$ctrl.project"
                        project-states="$ctrl.AssessmentStates"
                        get-project-progress="$ctrl.getProjectProgress"
                        ng-disabled="$ctrl.section.IsLocked"
                        check-submit-state="$ctrl.getRemainingQuestions"
                        ng-if="::$ctrl.viewState.canViewQuestionChangeToggle">
                    </question-change-toggle>
                    <assessment-question-side-buttons
                        class="margin-left-2"
                        ng-if="!$ctrl.viewState.disableSidebuttons"
                        current-user="$ctrl.currentUser"
                        project="$ctrl.project"
                        project-level-approval="$ctrl.projectLevelApproval"
                        section="$ctrl.section"
                        question="$ctrl.question"
                        on-button-click="$ctrl.handleQuestionAction(questionAction)">
                    </assessment-question-side-buttons>
                </div>
            </div>
        </div>
    `,
};

export default AssessmentQuestion;
