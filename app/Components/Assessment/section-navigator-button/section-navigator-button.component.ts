
class SectionNavigatorButton {
    static $inject: string[] = ["$scope", "ENUMS"];

    private scope: any;
    private sectionStates: any;
    private section: any;

    constructor($scope: any, ENUMS: any) {
        this.scope = $scope;
        this.sectionStates = ENUMS.SectionStates;
    }

    public goToSection(): void {
        this.scope.$emit("GO_TO_SECTION", this.section.id);
    }

}

const sectionNavigatorButton = {
    bindings: {
        section: "<",
    },
    controller: SectionNavigatorButton,
    template: () => {
        return `<div
                    class="section-navigator-button row-vertical-center full-width"
                    ng-class="{
                        'is-active': $ctrl.section.isActive,
                        'is-completed': $ctrl.section.statusId === $ctrl.sectionStates.Completed,
                        'not-started': $ctrl.section.statusId === $ctrl.sectionStates.NotStarted
                    }">
                        <div class="section-navigator-button__container">
                            <h4 class="section-navigator-button__text margin-all-0"> {{ $ctrl.section.name }} </h4>
                            <button
                                class="section-navigator-button__button row-horizontal-vertical-center"
                                ng-click="$ctrl.goToSection()"
                                >
                                <i class="section-navigator-button__icon ot ot-check"></i>
                                <span
                                    class="section-navigator-button__required"
                                    ng-if="$ctrl.section.incompleteQuestionCount"
                                    >
                                </span>
                            </button>
                        </div>
                        <span class="section-navigator-button__connector stretch-horizontal"></span>
                </div>`;
    },
};

export default sectionNavigatorButton;
