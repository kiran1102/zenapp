import _ from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IQuestion, IAssessmentQuestionAction } from "interfaces/question.interface";

import { QuestionResponseTypes } from "enums/assessment-question.enum";

class AssessmentQuestionYesNoController {

    private question: IQuestion;
    private isDisabled: boolean;
    private onUpdate: ({}) => void;

    private QuestionResponseTypes: any;

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
    }

    private sendAction(type: string, value?: boolean | null): void {
        const actionType: IAssessmentQuestionAction = { type };
        if (type === "YES_SELECTED" || type === "NO_SELECTED") {
            _.assign(actionType, { value });
        }
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        question: "<",
        isDisabled: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionYesNoController,
    template: `
        <button
            class="one-button one-button--select question__yesno__button min-width-15 margin-right-1 margin-bottom-1"
            ng-class="{'selected': $ctrl.question.Answer}"
            ng-click="$ctrl.sendAction('YES_SELECTED', $ctrl.question.Answer)"
            ng-disabled="$ctrl.isDisabled"
            ng-model="$ctrl.question.Answer"
            uib-btn-radio="true"
            uncheckable="">
            {{::$root.t("Yes")}}
        </button>
        <button
            class="one-button one-button--select question__yesno__button min-width-15 margin-right-1 margin-bottom-1"
            ng-class="{'selected': $ctrl.question.Answer === false}"
            ng-click="$ctrl.sendAction('NO_SELECTED', $ctrl.question.Answer)"
            ng-disabled="$ctrl.isDisabled"
            ng-model="$ctrl.question.Answer"
            uib-btn-radio="false"
            uncheckable="">
            {{::$root.t("No")}}
        </button>
        <br>
        <assessment-question-not-sure-button
            button-class="margin-right-1 margin-top-bottom-1 min-width-15"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
            on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
            ng-if="$ctrl.question.AllowNotSure">
        </assessment-question-not-sure-button>
        <assessment-question-not-applicable-button
            button-class="margin-right-1 margin-top-bottom-1 min-width-15"
            is-disabled="$ctrl.isDisabled"
            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
            on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
            ng-if="$ctrl.question.AllowNotApplicable">
        </assessment-question-not-applicable-button>
    `,
};
