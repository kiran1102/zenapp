import * as _ from "lodash";

class AssessmentDetailsBanner {

    static $inject: string[] = ["$scope", "ENUMS", "AssessmentDetailsConfig", "OneAssessment"];

    private scope: any;
    private readOnly: boolean;
    private detailsConfig: any;
    private context: number;
    private ContextTypes: any;
    private details: any;

    constructor(readonly $scope: any, ENUMS: any, readonly AssessmentDetailsConfig: any, readonly OneAssessment: any) {
        this.ContextTypes = ENUMS.ContextTypes;
    }

    public updateDetail(value, key): void {
        this.$scope.$emit("UPDATE_ASSESSMENT_DETAIL", key, value);
    }

    public $onInit(): void {
        this.startListeners();
        if (this.context === this.ContextTypes.DMAssessment) {
            this.details = {
                orgGroup: this.OneAssessment.getOrgGroup(),
                createdBy: this.OneAssessment.getCreator(),
                respondent: this.OneAssessment.getRespondent(),
                approver: this.OneAssessment.getApprover(),
                deadline: this.OneAssessment.getDeadline(),
                team: this.OneAssessment.getTeam(),
            };
            if (!_.isString(this.details.respondent) && _.isUndefined(this.details.respondent.FullName)) {
                this.details.respondent.FullName = this.details.respondent.Email;
            }
        }
        this.readOnly = this.OneAssessment.getReadOnly();
        this.detailsConfig = this.AssessmentDetailsConfig.getConfig(this.context);
        if (this.readOnly) {
            _.forEach(this.detailsConfig, (detail: any): void => {
                detail.isEditable = false;
            });
        }
    }

    private startListeners(): void {
        this.$scope.$on("APPROVER_CHANGED", (): void => {
            this.details.approver = this.OneAssessment.getApprover();
        });
        this.$scope.$on("RESPONDENT_CHANGED", (): void => {
            this.details.respondent = this.OneAssessment.getRespondent();
        });
        this.$scope.$on("DETAILS_CHANGED", (): void => {
            if (this.context === this.ContextTypes.DMAssessment) {
                this.details = {
                    orgGroup: this.OneAssessment.getOrgGroup(),
                    createdBy: this.OneAssessment.getCreator(),
                    respondent: this.OneAssessment.getRespondent(),
                    approver: this.OneAssessment.getApprover(),
                    deadline: this.OneAssessment.getDeadline(),
                    team: this.OneAssessment.getTeam(),
                };
            }
        });
    }
}

const assessmentDetailsBanner = {
    bindings: {
        context: "<",
        isHidden: "<?",
    },
    controller: AssessmentDetailsBanner,
    template: () => {
        return `
            <section
                class="assessment-details-banner"
                ng-class="{'is-hidden': $ctrl.isHidden}"
                expand="!$ctrl.isHidden"
                transition="200"
                >
                    <ul class="assessment-details-banner__list">
                        <li
                            class="assessment-details-banner__item"
                            ng-repeat="item in $ctrl.detailsConfig track by $index"
                            >
                            <label class="assessment-details-banner__label">{{ item.label }}</label>
                            <one-editable-types
                                class="assessment-details-banner__text"
                                model="$ctrl.details[item.key]"
                                handle-edit="$ctrl.updateDetail(model, item.key)"
                                options="$ctrl.details[item.optionsKey]"
                                config="item"
                                >
                            </one-editable-types>
                        </li>
                    </ul>
            </section>
        `;
    },
};

export default assessmentDetailsBanner;
