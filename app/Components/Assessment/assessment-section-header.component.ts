import AssessmentSectionHeaderViewLogic from "generalcomponent/Assessment/assessment-section-header-view-logic.service";

import { IChangesObject } from "angular";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { IUser } from "interfaces/user.interface";
import { ISection } from "interfaces/section.interface";

class AssessmentSectionHeaderCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "AssessmentSectionHeaderViewLogic"];

    private assessmentModel: IPiaAssessment;
    private currentSection: ISection;

    private viewState: any;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
        private readonly AssessmentSectionHeaderViewLogic: AssessmentSectionHeaderViewLogic,
    ) { }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.assessmentModel) {
            this.viewState = this.AssessmentSectionHeaderViewLogic.calculateNewViewState(this.assessmentModel, this.currentSection);
        }
    }
}

export default {
    bindings: {
        actions: "<",
        currentSection: "<",
        labels: "<",
        assessmentModel: "<",
    },
    controller: AssessmentSectionHeaderCtrl,
    template: `
        <header class="welcome__header padding-left-right-2">
            <div class="full-width">

                <ul class="banner">
                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowSectionStatus">
                        <h4>{{::$root.t("Status")}}</h4>
                        <span class="welcome__tag projectlabels-tag__{{$ctrl.currentSection.StateLabel}}" ng-bind="$ctrl.currentSection.StateName"></span>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowName">
                        <h4>{{::$root.t("Name")}}</h4>
                        <div
                            class="banner-item__text"
                            editable-text="$ctrl.assessmentModel.Name"
                            ng-if="$ctrl.viewState.canEditName"
                            onaftersave="$ctrl.actions.saveProject()">
                            <span class="text-pointer hover-underline">{{ $ctrl.assessmentModel.Name || "- - - - -" }}</span>
                            <i class="ot ot-edit text-decoration-none text-pointer"></i>
                        </div>
                        <div ng-if="!$ctrl.viewState.canEditName" class="banner-item__text">
                            <span class="hover-no-underline">{{ $ctrl.assessmentModel.Name || "- - - - -" }}</span>
                        </div>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowOrg">
                        <h4>{{::$root.t("Organization")}}</h4>
                        <div class="banner-item__text" ng-if="$ctrl.viewState.canEditOrg" ng-click="$ctrl.actions.openChangeOrgModal()">
                            <span class="text-pointer hover-underline">{{ $ctrl.assessmentModel.OrgGroup || "- - - - -" }}</span>
                            <i class="ot ot-edit text-decoration-none text-pointer"></i>
                        </div>
                        <div ng-if="!$ctrl.viewState.canEditOrg" class="banner-item__text">
                            <span class="hover-no-underline">{{ $ctrl.assessmentModel.OrgGroup || "- - - - -" }}</span>
                        </div>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowCreator">
                        <h4>{{::$root.t("Creator")}}</h4>
                        <div class="banner-item__text">{{ $ctrl.assessmentModel.Creator || "- - - - -" }}</div>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowRespondent">
                        <h4>{{::$root.t("Respondent")}}</h4>
                        <div class="banner-item__text" ng-if="$ctrl.viewState.canEditRespondent" ng-click="$ctrl.actions.openProjectAssignmentsModal($ctrl.currentSection.Id, $ctrl.assessmentModel, $ctrl.currentSection)">
                            <span class="text-pointer hover-underline">{{ $ctrl.currentSection.Assignment.Assignee || $ctrl.assessmentModel.Lead || "- - - - -" }}</span>
                            <i class="ot ot-edit text-decoration-none text-pointer"></i>
                        </div>
                        <div ng-if="!$ctrl.viewState.canEditRespondent" class="banner-item__text">
                            <span class="hover-no-underline">{{ $ctrl.currentSection.Assignment.Assignee || $ctrl.assessmentModel.Lead || "- - - - -" }}</span>
                        </div>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowDeadline">
                        <h4>{{::$root.t("Deadline")}}</h4>
                        <div class="banner-item__text" ng-if="$ctrl.viewState.canEditDeadline" ng-click="$ctrl.actions.openDeadlinesModal()">
                            <one-date
                                class="text-pointer hover-underline"
                                data-id="AssessmentSectionHeaderDeadline"
                                ng-if="$ctrl.assessmentModel.Deadline"
                                date-to-format="$ctrl.assessmentModel.Deadline"
                            ></one-date>
                            <span class="text-pointer hover-underline" ng-if="!$ctrl.assessmentModel.Deadline">- - - - -</span>
                            <i class="ot ot-edit text-decoration-none text-pointer"></i>
                        </div>
                        <div class="banner-item__text" ng-if="!$ctrl.viewState.canEditDeadline" class="banner-item__text">
                            <one-date
                                class="text-pointer hover-no-underline"
                                data-id="AssessmentSectionHeaderDeadline"
                                ng-if="$ctrl.assessmentModel.Deadline"
                                date-to-format="$ctrl.assessmentModel.Deadline"
                            ></one-date>
                            <span class="hover-no-underline" ng-if="!$ctrl.assessmentModel.Deadline">- - - - -</span>
                        </div>
                    </li>

                    <li class="banner-item margin-right-1" ng-if="$ctrl.viewState.canShowApprover">
                        <h4>{{ ::($ctrl.labels ? $ctrl.labels.projectApprover : $root.t("Approver")) }}</h4>
                        <div class="banner-item__text" ng-if="$ctrl.viewState.canEditApprover" ng-click="$ctrl.actions.openProjectReviewerModal()">
                            <span class="text-pointer hover-underline">{{ $ctrl.assessmentModel.Reviewer || "- - - - -" }}</span>
                            <i class="ot ot-edit text-decoration-none text-pointer"></i>
                        </div>
                        <div ng-if="!$ctrl.viewState.canEditApprover" class="banner-item__text">
                            <span class="hover-no-underline">{{ $ctrl.assessmentModel.Reviewer || "- - - - -" }}</span>
                        </div>
                    </li>
                </ul>

                <div class="welcome__description inline-block" ng-if="$ctrl.viewState.canShowDescription">
                    <h4>{{::$root.t("Description")}}</h4>
                    <div
                        class="banner-item__text"
                        ng-if="$ctrl.viewState.canEditDescription"
                        editable-textarea="$ctrl.assessmentModel.Description"
                        onaftersave="$ctrl.actions.saveProject()">
                        <span class="text-pointer hover-underline">{{ $ctrl.assessmentModel.Description || "- - - - -" }}</span>
                        <i class="ot ot-edit text-decoration-none text-pointer"></i>
                    </div>
                    <div class="banner-item__text" ng-if="!$ctrl.viewState.canEditDescription">
                        <span class="hover-no-underline">{{ $ctrl.assessmentModel.Description || "- - - - -" }}</span>
                    </div>
                </div>

            </div>
        </header>
    `,
};
