// Enums
import { ExportTypes } from "enums/export-types.enum";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Servcies
import { DMAssessmentsService } from "app/scripts/DataMapping/Services/Assessment/dm-assessments.service";

class DmAssessmentsHeader {

    static $inject: string[] = [
        "$scope",
        "$rootScope",
        "$state",
        "DMAssessments",
        "ENUMS",
        "Permissions",
    ];

    private translate: any;
    private goTo: any;
    private States: any;
    private InventoryIds: any;
    private exportInProgress = false;
    private templateType: any[];
    private DMTemplateTypes: any;
    private exportOptions: IDropdownOption[];

    constructor(
        readonly $scope: any,
        $rootScope: IExtendedRootScopeService,
        $state: ng.ui.IStateService,
        private readonly DMAssessments: DMAssessmentsService,
        ENUMS: any,
        private Permissions: any) {
        this.translate = $rootScope.t;
        this.goTo = $state.go;
        this.States = ENUMS.DMAssessmentStates;
        this.InventoryIds = ENUMS.InventoryTableIds;
        this.DMTemplateTypes = ENUMS.DMAssessmentTemplateTypes;
    }

    $onInit() {
        this.exportOptions = [
            {
                text: this.translate("ProcessingActivities"),
                action: (): void => {
                    this.exportAssessments(this.DMTemplateTypes.ProcessingActivity);
                },
            },
            {
                text: this.translate("Assets"),
                action: (): void => {
                    this.exportAssessments(this.DMTemplateTypes.Asset);
                },
            },
        ];
    }

    goToAssets(): void {
        this.goTo("zen.app.pia.module.datamap.views.inventory.list", { Id: this.InventoryIds.Applications });
    }

    exportAssessments(templateType: number): void {
        this.exportInProgress = true;
        this.DMAssessments.exportV2Report(String(templateType)).then(() => {
            this.exportInProgress = false;
        });
    }
}

export default {
    bindings: {
        filters: "<?",
    },
    controller: DmAssessmentsHeader,
    template: `
        <one-header class="dm-assessments-header" hide-shadow="$ctrl.openFilterOptions">
            <header-title class="dm-assessments-header__title">
                <span class="dm-assessments-header__name"> {{::$root.t("Assessments")}} </span>
            </header-title>
            <div
                class="dm-assessments-header__actions row-flex-start"
                >
                <one-dropdown
                    ng-if="$ctrl.Permissions.canShow('DataMappingExportAssessments')"
                    class="inline-block"
                    text="{{::$ctrl.translate('Export')}}"
                    is-loading="$ctrl.exportInProgress"
                    is-disabled="$ctrl.exportInProgress"
                    round-button="true"
                    is-large="true"
                    align-right="true"
                    options="$ctrl.exportOptions"
                    >
                </one-dropdown>
            </div>
        </one-header>
    `,
};
