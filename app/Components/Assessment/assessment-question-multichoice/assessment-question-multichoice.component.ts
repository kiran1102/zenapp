import _ from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestion, IAssessmentQuestionAction, IMultiChoiceDataTypes } from "interfaces/question.interface";
import { IAttributeValue } from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { MultiChoiceTypes } from "enums/question-types.enum";

import * as ACTIONS from "../assessment-actions.constants";

import { QuestionMultiChoiceTypes, QuestionResponseTypes } from "enums/assessment-question.enum";
import { getIsIE11 } from "oneRedux/reducers/setting.reducer";

interface ISingleSelectModel extends IAttributeValue {
    InputValue: string;
    IsPlaceholder: boolean;
    Name: string;
    Value: string;
}

class AssessmentQuestionMultichoiceController {

    static $inject: string[] = ["store", "$rootScope"];

    private question: IQuestion;
    private tempModel: any;
    private onUpdate: ({}) => void;
    private isIE11 = false;

    private QuestionMultiChoiceTypes: any;
    private QuestionResponseTypes: any;
    private placeHolderText: string;
    private multiChoiceTypes: IMultiChoiceDataTypes;

    constructor(
        private store: IStore,
        private $rootScope: IExtendedRootScopeService,
    ) {}

    public $onInit(): void {
        this.QuestionMultiChoiceTypes = QuestionMultiChoiceTypes;
        this.QuestionResponseTypes = QuestionResponseTypes;
        this.multiChoiceTypes = MultiChoiceTypes;
        this.placeHolderText = getIsIE11(this.store.getState()) ? "" : this.$rootScope.t("TypeOrSelectAnOption");
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.question) this.question = _.cloneDeep(this.question);
    }

    private handleSingleSelection(model: ISingleSelectModel): void {
        if (model.InputValue) {
            this.sendAction("OTHER_OPTION_UPDATED", model.InputValue);
        } else {
            this.sendAction("MULTICHOICE_OPTION_SELECTED", model.Value, model.Name);
        }
    }

    private sendAction(type: string, value?: any, name?: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        if (type === ACTIONS.OTHER_OPTION_UPDATED || type === ACTIONS.MULTICHOICE_OPTION_SELECTED) {
            _.assign(actionType, { value, name });
        }
        this.onUpdate({ actionType });
    }
}

export default {
    bindings: {
        question: "<",
        project: "<",
        isDisabled: "<",
        onUpdate: "&",
    },
    controller: AssessmentQuestionMultichoiceController,
    template: `
        <div class="question__multichoice">
            <one-searchable-dropdown
                identifier="questionMultichoiceDropdown"
                dropdown-class="margin-bottom-1"
                ng-if="$ctrl.question.DisplayTypeId === $ctrl.QuestionMultiChoiceTypes.Dropdown"
                model="$ctrl.question.Answer"
                options="$ctrl.question.Options"
                option-label-key="Name"
                option-value-key="Value"
                placeholder="{{::$root.t('TypeOrSelectAnOption')}}"
                tooltip-enabled="$ctrl.question.OptionHintEnabled"
                select-callback="$ctrl.sendAction('MULTICHOICE_OPTION_SELECTED', dataFromDropdown)"
            ></one-searchable-dropdown>

            <typeahead
                identifier="questionAssetDiscovery"
                class="question__typeahead"
                ng-if="$ctrl.question.OptionListTypeId === $ctrl.multiChoiceTypes.AssetDiscovery"
                options="$ctrl.question.Options"
                order-by-label="false"
                model="$ctrl.question.AnswerName"
                label-key="Name"
                has-arrow="true"
                clear-on-select="false"
                disable-if-no-options="false"
                placeholder-text="$ctrl.placeHolderText"
                allow-other="$ctrl.question.AllowOther"
                other-option-prepend="{{::$root.t('CreateNew')}}"
                allow-empty="false"
                empty-text="{{::$root.t('NoAnswer')}}"
                on-select="$ctrl.handleSingleSelection(selection)"
                scroll-parent="$ctrl.scrollParent"
                append-to-body="false"
                form-class="question__multichoice-typeahead"
                input-limit="2"
                use-virtual-scroll="true"
                options-limit="1000"
                >
            </typeahead>

            <div class="btn-group">
                <div
                    class="option-container"
                    ng-if="$ctrl.question.Options.length && option.Name && $ctrl.question.DisplayTypeId !== $ctrl.QuestionMultiChoiceTypes.Dropdown && $ctrl.question.OptionListTypeId !== $ctrl.multiChoiceTypes.AssetDiscovery"
                    ng-repeat="option in $ctrl.question.Options track by $index">
                    <button
                        class="one-button one-button--select question__multichoice__button"
                        ng-model="option.Answer"
                        ng-class="{'selected': $ctrl.question.Answer === option.Value}"
                        ng-click="$ctrl.sendAction('MULTICHOICE_OPTION_SELECTED', option.Value)"
                        ng-disabled="$ctrl.isDisabled"
                        tooltip-append-to-body="true"
                        tooltip-enable="$ctrl.question.OptionHintEnabled"
                        tooltip-placement="top"
                        uib-tooltip="{{option.Hint}}">
                        {{option.Name || '- - - -'}}
                    </button>
                </div>

                <assessment-question-other-option-button
                    class="question__multiselect-other"
                    is-disabled="$ctrl.isDisabled"
                    ng-if="$ctrl.question.AllowOther && $ctrl.question.OptionListTypeId !== $ctrl.multiChoiceTypes.AssetDiscovery"
                    option-text="$ctrl.question.OtherAnswer"
                    on-after-save="$ctrl.sendAction('OTHER_OPTION_UPDATED', value)">
                </assessment-question-other-option-button>

                <div class="option-container">
                    <assessment-question-not-sure-button
                        button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                        is-disabled="$ctrl.isDisabled"
                        is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
                        on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                        ng-if="$ctrl.question.AllowNotSure">
                    </assessment-question-not-sure-button>
                </div>
                <div class="option-container">
                    <assessment-question-not-applicable-button
                        button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                        is-disabled="$ctrl.isDisabled"
                        is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
                        on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                        ng-if="$ctrl.question.AllowNotApplicable">
                    </assessment-question-not-applicable-button>
                </div>
            </div>
        </div>
    `,
};
