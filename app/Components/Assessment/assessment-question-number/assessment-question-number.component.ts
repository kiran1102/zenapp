import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { QuestionTypes } from "enums/assessment-question.enum";

class AssessmentQuestionNumberController {

    static $inject: string[] = ["$rootScope"];

    private questionType: number;
    private isQuestionContent: boolean;

    constructor(public readonly $rootScope: IExtendedRootScopeService) {}

    public $onInit(): void {
        const { Content } = QuestionTypes;
        this.isQuestionContent = this.questionType === Content;
    }
}

export default {
    bindings: {
        isLocked: "<",
        questionLabelClass: "<",
        questionNumber: "<",
        questionType: "<",
    },
    controller: AssessmentQuestionNumberController,
    template: `
        <label
            class="assessment-question__label {{$ctrl.questionLabelClass}}"
            data-ng-if="!$ctrl.isLocked">
            <h4 ng-if="!$ctrl.isQuestionContent">{{$ctrl.questionNumber}}</h4>
            <i class="fa fa-quote-right" ng-if="$ctrl.isQuestionContent"></i>
        </label>
        <label
            class="assessment-question__label"
            ng-if="$ctrl.isLocked"
            tooltip-append-to-body="true"
            tooltip-placement="top"
            tooltip-popup-close-delay="200"
            tooltip-trigger="mouseenter"
            uib-tooltip="{{::$root.t('QuestionSubmittedNoLongerEditable')}}">
            <h4 ng-if="!$ctrl.isQuestionContent">{{$ctrl.questionNumber}}</h4>
            <i class="fa fa-quote-right" ng-if="$ctrl.isQuestionContent"></i>
            <i class="fa fa-lock" ng-if="!$ctrl.isQuestionContent"></i>
        </label>
    `,
};
