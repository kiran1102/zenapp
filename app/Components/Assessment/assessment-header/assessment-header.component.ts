import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class AssessmentHeader implements ng.IComponentController {

    static $inject: string[] = ["$scope", "$rootScope", "ENUMS", "$injector"];

    private translate: any;
    private AssessmentStateLabels: any;
    private AssessmentStateClasses: any;
    private ContextTypes: any;
    private context: number;
    private assessmentFactory: any;
    private assessment: any;
    private detailsHidden = true;

    constructor(
        readonly $scope: any,
        $rootScope: IExtendedRootScopeService,
        ENUMS: any,
        readonly $injector: ng.auto.IInjectorService) {
        this.AssessmentStateLabels = ENUMS.DMAssessmentStateLabels;
        this.AssessmentStateClasses = ENUMS.DMAssessmentStateClasses;
        this.ContextTypes = ENUMS.ContextTypes;
        this.translate = $rootScope.t;
    }

    public $onInit(): void {
        this.$scope.$on("STATUS_UPDATED", (): void => {
            this.assessment.statusId = this.assessmentFactory.getStatus();
        });
        if (this.context === this.ContextTypes.DMAssessment) {
            this.assessmentFactory = this.$injector.get("OneAssessment");
        }
        this.assessment = this.assessmentFactory.getAssessment();
    }

    public toggleDetails(): any {
        this.detailsHidden = !this.detailsHidden;
    }

}

const assessmentHeaderComponent: ng.IComponentOptions = {
    bindings: {
        context: "<",
    },
    controller: AssessmentHeader,
    template: `
        <header class="assessment-header padding-left-right-2 padding-top-2 padding-bottom-1">
            <span class="row-horizontal-center">
                <h2 class="heading1 margin-none text-bold"> {{$ctrl.assessment.name}} </h2>
                <one-tag
                    class="margin-left-2 margin-right-3 margin-top-half text-nowrap"
                    plain-text="{{$root.t($ctrl.AssessmentStateLabels[$ctrl.assessment.statusId])}}"
                    label-class="{{$ctrl.AssessmentStateClasses[$ctrl.assessment.statusId]}}"
                    size="medium"
                    >
                </one-tag>
            </span>
            <assessment-details-banner
                class="assessment-header__details centered-max-100"
                context="$ctrl.context"
                is-hidden="$ctrl.detailsHidden"
                >
            </assessment-details-banner>
            <section-navigator
                class="assessment-header__section-nav centered-max-100 padding-top-1"
                context="$ctrl.context"
                >
            </section-navigator>
            <button
                class="assessment-header__toggle"
                ng-click="$ctrl.toggleDetails()"
                uib-tooltip="{{:: $ctrl.translate('ToggleAssessmentDetails', { Assessment: $ctrl.translate('Assessment') }) }}"
                tooltip-placement="top-right"
                tooltip-append-to-body="true"
                >
                <i
                    class="assessment-header__toggle-icon fa"
                    ng-class="!$ctrl.detailsHidden ? 'fa-caret-down' : 'fa-caret-left'"
                />
            </button>
        </header>
    `,
};

export default assessmentHeaderComponent;
