import { includes, find } from "lodash";
import { getQuestionApprovalEnabled } from "oneRedux/reducers/setting.reducer";
import AssessmentBusinessLogic from "generalcomponent/Assessment/assessment-business-logic.service";

import { AssessmentPermissions, AssessmentStates } from "constants/assessment.constants";
import { AssignmentTypes } from "constants/assignment-types.constant";

import { IUser } from "interfaces/user.interface";
import { IPiaAssessment } from "interfaces/assessment.interface";
import { ISection } from "interfaces/section.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IStore } from "interfaces/redux.interface";
import { RiskPermissions } from "constants/risk.constants";

export default class AssessmentMenuPermissionService {
    static $inject: string[] = ["store", "Permissions", "AssessmentBusinessLogic"];

    private assessmentMenuPermissions;

    constructor(
        private store: IStore,
        private permissions: Permissions,
        private assessmentBusinessLogic: AssessmentBusinessLogic,
    ) {
        const { UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed } = AssessmentStates;
        const { AssessmentViewAddedInfo, AssesmentRiskCountView } = AssessmentPermissions;
        const { approver, respondent } = AssignmentTypes;
        const { AssessmentRiskView } = RiskPermissions;

        this.assessmentMenuPermissions = {
            canShowAcceptedPill: {
                questionApprovalShouldBeOn: true,
                allowedPermission:          AssessmentViewAddedInfo,
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
            },
            canShowInfoAddedPill: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
            },
            canShowInfoRequestedPill: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
            },
            canShowRiskPill: {
                allowedPermission:          AssesmentRiskCountView,
                allowedAssessmentStates:    [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
            },
            canShowUnansweredPill: {
                allowedPermission:          AssessmentViewAddedInfo,
                allowedAssessmentStates:    [UnderReview, InfoNeeded],
            },
            canShowRiskSummaryTab: {
                allowedPermission:          AssessmentRiskView,
                allowedAssessmentStates:    [UnderReview, InfoNeeded, RiskAssessment, RiskTreatment, Completed],
            },
        };
    }

    public hasMenuPermission(permissionToCheck: string, assessment: IPiaAssessment): boolean {
        const config = this.assessmentMenuPermissions[permissionToCheck];

        return (this.hasPermission(config.allowedPermission)
            || this.isAssignedToAssessmentOrSection(assessment)
            || this.assessmentBusinessLogic.isRiskOwner(assessment))
            && this.isAllowedState(config.allowedAssessmentStates, assessment.StateDesc)
            && this.isQuestionApprovalSettingMatching(config.questionApprovalShouldBeOn, this.isQuestionApprovalSettingEnabled());
    }

    private hasPermission(permissionToCheck: string): boolean {
        return this.permissions.canShow(permissionToCheck) || this.permissions.canShow("ProjectsFullAccess");
    }

    private isAllowedState(stateArray: Array<string | number>, state: string | number): boolean {
        return includes(stateArray, state);
    }

    private isAssignedToAssessmentOrSection(assessment: IPiaAssessment): boolean {
        const { assignedSectionsLength } = this.assessmentBusinessLogic.getSections(assessment);
        return assignedSectionsLength > 0;
    }

    private isQuestionApprovalSettingMatching(permissionToCompare: boolean | undefined, currentSetting: boolean): boolean {
        if (permissionToCompare === undefined) {
            return true;
        }
        return permissionToCompare === currentSetting;
    }

    private isQuestionApprovalSettingEnabled(): boolean {
        return getQuestionApprovalEnabled(this.store.getState());
    }

}
