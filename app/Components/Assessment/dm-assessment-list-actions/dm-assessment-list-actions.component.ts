import { IActionIcon } from "interfaces/action-icons.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class DMAssessmentListActionsController {
    static $inject: string[] = ["$scope", "$rootScope", "NotifyService", "ENUMS", "Permissions"];

    private rowId: string;
    private actions: IActionIcon[];
    private deleteLoading = false;
    private actionKeyEnums: any;
    private AssessmentStates: any;
    private assessmentStatus: number;

    constructor(
        private $scope: any,
        private $rootScope: IExtendedRootScopeService,
        private NotifyService: any,
        ENUMS: any,
        private Permissions: any) {

        this.actionKeyEnums = ENUMS.ActionKeys;
        this.AssessmentStates = ENUMS.DMAssessmentStates;
    }

    $onInit() {
        this.setActionsConfig();
    }

    $onChanges(changes: any): void {
        if (!changes.rowId.isFirstChange() && changes.rowId.currentValue !== changes.rowId.previousValue) {
            this.deleteLoading = false;
            this.setActionsConfig();
        }
    }

    private setActionsConfig(): void {
        let actions = [];
        if (this.Permissions.canShow("DataMappingAssessmentsDelete")) {
            actions = [...actions, {
                key: this.actionKeyEnums.Delete,
                isLoading: this.deleteLoading,
                handleClick: this.deleteCell,
                iconClass: "fa-trash",
            }];
        }
        this.actions = actions;
    }

    private deleteCell = (): void => {
        this.deleteLoading = true;
        this.setActionsConfig();
        this.NotifyService.confirm(this.$rootScope.t("DeleteAssessment"), this.$rootScope.t("SureYouWantToDeleteAssessment"), "", "Cancel").then((result: any): void => {
            if (result) {
                this.$scope.$emit("DELETE_ASSESSMENT", this.rowId);
            } else {
                this.deleteLoading = false;
                this.setActionsConfig();
            }
        });
    }

}

const dmAssessmentListActionsTemplate = `
        <action-icons
            actions="$ctrl.actions"
            >
        </action-icons>
	`;

const dmAssessmentListActionsComponent = {
    controller: DMAssessmentListActionsController,
    template: dmAssessmentListActionsTemplate,
    bindings: {
        rowId: "@",
        assessmentStatus: "<",
    },
};

export default dmAssessmentListActionsComponent;
