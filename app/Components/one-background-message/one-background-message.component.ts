const oneBackgroundMessage = {
    bindings: {
        showImage: "<?",
        imageSrc: "@?",
        showEmptyStateImg: "<?",
        row1: "@",
        row2: "@",
        rowStyled: "<?",
    },
    template:
        `
            <div class="one-background-message">
                <img ng-if="::$ctrl.showEmptyStateImg" class="one-background-message--row block" src="images/empty_state_icon.svg"/>
                <img ng-if="::$ctrl.showImage && $ctrl.imageSrc" class="one-background-message--row block" ng-src="{{$ctrl.imageSrc}}" />
                <h2
                    ng-if="::$ctrl.showEmptyStateImg"
                    ng-class="{'one-background-message--row-styled padding-top-1 padding-left-1' : $ctrl.rowStyled}"
                    class="one-background-message--row flex text-center text-bold">
                    {{$root.t('NoItemsToDisplay')}}
                </h2>
                <h2 class="one-background-message--row flex text-center text-bold">
                    {{$ctrl.row1}}
                </h2>
                <h2 ng-if="!!$ctrl.row2" class="one-background-message--row flex text-center text-bold">
                    {{$ctrl.row2}}
                </h2>
            </div>
        `,
};

export default oneBackgroundMessage;
