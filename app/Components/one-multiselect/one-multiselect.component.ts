import { KeyCodes } from "enums/key-codes.enum";
import { filter, forEach, isUndefined, throttle } from "lodash";
import Utilities from "Utilities";
import $ from "jquery";
import JQuery from "jquery";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class MultiselectController implements ng.IComponentController {

    static $inject: string[] = [
        "$timeout",
        "$animate",
        "$element",
        "$rootScope",
        "$scope",
    ];

    private scrollUl: HTMLElement;
    private uiSelect: HTMLElement;
    private searchInput: HTMLElement;
    private selected: { model: any, latest: any } = { model: null, latest: null };
    private uuid: string = Utilities.uuid();
    private lastSave: Date = new Date();
    private taggingTokens = "ENTER";

    private allowEmpty: boolean;
    private allowOther: boolean;
    private allowOtherLabel: string;
    private allowOtherLabelAppend: string;
    private allowSorting: boolean;
    private emptyOptionText: string;
    private emptyOptionLast: boolean;
    private handleRemove: any;
    private handleSelect: any;
    private identifier: string;
    private isDisabled: boolean;
    private labelKey: string;
    private translationKey: string;
    private list: any[];
    private model: any;
    private onBlur: any;
    private openOnInit: boolean;
    private orderByLabel: boolean;
    private placeholder: string;
    private appendToBody: boolean;
    private pillTabbing: boolean;
    private throttleValue: number;
    private translate: Function;
    private startValue = 0;
    private limit = 25;
    private boundingSize: number;
    private lastScrollPosition: number;
    private isLoading = false;
    private maxInitValue = 50;
    private labelValue: string;
    private isScrolling: any;

    constructor(
        private readonly $timeout: ng.ITimeoutService,
        private readonly $animate: any,
        private readonly $element: HTMLElement,
        private readonly $rootScope: IExtendedRootScopeService,
        private $scope: any,
    ) {}

    $onInit() {
        this.labelValue = this.returnLabelValue();
        this.translate = this.$rootScope.t;
        if (isUndefined(this.pillTabbing)) this.pillTabbing = true;
        this.$animate.enabled(this.$element, true);
        this.$timeout((): void => {
            this.labelValue = this.returnLabelValue();
            if (!this.pillTabbing) this.removeMultiselectPillTab();
            this.uiSelect = $(`[data-id='${this.identifier || this.uuid}']`)[0];
            this.$animate.enabled(this.uiSelect, true);
            this.searchInput = this.uiSelect ? $(this.uiSelect).find("input.ui-select-search")[0] : null;
            this.scrollUl = this.uiSelect ? $(this.uiSelect).find("ul.ui-select-choices")[0] : null;
            this.searchInput.onfocus = this.handleFocusEvents;
            this.searchInput.onkeydown = this.handleKeyEvents;
            if (this.openOnInit) this.searchInput.click();
            if (this.scrollUl) this.scrollUl.onscroll = throttle(this.handleScrollEvents, 200);
        }, 0, true);
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.model) this.selected.model = changes.model.currentValue;
        this.selected.latest = this.selected.model;
        this.allowOtherLabel = this.allowOtherLabel ? this.allowOtherLabel + " " : "";
        this.allowOtherLabelAppend = this.allowOtherLabelAppend ? " " + this.allowOtherLabelAppend : "";

        if ((changes.allowEmpty && changes.allowEmpty.currentValue)
            || (this.allowEmpty && (changes.emptyOptionText || changes.emptyOptionLast))
            || (this.allowEmpty && (this.emptyOptionText || this.emptyOptionLast))) {
            this.handleEmptyOptionListItem(this.emptyOptionText, this.emptyOptionLast);
        }
    }

    onSelect($item: any): void {
        if (!$item) {
            this.handleRemove({ $model: this.selected.latest, $item });
            return;
        }
        this.selected.model = filter(this.selected.model, (item: any): boolean => !item.oneEmpty);
        if (this.handleSelect) {
            this.handleSelect({ $model: this.selected.model, $item });
            this.onAfterSave();
        } else this.selected.model = this.selected.latest;
    }

    onRemove($item: any): void {
        if (this.handleRemove) {
            this.handleRemove({ $model: this.selected.model, $item });
            this.onAfterSave();
        } else this.selected.model = this.selected.latest;
    }

    onInputBlur(force: boolean): void {
        if (!this.pillTabbing) this.removeMultiselectPillTab();
        if (this.onBlur) {
            this.$timeout((): void => {
                const blurDelta: boolean = this.lastSave.getTime() + 1000 < (new Date()).getTime();
                if (force || blurDelta) this.onBlur({ $model: this.selected.model });
            }, 0);
        }
    }

    allowOtherTransform = (other: string): any => {
        const placeholder: any = {
            IsPlaceholder: true,
        };
        placeholder[this.labelKey] = other;
        placeholder.oneOther = `${this.allowOtherLabel}${other}${this.allowOtherLabelAppend}`;
        return placeholder;
    }

    formatItem(item: { [key: string]: any }): string {
        if (this.translationKey && item && item[this.translationKey]) {
            return this.translate(item[this.translationKey]);
        }
        if (this.labelKey) {
            return item[this.labelKey];
        }
        return "";
    }

    returnLabelValue(): string {
        if (this.translationKey) {
            return this.translationKey;
        }
        if (this.labelKey) {
            return this.labelKey;
        }
        return "";
    }

    private toggleLimit(reset?: boolean, limit?: number): void {
        if (reset) {
         this.throttleValue = this.maxInitValue;
         this.$timeout((): void => {
             this.lastScrollPosition = 0;
             $(this.scrollUl).scrollTop(0);
         }, 0);
        } else if (!reset) {
            this.throttleValue = (this.throttleValue + limit <= this.maxInitValue) ? this.maxInitValue : this.throttleValue + limit;
        }
    }
    private handleFocusEvents = (event: any): void => {
        this.toggleLimit(true, this.limit);
    }
    private handleScrollEvents = (event: any): void => {
        window.clearTimeout( this.isScrolling );
        const target = event.target || event.srcElement;
        const resetScroll = !(this.boundingSize === target.getBoundingClientRect().top);
        const currentPosition = target.scrollTop;
        const isScrollingUp = currentPosition <= this.lastScrollPosition;
        if (isScrollingUp || this.throttleValue >= this.list.length) {
            return;
        }
        if (target.scrollTop + target.getBoundingClientRect().top >= target.scrollHeight * .80 && !isScrollingUp) {
            this.isLoading = true;
            this.toggleLimit(false, this.limit);
            target.scrollTop = target.scrollHeight / 3;
        }
        this.lastScrollPosition = target.scrollTop;
        // Now lets start our timer to turn off the loading icon.
        this.$timeout((): void => {
            this.isLoading = false;
        }, 750);
    }
    private removeMultiselectPillTab(): void {
        const pillElements: HTMLCollectionOf<Element> = document.getElementsByClassName("ui-select-match-close");
        if (pillElements) {
            forEach(pillElements, (element: HTMLElement): void => {
                element.setAttribute("tabindex", "-1");
            });
        }
    }

    private handleKeyEvents = (event: any): void => {
        if (event && event.keyCode === KeyCodes.Tab) {
            this.onInputBlur(true);
        }
    }

    private onAfterSave(): void {
        this.lastSave = new Date();
        this.selected.latest = this.selected.model;
        this.searchInput.focus();
    }

    private handleEmptyOptionListItem(text: string, isLast: boolean = false): void {
        this.list = filter(this.list, (item: any): boolean => !item.oneEmpty);
        const emptyOption: any = {
            oneEmpty: true,
            oneOther: text || "- - - -",
        };
        emptyOption[this.translationKey ? this.translationKey : this.labelKey] = "";
        if (isLast) {
            this.list.push(emptyOption);
        } else {
            this.list.unshift(emptyOption);
        }
    }

}

const MultiselectTemplate = `
    <div
        class="one-multiselect__container"
        one-click-outside="true"
        one-click-action="$ctrl.onInputBlur()"
        >
        <ui-select
            ng-if="::!$ctrl.allowOther"
            data-id="{{::($ctrl.identifier || $ctrl.uuid)}}"
            class="one-multiselect {{$ctrl.containerClass}}"
            on-select="$ctrl.onSelect($item)"
            on-remove="$ctrl.onRemove($item)"
            ng-disabled="$ctrl.isDisabled"
            ng-model="$ctrl.selected.model"
			close-on-select="false"
			append-to-body="$ctrl.appendToBody"
            skip-focusser="true"
            multiple
            sortable="$ctrl.allowSorting"
            tagging-tokens="{{::$ctrl.taggingTokens}}"
            tag-on-blur="false"
            ui-select-close-on-tab
            ui-select-open-on-focus
            ng-style="$ctrl.isParentModal ? {'z-index' : '1051'} : ''"
            >
            <ui-select-match placeholder="{{::($ctrl.placeholder || '')}}">
                {{ $ctrl.formatItem($item) }}
            </ui-select-match>
            <ui-select-choices repeat="item in $ctrl.list | filter: $select.search | limitTo: $ctrl.throttleValue">
                <div ng-bind-html="(item.oneOther || $ctrl.formatItem(item)) | highlight: $select.search"></div>
            </ui-select-choices>
        </ui-select>
        <ui-select
            ng-if="::$ctrl.allowOther"
            data-id="{{::($ctrl.identifier || $ctrl.uuid)}}"
            class="one-multiselect {{$ctrl.containerClass}}"
            on-select="$ctrl.onSelect($item)"
            on-remove="$ctrl.onRemove($item)"
            ng-disabled="$ctrl.isDisabled"
            ng-model="$ctrl.selected.model"
			close-on-select="false"
			append-to-body="$ctrl.appendToBody"
            skip-focusser="true"
            multiple
            sortable="$ctrl.allowSorting"
            tagging="$ctrl.allowOtherTransform"
            tagging-label="{{::$ctrl.allowOtherLabel}}"
            tagging-tokens="{{::$ctrl.taggingTokens}}"
            tag-on-blur="false"
            ui-select-close-on-tab
            ui-select-open-on-focus
            ng-style="$ctrl.isParentModal ? {'z-index' : '1051'} : ''"
            >
            <ui-select-match placeholder="{{::($ctrl.placeholder || '')}}">
                {{ $ctrl.formatItem($item) }}
            </ui-select-match>
            <ui-select-choices repeat="item in $ctrl.list | filter: $select.search | limitTo: $ctrl.throttleValue">
                <div ng-bind-html="(item.oneOther || $ctrl.formatItem(item)) | highlight: $select.search"></div>
            </ui-select-choices>
        </ui-select>
    </div>
`;

const MultiselectComponent: ng.IComponentOptions = {
    controller: MultiselectController,
    template: MultiselectTemplate,
    bindings: {
        allowEmpty: "<?",
        allowOther: "<?",
        allowOtherLabel: "@?",
        allowOtherLabelAppend: "@?",
        allowSorting: "<?",
        appendToBody: "<?",
        containerClass: "@?",
        emptyOptionText: "@?",
        emptyOptionLast: "<?",
        handleRemove: "&?",
        handleSelect: "&",
        identifier: "@?",
        isDisabled: "<?",
        labelKey: "@?",
        translationKey: "@?",
        list: "<",
        model: "<",
        onBlur: "&?",
        openOnInit: "<?",
        orderByLabel: "<?",
        placeholder: "@?",
        pillTabbing: "<?",
        isParentModal: "<?",
        throttleValue: "<?",
    },
};

export default MultiselectComponent;
