declare var angular: any;

const OneHeaderTemplate = `
    <div class="one-header" ng-class="{'shadow-none':$ctrl.hideShadow}">
        <div class="one-header__bar">
            <div class="one-header__title" ng-transclude="title"></div>
            <div class="one-header__body" ng-transclude="body"></div>
            <div class="one-header__actions" ng-transclude="actions"></div>
        </div>
        <div class="one-header__dropdown" ng-transclude="dropdown"></div>
    </div>
    `;

const OneHeaderComponent: any = {
    template: OneHeaderTemplate,
    bindings: {
        hideShadow: "<?",
    },
    transclude: {
        title: "headerTitle",
        body: "?headerBody",
        actions: "?headerActions",
        dropdown: "?headerDropdown",
    },
};

export default OneHeaderComponent;
