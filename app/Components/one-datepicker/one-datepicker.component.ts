import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IDateTimeOutputModel } from "@onetrust/vitreus";

class OneDatepickerController {
    static $inject: string[] = ["$rootScope"];

    maxDate: Date;
    dateModel: Date;
    isRequired: boolean;
    dateDisableUntil: Date;
    dateDisableSince: Date;
    private defaultMinDate: Date = new Date(0);
    private minDate: Date;
    private currentDate: Date = new Date();
    private onChange: any;
    private placeholder: string;
    private useMinDate: boolean;

    constructor(readonly $rootScope: IExtendedRootScopeService) {}

    $onInit() {
        this.placeholder = this.placeholder || this.$rootScope.t("ChooseDate");
    }

    $onChanges(changes: ng.IOnChangesObject) {
        if (changes.minDate && changes.minDate.currentValue) {
            this.dateDisableUntil = this.getMinDate();
        }
        if (changes.maxDate && changes.maxDate.currentValue) {
            this.dateDisableSince = this.getMaxDate();
        }
    }

    getMinDate(): Date {
        if (this.minDate) {
            return this.minDate;
        } else if (this.useMinDate) {
            return this.defaultMinDate;
        } else {
            return this.currentDate;
        }
    }

    getMaxDate(): Date {
        if (this.maxDate) {
            return this.maxDate;
        } else {
            return this.currentDate;
        }
    }

    dateChanged(date: IDateTimeOutputModel): void {
        this.onChange({ value: date.jsdate });
    }
}

const OneDatepickerComponent = {
    bindings: {
        dateModel: "<?",
        isRequired: "<?",
        label: "@?",
        minDate: "<?",
        maxDate: "<?",
        useMinDate: "<?",
        onChange: "&",
        placeholder: "@?",
        isDisabled: "<?",
        identifier: "@?",
    },
    controller: OneDatepickerController,
    template: `
        <div class="one-datepicker">
            <label class="one-datepicker__label text-medium" ng-if="$ctrl.label" ng-bind-html="$ctrl.label"></label>
            <span class="one-datepicker__required" ng-if="$ctrl.isRequired">*</span>
            <div class="one-datepicker__container">
                <downgrade-one-date-time-picker
                    [identifier]="$ctrl.identifier"
                    [disabled]="$ctrl.isDisabled"
                    [date-disable-until]="$ctrl.dateDisableUntil"
                    [date-disable-since]="$ctrl.dateDisableSince"
                    [date-placeholder]="$ctrl.placeholder"
                    [date-time-model]="$ctrl.dateModel"
                    (on-change)="$ctrl.dateChanged($event)"
                ></downgrade-one-date-time-picker>
            </div>
        </div>
    `,
};

export default OneDatepickerComponent;
