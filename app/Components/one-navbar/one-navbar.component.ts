
const oneNavbar = {
    transclude: true,
    bindings: {
        brand: "@",
    },
    template: () => {
        return `
                <div class="one-navbar">
                    <div class="one-navbar__container">
                        <h4> {{::$root.t($ctrl.brand)}}</h4>
                        <ng-transclude class="one-navbar__buttons"></ng-transclude>
                    </div>
                </div>`;
    },
};

export default oneNavbar;
