const oneButton: ng.IComponentOptions = {
    bindings: {
        buttonClass: "@?",
        buttonTextClass: "@?",
        buttonClick: "&",
        disableTabbing: "<?",
        enableLoading: "<?",
        icon: "@",
        iconClass: "@?",
        rightSideIcon: "@?",
        rightSideIconClass: "@?",
        hasNoBorder: "<?",
        identifier: "@",
        isDisabled: "<?",
        isSelected: "<?",
        isLoading: "<?",
        isCircular: "<?",
        isRounded: "<?",
        isFloating: "<?",
        text: "@",
        type: "@?",
        wrapText: "<?",
        focus: "<?",
        buttonStyle: "<?",
    },
    template: `
        <button
            class="one-button"
            tabindex="{{$ctrl.disableTabbing ? -1 : 0}}"
            ng-class="{
                'one-button--primary': $ctrl.type === 'primary',
                'one-button--select': $ctrl.type === 'select',
                'one-button--radio': $ctrl.type === 'radio',
                'one-button--cta': $ctrl.type === 'cta',
                'one-button--warning': $ctrl.type === 'warning',
                'one-button--danger': $ctrl.type === 'danger',
                'one-button--other': $ctrl.type === 'other',
                'one-button--grey': $ctrl.type === 'grey',
                'one-button--circular': $ctrl.isCircular,
                'one-button--rounded': $ctrl.isRounded,
                'one-button--floating': $ctrl.isFloating,
                'one-button--no-border': $ctrl.hasNoBorder,
                'text-wrap': $ctrl.wrapText,
                'text-nowrap': !$ctrl.wrapText,
                'selected': $ctrl.isSelected,
                '{{$ctrl.buttonClass}}': $ctrl.buttonClass,
            }"
            focus-if="$ctrl.focus"
            ng-click="$ctrl.buttonClick()"
            ng-disabled="$ctrl.isDisabled"
            data-id="{{::$ctrl.identifier}}"
            ng-style="$ctrl.buttonStyle"
            type="button"
        >
            <span class="one-button__container">
                <i
                    ng-if="::$ctrl.icon"
                    ng-class="[$ctrl.icon, $ctrl.iconClass, {'invisible': $ctrl.isLoading}]"
                    >
                </i>
                <span
                    class="one-button__text {{$ctrl.buttonTextClass}}"
                    ng-class="{'invisible': $ctrl.isLoading, 'margin-left-right-half': $ctrl.text && !$ctrl.isLoading}">
                        {{$ctrl.text}}
                </span>
                <i
                    ng-if="::$ctrl.rightSideIcon"
                    ng-class="[$ctrl.rightSideIcon, $ctrl.rightSideIconClass, {'invisible': $ctrl.isLoading}]"
                    >
                </i>
                <span
                    ng-if="$ctrl.enableLoading && $ctrl.isLoading"
                    class="one-button__loading fa fa-spinner fa-pulse fa-fw">
                </span>
            </span>
        </button>`
    ,
};

export default oneButton;
