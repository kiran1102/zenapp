export default {
    bindings: {
        headerTitle: "@",
    },
    transclude: true,
    template: `
            <div class="flex one-accordion">
                <button class="row-space-between one-accordion__button" ng-click="$ctrl.opened = !$ctrl.opened">
                    <span class="padding-right-1 one-accordion__button-title text-nowrap">{{$ctrl.headerTitle}}</span>
                    <i ng-if="!$ctrl.opened" class="ot ot-plus one-accordion__button-icon" aria-hidden="true"></i>
                    <i ng-if="$ctrl.opened" class="ot ot-minus one-accordion__button-icon" aria-hidden="true"></i>
                </button>
                <div class="one-accordion__panel" ng-transclude expand="$ctrl.opened">
                </div>
            </div>
        `,
};
