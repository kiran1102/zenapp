export default {
    bindings: {
        tree: "<",
        childKey: "@?",
        labelKey: "@",
        onToggle: "&",
        isToggled: "&",
        onSelect: "&",
        isSelected: "&",
        isDisabled: "&",
        trackBy: "@?",
    },
    template: `
        <div
            class="row-nowrap row-vertical-center padding-left-right-1"
            ng-repeat="node in $ctrl.tree track by node[$ctrl.trackBy]"
            ng-class="{
                'background-grey border-top-bottom': node[$ctrl.childKey] && node[$ctrl.childKey].length,
                'background-active': $ctrl.isSelected({node: node}),
            }"
            >
            <one-checkbox
                class="padding-all-half"
                identifier="OneTreePickListNodeToggle"
                toggle="$ctrl.onToggle({node: node})"
                is-selected="$ctrl.isToggled({node: node})"
                is-disabled="$ctrl.isDisabled({node: node})"
            ></one-checkbox>
            <div
                class="row-space-between flex margin-left-half padding-top-bottom-1 no-outline"
                ng-click="$ctrl.onSelect({node: node})"
                >
                <span ng-bind="node[$ctrl.labelKey]"></span>
                <span
                    ng-if="node[$ctrl.childKey] && node[$ctrl.childKey].length"
                    class="padding-left-right-1 ot ot-angle-right text-large text-bold"
                ></span>
            </div>
        </div>
    `,
};
