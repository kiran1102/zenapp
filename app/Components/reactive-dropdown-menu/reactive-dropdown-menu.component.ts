import * as _ from "lodash";
import { IReactiveDropdownOption } from "interfaces/reactive-dropdown-option.interface";

class ReactiveDropdownMenu {
    private listClass: string;
    private alignRight: boolean;
    private itemClass: string;
    private onChange: Function;
    private options: IReactiveDropdownOption[];
}

const reactiveDropdownMenuComponent: ng.IComponentOptions = {
    controller: ReactiveDropdownMenu,
    bindings: {
        listClass: "@?",
        alignRight: "<?",
        itemClass: "@?",
        onChange: "&",
        options: "<",
        identifier: "@?",
    },
    template: `
        <ul
            uib-dropdown-menu
            role="menu"
            class="one-dropdown__list {{ $ctrl.listClass }} text-left shadow shadow-transition"
            ng-class="{ 'dropdown-menu-right': $ctrl.alignRight }"
            ng-if="$ctrl.options && $ctrl.options.length"
        >
            <li
                class="one-dropdown__item-wrapper"
                ng-repeat="(index, option) in $ctrl.options track by $index"
                uib-tooltip="{{::option.toolTip }}"
                tooltip-placement="{{::option.toolTipDirection }}"
                tooltip-append-to-body="true"
            >
                <a
                    class="one-dropdown__item {{ ::$ctrl.itemClass }}"
                    ng-class="{'one-dropdown__item--disabled': option.isDisabled}"
                    ng-click="$ctrl.onChange({ value: option.value })"
                    ot-data-id="{{$ctrl.identifier + ($index+1)}}"
                    >
                    <i ng-if="option.icon" class="{{ ::option.icon }} margin-right-half"></i>
                    <span ng-if="option.text || option.textKey">{{(option.text || $root.t(option.textKey) || "")}}</span>
                </a>
            </li>
        </ul>
    `,
};

export default reactiveDropdownMenuComponent;
