import OneBreadcrumbService from "sharedServices/one-breadcrumb.service";
import { IBreadcrumb } from "interfaces/breadcrumb.interface";
import { IStateProvider } from "angular-ui-router";
import { IPromise } from "angular";

class OneNavHeaderController implements ng.IComponentController {
    static $inject: string[] = ["$state", "OneBreadcrumbService"];

    public editBreadcrumbCallback: ({ bodyText: string }) => IPromise<boolean>;

    private breadcrumbs: IBreadcrumb[];
    private useBreadcrumbService: boolean;
    private titleClass: string;
    private breadcrumbClass: string;
    private body: string;
    private showEditIcon = false;
    private editBreadcrumbTitleEnabled: boolean;

    constructor(
        readonly $state: IStateProvider,
        readonly oneBreadcrumbService: OneBreadcrumbService,
    ) {
    }

    public $onInit(): void {
        if (!this.titleClass) {
            this.titleClass = "heading2 text-thin";
        }
        if (!this.breadcrumbClass) {
            this.breadcrumbClass = "text-medium-small";
        }
        if (this.useBreadcrumbService) {
            this.breadcrumbs = this.oneBreadcrumbService.getBreadcrumbs();
        }
    }

    public toggleEdit(edit = false): void {
        this.showEditIcon = edit;
    }

    public saveNewInput(bodyText: string): void {
        this.editBreadcrumbCallback({bodyText}).then((response: boolean) => {
            if (response) {
                this.body = bodyText;
            }
            this.toggleEdit();
        });
    }
}

export const OneNavHeader: ng.IComponentOptions = {
    controller: OneNavHeaderController,
    bindings: {
        title: "@",
        body: "@",
        breadcrumbText: "@",
        breadcrumbRoute: "@",
        breadcrumbIdentifier: "@?",
        useBreadcrumbService: "<",
        titleClass: "@",
        breadcrumbClass: "@",
        headerClass: "@?",
        isDisabled: "<?",
        editBreadcrumbTitleEnabled: "<?",
        editBreadcrumbCallback: "&?",
    },
    template: `
        <nav class="one-nav-header__breadcrumb-header-bar padding-left-right-2 background-grey border-bottom row-vertical-center row-space-between {{$ctrl.headerClass}}">
            <div class="one-nav-header__title">
                <div class="margin-bottom-0 text-thin row-vertical-center" tooltip-placement="bottom" uib-tooltip="{{$ctrl.title}}">
                        <span class="heading4 padding-right-1 max-width-30 overflow-ellipsis-nowrap">{{$ctrl.title}}</span>
                        <span ng-transclude="body"></span>
                </div>
                <div ng-if="!$ctrl.useBreadcrumbService" class="text-medium-small">
                    <a
                        ng-if="$ctrl.breadcrumbText && !$ctrl.isDisabled"
                        class="remove-anchor-style text-decoration-none"
                        ui-sref="{{::$ctrl.breadcrumbRoute}}"
                        ng-bind="$ctrl.breadcrumbText"
                        ot-auto-id="{{::$ctrl.breadcrumbIdentifier}}"
                    ></a>
                    <span ng-if="$ctrl.breadcrumbText && $ctrl.isDisabled">{{$ctrl.breadcrumbText}}</span>
                    <i
                        ng-if="$ctrl.breadcrumbText && $ctrl.body"
                        class="one-nav-header__breadcrumb-header-icon ot ot-chevron-right text-tiny"
                    ></i>
                    <span ng-if="$ctrl.body && !$ctrl.showEditIcon" class="one-nav-header__item-name text-bold text-success">{{$ctrl.body}}</span>
                    <div ng-if="$ctrl.showEditIcon" class="inline-block">
                        <one-editable-text
                            text="{{$ctrl.body}}"
                            input-small="true"
                            show-cancel="true"
                            handle-cancel="$ctrl.toggleEdit()"
                            handle-submit="$ctrl.saveNewInput(text)"
                            ot-auto-id="OneNavHeaderInput">
                        </one-editable-text>
                    </div>
                    <i
                        class="ot ot-pencil-square-o margin-left-half"
                        ng-if="$ctrl.editBreadcrumbTitleEnabled && !$ctrl.showEditIcon"
                        ng-click="$ctrl.toggleEdit(true)"
                        aria-hidden="true"
                        ot-auto-id="OneNavHeaderBreadcrumbTitleEditIcon">
                    </i>
                </div>
                <div ng-if="$ctrl.useBreadcrumbService" class="text-medium-small" ng-class="$ctrl.breadcrumbClass">
                    <a
                        ng-repeat-start="breadcrumb in $ctrl.breadcrumbs track by $index"
                        ng-class="{'text-bold': $last, 'color-green': $last}"
                        class="remove-anchor-style text-decoration-none"
                        ui-sref="{{::breadcrumb.stateName}}"
                        ng-bind="$last && $ctrl.breadcrumbText ? $ctrl.breadcrumbText : $root.t(breadcrumb.text)"
                        ot-auto-id="{{::breadcrumb.identifier}}"
                    ></a>
                    <i
                        ng-if="!$last"
                        class="one-nav-header__breadcrumb-header-icon ot ot-chevron-right text-tiny"
                        ng-repeat-end
                    ></i>
                </div>
            </div>
            <div class="one-nav-header__actions" ng-transclude="actions"></div>
        </nav>
    `,
    transclude: {
        actions: "?headerActions",
        body: "?headerBody",
    },
};
