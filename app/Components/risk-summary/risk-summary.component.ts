// Rxjs
import { Subscription, fromEvent, merge } from "rxjs";
import { startWith, filter } from "rxjs/operators";

// Third Party
import invariant from "invariant";
import {
    forEach,
    assign,
    find,
} from "lodash";

// Core
declare var OneConfirm: any;

// Constants and Enums
import { TemplateTypes } from "enums/template-type.enum";
import { ProjectStateTranslationKeys } from "constants/project-state-translation-keys.constant";
import * as AssessmentActions from "generalcomponent/Assessment/assessment-actions.constants";
import { AssessmentThankYouStates } from "constants/assessment.constants";

// Interfaces
import { IProject } from "interfaces/project.interface";
import {
    IFetchingRisksResponse,
    IRiskSummaryPermissionConfig,
    IRiskHeatMapLabels,
    IHeatMapTile,
} from "interfaces/risk.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import {
    IPaginationParams,
    IPaginationFilter,
} from "interfaces/pagination.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IAssessmentSubmitButtonConfig } from "interfaces/assessment.interface";
import { INextStepsModalData } from "interfaces/next-steps-modal.interface";
import { IHelpDialogModal } from "interfaces/help-dialog-modal.interface";

// Redux
import { AnyAction } from "redux";
import observableFromStore from "oneRedux/utils/observable-from-store";

import { getUserById } from "oneRedux/reducers/user.reducer";
import {
    getRiskSettings,
    getProjectSettings,
    isRiskSummaryEnabled,
} from "oneRedux/reducers/setting.reducer";
import {
    getAllRisksInOrder,
    getPendingRiskModels,
    getNonPendingRiskModels,
} from "oneRedux/reducers/risk.reducer";

// Services
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";
import RiskBusinessLogicService from "oneServices/logic/risk-business-logic.service";
import { ProjectService } from "oneServices/project.service";
import { ModalService } from "app/scripts/Shared/Services/modal.service";
import AssessmentFooterViewLogicService from "generalcomponent/Assessment/assessment-footer-view-logic.service";
import AssessmentBusinessLogicService from "generalcomponent/Assessment/assessment-business-logic.service";

class RiskSummary {

    static $inject: string[] = [
        "store",
        "$rootScope",
        "$q",
        "$timeout",
        "$state",
        "$stateParams",
        "HeatmapSettings",
        "SettingAction",
        "RiskAction",
        "RiskBusinessLogic",
        "Projects",
        "ENUMS",
        "ModalService",
        "Permissions",
        "PRINCIPAL",
        "Labels",
        "Users",
        "AssessmentFooterViewLogic",
        "AssessmentBusinessLogic",
    ];

    private dataLoaded = false;

    private batchedUnsub: Subscription[] = [];
    private shadowForHeaderGroupEnabled = false;

    private sub: Subscription;

    private permissions: IRiskSummaryPermissionConfig;
    private translations: any;

    private projectModel: IProject;
    private projectStateLabelText: string;
    private riskModels: any[] = [];
    private totalRiskCount = 0;
    private selectedRiskCount = 0;
    private hasSelectedRisks = false;
    private sortByOptions: IDropdownOption[];
    private selectedSortBy: IDropdownOption;
    // private groupByOptions: IDropdownOption[];
    // private selectedGroupBy: IDropdownOption;
    private isSubmitting: boolean;
    private footerViewState: IAssessmentSubmitButtonConfig;
    private currentUser: IUser;
    private pendingRiskModels: any;
    private nonPendingRiskModels: any;
    private pageMetadata: IPaginationParams<IPaginationFilter[]> = {
        page: 0,
        size: 9999,
        sort: {
            deadline: "asc",
        },
    };

    private heatmapEnabled: boolean;
    private axisLabels: IRiskHeatMapLabels;
    private tileMap: IHeatMapTile[][];

    constructor(
        private readonly store: IStore,
        private readonly $rootScope: any,
        private readonly $q: ng.IQService,
        private readonly $timeout: ng.ITimeoutService,
        private readonly $state: ng.ui.IStateService,
        private readonly $stateParams: ng.ui.IStateParamsService,
        private readonly HeatmapSettings: any,
        private readonly SettingAction: SettingActionService,
        private readonly RiskAction: RiskActionService,
        private readonly RiskBusinessLogic: RiskBusinessLogicService,
        private readonly Projects: ProjectService,
        private readonly ENUMS: any,
        private readonly modalService: ModalService,
        private readonly Permissions: any,
        PRINCIPAL: any,
        private readonly Labels: any,
        private readonly Users: any,
        private readonly AssessmentFooterViewLogic: AssessmentFooterViewLogicService,
        private readonly AssessmentBusinessLogic: AssessmentBusinessLogicService,
    ) {
        this.currentUser = PRINCIPAL.getUser();
    }

    $onInit(): void {
        const promises = [];

        if (!getRiskSettings(this.store.getState())) {
            promises.push(this.SettingAction.fetchRiskSettings());
        }

        if (!getProjectSettings(this.store.getState()) && !this.Permissions.canShow("Assessments")) {
            promises.push(this.SettingAction.fetchProjectSettings());
        }

        this.$q.all(promises).then((): void => {
            this.permissions = this.RiskBusinessLogic.getRiskSummaryPermissions();
            this.translations = this.RiskBusinessLogic.getRiskSummaryTranslations();

            this.heatmapEnabled = getRiskSettings(this.store.getState()).Enabled;

            this.sortByOptions = this.RiskBusinessLogic.getRiskSortByOptions();
            this.selectedSortBy = this.sortByOptions[0];
            this.pageMetadata.sort = assign(this.pageMetadata.sort, this.selectedSortBy.value);

            // TODO uncomment this once grouping in risk summary is enabled in the future release
            // this.groupByOptions = this.RiskBusinessLogic.getRiskGroupByOptions();
            // this.selectedGroupBy = this.groupByOptions[0];

            const { axisLabels, tileMap }: { axisLabels: IRiskHeatMapLabels, tileMap: IHeatMapTile[][] } = this.RiskBusinessLogic.buildRiskTileMap(4, 4);
            this.axisLabels = axisLabels;
            this.tileMap = tileMap;
        }).then((): ng.IPromise<void> => {
            const { ProjectId, Version } = this.$state.params;

            return this.RiskAction.fetchRisksByProjectId(ProjectId, Version, this.pageMetadata).then((res: IFetchingRisksResponse): void => {
                if (res) {
                    this.projectModel = res.Project;
                    this.subscribeToStore();
                }
                this.dataLoaded = true;
            });
        });
    }

    $onDestroy(): void {
        forEach(this.batchedUnsub, (sub: Subscription): void => sub.unsubscribe());
        if (this.sub) this.sub.unsubscribe();
    }

    private subscribeToStore(): void {
        this.sub = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            filter((state: IStoreState) => Boolean(state.settings.projectSettings)),
        ).subscribe((): void => this.componentWillReceiveState());
    }

    private isNeedAttentionSortSelected(): boolean {
        return this.selectedSortBy.value === "pendingAssessment";
    }

    private componentWillReceiveState(): undefined | void {
        this.riskModels = getAllRisksInOrder(this.store.getState());
        this.totalRiskCount = this.riskModels.length;

        if (this.isNeedAttentionSortSelected()) {
            this.pendingRiskModels = getPendingRiskModels(this.store.getState());
            this.nonPendingRiskModels = getNonPendingRiskModels(this.store.getState());
        } else {
            this.pendingRiskModels = getPendingRiskModels(this.store.getState());
            this.nonPendingRiskModels = this.riskModels;
        }

        // TODO enable this once multiselect risk is enabled
        // const selectedRisksMap: IStringToBooleanMap = getSelectedRiskIds(this.store.getState());
        // this.selectedRiskCount = keys(selectedRisksMap).length;
        // this.hasSelectedRisks = this.selectedRiskCount > 0;

        if (this.projectModel) {
            this.footerViewState = this.AssessmentFooterViewLogic.calculateAssessmentFooterButtonState(this.currentUser, this.projectModel, this.riskModels);
            this.projectStateLabelText = ProjectStateTranslationKeys[this.projectModel.StateDesc];
        }
    }

    private initScrollListenerForShadow(): boolean | void {
        const elementToListenTo: Element = document.getElementById("on-scroll-listener-id");
        const elementToShowShadow: Element = document.getElementById("header-group-id-for-shadow");
        const scroll$ = fromEvent(elementToListenTo, "scroll");
        const drag$ = fromEvent(elementToListenTo, "drag");
        const scrollOrDrag$ = merge(scroll$, drag$);

        if (!elementToListenTo && !elementToShowShadow) return false;

        const scrollOrDragSub: Subscription = scrollOrDrag$.subscribe((e: any): void => {
            if (e.target.scrollTop > 0 && !this.shadowForHeaderGroupEnabled) {
                // the reason class is directly toggled is because if you use ng-class, you have to trigger a digest cycle.
                elementToShowShadow.classList.add("shadow-bottom-6");
                this.shadowForHeaderGroupEnabled = true;
            } else if (e.target.scrollTop === 0 && this.shadowForHeaderGroupEnabled) {
                elementToShowShadow.classList.remove("shadow-bottom-6");
                this.shadowForHeaderGroupEnabled = false;
            }
        });

        this.batchedUnsub.push(scrollOrDragSub);
    }

    // TODO enable once multiselect is enabled
    // private initKeyListeners(): void {
    //     const keyboard$: Observable<KeyboardEvent> = fromEvent<KeyboardEvent>(document, "keydown");
    //     const keyboardSub: Subscription = keyboard$.subscribe((e: KeyboardEvent): void => {
    //         if (e.keyCode === 27) {
    //             if (keys(getSelectedRiskIds(this.store.getState())).length) {
    //                 this.unSelectAllRisks();
    //             }
    //         }
    //     });
    //     this.batchedUnsub.push(keyboardSub);
    // }
    //
    // private unSelectAllRisks(): void {
    //     this.RiskAction.unSelectAllRisks();
    // }

    private goToProjectList(): void {
        // TODO uncomment this when multiselect risk is enabled
        // this.unSelectAllRisks();
        const projectListState = this.projectModel.TemplateType === TemplateTypes.Datamapping
            ? "zen.app.pia.module.datamap.views.assessment.list"
            : "zen.app.pia.module.projects.list";

        this.$state.go(projectListState);
    }

    private goBackToProject(): void {
        const projectState = this.projectModel.TemplateType === TemplateTypes.Datamapping
            ? "zen.app.pia.module.datamap.views.assessment-old.single.module.questions"
            : "zen.app.pia.module.projects.assessment.module.questions";

        this.$state.go(projectState, {
            Id: this.$stateParams.ProjectId,
            Version: this.$stateParams.Version,
        });
    }

    // TODO move this to assessment action service
    private goToNeedMoreInfo(projectModel: IProject): void {
        const data: any = {
            Project: projectModel,
            Labels: this.Labels,
            TemplateType: this.ENUMS.TemplateTypes.Custom,
        };
        const modalInstance: any = this.modalService.renderModal(data, "SendBackCtrl", "SendBackModal.html", "sendBackModal");

        const destroySendBackModalCb: any = this.$rootScope.$on("sendBackModalCb", (event: ng.IAngularEvent, args: any): void => {
            if (args.response && this.Permissions.canShow("ProjectsList")) {
                this.goToProjectList();
            }
            destroySendBackModalCb();
        });
    }

    // TODO move this to assessment action service
    private goToUnderReviewFromNeedMoreInfo(projectModel: IProject): void {
        OneConfirm("", this.$rootScope.t("AreYouSureSubmitAssessment")).then((result: boolean): undefined | void => {
            if (!result) return;

            this.Projects.submitProject(projectModel.Id, projectModel.Version).then((response: IProtocolPacket): undefined | void => {
                if (response.result) {
                    if (!this.Permissions.canShow("ProjectsList")) {
                        this.$state.go("zen.app.pia.module.thanks", {
                            options: {
                                state: AssessmentThankYouStates.Complete,
                                projectName: projectModel.Name,
                                reviewerId: projectModel.ReviewerId,
                                canViewProjects: this.Permissions.canShow("ProjectsList"),
                            },
                            projectId: projectModel.Id,
                            templateType: this.ENUMS.TemplateTypes.Custom,
                            projectVersion: projectModel.Version,
                        });
                        return;
                    } else {
                        const modalData: IHelpDialogModal = {
                            modalTitle: this.$rootScope.t("Success"),
                            content: this.$rootScope.t("ProjectSubmittedApproversNotified"),
                            hideClose: !this.Permissions.canShow("ProjectsList"),
                        };
                        if (projectModel.ReviewerId) {
                            const Reviewer: IUser = getUserById(projectModel.ReviewerId)(this.store.getState()) || null;
                            if (!Reviewer) return;
                            modalData.content = this.$rootScope.t("ProjectSubmittedToReviewerEmailApproverDuplicate", {
                                ProjectTerm: this.$rootScope.customTerms.Project,
                                Reviewer: "<strong>" + projectModel.Reviewer + "</strong>",
                                ReviewerEmail: "<a href='mailto:" + Reviewer.Email + "?Subject=" + encodeURI(projectModel.Name) + "'>" + Reviewer.Email + "</a>",
                            });
                        }
                        this.modalService.setModalData(modalData);
                        this.modalService.openModal("helpDialogModal");
                        if (this.Permissions.canShow("ProjectsList")) {
                            this.goToProjectList();
                        }
                    }
                }
            });
        });
    }

    // TODO move this to assessment actions service
    private transitionToRiskSummary(projectModel: IProject): void {
        this.$state.go("zen.app.pia.module.projects.risk_summary", {
            ProjectId: projectModel.Id,
            Version: projectModel.Version,
        }, { reload: "zen.app.pia.module.projects.risk_summary" });
    }

    // TODO move this to assessment actions service
    private goToRiskAssessment(projectModel: IProject): void {
        const data = {
            Project: projectModel,
        };
        const modalInstance: any = this.modalService.renderModal(data, "AnalysisModalCtrl", "AnalysisModal.html", "analysisModal");
        this.$rootScope.$on("analysisModalCb", (event: ng.IAngularEvent, args: any): void => {
            if (args.cbInitiated) {
                this.transitionToRiskSummary(projectModel);
            }
        });
    }

    private openNextStepsModal(projectModel: IProject): void {
        const isRiskSummaryOn: boolean = isRiskSummaryEnabled(this.store.getState());
        const data: INextStepsModalData = {
            Project: projectModel,
            EnableRiskAnalysis: isRiskSummaryOn && this.AssessmentBusinessLogic.flaggedRiskExists(projectModel),
        };
        const modalInstance: any = this.modalService.renderModal(data, "NextStepsController", "NextStepsModal.html", "nextStepsModal");

        const destroyNextStepsModalCb: any = this.$rootScope.$on("nextStepsModalCb", (e: any, args: any): void => {
            if (args.response) {
                if (args.response.data === 1 && isRiskSummaryOn && this.AssessmentBusinessLogic.flaggedRiskExists(projectModel)) {
                    this.goToRiskAssessment(projectModel);
                }
                if (args.response.data === 1 && (!isRiskSummaryOn || !this.AssessmentBusinessLogic.flaggedRiskExists(projectModel))) {
                    projectModel.IsLinked = false;
                    this.store.dispatch({ type: "UNHANDLED_ACTION: assessment controller" }); // done here to trigger the footer change
                }
            }
            destroyNextStepsModalCb();
        });
    }

    // TODO move this to assessment action service
    private goToRiskTreatment(projectModel: IProject): ng.IPromise<any> {
        return this.Projects.mitigateProject(projectModel.Id, projectModel.Version).then((response: IProtocolPacket): undefined | void => {
            if (!response.result) {
                return;
            }
            if (this.Permissions.canShow("ProjectsList")) {
                this.goToProjectList();
            }
            const modalData: IHelpDialogModal = {
                modalTitle: this.$rootScope.t("RecommendationSent", { Recommendation: this.$rootScope.customTerms.Recommendation }),
                content: this.$rootScope.t("RecommendationSentToRiskOwner", {
                    Recommendations: this.$rootScope.customTerms.Recommendations,
                    Recommendation: this.$rootScope.customTerms.Recommendation }),
            };
            this.modalService.setModalData(modalData);
            this.modalService.openModal("helpDialogModal");
        });
    }

    // TODO move this to assessment action service
    private completeProjectFromUnderReview(projectModel: IProject): void {
        OneConfirm("", this.$rootScope.t("AreYouSureYouWantToApproveThisAssessment")).then((result: boolean): undefined | void => {
            if (!result) return;

            this.Projects.completeProject({ Id: projectModel.Id, ProjectVersion: projectModel.Version }).then((response: IProtocolPacket): undefined | void => {
                if (!response.result) {
                    return;
                }
                if (this.Permissions.canShow("ProjectsList")) {
                    this.goToProjectList();
                }
                const modalData: IHelpDialogModal = {
                    modalTitle: this.$rootScope.t("Approved"),
                    content: this.$rootScope.t("YouApprovedProjectEmailSentToTeamDuplicate", {
                        ProjectTerm: this.$rootScope.customTerms.Project,
                    }),
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("helpDialogModal");
            });
        });
    }

    // TODO move this to assessment action service
    private completeProjectFromRiskTreatment(projectModel: IProject): void {
        OneConfirm("", this.$rootScope.t("SureToEndAnalysisAndCompleteProject")).then((result: boolean): undefined | void => {
            if (!result) return;

            this.Projects.completeProject({ Id: projectModel.Id, ProjectVersion: projectModel.Version }).then((response: IProtocolPacket): undefined | void => {
                if (!response.result) {
                    return;
                }
                if (this.Permissions.canShow("ProjectsList")) {
                    this.goToProjectList();
                }
                const modalData: IHelpDialogModal = { modalTitle: this.$rootScope.t("Approved"), content: this.$rootScope.t(
                        "YouApprovedProjectEmailSentToTeamDuplicate",
                        { ProjectTerm: this.$rootScope.customTerms.Project },
                    ) };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("helpDialogModal");
            });
        });
    }

    // TODO move this to assessment action service
    private handleSubmitAction(actionType: AnyAction): void {
        const { type: action } = actionType;
        const { SUBMIT_ASSESSMENT, SEND_MITIGATIONS, SEND_BACK_MODAL, APPROVE_ASSESSMENT, OPEN_NEXT_STEPS_MODAL, OPEN_ANALYSIS_MODAL, SEND_RECOMMENDATIONS, COMPLETE_ANALYSIS } = AssessmentActions;

        switch (action) {
            case SEND_BACK_MODAL:
                this.goToNeedMoreInfo(this.projectModel);
                break;
            case SUBMIT_ASSESSMENT:
                this.goToUnderReviewFromNeedMoreInfo(this.projectModel);
                break;
            case OPEN_ANALYSIS_MODAL:
                this.goToRiskAssessment(this.projectModel);
                break;
            case OPEN_NEXT_STEPS_MODAL:
                this.openNextStepsModal(this.projectModel);
                break;
            case SEND_RECOMMENDATIONS:
                this.isSubmitting = true;
                this.goToRiskTreatment(this.projectModel).then(() => {
                    this.isSubmitting = false;
                });
                break;
            case APPROVE_ASSESSMENT:
                this.completeProjectFromUnderReview(this.projectModel);
                break;
            case COMPLETE_ANALYSIS:
                this.completeProjectFromRiskTreatment(this.projectModel);
                break;
            default:
                invariant(false, "The footer action must be defined.");
        }
    }

    private handleSortFilter(value, key): undefined | void {
        if (this.selectedSortBy.value === value) return;

        this.selectedSortBy = find(this.sortByOptions, (option: IDropdownOption): boolean => option.value === value);
        this.pageMetadata.sort = value;

        if (this.isNeedAttentionSortSelected()) {
            this.dataLoaded = false;
            this.$timeout(() => {
                this.componentWillReceiveState();
                this.dataLoaded = true;
            }, 250);
        } else {
            this.dataLoaded = false;
            this.RiskAction.fetchRisksByProjectId(this.projectModel.Id, this.projectModel.Version, this.pageMetadata).then((res: IFetchingRisksResponse): void => {
                this.dataLoaded = true;
            });
        }
    }
}

export default {
    controller: RiskSummary,
    template: require("./risk-summary.component.html"),
};
