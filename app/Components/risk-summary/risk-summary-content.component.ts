// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Third Party
import invariant from "invariant";
import {
    isNumber,
    assign,
} from "lodash";

// Constants
import { RiskSingleEditModalActions } from "constants/risk-single-edit-modal-actions.constant";

// Enums
import { RiskStates } from "enums/risk-states.enum";

// Interfaces
import {
    IRisk,
    IRiskSummaryContentViewConfig,
} from "interfaces/risk.interface";
import { IStore } from "interfaces/redux.interface";
import { IProject } from "interfaces/project.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion } from "interfaces/question.interface";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getUserById } from "oneRedux/reducers/user.reducer";
import {
    formatDate,
    isAnyPropChanged,
    areAllPropsDefined,
} from "oneRedux/utils/utils";
import {
    getRiskById,
    getTempRiskById,
} from "oneRedux/reducers/risk.reducer";

// Services
import { RiskActionService } from "oneServices/actions/risk-action.service";
import RiskSummaryContentViewLogicService, { RiskSummaryContentActions } from "oneServices/view-logic/risk-summary-content-view-logic.service";
import RiskBusinessLogic from "oneServices/logic/risk-business-logic.service";
import ProjectQuestionService from "oneServices/project-question.service";
import { RiskAPIService } from "oneServices/api/risk-api.service";
import RiskPermissionService from "oneServices/permissions/risk-permission.service";
import AssessmentQuestionViewLogicService from "generalcomponent/Assessment/assessment-question-view-logic.service";

class RiskSummaryContent {

    static $inject: string[] = [
        "store",
        "ENUMS",
        "RiskAction",
        "RiskAPI",
        "RiskBusinessLogic",
        "RiskSummaryContentViewLogic",
        "RiskPermission",
        "PRINCIPAL",
        "ProjectQuestions",
        "Answer",
        "Questions",
        "AssessmentQuestionViewLogic",
    ];

    // inherited prop
    private hoverEffectEnabled: boolean;
    private riskModelId: string;
    private actionCallback: Function;
    private enableStickyCheckbox: boolean;
    private permissions: any;
    private translations: any;
    private projectModel: IProject;

    // internal prop
    private sub: Subscription;
    private riskModel: IRisk;
    private tempRiskModel: IRisk;
    private isPanelExpanded = false;
    private isCheckboxToggled = false;
    private deadline: string;
    private riskOwnerName: string;
    private viewState: IRiskSummaryContentViewConfig;
    // private isTextareaEnabled: boolean;
    // private bottomSectionEnabled: boolean;
    // private isSubmittingTextarea: boolean;
    // private textareaSubmitEnabled: boolean;
    private currentUser: IUser;
    private isRemovingProp: boolean;
    private isFetchingQuestion: boolean;
    private riskRelatedQuestion: IQuestion;
    private questionViewState: any;
    private questionEvents: any;

    constructor(
        private readonly store: IStore,
        private readonly ENUMS: any,
        private readonly RiskAction: RiskActionService,
        private readonly RiskAPI: RiskAPIService,
        private readonly RiskBusinessLogic: RiskBusinessLogic,
        private readonly RiskSummaryContentViewLogic: RiskSummaryContentViewLogicService,
        private readonly RiskPermission: RiskPermissionService,
        PRINCIPAL: any,
        private readonly ProjectQuestions: ProjectQuestionService,
        private readonly Answer: any,
        private readonly Questions: any,
        private readonly AssessmentQuestionViewLogic: AssessmentQuestionViewLogicService,
    ) {
        this.currentUser = PRINCIPAL.getUser();
        this.questionEvents = {
            multiChoiceGroupSelected: Questions.dmIsQuestionSelected,
        };
    }

    $onInit(): void {
        this.sub = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((): void => this.componentWillReceiveState());
    }

    $onDestroy(): void {
        this.sub.unsubscribe();
    }

    private componentWillReceiveState(): void | undefined {
        const riskModelFromStore: IRisk = getRiskById(this.riskModelId)(this.store.getState()) as IRisk;
        const tempRiskModelFromStore: IRisk | undefined = getTempRiskById(this.riskModelId)(this.store.getState()) as IRisk;

        if (!riskModelFromStore) return;

        // optimization. we don't want to run this logic for other risk models
        if (this.riskModel === riskModelFromStore && this.tempRiskModel === tempRiskModelFromStore) return;

        // TODO enabled in the later release when multiselect risk feature is enabled
        // this.isCheckboxToggled = Boolean( getSelectedRisksMap(this.store.getState())[riskModelFromStore.Id] );

        this.deadline = riskModelFromStore.Deadline ? formatDate(riskModelFromStore.Deadline, "L") : this.translations.noDeadline;

        const isRiskOwnerEmpty: boolean = riskModelFromStore.RiskOwnerId === this.ENUMS.EmptyGuid;
        if (riskModelFromStore.RiskOwnerId && !isRiskOwnerEmpty) {
            this.riskOwnerName = getUserById(riskModelFromStore.RiskOwnerId)(this.store.getState()).FullName;
        } else {
            this.riskOwnerName = this.translations.unassigned;
        }

        if (riskModelFromStore) {
            this.viewState = this.RiskSummaryContentViewLogic.calculateNewViewState(this.currentUser, this.projectModel, riskModelFromStore, tempRiskModelFromStore);
        }

        this.riskModel = riskModelFromStore;
        this.tempRiskModel = tempRiskModelFromStore;
    }

    private toggleCheck(): void {
        if (this.isCheckboxToggled) {
            this.RiskAction.unSelectRisk(this.riskModel.Id);
        } else {
            this.RiskAction.selectRisk(this.riskModel.Id);
        }
    }

    private fetchQuestionThenSaveResponse(): void {
        if (!this.isFetchingQuestion) {
            this.isFetchingQuestion = true;
            this.ProjectQuestions.getState(this.riskModel.QuestionId).then((res) => {
                if (res.result) {
                    this.riskRelatedQuestion = this.Answer.handleResponses(null, res.data, null);
                    this.questionViewState = this.AssessmentQuestionViewLogic.calculateNewViewState(this.projectModel, this.riskRelatedQuestion);
                } else {
                    this.riskRelatedQuestion = null;
                }
                this.isFetchingQuestion = false;
            });
        }
    }

    private toggleExpand(): void {
        this.isPanelExpanded = !this.isPanelExpanded;

        if (this.isPanelExpanded && !this.riskRelatedQuestion) {
            this.fetchQuestionThenSaveResponse();
        }
    }

    private handleRiskLevelChange(riskLevel: number): void {
        invariant(isNumber(riskLevel), "Risk level must be a number.");

        if (this.riskModel.Level !== riskLevel) {
            const risk: IRisk = assign({}, this.riskModel, { Level: riskLevel });
            this.RiskAction.saveRiskLevel(risk);
        }
    }

    private handleRiskTileChange(riskTile: any): void {
        const { riskLevel: Level, yValue: RiskImpactLevel, xValue: RiskProbabilityLevel } = riskTile;
        const propsToCheck: string[] = ["Level", "RiskImpactLevel", "RiskProbabilityLevel"];
        const newRisk: IRisk = assign({}, this.riskModel, { Level, RiskImpactLevel, RiskProbabilityLevel });

        invariant(areAllPropsDefined(propsToCheck)(newRisk), "All three risk props must be defined in the model.");

        if (isAnyPropChanged(propsToCheck)(this.riskModel, newRisk)) {
            this.RiskAction.saveRiskTile(newRisk);
        }
    }

    private handleButtonAction(actionType: string): void {
        switch (actionType) {
            case "EXPAND_CARD":
                this.toggleExpand();
                break;
            case RiskSummaryContentActions.OPEN_RISK_MODAL:
                this.RiskAction.openEditRiskModal(this.riskModel.Id, { projectModel: this.projectModel });
                break;
            case RiskSummaryContentActions.ADD_RISK_RECOMMENDATION:
                this.RiskAction.openSingleEditRiskModal(this.riskModel.Id, { modalAction: RiskSingleEditModalActions.ADD_RISK_RECOMMENDATION });
                break;
            case RiskSummaryContentActions.ADD_RISK_REMEDIATION:
                this.RiskAction.openSingleEditRiskModal(this.riskModel.Id, { modalAction: RiskSingleEditModalActions.ADD_RISK_REMEDIATION });
                break;
            case RiskSummaryContentActions.ADD_RISK_EXCEPTION:
                this.RiskAction.openSingleEditRiskModal(this.riskModel.Id, { modalAction: RiskSingleEditModalActions.ADD_RISK_EXCEPTION });
                break;
            case RiskSummaryContentActions.ACCEPT_RISK_REMEDIATION:
                this.RiskAction.openAcceptRiskModal(this.riskModel.Id, RiskStates.Addressed);
                break;
            case RiskSummaryContentActions.ACCEPT_RISK_EXCEPTION:
                this.RiskAction.openAcceptRiskModal(this.riskModel.Id, RiskStates.Exception);
                break;
            case RiskSummaryContentActions.REJECT_RISK_REMEDIATION:
                this.RiskAction.openRejectRiskModal(this.riskModel.Id, RiskStates.Addressed);
                break;
            case RiskSummaryContentActions.REJECT_RISK_EXCEPTION:
                this.RiskAction.openRejectRiskModal(this.riskModel.Id, RiskStates.Exception);
                break;
            case RiskSummaryContentActions.DELETE_RISK_RECOMMENDATION:
                this.RiskAction.openDeletePropConfirmModal(this.riskModel.Id, "Recommendation");
                break;
            case RiskSummaryContentActions.DELETE_RISK_REMEDIATION:
                this.RiskAction.openDeletePropConfirmModal(this.riskModel.Id, "Remediation");
                break;
            case RiskSummaryContentActions.DELETE_RISK_EXCEPTION:
                this.RiskAction.openDeletePropConfirmModal(this.riskModel.Id, "RequestedException");
                break;
            case RiskSummaryContentActions.DELETE_RISK_MODEL:
                this.RiskAction.openDeleteRiskModal(this.riskModel.Id, this.riskModel.QuestionId);
                break;
        }
    }
}

export default {
    controller: RiskSummaryContent,
    bindings: {
        riskModelId: "@",
        enableStickyCheckbox: "<?",
        actionCallback: "&?",
        permissions: "<?",
        axisLabels: "<?",
        tileMap: "<?",
        translations: "<",
        projectModel: "<?",
        heatmapEnabled: "<?",
    },
    template: require("./risk-summary-content.component.html"),
};
