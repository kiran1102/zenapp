import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IScope, IAngularEvent } from "angular";

class OneExpandablePanelController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope"];

    private translate: any;
    private panelWidth: string;
    private rightPos: string;
    private hidden = true;

    constructor(readonly $scope: IScope, $rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
        this.rightPos = `-${this.panelWidth}`;
    }

    public $onInit(): void {
        this.$scope.$on("PANEL_SHOW", (event: IAngularEvent, args: any[]) => {
            this.show();
        });

        this.$scope.$on("PANEL_HIDE", (event: IAngularEvent, args: any[]) => {
            this.hide();
        });
    }

    private show(): void {
        this.hidden = false;
        this.rightPos = "0rem";
    }

    private hide(): void {
        this.rightPos = `-${this.panelWidth}`;
        this.hidden = true;
    }
}

export default {
    controller: OneExpandablePanelController,
    bindings: {
        panelWidth: "@",
    },
    transclude: true,
    template: `
        <div class="one-expandable-panel position-absolute height-100 top-0
            padding-all-2 shadow-left-6"
            ng-style="{'width': $ctrl.panelWidth, 'right': $ctrl.rightPos, 'margin-right': $ctrl.hidden ? '-1rem' : '0'}"
            >
            <div class="text-bold margin-bottom-2">
                <i class="fa fa-arrow-circle-o-right margin-right-1" ng-click="$ctrl.hide()"></i>
                <a class="remove-anchor-style" ng-click="$ctrl.hide()">{{::$ctrl.translate("Close")}}</a>
            </div>
            <ng-transclude></ng-transclude>
        </div>`,
};
