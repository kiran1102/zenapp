import $ from "jquery";
import JQuery from "jquery";

class EditableTextController implements ng.IComponentController {
    static $inject: string[] = ["$timeout"];

    private text: string;
    private oldText: string;

    private textArea: boolean;
    private handleSubmit: any;

    constructor(readonly $timeout: ng.ITimeoutService) {}

    $onInit() {
        this.oldText = this.text;

        this.$timeout(() => {
            const inputId: string = this.textArea ? "one-editable-text__textarea" : "#one-editable-text__input";
            const input: JQuery = $(inputId);
            if (input && input[0]) input[0].focus();
        });
    }

    public submit(): void {
        this.oldText = this.text;
        this.handleSubmit({text: this.text});
    }
}

const editableTextComponent: ng.IComponentOptions = {
    bindings: {
        text: "@",
        blockClass: "@",
        handleSubmit: "&?",
        handleCancel: "&?",
        handleChange: "&",
        textArea: "<",
        disabled: "<",
        inputClass: "@?",
        inputSmall: "<?",
    },
    controller: EditableTextController,
    template: `
        <form class="one-editable-text"
            ng-submit="$ctrl.submit()"
            >
            <div
                class="one-editable-text__group {{$ctrl.blockClass}}"
                ng-class="{'one-editable-text__group--column': $ctrl.textArea}"
                >
                <input
                    id="one-editable-text__input"
                    class="one-editable-text__controller one-editable-text__input one-input {{::$ctrl.inputClass}}"
                    ng-class="{ 'one-input--small': $ctrl.inputSmall }"
                    ng-if="!$ctrl.textArea"
                    ng-model="$ctrl.text"
                    ng-change="$ctrl.handleChange({text: $ctrl.text})"
                />
                <textarea
                    id="one-editable-text__textarea"
                    class="one-editable-text__controller one-editable-text__textarea one-input {{::$ctrl.inputClass}}"
                    ng-if="$ctrl.textArea"
                    ng-model="$ctrl.text"
                    ng-change="$ctrl.handleChange({text: $ctrl.text})"
                />
                <div ng-if="!$ctrl.disabled" class="flex one-editable-text__buttons-group"
                     ng-class="{'border-round-with-textarea': $ctrl.textArea, 'border-round-with-input': !$ctrl.textArea}"
                     >
                    <div class="one-button-group">
                        <one-button
                            ng-if="::$ctrl.handleSubmit"
                            button-class="{{$ctrl.inputSmall ? 'one-button--small' : 'one-button--slim'}} one-button--green-icon"
                            icon="ot fa-fw ot-check"
                            button-click="$ctrl.submit()"
                            identifier="editableTextSubmit"
                            >
                        </one-button>
                        <one-button
                            ng-if="::$ctrl.handleCancel"
                            button-class="{{$ctrl.inputSmall ? 'one-button--small' : 'one-button--slim'}} one-button--red-icon"
                            icon="ot fa-fw ot-times"
                            button-click="$ctrl.handleCancel({text: $ctrl.oldText})"
                            identifier="editableTextCancel"
                            >
                        </one-button>
                    </div>
                </div>
            </div>
        </form>
    `,
};

export default editableTextComponent;
