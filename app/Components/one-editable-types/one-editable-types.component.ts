import { some, toLower } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionOption } from "interfaces/question-options.interface";

class EditableTypes implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "$timeout", "ENUMS", "amDateFormatFilter"];

    private translate: any;
    private editorShowing = false;
    private inputTypes: any;
    private label: string | Date;
    private model: any;
    private handleEdit: any;
    private config: any;
    private options: any;
    private clickToClose: boolean;
    private buttonType = "select";
    private buttonClass: string;
    private isButtonRounded = false;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, readonly $timeout: ng.ITimeoutService, ENUMS: any, readonly amDateFormatFilter: any) {
        this.inputTypes = ENUMS.InputTypes;
        this.translate = $rootScope.t;
    }

    public openEditor(): void {
        if (this.config.inputType === this.inputTypes.Modal) {
            this.launchModal();
        } else {
            this.editorShowing = true;
            this.$timeout( (): void => {
                this.clickToClose = true;
            });
        }
    }

    public toggleEditor(otherButton: boolean = false): void {
        this.editorShowing = !this.editorShowing;
        if (!otherButton) {
            this.clickToClose = !this.clickToClose;
        }
    }

    public updateSelection(selection): void {
        this.editorShowing = false;
        this.clickToClose = false;
        this.model = selection;
        this.setLabel();
        this.handleEdit({ model: this.model });
        if (this.config.clearOnSelect) {
            this.model = null;
            this.setLabel();
        }
    }

    public $onChanges(): void {
        this.setLabel();
    }

    private setLabel(): void {
        if (this.config.inputType === this.inputTypes.Date) {
            this.label = this.model ? new Date(this.model) : null;
        } else {
            if (this.model) {
                const isExistingOption: boolean = some(this.options, (option: IQuestionOption) => toLower(option.value) === toLower(this.model));
                if (isExistingOption) return;
                this.label = this.config.labelKey && this.model[this.config.labelKey] ? this.model[this.config.labelKey] : this.model;
            } else if (this.config.isButton && !this.model) {
                this.label = null;
            } else {
                this.label = "- - - -";
            }
        }
    }

    private launchModal(): void {
        this.$scope.$emit(this.config.modalEvent, this.model, this.options, this.config);
    }

}

const editableTypesComponent: ng.IComponentOptions = {
    controller: EditableTypes,
    template: `
        <div class="one-editable-types">
            <span
                class="one-editable-types__text"
                ng-if="!$ctrl.config.isEditable && !$ctrl.config.isButton && $ctrl.config.inputType !== $ctrl.inputTypes.Date"
                >
                    {{ $ctrl.label || "- - - -" }}
            </span>
            <span
                class="one-editable-types__text"
                ng-if="!$ctrl.config.isEditable && !$ctrl.config.isButton && $ctrl.config.inputType === $ctrl.inputTypes.Date"
                >
                    {{ $ctrl.label || "- - - -" | date: short}}
            </span>
            <span
                ng-if="$ctrl.config.isEditable && !$ctrl.config.isButton && !$ctrl.editorShowing"
                class="one-editable-types__text text-link row-flex-start row-vertical-center"
                ng-click="$ctrl.openEditor()"
                >
                    <one-date
                        class="one-editable-types__text-value static-horizontal"
                        ng-if="$ctrl.config.inputType === $ctrl.inputTypes.Date"
                        date-to-format="$ctrl.label"
                        >
                    </one-date>
                    <span class="one-editable-types__text-value static-horizontal" ng-if="$ctrl.config.inputType !== $ctrl.inputTypes.Date">
                        {{ $ctrl.label || "- - - -" }}
                    </span>
                    <i class="one-editable-types__text-icon fa fa-pencil static-horizontal"></i>
            </span>
            <one-button
                ng-if="$ctrl.config.isEditable && $ctrl.config.isButton && !$ctrl.editorShowing"
                type="{{$ctrl.buttonType}}"
                wrap-text="true"
                button-click="$ctrl.toggleEditor(true)"
                button-class="question-multichoice__btn {{$ctrl.buttonClass}}"
                is-selected="$ctrl.label"
                is-rounded="$ctrl.isButtonRounded"
                text="{{$ctrl.label || $ctrl.translate('Other')}}"
                identifier="{{$ctrl.identifier || 'editableButton'}}"
                ng-disabled="$ctrl.isDisabled"
                wrap-text="true"
            >
            </one-button>
            <section
                class="one-editable-types__form"
                ng-if="$ctrl.config.isEditable && $ctrl.editorShowing"
                one-click-outside="$ctrl.clickToClose"
                one-click-action="$ctrl.toggleEditor()"
                >
                    <one-editable-text
                        class="one-editable-types__group"
                        ng-if="$ctrl.config.inputType === $ctrl.inputTypes.Text"
                        text="{{$ctrl.label}}"
                        handle-submit="$ctrl.updateSelection(text)"
                        handle-cancel="$ctrl.toggleEditor(true)"
                        >
                    </one-editable-text>
                    <typeahead
                        class="one-editable-types__typeahead"
                        ng-if="$ctrl.config.inputType === $ctrl.inputTypes.Select"
                        options="$ctrl.options"
                        model="$ctrl.model"
                        on-select="$ctrl.updateSelection(selection)"
                        label-key="{{$ctrl.config.optionLabelKey}}"
                        has-arrow="true"
                        allow-other="$ctrl.config.allowOther"
                        other-option-prepend="$ctrl.config.otherText"
                        allow-empty="$ctrl.config.allowEmpty"
                        empty-text="$ctrl.config.emptyText"
                        input-limit="2"
                        use-virtual-scroll="true"
                        options-limit="1000"
                        >
                    </typeahead>
                    <downgrade-one-date-time-picker
                        ng-if="$ctrl.config.inputType === $ctrl.inputTypes.Date"
                        [date-disable-until]="$ctrl.config.minDate"
                        [date-placeholder]="$root.t('EnterDeadlineDate')"
                        [date-time-model]="$ctrl.label"
                        (on-change)="$ctrl.updateSelection($event.jsdate)"
                    ></downgrade-one-date-time-picker>
            </section>
        </div>
    `,
    bindings: {
        buttonClass: "@?",
        buttonType: "@?",
        config: "<",
        isDisabled: "<",
        identifier: "@?",
        isButtonRounded: "<?",
        model: "<",
        options: "<?",
        handleEdit: "&",
    },
};

export default editableTypesComponent;
