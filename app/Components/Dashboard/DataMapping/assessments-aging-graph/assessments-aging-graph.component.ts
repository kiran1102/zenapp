declare var angular: any;
declare var d3: any;
import { forEach } from "lodash";
import Utilities from "Utilities";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { TimeStamp } from "oneServices/time-stamp.service";
import {
    AssessmentStatusKeys,
    AssessmentStates,
} from "constants/assessment.constants";

class AssessmentAgingGraphController {

    static $inject: string[] = ["$rootScope", "$state", "$scope", "$timeout", "TimeStamp"];

    private translate: any;
    private graphData: any[];
    private graphId: string;
    private width: number;
    private height: number;
    private margins: any;
    private colorMap: any;
    private statusMap: any;
    private statusMapArray: string[];
    private assessmentDetailsChanged: (value: any) => void;
    private dayMS: number = 24 * 60 * 60 * 1000;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly $state: ng.ui.IStateService,
        readonly $scope: any,
        readonly $timeout: ng.ITimeoutService,
        readonly TimeStamp: TimeStamp,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.width = 540;
        this.height = 270;

        this.margins = {
            left: 50,
            right: 80,
            top: 40,
            bottom: 50,
        };

        this.colorMap = {
            "Not Started": "#696969",
            "In Progress": "#008ACC",
            "Under Review": "#FDA028",
            "Completed": "#6CC04A",
        };

        this.statusMap = {
            "Not Started": "1",
            "In Progress": "2",
            "Under Review": "3",
            "Completed": "4",
        };

        this.statusMapArray = [
            this.translate("NotStarted"),
            this.translate("InProgress"),
            this.translate("UnderReview"),
            this.translate("Completed"),
        ];

        this.$scope.$on("updateAgingGraph", this.updateAgingGraph);
    }

    $onChanges(): void {
        this.$timeout((): void => {
            this.initializeGraphData();
            this.drawGraph(this.graphData, this.calcYRange(this.graphData));
        }, 0);
    }

    private initializeGraphData(): void {
        forEach(this.graphData, (d: any): void => {
            d.DaysAgo = Math.floor(d.Time / this.dayMS);
        });
    }

    private drawGraph(data: any, yAxisRange: any): void {
        d3.select(`#${this.graphId} > *`).remove();

        // Create SVG container
        const svg: any = d3.select(`#${this.graphId}`)
            .append("svg")
            .attr("width", this.width)
            .attr("height", this.height)
            .append("g")
            .attr("transform", `translate(${this.margins.left}, ${this.margins.top})`);

        // Set scale for x-axis
        const x = d3.scale.linear()
            .domain(this.calcXRange())
            .range([0, this.width - this.margins.left - this.margins.right]);

        // Set scale for y-axis
        const y = d3.scale.linear()
            .domain(yAxisRange)
            .range([this.height - this.margins.top - this.margins.bottom, 0]);

        // Draw placeholder elements for axes
        svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + y.range()[0] + ")");
        svg.append("g").attr("class", "y axis");

        // Draw y-axis label
        svg.append("text")
            .attr("fill", "#696969")
            .style("font-weight", "bold")
            .attr("text-anchor", "end")
            .attr("y", -20)
            .attr("x", -10)
            .text("(days)");

        // Draw tick-marks and labels on axes
        const xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickPadding(2)
            .tickFormat((d: any): string => this.statusMapArray[d - 1]);

        const yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(5);

        // Style x and y-axis lines
        svg.selectAll("g.y.axis").call(yAxis)
            .attr("fill", "#696969");
        svg.selectAll("g.x.axis").call(xAxis)
            .attr("fill", "#696969");

        // Style x and y-axis labels
        svg.selectAll("g.x.axis").call(xAxis).selectAll("text")
            .attr("y", 10)
            .attr("x", 0)
            .attr("transform", "rotate(15)")
            .style("text-anchor", "start")
            .style("font-weight", "bold");
        svg.selectAll("g.y.axis").call(yAxis).selectAll("text")
            .style("font-weight", "bold");

        // Creating data groups with unique ids
        const assessmentData = svg.selectAll("g.node").data(data, (d: any): number => d.Day);

        const assessmentPoints = assessmentData.enter().append("g").attr("class", "node")
            // Set position for dots
            .attr("transform", (d: any): string => {
                let xCoord: string;
                switch (d.StateDesc) {
                    case this.translate(AssessmentStatusKeys.COMPLETED):
                        xCoord = this.statusMap[AssessmentStates.Completed];
                        break;
                    case this.translate(AssessmentStatusKeys.IN_PROGRESS):
                        xCoord = this.statusMap[AssessmentStates.InProgress];
                        break;
                    case this.translate(AssessmentStatusKeys.NOT_STARTED):
                        xCoord = this.statusMap[AssessmentStates.NotStarted];
                        break;
                    case this.translate(AssessmentStatusKeys.UNDER_REVIEW):
                        xCoord = this.statusMap[AssessmentStates.UnderReview];
                        break;
                    default:
                        xCoord = this.statusMap[AssessmentStates.NotStarted];
                }
                return `translate(${x(xCoord)}, ${y(d.Day)})`;
            });

        // Drawing assessment points
        assessmentPoints.append("circle")
            .attr("r", 5)
            .style("fill", (d: any): string => {
                let color: string;
                switch (d.StateDesc) {
                    case this.translate(AssessmentStatusKeys.COMPLETED):
                        color = this.colorMap[AssessmentStates.Completed];
                        break;
                    case this.translate(AssessmentStatusKeys.IN_PROGRESS):
                        color = this.colorMap[AssessmentStates.InProgress];
                        break;
                    case this.translate(AssessmentStatusKeys.NOT_STARTED):
                        color = this.colorMap[AssessmentStates.NotStarted];
                        break;
                    case this.translate(AssessmentStatusKeys.UNDER_REVIEW):
                        color = this.colorMap[AssessmentStates.UnderReview];
                        break;
                    default:
                        color = this.colorMap[AssessmentStates.NotStarted];
                }
                return color;
            })
            // Create details to be shown on mouseover
            .on("mouseover", (d: any): void => {
                const assessmentDetails: any[] = [];
                assessmentDetails.push({ title: `${this.translate("AssessmentName")}`, value: d.name });
                assessmentDetails.push({ title: `${this.translate("AssessmentStatus")}`, value: d.StateDesc });
                assessmentDetails.push({ title: this.translate("StatusDuration"), value: this.TimeStamp.returnDayTime(d.createdDate) });
                assessmentDetails.push({ title: this.translate("OrganizationGroup"), value: d.orgGroup.name });
                this.assessmentDetailsChanged({ value: assessmentDetails });
            })
            // Clear details when mouse leaves an assessment point
            .on("mouseout", (d: any): void => {
                this.assessmentDetailsChanged({ value: [] });
            })
            // Go to single assessment UI when clicking an assessment point
            .on("click", (d: any): void => {
                this.goToAssessment(d.assessmentId);
                d3.event.stopPropagation();
            });
    }

    private calcXRange(): string[] {
        return [this.statusMap["Not Started"], this.statusMap["Completed"]];
    }

    private calcYRange(data: any): number[] {
        // Range adjusts to time range of retrieved assessments
        const arr = d3.extent(data, (d: any): number => d.Day);
        arr[0] = 0;
        arr[1] = Math.ceil(arr[1]);
        // Min range is 4
        if (arr[1] < 4) {
            arr[0] = 0;
            arr[1] = 4;
        }
        return arr;
    }

    private goToAssessment(id: string): void {
        this.$state.go("zen.app.pia.module.datamap.views.assessment.single", {
            Id: id,
        });
    }

    private updateAgingGraph = (e: any, data: any): void => {
        d3.select(`#${this.graphId} svg`).remove();

        if (data.projectAgeFilter !== "") {
            const filteredArray: any[] = [];
            this.initializeGraphData();
            forEach(this.graphData, (assessment: any): void => {
                if (assessment.DaysAgo <= data.projectAgeFilter) {
                    filteredArray.push(assessment);
                }
            });

            const yAxisRange: string[] = [this.calcYRange(filteredArray)[0], data.projectAgeFilter];

            this.drawGraph(filteredArray, yAxisRange);

        } else {
            this.initializeGraphData();
            this.drawGraph(this.graphData, this.calcYRange(this.graphData));
        }
    }

}

const assessmentsAgingGraph = {
    bindings: {
        graphId: "<",
        graphData: "<",
        assessmentDetailsChanged: "&",
    },
    controller: AssessmentAgingGraphController,
    template: (): string => {
        return `
                <div id="{{$ctrl.graphId}}"></div>
                `;
    },
};

export default assessmentsAgingGraph;
