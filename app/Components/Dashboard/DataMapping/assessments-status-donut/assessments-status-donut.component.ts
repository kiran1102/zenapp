declare var angular: any;
import * as _ from "lodash";

class AssessmentsStatusDonutController implements ng.IComponentController {

    static $inject: string[] = ["$scope", "DmDashboardData", "ENUMS"];

    private AssessmentColors: any;

    private ready = false;
    private assessmentStatuses: any;
    private orgTree: any;
    private orgFilter: any;
    private empty = true;

    private donutName = "assessmentStatusDonut";
    private graphId = "assessment-status-donut";
    private margin = 5;
    private outerRadius = 140;
    private innerRadius = 65;
    private sortArcs = true;

    constructor(readonly $scope: any, readonly DmDashboardData: any, ENUMS: any) {
        this.AssessmentColors = ENUMS.DMAssessmentStateColors;
    }

    $onInit() {
        this.startListeners();
    }

    private startListeners(): void {
        this.$scope.$on("FORMATTED_ASSESSMENT_DATA", (): void => {
            this.getAssessmentStatusData();
        });
        this.$scope.$on("FILTER_UPDATE", (): void => {
            this.ready = false;
        });
    }

    private getAssessmentStatusData(): void {
        if (this.DmDashboardData.graphData.length) {
            this.assessmentStatuses = this.DmDashboardData.graphData;
            _.forEach(this.assessmentStatuses, (status: any): void => {
                status.color = this.AssessmentColors[status.translation];
            });
            this.empty = this.assessmentStatuses.every((item: any): boolean => item.magnitude === 0);
        }
        this.ready = true;
    }
}

const assessmentsStatusDonut = {
    bindings: {
        cardSize: "@",
    },
    controller: AssessmentsStatusDonutController,
    template: `
                <dashboard-card
                    card-size="$ctrl.cardSize"
                    card-title="{{::$root.t('AssessmentsStatus')}}"
                    loading-text="Assessments"
                    ready="$ctrl.ready"
                    empty="$ctrl.empty"
                    empty-text="NoAssessments"
                    >
                    <card-content>
                        <one-donut-graph
                            ng-if="!$ctrl.empty"
                            donut-name="$ctrl.donutName"
                            donut-data="$ctrl.assessmentStatuses"
                            graph-id="{{$ctrl.graphId}}"
                            margin="$ctrl.margin"
                            outer-radius="$ctrl.outerRadius"
                            inner-radius="$ctrl.innerRadius"
                            sort-arcs="$ctrl.sortArcs"
                        ></one-donut-graph>
                    </card-content>
                </dashboard-card>
            `,
};

export default assessmentsStatusDonut;
