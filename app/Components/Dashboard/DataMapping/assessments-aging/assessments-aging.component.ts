declare var angular: any;
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {IAssessmentParams} from "app/scripts/DataMapping/Services/Assessment/dm-assessments.service";

class AssessmentsAgingController implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "$scope", "$filter", "ENUMS", "DMAssessments", "DmDashboardData"];

    private translate: any;
    private AssessmentStateLabels: any;
    private AssessmentStates: any;
    private agingFilterOptions: any[];
    private currentAgingFilter: string;
    private assessmentDetails: any;

    private ready = false;
    private empty = true;
    private agingGraphData: any[];
    private agingGraphId = "one-aging-graph";
    private orgTree: any;
    private orgFilter: any;
    private parentTreeFilter: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly $scope: any,
        $filter: ng.IFilterService,
        ENUMS: any,
        readonly DMAssessments: any,
        readonly DmDashboardData: any,
    ) {
        this.translate = $rootScope.t;
        this.AssessmentStateLabels = ENUMS.DMAssessmentStateLabels;
        this.AssessmentStates = ENUMS.DMAssessmentStates;
        this.parentTreeFilter = $filter("IsInParentTree");
    }

    $onInit() {
        this.agingFilterOptions = [
            { text: this.translate("ShowAll"), value: "" },
            { text: this.translate("Last15Days"), value: "15" },
            { text: this.translate("Last30Days"), value: "30" },
            { text: this.translate("Last60Days"), value: "60" },
            { text: this.translate("Last90Days"), value: "90" },
        ];
        this.startListeners();
    }

    private startListeners(): void {
        this.$scope.$on("FORMATTED_ASSESSMENT_DATA", (): void => {
            this.currentAgingFilter = "";
            this.getAssessmentsAgingData();
        });
        this.$scope.$on("FILTER_UPDATE", (): void => {
            this.ready = false;
        });
    }

    private getAssessmentsAgingData(): void {
        this.agingGraphData = _.filter(this.DmDashboardData.dataContent, "orgGroup");
        _.forEach(this.agingGraphData, (assessment: any): void => {
            assessment.OrgGroupId = assessment.orgGroup.id;
        });
        this.agingGraphData = this.parentTreeFilter(this.agingGraphData, this.orgTree, this.orgFilter.Id, "OrgGroupId", "down");
        this.empty = !this.agingGraphData.length;
        if (!this.empty) {
            const currentTime: Date = new Date();
            const dayMS: number = 24 * 60 * 60 * 1000;
            _.forEach(this.agingGraphData, (assessment: any): void => {
                const duration: number = new Date(assessment.createdDate).valueOf() - currentTime.valueOf();
                assessment.StateDesc = this.translate(this.AssessmentStateLabels[assessment.statusId]);
                assessment.StateName = _.findKey(this.AssessmentStates, assessment.statusId);
                assessment.Time = Math.abs(duration);
                assessment.Day = Math.abs(duration) / dayMS;
            });
        }
        this.ready = true;
    }

    private filterAssessmentsByAge(selection: any): void {
        this.assessmentDetails = null;
        this.$scope.$broadcast("updateAgingGraph", { projectAgeFilter: selection });
    }

    private showAssessmentDetails(value: any[]): void {
        if (_.isArray(value)) {
            this.assessmentDetails = (value.length > 0) ? value : null;
        }
        this.$scope.$apply();
    }

}

const assessmentsAging = {
    bindings: {
        orgTree: "<",
        orgFilter: "<",
        cardSize: "@",
    },
    controller: AssessmentsAgingController,
    template: `
                <dashboard-card
                    card-size="$ctrl.cardSize"
                    card-title="{{::$root.t('AssessmentsAging')}}"
                    loading-text="Assessments"
                    ready="$ctrl.ready"
                    empty="$ctrl.empty"
                    empty-text="NoAssessments"
                    >
                    <card-content class="dashboard-aging">
                        <div class="dashboard-aging__chart">
                            <assessments-aging-graph
                                class="aging-graph"
                                ng-if="$ctrl.ready && $ctrl.agingGraphData.length"
                                assessment-details-changed="$ctrl.showAssessmentDetails(value)"
                                graph-id="$ctrl.agingGraphId"
                                graph-data="$ctrl.agingGraphData"
                            ></assessments-aging-graph>
                        </div>
                        <div class="dashboard-aging__controls">
                            <div class="dashboard-aging__age-filter">
                                <label>{{::$root.t("AssessmentAgeFilter")}}</label>
                                <one-select
                                    label-key="text"
                                    value-key="value"
                                    model="$ctrl.currentAgingFilter"
                                    on-change="$ctrl.filterAssessmentsByAge(selection)"
                                    options="$ctrl.agingFilterOptions"
                                ></one-select>
                            </div>
                            <div class="dashboard-aging__details">
                                <label ng-if="!$ctrl.assessmentDetails">{{::$root.t("HoverOverAssessmentForInformation")}}</label>
                                <div class="dashboard-aging__detail-group" ng-repeat="assessmentDetail in $ctrl.assessmentDetails track by $index">
                                    <span><strong> {{ assessmentDetail.title }} </strong></span>
                                    <span> {{ assessmentDetail.value }} </span>
                                </div>
                            </div>
                        </div>
                    </card-content>
                </dashboard-card>
            `,
};

export default assessmentsAging;
