declare var angular: any;
import * as _ from "lodash";

class AssessmentsStatusCountsController implements ng.IComponentController {

    static $inject: string[] = ["$scope", "DmDashboardData", "ENUMS"];

    public $onChanges: any;
    private AssessmentStates: any;
    private AssessmentStateClasses: any;
    private ready = false;
    private empty = true;

    private assessmentStatuses: any;
    private cardSizeClass: string;
    private orgTree: any;
    private orgFilter: any;

    constructor(readonly $scope: any, readonly DmDashboardData: any, ENUMS: any) {
        this.AssessmentStates = ENUMS.DMAssessmentStates;
        this.AssessmentStateClasses = ENUMS.DMAssessmentStateClasses;
    }

    public $onInit() {
        this.startListeners();
    }

    private startListeners(): void {
        this.$scope.$on("FORMATTED_ASSESSMENT_DATA", (): void => {
            this.getAssessmentStatusData();
        });
        this.$scope.$on("FILTER_UPDATE", (): void => {
            this.ready = false;
        });
    }

    private getAssessmentStatusData(): void {
        if (this.DmDashboardData.graphData.length) {
            this.empty = false;
            this.assessmentStatuses = this.DmDashboardData.graphData;
            _.forEach(this.assessmentStatuses, (status: any): void => {
                status.label = status.translation;
                status.number = status.magnitude;
                status.inverseColors = true;
                status.class = this.AssessmentStateClasses[status.statusId];
                status.classInverse = `inverse-${this.AssessmentStateClasses[status.statusId]}`;
            });
        }
        this.cardSizeClass = (this.assessmentStatuses.length === 4) ? "large-card" : "small-card";
        this.ready = true;
    }
}

const assessmentsStatusCounts: ng.IComponentOptions = {
    bindings: {
        cardSize: "@",
    },
    controller: AssessmentsStatusCountsController,
    template: `
            <dashboard-card
                card-size="$ctrl.cardSize"
                card-title="{{::$root.t('AssessmentsStatus(Details)')}}"
                loading-text="Assessments"
                ready="$ctrl.ready"
                empty="false"
                empty-text="NoAssessments"
                >
                <card-content class="full-width-block">
                    <stat-card
                        cards="$ctrl.assessmentStatuses"
                        card-size-class="$ctrl.cardSizeClass"
                    ></stat-card>
                </card-content>
            </dashboard-card>
        `,
};

export default assessmentsStatusCounts;
