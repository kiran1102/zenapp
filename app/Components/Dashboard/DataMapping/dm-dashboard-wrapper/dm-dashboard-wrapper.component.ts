
const dmDashboardWrapper: ng.IComponentOptions = {
    bindings: {
        orgFilter: "<",
        orgTree: "<",
    },
    template: `
                <div class="dm-dashboard-wrapper">
                    <dashboard-row class="dm-dashboard-wrapper__row">
                        <cards class="dm-dashboard-wrapper__cards row-wrap width-100-percent">
                            <assessments-status-donut card-size="six"></assessments-status-donut>
                            <assessments-status-counts card-size="six"></assessments-status-counts>
                        </cards>
                    </dashboard-row>
                    <dashboard-row class="dm-dashboard-wrapper__row">
                        <cards class="dm-dashboard-wrapper__cards row-wrap width-100-percent">
                            <assessments-aging org-filter="$ctrl.orgFilter" org-tree="$ctrl.orgTree" card-size="nine"></assessments-aging>
                        </cards>
                    </dashboard-row>
                </div>
            `,
};

export default dmDashboardWrapper;
