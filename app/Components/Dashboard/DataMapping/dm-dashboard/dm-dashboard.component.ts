import * as _ from "lodash";
import TreeMap from "TreeMap";

class DmDashboardController implements ng.IComponentController {

    static $inject: string[] = ["$scope", "$timeout", "OrgGroupStoreNew", "DmDashboardData"];

    private orgData: any;
    private orgFilter: any;
    private currentOrg: any;
    private orgTreeMap: any;

    constructor(readonly $scope: any, private readonly $timeout: ng.ITimeoutService, private readonly OrgGroupStoreNew: any, readonly DmDashboardData: any) {}

    $onInit() {
        this.getOrgs();
    }

    private getOrgs(): void {
        this.$timeout((): void => {
            this.orgData = [this.OrgGroupStoreNew.orgTree];
            this.orgFilter = this.orgData[0];
            this.currentOrg = this.orgData[0].Id;
            this.orgTreeMap = new TreeMap("Id", "ParentId", "Children", this.orgFilter);
            this.getAssessmentsData();
        }, 0);
    }

    private updateDashboardCards(item: any): void {
        this.orgFilter = item;
        this.$scope.$broadcast("FILTER_UPDATE");
        this.getAssessmentsData();
    }

    private getAssessmentsData(): void {
        this.DmDashboardData.formatAssessmentsData(this.orgTreeMap, this.orgFilter.Id).then((response: any): void => {
            this.DmDashboardData.graphData = response;
            this.$scope.$broadcast("FORMATTED_ASSESSMENT_DATA");
        });
    }
}

const dmDashboard: ng.IComponentOptions = {
    bindings: {},
    controller: DmDashboardController,
    template: `
            <div class="dm-dashboard flex-full-height height-100">
                <dashboard-header class="dm-dashboard__header">
                    <dropdown>
                        <div>
                            <label>{{::$root.t("FilterByOrganizationGroup")}}</label>
                            <collection-picker
                                ng-model="$ctrl.currentOrg"
                                list="$ctrl.orgData"
                                cancel="false"
                                on-select="$ctrl.updateDashboardCards(item)"
                                collection-key-nodes='Children'
                                collection-key-name='Name'
                                >
                            </collection-picker>
                        </div>
                    </dropdown>
                </dashboard-header>
                <dm-dashboard-wrapper
                    class="dm-dashboard__wrapper flex-full-height"
                    org-filter="$ctrl.orgFilter"
                    org-tree="$ctrl.orgTreeMap"
                    >
                </dm-dashboard-wrapper>
            </div>
        `,
};

export default dmDashboard;
