import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IOneConfigurableItem {
    text?: string;
    icon?: string;
    readonly textKey?: string;
    readonly description?: string;
    readonly action?: any;
    readonly route?: string;
    readonly [index: string]: any;
}

class OneConfigurableItemController {

    static $inject: string[] = ["$rootScope", "ENUMS"];

    private YesNoText: any;

    private YesNoClasses: any = {
        Yes: "helper-icon-yes",
        No: "helper-icon-no",
        NotSure: "helper-icon-not-sure",
    };

    private item: IOneConfigurableItem;
    private itemTextIcon: string;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly ENUMS: any,
    ) {
    }

    $onInit() {
        this.YesNoText = this.ENUMS.YesNoText;

        this.item.text = (this.item.textKey ? this.$rootScope.t(this.item.textKey) : this.item.text);

        switch (this.item.text) {
            case this.YesNoText.Yes:
                this.item.icon = this.YesNoClasses.Yes || this.item.icon;
                break;
            case this.YesNoText.No:
                this.item.icon = this.YesNoClasses.No || this.item.icon;
                break;
            case this.YesNoText.NotSure:
                this.item.icon = this.YesNoClasses.NotSure || this.item.icon;
                break;
            default:
                break;
        }
    }

}

const OneConfigurableItemTemplate = `
        <one-label-content
            class="one-configurable-item {{::$ctrl.itemClass}}"
            ng-class="{
                'one-configurable-item--separated': $ctrl.itemSeparated,
                'one-configurable-item--first': $ctrl.itemFirst,
                'one-configurable-item--last': $ctrl.itemLast,
            }"
            name="{{$ctrl.item.name}}"
            description="{{(!$ctrl.itemHideDescription ? $ctrl.item.description : '')}}"
            label-class="{{ ::$ctrl.itemLabelClass }}"
            is-required="$ctrl.item.required"
            inline="$ctrl.itemInline"
            >
            <div class="one-configurable-item__body">
                <span
                    ng-if="$ctrl.item.icon"
                    class="one-configurable-item__icon {{ ::$ctrl.item.icon }} {{ ::$ctrl.itemIconClass }}"
                    >
                </span>
                <span
                    ng-if="!$ctrl.item.action"
                    class="one-configurable-item__text {{ ::$ctrl.itemTextClass }}"
                    ng-bind="($ctrl.item.text || '- - - -')"
                    title="{{::($ctrl.item.text || '- - - -')}}"
                    >
                </span>
                <a
                    ng-if="$ctrl.item.action"
                    class="one-configurable-item__link {{ ::$ctrl.itemTextClass }}"
                    ng-click="$ctrl.item.action($ctrl.item)"
                    ng-bind="($ctrl.item.text || '- - - -')"
                    title="{{::($ctrl.item.text || '- - - -')}}"
                    >
                </a>
            </div>
        </one-label-content>
    `;

const OneConfigurableItemComponent = {
    controller: OneConfigurableItemController,
    template: OneConfigurableItemTemplate,
    bindings: {
        item: "<",
        itemClass: "@?",
        itemLabelClass: "@?",
        itemTextClass: "@?",
        itemIconClass: "@?",
        itemInline: "<?",
        itemSeparated: "<?",
        itemHideDescription: "<?",
        itemEditable: "<?",
        itemFirst: "<?",
        itemLast: "<?",
        itemIndex: "<?",
    },
};

export default OneConfigurableItemComponent;
