class ExpandableTileController implements ng.IComponentController {

    static $inject: string[] = ["store", "$state", "RiskBusinessLogic"];

    private unsubFromStore: any;
    private tileIsExpanded: boolean;
    private isHovering: boolean;

    constructor(
        private readonly store: any,
        private readonly $state: any,
    ) {
        this.unsubFromStore = store.subscribe((): void => {
            // update this component state
        });
    }

    // 1. This component only reads data from store. It does not do operations on the data. No write access.

    // 2. This component only builds configurations necessary for the presentation of the child components.

    // 3. This component receives actions from the child components and only sends the received actions to the logic layer.

    $onDestroy() {
        this.unsubFromStore();
    }
}

const ExpandableTileComponent = {
    controller: ExpandableTileController,
    bindings: {
        boxShadowNone: "<?",
        hasBorder: "<?",
        hoverEffect: "<?",
        marginBottom: "<?",
    },
    transclude: {
    },
    template: `
        <div
            class="expandable-tile position-relative"
            ng-class="{
                'border-bottom': $ctrl.hasBorder,
                'margin-bottom-1': $ctrl.marginBottom,
                'box-shadow-none': $ctrl.boxShadowNone,
                'expandable-tile--expanded': $ctrl.tileIsExpanded
            }"
            ng-mouseover="$ctrl.isHovering = true"
            ng-mouseleave="$ctrl.isHovering = false"
        >
            <div ng-transclude class="expandable-tile__content"></div>
        </div>
    `,
};

export default ExpandableTileComponent;
