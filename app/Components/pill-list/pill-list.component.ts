class PillList {
    static $inject: string[] = ["$scope"];

    private list: any;
    private handleRemove: any;

    constructor(readonly $scope) {}

    public removeItem(item: any): void {
        this.handleRemove({item});
    }

}

const pillList: ng.IComponentOptions = {
    bindings: {
        list: "<",
        nameKey: "@",
        descriptionKey: "@?",
        tooltipKey: "@?",
        handleRemove: "&",
        isDisabled: "<",
    },
    controller: PillList,
    template: () => {
        return `<ul class="pill-list"

                >
                    <li
                        class="pill-list__item shadow shadow-transition"
                        ng-repeat="item in $ctrl.list track by $index"
                        ng-disabled="$ctrl.isDisabled"
                        >
                            <button
                                class="pill-list__remove"
                                ng-click="$ctrl.removeItem(item)"
                            >
                                <i class="pill-list__remove-icon ot ot-close" />
                            </button>
                            <h5
                                class="pill-list__name text-bold"
                                ng-if="$ctrl.nameKey"
                                uib-tooltip="{{ $ctrl.tooltipKey ? item[$ctrl.tooltipKey] : '' }}"
                            >
                                    {{ item[$ctrl.nameKey] }}
                            </h5>
                            <p
                                class="pill-list__description"
                                ng-if="$ctrl.descriptionKey">
                                    {{ item[$ctrl.descriptionKey] }}
                            </p>
                    </li>
                </ul>`;
    },
};

export default pillList;
