declare var angular: any;

class OneWelcomeTextController {

    static $inject: string[] = ["OneWelcomeTextService"];
    private translatedWelcomeText: any;

    constructor(readonly OneWelcomeTextService: any) {
        this.translatedWelcomeText = OneWelcomeTextService.getWelcomeText();
    }
}

const OneWelcomeTextTemplate = `
    <div
        class="one-welcome-text ql-editor",
        data-ng-click="$ctrl.onClick()",
        data-ng-bind-html="$ctrl.hasCustomText ? $ctrl.customWelcomeText : $ctrl.translatedWelcomeText">
    </div>
	`;

const OneWelcomeText: any = {
    controller: OneWelcomeTextController,
    template: OneWelcomeTextTemplate,
    bindings: {
        hasCustomText: "<?",
        customWelcomeText: "<?",
        onClick: "&?",
    },
};

export default OneWelcomeText;
