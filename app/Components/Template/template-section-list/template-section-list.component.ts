
class TemplateSectionList {
    static $inject: string[] = ["$scope", "ENUMS", "DMTemplate"];

    TemplateStates: any;

    constructor(
        readonly $scope: any,
        readonly ENUMS: any,
        readonly DMTemplate: any,
    ) {
        this.TemplateStates = ENUMS.DMTemplateStates;
    }

    public addSection = (): void => {
        this.$scope.$emit("ADD_SECTION");
    }
}

const templateSectionList = {
    controller: TemplateSectionList,
    bindings: {
        list: "<",
        isDisabled: "<?",
        readOnly: "<?",
        templateState: "<?",
    },
    template: `
        <fieldset
            ng-disabled="$ctrl.isDisabled"
            >
            <section class="template-section-list">
                <ul class="template-section-list__list">
                    <li
                        class="template-section-list__item"
                        ng-repeat="item in $ctrl.list track by $index"
                        >
                            <template-section-item
                                section="item"
                                read-only="$ctrl.readOnly"
                                template-state="$ctrl.templateState"
                                >
                            </template-section-item>
                    </li>
                    <new-item-field
                        class="template-section-list__add"
                        ng-if="$ctrl.templateState === $ctrl.TemplateStates.Draft"
                        text="{{$root.t('AddSection')}}"
                        icon-class="fa-plus"
                        handle-click="$ctrl.addSection()"
                        >
                    </new-item-field>
                </ul>
            </section>
        </fieldset>
    `,
};

export default templateSectionList;
