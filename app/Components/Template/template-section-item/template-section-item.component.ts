import * as _ from "lodash";
import { IActionIcon } from "interfaces/action-icons.interface";

class TemplateSectionItemController {
    static $inject: string[] = ["$scope", "ENUMS"];

    section: any;
    actions: IActionIcon[];
    readOnly: boolean;
    templateState: number;
    private TemplateStates: any;

    constructor(readonly $scope: any, readonly ENUMS: any) {
        this.TemplateStates = ENUMS.DMTemplateStates;
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        this.formatActions();
    }

    public toggleSectionOpen = (): void => {
        this.$scope.$emit("TOGGLE_SECTION", this.section.templateSectionUniqueId);
    }

    private formatActions(): void {
        this.actions = [
            {
                key: this.ENUMS.ActionKeys.Edit,
                handleClick: this.editSection,
                isHidden: this.readOnly && this.templateState === this.TemplateStates.Published,
                iconClass: "fa-pencil",
            },
            {
                key: this.ENUMS.ActionKeys.Delete,
                handleClick: this.deleteSection,
                isHidden: this.templateState === this.TemplateStates.Published || Boolean(_.find(this.section.questions, ["seeded", true])),
                iconClass: "fa-trash",
            },
        ];
    }

    private editSection = (): void => {
        this.$scope.$emit("EDIT_SECTION", this.section.templateSectionUniqueId);
    }

    private deleteSection = (): void => {
        this.$scope.$emit("DELETE_SECTION", this.section);
    }
}

export default {
    bindings: {
        section: "<",
        readOnly: "<?",
        templateState: "<?",
    },
    controller: TemplateSectionItemController,
    template: `
        <template-item
            item="$ctrl.section"
            read-only="$ctrl.readOnly"
            label="{{$root.t('Section')}}"
            actions="$ctrl.actions"
            toggle-item-open="$ctrl.toggleSectionOpen"
            >
            <preview
                ng-if="$ctrl.section.welcomeSection"
                >
                <question-welcome
                    class="template-section__welcome"
                    question="$ctrl.section.questions[0]"
                    >
                </question-welcome>
            </preview>
            <children
                ng-if="!$ctrl.section.welcomeSection"
                >
                <template-question-list
                    class="template-section__questions"
                    list="$ctrl.section.questions"
                    read-only="$ctrl.readOnly"
                    template-state="$ctrl.templateState"
                    section-id="$ctrl.section.templateSectionUniqueId"
                    >
                </template-question-list>
            </children>
        </template-item>
    `,
};
