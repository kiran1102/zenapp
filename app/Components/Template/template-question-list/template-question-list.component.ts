import { isUndefined, filter } from "lodash";

class TemplateQuestionList {
    static $inject: string[] = ["$scope", "ENUMS", "DMTemplate"];

    sectionId: string;
    questionOptions: any;
    filteredList: any[];
    isDraggable: boolean;
    allowNewItem: boolean;
    private list: any[];
    private targetClass = "template-question-list__list";

    constructor(
        readonly $scope: any,
        readonly ENUMS: any,
        readonly DMTemplate: any,
    ) {}

    $onInit() {
        this.questionOptions = {
            accept: this.accept,
            dropped: this.dropped,
            beforeDrop: this.beforeDrop,
        };
        if (this.DMTemplate.itemsReordering) {
            this.DMTemplate.toggleItemsReordering();
        }
        if (isUndefined(this.isDraggable)) this.isDraggable = true;
        if (isUndefined(this.allowNewItem)) this.allowNewItem = true;
    }

    $onChanges(): void {
        this.filteredList = this.list.length
            ? filter(this.list, (item: any): boolean => item.questionType !== this.ENUMS.DMQuestionTypes.Bumper)
            : [];
    }

    public addQuestion = (): void => {
        this.$scope.$emit("ADD_QUESTION", this.sectionId);
    }

    private accept = (source: any, dest: any): boolean => {
        if (source.$element[0].className.indexOf("question-bank__item") >= 0) {
            return true;
        }
        if (dest.$element[0].className.indexOf(this.targetClass) === -1 || !source.$modelValue) {
            return false;
        }
        if (!dest.$parent || !dest.$parent.$parent || !dest.$parent.$parent.$ctrl) {
            return false;
        }
        if (!source.$parent || !source.$parent.$parent || !source.$parent.$parent.$ctrl) {
            return false;
        }
        const sourceSectionId: string = source.$parent.$parent.$ctrl.sectionId;
        const destSectionId: string = dest.$parent.$parent.$ctrl.sectionId;
        if (sourceSectionId !== destSectionId) {
            return false;
        }
        return true;
    }

    private dropped = (event: any): void => {
        this.$scope.$emit("QUESTION_DROPPED");
    }

    private beforeDrop = (event: any): void => {
        this.$scope.$emit("MOVE_QUESTION", {
            sectionId: this.sectionId,
            questionId: event.source.nodeScope.$modelValue.templateQuestionUniqueId,
            index: event.dest.index,
        });
    }
}

export default {
    controller: TemplateQuestionList,
    bindings: {
        list: "<",
        readOnly: "<?",
        templateState: "<?",
        sectionId: "<",
        isDraggable: "<?",
        allowNewItem: "<?",
    },
    template: `
        <section
            class="template-question-list"
            ui-tree="$ctrl.questionOptions"
            data-empty-placeholder-enabled="false"
            >
            <ul
                class="template-question-list__list"
                ui-tree-nodes
                ng-model="$ctrl.filteredList"
                >
                <li
                    ui-tree-node
                    class="template-question-list__spacer"
                    ng-if="!$ctrl.filteredList.length"
                    >
                    &nbsp;
                </li>
                <!--
                    NOTE: Do NOT add track by to the below repeat as it will break drag and drop reordering.
                    https://github.com/Zentrust/ZenApp/pull/4280
                -->
                <li
                    class="template-question-list__item"
                    ng-repeat="item in $ctrl.filteredList"
                    ui-tree-node
                    data-index="{{$index}}"
                    >
                    <template-question-item
                        question="item"
                        read-only="$ctrl.readOnly"
                        template-state="$ctrl.templateState"
                        section-id="$ctrl.sectionId"
                        section-questions="$ctrl.filteredList"
                        is-last-item="$index === $ctrl.filteredList.length - 1"
                        is-draggable="$ctrl.isDraggable"
                        >
                    </template-question-item>
                </li>
            </ul>
            <new-item-field
                class="template-question-list__add"
                ng-if="$ctrl.templateState === $ctrl.ENUMS.DMTemplateStates.Draft && $ctrl.allowNewItem"
                text="{{$root.t('AddQuestion')}}"
                icon-class="fa-plus"
                handle-click="$ctrl.addQuestion()"
                >
            </new-item-field>
        </section>
    `,
};
