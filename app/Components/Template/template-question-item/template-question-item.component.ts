import { INotifyUserModal } from "interfaces/notify-user-modal.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDMAssessmentQuestion, IDMConditions } from "interfaces/question.interface";
import { AttributeCodes } from "constants/attribute-codes.constant";
import { isArray, isUndefined, map, forEach, find, flattenDeep, compact } from "lodash";
import { IActionIcon } from "interfaces/action-icons.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ContextTypes } from "enums/context-types.enum";
import { DMTemplateStates } from "enums/template-states.enum";
import { DMQuestionTypes } from "enums/question-types.enum";
import { OptionTypes } from "enums/inventory.enum";
import { ActionKeys } from "constants/action-keys.constant";
import DefaultQuestion from "sharedServices/default-question.service";
import { ModalService } from "sharedServices/modal.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class TemplateQuestionItem {

    static $inject: string[] = ["$rootScope", "$scope", "DefaultQuestion", "Permissions", "ModalService"];

    private question: any;
    private actions: IActionIcon[];
    private readOnly: boolean;
    private templateState: number;
    private TemplateStates: any;
    private sectionId: string;
    private sectionQuestions: IDMAssessmentQuestion[];
    private isLastItem: boolean;
    private questionList: any[];
    private dataSubjectPreview: any[];
    private defaultFollowupOptions: any[];
    private defaultAssetQuestionOptions: any[];
    private defaultProcessQuestionOptions: any[];
    private canDisablePermission: boolean = this.permissions.canShow("DataMappingDisableQuestions");
    private ContextTypes: any;
    private DMQuestionTypes: any;
    private isDraggable: boolean;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $scope: ng.IScope,
        private defaultQuestion: DefaultQuestion,
        private permissions: Permissions,
        private modalService: ModalService,
    ) {
        this.TemplateStates = DMTemplateStates;
        this.ContextTypes = ContextTypes;
        this.DMQuestionTypes = DMQuestionTypes;
    }

    $onInit() {
        this.dataSubjectPreview = this.defaultQuestion.getDataSubjectPreview();
        this.defaultFollowupOptions = this.defaultQuestion.getDefaultFollowupOptions();
        this.defaultAssetQuestionOptions = this.defaultQuestion.getDefaultAssetQuestionOptions();
        this.defaultProcessQuestionOptions = this.defaultQuestion.getDefaultProcessQuestionOptions();
        this.questionList = this.formatQuestionPreview();
        if (isUndefined(this.isDraggable)) this.isDraggable = true;
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        this.formatActions();
    }

    public formatQuestionPreview(): any {
        if (this.question.questionType === this.DMQuestionTypes.DataSubject) {
            return this.dataSubjectPreview;
        }
        if (this.question.questionType === this.DMQuestionTypes.Asset) {
            this.question.options = this.defaultAssetQuestionOptions;
            this.formatFollowupQuestions(1);
        }
        if (this.question.questionType === this.DMQuestionTypes.Process) {
            this.question.options = this.defaultProcessQuestionOptions;
            this.formatFollowupQuestions(0);
        }
        if (this.question.questionType === this.DMQuestionTypes.AppInfo) {
            this.question.applications = [ {name: "Asset Name"} ];
        }
        return [this.question];
    }

    public formatFollowupQuestions(questionIndex: number) {
        this.question.followUpQuestions[questionIndex].options = this.defaultFollowupOptions;
    }

    public formatActions(): void {
        const cannotModifyQuestion: boolean = this.readOnly
            && (this.templateState === this.TemplateStates.Published
            || this.question.questionType === this.DMQuestionTypes.AppInfo
            || this.question.attributeCode === AttributeCodes.Categories_of_Data_Subjects
            || this.question.attributeCode === AttributeCodes.Data_Elements);
        this.actions = [
            {
                key: ActionKeys.Conditions,
                handleClick: this.openConditionsModal,
                isHidden: this.question.questionType !== this.DMQuestionTypes.MultiSelect &&
                            this.question.questionType !== this.DMQuestionTypes.SingleSelect ||
                            (this.question.responseType !==  OptionTypes.Subjects &&
                            this.question.responseType !==  OptionTypes.Locations &&
                            this.question.responseType !==  OptionTypes.Categories &&
                            this.question.responseType !==  OptionTypes.Custom &&
                            this.question.responseType !== null) ||
                            this.question.attributeCode === AttributeCodes.Categories_of_Data_Subjects ||
                            (this.isLastItem && !this.question.conditionGroups) ||
                            this.question.disabled,
                showPill: Boolean(isArray(this.question.conditionGroups) && this.question.conditionGroups.length),
                iconClass: "fa-code-fork fa-stack-1x fa-rotate-90 action-icons__conditions",
                buttonClass: "text-center",
            },
            {
                key: ActionKeys.Enable,
                handleClick: this.toggleHideQuestion,
                isHidden: this.templateState === this.TemplateStates.Published
                || !this.canDisablePermission || (cannotModifyQuestion || this.question.disabled)
                || this.question.attributeCode === AttributeCodes.Categories_of_Data_Subjects
                || this.question.attributeCode === AttributeCodes.Data_Elements,
                iconClass: "fa-eye-slash",
                hasTooltip: true,
                tooltipPlacement: "top-right",
                tooltipCloseDelay: 200,
                tooltipTrigger: "mouseenter",
                tooltipTranslationKey: "DisableQuestionTooltip",
                buttonClass: "text-center",
            },
            {
                key: ActionKeys.Disable,
                handleClick: this.toggleHideQuestion,
                isHidden: this.templateState === this.TemplateStates.Published
                || !this.canDisablePermission || (cannotModifyQuestion || !this.question.disabled)
                || this.question.attributeCode === AttributeCodes.Categories_of_Data_Subjects
                || this.question.attributeCode === AttributeCodes.Data_Elements,
                iconClass: "fa-eye",
                hasTooltip: true,
                tooltipPlacement: "top-right",
                tooltipCloseDelay: 200,
                tooltipTrigger: "mouseenter",
                tooltipTranslationKey: "DisableQuestionTooltip",
                buttonClass: "text-center",
            },
            {
                key: ActionKeys.Edit,
                handleClick: this.editQuestion,
                isHidden: cannotModifyQuestion || this.question.questionType === this.DMQuestionTypes.DataSubject
                || this.question.disabled || this.question.attributeCode === AttributeCodes.Data_Subjects_Region
                || this.question.attributeCode === AttributeCodes.Data_Subjects_Volume
                || this.question.attributeCode === AttributeCodes.Categories_of_Data_Subjects
                || this.question.attributeCode === AttributeCodes.Data_Elements,
                iconClass: "fa-pencil",
                buttonClass: "text-center",
            },
            {
                key: ActionKeys.Delete,
                handleClick: this.deleteQuestion,
                isHidden: this.question.questionType === this.DMQuestionTypes.DataSubject || this.templateState === this.TemplateStates.Published || this.question.seeded,
                iconClass: "fa-trash",
                buttonClass: "text-center",
            },
        ];
    }

    public editQuestion = (): void => {
        this.$scope.$emit("EDIT_QUESTION", this.sectionId, this.question);
    }

    public toggleHideQuestion = (): void | undefined => {
        if (this.isConditionTarget(this.question.templateQuestionUniqueId) || !isUndefined(this.question.conditionGroups)) {
            const modalData: INotifyUserModal = {
                modalTitle: "",
                notificationText: "",
            };
            if (this.isConditionTarget(this.question.templateQuestionUniqueId)) {
                modalData.modalTitle = this.$rootScope.t("ConditionalLogicPresent");
                modalData.notificationText = this.$rootScope.t("ConditionalLogicPresentMessage");
            } else if (!isUndefined(this.question.conditionGroups)) {
                modalData.modalTitle = this.$rootScope.t("ConditionalLogicPresent");
                modalData.notificationText = this.$rootScope.t("QuestionHasCondtionsMessage");
            }
            this.modalService.setModalData(modalData);
            this.modalService.openModal("notifyUserModal");
            return;
        }
        this.$scope.$emit("TOGGLE_HIDE_QUESTION", this.sectionId, this.question.templateQuestionUniqueId, this.question);
        this.formatActions();
    }

    public deleteQuestion = (): void => {
        this.$scope.$emit("DELETE_QUESTION", this.question);
    }

    public openConditionsModal = (): void => {
        this.$scope.$emit("EDIT_CONDITIONS", this.sectionId, this.question);
    }

    public toggleQuestionOpen = (): void => {
        this.$scope.$emit("TOGGLE_QUESTION", this.question.templateQuestionUniqueId);
    }

    public isConditionTarget(questionId: string): boolean {
        const conditions: IDMConditions[] = flattenDeep(map(this.sectionQuestions, "conditionGroups"));
        return Boolean(find(compact(conditions), (condition: IDMConditions): boolean => condition.targetQuestionId === questionId));
    }

}

export default {
    controller: TemplateQuestionItem,
    bindings: {
        question: "<",
        readOnly: "<?",
        templateState: "<?",
        sectionId: "<",
        sectionQuestions: "<",
        isLastItem: "<",
        isDraggable: "<?",
    },
    template: `
        <template-item
            item="$ctrl.question"
            read-only="$ctrl.readOnly"
            label="{{::$root.t('Question')}}"
            actions="$ctrl.actions"
            toggle-item-open="$ctrl.toggleQuestionOpen"
            is-draggable="$ctrl.templateState === $ctrl.TemplateStates.Draft && $ctrl.isDraggable"
            >
            <preview ng-if="$ctrl.question.questionType !== $ctrl.DMQuestionTypes.DataSubject">
                <question-list
                    class="template-item__preview"
                    questions="$ctrl.questionList"
                    context="$ctrl.ContextTypes.DMTemplate"
                    >
                </question-list>
            </preview>
            <children
                ng-if="$ctrl.question.questionType === $ctrl.DMQuestionTypes.DataSubject"
                >
                <template-question-list
                    class="template-section__questions"
                    list="$ctrl.question.followUpQuestions"
                    read-only="$ctrl.readOnly"
                    template-state="$ctrl.templateState"
                    section-id="$ctrl.section.templateSectionUniqueId"
                    is-draggable="false"
                    allow-new-item="false"
                    >
                </template-question-list>
            </children>
        </template-item>
    `,
};
