const templateSidebar = {
    bindings: {
        templateType: "<",
        disabledDraggableTypes: "<",
    },
    template: () => {
        return `
                <section class="template-sidebar">
                    <h4 class="template-sidebar__title">
                        {{::$root.t("TemplateBuilder")}}
                    </h4>
                    <section class="template-sidebar__group">
                        <label class="template-sidebar__label text-center">
                            {{::$root.t("DragAndDropQuestionToTemplate")}}
                        </label>
                        <question-bank
                            class="template-sidebar__types"
                            template-type="$ctrl.templateType"
                            target-class="template-question-list__list"
                            disabled-types="$ctrl.disabledDraggableTypes"
                            >
                        </question-bank>
                    </section>
                </section>
            `;
    },
};

export default templateSidebar;
