import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class TemplateVersionList {
    static $inject: string[] = ["$scope", "$rootScope", "ENUMS"];

    translate: any;
    TemplateStates: any;
    currentVersion: number;
    InputTypes: any;
    details: any[];

    constructor(
        readonly $scope: any,
        $rootScope: IExtendedRootScopeService,
        ENUMS: any,
    ) {
        this.translate = $rootScope.t;
        this.TemplateStates = ENUMS.DMTemplateStates;
        this.InputTypes = ENUMS.InputTypes;
    }

    $onInit() {
        this.details = [
            {
                show: (item: any): boolean => Boolean(item.createdBy && item.createdBy.firstName && item.createdBy.lastName),
                label: this.translate("CreatedBy"),
                type: this.InputTypes.Text,
                getValue: (item: any): string => `${item.createdBy.firstName} ${item.createdBy.lastName}`,
            },
            {
                show: (item: any): boolean => Boolean(item.createdDate),
                label: this.translate("Created"),
                type: this.InputTypes.Date,
                getValue: (item: any): Date => item.createdDate,
            },
            {
                show: (item: any): boolean => Boolean(item.publishedBy && item.publishedBy.firstName && item.publishedBy.lastName),
                label: this.translate("PublishedBy"),
                type: this.InputTypes.Text,
                getValue: (item: any): string => `${item.publishedBy.firstName} ${item.publishedBy.lastName}`,
            },
            {
                show: (item: any): boolean => Boolean(item.publishDate),
                label: this.translate("Published"),
                type: this.InputTypes.Date,
                getValue: (item: any): Date => item.publishDate,
            },
        ];
    }

    public toggleItem(item: any): void {
        item.isExpanded = !item.isExpanded;
    }

    public goToVersion(version: number, id: string): void {
        if (version !== this.currentVersion) {
            this.$scope.$emit("GO_TO_VERSION", id);
        }
    }

    public deleteVersion(version: number, id: string): void {
        const isCurrentVersion: boolean = version === this.currentVersion;
        this.$scope.$emit("DELETE_VERSION", id, isCurrentVersion);
    }
}

const templateVersionList: ng.IComponentOptions = {
    controller: TemplateVersionList,
    bindings: {
        currentVersion: "<",
        versionList: "<",
    },
    template: (): string => {
        return `
            <section class="template-version-list">
                <h4 class="template-version-list__title text-uppercase text-bold text-center"> {{::$ctrl.translate("VersionHistory")}} </h4>
                <ul
                    class="template-version-list__list"
                    ng-repeat="group in $ctrl.versionList track by $index"
                    ng-if="group.versions && group.versions.length"
                    >
                        <li class="template-version-list__divider text-center">
                            {{ group.title }}
                        </li>
                        <li
                            class="template-version-list__item"
                            ng-repeat="item in group.versions track by item.id"
                            ng-class="::{'is-current-version': item.templateVersion === $ctrl.currentVersion}"
                            >
                                <header
                                    class="template-version-list__tab"
                                    ng-click="$ctrl.goToVersion(item.templateVersion, item.id)"
                                    >
                                    <section class="template-version-list__text">
                                        <span class="template-version-list__number text-bold">
                                            {{::($ctrl.translate("Version") + " " + item.templateVersion)}}
                                        </span>
                                        <one-tag
                                            class="template-version-list__state"
                                            label-class="{{::item.statusId === $ctrl.TemplateStates.Published ? 'completed' : 'current-draft'}}"
                                            plain-text="{{::item.statusId === $ctrl.TemplateStates.Draft ? $ctrl.translate('Draft') : $ctrl.translate('Published')}}"
                                            size="small"
                                            >
                                        </one-tag>
                                        <i
                                            class="template-version-list__delete fa fa-trash-o"
                                            ng-if="item.statusId === $ctrl.TemplateStates.Draft"
                                            ng-click="$ctrl.deleteVersion(item.templateVersion, item.id);$event.stopPropagation();"
                                            uib-tooltip="{{::$root.t('DiscardDraft')}}"
                                            tooltip-placement="right"
                                            tooltip-trigger="mouseenter"
                                            >
                                        </i>
                                        <span class="template-version-list__date text-bold">
                                            {{::$ctrl.translate("Created")}}
                                            <one-date
                                                date-to-format="item.createdDate"
                                                enable-tooltip="true"
                                                >
                                            </one-date>
                                        </span>
                                    </section>
                                    <button
                                        class="template-version-list__toggle"
                                        ng-click="$ctrl.toggleItem(item);$event.stopPropagation();"
                                        >
                                            <i class="fa fa-caret-{{ item.isExpanded ? 'up' : 'down'}}"></i>
                                    </button>
                                </header>
                                <section
                                    class="template-version-list__details"
                                    ng-if="item.isExpanded"
                                    >
                                        <one-label-content
                                            class="template-version-list__details-group"
                                            ng-repeat="detail in $ctrl.details track by $index"
                                            ng-if="::detail.show(item)"
                                            name="{{::detail.label}}"
                                            label-class="template-version-list__details-label"
                                            inline="true"
                                            >
                                                <span class="template-version-list__details-text text-bold">
                                                    <span ng-if="::(detail.type === $ctrl.InputTypes.Text)">
                                                        {{::detail.getValue(item)}}
                                                    </span>
                                                    <one-date
                                                        ng-if="::(detail.type === $ctrl.InputTypes.Date)"
                                                        date-to-format="::detail.getValue(item)"
                                                        enable-tooltip="true"
                                                        >
                                                    </one-date>
                                                </span>
                                        </one-label-content>
                                </section>
                        </li>
                </ul>
            </section>
        `;
    },
};

export default templateVersionList;
