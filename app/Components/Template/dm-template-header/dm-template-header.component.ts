import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";

class DmTemplateHeaderController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "ENUMS", "Permissions"];

    private TemplateStates: any;
    private InventoryTypes: any;
    private template: any;
    private translate: any;
    private editOptions: IDropdownOption[];

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, readonly ENUMS: any, private Permissions: any) {
        this.TemplateStates = this.ENUMS.DMTemplateStates;
        this.InventoryTypes = this.ENUMS.InventoryTableIds;
        this.translate = $rootScope.t;
    }

    $onInit() {
        const editOptions = [];
        if (this.template.isRootOrg && this.hasPermissionToEdit()) {
            editOptions.push({
                text: this.translate("ThisVersion"),
                action: this.toggleEditThisVersion,
                isDisabled: this.template.templateVersion === 1,
                toolTip: this.template.templateVersion === 1 ? this.translate("VersionOneReadOnly") : null,
                toolTipDirection: "left",
            });
        }
        if (this.template.isRootOrg && this.Permissions.canShow("DataMappingTemplateEditNewVersion")) {
            editOptions.push({
                text: this.translate("NewVersion"),
                action: this.editNewVersion,
            });
        }
        this.editOptions = editOptions;
    }

    public toggleEditThisVersion = (): void => {
        this.$scope.$emit("TOGGLE_EDIT_THIS_VERSION");
    }

    public editNewVersion = (): void => {
        this.$scope.$emit("EDIT_NEW_VERSION");
    }

    public publishTemplate = (): void => {
        this.$scope.$emit("PUBLISH_TEMPLATE");
    }

    public toggleVersionHistory = (): void => {
        this.$scope.$emit("TOGGLE_VERSION_HISTORY");
    }

    private canEditTemplate(): boolean {
        return !this.template.editMode &&
        this.template.statusId === this.TemplateStates.Published &&
        this.template.isLatestPublishedVersion &&
        this.template.canEdit &&
        (this.hasPermissionToEdit() || this.Permissions.canShow("DataMappingTemplateEditNewVersion"));
    }

    private hasPermissionToEdit(): boolean {
        if (this.template.templateType.toString() === this.InventoryTypes.Assets) {
            return this.Permissions.canShow("DataMappingAssetTemplateEdit");
        } else if (this.template.templateType.toString() === this.InventoryTypes.Processes) {
            return this.Permissions.canShow("DataMappingPATemplateEdit");
        }
        return false;
    }
}

export default {
    bindings: {
        template: "<",
    },
    controller: DmTemplateHeaderController,
    template: `
        <one-header>
            <header-title class="row-vertical-center">
                <span class="margin-right-1"> {{::$ctrl.translate("Template")}} </span>
                <i class="margin-right-1 fa fa-angle-right"></i>
                <span class="margin-right-1"> {{::$ctrl.translate($ctrl.template.nameKey)}} </span>
                <one-tag
                    class="margin-right-1"
                    plain-text="V{{::$ctrl.template.templateVersion || 1}}"
                    >
                </one-tag>
                <one-tag
                    class="margin-right-1"
                    ng-if="$ctrl.template.statusId === $ctrl.TemplateStates.Published"
                    label-class="completed"
                    plain-text="{{::$ctrl.translate('Published')}}"
                    >
                </one-tag>
                <one-tag
                    class="margin-right-1"
                    ng-if="$ctrl.template.statusId === $ctrl.TemplateStates.Draft"
                    plain-text="{{::$ctrl.translate('Draft')}}"
                    >
                </one-tag>
            </header-title>
            <header-body class="dm-template-header__body row-vertical-center row-flex-end">
                <one-tag
                    ng-if="!$ctrl.template.editMode && $ctrl.template.statusId === $ctrl.TemplateStates.Published"
                    plain-text="{{::$ctrl.translate('ReadOnly')}}"
                    >
                </one-tag>
                <one-tag
                    ng-if="$ctrl.template.editMode"
                    label-class="completed"
                    plain-text="{{::$ctrl.translate('EditMode')}}"
                    >
                </one-tag>
            </header-body>
            <div class="row-vertical-center">
                <one-dropdown
                    class="one-header__button margin-right-1"
                    ng-if="$ctrl.canEditTemplate()"
                    text="{{::$ctrl.translate('EditTemplate')}}"
                    options="$ctrl.editOptions"
                    list-class="dm-template-header__edit-list"
                    item-class="dm-template-header__edit-item text-center"
                    >
                </one-dropdown>
                <one-button
                    class="one-header__button margin-right-1"
                    ng-if="$ctrl.template.editMode"
                    text="{{::$ctrl.translate('SwitchToReadOnly')}}"
                    button-click="$ctrl.toggleEditThisVersion()"
                    identifier="templateSwitchToReadOnly"
                    >
                </one-button>
                <one-button
                    class="one-header__button margin-right-1"
                    ng-if="$ctrl.Permissions.canShow('DataMappingTemplateShowVersions') && $ctrl.template.canEdit"
                    text="{{$ctrl.template.showingVersionHistory ? $ctrl.translate('HideVersions') : $ctrl.translate('ShowVersions')}}"
                    button-click="$ctrl.toggleVersionHistory()"
                    identifier="templateToggleVersionHistory"
                    >
                </one-button>
                <one-button
                    class="one-header__button"
                    ng-if="$ctrl.template.statusId === $ctrl.TemplateStates.Draft && $ctrl.Permissions.canShow('DataMappingTemplatePublish')"
                    text="{{::$ctrl.translate('PublishTemplate')}}"
                    button-click="$ctrl.publishTemplate()"
                    identifier="templatePublish"
                    >
                </one-button>
            </div>
        </one-header>
    `,
};
