import { IDMAssessmentTemplate } from "modules/template/interfaces/template.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "modules/shared/services/provider/notification.service";

class DmTemplateController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "$state", "OneTemplate", "ENUMS", "NotificationService"];

    public ready = false;
    public template: any = {};
    public templateId: string;
    private translate: any;
    private itemsUpdating = false;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, readonly $state: ng.ui.IStateService, readonly OneTemplate: any, readonly ENUMS: any, private notificationService: NotificationService) {
        this.templateId = $state.params.templateId;
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.OneTemplate.init(this.templateId, null, this.$state.params.templateData).then((response: any): void => {
            if (!response.result) return;
            this.template = response.data;
            this.$state.params.templateData = null;
            this.ready = true;
            this.startEventWatchers();
        });
    }

    private startEventWatchers(): void {
        this.$scope.$on("TOGGLE_EDIT_THIS_VERSION", (): void => {
            this.template = this.OneTemplate.toggleEditMode();
        });
        this.$scope.$on("TOGGLE_VERSION_HISTORY", (): void => {
            this.template = this.OneTemplate.toggleVersionHistory();
        });
        this.$scope.$on("EDIT_SECTION", (event: ng.IAngularEvent, sectionId: string): void => {
            const callback = (response) => {
                this.template = response.data;
            };
            this.OneTemplate.launchSectionModal(callback, sectionId);
        });
        this.$scope.$on("ADD_SECTION", (): void => {
            const callback = (response) => {
                this.template = response.data;
            };
            this.OneTemplate.launchSectionModal(callback);
        });
        this.$scope.$on("EDIT_QUESTION", (event: ng.IAngularEvent, sectionId: string, question: any): void => {
            this.OneTemplate.launchQuestionModal(sectionId, question).then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("ADD_QUESTION", (event: ng.IAngularEvent, sectionId: string, config?: any): void => {
            this.OneTemplate.launchQuestionModal(sectionId, null, config).then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("ADD_PLACEHOLDER_QUESTION", (event: ng.IAngularEvent, sectionId: string, config?: any): void => {
            this.OneTemplate.addPlaceholderQuestion(sectionId, config).then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("EDIT_CONDITIONS", (event: ng.IAngularEvent, sectionId: string, question: any): void => {
            this.OneTemplate.launchConditionsModal(sectionId, question).then((response: any): void => {
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("EDIT_NEW_VERSION", (): void => {
            this.ready = false;
            this.OneTemplate.goToNewVersion().then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
                this.$state.params.templateData = null;
                this.ready = true;
            });
        });
        this.$scope.$on("DELETE_SECTION", (event: ng.IAngularEvent, section: any): void => {
            this.OneTemplate.deleteSection(section).then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("DELETE_QUESTION", (event: ng.IAngularEvent, question: any): void => {
            this.OneTemplate.deleteQuestion(question).then((response: any): any => {
                if (!response.result) return;
                this.template = response.data;
            });
        });

        this.$scope.$on("PUBLISH_TEMPLATE", (): void => {
            const templateSectionLength: number = this.template.sections.length;
            let allSectionHaveQuestions = true;
            if (templateSectionLength > 1) {
                for (let i = 1; i < templateSectionLength; i++) {
                    if (this.template.sections[i].questions.length <= 1) {
                        allSectionHaveQuestions = false;
                        break;
                    }
                }
            }
            if (!(templateSectionLength > 1 && allSectionHaveQuestions)) {
                const alertText: string = this.translate("TemplateHasNoSectionsOrNoQuestions");
                this.notificationService.alertWarning(this.translate("Warning"), (alertText));
            } else {
                this.OneTemplate.publishTemplate().then((response: any): void => {
                    if (response.result) {
                        this.template = response.data;
                    }
                    this.ready = true;
                });
            }
        });

        this.$scope.$on("TOGGLE_SECTION", (event: ng.IAngularEvent, sectionId: string): void => {
            this.template = this.OneTemplate.toggleSection(sectionId);
        });
        this.$scope.$on("TOGGLE_QUESTION", (event: ng.IAngularEvent, questionId: string): void => {
            this.template = this.OneTemplate.toggleQuestion(questionId);
        });
        this.$scope.$on("TOGGLE_HIDE_QUESTION", (event: ng.IAngularEvent, sectionId: string, questionId: string): void => {
            this.itemsUpdating = true;
            this.OneTemplate.toggleHideQuestion(sectionId, questionId).then((updatedTemplate: IDMAssessmentTemplate): void => {
                this.itemsUpdating = false;
                if (updatedTemplate) {
                    this.template = updatedTemplate;
                }
            });
        });
        this.$scope.$on("GO_TO_VERSION", (event: ng.IAngularEvent, templateId: string): void => {
            this.ready = false;
            this.OneTemplate.goToVersion(templateId).then((response: any): void => {
                if (response.result) {
                    this.template = response.data;
                }
                this.ready = true;
            });
        });
        this.$scope.$on("DELETE_VERSION", (event: ng.IAngularEvent, templateId: string, isCurrentVersion: boolean): void => {
            this.ready = !isCurrentVersion;
            this.OneTemplate.deleteVersion(templateId, isCurrentVersion).then((response: any): void => {
                if (response.result) {
                    this.template = response.data;
                }
                this.ready = true;
            });
        });
        this.$scope.$on("MOVE_QUESTION", (event: ng.IAngularEvent, moveConfig: any): void => {
            this.itemsUpdating = true;
            this.OneTemplate.moveQuestion(moveConfig).then((response: any): undefined | void => {
                this.itemsUpdating = false;
                if (!response.result) return;
                this.template = response.data;
            });
        });
        this.$scope.$on("DM_TEMPLATE_START_LOADING", (): void => {
            this.ready = false;
        });
    }
}

const dmTemplateComponent: ng.IComponentOptions = {
    controller: DmTemplateController,
    template:  `
        <loading
            ng-if="!$ctrl.ready"
            loading-class="dm-template__loading"
            show-text="false"
            >
        </loading>
        <section
            class="dm-template"
            ng-if="$ctrl.ready"
            >
                <dm-template-header
                    class="dm-template__header"
                    template="$ctrl.template"
                    >
                </dm-template-header>
                <section class="dm-template__body">
                    <template-sidebar
                        class="dm-template__sidebar"
                        ng-if="$ctrl.template.statusId === $ctrl.ENUMS.DMTemplateStates.Draft"
                        template-type="::$ctrl.ENUMS.TemplateTypes.NewDatamapping"
                        disabled-draggable-types="$ctrl.template.disabledDraggableTypes"
                        >
                    </template-sidebar>
                    <template-section-list
                        class="dm-template__list"
                        list="$ctrl.template.sections"
                        is-disabled="$ctrl.itemsUpdating"
                        read-only="!$ctrl.template.editMode"
                        template-state="$ctrl.template.statusId"
                        >
                    </template-section-list>
                    <template-version-list
                        class="dm-template__versions"
                        ng-if="$ctrl.template.showingVersionHistory"
                        version-list="$ctrl.template.versionList"
                        current-version="$ctrl.template.templateVersion"
                        >
                    </template-version-list>
                </section>
        </section>
    `,
};

export default dmTemplateComponent;
