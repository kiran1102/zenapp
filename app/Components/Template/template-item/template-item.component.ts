const templateItem: ng.IComponentOptions = {
    bindings: {
        item: "<",
        readOnly: "<?",
        actions: "<?",
        label: "@",
        valueKey: "@?",
        toggleItemOpen: "<?",
        isDraggable: "<?",
    },
    transclude: {
        preview: "?preview",
        children: "?children",
    },
    template: () => {
        return `
            <section class="template-item">
                <div
                    class="template-item__container"
                    ng-if="!$ctrl.item.isLoadingPlaceholder"
                    ng-class="{ 'template-item__container--disabled': $ctrl.item.disabled }"
                    >
                    <button
                        class="template-item__toggle text-bold"
                        ng-if="$ctrl.toggleItemOpen && !$ctrl.item.disabled"
                        ng-click="$ctrl.toggleItemOpen()"
                        >
                            <i
                                class="template-section__welcome-icon ot ot-star"
                                ng-if="$ctrl.item.welcomeSection"
                                >
                            </i>
                            <span
                                class="template-item__toggle-title"
                                ng-if="!$ctrl.item.welcomeSection"
                                >
                                    {{$ctrl.label}} {{$ctrl.item.number}}
                            </span>
                            <i
                                class="template-item__toggle-arrow fa"
                                ng-class="{
                                    'fa-caret-right': !$ctrl.item.isOpen,
                                    'fa-caret-down': $ctrl.item.isOpen,
                                }"
                                >
                            </i>
                    </button>
                    <button
                        class="template-item__toggle text-bold"
                        ng-if="$ctrl.item.disabled"
                        ng-click="$ctrl.toggleItemOpen()"
                        >
                            <i class="template-section__welcome-icon fa fa-eye-slash"></i>
                    </button>
                    <section
                        class="template-item__text text-normal"
                        ng-if="$ctrl.isDraggable"
                        ui-tree-handle
                        ng-class="{'template-item__text--welcome': $ctrl.item.welcomeSection}"
                        >
                            {{ $ctrl.item.name }}
                            <sup
                                ng-if="$ctrl.item.required"
                                class="helper-icon-required"
                                >
                            </sup>
                    </section>
                    <section
                        class="template-item__text text-normal"
                        ng-if="!$ctrl.isDraggable"
                        ng-class="{'template-item__text--welcome': $ctrl.item.welcomeSection}"
                        >
                            {{ $ctrl.item.name }}
                            <sup
                                ng-if="$ctrl.item.required"
                                class="helper-icon-required"
                                >
                            </sup>
                    </section>
                    <section
                        class="template-item__actions"
                        ng-if="!$ctrl.item.welcomeSection"
                        >
                        <action-icons
                            actions="$ctrl.actions"
                            target="{id: $ctrl.item[$ctrl.valueKey]}"
                            >
                        </action-icons>
                    </section>
                </div>
                <div
                    class="template-item__container"
                    ng-if="$ctrl.item.isLoadingPlaceholder"
                    >
                        <loading
                            class="template-item__loading"
                            show-text="false"
                            >
                        </loading>
                </div>
                <div
                    ng-if="$ctrl.item.isOpen"
                    ng-transclude="preview"
                    >
                </div>
                <div
                    ng-if="$ctrl.item.isOpen"
                    ng-transclude="children"
                    >
                </div>
            </section>
        `;
    },
};

export default templateItem;
