import * as _ from "lodash";
class QuestionBank {

    static $inject: string[] = ["$state", "$scope", "ENUMS", "QuestionTypes"];

    private draggableTypes: any[];
    private templateType: number;
    private targetClass: string;
    private DragTypes: any;
    private questionOptions: any;
    private loadingPlaceholder: any;
    private typeId: number;

    constructor(readonly $state: any, readonly $scope: any, readonly ENUMS: any, readonly QuestionTypes: any) {}

    $onInit() {
        this.typeId = _.toInteger(this.$state.params.templateType);
        this.draggableTypes = this.QuestionTypes.getDraggableQuestionTypes(this.templateType, this.typeId);
        this.DragTypes = this.ENUMS.DragTypes;
        this.questionOptions = {
            accept: this.accept,
            dropped: this.dropped,
            beforeDrop: this.beforeDrop,
        };
        this.loadingPlaceholder = { isLoadingPlaceholder: true };
    }

    public accept = (source: any, dest: any, index: number): boolean => {
        return false;
    }

    public dropped = (event: any): void => {
        this.$scope.$emit("QUESTION_DROPPED");
    }

    public beforeDrop = (event: any): boolean => {
        const source: any = event.source.nodeScope;
        const destination: any = event.dest.nodesScope;
        if (destination.$element[0].className.indexOf(this.targetClass) === -1) {
            return false;
        } else if (source.$modelValue) {
            if (destination.$parent && destination.$parent.$parent && destination.$parent.$parent.$ctrl) {
                const sectionId: string = destination.$parent.$parent.$ctrl.sectionId;
                const questionList: any = destination.$ctrl.list;
                const destinationIndex: number = event.dest.index;
                let previousQuestionIdTemp = "";
                if (questionList.length) {
                    const question: any = questionList[destinationIndex - 1];
                    if (question) {
                        previousQuestionIdTemp = question.templateQuestionUniqueId;
                    } else if (destinationIndex === -1) {
                        const lastQuestion: any = questionList[questionList.length - 1];
                        previousQuestionIdTemp = lastQuestion.templateQuestionUniqueId;
                    }
                }
                const previousQuestionId = previousQuestionIdTemp;
                switch (source.$modelValue.DragType) {
                    case this.DragTypes.QuestionAddGroup:
                        questionList.splice(destinationIndex, 0, this.loadingPlaceholder);
                        this.$scope.$emit("ADD_PLACEHOLDER_QUESTION", sectionId, {
                            questionType: source.$modelValue.Type,
                            previousQuestionId,
                            index: event.dest.index,
                        });
                        break;
                    case this.DragTypes.QuestionAdd:
                        this.$scope.$emit("ADD_QUESTION", sectionId, {
                            questionType: source.$modelValue.Type,
                            previousQuestionId,
                            index: event.dest.index,
                        });
                        break;
                    default:
                        break;
                }
            }
            return false;
        }
    }
}

const questionBank: ng.IComponentOptions = {
    controller: QuestionBank,
    bindings: {
        templateType: "<",
        targetClass: "@",
        disabledTypes: "<",
    },
    template: (): string => {
        return `
            <div
                class="question-bank"
                ui-tree="$ctrl.questionOptions"
                data-clone-enabled="true"
                data-nodrop-enabled="true"
                >
                    <ul
                        class="question-bank__list"
                        ui-tree-nodes
                        ng-model="$ctrl.draggableTypes"
                        >
                            <li
                                class="question-bank__item text-center"
                                ui-tree-node
                                ng-repeat="type in $ctrl.draggableTypes track by $index"
                                ng-class="{'question-bank__item--disabled': $ctrl.disabledTypes[type.Type]}"
                                >
                                    <span
                                        class="question-bank__text"
                                        ui-tree-handle
                                        >
                                        {{ ::type.TypeName }}
                                    </span>
                            </li>
                    </ul>
            </div>
        `;
    },
};

export default questionBank;
