import Utilities from "Utilities";
import * as _ from "lodash";
import $ from "jquery";
import JQuery from "jquery";

class SearchableDropdownController {
    private uuid: string;
    private model: any; // value that is two-way bound with question object/model
    private options: any; // array of items to display in the dropdown
    private optionLabelKey: string; // <ng-repeat="option in options"> <span> option[optionLabelKey] </span> </>
    private optionValueKey: string; // this.model = option[optionValueKey]
    private placeholder: string;
    private selectedIndex: number; // index of the highlighted object
    private dropdownOpen = false;
    private inputText: string; // text that should display in the input
    private tooltipEnabled: boolean;
    private dropdownClass: string;
    private selectCallback: (arg: any) => any;
    private identifier: string;
    private keyEnums: any = {
        Enter: 13,
        Escape: 27,
        UpArrow: 38,
        DownArrow: 40,
        LeftArrow: 37,
        RightArrow: 39,
    };

    $onInit() {
        this.inputText = _.isNil(this.model) ? this.model : this.getSelectionValue(Number(this.model), this.optionLabelKey);
        this.uuid = Utilities.uuid();
    }

    public $onChanges(changes: ng.IOnChangesObject) {
        if (changes.model) {
            this.inputText = _.isNil(this.model) ? this.model : this.getSelectionValue(Number(this.model), this.optionLabelKey);
        }
    }

    // arrow keypresses are handled with keydown event to allow holding arrow key
    private keyDown(e: any): void {
        if (e.type === "keydown") {
            switch (e.keyCode) {
                case this.keyEnums.UpArrow:
                    this.decrementIndex();
                    this.findMatchingSelectionThenOpenDropdown(this.inputText);
                    this.checkIfSelectionIsInView(this.selectedIndex);
                    break;
                case this.keyEnums.DownArrow:
                    this.incrementIndex();
                    this.findMatchingSelectionThenOpenDropdown(this.inputText);
                    this.checkIfSelectionIsInView(this.selectedIndex);
                    break;
                case this.keyEnums.Enter:
                    this.closeDropdown();
                    this.saveSelection(this.selectedIndex);
                    break;
                case this.keyEnums.Escape:
                    this.closeDropdown();
                    this.deselect();
                    this.revertInputText(); // take this line out to remove escape undo function
                    break;
                default:
                    break;
            }
        }
    }

    private findMatchingSelectionThenOpenDropdown(value: any): void {
        if (!this.dropdownOpen) {
            if (value) this.findMatchingSelection(value);
            this.openDropdown();
        }
    }

    private keyUp(e: any): void {
        if (!this.isInteractionKey(e)) {
            this.findMatchingSelection(e.target.value);
            this.openDropdown();
        }
    }

    private isInteractionKey(e: any): boolean {
        return _.values(this.keyEnums).indexOf(e.keyCode) !== -1;
    }

    private saveSelection(index: number): void {
        if (index >= 0) {
            this.inputText = this.getSelectionValue(index, this.optionLabelKey);
            this.model = this.getSelectionValue(index, this.optionValueKey);
        } else {
            this.inputText = null;
            this.model = null;
        }
        if (_.isFunction(this.selectCallback)) {
            this.selectCallback({ dataFromDropdown: this.model });
        }
    }

    private getSelectionValue(index: number, key: string): string {
        if (this.options[index]) {
            return this.options[index][key];
        }
    }

    private dropdownToggled(): void {
        this.findMatchingSelection(this.inputText);
        if (!this.dropdownOpen) this.shouldModelReset();
        this.checkIfSelectionIsInView(this.selectedIndex);
    }

    private shouldModelReset(): void {
        if (!this.inputText) {
            this.model = null;
            this.selectedIndex = null;
        }

        if (this.model) {
            this.revertInputText();
        } else {
            this.inputText = null;
        }
    }

    private revertInputText(): void {
        this.inputText = this.getSelectionValue(Number(this.model), this.optionLabelKey);
    }

    private openDropdown(): void {
        this.dropdownOpen = true;
        this.checkIfSelectionIsInView(this.selectedIndex);
    }

    private closeDropdown(): void {
        this.dropdownOpen = false;
    }

    private findMatchingSelection(str: string): void {
        const reg: RegExp = this.buildRegEx(str);
        this.selectedIndex = this.findMatchIndex(this.options, reg);
    }

    private buildRegEx(str: string): any {
        if (_.isUndefined(str) || str === null) str = "";
        return new RegExp(`[${str}]`, "g"); // Don't remove "g" flag or the app will go into an infinite loop!!!
    }

    private findMatchIndex(options: any, reg: any): number {
        const [firstMatch, ...rest] = options
            .map((option: any, i: number): any => {
                const result = [];
                let match = reg.exec(option[this.optionLabelKey]);
                while (match != null) {
                    [match] = match;
                    if (!_.includes(result, match)) result.push(match);
                    match = reg.exec(option[this.optionLabelKey]);
                }
                reg.lastIndex = 0; // reset regex object's cached index. you can also do reg.exec("")
                return {
                    index: i,
                    matchLength: result.length,
                };
            })
            .filter((regResult: any): boolean => regResult.matchLength > 0)
            .sort((a: any, b: any): number => b.matchLength - a.matchLength);

        return firstMatch ? firstMatch.index : -1;
    }

    private deselect(): void {
        this.selectedIndex = null;
    }

    private incrementIndex(): void {
        if (_.isNumber(this.selectedIndex)) {
            this.selectedIndex++;
            if (this.selectedIndex > this.options.length - 1) this.selectedIndex = 0; // set index to 0 if the selection goes over the last item
        } else {
            this.selectedIndex = 0;
        }
    }

    private decrementIndex(): void {
        if (_.isNumber(this.selectedIndex)) {
            this.selectedIndex--;
            if (this.selectedIndex < 0) this.selectedIndex = this.options.length - 1; // set index to last index if the selection goes before the first item
        } else {
            this.selectedIndex = this.options.length - 1;
        }
    }

    private checkIfSelectionIsInView(index: number): void {
        const listDomElem: HTMLElement = $(`div.${this.uuid} ul`)[0];
        const selectedDomElem: HTMLElement = $(`div.${this.uuid} ul li`)[index];

        if (listDomElem && selectedDomElem) {
            const scrollAmount: number = listDomElem.scrollTop;
            const visibleListHeight: number = listDomElem.clientHeight;

            const selectedYOffset: number = selectedDomElem.offsetTop;
            const selectedHeight: number = selectedDomElem.clientHeight;

            const selectionIsBelowTopScrollWindow: boolean = selectedYOffset >= scrollAmount;
            const selectionIsAboveBottomScrollWindow: boolean = selectedYOffset + selectedHeight <= scrollAmount + visibleListHeight;

            if (!selectionIsBelowTopScrollWindow) {
                listDomElem.scrollTop = selectedYOffset;
            } else if (!selectionIsAboveBottomScrollWindow) {
                listDomElem.scrollTop = selectedYOffset + selectedHeight - visibleListHeight;
            }
        }
    }
}

const SearchableDropdownComponent: any = {
    bindings: {
        model: "<",
        options: "<",
        optionLabelKey: "@",
        optionValueKey: "@",
        placeholder: "@?",
        tooltipEnabled: "=",
        dropdownClass: "@?",
        selectCallback: "&?",
        identifier: "@?",
    },
    controller: SearchableDropdownController,
    template: `
                <div
                    id="{{::$ctrl.identifier}}"
                    class="one-searchable-dropdown {{::$ctrl.dropdownClass}} {{::$ctrl.uuid}}"
                    uib-dropdown
                    keyboard-nav
                    is-open="$ctrl.dropdownOpen"
                    on-toggle="$ctrl.dropdownToggled()"
                    auto-close="always outsideClick">

                    <input
                        class="one-searchable-dropdown__input padding-top-bottom-0 padding-left-right-1 width-100-percent"
                        uib-dropdown-toggle
                        placeholder="{{::($ctrl.placeholder || '')}}"
                        ng-keyup="$ctrl.keyUp($event)"
                        ng-keydown="$ctrl.keyDown($event)"
                        ng-model="$ctrl.inputText">
                    <i
                        class="one-searchable-dropdown__icon fa fa-caret-down",
                        ng-if="!$ctrl.dropdownOpen && !$ctrl.model"/>

                    <ul
                        class="one-searchable-dropdown__list width-100-percent text-left shadow shadow-transition"
                        uib-dropdown-menu
                        role="menu"
                        ng-if="$ctrl.options.length">

                        <li
                            class="one-searchable-dropdown__item-wrapper"
                            uib-tooltip="{{option.Hint}}"
                            tooltip-class="one-searchable-dropdown__tooltip--offset-left"
                            tooltip-placement="top-left"
                            tooltip-append-to-body="true"
                            tooltip-animation="false"
                            tooltip-enable="$ctrl.tooltipEnabled"
                            ng-repeat="(index, option) in $ctrl.options track by $index">

                            <a
                                class="one-searchable-dropdown__item"
                                ng-class="{'selected': index === $ctrl.selectedIndex}"
                                ng-click="$ctrl.saveSelection(index)">

                                <span>
                                    {{option[$ctrl.optionLabelKey]}}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            `,

};

export default SearchableDropdownComponent;
