import * as _ from "lodash";

export default {
    bindings: {
        checked: "<",
    },

    /* tslint:disable:only-arrow-functions object-literal-shorthand */
    controller: function() {
        this.checked = !_.isUndefined(this.checked) ? this.checked : false;
    },
    /* tslint:enable: */

    template: () => {
        return `<span class="check" ng-if="$ctrl.checked">&check;</span>`;
    },
};
