export default {
    bindings: {
        buttonText: "@",
        buttonType: "@",
        buttonIdentifier: "@",
        options: "<",
        buttonAction: "&",
        isDisabled: "<?",
        isLoading: "<?",
        enableLoading: "<?",
    },
    template: `
        <div class="one-split-dropdown">
            <one-button
                button-class="one-split-dropdown__button"
                type="{{::$ctrl.buttonType}}"
                text="{{::$ctrl.buttonText}}"
                button-click="::$ctrl.buttonAction()"
                is-disabled="$ctrl.isDisabled"
                identifier="{{::$ctrl.buttonIdentifier}}"
                enable-loading="$ctrl.enableLoading"
                is-loading="$ctrl.isLoading"
                is-rounded="true"
                >
            </one-button>
            <one-dropdown
                button-class="one-split-dropdown__dropdown one-button--{{::$ctrl.buttonType}}"
                options="$ctrl.options"
                is-disabled="$ctrl.isDisabled"
                align-right="true"
                icon="ot ot-angle-down"
                hide-caret="true"
                >
            </one-dropdown>
        </div>
    `,
};
