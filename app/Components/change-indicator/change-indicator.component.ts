import * as _ from "lodash";

export default {
    bindings: {
        changedBy: "<",
    },

    /* tslint:disable:only-arrow-functions object-literal-shorthand */
    controller: function() {
        const DEFAULT: any = {
            changedBy: 0,
        };

        this.caretClass = this.changedBy > 0 ? "fa-caret-up" : "fa-caret-down";
        this.changedBy = !_.isUndefined(this.changedBy) ? this.changedBy : DEFAULT.changedBy;
    },
    /* tslint:enable: */

    template: () => {
        return `<span class='change-indicator'>
				<span ng-if='$ctrl.changedBy == 0'>&ndash;</span>
				<i ng-if='$ctrl.changedBy != 0' class='fa {{$ctrl.caretClass}}'></i>
				<span ng-if='$ctrl.changedBy != 0' ng-bind='$ctrl.changedBy | absolute' class='changed-by-value'></span>
			</span>`;
    },
};
