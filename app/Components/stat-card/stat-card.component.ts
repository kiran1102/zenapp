
const statCard = {
    bindings: {
        cards: "<",
        cardSizeClass: "<",
    },
    template: () => {
        return `<div class="stat-cards">
                            <div class="stat-cards__container"
                                ng-repeat="card in $ctrl.cards track by $index"
                                ng-class="(card.inverseColors ? card.classInverse : card.class) + ' ' + $ctrl.cardSizeClass">
                                <span class="stat-cards__number text-bold">{{card.number}}</span>
                                <label class="stat-cards__label">{{::$root.t(card.label)}}</label>
                            </div>
                        </div>`;
    },
};

export default statCard;
