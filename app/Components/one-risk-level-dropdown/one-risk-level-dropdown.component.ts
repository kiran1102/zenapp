import _ from "lodash";

class RiskLevelDropdown {
}

const OneRiskLevelDropdownComponent = {
    controller: RiskLevelDropdown,
    bindings: {
        onRiskLevelChange: "&?",
        alignRight: "<?",
    },
    template: `
        <ul
            ng-class="{ 'dropdown-menu-right': $ctrl.alignRight }"
            uib-dropdown-menu
            role="menu"
            class="risk-level-dropdown margin-top-1"
        >
            <div class="text-bold" style="color: #8b8b8b; margin-bottom: 0.3rem;">Set Risk Level:</div>
            <div style="display: flex;justify-content: center;">
                <span class="risk-level-round-button
                             risk-level-round-button--hover-enabled
                             risk-level-round-button--shadow-enabled
                             risk-very-high--bg
                             row-horizontal-vertical-center
                             margin-right-1"
                    ng-click="$ctrl.onRiskLevelChange({ value: 4 })"
                >
                    <i class="fa fa-flag fa-inverse text-xsmall"></i>
                </span>
                <span class="risk-level-round-button
                             risk-level-round-button--hover-enabled
                             risk-level-round-button--shadow-enabled
                             risk-high--bg
                             row-horizontal-vertical-center
                             margin-right-1"
                    ng-click="$ctrl.onRiskLevelChange({ value: 3 })"
                >
                    <i class="fa fa-flag fa-inverse text-xsmall"></i>
                </span>
                <span class="risk-level-round-button
                             risk-level-round-button--hover-enabled
                             risk-level-round-button--shadow-enabled
                             risk-medium--bg
                             row-horizontal-vertical-center
                             margin-right-1"
                    ng-click="$ctrl.onRiskLevelChange({ value: 2 })"
                >
                    <i class="fa fa-flag fa-inverse text-xsmall"></i>
                </span>
                <span class="risk-level-round-button
                             risk-level-round-button--hover-enabled
                             risk-level-round-button--shadow-enabled
                             risk-low--bg
                             row-horizontal-vertical-center"
                    ng-click="$ctrl.onRiskLevelChange({ value: 1 })"
                >
                    <i class="fa fa-flag fa-inverse text-xsmall"></i>
                </span>
            </div>
        </ul>
    `,
};

export default OneRiskLevelDropdownComponent;
