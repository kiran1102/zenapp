class OneSearchInputCtrl implements ng.IComponentController {
    private model: string;
    private onSearch: ({model: string, clear: boolean}) => void;
    private onKeypress: ({event}) => void;

    private clearInput(): void {
        this.model = "";
        this.onSearch({ model: this.model, clear: true });
    }

}

export default {
    bindings: {
        model: "<",
        onSearch: "&",
        placeholder: "@?",
        throttle: "<?",
        inputName: "@?",
        inputId: "@?",
        inputClass: "@?",
        searchClass: "@?",
        onKeypress: "&?",
        identifier: "@?",
    },
    controller: OneSearchInputCtrl,
    template: `
        <form class="one-search-input min-width-20 margin-all-0 border row-vertical-center full-width background-white {{$ctrl.searchClass}}">
            <input
                ot-auto-id="{{$ctrl.identifier + 'TextBox'}}"
                id="{{::$ctrl.inputId}}"
                name="{{::$ctrl.inputName}}"
                type="search"
                class="one-input one-search-input__input no-outline border-none flex {{$ctrl.inputClass}}"
                placeholder="{{::$ctrl.placeholder}}"
                ng-model="$ctrl.model"
                ng-change="$ctrl.onSearch({ model: $ctrl.model })"
                ng-model-options="{debounce: $ctrl.throttle || 0}"
                ng-keypress="$ctrl.onKeypress({ event : $event })"
            />
            <label class="one-input__icon padding-left-right-1 margin-all-0" for="{{::$ctrl.inputName}}">
                <i class="ot ot-search" ng-if="!$ctrl.model"></i>
                <i class="ot ot-times" ng-if="$ctrl.model" ng-click="$ctrl.clearInput()" ot-data-id="{{$ctrl.identifier +'_'+ 'ClearInputIcon'}}"></i>
            </label>
        </form>
    `,
};
