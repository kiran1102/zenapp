class OverflowTooltipCtrl implements ng.IComponentController {
    static $inject: string[] = ["$element"];

    private containerEl: Element;
    private content: string;
    private tooltipContent: string;

    constructor(private readonly $element: ng.IAugmentedJQuery) {}

    $onInit(): void {
        this.tooltipContent = this.content;
        this.containerEl = this.$element[0].children[0];
    }

    private checkForOverflow(): void {
        if (this.containerEl) {
            this.tooltipContent = this.containerEl.scrollWidth > this.containerEl.clientWidth ? this.content : "";
        }
    }
}

export default  {
    controller: OverflowTooltipCtrl,
    bindings: {
        content: "@",
        tooltipPlacement: "@?",
        tooltipClass: "@?",
    },
    template: `
		<span
            class="text-nowrap overflow-ellipsis"
            uib-tooltip="{{$ctrl.tooltipContent}}"
            tooltip-append-to-body="true"
            tooltip-placement="auto {{::$ctrl.tooltipPlacement || 'top'}}"
            tooltip-class="{{::$ctrl.tooltipClass}}"
            ng-mouseover="$ctrl.checkForOverflow()"
            ng-bind="$ctrl.content"
            >
		</span>
	`,
};
