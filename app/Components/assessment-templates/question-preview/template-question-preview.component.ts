// Constants
import { QuestionTypes } from "constants/question-types.constant";

// Interfaces
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IQuestionTypes } from "modules/template/interfaces/template.interface";
import { QuestionAnswerTypes } from "enums/question-types.enum";

export class TemplateQuestionPreview implements ng.IComponentController {
    questionAnswerTypes = QuestionAnswerTypes;

    private question: IQuestionDetails | null;
    private questionTypes: IStringMap<IQuestionTypes> = QuestionTypes;
    private appendToBody = false;
}

const questionPreviewModal: IStringMap<string> = {
    template: `
        <div class="one-label-content margin-top-2 padding-left-right-2 question-preview">
            <div
                class="text-bold one-label-content__text row-vertical-center"
                ng-if="$ctrl.question.content"
            >
                <div class="word-wrap">
                    <sup
                        ng-if="$ctrl.question.required"
                        class="helper-icon-required"
                    ></sup>
                    <span ng-bind="$ctrl.question.content"></span>
                    <i
                        ng-if="$ctrl.question.hint"
                        class="ot ot-info-circle margin-left-1"
                        uib-tooltip="{{$ctrl.question.hint}}"
                        tooltip-placement="top-right"
                        tooltip-append-to-body="true"
                        tooltip-enabled="Boolean($ctrl.question.hint)"
                    ></i>
                </div>
            </div>
            <div
                class="question-preview__description"
                ng-if="$ctrl.question.description"
            >
                <p
                    ng-bind-html="$ctrl.question.description"
                    class="ql-editor padding-left-0"
                ></p>
            </div>
            <question-textbox
                question="$ctrl.question"
                is-disabled="false"
                ng-if="$ctrl.questionTypes.TextBox.name === $ctrl.question.questionType"
            ></question-textbox>
            <multiselect-button-question
                ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiChoice.name && (!$ctrl.question.attributeJson.displayAnswerType || $ctrl.question.attributeJson.displayAnswerType === $ctrl.questionAnswerTypes.Button)  && $ctrl.question.attributeJson.isMultiSelect"
                question="$ctrl.question"
            >
            </multiselect-button-question>
            <multiselect-dropdown-question
                ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiChoice.name &&  $ctrl.question.attributeJson.displayAnswerType === $ctrl.questionAnswerTypes.Dropdown && $ctrl.question.attributeJson.isMultiSelect"
                question="$ctrl.question"
            >
            </multiselect-dropdown-question>
            <single-select-dropdown-question
                append-to-body="$ctrl.appendToBody"
                ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiChoice.name && $ctrl.question.attributeJson.displayAnswerType === $ctrl.questionAnswerTypes.Dropdown && !$ctrl.question.attributeJson.isMultiSelect"
                question="$ctrl.question"
            >
            </single-select-dropdown-question>
            <single-select-button-question
                ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiChoice.name && (!$ctrl.question.attributeJson.displayAnswerType || $ctrl.question.attributeJson.displayAnswerType === $ctrl.questionAnswerTypes.Button) && !$ctrl.question.attributeJson.isMultiSelect"
                question="$ctrl.question"
            >
            </single-select-button-question>
            <question-yes-no
                question="$ctrl.question"
                is-disabled="false"
                ng-if="$ctrl.questionTypes.YesNo.name === $ctrl.question.questionType"
            ></question-yes-no>
            <question-date
                question="$ctrl.question"
                is-disabled="false"
                ng-if="$ctrl.questionTypes.Date.name === $ctrl.question.questionType"
            ></question-date>
            <div ng-if="$ctrl.questionTypes.Attribute.name === $ctrl.question.questionType">
                <one-label-content
                    is-required="$ctrl.question.attributeJson.requireJustification"
                    name="{{::$root.t('JustifyYourAnswerBelow')}}"
                    ng-if="$ctrl.question.attributeJson.allowJustification"
                    wrapper-class="margin-top-1"
                >
                    <one-textarea
                        placeholder="{{::$root.t('EnterJustificationsHere', {Justifications: $root.customTerms.Justification.toLowerCase()})}}"
                    >
                    </one-textarea>
                </one-label-content>
            </div>
        </div>
    `,
};

export const TemplateQuestionPreviewComponent = {
    controller: TemplateQuestionPreview,
    template: questionPreviewModal.template,
    bindings: {
        question: "<",
    },
};
