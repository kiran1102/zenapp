import {
    cloneDeep,
    forEach,
    isString,
    isArray,
    find,
    findIndex,
    toLower,
    some,
} from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IEditableTypeConfig } from "interfaces/editable-type-config.interface";
import { IStringMap } from "interfaces/generic.interface";
import {
    IQuestionDetails,
    IQuestionOptionDetails,
    IQuestionAnswer,
} from "interfaces/assessment-template-question.interface";

import { QuestionResponseType } from "constants/question-types.constant";

export interface IMultichoiceQuestionAnswer {
    responses?: IQuestionAnswer[];
    otherResponses?: IQuestionAnswer[];
    notSure?: boolean;
    notApplicable?: boolean;
}

class MultiselectButtonQuestionController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
    ];

    private answer: IMultichoiceQuestionAnswer;
    private newOtherConfig: IEditableTypeConfig;
    private newOtherModel = "";
    private options: IQuestionOptionDetails[];
    private otherConfig: IEditableTypeConfig;
    private question: IQuestionDetails;
    private translations: IStringMap<string>;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            enterJustificationHere: this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() }),
            justifyYourAnswerBelow: this.$rootScope.t("JustifyYourAnswerBelow"),
            notSure: this.$rootScope.t("NotSure"),
            notApplicable: this.$rootScope.t("NotApplicable"),
        };
        // TODO: add type and enum for the types
        this.otherConfig = {
            isEditable: true,
            isButton: true,
            inputType: 10,
        };
        this.newOtherConfig = { ...this.otherConfig, clearOnSelect: true };
        this.initialize();
    }

    public $onChanges(data: ng.IOnChangesObject): void {
        if (!data.question.isFirstChange()) {
            this.initialize();
        }
    }

    public answerQuestion = (): void => {
        if (this.answer.responses.length || this.answer.otherResponses.length) {
            this.answer.notSure = false;
            this.answer.notApplicable = false;
        }
    }

    public initialize = (): void => {
        this.options = cloneDeep(this.question.options);
        this.answer = {
            responses: [],
            otherResponses: [],
            notApplicable: false,
            notSure: false,
        };
        this.parseResponses();
    }

    public toggleNotApplicableAnswer = (): void => {
        this.answer.notApplicable = !this.answer.notApplicable;
        if (this.answer.notApplicable) {
            this.answer.notSure = false;
            this.resetOptions();
        }
    }

    public toggleNotSureAnswer = (): void => {
        this.answer.notSure = !this.answer.notSure;
        if (this.answer.notSure) {
            this.answer.notApplicable = false;
            this.resetOptions();
        }
    }

    private addOther = (otherResponse: string): void => {
        if (otherResponse && this.isCurrentOtherValue(otherResponse)) return;

        // If other response is same as the option value, then select the option instead adding new
        const currentOptionIndex: number = this.currentQuestionOptionIndex(otherResponse);
        const currentQuestionOption: IQuestionOptionDetails = this.options[currentOptionIndex];

        if (otherResponse && currentQuestionOption) {
            const option = find(this.answer.responses, (response: IQuestionAnswer) => response.answerId === currentQuestionOption.id);
            if (!option) {
                currentQuestionOption.isSelected = false;
                this.answerOption(currentQuestionOption, currentOptionIndex);
            }
        } else if (otherResponse) {
            this.answer.otherResponses.push({
                answer: otherResponse,
                answerType: QuestionResponseType.Others,
            });
        }
        this.answerQuestion();
    }

    private answerOption = (option: IQuestionOptionDetails, index: number): void => {
        // Adding the selected option
        if (!option.isSelected) {
            this.answer.responses.push({
                answerId: option.id,
                answer: option.option,
                answerType: QuestionResponseType.Default,
            });
        } else {
            // Removing the selection
            this.removeOptionFromAnswers(index);
        }
        option.isSelected = !option.isSelected;
        this.answerQuestion();
    }

    private currentQuestionOptionIndex = (value: string): number => {
        return findIndex(this.options, (option: IQuestionOptionDetails) => toLower(option.option) === toLower(value));
    }

    private isCurrentOtherValue = (value: string): boolean => {
        return some(this.answer.otherResponses, (response: IQuestionAnswer) => toLower(response.answer) === toLower(value));
    }

    private parseResponses = (): void => {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private removeOptionFromAnswers = (index: number): void => {
        if (isArray(this.answer.responses) && this.answer.responses.length) {
            this.answer.responses.splice(index, 1);
        }
    }

    private resetOptions = (): void => {
        forEach(this.options, (option: IQuestionOptionDetails): void => {
            option.isSelected = false;
        });
        this.answer.responses = [];
        this.answer.otherResponses = [];
        this.answerQuestion();
    }

    private updateOther = (otherResponse: string, index: number): void | undefined => {
        if (this.isCurrentOtherValue(otherResponse) && this.answer.otherResponses[index].answer !== otherResponse) {
            this.answer.otherResponses.splice(index, 1);
        }

        if (otherResponse) {
            const currentOptionIndex: number = this.currentQuestionOptionIndex(otherResponse);
            const currentQuestionOption: IQuestionOptionDetails = this.options[currentOptionIndex];
            if (currentQuestionOption) {
                const option = find(this.answer.responses, (response: IQuestionAnswer) => response.answerId === currentQuestionOption.id);
                if (!option) {
                    currentQuestionOption.isSelected = false;
                    this.answerOption(currentQuestionOption, currentOptionIndex);
                }
            } else {
                this.answer.otherResponses[index].answer = otherResponse;
            }
        }
        this.answerQuestion();
    }

}

export const MultiselectButtonQuestion = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: MultiselectButtonQuestionController,
    template: `
    <section class="multichoice-question">
        <div class="row-wrap">
            <div
                class="max-width-full row-space-between row-wrap flex"
            >
                <div
                    class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                    ng-if="$ctrl.options.length && option.option"
                    ng-repeat="option in $ctrl.options track by $index"
                >
                    <one-button
                        button-class="no-outline full-width"
                        button-click="$ctrl.answerOption(option,$index)"
                        class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                        identifier="questionMultichoiceOption{{$index}}"
                        is-disabled="$ctrl.readOnly"
                        is-selected="option.isSelected"
                        text="{{option.option}}"
                        tooltip-append-to-body="true"
                        tooltip-enable="$ctrl.question.attributeJson.optionHintEnabled"
                        tooltip-placement="top"
                        type="select"
                        uib-tooltip="{{option.attributeJson.hint}}"
                        wrap-text="true"
                    >
                    </one-button>
                </div>
                <div
                    class="max-width-full flex-full-50 multiselect-button__allowOther centered-50-percent margin-bottom-1"
                    ng-if="$ctrl.question.attributeJson.allowOther"
                    ng-repeat="otherResponse in $ctrl.answer.otherResponses track by $index"
                >
                    <one-editable-types
                        config="$ctrl.otherConfig"
                        class="full-size flex-full-width flex-full-50 padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                        handle-edit="$ctrl.updateOther(model, $index)"
                        is-disabled="$ctrl.readOnly"
                        model="otherResponse.answer"
                        show-cancel="true"
                    >
                    </one-editable-types>
                </div>
                <div
                    class="flex-full-50 multiselect-button__allowOther centered-50-percent margin-bottom-1"
                    ng-if="$ctrl.question.attributeJson.allowOther"
                >
                    <one-editable-types
                        config="$ctrl.newOtherConfig"
                        class="full-size flex-full-width flex-full-50 padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                        handle-edit="$ctrl.addOther(model)"
                        is-disabled="$ctrl.readOnly"
                        model="$ctrl.newOtherModel"
                        show-cancel="true"
                    >
                    </one-editable-types>
                </div>
                <div
                    class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                    ng-if="$ctrl.question.attributeJson.allowNotSure"
                >
                    <one-button
                        button-class="flex-full-25 no-outline full-width"
                        button-click="$ctrl.toggleNotSureAnswer()"
                        class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                        identifier="answerNotSure"
                        is-disabled="$ctrl.readOnly"
                        is-selected="$ctrl.answer.notSure"
                        text="{{::$ctrl.translations.notSure}}"
                        type="select"
                        wrap-text="true"
                    >
                    </one-button>
                </div>
                <div
                    class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                    ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                >
                    <one-button
                        button-class="flex-full-25 no-outline full-width"
                        button-click="$ctrl.toggleNotApplicableAnswer()"
                        class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                        identifier="answerNotApplicable"
                        is-disabled="$ctrl.readOnly"
                        is-selected="$ctrl.answer.notApplicable"
                        text="{{::$ctrl.translations.notApplicable}}"
                        type="select"
                        wrap-text="true"
                    >
                    </one-button>
                </div>
            </div>
        </div>
        <one-label-content
            is-required="$ctrl.question.attributeJson.requireJustification"
            name={{::$ctrl.translations.justifyYourAnswerBelow}}
            ng-if="$ctrl.question.attributeJson.allowJustification"
            wrapper-class="margin-top-2"
        >
            <one-textarea
                placeholder="{{::$ctrl.translations.enterJustificationHere}}"
            >
            </one-textarea>
        </one-label-content>
    </section>
        `,
};
