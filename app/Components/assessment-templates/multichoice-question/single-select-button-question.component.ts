import {
    find,
    isString,
    some,
    toLower,
} from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IEditableTypeConfig } from "interfaces/editable-type-config.interface";
import {
    IQuestionDetails,
    IQuestionOptionDetails,
    IQuestionAnswer,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

class SingleSelectButtonQuestionController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
    ];

    private answer: any = {};
    private otherConfig: IEditableTypeConfig;
    private question: IQuestionDetails;
    private translations: IStringMap<string>;

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            enterJustificationHere: this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() }),
            justifyYourAnswerBelow: this.$rootScope.t("JustifyYourAnswerBelow"),
            notSure: this.$rootScope.t("NotSure"),
            notApplicable: this.$rootScope.t("NotApplicable"),
        };
        this.otherConfig = {
            isEditable: true,
            isButton: true,
            inputType: 10,
        };
        this.initialize();
    }

    public initialize = (): void => {
        this.answer = {};
    }

    public addOther = (response: string): void => {
        if (some(this.question.options, (option: IQuestionOptionDetails) => toLower(option.option) === toLower(response))) {
            const option: IQuestionOptionDetails = find(this.question.options, (questionOption: IQuestionOptionDetails) => toLower(questionOption.option) === toLower(response));
            if (this.answer.response !== option.id) {
                this.toggleOption(option.id);
            }
        } else {
            this.answer = { otherResponse: response };
        }
    }

    public toggleNotApplicableAnswer = (): void => {
        this.answer.notApplicable = !this.answer.notApplicable;
        this.answer = this.answer.notApplicable ? { notApplicable: true } : {};
    }

    public toggleNotSureAnswer = (): void => {
        this.answer.notSure = !this.answer.notSure;
        this.answer = this.answer.notSure ? { notSure: true } : {};
    }

    public toggleOption(answer: string): void {
        this.answer = { response: (this.answer.response === answer) ? "" : answer };
    }

}

export const SingleSelectButtonQuestion: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: SingleSelectButtonQuestionController,
    template: `
            <section class="single-select-question">
                <div class="row-wrap">
                    <div
                        class="max-width-full row-space-between row-wrap flex"
                    >
                        <div
                            class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                            ng-if="$ctrl.question.options.length && option.option"
                            ng-repeat="option in $ctrl.question.options track by $index"
                        >
                            <one-button
                                button-class="no-outline full-width"
                                button-click="$ctrl.toggleOption(option.id)"
                                class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                                identifier="questionSingleSelectOption{{$index}}"
                                is-disabled="$ctrl.readOnly"
                                is-selected="$ctrl.answer.response === option.id"
                                text="{{option.option}}"
                                tooltip-append-to-body="true"
                                tooltip-enable="$ctrl.question.attributeJson.optionHintEnabled"
                                tooltip-placement="top"
                                type="select"
                                uib-tooltip="{{option.attributeJson.hint}}"
                                wrap-text="true"
                            >
                            </one-button>
                        </div>
                        <div
                            class="multiselect-button__allowOther flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                            ng-if="$ctrl.question.attributeJson.allowOther"
                        >
                            <one-editable-types
                                config="$ctrl.otherConfig"
                                class="full-size flex-full-width flex-full-50 padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                                handle-edit="$ctrl.addOther(model)"
                                is-disabled="$ctrl.readOnly"
                                model="$ctrl.answer.otherResponse"
                                show-cancel="true"
                            >
                            </one-editable-types>
                        </div>
                        <div
                            class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                            ng-if="$ctrl.question.attributeJson.allowNotSure"
                        >
                            <one-button
                                button-class="flex-full-25 no-outline full-width"
                                button-click="$ctrl.toggleNotSureAnswer()"
                                class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                                identifier="answerNotSure"
                                is-disabled="$ctrl.readOnly"
                                is-selected="$ctrl.answer.notSure"
                                text="{{::$ctrl.translations.notSure}}"
                                type="select"
                                wrap-text="true"
                            >
                            </one-button>
                        </div>
                        <div
                            class="flex-full-50 centered-50-percent margin-bottom-1 row-wrap flex"
                            ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                        >
                            <one-button
                                button-class="flex-full-25 no-outline full-width"
                                button-click="$ctrl.toggleNotApplicableAnswer()"
                                class="max-width-full flex-full-width padding-top-bottom-1 padding-right-1 padding-left-0 overflow-ellipsis-nowrap text-wrap"
                                identifier="answerNotApplicable"
                                is-disabled="$ctrl.readOnly"
                                is-selected="$ctrl.answer.notApplicable"

                                text="{{::$ctrl.translations.notApplicable}}"
                                type="select"
                                wrap-text="true"
                            >
                            </one-button>
                        </div>
                    </div>
                </div>
                <one-label-content
                    is-required="$ctrl.question.attributeJson.requireJustification"
                    name={{::$ctrl.translations.justifyYourAnswerBelow}}
                    ng-if="$ctrl.question.attributeJson.allowJustification"
                    wrapper-class="margin-top-2"
                >
                    <one-textarea
                        placeholder="{{::$ctrl.translations.enterJustificationHere}}"
                    >
                    </one-textarea>
                </one-label-content>
            </section>
        `,
};
