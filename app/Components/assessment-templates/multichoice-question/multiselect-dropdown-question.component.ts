import {
    cloneDeep,
    find,
    findIndex,
    forEach,
    includes,
    isArray,
    some,
    toLower,
    filter,
    map,
} from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IQuestionDetails,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

class MultiselectDropdownQuestionController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
    ];

    private answers: IQuestionOptionDetails[];
    private notApplicable: boolean;
    private notSure: boolean;
    private options: IQuestionOptionDetails[];
    private originalOptions: IQuestionOptionDetails[];
    private question: IQuestionDetails;
    private selections: string[];
    private translations: IStringMap<string>;

    constructor(
        private $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            add: this.$rootScope.t("Add"),
            enterJustificationHere: this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() }),
            justifyYourAnswerBelow: this.$rootScope.t("JustifyYourAnswerBelow"),
            notSure: this.$rootScope.t("NotSure"),
            notApplicable: this.$rootScope.t("NotApplicable"),
        };
        this.initialize();
    }

    public $onChanges(data: ng.IOnChangesObject): void {
        if (!data.question.isFirstChange()) {
            this.initialize();
        }
    }

    public removeOption(option: IQuestionOptionDetails): void {
        if (isArray(this.options) && this.options.length) {
            const optionIndex: number = findIndex(this.options, (opt: IQuestionOptionDetails): boolean => {
                return option.option === opt.option;
            });
            this.options.splice(optionIndex, 1);
            this.options = [...this.options];
        }
    }

    public removeAnswer(option: IQuestionOptionDetails): void {
        if (isArray(this.answers) && this.answers.length) {
            const optionIndex: number = this.answers.indexOf(option);
            this.answers.splice(optionIndex, 1);
            if (option.id) {
                this.options = filter(this.originalOptions, (originalOption: IQuestionOptionDetails): boolean => {
                    return some(this.options, (optionIterator: IQuestionOptionDetails) => optionIterator.id === originalOption.id)
                        || originalOption.id === option.id;
                });
            }
            this.removeSelection(option);
        }
    }

    public removeSelection(option: IQuestionOptionDetails): void {
        if (isArray(this.selections) && this.selections.length) {
            const optionIndex: number = findIndex(this.selections, (selection: any): boolean => {
                return option.id ? option.id === selection.id : option.option === selection.value;
            });
            this.selections.splice(optionIndex, 1);
        }
    }

    private addOption = (option: any): void => {
        if (option) {
            const existingOption: IQuestionOptionDetails | undefined = find(this.question.options, (questionOption: IQuestionOptionDetails): boolean => toLower(questionOption.option) === toLower(option.InputValue));
            const isExistingSelection: boolean = some(this.selections, (response: IQuestionOptionDetails): boolean => toLower(response.option) === toLower(option.InputValue));

            if ((option.IsPlaceholder && !option.InputValue) || isExistingSelection) {
                return;
            }
            if (option.id) {
                this.removeOption(option);
            } else if (existingOption) {
                option = existingOption;
                this.removeOption(option);
            } else {
                option = { option: option.InputValue };
            }

            if (!includes(this.answers, option) && !includes(this.selections, option)) {
                this.answers.push(option);
                this.selections.push(option);
                this.notSure = false;
                this.notApplicable = false;
            }
        }
    }

    private initialize = (): void => {
        this.answers = [];
        this.notApplicable = false;
        this.notSure = false;
        this.selections = [];
        if (this.question.attributeJson.optionHintEnabled) {
            this.originalOptions = map(this.question.options, (option: IQuestionOptionDetails): IQuestionOptionDetails => {
                return {
                    ...option,
                    Hint: option.attributeJson.hint,
                    tooltipHintEnabled: true,
                    attributeJson: null,
                };
            });
        } else {
            this.originalOptions = [...this.question.options];
        }
        this.options = [...this.originalOptions];
    }

    private removeAnswersForOptions(): void {
        const updatedOptions: IQuestionOptionDetails[] = [];
        if (isArray(this.answers) && this.answers.length) {
            forEach(this.answers, (selection: any): void => {
                if (selection.id) {
                    updatedOptions.push(selection);
                }
            });
            this.selections = [];
            this.answers = [];
            this.options = [...updatedOptions, ...this.options];
        }
    }

    private toggleNotApplicableAnswer = (): void => {
        this.notApplicable = !this.notApplicable;
        if (this.notApplicable) {
            this.notSure = false;
            this.removeAnswersForOptions();
        }
    }

    private toggleNotSureAnswer = (): void => {
        this.notSure = !this.notSure;
        if (this.notSure) {
            this.notApplicable = false;
            this.removeAnswersForOptions();
        }
    }
}

export const MultiselectDropdownQuestion: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: MultiselectDropdownQuestionController,
    template: `
        <section>
            <typeahead
                allow-empty="true"
                allow-other="$ctrl.question.attributeJson.allowOther"
                class="question-typeahead-multiselect__input"
                clear-on-select="true"
                disable-if-no-options="!$ctrl.question.attributeJson.allowOther"
                empty-text="- - - -"
                has-arrow="true"
                is-disabled="$ctrl.readOnly"
                label-key="option"
                on-select="$ctrl.addOption(selection)"
                options="$ctrl.options"
                order-by-label="false"
                other-option-prepend="Add"
                type="$ctrl.question.questionType"
                input-limit="2"
                use-virtual-scroll="true"
                options-limit="1000"
            >
            </typeahead>
            <pill-list
                list="$ctrl.selections",
                name-key="option",
                tooltip-key="Hint",
                handle-remove="$ctrl.removeAnswer(item)"
                is-disabled="$ctrl.readOnly"
            >
            </pill-list>
            <div class="row-wrap">
                <one-button
                    button-class="flex-full-25 no-outline full-width"
                    button-click="$ctrl.toggleNotSureAnswer()"
                    class="padding-top-bottom-1 padding-right-1 padding-left-0 max-25 flex-full-25"
                    identifier="answerNotSure"
                    is-disabled="$ctrl.readOnly"
                    is-selected="$ctrl.notSure"
                    ng-if="$ctrl.question.attributeJson.allowNotSure"
                    text="{{::$ctrl.translations.notSure}}"
                    type="select"
                    wrap-text="true"
                >
                </one-button>
                <one-button
                    button-class="flex-full-25 no-outline full-width"
                    button-click="$ctrl.toggleNotApplicableAnswer()"
                    class="padding-top-bottom-1 padding-right-1 padding-left-0 max-25 flex-full-25"
                    identifier="answerNotApplicable"
                    is-disabled="$ctrl.readOnly"
                    is-selected="$ctrl.notApplicable"
                    ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                    text="{{::$ctrl.translations.notApplicable}}"
                    type="select"
                    wrap-text="true"
                >
                </one-button>
            </div>
                <one-label-content
                    is-required="$ctrl.question.attributeJson.requireJustification"
                    name={{::$ctrl.translations.justifyYourAnswerBelow}}
                    ng-if="$ctrl.question.attributeJson.allowJustification"
                    wrapper-class="margin-top-2"
                >
                    <one-textarea
                        placeholder="{{::$ctrl.translations.enterJustificationHere}}"
                    >
                    </one-textarea>
                </one-label-content>
        </section>
`,
};
