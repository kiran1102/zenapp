// Rxjs
import { Subject } from "rxjs";
import { startWith, takeUntil } from "rxjs/operators";

// 3rd Party
import _, {
    find,
    toLower,
    isFunction,
} from "lodash";
import {
    getQuestionResponseModels,
} from "modules/assessment/reducers/assessment-detail.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Interfaces
import {
    IQuestionDetails,
    IQuestionOptionDetails,
} from "interfaces/assessment-template-question.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";
import { QuestionResponseType } from "constants/question-types.constant";

class SingleSelectDropdownQuestionController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "store",
    ];

    private answer: any = {};
    private options: IQuestionOptionDetails[];
    private onChange: (param: any) => any;
    private question: IQuestionDetails;
    private translations: IStringMap<string>;
    private destroy$ = new Subject();

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private store: IStore,
    ) { }

    public $onInit(): void {
        this.translations = {
            add: this.$rootScope.t("Add"),
            enterJustificationHere: this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() }),
            justifyYourAnswerBelow: this.$rootScope.t("JustifyYourAnswerBelow"),
            notSure: this.$rootScope.t("NotSure"),
            notApplicable: this.$rootScope.t("NotApplicable"),
        };

        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe(() => this.componentWillReceiveState());
    }

    public $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private addOption = (option: any): void => {
        if (option.id) {
            this.answer = { key: option.id, value: option.option };
        } else if (option.InputValue) {
            const existingOption: IQuestionOptionDetails | undefined = find(this.question.options, (questionOption: IQuestionOptionDetails) => toLower(questionOption.option) === toLower(option.InputValue));
            if (existingOption) {
                this.answer = { key: existingOption.id, value: existingOption.option };
            } else {
                this.answer = { value: option.InputValue };
            }
        } else {
            this.answer = {};
        }

        if (isFunction(this.onChange)) {
            this.onChange(this.answer);
        }
    }

    private componentWillReceiveState = (): void => {
        this.options = this.configureOptions();
        this.setResponses();
    }

    private setResponses = (): void => {
        const responseIds = this.question.responseIds;
        const responseModels = getQuestionResponseModels(this.question.id)(this.store.getState());
        for (const id of responseIds) {
            if (responseModels[id] && responseModels[id].type === QuestionResponseType.Default) {
                this.answer.value = responseModels[id].response;
            }
        }
    }

    private configureOptions = (): IQuestionOptionDetails[] => {
        return _(this.question.options)
            .filter((option: IQuestionOptionDetails) => option && option.optionType === QuestionResponseType.Default)
            .map((option: IQuestionOptionDetails) => {
                if (this.question && this.question.attributeJson && this.question.attributeJson.optionHintEnabled) {
                    option.Hint = option.attributeJson.hint;
                    option.tooltipHintEnabled = true;
                }
                return option;
            })
            .value();
    }

    private toggleNotApplicableAnswer = (): void => {
        this.answer.notApplicable = !this.answer.notApplicable;
        this.answer = this.answer.notApplicable ? { notApplicable: true } : {};
    }

    private toggleNotSureAnswer = (): void => {
        this.answer.notSure = !this.answer.notSure;
        this.answer = this.answer.notSure ? { notSure: true } : {};
    }
}

export const SingleSelectDropdownQuestion: ng.IComponentOptions = {
    bindings: {
        appendToBody: "<",
        question: "<",
        readOnly: "<",
        onChange: "&",
    },
    controller: SingleSelectDropdownQuestionController,
    template: `
            <section>
                <typeahead
                    append-to-body="$ctrl.appendToBody"
                    class="question-typeahead__input"
                    options="$ctrl.options"
                    model="$ctrl.answer.value"
                    on-select="$ctrl.addOption(selection)"
                    type="$ctrl.question.questionType"
                    label-key="option"
                    has-arrow="true"
                    is-disabled="$ctrl.readOnly"
                    allow-empty="true"
                    empty-text="- - - -"
                    order-by-label="false"
                    other-option-prepend="{{::$ctrl.translations.add}}"
                    allow-other="$ctrl.question.attributeJson.allowOther"
                    disable-if-no-options="!$ctrl.question.attributeJson.allowOther"
                    input-limit="2"
                    use-virtual-scroll="true"
                    options-limit="1000"
                >
                </typeahead>
                <div class="row-wrap">
                    <one-button
                        button-class="flex-full-25 no-outline full-width"
                        button-click="$ctrl.toggleNotSureAnswer()"
                        class="padding-top-bottom-1 padding-right-1 padding-left-0 max-25 flex-full-25"
                        identifier="answerNotSure"
                        is-disabled="$ctrl.readOnly"
                        is-selected="$ctrl.answer.notSure"
                        ng-if="$ctrl.question.attributeJson.allowNotSure"
                        text="{{::$ctrl.translations.notSure}}"
                        type="select"
                        wrap-text="true"
                    >
                    </one-button>
                    <one-button
                        button-class="flex-full-25 no-outline full-width"
                        button-click="$ctrl.toggleNotApplicableAnswer()"
                        class="padding-top-bottom-1 padding-right-1 padding-left-0 max-25 flex-full-25"
                        identifier="answerNotApplicable"
                        is-disabled="$ctrl.readOnly"
                        is-selected="$ctrl.answer.notApplicable"
                        ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                        text="{{::$ctrl.translations.notApplicable}}"
                        type="select"
                        wrap-text="true"
                    >
                    </one-button>
                </div>
                <one-label-content
                    is-required="$ctrl.question.attributeJson.requireJustification"
                    name={{::$ctrl.translations.justifyYourAnswerBelow}}
                    ng-if="$ctrl.question.attributeJson.allowJustification"
                    wrapper-class="margin-top-2"
                >
                    <one-textarea
                        placeholder="{{::$ctrl.translations.enterJustificationHere}}"
                    >
                    </one-textarea>
                </one-label-content>
            </section>
        `,
};
