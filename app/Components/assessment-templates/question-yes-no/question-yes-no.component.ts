import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

import * as ACTIONS from "../../Assessment/assessment-actions.constants";

class QuestionYesNoController {

    static $inject: string[] = ["$rootScope"];

    private answer: boolean | null = null;
    private isDisabled: boolean;
    private notApplicable: boolean;
    private notSure: boolean;
    private question: IQuestionDetails;
    private translations: IStringMap<string>;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            enterJustificationHere: this.$rootScope.t("EnterJustificationsHere", { Justifications: this.$rootScope.customTerms.Justification.toLowerCase() }),
            justifyYourAnswerBelow: this.$rootScope.t("JustifyYourAnswerBelow"),
            no: this.$rootScope.t("No"),
            notApplicable: this.$rootScope.t("NotApplicable"),
            notSure: this.$rootScope.t("NotSure"),
            yes: this.$rootScope.t("Yes"),
        };
    }

    public toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
            this.notSure = true;
        } else {
            this.notSure = false;
        }
    }

    public toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
            this.notApplicable = true;
        } else {
            this.notApplicable = false;
        }
    }

    public setYesNo(value: boolean): void {
        this.answer = value;
        this.notSure = false;
        this.notApplicable = false;
    }

    private sendAction(type: string, value?: boolean): void {
        switch (type) {
            case ACTIONS.YES_SELECTED:
            case ACTIONS.NO_SELECTED:
                this.setYesNo(value);
                break;
            case ACTIONS.NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case ACTIONS.NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
    }
}

export const QuestionYesNoComponent = {
    bindings: {
        question: "<",
        isDisabled: "<",
    },
    controller: QuestionYesNoController,
    template: `
        <one-button
            type="select"
            button-click="$ctrl.sendAction('YES_SELECTED', $ctrl.answer!==true?true:null)"
            button-class="margin-right-1 margin-bottom-1 no-outline"
            is-selected="$ctrl.answer"
            text="{{::$ctrl.translations.yes}}"
            is-disabled="$ctrl.isDisabled"
            identifier="yesNoQuestionYesBtn"
        ></one-button>
        <one-button
            type="select"
            button-click="$ctrl.sendAction('NO_SELECTED', $ctrl.answer!==false?false:null)"
            button-class="margin-right-1 margin-bottom-1 no-outline"
            is-selected="$ctrl.answer === false"
            text="{{::$ctrl.translations.no}}"
            is-disabled="$ctrl.isDisabled"
            identifier="yesNoQuestionNoBtn"
        ></one-button>
        <one-button
            type="select"
            button-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
            button-class="margin-right-1 margin-bottom-1 no-outline"
            is-selected="$ctrl.notSure"
            text="{{::$ctrl.translations.notSure}}"
            is-disabled="$ctrl.isDisabled"
            ng-if="$ctrl.question.attributeJson.allowNotSure"
            identifier="yesNoQuestionNotSureBtn"
        ></one-button>
        <one-button
            type="select"
            button-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
            button-class="margin-right-1 margin-bottom-1 no-outline"
            is-selected="$ctrl.notApplicable"
            text="{{::$ctrl.translations.notApplicable}}"
            is-disabled="$ctrl.isDisabled"
            ng-if="$ctrl.question.attributeJson.allowNotApplicable"
            identifier="yesNoQuestionNotApplicableBtn"
        ></one-button>
        <one-label-content
            is-required="$ctrl.question.attributeJson.requireJustification"
            name="{{::$ctrl.translations.justifyYourAnswerBelow}}"
            ng-if="$ctrl.question.attributeJson.allowJustification"
            wrapper-class="margin-top-2"
        >
            <one-textarea
                ng-if="$ctrl.question.attributeJson.allowJustification"
                placeholder="{{::$ctrl.translations.enterJustificationHere}}"
                input-changed="$ctrl.sendAction('JUSTIFICATION_UPDATED')"
                is-disabled="$ctrl.isDisabled"
            ></one-textarea>
        </one-label-content>
    `,
};
