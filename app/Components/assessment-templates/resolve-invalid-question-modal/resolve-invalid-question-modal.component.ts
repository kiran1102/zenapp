// 3rd Party
import { find } from "lodash";
import { buildDefaultModalTemplate } from "generalcomponent/Modals/modal-template/modal-template";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getCurrentTemplate } from "oneRedux/reducers/template.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import TemplateAction from "templateServices/actions/template-action.service";
import { TemplateBuilderService } from "templateServices/template-builder.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IAssessmentTemplateQuestion,
    IAssessmentTemplateAttribute,
    IQuestionDetails,
    IQuestionBuilderModalData,
    ICreateQuestionParams,
} from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IStore } from "interfaces/redux.interface";

class ResolveInvalidQuestionModalController implements ng.IComponentController {
    static $inject: string[] = [
        "ModalService",
        "$rootScope",
        "store",
        "TemplateAction",
        "TemplateBuilderService",
    ];

    private attributePlaceholderText: string;
    private content: string;
    private inventoryAttributes: IAssessmentTemplateAttribute[];
    private inventoryQuestions: IAssessmentTemplateQuestion[];
    private isAttributeDropdownDisabled: boolean;
    private isSubmitButtonDisabled = true;
    private modalData: IQuestionBuilderModalData;
    private question: IQuestionDetails = null;
    private saveInProgress = false;
    private selectedInventoryQuestionId: string;
    private translations: IStringMap<string>;

    constructor(
        private readonly modalService: ModalService,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly store: IStore,
        private readonly templateAction: TemplateAction,
        private readonly TemplateBuilder: TemplateBuilderService,
    ) { }

    public $onInit(): void {
        this.translations = {...this.TemplateBuilder.getTranslations(), modalTitle: this.$rootScope.t("ResolveQuestion")};
        this.modalData = getModalData(this.store.getState());
        this.selectedInventoryQuestionId = this.modalData.question.parentQuestionId;
        this.inventoryQuestions = this.TemplateBuilder.fillInventoryQuestionsDropdown(this.modalData.sectionId, this.modalData.nextQuestionId);

        if (this.selectedInventoryQuestionId) {
            this.onInventoryQuestionChange(this.selectedInventoryQuestionId);
        }
    }

    private onInventoryQuestionChange = (questionSelection: string): void => {
        this.isAttributeDropdownDisabled = true;
        this.inventoryAttributes = null;
        if (questionSelection) {
            this.attributePlaceholderText = this.translations.loading;
            this.templateAction.getAttributesByInventory(this.modalData.templateId, questionSelection)
                .then((res: IAssessmentTemplateAttribute[]): void => {
                    const { isAttributeDropdownDisabled, attributePlaceholderText } = this.TemplateBuilder.handleInventoryQuestionChangeAction(res);
                    this.inventoryAttributes = res;
                    this.isAttributeDropdownDisabled = isAttributeDropdownDisabled;
                    this.attributePlaceholderText = attributePlaceholderText;
                });
        }
    }

    private onAttributeChange = (attributeSelection: string): void => {
        this.isSubmitButtonDisabled = !attributeSelection;
        const attribute = find(this.inventoryAttributes, {id: attributeSelection});
        this.question = this.TemplateBuilder.buildQuestionForResolveInvalidQuestion(attribute, this.selectedInventoryQuestionId);
    }

    private onDeleteQuestion(): void {
        const modalData: any = {
            modalTitle: this.translations.deleteQuestion,
            confirmationText: this.translations.sureToDeleteThisQuestion,
            cancelButtonText: this.translations.cancel,
            submitButtonText: this.translations.confirm,
            promiseToResolve:
                (): ng.IPromise<boolean> => this.templateAction.deleteTemplateQuestion(getCurrentTemplate(this.store.getState()).id, this.modalData.sectionId, this.modalData.question.id)
                    .then((result: boolean): boolean => {
                        return result;
                    }),
        };
        this.modalService.closeModal();
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    private onResolveAttributeQuestion(): void {
        this.isSubmitButtonDisabled = this.saveInProgress = true;
        const contract: ICreateQuestionParams = {
            nextQuestionId: this.modalData.nextQuestionId || null,
            question: this.question,
        };

        this.templateAction.resolveInvalidQuestion(this.modalData.templateId, this.modalData.sectionId, this.modalData.question.id, contract)
            .then((response: boolean): void => {
                if (response) {
                    this.onInventoryQuestionChange(this.selectedInventoryQuestionId);
                    this.saveInProgress = false;
                    this.closeModal();
                } else {
                    this.isSubmitButtonDisabled = this.saveInProgress = false;
                }
            });
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}

const resolveInvalidQuestionModal: IStringMap<string> = {
    template: `
        <div class="overflow-y-auto">
            <div class="link-editor flex flex-full-height padding-all-2">
                <div class="row-horizontal-center margin-bottom-2">
                    <span class="icon-huge fa-stack fa-lg">
                        <i class="ot ot-exclamation-circle fa-2x color-orange"></i>
                    </span>
                </div>
                <div class="row-horizontal-center margin-bottom-2 padding-left-right-2">
                    {{::$ctrl.translations.resolveInvalidQuestionModalHelpText}}
                </div>
                <one-label-content
                    is-required="true"
                    name="{{::$ctrl.translations.whichInventoryQuestionFollowUpFor}}"
                    required-flag-before-label="true"
                    wrapper-class="margin-top-bottom-1 padding-left-right-2"
                    >
                    <one-select
                        is-required="true"
                        label-key="content"
                        model="$ctrl.selectedInventoryQuestionId"
                        on-change="$ctrl.onInventoryQuestionChange(selection)"
                        options="$ctrl.inventoryQuestions"
                        placeholder-text="{{$ctrl.translations.selectInventoryQuestion}}"
                        value-key="id"
                    ></one-select>
                </one-label-content>
                <one-label-content
                    is-required="true"
                    name="{{::$ctrl.translations.whichAttribute}}"
                    required-flag-before-label="true"
                    wrapper-class="margin-bottom-2 padding-left-right-2"
                    >
                    <one-select
                        is-disabled="$ctrl.isAttributeDropdownDisabled"
                        is-required="true"
                        label-key="name"
                        model=""
                        on-change="$ctrl.onAttributeChange(selection)"
                        options="$ctrl.inventoryAttributes"
                        placeholder-text="{{$ctrl.attributePlaceholderText}}"
                        translation-key="nameKey"
                        value-key="id"
                    ></one-select>
                </one-label-content>
                <footer class="row-flex-end static-horizontal padding-top-1">
                    <one-button
                        button-class="margin-right-1 no-outline"
                        button-click="$ctrl.closeModal()"
                        has-no-border="true"
                        identifier="TemplateResolveInvalidQuestionCancelButton"
                        text="{{::$ctrl.translations.cancel}}"
                    ></one-button>
                    <one-button
                        button-class="margin-right-1 no-outline"
                        button-click="$ctrl.onDeleteQuestion()"
                        identifier="TemplateResolveInvalidQuestionDeleteButton"
                        is-rounded="true"
                        text="{{::$ctrl.translations.deleteQuestion}}"
                    ></one-button>
                    <one-button
                        button-class="margin-right-1 no-outline"
                        button-click="$ctrl.onResolveAttributeQuestion()"
                        enable-loading="true"
                        identifier="TemplateResolveInvalidQuestionResolveButton"
                        is-disabled="$ctrl.isSubmitButtonDisabled"
                        is-rounded="true"
                        is-loading="$ctrl.saveInProgress"
                        text="{{::$ctrl.translations.resolve}}"
                        type="primary"
                    ></one-button>
                </footer>
            </div>
        </div>`,
};
export const ResolveInvalidQuestionModal: ng.IComponentOptions = {
    controller: ResolveInvalidQuestionModalController,
    template: buildDefaultModalTemplate(resolveInvalidQuestionModal.template),
};
