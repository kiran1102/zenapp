import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

import * as ACTIONS from "../../Assessment/assessment-actions.constants";

class QuestionDateController {

    static $inject: string[] = ["$rootScope"];

    private answer: Date;
    private isDisabled: boolean;
    private notApplicable: boolean;
    private notSure: boolean;
    private question: IQuestionDetails;
    private translations: IStringMap<string>;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            chooseDate: this.$rootScope.t("ChooseDate"),
            notApplicable: this.$rootScope.t("NotApplicable"),
            notSure: this.$rootScope.t("NotSure"),
        };
    }

    public toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
            this.notSure = true;
        } else {
            this.notSure = false;
        }
    }

    public toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
            this.notApplicable = true;
        } else {
            this.notApplicable = false;
        }
    }

    public setDate(value: Date): void {
        this.answer = value;
        this.notSure = false;
        this.notApplicable = false;
    }

    public sendAction(type: string, value?: Date): void {
        switch (type) {
            case ACTIONS.DATE_SELECTED:
                this.setDate(value);
                break;
            case ACTIONS.NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case ACTIONS.NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
    }
}

export const QuestionDateComponent = {
    bindings: {
        isDisabled: "<?",
        question: "<",
    },
    controller: QuestionDateController,
    template: `
        <div class="padding-all-1">
            <downgrade-one-date-time-picker
                [disabled]="$ctrl.isDisabled"
                [date-disable-until]="$ctrl.config.minDate"
                [date-placeholder]="$ctrl.translations.chooseDate"
                [date-time-model]="$ctrl.answer"
                (on-change)="$ctrl.sendAction('DATE_SELECTED', $event.jsdate)"
            ></downgrade-one-date-time-picker>
        </div>
        <div class="padding-all-1">
            <one-button
                type="select"
                button-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                button-class="margin-right-1 margin-bottom-1 no-outline"
                is-selected="$ctrl.notSure"
                text="{{::$ctrl.translations.notSure}}"
                is-disabled="$ctrl.isDisabled"
                ng-if="$ctrl.question.attributeJson.allowNotSure"
                identifier="dateQuestionNotSureBtn"
            ></one-button>
            <one-button
                type="select"
                button-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                button-class="margin-right-1 margin-bottom-1 no-outline"
                is-selected="$ctrl.notApplicable"
                text="{{::$ctrl.translations.notApplicable}}"
                is-disabled="$ctrl.isDisabled"
                ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                identifier="dateQuestionNotApplicableBtn"
            ></one-button>
        </div>
    `,
};
