import { isUndefined } from "lodash";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAssessmentQuestionAction } from "interfaces/question.interface";
import { IQuestionDetails } from "interfaces/assessment-template-question.interface";
import { IStringMap } from "interfaces/generic.interface";

import * as ACTIONS from "../../Assessment/assessment-actions.constants";

class QuestionTextboxController {

    static $inject: string[] = ["$rootScope", "Permissions"];

    private answer: string;
    private isDisabled: boolean;
    private notApplicable: boolean;
    private notSure: boolean;
    private onUpdate: Function;
    private question: IQuestionDetails;

    private translations: IStringMap<string>;

    constructor(
        public readonly $rootScope: IExtendedRootScopeService,
    ) { }

    public $onInit(): void {
        this.translations = {
            notApplicable: this.$rootScope.t("NotApplicable"),
            notSure: this.$rootScope.t("NotSure"),
            typeTextResponse: this.$rootScope.t("TypeTextResponse"),
        };
    }

    public toggleNotSure(): void {
        if (!this.notSure) {
            this.answer = null;
            this.notApplicable = false;
            this.notSure = true;
        } else {
            this.notSure = false;
        }
    }

    public toggleNotApplicable(): void {
        if (!this.notApplicable) {
            this.answer = null;
            this.notSure = false;
            this.notApplicable = true;
        } else {
            this.notApplicable = false;
        }
    }

    public setTextbox(value: string): void {
        this.answer = value;
        this.notSure = false;
        this.notApplicable = false;
    }

    private sendAction(type: string, value?: string): void {
        const actionType: IAssessmentQuestionAction = { type };
        if (type === ACTIONS.TEXTBOX_UPDATED) {
            actionType.value = isUndefined(value) ? "" : value;
        }
        switch (type) {
            case ACTIONS.TEXTBOX_UPDATED:
                this.setTextbox(value);
                break;
            case ACTIONS.NOT_SURE_SELECTED:
                this.toggleNotSure();
                break;
            case ACTIONS.NOT_APPLICABLE_SELECTED:
                this.toggleNotApplicable();
                break;
        }
        this.onUpdate({ actionType });
    }
}

export const QuestionTextBoxComponent = {
    bindings: {
        isDisabled: "<?",
        question: "<",
        onUpdate: "&",
    },
    controller: QuestionTextboxController,
    template: `
        <textarea
            class="one-textarea full-width resize-none"
            ng-change="$ctrl.sendAction('TEXTBOX_UPDATED', $ctrl.answer)"
            ng-if="!$ctrl.isDisabled"
            ng-model="$ctrl.answer"
            placeholder="{{::$ctrl.translations.typeTextResponse}}"
            rows="3"
        ></textarea>
        <div
            class="question__content question__content--readonly padding-all-1"
            ng-if="$ctrl.isDisabled"
        >
            {{::$ctrl.answer }}
        </div>
        <div class="padding-top-bottom-1">
            <one-button
                type="select"
                button-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                button-class="margin-right-1 margin-bottom-1 no-outline"
                is-selected="$ctrl.notSure"
                text="{{::$ctrl.translations.notSure}}"
                is-disabled="$ctrl.isDisabled"
                ng-if="$ctrl.question.attributeJson.allowNotSure"
                identifier="textboxQuestionNotSureBtn"
            ></one-button>
            <one-button
                type="select"
                button-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                button-class="margin-right-1 margin-bottom-1 no-outline"
                is-selected="$ctrl.notApplicable"
                text="{{::$ctrl.translations.notApplicable}}"
                is-disabled="$ctrl.isDisabled"
                ng-if="$ctrl.question.attributeJson.allowNotApplicable"
                identifier="textboxQuestionNotApplicableBtn"
            ></one-button>
        </div>
    `,
};
