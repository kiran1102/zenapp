import { RiskActionService } from "oneServices/actions/risk-action.service";
import _ from "lodash";

class RiskActionButtons {

    static $inject: string[] = ["RiskAction"];

    private onOpenRiskModal: (arg: any) => any;
    private onOpenDeleteModal: (arg: any) => any;
    private onRiskDropdownToggle: (arg: any) => any;
    private onRiskLevelChange: (arg: any) => any;
    private onRiskTileChange: (arg: any) => any;
    private isDropdownOpen: boolean;
    private heatmapEnabled: boolean;
    private enableLevelChange: boolean;
    private enableRiskEdit: boolean;
    private enableRiskDelete: boolean;

    constructor(
        private readonly store: any,
        private readonly RiskAction: RiskActionService,
    ) {}

    private handleDropdownToggle(isOpen: boolean): void {
        this.isDropdownOpen = isOpen;
        if (_.isFunction(this.onRiskDropdownToggle)) {
            this.onRiskDropdownToggle({ value: isOpen });
        }
    }

    private handleRiskLevelChange(value: number): void {
        if (_.isFunction(this.onRiskLevelChange)) {
            this.onRiskLevelChange({ value });
        }
    }

    private handleTileClick(tile: any): void {
        if (_.isFunction(this.onRiskTileChange)) {
            this.onRiskTileChange({ value: tile });
        }
    }
}

const OneRiskActionButtonsComponent = {
    controller: RiskActionButtons,
    bindings: {
        onOpenRiskModal: "&?",
        onOpenDeleteModal: "&?",
        onRiskLevelChange: "&?",
        onRiskDropdownToggle: "&?",
        onRiskTileChange: "&?",
        heatmapEnabled: "<?",
        axisLabels: "<?",
        tileMap: "<?",
        enableLevelChange: "<?",
        enableRiskEdit: "<?",
        enableRiskDelete: "<?",
        alignRight: "<?",
    },
    template: `
        <span>
            <span
                uib-dropdown
                auto-close="always outsideClick"
                on-toggle="$ctrl.handleDropdownToggle(open)"
                class="margin-right-2"
            >
                <i
                    ng-if="$ctrl.enableLevelChange"
                    uib-dropdown-toggle
                    class="remove-anchor-style fa fa-flag text-pointer"
                ></i>
                <one-risk-level-dropdown
                    ng-if="$ctrl.enableLevelChange && !$ctrl.heatmapEnabled"
                    ng-class="{ 'open': $ctrl.isDropdownOpen }"
                    on-risk-level-change="$ctrl.handleRiskLevelChange(value)"
                    align-right="$ctrl.alignRight"
                >
                </one-risk-level-dropdown>
                <one-risk-tile-dropdown
                    ng-if="$ctrl.enableLevelChange && $ctrl.heatmapEnabled"
                    ng-class="{ 'open': $ctrl.isDropdownOpen }"
                    on-tile-select="$ctrl.handleTileClick(value)"
                    axis-labels="$ctrl.axisLabels"
                    tile-map="$ctrl.tileMap"
                    align-right="$ctrl.alignRight"
                >
                </one-risk-tile-dropdown>
            </span>
            <i
                ng-if="$ctrl.enableRiskEdit"
                ng-click="$ctrl.onOpenRiskModal()"
                class="remove-anchor-style fa fa-pencil-square-o text-pointer margin-right-2"
            ></i>
            <i
                ng-if="$ctrl.enableRiskDelete"
                ng-click="$ctrl.onOpenDeleteModal()"
                class="remove-anchor-style fa fa-trash-o text-pointer"
            ></i>
        </span>
    `,
};

export default OneRiskActionButtonsComponent;
