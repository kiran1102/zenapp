import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// This Component receives either an array of strings/objects of any length.
// For object arrays, you will need to provide a key for the value you want to extract.
// For string arrays, just provide the array.
//
// Returns: A grammatically correct list of strings separated by commas or 'and' when needed.
// User may also provide strings to prepend and append around the comma separated list.
//
// ex:
// ['Hello', 'World', 'Sunshine'] outputs to ==> 'Hello, World, and Sunshine'

class CommaSeparatedListController {

    static $inject: any[] = ["$rootScope"];
    private andTranslated: string;

    constructor($rootScope: IExtendedRootScopeService) {
        this.andTranslated = $rootScope.t("And").toString().toLowerCase();
    }

    private formatListToString(list: any[], key: string): string {
        if (list.length === 1) {
            return key ? list[0][key] : list[0];
        } else if (list && list.length === 2) {
            return key ? `${list[0][key]} ${this.andTranslated} ${list[1][key]}` : `${list[0]} ${this.andTranslated} ${list[1]}`;
        }
        return this.formatLongListToString(list, key);
    }

    private formatLongListToString(list: any[], key: string): string {
        const length: number = list.length;
        let finalString = "";
        _.each(list, (item: any, i: number): void => {
            if (i === (length - 1)) {
                finalString += key ? `${this.andTranslated} ${item[key]}` : `${this.andTranslated} ${item}`;
            } else {
                finalString += key ? `${item[key]}, ` : `${item}, `;
            }
        });
        return finalString;
    }
}

const CommaSeparatedListTemplate = `
    <p
        class="{{$ctrl.className}}">
        <span data-ng-if="$ctrl.prependText">
            {{::$ctrl.prependText}}
        </span>
        <span data-ng-if="$ctrl.list">
            {{::$ctrl.formatListToString($ctrl.list, $ctrl.listKey)}}
        </span>
        <span data-ng-if="$ctrl.appendText">
            {{::$ctrl.appendText}}
        </span>
    </p>
`;

const CommaSeparatedListComponent: any = {
    controller: CommaSeparatedListController,
    template: CommaSeparatedListTemplate,
    bindings: {
        className: "<",
        list: "<",
        listKey: "<?",
        prependText: "<?",
        appendText: "<?",
    },
};

export default CommaSeparatedListComponent;
