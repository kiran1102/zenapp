export const OnePopupFooterComponent: ng.IComponentOptions = {
    bindings: {
        visible: "<",
        footerClass: "@?",
        fullCenter: "<?",
        footerMainClass: "@?",
    },
    template: `
        <div class="one-popup-footer position-relative {{$ctrl.footerClass}}">
            <div
                class="one-popup-footer__main row-vertical-center position-absolute shadow2 full-width {{$ctrl.footerMainClass}}"
                ng-class="{'one-popup-footer__main--visible': $ctrl.visible}"
                >
                <div class="one-popup-footer__content row-nowrap full-width">
                    <div ng-if="!$ctrl.fullCenter" class="one-popup-footer__left-actions row-flex-start flex-grow-1 margin-left-1" ng-transclude="leftActions"></div>
                    <div class="one-popup-footer__center-actions flex-grow-1 margin-left-right-1" ng-transclude="centerActions"></div>
                    <div ng-if="!$ctrl.fullCenter" class="one-popup-footer__right-actions row-flex-end margin-right-1" ng-transclude="rightActions"></div>
                </div>
            </div>
        </div>
    `,
    transclude: {
        leftActions: "?leftActions",
        centerActions: "?centerActions",
        rightActions: "?rightActions",
    },
};
