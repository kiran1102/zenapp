import { CheckboxValues } from "enums/checkbox-values.enum";
import * as _ from "lodash";

class OneCheckboxController implements ng.IComponentController {
    static $inject: string[] = ["ENUMS"];

    private checkboxTypes: any;
    private toggle: any;
    private isSelected: boolean;
    private isDisabled: boolean;
    private selectType = 0;
    private type: string;
    private identifier: string;

    constructor(ENUMS: any) {
        this.checkboxTypes = ENUMS.CheckboxTypes;
        if (_.isUndefined(this.isSelected)) this.isSelected = false;
        if (_.isUndefined(this.isDisabled)) this.isDisabled = false;
    }

    private GetSelectType(): string {
        switch (this.selectType) {
            case CheckboxValues.Check:
                return "ot ot-check";
            case CheckboxValues.Cross:
                return "ot ot-times";
            case CheckboxValues.Minus:
                return "fa fa-minus";
            case CheckboxValues.Question:
                return "fa fa-question";
            default:
                return "ot ot-check";
        }
    }
}

const OneCheckboxTemplate = `
    <div class="row-nowrap">
        <button class="one-checkbox"
            type="button"
            ng-click="$ctrl.toggle()"
            ng-disabled="$ctrl.isDisabled"
            data-id="{{::$ctrl.identifier}}"
            ng-class="{
                'selected': $ctrl.isSelected,
                'one-checkbox--primary': $ctrl.type === $ctrl.checkboxTypes.Primary,
                'one-checkbox--danger': $ctrl.type === $ctrl.checkboxTypes.Danger,
            }"
            >
            <i class="one-checkbox__icon {{ $ctrl.GetSelectType() }}" ></i>
        </button>
        <label
            ng-if="::$ctrl.text"
            class="padding-left-1"
            ng-bind="::$ctrl.text"
        >
        </label>
    </div>
`;

const OneCheckboxComponent: ng.IComponentOptions = {
    controller: OneCheckboxController,
    template: OneCheckboxTemplate,
    bindings: {
        toggle: "&",
        isSelected: "<",
        isDisabled: "<?",
        selectType: "<?",
        type: "<?",
        identifier: "@",
        text: "@?",
    },
};

export default OneCheckboxComponent;
