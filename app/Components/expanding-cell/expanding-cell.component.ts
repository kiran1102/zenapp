import Utilities from "Utilities";
import $ from "jquery";
import JQuery from "jquery";

class ExpandingCellCtrl implements ng.IComponentController {
    static $inject: string[] = ["$timeout"];

    private content: string;
    private uuid: string;
    private max: number;

    constructor(private readonly $timeout: ng.ITimeoutService) {
        this.uuid = Utilities.uuid();
    }

    $onInit(): void {
        this.$timeout( (): void => {
            const containerEl: JQuery = $(`#cell-${this.uuid}`);
            const elFullWidth: number = containerEl.width();
            if (elFullWidth > this.max) containerEl.css({"width": this.max, "white-space": "normal", "display": "block"});
        }, 0);
    }
}

export default  {
    controller: ExpandingCellCtrl,
    bindings: {
        max: "<",
        content: "@",
    },
    template: `
		<span
            id="{{::'cell-' + $ctrl.uuid}}"
            class="text-nowrap"
            ng-bind="$ctrl.content"
            >
		</span>
	`,
};
