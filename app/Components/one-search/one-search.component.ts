
class OneSearchController {

    model: any;
    onSearch: any;
    placeholder: any;
    throttle: any;

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.model.currentValue !== changes.model.previousValue) this.handleSearchAction();
    }

    public handleSearchAction(): void {
        this.onSearch({ model: this.model });
    }
}

const oneSearchComponent: any = {
    bindings: {
        model: "<",
        onSearch: "&",
        placeholder: "@?",
        throttle: "<?",
        wrapperClass: "@",
        isRounded: "<?",
    },
    controller: OneSearchController,
    template: () => `
            <form
                class="one-search overflow-hidden {{$ctrl.wrapperClass}}"
                ng-class="{'one-search--rounded': $ctrl.isRounded}"
                >
                <label class="one-search__label">
                    <i class="one-search__icon ot ot-search"></i>
                </label>
                <input
                    class="one-search__input"
                    placeholder="{{::$ctrl.placeholder}}"
                    ng-model="$ctrl.model"
                    ng-change="$ctrl.handleSearchAction()"
                    ng-model-options="{debounce: $ctrl.throttle || 0}"
                />
            </form>
        `,
};

export default oneSearchComponent;
