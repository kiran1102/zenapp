export const OneLabelContentComponent: ng.IComponentOptions = {
    bindings: {
        name: "@?",
        description: "@?",
        descriptionDelay: "<?",
        hideDescriptionStyle: "<?",
        hideName: "<?",
        labelClass: "@?",
        labelStyle: "<?",
        bodyClass: "@?",
        isRequired: "<?",
        requiredFlagBeforeLabel: "<?",
        inline: "<?",
        split: "<?",
        tooltipPosition: "@?",
        icon: "@",
        iconClass: "@?",
        wrapperClass: "@?",
        legendColor: "@?",
        nameDescription: "@?",
        labelName: "@?",
    },
    transclude: true,
    template: `
        <div
            class="one-label-content {{$ctrl.wrapperClass}}"
            ng-class="{
                'one-label-content--inline align-center': $ctrl.inline,
                'one-label-content--inline-split': $ctrl.inline && $ctrl.split,
            }"
        >
            <div
                class="text-bold one-label-content__text row-vertical-center {{ ::$ctrl.labelClass }}"
                ng-style="$ctrl.labelStyle || {}"
                ng-if="::$ctrl.name || $ctrl.hideName || $ctrl.labelName"
                >
                <span
                ng-if="::$ctrl.legendColor"
                class="one-label-content__legend inline-block margin-right-1"
                ng-class="{
                    'one-label-content__legend--green': $ctrl.legendColor === 'map-accent-green',
                    'one-label-content__legend--blue': $ctrl.legendColor === 'map-accent-blue',
                    'one-label-content__legend--purple': $ctrl.legendColor === 'map-accent-purple',
                    'one-label-content__legend--pink': $ctrl.legendColor === 'map-accent-pink',
                }"
                    >
                </span>
                <abbr
                    ng-if="$ctrl.isRequired && $ctrl.requiredFlagBeforeLabel"
                    class="slds-required margin-right-half"
                    >*
                </abbr>
                <div class="column-nowrap min-width-1">
                    <div
                        ng-class="{ 'border-bottom-dashed': !$ctrl.hideDescriptionStyle && $ctrl.description && !$ctrl.icon }"
                        uib-tooltip="{{ $ctrl.description }}"
                        tooltip-placement="{{$ctrl.tooltipPosition || 'top-right'}}"
                        tooltip-append-to-body="true"
                        tooltip-popup-delay="{{::$ctrl.descriptionDelay}}"
                        tooltip-enable="::!$ctrl.icon && $ctrl.description"
                        >
                        {{$ctrl.name ? $ctrl.name : $ctrl.labelName}}
                        <sup
                            ng-if="$ctrl.isRequired && !$ctrl.requiredFlagBeforeLabel"
                            class="helper-icon-required one-label-content__required"
                        ></sup>
                    </div>
                    <p
                        ng-if="::$ctrl.nameDescription"
                        class="text-small text-thin"
                        ng-bind="$ctrl.nameDescription"
                        >
                    </p>
                </div>
                <i
                    ng-if="::$ctrl.icon"
                    ng-class="[$ctrl.icon, $ctrl.iconClass, {'invisible': $ctrl.isLoading}]"
                    uib-tooltip="{{ ::$ctrl.description }}"
                    tooltip-placement="top-right"
                    tooltip-append-to-body="true"
                    tooltip-trigger="{{{true: 'mouseenter', false: 'never'}[$ctrl.description]}}"
                    >
                </i>
            </div>
            <ng-transclude class="one-label-content__body {{ ::$ctrl.bodyClass }}"></ng-transclude>
        </div>
    `,
};
