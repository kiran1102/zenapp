
class SectionNavigatorToggle {

    static $inject: string[] = [
        "$scope",
        "$timeout",
        "ENUMS",
    ];

    private scope: any;
    private timeout: any;
    private sectionStates: any;
    private transitionTimes: any;
    private currentSection: any;
    private nextSection: any;
    private previousSection: any;
    private fadeOut: boolean;

    constructor($scope: any, $timeout: ng.ITimeoutService, ENUMS: any) {
        this.scope = $scope;
        this.timeout = $timeout;
        this.sectionStates = ENUMS.SectionStates;
        this.transitionTimes = ENUMS.TransitionTimes;
    }

    $onChanges() {
        this.fadeNavigator();
    }

    public goToSection(sectionId: any): void {
        this.fadeNavigator();
        this.scope.$emit("GO_TO_SECTION", sectionId);
    }

    private fadeNavigator() {
        this.fadeOut = true;
        this.timeout(() => {
            this.fadeOut = false;
        }, this.transitionTimes.SectionNavFade);
    }

}

const sectionNavigatorToggle = {
    bindings: {
        currentSection: "<",
        nextSection: "<",
        previousSection: "<",
    },
    controller: SectionNavigatorToggle,
    template: () => {
        return `<ul class="section-navigator-toggle" ng-class="{'fade-out': $ctrl.fadeOut}">
                    <li class="section-navigator-toggle__item">
                        <div class="section-navigator-toggle__content" ng-class="{'is-hidden': !$ctrl.previousSection}">
                            <button
                                class="section-navigator-toggle__btn"
                                ng-click="$ctrl.goToSection($ctrl.previousSection.Id)"
                                ng-disabled="$ctrl.previousSection.Status === $ctrl.sectionStates.NotStarted">
                                    <i class="section-navigator-toggle__name-icon section-navigator-toggle__name-icon--left fa fa-chevron-left"></i>
                                    <span class="section-navigator-toggle__name"> {{ $ctrl.previousSection.Name }}</span>
                            </button>
                        </div>
                    </li>
                    <li class="section-navigator-toggle__item">
                        <span class="section-navigator-toggle__name"> {{ $ctrl.currentSection.Name }} </span>
                    </li>
                    <li class="section-navigator-toggle__item">
                        <div class="section-navigator-toggle__content" ng-class="{'is-hidden': !$ctrl.nextSection}">
                            <button
                                class="section-navigator-toggle__btn"
                                ng-click="$ctrl.goToSection($ctrl.nextSection.Id)"
                                ng-disabled="$ctrl.nextSection.Status === $ctrl.sectionStates.NotStarted">
                                    <span class="section-navigator-toggle__name"> {{ $ctrl.nextSection.Name }}</span>
                                    <i class="section-navigator-toggle__name-icon section-navigator-toggle__name-icon--right fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </li>
                </ul>`;
    },
};

export default sectionNavigatorToggle;
