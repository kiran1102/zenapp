// 3rd Party
import { isUndefined } from "lodash";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";

class DropdownController implements ng.IComponentController {

    private text: string;
    private icon: string;
    private hideCaret: boolean;
    private dropdownClass: string;
    private listClass: string;
    private alignRight: boolean;
    private itemClass: string;
    private options: IDropdownOption[];
    private textUpdate: boolean;
    private defaultIndex: number;
    private roundButton: boolean;
    private isLarge: boolean;
    private flatButton: boolean;
    private getOptions: any;
    private onToggle: any;
    private appendToBody = false;

    public $onInit(): void {
        if (isUndefined(this.isLarge)) this.isLarge = false;
    }

    public $onChanges(): void {
        if (this.defaultIndex && this.defaultIndex < this.options.length) {
            this.optionSelected(this.options[this.defaultIndex]);
        }
    }

    private optionSelected(option: any): void {
        if (this.textUpdate) {
            this.text = option.text;
        }
        option.action(option);
    }

    private toggled(isOpen: boolean): void {
        if (isOpen) this.options = this.getOptions ? this.getOptions() : this.options;
        if (this.onToggle) this.onToggle({isOpen});
    }
}

export const DropdownComponent: ng.IComponentOptions = {
    bindings: {
        enableBorder: "<?",
        text: "@?",
        icon: "@?",
        hideCaret: "<?",
        dropdownClass: "@?",
        buttonClass: "@?",
        listClass: "@?",
        alignRight: "<?",
        itemClass: "@?",
        options: "<?",
        textUpdate: "<?",
        defaultIndex: "<?",
        roundButton: "<?",
        getOptions: "&?",
        iconClass: "@?",
        flatButton: "<?",
        onToggle: "&?",
        isLoading: "<?",
        isDisabled: "<?",
        isLarge: "<?",
        appendToBody: "<?",
        identifier: "@?",
    },
    controller: DropdownController,
    template: `
        <span
            class="one-dropdown {{ ::$ctrl.dropdownClass }}"
            uib-dropdown
            keyboard-nav
            ot-auto-id="{{$ctrl.identifier+'Button'}}"
            auto-close="always outsideClick"
            on-toggle="$ctrl.toggled(open)"
            ng-disabled="$ctrl.isDisabled"
            dropdown-append-to-body="$ctrl.appendToBody"
            >
            <button
                class="one-button one-dropdown__button {{ ::$ctrl.buttonClass }}"
                uib-dropdown-toggle
                ng-class="{
                    'one-dropdown__button--round': $ctrl.roundButton && !$ctrl.isLarge,
                    'one-dropdown__button--round--large': $ctrl.roundButton && $ctrl.isLarge,
                    'one-dropdown__button--flat': $ctrl.flatButton,
                }"
                >
                <i class="media-middle {{::($ctrl.icon)}}" ng-if="$ctrl.icon"></i>
                <span
                    ng-if="$ctrl.isLoading"
                    class="one-button__loading fa fa-spinner fa-pulse fa-fw">
                </span>
                <span
                    class="one-dropdown__button-text"
                    ng-if="$ctrl.text"
                    ng-class="{'invisible': $ctrl.isLoading}"
                    >
                    {{$ctrl.text}}
                </span>
                <span
                    class="one-dropdown__button-icon {{ ::$ctrl.iconClass }} ot ot-caret-down"
                    ng-if="::!$ctrl.hideCaret"
                    ng-class="{'invisible': $ctrl.isLoading}"
                    >
                </span>
            </button>
            <ul
                uib-dropdown-menu
                role="menu"
                class="one-dropdown__list {{ ::$ctrl.listClass }} text-left shadow shadow-transition"
                ng-class="{'invisible': $ctrl.isLoading, 'dropdown-menu-right': $ctrl.alignRight }"
                ng-if="$ctrl.options && $ctrl.options.length"
                >
                <li
                    class="one-dropdown__item-wrapper"
                    ng-repeat="(index, option) in $ctrl.options track by $index"
                    uib-tooltip="{{:: option.toolTip }}"
                    tooltip-placement="{{:: option.toolTipDirection }}"
                    tooltip-append-to-body="true"
                    >
                    <a ng-if="option.action"
                        class="one-dropdown__item {{ ::$ctrl.itemClass }}"
                        ot-auto-id="{{option.identifier || $ctrl.identifier+'Action_'+($index+1)}}"
                        ng-class="{'one-dropdown__item--disabled': option.isDisabled, 'text-bold': option.bolderOption, 'border-bottom-3': $ctrl.enableBorder && $ctrl.options.length > 1}"
                        ng-click="$ctrl.optionSelected(option)"
                        >
                        <i ng-if="option.icon" class="{{ ::option.icon }}"></i>
                        <span ng-if="option.text || option.textKey">{{(option.text || $root.t(option.textKey) || "")}}</span>
                    </a>
                    <a ng-if="option.route"
                        class="one-dropdown__item {{ ::$ctrl.itemClass }}"
                        ot-auto-id="{{option.identifier || $ctrl.identifier+'Route_'+($index+1)}}"
                        ng-class="{'one-dropdown__item--disabled': option.isDisabled}"
                        ui-sref="{{::option.route}}"
                        >
                        <i ng-if="option.icon" class="{{ ::option.icon }}"></i>
                        <span ng-if="option.text || option.textKey">{{(option.text || $root.t(option.textKey) || "")}}</span>
                    </a>
                </li>
            </ul>
        </span>
    `,
};
