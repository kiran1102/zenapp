declare var angular: any;
import {
    isUndefined,
    orderBy,
    isNull,
    filter,
    toLower,
    isFunction,
} from "lodash";
import Utilities from "Utilities";
import { KeyCodes } from "enums/key-codes.enum";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

class Typeahead implements ng.IComponentController {

    static $inject: string[] = [
        "$scope",
        "$timeout",
        "$rootScope",
        "$filter",
        "$templateCache",
        "$element",
    ];

    private options: any[];
    private allOptions: any[];
    private model: any;
    private previousModel: any;
    private onSelect: any;
    private labelKey: string;
    private translationKey: string;
    private hasArrow: boolean;
    private loadOnClick: boolean;
    private clearOnSelect: boolean;
    private onBlur: any;
    private onFocus: any;
    private onChange: any;
    private showingOptions: boolean;
    private disableIfNoOptions: boolean;
    private isDisabled: boolean;
    private hasError: boolean;
    private placeholderText: string;
    private allowOther: any;
    private otherOptionPrepend: string;
    private allowEmpty: boolean;
    private emptyText: string;
    private emptyOptionLast: boolean;
    private formClass: string;
    private appendToBody: boolean;
    private orderByLabel: boolean;
    private scrollParent: HTMLElement;
    private uniqueId: string;
    private focusFirst: boolean;
    private translate: Function;
    private placeholder: string;
    private isHovering = false;
    private asyncOptionsFunction: () => any;
    private optionsLimit: number;
    private inputLimit: number;
    private useVirtualScroll: boolean;
    private isLoading: boolean;

    constructor(
        readonly $scope: any,
        readonly $timeout: ng.ITimeoutService,
        readonly $rootScope: IExtendedRootScopeService,
        public readonly $filter: ng.IFilterService,
        readonly $templateCache: ng.ITemplateCacheService,
        private readonly $element: HTMLElement,
    ) {
        this.translate = this.$rootScope.t;
    }

    public $onInit(): void {
        this.uniqueId = Utilities.uuid();
        if (isUndefined(this.appendToBody)) this.appendToBody = true;
        if (isUndefined(this.orderByLabel)) this.orderByLabel = true;
        if (isUndefined(this.disableIfNoOptions)) this.disableIfNoOptions = true;
        if (isUndefined(this.focusFirst)) this.focusFirst = true;
        if (isUndefined(this.emptyText)) this.emptyText = "- - - -";
        if (isUndefined(this.optionsLimit)) this.optionsLimit = 50;
        if (isUndefined(this.inputLimit)) this.inputLimit = 2;
        if (isUndefined(this.useVirtualScroll)) this.useVirtualScroll = false;
        this.$templateCache.put("uib/template/typeahead/typeahead-popup.html", require("./typeahead-list-template.component.html"));
        this.$templateCache.put("uib/template/typeahead/typeahead-match.html", require("./typeahead-item-template.component.html"));
        if (this.orderByLabel) {
            this.options = orderBy(this.options, (o: any): string | number => this.formatOption(o));
        }
        this.previousModel = this.model;
        this.uniqueId = Utilities.uuid();
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.options) {
            this.placeholder = this.getPlaceholder();
            this.allOptions = changes.options.currentValue;
        }
        if (changes.model) {
          this.previousModel = changes.model.currentValue;
        }
        if (
            changes.isLoading
            && changes.isLoading.previousValue === true
            && changes.isLoading.currentValue === false
            && document.activeElement === this.$element[0].firstElementChild[0]
        ) {
            this.$element[0].firstElementChild[0].blur();
            this.$timeout(() => {
                this.$element[0].firstElementChild[0].focus();
            }, 0);
        }
    }

    public mouseHovering(isHovering: boolean): void {
        this.isHovering = isHovering;
    }

    public formatOption(option: { [key: string]: any } | string | number): string | number {
        if (typeof option === "string" || typeof option === "number") return option;
        if (option) {
            if (this.translationKey && option[this.translationKey]) {
                return this.translate(option[this.translationKey]);
            }
            if (this.labelKey) {
                return option[this.labelKey];
            }
        }
        return "";
    }

    private handleSelection(response): void {
        this.mouseHovering(false);
        if (response && response.IsPlaceholder) {
            this.model = response.InputValue;
        }
        this.onSelect({ selection: response });
        if (this.clearOnSelect) {
            this.model = "";
        }
        if (!isUndefined(this.model) && !isNull(this.model)) {
            this.previousModel = this.model;
        }
    }

    private typeaheadOnBlur(): void {
        if (
            this.isHovering &&
            (isUndefined(this.model) || isNull(this.model)) &&
            this.$element[0].firstElementChild[0] &&
            this.$scope.typeaheadForm.typeaheadFormInput.$viewValue === ""
        ) {
            this.$element[0].firstElementChild[0].focus();
        }
        if (this.scrollParent) {
            this.scrollParent.onscroll = null;
        }
        if (this.onBlur) this.onBlur();
        if ((isUndefined(this.model) || isNull(this.model)) && !this.isHovering) {
            this.model = this.previousModel;
        }
    }

    private typeaheadOnFocus(): void {
        if (this.scrollParent) {
            this.repositionMenuOnScroll();
        }
        if (this.onFocus) this.onFocus();
    }

    private typeaheadOnClick(viewValue) {
        if ( typeof viewValue === "undefined" ) {
             return;
        } else {
            this.$scope.typeaheadForm.typeaheadFormInput.$setViewValue("");
        }
    }

    private typeaheadOnChange(): void {
        if (this.scrollParent) {
            this.repositionMenuOnScroll();
        }
        if (this.onChange) this.onChange();
    }

    private typeaheadOnKeyup(event): void {
        if (event.keyCode === KeyCodes.Esc) {
            this.handleSelection(this.model);
        }
    }

    private repositionMenuOnScroll(): void | undefined {
        if (!this.scrollParent || this.scrollParent.onscroll) return;
        this.$timeout((): void => {
            if (!this.showingOptions) return;
            const typeaheadMenu: HTMLElement = document.getElementById(this.uniqueId).parentElement;
            const topStyleValue: string = typeaheadMenu.style.top;
            const topStyleNum: number = topStyleValue ? parseInt(topStyleValue.slice(0, -2), 10) : 0;
            const currentScrollPos: number = this.scrollParent.scrollTop;
            const initialOffset: number = topStyleNum + currentScrollPos;
            this.scrollParent.onscroll = (): void => {
                typeaheadMenu.style.top = `${initialOffset - this.scrollParent.scrollTop}px`;
            };
        }, 0);
    }

    private getOptions(viewValue) {
        // Will check for input value and act as a limiter for performance
        if ( (viewValue && viewValue.length >= this.inputLimit) || ( this.loadOnClick && isUndefined(viewValue) ) ) {
            if ( this.loadOnClick && isFunction(this.asyncOptionsFunction ) && this.options.length === 0 ) {
                this.isLoading = true;
                return this.asyncOptionsFunction().then((res) => {
                    this.allOptions = res;
                    const filtered = this.typeaheadFilter(res, viewValue, this.allowOther, this.otherOptionPrepend, this.allowEmpty, this.emptyText, this.labelKey, this.emptyOptionLast);
                    this.options = filtered.slice(0, this.optionsLimit - 1);
                    if (this.orderByLabel) {
                        this.options = orderBy(this.options, (option: any): string | number => this.formatOption(option));
                    }
                    this.isLoading = false;
                    return this.options;
                });
            } else {
                const filtered = this.typeaheadFilter(this.allOptions, viewValue, this.allowOther, this.otherOptionPrepend, this.allowEmpty, this.emptyText, this.labelKey, this.emptyOptionLast);
                this.options = filtered.slice(0, this.optionsLimit - 1);
                return this.options;
            }
        } else {
            if (this.allOptions && this.allOptions.length > 0) {
                const filtered = this.typeaheadFilter(this.allOptions, viewValue, this.allowOther, this.otherOptionPrepend, this.allowEmpty, this.emptyText, this.labelKey, this.emptyOptionLast);
                this.options = filtered.slice(0, this.optionsLimit - 1);
            } else {
                this.options = [];
            }
            return this.options ;
        }
    }

    private typeaheadFilter (items: any[], inputValue: string, allowOther: boolean, otherOptionPrepend: string, allowEmpty: boolean, emptyText: string, labelKey: string, emptyOptionLast: boolean = false): any[] {
        const filteredList: any[] = labelKey ?
            filter(items, (item: any): boolean => toLower(item[labelKey]).indexOf(toLower(inputValue)) > -1) :
            this.$filter("filter")(items, { $: inputValue });

        if (allowEmpty) {
            this.showNoAnswer(filteredList, labelKey, emptyText, emptyOptionLast, Boolean(inputValue));
        }
        if (allowOther) {
            this.showInputWithHint(filteredList, inputValue, labelKey, otherOptionPrepend);
        }

        return filteredList;
    };

    private showNoAnswer(filteredList: any[], labelKey: string, emptyText: string, emptyOptionLast: boolean = false, startedTyping: boolean = false): void {
        const placeholder: any = {
            InputValue: "",
            IsPlaceholder: true,
        };
        placeholder[labelKey] = emptyText;

        if (!emptyOptionLast && !startedTyping) {
            filteredList.unshift(placeholder);
        } else {
            filteredList.push(placeholder);
        }
    }

    private showInputWithHint(filteredList: any[], inputValue: string, labelKey: string, hintWord: string): void {
        if (inputValue) {
            const nameWithHint: string = hintWord ? `${hintWord} "${inputValue}"` : inputValue;
            const placeholder: any = {
                InputValue: inputValue,
                IsPlaceholder: true,
            };
            placeholder[labelKey] = nameWithHint;
            filteredList.unshift(placeholder);
        }
    }

    private getPlaceholder(): string {
        if (this.disableIfNoOptions && !(this.options && this.options.length)) return this.translate("NoOptionsAvailable");
        if (this.placeholderText) return this.placeholderText;
        if (this.options && this.options.length > this.optionsLimit) return this.translate("SearchForMoreOptions");
        return "- - - -";
    }

}

const typeahead: ng.IComponentOptions = {
    bindings: {
        options: "<",
        loadOnClick: "<?",
        asyncOptionsFunction: "&?",
        model: "<?",
        onSelect: "&",
        labelKey: "@?",
        translationKey: "@?",
        hasArrow: "<?",
        clearOnSelect: "<?",
        onBlur: "&?",
        disableIfNoOptions: "<?",
        identifier: "<?",
        isRequired: "<?",
        isDisabled: "<?",
        hasError: "<?",
        placeholderText: "<?",
        allowOther: "<?",
        otherOptionPrepend: "@?",
        allowEmpty: "<?",
        emptyText: "@?",
        onFocus: "<?",
        onChange: "<?",
        emptyOptionLast: "<?",
        formClass: "@?",
        appendToBody: "<",
        scrollParent: "<?",
        orderByLabel: "<?",
        focusFirst: "<?",
        isParentModal: "<?",
        isLoading: "<?",
        showArrowByDefault: "<?",
        optionsLimit: "<?",
        inputLimit: "<?",
        useVirtualScroll: "<?",
    },
    controller: Typeahead,
    template: `
        <form
            class="typeahead {{$ctrl.formClass}}"
            name="typeaheadForm"
            data-ng-submit="$ctrl.handleSelection($ctrl.model)">
                <input
                    class="typeahead__input one-input"
                    placeholder="{{$ctrl.placeholder}}"
                    name="typeaheadFormInput"
                    type="text"
                    data-id="{{$ctrl.identifier}}"
                    data-ng-model="$ctrl.model"
                    ng-model-options="{ debounce: 350 }"
                    data-ng-blur="$ctrl.typeaheadOnBlur()"
                    data-ng-class="{'has-errors': $ctrl.hasError, 'has-arrow': ($ctrl.hasArrow && !$ctrl.showingOptions && !$ctrl.model) || $ctrl.showArrowByDefault }"
                    data-ng-disabled="($ctrl.disableIfNoOptions && !$ctrl.allOptions.length) || $ctrl.isDisabled"
                    data-ng-keyup="$ctrl.typeaheadOnKeyup($event)"
                    uib-typeahead="option as $ctrl.formatOption(option) for option in $ctrl.getOptions($viewValue)"
                    typeahead-editable="false"
                    typeahead-on-select="$ctrl.handleSelection($model)"
                    typeahead-min-length="0"
                    typeahead-focus-on-select="false"
                    typeahead-append-to-body="$ctrl.appendToBody"
                    typeahead-is-open="$ctrl.showingOptions"
                    autocomplete="off"
                    data-ng-focus="$ctrl.typeaheadOnFocus()"
                    data-ng-change="$ctrl.typeaheadOnChange()"
                    data-ng-required="$ctrl.isRequired"
                    ng-click="$ctrl.typeaheadOnClick($viewValue)"
                    other-option-prepend="{{$ctrl.otherOptionPrepend}}"
                    typeahead-focus-first="$ctrl.focusFirst"
                />
                <i
                    class="typeahead__icon fa fa-caret-down",
                    data-ng-if="($ctrl.hasArrow && !$ctrl.isLoading && !$ctrl.showingOptions && !$ctrl.model) || ($ctrl.showArrowByDefault)" />
                <i
                    class="typeahead__iconloading fa fa-spinner fa-pulse fa-fw",
                    data-ng-if="$ctrl.isLoading " />
        </form>
    `,
};

export default typeahead;
