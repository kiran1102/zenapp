import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NoteTypes } from "enums/notes.enum";
import { Permissions } from "modules/shared/services/helper/permissions.service";

class OneReviewNotesController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "Permissions"];

    public debouncedApprovalUpdate = _.debounce((data: any): void => {
        this.pageSlide.approvalNotes = data;
        this.$scope.$apply();
    }, 750);

    public debouncedInternalUpdate = _.debounce((data: any): void => {
        this.pageSlide.internalNotes = data;
        this.$scope.$apply();
    }, 750);

    private translations: any;
    private permissions: any;
    private noteTypes: any;
    private project: any;
    private approvalNotesLength: number;
    private internalNotesLength: number;
    private showNotes: number;
    private approvalPlaceholder: string;
    private internalPlaceholder: string;
    private editorApprovalNotesClassName: string;
    private editorApprovalNotesConfig: any;
    private editorInternalNotesClassName: string;
    private editorInternalNotesConfig: any;
    private pageSlide: any;

    constructor(
        private $scope: any,
        private $rootScope: IExtendedRootScopeService,
        private permissionsService: Permissions,
    ) {}

    $onInit() {
        this.translations = {
            reviewNotes: this.$rootScope.t("ReviewNotes"),
            approvalNotes: this.$rootScope.t("ApprovalNotes"),
            enterApprovalNotes: this.$rootScope.t("EnterApprovalNotes", { ApprovalNotes: this.$rootScope.t("ApprovalNotes") }),
            internalNotes: this.$rootScope.t("InternalNotes"),
            enterInternalNotes: this.$rootScope.t("EnterInternalNotes"),
            noNotesHaveBeenAdded: this.$rootScope.t("NoNotesHaveBeenAdded"),
        };

        this.permissions = {
            notesApprovalEdit: this.permissionsService.canShow("NotesApprovalEdit"),
            notesApprovalView: this.permissionsService.canShow("NotesApprovalView"),
            notesInternalEdit: this.permissionsService.canShow("NotesInternalEdit"),
            notesInternalView: this.permissionsService.canShow("NotesInternalView"),
        };

        this.noteTypes = NoteTypes;
        this.showNotes = NoteTypes.Approval;

        this.approvalNotesLength = _.filter(this.project.Notes, (note: any) => note.Type === NoteTypes.Approval).length;
        this.internalNotesLength = _.filter(this.project.Notes, (note: any) => note.Type === NoteTypes.Internal).length;

        this.editorApprovalNotesClassName = "pageslide__notes-text";
        this.editorApprovalNotesConfig = {
            placeholder: this.createPlaceholder(NoteTypes.Approval),
        };
        this.editorInternalNotesClassName = "pageslide__notes-text";
        this.editorInternalNotesConfig = {
            placeholder: this.createPlaceholder(NoteTypes.Internal),
        };
    }

    private createPlaceholder(type: number): string {
        if (this.permissions.notesApprovalEdit &&
            !this.approvalNotesLength &&
            type === NoteTypes.Approval) {
            return this.translations.enterApprovalNotes;
        }
        if (this.permissions.notesInternalEdit &&
            !this.internalNotesLength &&
            type === NoteTypes.Internal) {
            return this.translations.enterInternalNotes;
        }
        return this.translations.noNotesHaveBeenAdded;
    }

}

const OneReviewNotesTemplate = `
<section class="pageslide__wrapper">
    <header class="pageslide__header">
        <h3 class="pageslide__header-text">{{::$ctrl.translations.reviewNotes}}</h3>
        <i
            class="ot ot-close"
            ng-click="$ctrl.pageSlide.checked = !$ctrl.pageSlide.checked">
        </i>
    </header>

    <div class="pageslide__body padding-all-1">
        <div
            class="pageslide__button-row row-space-around text-center margin-bottom-1">
            <one-button
                button-click="$ctrl.showNotes = $ctrl.noteTypes.Approval"
                is-selected="$ctrl.showNotes === $ctrl.noteTypes.Approval"
                ng-if="::$ctrl.permissions.notesInternalView"
                text="{{::$ctrl.translations.approvalNotes}}"
                type="select">
            </one-button>

            <one-button
                button-click="$ctrl.showNotes = $ctrl.noteTypes.Internal"
                is-selected="$ctrl.showNotes === $ctrl.noteTypes.Internal"
                ng-if="::$ctrl.permissions.notesInternalView"
                text="{{::$ctrl.translations.internalNotes}}"
                type="select">
            </one-button>
        </div>

        <fieldset
            class="pageslide__notes pageslide__notes--approval"
            ng-if="$ctrl.permissions.notesApprovalView && $ctrl.showNotes === $ctrl.noteTypes.Approval"
            data-ng-disabled="::!$ctrl.permissions.notesApprovalEdit">
            <h5
                class="pageslide__notes-headline"
                ng-class="{'padding-top-1': $ctrl.permissions.notesInternalView}">
                {{::$ctrl.translations.approvalNotes}}
            </h5>
            <one-rich-text-editor
                editor-class-name="editorApprovalNotesClassName"
                content="$ctrl.pageSlide.approvalNotes"
                config="$ctrl.editorApprovalNotesConfig"
                on-content-change="$ctrl.debouncedApprovalUpdate(data)" />
        </fieldset>

        <div
            class="pageslide__notes pageslide__notes--internal"
            ng-if="$ctrl.permissions.notesInternalView && $ctrl.showNotes === $ctrl.noteTypes.Internal">
            <h5
                class="pageslide__notes-headline"
                ng-class="{'padding-top-1': $ctrl.permissions.notesInternalView}">
                {{::$ctrl.translations.internalNotes}}
            </h5>
            <one-rich-text-editor editor-class-name="editorInternalNotesClassName" content="$ctrl.pageSlide.internalNotes"
                config="$ctrl.editorInternalNotesConfig" on-content-change="$ctrl.debouncedInternalUpdate(data)"></one-rich-text-editor>
        </div>
    </div>
</section>
`;

const OneReviewNotesComponent: ng.IComponentOptions = {
    controller: OneReviewNotesController,
    template: OneReviewNotesTemplate,
    bindings: {
        pageSlide: "<",
        project: "<",
    },
};

export default OneReviewNotesComponent;
