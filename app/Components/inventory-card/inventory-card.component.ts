class InventoryCardCtrl implements ng.IComponentController {
    static $inject: string[] = ["$scope", "Tables", "TableHelper"];

    private Ready: boolean;

    private getRecord: any;
    private tableHelper: any;
    private destroyWatcher: any;

    private tableId: string;
    private inventoryId: string;
    private inventoryName: string;
    private Keys: any;
    private Inventory: any;

    constructor(readonly $scope: any, readonly Tables: any, readonly TableHelper: any) {}

    $onInit() {
        this.Ready = false;

        this.getRecord = this.Tables.getRecord;
        this.tableHelper = this.TableHelper;

        this.destroyWatcher = this.$scope.$watch(
            () => this.inventoryId,
            this.watchInventoryChange,
        );

        if (this.tableId && this.inventoryId) {
            this.handleInventoryChange(this.tableId, this.inventoryId);
        }
    }

    public $onDestroy(): void {
        this.destroyWatcher();
    }

    private handleInventoryChange(tableId: string, inventoryId: string): void {
        let firstTime = false;
        if (tableId && inventoryId) {
            if (!this.Inventory) {
                this.Ready = false;
                firstTime = true;
            }
            this.Keys = this.tableHelper.getKeys(tableId);
            this.getRecord(inventoryId).then(
                (response) => {
                    if (response.result) {
                        this.Inventory = this.deserialize(tableId, response.data);
                    } else {
                        this.Inventory = null;
                    }
                    if (firstTime) {
                        this.Ready = true;
                    }
                },
            );
        }
    }

    private watchInventoryChange = (newId: string, oldId: string): void => {
        if (newId !== oldId) {
            this.inventoryId = newId;
            this.handleInventoryChange(this.tableId, newId);
        }
    }

    private deserialize(tableId: string, record: any): any {
        const item = {};
        for (const column in record) {
            if (record.hasOwnProperty(column)) {
                const cell = record[column];
                const props = this.tableHelper.getColumnProperties(tableId, column);
                // If column is not hidden, had a valid column name, and valid value, then set the item.
                if (!props[column].Hide && cell[this.Keys.Column_Name] && cell[this.Keys.Column_Name] !== "Name" && cell[this.Keys.Item_Name]) {
                    item[cell[this.Keys.Column_Name]] = cell[this.Keys.Item_Name];
                }
            }
        }
        return item;
    }

}

const inventoryComponent: ng.IComponentOptions = {
    bindings: {
        inventoryId: "<",
        inventoryName: "@?",
        inventoryNamePrefix: "@?",
        tableId: "<",
    },
    controller: InventoryCardCtrl,
    template: `
        <div class="inventory-card">
        	<loading ng-if="!$ctrl.Ready" text="{{$ctrl.inventoryName}}" show-text="!!$ctrl.inventoryName" loading-class="inventory-card__loading"></loading>
        	<div ng-if="$ctrl.Ready" class="inventory-card__container">
        		<h3 ng-if="$ctrl.inventoryName || $ctrl.inventoryNamePrefix" class="inventory-card__title">
                    <span ng-if="$ctrl.inventoryNamePrefix" class="inventory-card__title--prefix">{{$ctrl.inventoryNamePrefix}}</span>
                    <span ng-if="$ctrl.inventoryName" class="inventory-card__title--name">{{$ctrl.inventoryName}}</span>
                </h3>
        		<ul ng-if="$ctrl.Inventory" class="inventory-card__details">
        			<li ng-repeat="(key, value) in $ctrl.Inventory track by $index" class="inventory-card__details-item">
                        <label class="inventory-card__details-name">{{key}}</label>
                        <span class="inventory-card__details-value">{{value}}</span>
                    </li>
        		</ul>
        		<h4 ng-if="!$ctrl.Inventory" class="inventory-card__not-found">
                    {{::($ctrl.inventoryName + " ")}}{{::$root.t("NotFound")}}
                </h4>
            </div>
        </div>
    `,
};

export default inventoryComponent;
