import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OneDraggableCard implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private titleBarClass: string;
    private titleBarColor: string;
    private draggableIcon: boolean;
    private onDrag: any;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }
    public dragCallback(id: any): boolean {
        if (this.onDrag) {
            return this.onDrag(id);
        }
        return false;
    }

    public $onInit(): void {
        this.titleBarClass = `one-draggable-card__title-bar--${this.titleBarColor || "default"}`;
    }
}

const oneDraggableCard = {
    bindings: {
        id: "<",
        titleBarColor: "@",
        iconClass: "@?",
        canShowIcon: "<?",
        draggableIcon: "<?",
        hideCardBody: "<?",
        onDrag: "<?",
        iconCallback: "&?",
    },
    controller: OneDraggableCard,
    transclude: {
        title: "cardTitle",
        body: "?cardBody",
    },
    template: `
			<div
				class="one-draggable-card"
				ng-class="{ 'one-draggable-card--no-body': $ctrl.hideCardBody }"
				dnd-draggable="$ctrl.id"
				dnd-moved="$ctrl.dragCallback($ctrl.id)"
			>
				<div class="one-draggable-card__title-bar padding-all-1 {{$ctrl.titleBarClass}}">
					<span class="one-draggable-card__handle row-space-between">
						<span ng-if="$ctrl.draggableIcon" class="margin-right-1">
							<i class="fa fa-ellipsis-v"></i>
							<i class="fa fa-ellipsis-v"></i>
                        </span>
                        <span class="one-draggable-card__title" ng-transclude="title"></span>
                        <i
                            ng-if="$ctrl.canShowIcon"
                            class="ot {{$ctrl.iconClass}}"
                            ng-click="$ctrl.iconCallback({cookie:$ctrl.id})"
                        />
					</span>
				</div>
				<div class="one-draggable-card__body padding-all-1"
						ng-if="!$ctrl.hideCardBody"
						ng-transclude="body">
				</div>
			</div>
		`,
};

export default oneDraggableCard;
