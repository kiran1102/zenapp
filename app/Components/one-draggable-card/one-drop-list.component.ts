import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OneDropList implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private onDrop: (id: any, index: number) => boolean;
    private noBorder: boolean;
    private dropList: any;
    private wrap: boolean;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    public dropCallback(index: number, id: any): boolean {
        if (this.onDrop) {
            return this.onDrop(id, index);
        }
        return false;
    }
}

const oneDropList = {
    controller: OneDropList,
    transclude: true,
    bindings: {
        onDrop: "<?",
        noBorder: "<?",
        wrap: "<?",
        loading: "=",
    },
    template: `
			<div
				class="one-drop-list margin-all-half text-left full-width"
				ng-class="{
					'one-drop-list--borderless': $ctrl.noBorder,
					'row-wrap': $ctrl.wrap,
					'row-horizontal-center': $ctrl.wrap
				}"
				dnd-list
				dnd-drop="$ctrl.dropCallback(index, item)"
				ng-transclude
				ng-disabled="$ctrl.loading"
			>
			</div>
		`,
};

export default oneDropList;
