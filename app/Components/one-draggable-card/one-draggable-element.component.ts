import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OneDraggableElement implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private selectable: boolean;
    private onDrag: any;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }
    public dragCallback(id: any): boolean {
        if (this.onDrag) {
            return this.onDrag(id);
        }
        return false;
    }
}

const oneDraggableElement = {
    bindings: {
        id: "<",
        onDrag: "<?",
        isDisabled: "<?",
    },
    controller: OneDraggableElement,
    transclude: {
        title: "element",
    },
    template: `
			<div class="one-draggable-element"
				dnd-draggable="$ctrl.id"
				dnd-moved="$ctrl.dragCallback($ctrl.id)"
				dnd-disable-if="$ctrl.isDisabled"
			>
				<div ng-transclude="title">
				</div>
			</div>
		`,
};

export default oneDraggableElement;
