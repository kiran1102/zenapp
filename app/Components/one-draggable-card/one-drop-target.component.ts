import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OneDropTarget implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private onDrop: (id: string) => boolean;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    public dropCallback(index: number, id: string): boolean {
        if (this.onDrop) {
            return this.onDrop(id);
        }
        return false;
    }
}

const oneDropTarget = {
    controller: OneDropTarget,
    transclude: true,
    bindings: {
        onDrop: "<?",
    },
    template: () => {
        return `<div class="one-drop-target"
					 dnd-list
					 dnd-drop="$ctrl.dropCallback(index, item)"
                     ng-transclude
                >
				</div>`;
    },
};

export default oneDropTarget;
