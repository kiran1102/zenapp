import * as _ from "lodash";
class OneOrgHeaderButton {

    private canSwitch: boolean;
    private selectedOrg: any;
    private orgList: any;
    private isDropdownOpen: boolean;
    private actionCallback: (action: any) => void;

    private dropdownToggled(open): void {
        this.isDropdownOpen = open;
    }

    private orgSelected(action, payload) {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action, payload });
        }
    }
}

const oneOrgHeaderButtonComponent: ng.IComponentOptions = {
    controller: OneOrgHeaderButton,
    bindings: {
        canSwitch: "<?",
        selectedOrg: "<?",
        orgList: "<",
        actionCallback: "&?",
    },
    template: `
        <div
            uib-dropdown
            uib-dropdown-toggle
            on-toggle="$ctrl.dropdownToggled(open)"
        >
            <div
                class="one-org-header-button__text"
                ng-class="{ 'enabled': $ctrl.canSwitch }"
            >
                {{$ctrl.selectedOrg.name}}
                <span
                    ng-if="$ctrl.orgList.length > 1 && $ctrl.canSwitch"
                    class="caret"
                ></span>
            </div>
            <one-org-dropdown
                ng-if="$ctrl.orgList.length > 1 && $ctrl.canSwitch"
                ng-class="{ 'open': $ctrl.isDropdownOpen }"
                org-list="$ctrl.orgList"
                selected-org="$ctrl.selectedOrg"
                align-right="true"
                action-callback="$ctrl.orgSelected(action, payload)"
            >
            </one-org-dropdown>
        </div>
    `,
};

export default oneOrgHeaderButtonComponent;
