
const OneLinkTemplate = `
    <a
        class="one-link"
        ui-sref="{{$ctrl.route}}($ctrl.routeParams)"
        >
        <span
            class="one-link__text"
            ng-if="$ctrl.text"
            ng-bind="$ctrl.text"
            >
        </span>
        <span
            class="helper-icon-link one-link__icon"
            ng-if="!$ctrl.hideIcon"
            >
        </span>
    </a>
    `;

const OneLinkComponent: any = {
    template: OneLinkTemplate,
    bindings: {
        route: "@?",
        routeParams: "<",
        text: "@?",
        hideIcon: "<?",
    },
};

export default OneLinkComponent;
