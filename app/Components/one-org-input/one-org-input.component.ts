import * as _ from "lodash";

class OneOrgInput implements ng.IComponentController {

    private selectedOrg: any;
    private orgList: any;
    private isDropdownOpen: boolean;
    private appendToBody: boolean;
    private placeholder: any;
    private actionCallback: (action: any) => void;

    public $onInit(): void {
        // this.selectedOrg = this.orgList[0];
    }

    private dropdownToggled(open): void {
        this.isDropdownOpen = open;
    }

    private orgSelected(action, payload) {
        this.selectedOrg = payload.org;
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action, payload });
        }
    }
}

export default {
    controller: OneOrgInput,
    bindings: {
        identifier: "@?",
        orgList: "<",
        selectedOrg: "<?",
        placeholder: "@?",
        actionCallback: "&?",
        appendToBody: "<?",
    },
    template: `
        <div
            uib-dropdown
            uib-dropdown-toggle
            ng-attr-dropdown-append-to-body="$ctrl.appendToBody"
            is-open="$ctrl.isDropdownOpen"
            on-toggle="$ctrl.dropdownToggled(open)"
            class="one-org-input shadow-on-hover"
            >
            <input
                class="width-0 opacity-0 no-outline"
                type="text"
                ng-model="$ctrl.selectedOrg.id"
                ng-focus="$ctrl.dropdownToggled(true)"
                ng-blur="$ctrl.dropdownToggled(false)"
                ot-auto-id="{{::$ctrl.identifier + 'Input'}}"
            />
            <div ng-if="$ctrl.selectedOrg.name" class="one-org-input__text">
                {{$ctrl.selectedOrg.name}}
            </div>
            <div ng-if="!$ctrl.selectedOrg.name && $ctrl.placeholder" class="one-org-input__text">
                {{$ctrl.placeholder}}
            </div>
            <span
                ng-if="$ctrl.orgList.length > 0"
                class="caret"
            ></span>
            <one-org-dropdown
                ng-if="$ctrl.orgList.length > 0"
                ng-class="{ 'open': $ctrl.isDropdownOpen }"
                org-list="$ctrl.orgList"
                selected-org="$ctrl.selectedOrg"
                action-callback="$ctrl.orgSelected(action, payload)"
                identifier="{{$ctrl.identifier}}"
            >
            </one-org-dropdown>
        </div>
    `,
};
