import * as _ from "lodash";

class PiaProjectName  implements ng.IComponentController {
    private rowData: any;
    private versionOptions: any[];
    private selectedOption: any;
    private actionCallback: (action: any) => void;

    public $onChanges(changes: ng.IOnChangesObject) {
        // Currently on reload, backend sends the lastest project version this logic works.
        // Chance of running into issues if backend sends most recently selected project version.
        if (this.rowData.Version > 1 && !this.versionOptions) {
            const transform = (val: any, i: number) => ({ label: `v${i + 1}`, value: i + 1 });
            this.versionOptions = Array.apply(null, { length: this.rowData.Version }).map(transform).reverse();
            this.selectedOption = this.versionOptions[0];
        } else if (this.versionOptions && changes.rowData.currentValue && changes.rowData.currentValue.IsLatestVersion) {
            this.selectedOption = this.versionOptions[0];
        }
    }

    private sendAction(action: string, option: any) {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action, payload: { rowData: this.rowData, version: option.value } });
        }
    }
}

const piaProjectName: ng.IComponentOptions  = {
    controller: PiaProjectName,
    bindings: {
        rowData: "<?",
        actionCallback: "&?",
    },
    template: `
                <div
                    data-id="piaProjectTableRowDataName"
                    class="pia-project-table__name-wrapper flex"
                    ng-class="{'padding-right-': $ctrl.rowData.Version > 1 || !$ctrl.rowData.IsLatestVersion}"
                >
                    <div
                        class="row-vertical-center stretch-horizontal flex-grow-1"
                        uib-tooltip="{{$ctrl.rowData.TemplateName}}"
                        tooltip-placement="top-left"
                        tooltip-trigger="mouseenter"
                        tooltip-class="margin-left-1"
                    >
                        <div class="row-vertical-center pia-project-table__name-share-icon width-2">
                            <i
                                ng-if="$ctrl.rowData.isShared"
                                class="fa fa-users"
                            ></i>
                        </div>
                        <div class="overflow-ellipsis">
                            {{$ctrl.rowData.Name}}
                        </div>
                    </div>
                    <div
                        ng-if="$ctrl.rowData.Version > 1 || !$ctrl.rowData.IsLatestVersion"
                        class="pia-project-table__name-version-select-wrapper"
                    >
                        <i
                            class="pia-project-table__name-arrow fa fa-caret-down"></i>
                        <select
                            data-id="piaProjectTableRowDataNameVersionSelect"
                            class="pia-project-table__name-version-select"
                            ng-options="option as option.label for option in $ctrl.versionOptions"
                            ng-model="$ctrl.selectedOption"
                            ng-change="$ctrl.sendAction('VERSION_CHANGED', $ctrl.selectedOption)"
                            ng-click="$event.stopPropagation()"
                        >
                        </select>
                    </div>
                </div>
                `,
};

export default piaProjectName;
