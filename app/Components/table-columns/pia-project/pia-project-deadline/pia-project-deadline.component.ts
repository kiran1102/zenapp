
class PiaProjectDeadline {
    private rowData: any;
}

const piaProjectDeadline = {
    controller: PiaProjectDeadline,
    bindings: {
        rowData: "<?",
    },
    template: `
                <one-date
                    ng-if="$ctrl.rowData.Deadline"
                    data-id="piaProjectTableRowDataDeadline"
                    date-to-format="$ctrl.rowData.Deadline"
                    enable-tooltip="true">
                </one-date>
                <span ng-if="!$ctrl.rowData.Deadline">
                    {{$root.t("NoDeadline")}}
                </span>
                `,
};

export default piaProjectDeadline;
