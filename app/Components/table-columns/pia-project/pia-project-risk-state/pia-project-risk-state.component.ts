
class PiaProjectRiskState {
    private rowData: any;
}

const piaProjectRiskState = {
    controller: PiaProjectRiskState,
    bindings: {
        rowData: "<?",
    },
    template: `
            <span
                data-id="piaProjectTableRowDataRiskStateNoRisk"
                ng-if="!$ctrl.rowData.riskClass">
                -
            </span>
            <span
                data-id="piaProjectTableRowDataRiskStateHasRisk"
                ng-if="$ctrl.rowData.riskClass"
                class="pia-project-table__risk-state-arrow {{$ctrl.rowData.riskClass}}"
                uib-tooltip="{{$ctrl.rowData.riskWord}}"
                tooltip-placement="right"
                tooltip-trigger="mouseenter">
            </span>
            `,
};

export default piaProjectRiskState;
