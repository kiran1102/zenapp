import * as _ from "lodash";

class PiaProjectButtons implements ng.IComponentController {
    private rowData: any;
    private actionCallback: (action: any) => void;
    private duplicatingEnabled: boolean; // show/hide a button
    private duplicatingDeactivated: boolean; // shows grayed out button
    private duplicatingTooltipText: string;
    private taggingEnabled: boolean;
    private versioningEnabled: boolean; // show/hide a button
    private versioningDeactivated: boolean; // shows grayed out button
    private versioningTooltipText: string;
    private showHorizontalLine: boolean;
    private permissions: any;
    private displaySpinner: boolean;

    public $onInit(): void {
        this.showHorizontalLine =
            (this.taggingEnabled && this.permissions.canShowTagging) ||
            (this.taggingEnabled && this.permissions.canShowTaggingUpgrade && this.rowData.IsLatestVersion) ||
            this.versioningEnabled ||
            this.duplicatingEnabled;
    }

    private sendAction(action: any): void {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action, payload: { rowData: this.rowData } });
        }
    }
}

const piaProjectButtons = {
    controller: PiaProjectButtons,
    bindings: {
        rowData: "<?",
        actionCallback: "&?",
        duplicatingEnabled: "<?",
        duplicatingDeactivated: "<?",
        duplicatingTooltipText: "@?",
        taggingEnabled: "<?",
        versioningEnabled: "<?",
        versioningDeactivated: "<?",
        versioningTooltipText: "@?",
        permissions: "<?",
        displaySpinner: "<?",
    },
    template: `
            <div
                data-id="piaProjectTableRowDataButtons"
                class="row-horizontal-vertical-center width-100-percent height-100 dropdown-menu-right"
                uib-dropdown
                uib-dropdown-toggle
                dropdown-append-to-body="true"
                auto-close="always outsideClick"
                >
                <i
                    class="pia-project-buttons__icon fa ot"
                    ng-class="$ctrl.displaySpinner ? 'fa-spinner fa-pulse fa-fw' : 'ot-ellipsis-h'"
                ></i>

                <ul
                    uib-dropdown-menu
                    class="pia-project-buttons__menu dropdown-menu-right"
                    >
                    <li
                        ng-if="$ctrl.permissions.canShowProjectsExportPdf"
                        class="pia-project-buttons__menu-item">
                        <a
                            data-id="piaProjectTableButtonExportPdf"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('EXPORT_PDF')"
                            >
                            <i class="pia-project-buttons__menu-item--icon fa fa-file-pdf-o"></i>
                            &nbsp;{{::$root.t("ExportAsPDF")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.permissions.canShowProjectsExportPdfUpgrade"
                        class="pia-project-buttons__menu-item">
                        <a
                            data-id="piaProjectTableButtonExportPdfUpgrade"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHOW_EXPORT_PDF_UPGRADE_MODAL')"
                            >
                            <i class="pia-project-buttons__menu-item--icon fa fa-file-pdf-o"></i>
                            &nbsp;{{::$root.t("ExportAsPDF")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.permissions.canShowProjectsViewHistory && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonProjectHistory"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHOW_PROJECT_HISTORY')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-history"></i>
                            &nbsp;{{::$root.t("ViewProjectHistory")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.permissions.ProjectsViewHistoryUpgrade && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonProjectHistoryUpgrade"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHOW_PROJECT_HISTORY_UPGRADE_MODAL')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-history"></i>
                            &nbsp;{{::$root.t("ViewProjectHistory")}}
                        </a>
                    </li>
                    <li
                        ng-if="::$ctrl.permissions.canShowShareProject && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonShareProject"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHARE_PROJECT')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-users"></i>
                            &nbsp;{{::$root.t("ShareProject")}}
                        </a>
                    </li>
                    <li
                        ng-if="::$ctrl.permissions.canShowShareProjectUpgrade && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonShareProjectUpgrade"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHARE_PROJECT_UPGRADE_MODAL')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-users"></i>
                            &nbsp;{{::$root.t("ShareProject")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.permissions.canShowProjectsDelete && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item">
                        <a
                            data-id="piaProjectTableButtonDeleteProject"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('DELETE_PROJECT')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-trash"></i>
                            &nbsp;{{::$root.t("Delete")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.permissions.canShowProjectsDeleteUpgrade && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item">
                        <a
                            data-id="piaProjectTableButtonDeleteProjectUpgrade"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHOW_DELETE_PROJECT_UPGRADE_MODAL')">
                            <i class="pia-project-buttons__menu-item--icon fa fa-trash"></i>
                            &nbsp;{{::$root.t("Delete")}}
                        </a>
                    </li>
                    <hr
                        ng-if="$ctrl.showHorizontalLine"
                        class="pia-project-buttons__menu-item--split-line"
                        />
                    <li
                        ng-if="$ctrl.duplicatingEnabled"
                        class="pia-project-buttons__menu-item"
                        uib-tooltip="{{$ctrl.duplicatingTooltipText}}"
                        tooltip-placement="top"
                    >
                        <a
                            data-id="piaProjectTableButtonDuplicateProject"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-class="{ 'deactivated': $ctrl.duplicatingDeactivated }"
                            ng-click="$ctrl.duplicatingDeactivated || $ctrl.sendAction('DUPLICATE_PROJECT')"
                        >
                            <i class="pia-project-buttons__menu-item--icon fa fa-copy"></i>
                            &nbsp;{{::$root.t("DuplicateProject", {ProjectTerm: $root.customTerms.Projects})}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.taggingEnabled && $ctrl.permissions.canShowTagging"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonTagging"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('TAG_PROJECT')"
                            >
                            <i class="pia-project-buttons__menu-item--icon fa fa-tags"></i>
                            &nbsp;{{::$root.t("Tagging")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.taggingEnabled && $ctrl.permissions.canShowTaggingUpgrade && $ctrl.rowData.IsLatestVersion"
                        class="pia-project-buttons__menu-item"
                        >
                        <a
                            data-id="piaProjectTableButtonTaggingUpgrade"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-click="$ctrl.sendAction('SHOW_TAG_PROJECT_UPGRADE_MODAL')"
                            >
                            <i class="pia-project-buttons__menu-item--icon fa fa-tags"></i>
                            &nbsp;{{::$root.t("Tagging")}}
                        </a>
                    </li>
                    <li
                        ng-if="$ctrl.versioningEnabled"
                        class="pia-project-buttons__menu-item"
                        uib-tooltip="{{$ctrl.versioningTooltipText}}"
                        tooltip-placement="top"
                    >
                        <a
                            data-id="piaProjectTableButtonNewProjectVersion"
                            class="pia-project-buttons__menu-item--anchor"
                            ng-class="{ 'deactivated': $ctrl.versioningDeactivated }"
                            ng-click="$ctrl.versioningDeactivated || $ctrl.sendAction('CREATE_NEW_PROJECT_VERSION')"
                        >
                            <i class="pia-project-buttons__menu-item--icon fa fa-plus"></i>
                            &nbsp;{{::$root.t("NewProjectVersion")}}
                        </a>
                    </li>
                </ul>
            </div>
            `,
};

export default piaProjectButtons;
