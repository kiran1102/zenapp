class PiaProjectStateLabel {
    private rowData: any;
}

const piaProjectStateLabel: ng.IComponentOptions = {
    controller: PiaProjectStateLabel,
    bindings: {
        rowData: "<?",
    },
    template: `
                <span
                    data-id="piaProjectTableRowDataStateLabel"
                    class="text-nowrap projectlabels-tag__{{$ctrl.rowData.StateLabel}}"
                    >
                        {{$ctrl.rowData.StateName}}
                </span>
                `,
};

export default piaProjectStateLabel;
