// Rxjs
import { startWith } from "rxjs/operators";

// 3rd Party
import moment from "moment";
import "moment-timezone";
import { Subscription } from "rxjs";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getUserById } from "oneRedux/reducers/user.reducer";
import { getOrgById } from "oneRedux/reducers/org.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IStringMap } from "interfaces/generic.interface";

// Enums
import { ModuleTranslationKeys } from "enums/module-types.enum";
import {
    ReportsTypePrettyNames,
    ReportType,
} from "reportsModule/reports-shared/enums/reports.enum";

// Services
import { TimeStamp } from "oneServices/time-stamp.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class ListTableRowCellController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "$state",
        "store",
        "TimeStamp",
    ];

    // inherited prop
    private component: string;
    private model: any;
    private modelPropKey: string;
    private maxWidth: string;

    // computed prop
    private cellText = "";
    private modifierClass: string;
    private sub: Subscription;

    private translate: (key: string, paramValues?: any) => string;
    private route: string;
    private routeParams: object;
    private routeString: string;
    private cellStyle: IStringMap<any>;

    constructor(
        $rootScope: IExtendedRootScopeService,
        private $state: ng.ui.IStateService,
        private store: IStore,
        private timeStamp: TimeStamp,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit(): void {
        this.sub = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe(() => this.componentWillReceiveState());
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (
            (changes.model && changes.model.previousValue !== changes.model.currentValue) ||
            changes.routeParams || changes.route
        ) {
            if (this.route) {
                this.routeString = this.routeParams ? `${this.route}(${JSON.stringify(this.routeParams)})` : this.route;
            }
            this.componentWillReceiveState();
        }
    }

    $onDestroy(): void {
        if (this.sub) this.sub.unsubscribe();
    }

    private componentWillReceiveState(): void {
        this.cellStyle = { "max-width": this.maxWidth || "initial", ...this.model.style };
        switch (this.component) {
            case "module-type-table-cell":
                this.cellText = this.translate(ModuleTranslationKeys[this.model[this.modelPropKey]]);
                break;
            case "user-name-table-cell":
                const systemUser: IUser = getUserById(this.model[this.modelPropKey])(this.store.getState());
                this.cellText = systemUser ? systemUser.FullName : this.model[this.modelPropKey] || "";
                break;
            case "org-name-table-cell":
                this.cellText = getOrgById(this.model[this.modelPropKey])(this.store.getState()).Name;
                break;
            case "date-table-cell":
                this.cellText = this.model[this.modelPropKey] ? this.timeStamp.formatDateWithoutTimezone(this.model[this.modelPropKey]) : "";
                break;
            case "active-tag-table-cell":
                this.cellText = this.model[this.modelPropKey] ? this.translate("Active") : this.translate("Inactive");
                this.modifierClass = `one-tag--rounded one-tag--${this.model[this.modelPropKey] ? "blue" : "red" }`;
                break;
            case "type":
                this.cellText = this.model[this.modelPropKey] === ReportType.PDF ? ReportType.PDF  : this.translate(ReportsTypePrettyNames[this.model[this.modelPropKey]]);
                break;
            default:
                this.cellText = this.model[this.modelPropKey];
        }
    }
}

export default {
    bindings: {
        component: "@?",
        model: "<",
        modelPropKey: "@",
        route: "@?",
        routeParams: "<?",
        link: "@?",
        scrollOverflow: "<?",
        maxWidth: "@?",
    },
    controller: ListTableRowCellController,
    template: `
        <scrollable-cell
            class="list-table__cell block media-middle text-nowrap position-relative full-width"
            is-disabled="!$ctrl.scrollOverflow"
            ng-style="::$ctrl.cellStyle"
            >
            <cell-content>
                <span ng-if="::!$ctrl.component" ng-bind="$ctrl.cellText"></span>
                <a
                    ng-if="$ctrl.component === 'link-table-cell' && $ctrl.routeString"
                    class="text-bold text-link text-decoration-none list-table__cell--link"
                    ui-sref="{{$ctrl.routeString}}"
                    ng-bind="$ctrl.cellText"
                ></a>
                <a
                    ng-if="$ctrl.component === 'link-table-cell' && $ctrl.link"
                    class="text-bold text-link text-decoration-none list-table__cell--link"
                    href="{{$ctrl.link}}"
                    ng-bind="$ctrl.cellText"
                ></a>
                <span ng-if="$ctrl.component === 'link-table-cell-translation' && !$ctrl.routeString && !$ctrl.link">{{::$root.t($ctrl.cellText)}}</span>
                <span ng-if="$ctrl.component === 'link-table-cell' && !$ctrl.routeString && !$ctrl.link" ng-bind="$ctrl.cellText"></span>
                <span ng-if="::$ctrl.component === 'module-type-table-cell'" ng-bind="$ctrl.cellText"></span>
                <span ng-if="::$ctrl.component === 'user-name-table-cell'" ng-bind="$ctrl.cellText"></span>
                <span ng-if="::$ctrl.component === 'org-name-table-cell'" ng-bind="$ctrl.cellText"></span>
                <span ng-if="::$ctrl.component === 'type'" ng-bind="$ctrl.cellText"></span>
                <one-date ng-if="::$ctrl.component === 'datetime-table-cell'" date-to-format="$ctrl.cellText" fall-back-to-value="true"></one-date>
                <one-date ng-if="::$ctrl.component === 'dateandtime-table-cell'" date-to-format="$ctrl.cellText" fall-back-to-value="false"></one-date>
                <span ng-if="::$ctrl.component === 'date-table-cell'" ng-bind="$ctrl.cellText"></span>
                <one-tag
                    ng-if="::$ctrl.component === 'active-tag-table-cell'"
                    plain-text="{{$ctrl.cellText}}"
                    label-class="{{$ctrl.modifierClass}}"
                ></one-tag>
                <span ng-if="$ctrl.component === 'html-table-cell'" ng-bind-html="$ctrl.cellText"></span>
            </cell-content>
        </scrollable-cell>
    `,
};
