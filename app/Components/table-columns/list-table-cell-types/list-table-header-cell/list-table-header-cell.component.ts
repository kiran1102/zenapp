// import invariant from "invariant";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IStringMap } from "interfaces/generic.interface";

class ListTableHeaderCellController implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: Function;
    private cellConfig: any;
    private headerTextKey: string;
    private translationKey: string;
    private headerText: string;
    private maxWidth: string;
    private style: IStringMap<string|number>;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    public $onInit(): void {
        // invariant(this.headerTextKey, "The key for the header text must be supplied.");
        // invariant(this.cellConfig, "Cell config object must be supplied.");
        this.headerText = this.translationKey && this.cellConfig[this.translationKey] ? this.translate(this.cellConfig[this.translationKey]) : this.cellConfig[this.headerTextKey];
        this.style = { "max-width": this.maxWidth || "initial", ...this.cellConfig.style };
    }

}

export default {
    bindings: {
        cellConfig: "<",
        headerTextKey: "@",
        translationKey: "@?",
        isSorted: "<?",
        sortAscending: "<?",
        isSortable: "<?",
        alignLeft: "<?",
        scrollOverflow: "<?",
        maxWidth: "@?",
    },
    controller: ListTableHeaderCellController,
    template: `
        <span
            class="text-bold list-table__cell list-table__cell--header text-nowrap position-relative"
            ng-class="::{
                'row-vertical-center text-left': $ctrl.alignLeft,
                'row-horizontal-vertical-center text-center': !$ctrl.alignLeft,
                'list-table__cell--sortable padding-left-right-3 cursor-pointer': $ctrl.isSortable,
                'cursor-default': !$ctrl.isSortable,
            }"
            ng-style="::$ctrl.style"
            >
            <span
                class="block max-width-full min-width-1"
                ng-class="{'position-relative': $ctrl.isSortable}"
                >
                <i
                    class="list-table__sort display-none fa absolute-vertical-center padding-left-half"
                    ng-class="{
                        'fa-caret-down': !$ctrl.sortAscending,
                        'fa-caret-up': $ctrl.sortAscending,
                        'block': $ctrl.isSorted,
                    }"
                    >
                </i>
                <span
                    ng-if="!$ctrl.scrollOverflow"
                    ng-bind="$ctrl.headerText"
                    >
                </span>
                <scrollable-cell
                    class="block full-width"
                    ng-if="$ctrl.scrollOverflow"
                    >
                    <cell-content>{{$ctrl.headerText}}</cell-content>
                </scrollable-cell>
            </span>
        </span>
    `,
};
