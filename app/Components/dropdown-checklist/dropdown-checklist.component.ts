import { forEach, find, isFunction, filter } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

interface IOption {
    [key: string]: any;
    isSelected?: boolean;
}

class DropdownChecklistCtrl implements ng.IComponentController {

    static $inject: string[] = ["$rootScope"];

    private selectionText: string;
    private selectedCount = 0;
    private translate: Function;
    private options: any[];
    private placeholder: string;
    private optionLabel: string;
    private translationKey: string;
    private handleSelection: (handleParam: {selection: any[]}) => void;
    private handleToggle: (isOpen: boolean, options: any[]) => void;

    constructor($rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    public $onChanges(changes: ng.IOnChangesObject) {
        if (changes && changes.options) {
            this.selectedCount = this.countSelected();
            this.selectionText = this.getSelectionText();
        }
    }

    private countSelected(): number {
        if (!this.options || !this.options.length) return 0;
        let count = 0;
        forEach(this.options, (option: IOption): void => {
            if (option.isSelected) count++;
        });
        return count;
    }

    private toggle(isOpen: boolean): void {
        if (isFunction(this.handleToggle)) this.handleToggle(isOpen, this.options);
    }

    private selectionOption(option: IOption): void {
        option.isSelected = !option.isSelected;
        if (option.isSelected) {
            this.selectedCount++;
        } else {
            this.selectedCount--;
        }
        this.selectionText = this.getSelectionText(option);
        this.handleSelection({selection: this.getallSelectedOptions()});
    }

    private getSelectionText(option?: IOption): string {
        if (option && option.isSelected) {
            return this.selectedCount > 1 ? `${this.selectedCount} ${this.translate("Options")}` : this.getOptionLabel(option);
        }
        return this.selectedCount > 1 ? `${this.selectedCount} ${this.translate("Options")}` :
            (this.selectedCount > 0 ? this.getOptionLabel(this.getSingleSelectedOption()) : this.placeholder);
    }

    private getSingleSelectedOption(): IOption {
        return find(this.options, (option) => option.isSelected === true);
    }

    private getallSelectedOptions(): IOption[] {
        return filter(this.options, (option) => option.isSelected === true);
    }

    private getOptionLabel(option: IOption): string {
        if (!option) return "";
        return this.translationKey && option[this.translationKey] ? this.translate(option[this.translationKey]) : option[this.optionLabel];
    }
}

export default {
    bindings: {
        options: "<",
        optionLabel: "@?",
        translationKey: "@?",
        placeholder: "@",
        isOpen: "<?",
        isDisabled: "<?",
        handleSelection: "&",
        handleToggle: "&?",
    },
    controller: DropdownChecklistCtrl,
    template: `
        <span
            class="one-select block"
            ng-class="{'disabled': $ctrl.isDisabled || !$ctrl.options || !$ctrl.options.length}"
            uib-dropdown
            keyboard-nav
            auto-close="outsideClick"
            on-toggle="$ctrl.toggle(open)"
            is-open="$ctrl.isOpen"
            >
            <button
                class="one-select__select text-left"
                uib-dropdown-toggle
                >
                <span>{{$ctrl.selectionText || $ctrl.placeholder}}</span>
            </button>
            <i class="one-select__arrow fa fa-caret-down"></i>
            <ul
                class="dropdown-checklist__list shadow full-width margin-all-0 padding-all-1 border-none overflow-y-auto"
                uib-dropdown-menu
                role="menu"
                >
                <li
                    class="dropdown-checklist__item margin-bottom-1"
                    ng-repeat="option in $ctrl.options track by $index"
                    ng-click="$ctrl.selectionOption(option)"
                    >
                    <one-checkbox
                        class="margin-right-1"
                        is-selected="option.isSelected"
                        >
                    </one-checkbox>
                    {{$ctrl.getOptionLabel(option)}}
                </li>
            </ul>
        </span>
    `,
};
