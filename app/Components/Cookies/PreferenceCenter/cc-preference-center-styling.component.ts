declare var OneConfirm: any;
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { EditablePreferenceItems } from "enums/cookies/preference-center.enum";

class CcPreferenceCenterStyling implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "CookieConsentService",
        "CookiePreferenceCenterService",
    ];

    itemBeingEdited: EditablePreferenceItems;
    PREFERENCE_ITEMS = EditablePreferenceItems;

    private translate: any;
    private minicolorsSettings: any;
    private preferenceCenterStyling: any;
    private logoFile: any;
    private handleStyleChange: any;
    private isCenterTileSkin: boolean;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        cookieConsentService: CookieConsentService,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
    ) {
        // TODO: should be in ngOnInit when converted
        this.translate = $rootScope.t;
        this.minicolorsSettings = cookieConsentService.getMinicolorSettings();
    }

    setItemBeingEdited(itemBeingEdited: EditablePreferenceItems) {
        this.itemBeingEdited = itemBeingEdited;
    }

    private updateLogo(): void {
        if (this.logoFile) {
            const valid_extensions: RegExp = /(\jpg|\jpeg|\gif|\png)$/i;
            const splittedStrings: any = this.logoFile.name.split(".");
            if (
                valid_extensions.test(
                    splittedStrings[splittedStrings.length - 1],
                )
            ) {
                const reader: FileReader = new FileReader();
                reader.onloadend = (e: any): void => {
                    this.preferenceCenterStyling.logoFileData = e.target.result;
                    this.preferenceCenterStyling.logoFileName = this.logoFile.name;
                    if (!this.preferenceCenterStyling.primaryColor) {
                        this.preferenceCenterStyling.primaryColor = "#f2f2f2";
                    }
                    this.handleStyleChange(this.preferenceCenterStyling);
                    this.$rootScope.$apply();
                };
                reader.readAsDataURL(this.logoFile);
            } else {
                OneConfirm(
                    this.translate("ValidFileFormat"),
                    this.translate("UploadValidImageFileFormat"),
                    "",
                    "",
                    true,
                    this.translate("Ok"),
                    this.translate("Cancel"),
                ).then(
                    (revert: boolean): void => {
                        const resetLogo = document.getElementById(
                            "preferenceCenterLogoFile",
                        ) as HTMLFormElement;
                        resetLogo.value = null;
                    },
                );
            }
        }
    }

    private onPreferenceStyleChange(styleType: string): void {
        this.preferenceCenterStyling.style = styleType;
        this.handleStyleChange(this.preferenceCenterStyling);
    }

    private resetToDefaultLogo(): void {
        OneConfirm(
            this.translate("RevertToDefault"),
            this.translate("AreYouSureYouWantToRevertToDefaultLogo"),
            "Confirm",
            "Ok",
            true,
            this.translate("Ok"),
            this.translate("Cancel"),
        ).then(
            (revert: boolean): void => {
                if (revert) {
                    const resetLogo = document.getElementById(
                        "preferenceCenterLogoFile",
                    ) as HTMLFormElement;
                    resetLogo.value = null;
                    this.preferenceCenterStyling.logoFileData = null;
                    this.preferenceCenterStyling.logoFileName = null;
                    this.preferenceCenterStyling.primaryColor = null;
                    this.handleStyleChange(this.preferenceCenterStyling);
                }
            },
        );
    }
}

const ccPreferenceCenterStyling: ng.IComponentOptions = {
    bindings: {
        preferenceCenterStyling: "=",
        handleStyleChange: "&",
        isCenterTileSkin: "<?",
        itemBeingEdited: "=",
    },
    controller: CcPreferenceCenterStyling,
    template: `
		<div class="form-group">
			<div  class="border-bottom" ng-if="!$ctrl.isCenterTileSkin">
				<label class="padding-top-1 text-bold">{{::$ctrl.translate('Style')}}</label>
				<div class="cc-accordion-navigation__button-container row-horizontal-center margin-all-1">
					<one-button
						identifier="CookiePrefCenterStyleModern"
						type="select"
						class="cc-accordion-navigation__button"
						is-selected="$ctrl.preferenceCenterStyling.style === 'MODERN'"
						button-class="full-width"
						button-click="$ctrl.onPreferenceStyleChange('MODERN')"
						text="{{::$root.t('Modern')}}"
					>
					</one-button>
					<one-button
						identifier="CookiePrefCenterStyleClassic"
						type="select"
						class="cc-accordion-navigation__button"
						is-selected="$ctrl.preferenceCenterStyling.style === 'CLASSIC'"
						button-class="full-width"
						text="{{::$root.t('Classic')}}"
						button-click="$ctrl.onPreferenceStyleChange('CLASSIC')"
					>
					</one-button>
				</div>
			</div>
			<div class="border-bottom">
				<label class="padding-top-1 text-bold">Colors</label>
				<div class="cc-accordion-navigation__item-content-section">
					<label class="padding-top-1 text-normal">{{::$ctrl.translate('PrimaryColor')}}</label>
					<input
						id="button-color-input"
						type="text"
						minicolors="$ctrl.minicolorsSettings"
						ng-model="$ctrl.preferenceCenterStyling.primaryColor"
						class="cc-accordion-navigation__item-content-input one-input"
						ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.PRIMARY_COLOR)"
						ng-blur="$ctrl.setItemBeingEdited(null)"/>
				</div>
				<div class="cc-accordion-navigation__item-content-section">
					<label class="padding-top-1 text-normal">{{::$ctrl.translate('ButtonColor')}}</label>
					<input
						id="button-color-input"
						type="text"
						minicolors="$ctrl.minicolorsSettings"
						ng-model="$ctrl.preferenceCenterStyling.buttonColor"
						class="cc-accordion-navigation__item-content-input one-input"
						ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.BUTTON_COLOR)"
						ng-blur="$ctrl.setItemBeingEdited(null)"/>
				</div>
				<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin">
					<label class="padding-top-1 text-normal">{{::$ctrl.translate('Menu')}}</label>
					<input
						id="button-color-input"
						type="text"
						minicolors="$ctrl.minicolorsSettings"
						ng-model="$ctrl.preferenceCenterStyling.menu"
						class="cc-accordion-navigation__item-content-input one-input"
						ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.MENU_COLOR)"
						ng-blur="$ctrl.setItemBeingEdited(null)"/>
				</div>
				<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin">
					<label class="padding-top-1 text-normal">{{::$ctrl.translate('MenuHighlightColor')}}</label>
					<input
						id="button-color-input"
						type="text"
						minicolors="$ctrl.minicolorsSettings"
						ng-model="$ctrl.preferenceCenterStyling.menuHighlightColor"
						class="cc-accordion-navigation__item-content-input one-input"
						ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.MENU_HIGHLIGHT_COLOR)"
						ng-blur="$ctrl.setItemBeingEdited(null)"/>
				</div>
				<div class="padding-bottom-1"></div>
			</div>
			<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin"
				ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.YOUR_LOGO)"
				ng-mouseleave="$ctrl.setItemBeingEdited(null)">
				<label class="padding-top-1 text-bold">{{::$ctrl.translate('YourLogo')}}</label>
				<div class="cc-accordion-navigation__item-content-section">
					<input
						data-file-on-change
						ng-model="$ctrl.logoFile"
						ng-change="$ctrl.updateLogo()"
						class="cc-accordion-navigation__item-content-input one-input padding-all-1"
						type="file"
						id="preferenceCenterLogoFile"
						accept="image/*"
					/>
				</div>
				<div class="flex static-vertical padding-top-1">
					<one-button
						type="primary"
						button-class="one-button--small"
						button-click="$ctrl.resetToDefaultLogo()"
						text="{{::$ctrl.translate('ResetToDefaultLogo')}}"
						identifier="resetToDefaultLogo"
					>
				</div>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$ctrl.translate('CustomCss')}}</label>
				<textarea class="one-text-area cc-accordion-navigation__item-content-input"
					name="textCustomCSS"
					ng-model="$ctrl.preferenceCenterStyling.customCSS">
				</textarea>
			</div>
		</div>`,
};

export default ccPreferenceCenterStyling;
