import { Directive, ElementRef, Injector, Input } from "@angular/core";
import { UpgradeComponent } from "@angular/upgrade/static";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

@Directive({
    selector: "upgraded-preference-center",
})
export class UpgradedPreferenceCenter extends UpgradeComponent {

    @Input() public templateJson: ITemplateJson;

    constructor(elementRef: ElementRef, injector: Injector) {
        super("preferenceCenterTemplate", elementRef, injector);
    }
}
