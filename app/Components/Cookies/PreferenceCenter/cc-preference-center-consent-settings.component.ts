// 3rd Party
import {
    each,
    find,
    remove,
} from "lodash";

// Enums
import * as CookieEnum from "../../../scripts/Cookies/Services/cookie.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IConsentSettingsGroupStatus,
    IValueId,
} from "interfaces/cookies/cc-pref-center.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Redux
import { getCurrentOrgId } from "oneRedux/reducers/org.reducer";

// Services
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";

class CcPreferenceCenterConsentSettings implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "Permissions",
        "$state",
        "CookiePreferenceCenterService",
        "store",
        "CookiePolicyService",
    ];

    customGroupIdEnabled = this.permissions.canShow("CookieCustomGroupId");
    canShowDNTOption = false;
    canShowCookieLogging = false;
    disableCookieLogging = false;
    isDntEnabled: boolean;
    saveConsentSettingInProgress = false;
    disabledDntToggle = false;
    translate = this.$rootScope.t;
    dntValueChangeCallback: () => void;
    private consentSettings: any;
    private consentModels: any;
    private ownedStatus: any;
    private isCustomFieldEnabled: boolean;
    private isConsentLoggingEnabled: boolean;
    private visitorOptions: IValueId[];
    private collectionPointId: string;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        private permissions: Permissions,
        readonly $state: ng.ui.IStateService,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
        private readonly store: IStore,
        private cookiePolicyService: CookiePolicyService,
    ) { }

    public $onInit(): void {
        this.canShowDNTOption = this.permissions.canShow("CookieAllowGroupDNT");
        this.canShowCookieLogging = this.permissions.canShow("CookieConsentLogging");
        this.setConsentList();
        this.visitorOptions = [{
            id: 1,
            value: "Cookie ID",
        }];
        this.consentSettings.visitorId = this.visitorOptions[0].id;
        if (this.canShowCookieLogging) {
            this.getCollectionPoint();
        }
    }

    public setConsentList = (): void => {
        if (this.consentModels) {
            each(this.consentModels, (consentModel: any): void => {
                if (consentModel.id === CookieEnum.ConsentModels.OwnerDefined) {
                    this.ownedStatus = consentModel;
                    this.isCustomFieldEnabled = true;
                }
            });
            if (this.consentSettings.consentModel !== CookieEnum.ConsentModels.OwnerDefined) {
                remove(this.consentModels, (consentModel: any): boolean => consentModel.id === CookieEnum.ConsentModels.OwnerDefined);
                this.isCustomFieldEnabled = false;
            }
        }
    }
    public setGroupDefaultStatus = (status: number): void => {
        each(this.consentSettings.groupStatuses, (group: any): void => {
            if (group.purposeGroupId !== CookieEnum.PurposeGroupId.StrictlyNecessaryCookies) {
                group.status = status;
            }
        });
        remove(this.consentModels, (consentModel: any): boolean => consentModel.id === CookieEnum.ConsentModels.OwnerDefined);
    }
    public onConsentModelsChange = (selectedConsent: number): void => {
        this.consentSettings.consentModel = selectedConsent;
        switch (selectedConsent) {
            case CookieEnum.ConsentModels.InformationOnly:
                this.setGroupDefaultStatus(CookieEnum.DefaultStatuses.AlwaysActive);
                break;
            case CookieEnum.ConsentModels.ImpliedConsent:
                this.setGroupDefaultStatus(CookieEnum.DefaultStatuses.Active);
                break;
            case CookieEnum.ConsentModels.ExplicitConsent:
                this.setGroupDefaultStatus(CookieEnum.DefaultStatuses.Inactive);
                break;
            case CookieEnum.ConsentModels.SoftOptIn:
                this.setGroupDefaultStatus(CookieEnum.DefaultStatuses.InactiveLandingPage);
                break;
            default:
                break;
        }
        this.updateConsentChange();
    }
    public onGroupStatusChange = (selectedConsent: number, group): void => {
        group.status = selectedConsent;
        const isOwnedConsentPresent: boolean = find(this.consentModels, (consentModel: any): boolean => consentModel.id === CookieEnum.ConsentModels.OwnerDefined);
        if (!isOwnedConsentPresent) {
            this.consentModels.push(this.ownedStatus);
        }
        this.consentSettings.consentModel = CookieEnum.ConsentModels.OwnerDefined;
        this.updateConsentChange();
    }
    public isDisabledGroup = (purposeId: number): boolean => {
        return !this.isCustomFieldEnabled || purposeId === CookieEnum.PurposeGroupId.StrictlyNecessaryCookies || false;
    }

    public onToggleGroup = (group: IConsentSettingsGroupStatus): void => {
        group.isExpanded = !group.isExpanded;
    }

    public toggleEnableCustomField = (): void => {
        this.isCustomFieldEnabled = !this.isCustomFieldEnabled;
    }

    public toggleEnableLoggingField = (): void => {
        this.disableCookieLogging = true;
        const organizationId = getCurrentOrgId(this.store.getState());
        this.isConsentLoggingEnabled = !this.isConsentLoggingEnabled;
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePolicyService.updateCustomLogging(domainId, organizationId, this.isConsentLoggingEnabled)
            .then((response: IProtocolResponse<string>): void => {
                this.disableCookieLogging = false;
                if (!response.result) {
                    this.isConsentLoggingEnabled = !this.isConsentLoggingEnabled;
                }
                this.collectionPointId = response.data;
            });
    }

    getCollectionPoint = (): void => {
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePreferenceCenterService.getCollectionPoint(domainId)
            .then((res: IProtocolResponse<string>) => this.collectionPointId = res.data);
    }

    toggleDNT(): void {
        this.disabledDntToggle = true;
        this.isDntEnabled = !this.isDntEnabled;
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePolicyService.updateDntToggle(domainId, this.isDntEnabled)
            .then((response: IProtocolResponse<{ groups: number[] }>): void => {
                this.disabledDntToggle = false;
                if (!response.result) return;
                this.dntValueChangeCallback();
            });
    }

    private updateConsentChange(): void {
        this.saveConsentSettingInProgress = true;
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePolicyService.updateConsentConfiguration(domainId, this.consentSettings)
            .then((): void => {
                this.saveConsentSettingInProgress = false;
            });
    }

}

const ccPreferenceCenterConsentSettings: ng.IComponentOptions = {
    bindings: {
        isConsentLoggingEnabled: "=",
        consentSettings: "=",
        consentModels: "=",
        defaultStatuses: "<",
        isDntEnabled: "<",
        dntValueChangeCallback: "&",
    },
    controller: CcPreferenceCenterConsentSettings,
    template: `
    <div class="form-group">
        <one-label-content
            ng-if="$ctrl.canShowDNTOption"
            wrapper-class="margin-bottom-2 text-bold"
            body-class="row-space-between"
        >
            <label>{{::$ctrl.translate('EnableDoNotTrackRequests')}}</label>
            <div class="row-space-between">
                <downgrade-switch-toggle
                    class="cc-accordion-navigation__item-content-toggle padding-bottom-1"
                    [identifier]="'ConsentSettingsDoNotTrack'"
                    [is-disabled]="$ctrl.disabledDntToggle"
                    [toggle-state]="$ctrl.isDntEnabled"
                    (callback)="$ctrl.toggleDNT()">
                </downgrade-switch-toggle>
                <i
                    class="margin-top-half margin-left-half ot ot-info-circle text-large"
                    uib-tooltip="{{::$ctrl.translate('DoNotTrackRequestsTooltipInfo')}}",
                    tooltip-append-to-body="true",
                    tooltip-trigger="mouseenter"
                />
            </div>
        </one-label-content>
        <div class="row margin-bottom-3 margin-right-0">
            <label class="col-sm-5">{{::$ctrl.translate('ConsentModel')}}</label>
			<one-select
				class="col-sm-6"
				name="{{::$ctrl.translate('Status')}}"
				options="$ctrl.consentModels"
				model="$ctrl.consentSettings.consentModel"
				on-change="$ctrl.onConsentModelsChange(selection)"
				label-key="Name"
				value-key="id"
				is-disabled="$ctrl.isCustomFieldEnabled || $ctrl.saveConsentSettingInProgress"
			>
			</one-select>
			<i class="fa fa-lg fa-question-circle col-sm-1"
			 	uib-tooltip="{{::$ctrl.translate('CookieConsentModelHelp')}}",
                tooltip-append-to-body="true",
                tooltip-trigger="mouseenter"
			/>
		</div>
		<div class="cc-accordion-navigation__item-content-section">
			<one-label-content wrapper-class="margin-bottom-2 text-bold">{{::$ctrl.translate('EnableCustom')}}
				<downgrade-switch-toggle
					class="cc-accordion-navigation__item-content-toggle pull-right padding-bottom-1"
					[toggle-state]="$ctrl.isCustomFieldEnabled"
					(callback)="$ctrl.toggleEnableCustomField()">
				</downgrade-switch-toggle>
            </one-label-content>
		</div>
		<div class="row cc-preference-center-consent-settings__table-border margin-right-0">
			<label class="col-sm-2 padding-left-right-0 text-center">{{::$ctrl.translate('ID')}}</label>
			<label class="col-sm-4 padding-left-right-0">{{::$ctrl.translate('CookieGroups')}}</label>
			<label class="col-sm-6 padding-left-right-0">{{::$ctrl.translate('DefaultStatus')}}</label>
		</div>
		<div class="row padding-bottom-1 padding-top-1 margin-right-0" ng-repeat="group in $ctrl.consentSettings.groupStatuses | orderBy:'orderPreview' track by group.groupLanguagePropertiesSetId">
			<label class="col-sm-2 text-normal padding-left-right-0 text-center">
				<i class="fa fa-fw no-outline"
					ng-class="{'fa-caret-down': group.isExpanded, 'fa-caret-right': !group.isExpanded }"
					ng-click="$ctrl.onToggleGroup(group)"
					ng-if="group.subGroups.length"
                />
                {{ ($ctrl.customGroupIdEnabled && group.customGroupId) ? group.customGroupId : group.purposeGroupId }}
            </label>
			<label class="col-sm-4 text-normal padding-left-right-0 word-break">{{::group.groupName}}</label>
			<one-select
				class="col-sm-6 text-normal padding-left-right-0"
				name="{{::$ctrl.translate('Status')}}"
				options="$ctrl.defaultStatuses"
				model="group.status"
				on-change="$ctrl.onGroupStatusChange(selection, group)"
				label-key="Text"
				value-key="id"
				select-class="cc-preference-center-consent-settings__select"
				is-disabled="$ctrl.isDisabledGroup(group.purposeGroupId) || $ctrl.saveConsentSettingInProgress"
			>
			</one-select>
			<div uib-collapse="!group.isExpanded">
				<div class="row cc-preference-center-consent-settings margin-right-0">
					<label class="col-sm-6 padding-top-1 text-center ">{{::$ctrl.translate('ID')}}</label>
					<label class="col-sm-6 padding-top-1" style="margin-left: -6rem;">{{::$ctrl.translate('SubGroupName')}}</label>
				</div>
				<hr class="cc-preference-center-consent-settings__divider margin-top-bottom-0 margin-left-6 centered-max-25"/>
				<div class="row padding-top-bottom-1 margin-right-0" ng-repeat="subgroup in group.subGroups | orderBy:'orderPreview' track by $index">
                    <label class="col-sm-6 text-normal text-center">
                        {{ ($ctrl.customGroupIdEnabled && subgroup.customGroupId) ? subgroup.customGroupId : "0_"+subgroup.id }}
                    </label>
					<label class="col-sm-6 text-normal" style="margin-left: -6rem;">{{::subgroup.groupName}}</label>
				</div>
			</div>
        </div>
        <div
            class="cc-accordion-navigation__item-content-section margin-top-2 border-top-1 enable-logging padding-top-1"
            ng-if="$ctrl.canShowCookieLogging"
        >
			<one-label-content wrapper-class="margin-bottom-2 text-bold">{{::$ctrl.translate('EnableLogging')}}
				<downgrade-switch-toggle
					class="cc-accordion-navigation__item-content-toggle pull-right padding-bottom-1"
                    [is-disabled]="$ctrl.disableCookieLogging"
                    [toggle-state]="$ctrl.isConsentLoggingEnabled"
                    (callback)="$ctrl.toggleEnableLoggingField()">
				</downgrade-switch-toggle>
            </one-label-content>
            <div
                class="row margin-bottom-3 margin-right-0"
                ng-if="$ctrl.isConsentLoggingEnabled"
            >
                <label class="col-sm-12">
                    <abbr class="slds-required" title="required">*</abbr>
                    {{::$ctrl.translate('UniqueSiteVisitorID')}}
                </label>
                <one-select
                    class="col-sm-12"
                    name="{{::$ctrl.translate('UniqueSiteVisitorID')}}"
                    options="$ctrl.visitorOptions"
                    model="$ctrl.consentSettings.visitorId"
                    on-change="$ctrl.onVisitorChange(selection)"
                    label-key="value"
                    value-key="id"
                    is-disabled="true"
                >
                </one-select>
            </div>
            <div
                class="webform-sidebar--edit-link margin-top-bottom-1"
                ng-if="$ctrl.isConsentLoggingEnabled"
            >
                <a
                    class="one-link"
                    target="_blank"
                    ui-sref="zen.app.pia.module.consentreceipt.views.transactions.list({collectionPointId : $ctrl.collectionPointId })"
                >
                    <span class="one-link__text">
                        {{::$root.t('ViewConsentRecords')}}
                    </span>
                    <span class="helper-icon-link one-link__icon"
                        ng-if="!$ctrl.hideIcon"
                    </span>
                </a>
            </div>
        </div>
	</div>
	`,
};

export default ccPreferenceCenterConsentSettings;
