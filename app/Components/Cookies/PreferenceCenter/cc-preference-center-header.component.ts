import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class CcPreferenceCenterHeader implements ng.IComponentController {

    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private isUnpublishedChanges: boolean;
    private pageReady: boolean;
    private useTemplate: boolean;
    private populatePreferenceCenter: () => void;

    constructor(readonly $rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }
}

const ccPreferenceCenterHeader: ng.IComponentOptions = {
    bindings: {
        populatePreferenceCenter: "&",
        isUnpublishedChanges: "=",
        pageReady: "=",
        useTemplate: "<?",
    },
    controller: CcPreferenceCenterHeader,
    template: `
			<cc-cookie-compliance-header
				navigation-title="{{::$ctrl.translate('PreferenceCenter')}}",
				domain-change-function="$ctrl.populatePreferenceCenter()",
				is-unpublished-changes="$ctrl.isUnpublishedChanges",
				page-ready="$ctrl.pageReady",
				show-preview-button="false",
				show-advance-settings-button="true",
				show-publish-button="true",
				show-language-selection="true",
				page-name="ccPreferenceCenter",
				use-template="$ctrl.useTemplate",
			>
			</cc-cookie-compliance-header>
			`,
};

export default ccPreferenceCenterHeader;
