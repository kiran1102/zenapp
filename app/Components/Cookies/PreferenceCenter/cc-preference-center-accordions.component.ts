declare var OneConfirm: any;
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";
import { IPrefCenterData, IPrefCenterPreviewCategory } from "interfaces/cookies/cc-pref-center.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

class CcPreferenceCenterAccordions implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "CookieComplianceStore", "CookiePreferenceCenterService", "Permissions"];

    public togglePreferenceCenterViews = false;

    private translate: (key: string) => string;
    private preferenceCenterData: IPrefCenterData;
    private domainId: number;
    private isUnpublishedChanges: boolean;
    private saveInProgress = false;
    private isLeftMenuNavigation: boolean;
    private useTemplate: boolean;
    private handleStyleChange: any;
    private isCenterTileSkin: boolean;
    private handleCallbackSubGroupOptOut: ({ toggleStatus, isToggleTrigger: boolean }) => boolean;
    private canShowIABSettings = false;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
        readonly permissions: Permissions,
    ) {
        this.translate = this.$rootScope.t;
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.canShowIABSettings = this.permissions.canShow("CookieIABIntegration");
        this.togglePreferenceCenterViews = this.permissions.canShow("CookieAngularUpgradePrefCenter");
    }

    public validForm = (): boolean => {
        const details: any = this.preferenceCenterData.details;
        return (details.privacyTitle && details.privacyText && details.cookiePolicyLink
            && details.cookiePolicyURL && details.allowAllCookiesButtonLabel
            && details.saveSettingsButtonLabel && details.activeLabelText
            && details.alwaysActiveLabelText);
    }
    public updatePreferenceCenter = (): void => {
        this.saveInProgress = true;
        this.cookiePreferenceCenterService.updatePreferenceCenter(this.domainId, this.preferenceCenterData).then((response: any): void => {
            this.isUnpublishedChanges = true;
            this.saveInProgress = false;
        });
    }
    public callbackSubGroupOptOut(toggleStatus: boolean, isToggleTrigger: boolean): boolean {
        return this.handleCallbackSubGroupOptOut({ toggleStatus, isToggleTrigger });
    }

    public upgradedCallbackSubGroupOptOut(toggleObject): boolean {
        return this.handleCallbackSubGroupOptOut(toggleObject);
    }
    private revertToDefaultStyle = (): void => {
        OneConfirm(this.translate("RevertToDefault"), this.translate("AreYouSureYouWantToRevertToDefaultStyle"), "Confirm", "Ok", true, this.translate("Ok"), this.translate("Cancel")).then(
            (revert: boolean): void => {
                if (revert) {
                    this.preferenceCenterData.styling = {
                        style: "MODERN",
                        primaryColor: null,
                        buttonColor: null,
                        menu: null,
                        menuHighlightColor: null,
                        logoFileName: null,
                        logoFileData: null,
                        customCSS: null,
                    };
                    this.handleStyleChange(this.preferenceCenterData.styling, true);
                    const resetLogo = document.getElementById("preferenceCenterLogoFile") as HTMLFormElement;
                    if (resetLogo) {
                        resetLogo.value = null;
                    }
                }
            });
    }

    private triggerStyleChange = (): void => {
        this.handleStyleChange(this.preferenceCenterData.styling);
    }
}

const ccPreferenceCenterAccordions: ng.IComponentOptions = {
    bindings: {
        preferenceCenterData: "=",
        isUnpublishedChanges: "=",
        handleStyleChange: "&",
        handleCallbackSubGroupOptOut: "&",
        useTemplate: "<?",
        isLeftMenuNavigation: "<?",
        isCenterTileSkin: "<?",
        itemBeingEdited: "=?",
    },
    controller: CcPreferenceCenterAccordions,
    template: `
		<nav class="cc-accordion-navigation height-100">
			<form class="margin-all-0 full-width" name="preferenceCenterForm" novalidate>
                <uib-accordion close-others="true" ng-if="!$ctrl.useTemplate || !$ctrl.isLeftMenuNavigation">
                    <div
                        ng-if="$ctrl.canShowIABSettings"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('IABConfiguration')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
							<cc-preference-center-iab-edit-details preference-center-form="preferenceCenterForm"
								[preference-center-content]="$ctrl.preferenceCenterData.details"
                                is-center-tile-skin="$ctrl.isCenterTileSkin"
							>
							</cc-preference-center-iab-edit-details>
						</div>
					</div>
                    <div
                        ng-if="!$ctrl.togglePreferenceCenterViews"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('PreferenceCenterDetails')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
							<cc-preference-center-edit-details preference-center-form="preferenceCenterForm"
								preference-center-content="$ctrl.preferenceCenterData.details"
								callback-sub-group-opt-out="$ctrl.callbackSubGroupOptOut(toggleStatus, isToggleTrigger)"
                                is-center-tile-skin="$ctrl.isCenterTileSkin"
                                item-being-edited="$ctrl.itemBeingEdited"
							>
							</cc-preference-center-edit-details>
						</div>
                    </div>
                    <div
                        ng-if="$ctrl.togglePreferenceCenterViews"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('PreferenceCenterDetails')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
                            <downgrade-preference-center-edit-details
								[preference-center-content]="$ctrl.preferenceCenterData.details"
                                [show-sub-group-toggles]="$ctrl.preferenceCenterData.consentSettings.showSubgroupToggles"
								[is-center-tile-skin]="$ctrl.isCenterTileSkin"
                                (callback-sub-group-opt-out)="$ctrl.upgradedCallbackSubGroupOptOut($event)"
							>
							</downgrade-preference-center-edit-details>
						</div>
					</div>
                    <div
                        ng-if="!$ctrl.togglePreferenceCenterViews"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('PreferenceCenterStyling')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
							<cc-preference-center-styling
								preference-center-styling="$ctrl.preferenceCenterData.styling"
								handle-style-change="$ctrl.triggerStyleChange()"
                                is-center-tile-skin="$ctrl.isCenterTileSkin"
                                item-being-edited="$ctrl.itemBeingEdited"
							>
							</cc-preference-center-styling>
						</div>
                    </div>
                    <div
                        ng-if="$ctrl.togglePreferenceCenterViews"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('PreferenceCenterStyling')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
							<downgrade-preference-center-styling
								[preference-center-styling]="$ctrl.preferenceCenterData.styling"
								[is-center-tile-skin]="$ctrl.isCenterTileSkin"
								(handle-style-change)="$ctrl.triggerStyleChange()"
							>
							</downgrade-preference-center-styling>
						</div>
					</div>
				</uib-accordion>
				<div class="cc-accordion-navigation__button-container row-horizontal-center margin-all-1">
					<one-button
						ng-if="!$ctrl.useTemplate"
						class="cc-accordion-navigation__button"
						button-class="full-width"
						button-click="$ctrl.revertToDefaultStyle()"
						text="{{::$root.t('RevertToDefault')}}"
						identifier="revertToDefaultStyle"
					>
					</one-button>
                    <one-button type="primary"
						ng-if="!$ctrl.useTemplate"
                        class="cc-accordion-navigation__button"
						button-class="full-width"
						enable-loading="true"
						is-disabled="$ctrl.saveInProgress || !$ctrl.validForm()"
						is-loading="$ctrl.saveInProgress"
						text="{{::$root.t('Save')}}"
						button-click="$ctrl.updatePreferenceCenter()"
						identifier="savePreferenceChange"
					>
					</one-button>
                </div>
			</form>
		</nav>`,
};

export default ccPreferenceCenterAccordions;
