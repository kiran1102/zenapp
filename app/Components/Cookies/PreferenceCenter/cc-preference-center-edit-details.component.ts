import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IPrefCenterDetails } from "interfaces/cookies/cc-pref-center.interface";
import { EditablePreferenceItems } from "enums/cookies/preference-center.enum";

class CcPreferenceCenterEditDetails implements ng.IComponentController {
    static $inject: string[] = ["Permissions"];

    itemBeingEdited: EditablePreferenceItems;
    PREFERENCE_ITEMS = EditablePreferenceItems;
    private preferenceCenterContent: IPrefCenterDetails;
    private showSubgroupToggle: boolean = this.permissions.canShow(
        "CookieSubGroupToggle",
    );
    private showSubgroupPreference: boolean;
    private showSubgroupPreferenceAndToggle: boolean;
    private callbackSubGroupOptOut: (
        { toggleStatus, isToggleTrigger: boolean },
    ) => boolean;
    private subgroupOptOut: boolean;
    private isCenterTileSkin: boolean;

    constructor(private permissions: Permissions) {}

    public $onInit(): void {
        this.showSubgroupPreference =
            this.permissions.canShow("CookiePreferenceSubgroups") &&
            !this.isCenterTileSkin;
        this.showSubgroupPreferenceAndToggle =
            this.showSubgroupToggle && this.showSubgroupPreference;
        if (this.showSubgroupPreferenceAndToggle) {
            this.subgroupOptOut = this.toggleSubGroupOptOut(false);
        }
    }

    setItemBeingEdited(itemBeingEdited: EditablePreferenceItems) {
        this.itemBeingEdited = itemBeingEdited;
    }

    public toggleSwitchLinkToCookipedia = (): void => {
        this.preferenceCenterContent.linkToCookiepedia = !this
            .preferenceCenterContent.linkToCookiepedia;
    }

    public toggleSwitchShowCookieList = (): void => {
        this.preferenceCenterContent.showCookiesList = !this
            .preferenceCenterContent.showCookiesList;
        this.subgroupOptOut = false;
        this.callbackSubGroupOptOut({
            toggleStatus: false,
            isToggleTrigger: true,
        });
    }

    public toggleSwitchShowPrefCloseButton = (): void => {
        this.preferenceCenterContent.showPreferenceCenterCloseButton = !this
            .preferenceCenterContent.showPreferenceCenterCloseButton;
    }

    public toggleShowSubGroupDescription = (): void => {
        this.preferenceCenterContent.showSubGroupDescription = !this
            .preferenceCenterContent.showSubGroupDescription;
    }

    public toggleShowSubGroupCookies = (): void => {
        this.preferenceCenterContent.showSubGroupCookies = !this
            .preferenceCenterContent.showSubGroupCookies;
    }

    public toggleSubGroupOptOut = (isToggleTrigger: boolean): boolean => {
        if (isToggleTrigger) {
            this.subgroupOptOut = !this.subgroupOptOut;
        }
        return this.callbackSubGroupOptOut({
            toggleStatus: this.subgroupOptOut,
            isToggleTrigger,
        });
    }
}

const ccPreferenceCenterEditDetails: ng.IComponentOptions = {
    bindings: {
        preferenceCenterContent: "=",
        preferenceCenterForm: "<",
        callbackSubGroupOptOut: "&",
        isCenterTileSkin: "<?",
        itemBeingEdited: "=",
    },
    controller: CcPreferenceCenterEditDetails,
    template: `
		<div class="form-group cc-preference-center-edit-details">
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('Title')}}</label>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textTitle"
					type="text"
					autofocus="true"
					ng-model="$ctrl.preferenceCenterContent.title"
					placeholder="{{::$root.t('EnterTitle')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.TITLE)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('YourPrivacyTitle')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textPrivacyTitle"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.privacyTitle"
					placeholder="{{::$root.t('EnterYourPrivacyTitle')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.YOUR_PRIVACY_TITLE)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textPrivacyTitle.$invalid && $ctrl.preferenceCenterForm.textPrivacyTitle.$touched'
					>
					{{::$root.t('PleaseEnterYourPrivacyTitle')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('YourPrivacyText')}}</label>
				<sup class="helper-icon-required"></sup>
				<textarea class="one-text-area cc-accordion-navigation__item-content-input cc-preference-center-edit-details__textarea"
					name="textPrivacyText"
					ng-required="true"
					placeholder="{{::$root.t('EnterPrivacyText')}}"
					ng-model="$ctrl.preferenceCenterContent.privacyText"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.YOUR_PRIVACY_TEXT)"
					ng-blur="$ctrl.setItemBeingEdited(null)">
				</textarea>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textPrivacyText.$invalid && $ctrl.preferenceCenterForm.textPrivacyText.$touched'
					>
					{{::$root.t('PleaseEnterPrivacyText')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('CookiePolicyLink')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textCookiePolicyLink"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.cookiePolicyLink",
					placeholder="{{::$root.t('EnterCookiePolicyLink')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.COOKIE_POLICY_LINK)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textCookiePolicyLink.$invalid && $ctrl.preferenceCenterForm.textCookiePolicyLink.$touched'
					>
					{{::$root.t('PleaseEnterCookiePolicyLink')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('CookiePolicyURL')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textCookiePolicyURL"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.cookiePolicyURL"
					placeholder="{{::$root.t('EnterCookiePolicyURL')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.COOKIE_POLICY_LINK)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textCookiePolicyURL.$invalid && $ctrl.preferenceCenterForm.textCookiePolicyURL.$touched'
					>
					{{::$root.t('PleaseEnterCookiePolicyURL')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('AllowAllCookiesButton')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textAllowAllButton"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.allowAllCookiesButtonLabel"
					placeholder="{{::$root.t('EnterAllowAllCookiesButtonLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.ALLOW_ALL_COOKIES_BUTTON)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textAllowAllButton.$invalid && $ctrl.preferenceCenterForm.textAllowAllButton.$touched'
					>
					{{::$root.t('PleaseEnterAllowAllCookiesButtonLabel')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('SaveSettingsButton')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textSaveSettingsButton"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.saveSettingsButtonLabel",
					placeholder="{{::$root.t('EnterSaveSettingsButtonLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SAVE_SETTINGS_BUTTON)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textSaveSettingsButton.$invalid && $ctrl.preferenceCenterForm.textSaveSettingsButton.$touched'
					>
					{{::$root.t('PleaseEnterSaveSettingsButtonLabel')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin">
				<label class="padding-top-1 text-bold">{{::$root.t('ActiveLabel')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textActiveLabelText"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.activeLabelText",
					placeholder="{{::$root.t('EnterActiveLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.ACTIVE_LABEL)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textActiveLabelText.$invalid && $ctrl.preferenceCenterForm.textActiveLabelText.$touched'
					>
					{{::$root.t('PleaseEnterActiveLabel')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin">
				<label class="padding-top-1 text-bold">{{::$root.t('InactiveLabel')}}</label>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textInactiveLabelText"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.inactiveLabelText",
					placeholder="{{::$root.t('EnterInactiveLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.INACTIVE_LABEL)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
			</div>
			<div class="cc-accordion-navigation__item-content-section">
				<label class="padding-top-1 text-bold">{{::$root.t('AlwaysActiveLabel')}}</label>
				<sup class="helper-icon-required"></sup>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textAlwaysActiveLabelText"
					type="text"
					ng-required="true"
					ng-model="$ctrl.preferenceCenterContent.alwaysActiveLabelText",
					placeholder="{{::$root.t('EnterAlwaysActiveLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.ALWAYS_ACTIVE_LABEL)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
				<p
					class="text-danger"
					ng-if='$ctrl.preferenceCenterForm.textAlwaysActiveLabelText.$invalid && $ctrl.preferenceCenterForm.textAlwaysActiveLabelText.$touched'
					>
					{{::$root.t('PleaseEnterAlwaysActiveLabel')}}
				</p>
			</div>
			<div class="cc-accordion-navigation__item-content-section" ng-if="!$ctrl.isCenterTileSkin">
				<label class="padding-top-1 text-bold">{{::$root.t('CookiesUsedLabel')}}</label>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textCookiesUsedLabelText"
					type="text"
					ng-model="$ctrl.preferenceCenterContent.cookiesUsedLabelText",
					placeholder="{{::$root.t('EnterCookiesUsedLabel')}}"
					ng-focus="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.COOKIES_USED_LABEL)"
					ng-blur="$ctrl.setItemBeingEdited(null)"
				/>
			</div>
            <div class="row-flex-end row-reverse margin-top-1" ng-if="!$ctrl.isCenterTileSkin">
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('ShowCookiesList')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SHOW_COOKIES_LIST)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [identifier]="'CookiesShowCookiesList'"
                    [toggle-state]="$ctrl.preferenceCenterContent.showCookiesList"
                    (callback)="$ctrl.toggleSwitchShowCookieList()">
                </downgrade-switch-toggle>
            </div>
            <div class="row-flex-end row-reverse margin-top-half" ng-if="!$ctrl.isCenterTileSkin">
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('LinkToCookiepedia')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.LINK_TO_COOKIEPEDIA)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [is-disabled]="!$ctrl.preferenceCenterContent.showCookiesList"
                    [toggle-state]="$ctrl.preferenceCenterContent.showCookiesList && $ctrl.preferenceCenterContent.linkToCookiepedia"
                    (callback)="$ctrl.toggleSwitchLinkToCookipedia()">
                </downgrade-switch-toggle>
            </div>
            <div class="row-flex-end row-reverse margin-top-half" ng-if='::$ctrl.showSubgroupPreference'>
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('ShowSubGroupDescription')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SHOW_SUBGROUP_DESC)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [is-disabled]="!$ctrl.preferenceCenterContent.showCookiesList"
                    [toggle-state]="$ctrl.preferenceCenterContent.showCookiesList && $ctrl.preferenceCenterContent.showSubGroupDescription"
                    (callback)="$ctrl.toggleShowSubGroupDescription()">
                </downgrade-switch-toggle>
            </div>
            <div class="row-flex-end row-reverse margin-top-half" ng-if='::$ctrl.showSubgroupPreference'>
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('ShowSubGroupCookies')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SHOW_SUBGROUP_COOKIES)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [is-disabled]="!$ctrl.preferenceCenterContent.showCookiesList"
                    [toggle-state]="$ctrl.preferenceCenterContent.showCookiesList && $ctrl.preferenceCenterContent.showSubGroupCookies"
                    (callback)="$ctrl.toggleShowSubGroupCookies()">
                </downgrade-switch-toggle>
            </div>
            <div class="row-flex-end row-reverse margin-top-half" ng-if='::$ctrl.showSubgroupPreferenceAndToggle'>
                <i class="margin-left-half fa fa-lg fa-question-circle col-sm-1"
                    uib-tooltip="{{::$root.t('SubGroupOptOutToolTip')}}",
                    tooltip-append-to-body="true",
                    tooltip-trigger="mouseenter"
                />
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('SubGroupOptOut')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SUBGROUP_OPT_OUT)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [is-disabled]="!$ctrl.preferenceCenterContent.showCookiesList"
                    [toggle-state]="$ctrl.preferenceCenterContent.showCookiesList && $ctrl.subgroupOptOut"
                    (callback)="$ctrl.toggleSubGroupOptOut(true)">
                </downgrade-switch-toggle>
            </div>
            <div class="row-flex-end row-reverse margin-top-half">
                <label class="margin-left-half margin-bottom-0 text-bold">{{::$root.t('ShowCloseButton')}}</label>
				<downgrade-switch-toggle
					ng-mouseover="$ctrl.setItemBeingEdited($ctrl.PREFERENCE_ITEMS.SHOW_CLOSE_BUTTON)"
					ng-mouseleave="$ctrl.setItemBeingEdited(null)"
                    [toggle-state]="$ctrl.preferenceCenterContent.showPreferenceCenterCloseButton"
                    (callback)="$ctrl.toggleSwitchShowPrefCloseButton()">
                </downgrade-switch-toggle>
            </div>
			<div
				class="cc-accordion-navigation__item-content-section"
				ng-if="$ctrl.preferenceCenterContent.showPreferenceCenterCloseButton"
			>
				<label class="padding-top-1 text-normal">
					{{::$root.t('AriaLabel')}}
					<i
						class="fa fa-lg fa-question-circle col-sm-1"
						uib-tooltip="{{::$root.t('AriaLabelTooltip')}}",
						tooltip-append-to-body="true",
						tooltip-trigger="mouseenter"
					/>
				</label>
				<input class="cc-accordion-navigation__item-content-input one-input"
					name="textSaveSettingsButton"
					type="text"
					ng-model="$ctrl.preferenceCenterContent.closeText"
				/>
			</div>
		</div>`,
};

export default ccPreferenceCenterEditDetails;
