import { find, includes } from "lodash";

// Interfaces
import { IDomain } from "interfaces/cookies/cc-domain.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import {
    IPrefCenterData,
    IPrefCenterPreviewCategory,
    IPrefCenterLanguageResources,
} from "interfaces/cookies/cc-pref-center.interface";
import { IPreviewStyle } from "interfaces/cookies/cc-preview-script.interface";
import { ICookieGroupDetail } from "interfaces/cookies/cookie-group-detail.interface";

// Services
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Constants
import { CookieGroup } from "constants/cookies/cookie-group.constant";
import { EditablePreferenceItems } from "enums/cookies/preference-center.enum";

class CcPreferenceCenter implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePreferenceCenterService",
        "CookieComplianceStore",
        "CookiePolicyService",
        "CookieConsentService",
        "NotificationService",
    ];

    public translate: (str: string) => string;
    public itemBeingEdited: EditablePreferenceItems = null;
    public policyGroupData: ICookieGroupDetail[];
    private preferenceCenterData: IPrefCenterData;
    private domainId: number;
    private pageReady: boolean;
    private isUnpublishedChanges: boolean;
    private selectedWebsite: string;
    private previewCssUrl: string;
    private previewLogoUrl: string;
    private useTemplate = false;
    private previewStyle: IPreviewStyle;
    private categoryPreview: IPrefCenterPreviewCategory;
    private isCenterTileSkin: boolean;
    private domainModel: any;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly cookiePolicyService: CookiePolicyService,
        readonly cookieConsentService: CookieConsentService,
        private notificationService: NotificationService,
    ) {
        this.translate = $rootScope.t;
    }

    public $onInit(): void {
        this.populateData();
    }
    public populatePreferenceCenter = (): void => {
        this.pageReady = false;
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();

        const domainList: IDomain[] = this.cookieComplianceStore.getWebsiteList();
        const SelectedDomain: IDomain = find(domainList, (domain: IDomain): boolean => domain.auditDomain === this.selectedWebsite);
        if (SelectedDomain.templateId !== 0) {
            this.notificationService.alertNote(this.translate("Information"), this.translate("PrefCenterUseTemplateToolTip"));
            this.useTemplate = true;
        } else {
            this.useTemplate = false;
        }
        this.cookiePolicyService.getPublishedStatus(this.domainId).then((response: IProtocolPacket): void => {
            this.isUnpublishedChanges = response.data.unpublished;
        });
        this.cookiePreferenceCenterService.getPreferenceCenter(this.domainId).then((response: IPrefCenterData): void => {
            this.preferenceCenterData = response;
            this.assignCategoriesForPreview();
            this.handleStyleChange(this.preferenceCenterData.styling);
            // To be removed and used new field from Service once API changes integrated
            let skinName = "";
            if (this.domainId) {
                this.cookieConsentService.getDomainModel(this.domainId).then((data: any): void => {
                    this.domainModel = data;
                    if (data.skinName) {
                        skinName = data.skinName;
                        this.isCenterTileSkin = includes(skinName, "default_flat_center");
                    }
                    if (this.policyGroupData) {
                        this.pageReady = true;
                    }
                });
            }
        });
        this.cookiePolicyService.getAssignedGroupByDomainId(this.domainId).then(
            (response: ICookieGroupDetail[]): void => {
                this.policyGroupData = response;
                if (this.domainModel) {
                    this.pageReady = true;
                }
        });
    }

    public handleStyleChange = (prefStyle: any, revertToDefault: boolean = false): void => {
        this.previewCssUrl = this.cookiePreferenceCenterService.getPreferencePreviewCssUrl(prefStyle.style);
        this.previewLogoUrl = revertToDefault ? null : this.cookiePreferenceCenterService.getPreferencePreviewLogo(prefStyle.style, prefStyle.logoFileData);
        this.previewStyle = { cssURL: this.previewCssUrl, logoURL: this.previewLogoUrl };
    }
    public handleCallbackSubGroupOptOut(toggleStatus: boolean, isToggleTrigger: boolean): boolean {
        if (isToggleTrigger) {
            this.preferenceCenterData.consentSettings.showSubgroupToggles = toggleStatus;
            return toggleStatus;
        } else {
            return this.preferenceCenterData.consentSettings.showSubgroupToggles;
        }
    }

    private populateData = (): void => {
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        if (!this.selectedWebsite) {
            this.$rootScope.$emit("ccInitCookieData", [this.selectedWebsite, "ccPreferenceCenter"]);
            this.$rootScope.$on("ccResumePreferenceCenter", (event: any, data: string): void => {
                this.populatePreferenceCenter();
            });
        } else {
            this.populatePreferenceCenter();
        }
    }

    private assignCategoriesForPreview(): void {
        if (this.preferenceCenterData.languageResources.length) {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.findCookieGroupName(CookieGroup.StrictlyNecessaryCookies),
                PerformanceCookies: this.findCookieGroupName(CookieGroup.PerformanceCookies),
            };
        } else {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.translate("StrictlyNecessary"),
                PerformanceCookies: this.translate("PerformanceCookies"),
            };
        }
    }

    private findCookieGroupName(groupKey: string): string {
        const languageResources = this.preferenceCenterData.languageResources;
        const language = find(languageResources, (resource: IPrefCenterLanguageResources): boolean => resource.key === groupKey);
        return language.value;
    }
}

export const ccPreferenceCenter: ng.IComponentOptions = {
    bindings: {},
    controller: CcPreferenceCenter,
    template: `
		<div class="cc-preference-center full-size column-nowrap">
			<cc-preference-center-header ng-if="$ctrl.selectedWebsite"
				populate-preference-center="$ctrl.populatePreferenceCenter()"
				page-ready="$ctrl.pageReady"
				is-unpublished-changes="$ctrl.isUnpublishedChanges"
				use-template="$ctrl.useTemplate"
			>
			</cc-preference-center-header>
    		<div class="module-wrapper">
				<loading text="{{ ::labels.loading }}" ng-if="!$ctrl.pageReady">
				</loading>
				<div class="flex full-size overflow-auto">
					<div class="cookiecompliance__edit-wrapper flex" ng-if="$ctrl.pageReady">
						<cc-preference-center-accordions
							preference-center-data="$ctrl.preferenceCenterData"
							is-unpublished-changes="$ctrl.isUnpublishedChanges"
							handle-style-change="$ctrl.handleStyleChange($ctrl.preferenceCenterData.styling)"
							handle-callback-sub-group-opt-out="$ctrl.handleCallbackSubGroupOptOut(toggleStatus, isToggleTrigger)"
							use-template="$ctrl.useTemplate"
							is-left-menu-navigation="true"
                            is-center-tile-skin="$ctrl.isCenterTileSkin"
                            item-being-edited="$ctrl.itemBeingEdited"
							>
						</cc-preference-center-accordions>
						<downgrade-cc-preference-center-preview class="flex margin-bottom-1"
                            [(preference-center-data)]="$ctrl.preferenceCenterData"
                            [policy-group-data]="$ctrl.policyGroupData"
							[preview-style]="$ctrl.previewStyle"
							[category-preview]="$ctrl.categoryPreview"
                            [use-template]="$ctrl.useTemplate"
                            [is-center-tile-skin]="$ctrl.isCenterTileSkin"
                            [item-being-edited]="$ctrl.itemBeingEdited"
						>
						</downgrade-cc-preference-center-preview>
					</div>
				</div>
			</div>
		</div>

	`,
};

export default ccPreferenceCenter;
