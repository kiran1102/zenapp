import {
    Component,
    Input,
} from "@angular/core";
import { IPrefCenterDetails } from "interfaces/cookies/cc-pref-center.interface";

@Component({
    selector: "cc-preference-center-iab-edit-details",
    templateUrl: "./cc-preference-center-iab-edit-details.component.html",
})

export class CcPreferenceCenterIabEditDetails {
    public isExpanded = false;
    public isCenterTileSkin: boolean;

    @Input() public preferenceCenterContent: IPrefCenterDetails;

    public onToggleAllowVendorConsent = (): void => {
        this.preferenceCenterContent.vendorLevelOptOut = !this.preferenceCenterContent.vendorLevelOptOut;
    }
}
