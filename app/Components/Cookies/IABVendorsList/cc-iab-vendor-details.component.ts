import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

import { isArray } from "lodash";

// Interfaces
import { IBreadcrumb } from "interfaces/breadcrumb.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import {
    IfilterMeta,
    IcustomFilter,
    IVendorsResponse,
    IvendorRecord,
    IvendorTableData,
    IFilterParams,
    IselectedFiltersList,
} from "interfaces/cookies/cc-iab-purpose.interface";

@Component({
    selector: "cc-iab-vendor-details",
    templateUrl: "./cc-iab-vendor-details.component.html",
})
export class CCIABVendorDetails {
    public breadCrumb: IBreadcrumb;
    public hasMinActiveVendors: boolean;
    public hasMinInActiveVendors: boolean;
    public isLoading = false;
    public currentActivePage = 0;
    public currentInActivePage = 0;
    public isActiveListExpanded = false;
    public isInactiveListExpanded = false;
    public isFilterActive = false;
    public filterMetaData: IfilterMeta[];
    public filterData: IselectedFiltersList[] = [];
    public activeSearchText = "";
    public inactiveSearchText = "";

    private activeVendorsInfo: IvendorTableData;
    private inActiveVendorsInfo: IvendorTableData;
    private minDefaultPageLength = 18;
    private showActiveVendorsEle = true;
    private showInactiveVendorsEle = true;

    constructor(
        private translatePipe: TranslatePipe,
        public stateService: StateService,
        private cookieIABIntegrationService: CookieIABIntegrationService,
    ) { }

    public ngOnInit(): void {
        this.breadCrumb = {
            stateName: "zen.app.pia.module.cookiecompliance.views.iabvendorslist",
            text: this.translatePipe.transform("VendorsList"),
        };
        this.getVendorDetails();
        this.getPurposesAndFeatures();
    }

    public getPurposesAndFeatures(): void {
        this.cookieIABIntegrationService.getPurposesAndFeatures()
            .then((res: IfilterMeta[]) => this.filterMetaData = res);
    }

    public getVendorDetails(activePage: number = 0, inactivePage: number = 0, activePageSize: number = 20, inactivePageSize: number = 20, activeName: string = "", inactiveName: string = "", filterData: IcustomFilter | IcustomFilter[] = null): void {
        this.isLoading = true;
        this.currentActivePage = activePage;
        this.currentInActivePage = inactivePage;
        this.activeSearchText = activeName;
        this.inactiveSearchText = inactiveName;
        if (isArray(filterData) && filterData.length === 0) {
            filterData = null;
            this.filterData = [];
        }
        this.isFilterActive = Boolean(filterData);
        this.cookieIABIntegrationService.getVendorDetails(this.stateService.params.listId, activePage, activePageSize, inactivePage, inactivePageSize, activeName, inactiveName, filterData)
            .then((response: IVendorsResponse): void => {
                this.isLoading = false;
                const activeVendors = [];
                const inActiveVendors = [];
                if (!response.activeVendors) {
                    response.activeVendors = {
                        content: [],
                    };
                }

                if (!response.inactiveVendors) {
                    response.inactiveVendors = {
                        content: [],
                    };
                }
                response.activeVendors.content.forEach((vendor) => {
                    vendor["firstPurposeName"] = vendor.purposes.length ? vendor.purposes.map((e) => e.purposeName).join(", ") : this.translatePipe.transform("NoPurposes");
                    vendor["featuresNames"] = vendor.features.length ? vendor.features.map((e) => e.name).join(", ") : this.translatePipe.transform("NoFeatures");
                    activeVendors.push(vendor);
                });
                response.inactiveVendors.content.forEach((vendor) => {
                    vendor["firstPurposeName"] = vendor.purposes.length ? vendor.purposes.map((e) => e.purposeName).join(", ") : this.translatePipe.transform("NoPurposes");
                    vendor["featuresNames"] = vendor.features.length ? vendor.features.map((e) => e.name).join(", ") : this.translatePipe.transform("NoFeatures");
                    inActiveVendors.push(vendor);
                });
                this.hasMinActiveVendors = activeVendors.length >= this.minDefaultPageLength;
                this.hasMinInActiveVendors = inActiveVendors.length >= this.minDefaultPageLength;
                this.activeVendorsInfo = {
                    rows: [...activeVendors],
                    name: this.translatePipe.transform("ActiveVendors"),
                    totalElements: response.activeVendors.totalElements,
                    size: 20,
                    number: response.activeVendors.number,
                };
                this.inActiveVendorsInfo = {
                    rows: [...inActiveVendors],
                    name: this.translatePipe.transform("InactiveVendors"),
                    totalElements: response.inactiveVendors.totalElements,
                    size: 20,
                    number: response.inactiveVendors.number,
                };
            });
    }

    public onActivePageChange($event: IFilterParams): void {
        this.isActiveListExpanded = true;
        this.filterData = $event.filterMetaData;
        const selectedFilters = $event.filterData.length ? $event.filterData : null;
        const searchText = $event.searchText;
        if (searchText) {
            this.getVendorDetails($event.page, this.currentInActivePage, 20, 20, searchText);
        } else {
            this.getVendorDetails($event.page, this.currentInActivePage, 20, 20, null, null, selectedFilters);
        }
    }

    public onInactivePageChange($event: IFilterParams): void {
        this.isInactiveListExpanded = true;
        this.filterData = $event.filterMetaData;
        const selectedFilters = $event.filterData.length ? $event.filterData : null;
        const searchText = $event.searchText;
        if (searchText) {
            this.getVendorDetails(this.currentInActivePage, $event.page, 20, 20, "", searchText);
        } else {
            this.getVendorDetails(this.currentInActivePage, $event.page, 20, 20, null, null, selectedFilters);
        }
    }

    public activateOrDeactivateVendors = (vendors: IvendorRecord[]): void => {
        this.isLoading = true;
        this.isActiveListExpanded = false;
        this.isInactiveListExpanded = false;
        const vendorIds = vendors.filter((e) => e.selected).map((e) => e.id);
        this.cookieIABIntegrationService.updateVendors(vendorIds, this.stateService.params.listId)
            .then((response: boolean): void => {
                this.isLoading = false;
                this.getVendorDetails();
            });
    }

    public updateWhiteListingVendors = (vendors: IvendorRecord[]): void => {
        this.isLoading = true;
        const vendorIds = vendors.reduce((selectedIds, vendor) => {
            if (vendor.selected) {
                selectedIds.push(vendor.id);
            }
            return selectedIds;
        }, []);
        this.cookieIABIntegrationService.updateWhitelistVendors(vendorIds, this.stateService.params.listId)
            .then((response: boolean): void => {
                this.isLoading = false;
                this.getVendorDetails();
            });
    }

    public hideInactiveVendors(hide: boolean) {
        this.showInactiveVendorsEle = !hide;
        this.isActiveListExpanded = hide;
        this.getVendorDetails();
    }

    public hideActiveVendors(hide: boolean) {
        this.showActiveVendorsEle = !hide;
        this.isInactiveListExpanded = hide;
        this.getVendorDetails();
    }

}
