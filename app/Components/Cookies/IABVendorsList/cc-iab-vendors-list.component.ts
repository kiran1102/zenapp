import {
    Component,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Interfaces
import { IVendorColumns, IVersion, IVersionData } from "interfaces/cookies/cc-iab-purpose.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IStringMap } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ModalService } from "sharedServices/modal.service";

@Component({
    selector: "cc-iab-vendors-list",
    templateUrl: "./cc-iab-vendors-list.component.html",
})
export class CCIABVendorsList {
    public columns: IVendorColumns[] = [];
    public data: IVersionData;
    public isReady = false;
    private currentPage: number;
    private translations: IStringMap<string>;

    constructor(
        private stateService: StateService,
        private translatePipe: TranslatePipe,
        private cookieIABIntegrationService: CookieIABIntegrationService,
        private cookieComplianceStore: CookieComplianceStore,
        private modalService: ModalService,
    ) { }

    public ngOnInit(): void {
        this.getVersions();
        this.columns = [
            { columnHeader: this.translatePipe.transform("VersionName"), columnTitleKey: "listName", dataValueKey: "ListName", clickable: true },
            { columnHeader: this.translatePipe.transform("ManagingOrganization"), columnTitleKey: "organization", dataValueKey: "OrgName", clickable: false },
            { columnHeader: this.translatePipe.transform("Description"), columnTitleKey: "description", dataValueKey: "ListDesc", clickable: false },
        ];
        this.translations = {
            cancelButtonText: this.translatePipe.transform("Cancel"),
            deleteTemplate: this.translatePipe.transform("DeleteVendorList"),
            confirmButtonText: this.translatePipe.transform("Confirm"),
        };
    }

    public getVersions(page: number = 0): ng.IPromise<boolean> {
        this.isReady = false;
        return this.cookieIABIntegrationService.getListNames(page).then(
            (versions: IVersionData): boolean => {
                this.data = versions;
                this.isReady = true;
                return;
            },
        );
    }

    public addVendorsList = (row: IVersion | null = null): void => {
        const data = {};
        if (row && Object.keys(row).length) {
            data["version"] = row;
            data["edit"] = true;
        } else {
            data["version"] = {};
            data["edit"] = false;
        }

        this.modalService.setModalData({
            data,
            promiseToResolve: (): ng.IPromise<boolean> => this.getVersions(this.currentPage),
        });
        this.modalService.openModal("addVendorListModal");

    }

    public getEllipsisOptions(row: IVersion): () => IDropdownOption[] {
        return (): IDropdownOption[] => {
            return [
                {
                    text: this.translatePipe.transform("EditRecord"),
                    action: (): void => {
                        this.addVendorsList(row);
                    },
                },
                {
                    text: this.translatePipe.transform("DeleteRecord"),
                    action: (): void => {
                        this.deleteTemplate(row);
                    },
                },
            ];
        };
    }

    public goTo = (listId, listName): void => {
        this.stateService.go(
            "zen.app.pia.module.cookiecompliance.views.iabvendorsdetails",
            { listId, listName },
        );
    }

    public getPage = (page): void => {
        this.currentPage = page;
        this.getVersions(page);
    }

    public deleteTemplate = (bannerTemplate): void => {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translations.deleteTemplate,
            confirmationText: this.translatePipe.transform("DeleteVendorListMessage", { TemplateDraftName: bannerTemplate.ListName }),
            cancelButtonText: this.translations.cancelButtonText,
            submitButtonText: this.translations.confirmButtonText,
            promiseToResolve:
                (): ng.IPromise<boolean> => this.cookieIABIntegrationService.deleteVendorVersions(bannerTemplate.ListId)
                    .then((response: boolean): boolean => {
                        if (response) {
                            this.getVersions(this.currentPage);
                        } else {
                            setTimeout(() => {
                                this.alertTemplate();
                            }, 100);
                        }
                        return true;
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }

    public saveRecord = (): Promise<boolean> => {
        return new Promise((resolve) => {
            return resolve(true);
        });
    }

    public alertTemplate = (): void => {
        const modalData = {
            modalTitle: this.translatePipe.transform("Delete"),
            confirmationText: this.translatePipe.transform("ReassignVendors"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            showDiscard: false,
            showSave: false,
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
    }
}
