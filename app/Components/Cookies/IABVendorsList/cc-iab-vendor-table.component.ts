// 3rd party
import {
    Component,
    Input,
    Output,
    EventEmitter,
    ElementRef,
} from "@angular/core";

// Interfaces
import {
    IVendorColumns,
    IfilterMeta,
    IfilterOptions,
    IcustomFilter,
    IselectedFiltersList,
    IvendorTableData,
    IVendorsResponse,
} from "interfaces/cookies/cc-iab-purpose.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { ModalService } from "sharedServices/modal.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "cc-iab-vendor-table",
    templateUrl: "./cc-iab-vendor-table.component.html",
})
export class IABVendorTableComponent {
    @Input() vendorlist: IvendorTableData;
    @Input() vendorBtnText: string;
    @Input() isExpanded: boolean;
    @Input() filterMetaData: IfilterMeta[];
    @Input() selectedFiltersList: IselectedFiltersList[] = [];
    @Input() vendorSearchText: string;
    @Input() canWhiteListVendors: boolean;
    @Input() refreshVendors: boolean;
    @Input() versionId: number;
    @Input() isFilterActive: boolean;
    @Output() onTableExpanded = new EventEmitter();
    @Output() manageVendor = new EventEmitter();
    @Output() onChangePage = new EventEmitter();
    @Output() onFilterApply = new EventEmitter();
    @Output() onSearch = new EventEmitter();
    @Output() onWhiteListVendors = new EventEmitter();

    public columns: IVendorColumns[] = [];
    public config: IStringMap<number> = {
        defaultSize: 5,
        expandedSize: 20,
    };
    public filterData: IfilterMeta[] = [];
    public disableVendorBtn: boolean;
    public isReady = false;
    public vendor: IvendorTableData;
    public whitelistVendorText: string;
    public disableWhitelistVendorText: string;
    public isSideBarExpanded = false;
    public iabFilterOption: IfilterMeta;
    public filterValues: IfilterOptions[];
    public selectedFilterOption: IfilterOptions[] = [];
    public customFilters: IcustomFilter[] = [];
    public filterKeys: string[];
    public selectedFilterValues;
    public countSelectedVendors = 0;
    public showWhiteListBtn = false;
    public showUnWhiteListBtn = false;
    private currentPage = 0;
    private selectAll: boolean;

    constructor(
        private translatePipe: TranslatePipe,
        readonly cookieIABIntegrationService: CookieIABIntegrationService,
        readonly modalService: ModalService,
        private elementRef: ElementRef,
    ) { }

    public ngOnInit(): void {
        this.isReady = true;
        this.disableVendorBtn = true;
        this.columns = [
            { columnHeader: "Id", columnTitleKey: "vendorId", dataValueKey: null, selectable: true },
            { columnHeader: "Name", columnTitleKey: "vendorName", dataValueKey: "name", selectable: false },
            { columnHeader: "Purposes", columnTitleKey: "purposesName", dataValueKey: "firstPurposeName", selectable: false },
            { columnHeader: "Features", columnTitleKey: "featuresName", dataValueKey: "featuresNames", selectable: false },
            { columnHeader: "PolicyURL", columnTitleKey: "vendorPolicy", dataValueKey: "policyUrl", selectable: false },
        ];

        if (this.canWhiteListVendors) {
            const whiteListColumn = { columnHeader: "Whitelisted", columnTitleKey: "whitelistVendor", dataValueKey: "whitelistVendor", selectable: false };
            this.columns.splice(1, 0, whiteListColumn);
        }
        this.whitelistVendorText = this.translatePipe.transform("WhitelistVendor");
        this.disableWhitelistVendorText = this.translatePipe.transform("DisableWhitelistVendor");
    }

    public ngAfterViewInit(): void {
        if (this.vendorSearchText) {
            this.elementRef.nativeElement.querySelector("input.slds-search__input").focus();
        }
    }

    public ngOnChanges(): void {
        if (this.filterMetaData && this.filterMetaData.length) {
            this.filterData = [...this.filterMetaData];
        }
        if (this.selectedFiltersList) {
            this.showFilteredList();
            this.updateFilterData();
        }
        try {
            this.vendor = { ...this.vendorlist };
            this.adjustPageSize(this.currentPage);
        } catch (e) {
            this.vendor = {
                rows: [],
            };
        }
    }

    public toggleExpanded(): void {
        this.isExpanded = !this.isExpanded;
        if (!this.isExpanded) {
            this.vendorSearchText = "";
        }
        this.adjustPageSize(this.currentPage);
        this.onTableExpanded.emit(this.isExpanded);
    }

    public pageChange(page: number = 0): void {
        this.getCustomFilters();
        this.onChangePage.emit({
            page,
            filterMetaData: this.selectedFiltersList,
            filterData: this.customFilters,
            searchText: this.vendorSearchText,
        });
        this.checkOrUncheckSelectAllVendors();
    }

    public adjustPageSize(page: number = 0) {
        this.currentPage = page;
        const pageSize: number = this.isExpanded ? this.config.expandedSize : this.config.defaultSize;
        const selectedVendors = this.vendorlist.rows;
        this.vendor.rows = selectedVendors.slice((page * pageSize), ((page + 1) * pageSize));
    }

    public checkOrUncheckSelectAllVendors() {
        const value = this.vendor.rows.every((e) => e.selected) && this.vendor.rows.length;
        this.selectAll = Boolean(value);
    }

    public manageActiveOrInactiveVendors(): void {
        this.manageVendor.emit(this.vendor.rows);
        if (this.isExpanded) {
            this.toggleExpanded();
        }
    }

    public updateWhitelistVendors(whitelist: boolean): void {
        let vendors = [];
        vendors = this.vendor.rows.filter((e) => e.selected && e.whiteListed === whitelist);
        const vendorIds = vendors.map((e) => e.id);
        this.whiteListTemplate(vendorIds, whitelist);
    }

    public searchVendors(search: string): void {
        if (!search && search !== "") {
            return;
        }
        this.vendorSearchText = search;
        this.onSearch.emit(search);
    }

    public trackByFilterIndex(index, item): number {
        return index;
    }

    public removeFilter(filter: string): void {
        this.selectedFiltersList = this.selectedFiltersList.filter((e) => Object.keys(e)[0] !== filter);
        this.showFilteredList();
        this.updateFilterData();
    }

    public onFilterOptionChange(option: IfilterMeta): void {
        this.iabFilterOption = option;
        this.filterValues = option.valueOptions;
        this.selectedFilterOption = [];
    }

    public clearAllFilters(): void {
        this.selectedFiltersList = [];
        this.filterKeys = [];
        this.selectedFilterOption = [];
        this.updateFilterData();
    }

    public updateFilterData(): void {
        this.filterData = this.filterMetaData.filter((e) => this.filterKeys.indexOf(e.name) < 0);
    }

    public onFilterValueChange({currentValue}: { currentValue: IfilterOptions[] }): void {
        this.selectedFilterOption = currentValue;
        const selectedIds = this.selectedFilterOption.map((opt) => opt.id);
        this.filterValues = this.filterValues.filter((value) => selectedIds.indexOf(value.id) < 0);
    }

    public addData(): void {
        this.selectedFiltersList.push({
            [this.iabFilterOption.name]: this.selectedFilterOption,
        });
        this.showFilteredList();
        this.updateFilterData();
        this.iabFilterOption = null;
    }

    public showFilteredList() {
        this.selectedFilterOption = [];
        this.filterKeys = [];
        this.selectedFilterValues = [];
        this.selectedFiltersList.forEach((filter) => {
            const key = Object.keys(filter)[0];
            const values = filter[key].map((e) => e.value);
            this.filterKeys.push(key);
            this.selectedFilterValues.push(values);
        });
    }

    public applyFilters(): void {
        this.getCustomFilters();
        this.onFilterApply.emit(this.customFilters);
    }

    public getCustomFilters(): void {
        this.customFilters = [];
        this.selectedFiltersList.forEach((filter) => {
            const key = Object.keys(filter)[0];
            const values = filter[key].map((e) => e.id);
            this.customFilters.push({
                filterType: key,
                selectionIds: values,
            });
        });
    }

    public toggleSideBar() {
        this.isSideBarExpanded = !this.isSideBarExpanded;
        this.iabFilterOption = null;
        this.selectedFilterOption = [];
    }

    public refreshVendorsList() {
        this.cookieIABIntegrationService.refreshVendors(this.versionId).then((res: IVendorsResponse) => this.onChangePage.emit({
            page: 0,
            filterMetaData: this.selectedFiltersList,
            filterData: this.customFilters,
            searchText: "",
        }));
    }

    public selectAllVendors() {
        this.selectAll = !this.selectAll;
        this.vendor.rows = this.vendor.rows.map((vendor) => {
            vendor.selected = this.selectAll;
            return vendor;
        });
        this.changeVendorBtnState();
        this.checkOrUncheckSelectAllVendors();
    }

    public deselectWhiteListVendors(): void {
        this.vendorlist.rows = this.vendorlist.rows.map((vendor) => {
            vendor.selected = false;
            return vendor;
        });
        this.changeVendorBtnState();
        this.checkOrUncheckSelectAllVendors();
    }

    public changeVendorBtnState(): void {
        const selectedVendors = this.vendorlist.rows.filter((e) => e.selected);
        this.countSelectedVendors = selectedVendors.length;
        if (this.countSelectedVendors) {
            this.disableVendorBtn = false;
        } else {
            this.disableVendorBtn = true;
        }
        this.showWhiteListBtn = selectedVendors.some((vendor) => !vendor.whiteListed);
        this.showUnWhiteListBtn = selectedVendors.some((vendor) => vendor.whiteListed);
    }

    public whiteListTemplate = (vendorIds: number[], whiteList: boolean): void => {
        const modalTitle = whiteList ? "RemoveWhitelistVendor" : "WhitelistVendorList";
        const confirmationText = whiteList ? "RemoveWhitelistVendorMessage" : "WhiteListVendorMessage";
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform(modalTitle),
            confirmationText: this.translatePipe.transform(confirmationText),
            customConfirmationText: this.translatePipe.transform("DoYouWantToContinue"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Confirm"),
            modalIcon: `ot ${whiteList ? "ot-whitelist-remove" : "ot-whitelist"}`,
            modalIconTextClass: whiteList ? "color-cherry" : "color-green",
            promiseToResolve:
                (): ng.IPromise<boolean> => this.cookieIABIntegrationService.updateWhitelistVendors(vendorIds, this.versionId)
                    .then((response: boolean): boolean => {
                        if (response) {
                            this.pageChange(this.currentPage);
                        }
                        return true;
                    }),
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("deleteConfirmationModal");
    }
}
