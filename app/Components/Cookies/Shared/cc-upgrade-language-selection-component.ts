import {
    Component,
    Input,
    Output,
    EventEmitter,
    Injectable,
    Inject,
    OnInit,
    OnDestroy,
} from "@angular/core";

// Rxjs
import {
    Subject,
    Subscription,
} from "rxjs";

import { findIndex } from "lodash";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IBannerLangReponse,
    IBannerLanguage,
} from "interfaces/cookies/cc-banner-languages.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Services
import { ToastService } from "@onetrust/vitreus";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import { LanguagePollingService } from "cookiesService/cookie-language-polling.service";
import { ModalService } from "sharedServices/modal.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";

// Reducer
import {
    getPreferenceCenterDisabledLanguagesStatus,
    PreferenceCenterActions,
} from "oneRedux/reducers/preference-center.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "cc-language-selection-component",
    templateUrl: "./cc-upgrade-language-selection-component.html",
})

@Injectable()
export class CcLanguageSelectionComponent implements OnInit, OnDestroy {
    canManageLanguage: boolean;

    @Input()
    languages: IBannerLanguage[];
    @Input()
    hideAddNewButton: boolean;
    @Input()
    isDisabled: boolean;
    @Input()
    showLoading: boolean;
    @Output()
    changeDomain = new EventEmitter();
    @Output()
    test = new EventEmitter();
    @Output()
    disableDomain = new EventEmitter<boolean>();
    @Output() updateBannerLanguageList = new EventEmitter<IBannerLanguage[]>();

    selectedLanguage: IBannerLanguage;

    private domainId: number;
    private deletedLanguages: number;
    private isSubscribed: Subscription;
    private languagePolling: Subject<IBannerLangReponse> = new Subject();
    private errorLanguagesList: number[] = [];
    private subscriptions: Subscription[] = [];

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        private translatePipe: TranslatePipe,
        private permissions: Permissions,
        private cookieComplianceStore: CookieComplianceStore,
        private modalService: ModalService,
        private cookieMultiLanguageService: CookieMultiLanguageService,
        private languagePollingService: LanguagePollingService,
        private toastService: ToastService,
        private cookiesStorageService: CookiesStorageService,
    ) { }

    ngOnInit() {
        const storeState = this.store.getState();
        this.isDisabled = getPreferenceCenterDisabledLanguagesStatus(storeState);
        this.canManageLanguage = this.permissions.canShow("CookieLanguageManagement");
        this.subscriptions.push(this.cookiesStorageService.selectedLanguage.subscribe((lang: IBannerLanguage) => {
            this.selectedLanguage = lang;
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => {
            sub.unsubscribe();
        });
    }

    // Add new language
    addNewLanguage(): void {
        this.modalService.setModalData({
            headerTitle: this.translatePipe.transform("ManageLanguages"),
            domainId: this.cookieComplianceStore.getSelectedDomainId(),
            onModalClose: (deletedLanguagesLength: number) => {
                this.deletedLanguages = deletedLanguagesLength;
                const domainId = this.cookieComplianceStore.getSelectedDomainId();
                if (this.canManageLanguage) {
                    if (!this.isSubscribed) {
                        this.handleSubcription();
                    }
                    this.languagePollingService.startPolling(
                        0,
                        5000,
                        domainId,
                        this.languagePolling,
                    );
                } else {
                    this.handleFeatureToggle();
                }
            },
        });
        this.modalService.openModal("ccLanguageModal");
    }

    // TODO: Remove this code once the language new features are stabilized.
    handleFeatureToggle(): void {
        if (this.deletedLanguages) {
            this.loadBannerLanguage().then((res: boolean) => {
                if (res) {
                    this.updateBannerLanguage();
                }
            });
        } else {
            this.loadBannerLanguage();
        }
    }

    // Handle language polling subscription
    handleSubcription(): void {
        this.isSubscribed = this.languagePolling.subscribe((data) => {
            if (data.languages && data.languages.length) {
                this.languages = data.languages;
                this.updateBannerLanguageList.emit(this.languages);
                this.handleErrorInLanguages(data.failedLanguges);
            }
            this.showLoading = data.refreshNeeded;
            this.isDisabled = data.refreshNeeded;
            this.store.dispatch({
                type: PreferenceCenterActions.TOGGLE_DISABLE_LANGUAGES,
                isDisabledLanguages: data.refreshNeeded,
            });
            this.disableDomain.emit(data.refreshNeeded);
            if (!data.refreshNeeded && this.deletedLanguages) {
                this.updateBannerLanguage();
                this.errorLanguagesList = [];
            }
        });
    }

    // Handle error while modifying the languages
    handleErrorInLanguages(languages: IBannerLanguage[]): void {
        languages.forEach((language) => {
            if (this.errorLanguagesList.indexOf(language.language.id) < 0) {
                this.errorLanguagesList.push(language.language.id);
                this.showError(this.translatePipe.transform("ErrorLanguage"), language.language.culture);
            }
        });
    }

    // Update banner languages
    updateBannerLanguage(): void {
        const newLanguage = this.languages
            .sort((a, b) => a.domainId - b.domainId)[0];
        const list: IBannerLanguage[] = this.cookieComplianceStore.getWebsiteList();
        const domainId = this.cookieComplianceStore.getSelectedDomainId();
        const domainIndex = findIndex(list, (item) => item.domainId === domainId);
        if (domainIndex > -1) {
            list[domainIndex].domainId = newLanguage.domainId;
        }
        this.cookieComplianceStore.setWebsiteList(list);
        this.getDomainByLanguage(newLanguage);
    }

    // Load banner languages on refresh
    loadBannerLanguage(showLoading: boolean = false): ng.IPromise<boolean> {
        this.showLoading = showLoading;
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        return this.cookieMultiLanguageService.getBannerLanguages(this.domainId)
            .then((response: IBannerLangReponse): boolean => {
                this.showLoading = false;
                this.languages = response.languages;
                this.updateBannerLanguageList.emit(this.languages);
                const domainId = this.cookieComplianceStore.getSelectedDomainId();
                this.selectedLanguage = this.languages.filter((e) => e.domainId === domainId)[0];
                return true;
            });
    }

    // Get domain on change of language
    getDomainByLanguage(value: IBannerLanguage): void {
        this.cookiesStorageService.selectedLanguage.next(value);
        this.cookieComplianceStore.setSelectedLanguageDomainId(value.domainId);
        this.changeDomain.emit();
    }

    // Show error message to user if the language doesn't add properly
    showError(title: string, message: string): void {
        this.toastService.updateConfig({
            positionClass: "bottom-right",
            maxOpened: 4,
            closeOldestOnMax: true,
            closeOnNavigation: true,
            timeout: 2000,
            closeLabel: "close",
        });
        this.toastService.show(title, message, "error");
    }
}
