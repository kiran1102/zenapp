// Services
import { WebAuditService } from "cookiesService/web-audit.service";

class CcScanResultsExport implements ng.IComponentController {
    static $inject: string[] = [
        "WebAuditService",
    ];

    private domainName: string;
    private downloadInProgress: boolean;
    private buttonText: string;
    private rowKey: string;

    constructor(
        readonly webAuditService: WebAuditService,
    ) {
        this.downloadInProgress = false;
    }

    exportToExcel(): void {
        this.downloadInProgress = true;
        this.webAuditService.exportScanResultsByScannedDate(this.domainName, this.rowKey).then((): void => {
            this.downloadInProgress = false;
        });
    }

}

export default {
    bindings: {
        buttonText: "@",
        domainName: "@",
        rowKey: "<",
    },
    controller: CcScanResultsExport,
    template: `
				<one-button
					icon="true"
					icon-class="fa fa-external-link"
					enable-loading="true"
					text="{{ ::$ctrl.buttonText}}"
					button-click="$ctrl.exportToExcel()"
					is-disabled="$ctrl.downloadInProgress"
					is-loading="$ctrl.downloadInProgress"
				>
				</one-button>
	`,
};
