import { Component, Inject, OnInit } from "@angular/core";
import { ModalService } from "sharedServices/modal.service";
import { IScanBehindTutorialModalResolver } from "interfaces/cookies/scan-behind-tutorial-modal-resolver.interface";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { IStore } from "interfaces/redux.interface";

@Component({
    selector: "scan-behind-tutorial",
    templateUrl: "./scan-behind-tutorial-modal.component.html",
})

export class ScanBehindTutorialComponent implements OnInit {
    resolve: IScanBehindTutorialModalResolver;

    constructor(
        @Inject(StoreToken) public store: IStore,
        public modalService: ModalService,
    ) {
    }

    ngOnInit(): void {
        this.resolve = getModalData(this.store.getState());
    }

    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public handleSubmit(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
