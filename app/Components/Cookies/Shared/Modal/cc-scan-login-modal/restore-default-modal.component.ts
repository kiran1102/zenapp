import { isFunction } from "lodash";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { IRestoreDefaultModalResolve } from "interfaces/cookies/restore-default-modal-resolver.interface";

class RestoreDefaultModalController implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "ModalService"];

    private resolve: IRestoreDefaultModalResolve;
    private promiseToResolve: () => ng.IPromise<boolean>;
    private modalTitle: string;
    private confirmationText: string;
    private warningText: string;
    private cancelButtonText: string;
    private submitButtonText: string;
    private isSubmitting: boolean;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
    ) {}

    public $onInit(): void {
        this.promiseToResolve = this.resolve.promiseToResolve;
        this.modalTitle = this.resolve.modalTitle;
        this.confirmationText = this.resolve.confirmationText;
        this.warningText = this.resolve.warningText;
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.submitButtonText = this.resolve.submitButtonText || this.$rootScope.t("Restore");
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }
}

const restoreDefaultModalTemplate: { template: string } = {
    template: `
        <section class="restore-default-modal padding-all-2">
            <div class="row-horizontal-center margin-bottom-2">
                <span class="restore-default-modal__icon fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x color-orange"></i>
                    <i class="fa fa-rotate-left fa-stack-1x fa-inverse"></i>
                </span>
            </div>
			<div class="restore-default-modal__text text-center text-color-default">{{$ctrl.confirmationText}}</div>
			<div class="restore-default-modal__text text-center margin-top-bottom-2"><b>{{$ctrl.warningText}}</b></div>
            <footer class="row-flex-end margin-top-2">
                <one-button
                    button-class="margin-right-1"
                    button-click="$ctrl.closeModal()"
					text="{{$ctrl.cancelButtonText}}"
					is-rounded=true
					type="primary">
                </one-button>
				<one-button
                    button-click="$ctrl.handleSubmit()"
                    enable-loading="true"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="$ctrl.isSubmitting"
                    text="{{$ctrl.submitButtonText}}"
					is-rounded=true>
                </one-button>
            </footer>
        </section>
    `,
};

export default {
    controller: RestoreDefaultModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(restoreDefaultModalTemplate.template),
};
