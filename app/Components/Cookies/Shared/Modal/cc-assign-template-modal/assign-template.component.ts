import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { ModalService } from "sharedServices/modal.service";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

import { IBannerTemplate, IAssignTemplateModalResolver } from "interfaces/cookies/cc-banner-template.interface";

import {
    filter,
} from "lodash";

@Component({
    selector: "assign-template",
    templateUrl: "./assign-template.component.html",
    styles: ["[otFormElement] { margin-bottom: 20px; }"],
})
export class AssignTemplateComponent implements OnInit {
    public saveInProgress = false;
    public templates: IBannerTemplate[] = [];
    public templatesLookUp: IBannerTemplate[] = [];
    public showDefaultTemplates = true;
    public selectedTemplate: IBannerTemplate;
    public isLoading = false;
    public modalData: IAssignTemplateModalResolver;

 constructor(
        @Inject(StoreToken) public store: IStore,
        public modalService: ModalService,
        public cookieTemplateService: CookieTemplateService,
    ) {
        cookieTemplateService.getAllTemplates().then((response: IBannerTemplate[]): void => {
            if (response) {
                this.templates = response;
            }
        });
    }

    public assignTemplate = (): void => {
        this.isLoading = true;
        this.cookieTemplateService.assignDomainToTemplate(this.selectedTemplate.templateId, this.modalData.domainId)
            .then((response: boolean): void => {
                this.modalData.callback();
                this.isLoading = false;
                this.modalService.closeModal();
                this.modalService.clearModalData();
            });
    }

    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public validateForm = (): boolean => {
        return this.modalData.domainId && (this.selectedTemplate && this.selectedTemplate.templateName.length > 0);
    }

    ngOnInit(): void {
        this.cookieTemplateService.getAllTemplates().then((response: IBannerTemplate[]): void => {
            if (response) {
                this.templates = response;
            }
        });
        this.modalData = getModalData(this.store.getState());
    }

    onInputChange({key, value}): void {
        if (key === "Enter") { return; }
        this.showDefaultTemplates = false;
        this.filterList(this.templates, value);
    }

    onModelChange({currentValue}): void {
        this.selectedTemplate = currentValue;
    }

    private filterList(templates: IBannerTemplate[], stringSearch: string): any {
        this.templatesLookUp = filter(templates, (template: IBannerTemplate): boolean => (template.templateName.search(stringSearch) !== -1));
    }
}
