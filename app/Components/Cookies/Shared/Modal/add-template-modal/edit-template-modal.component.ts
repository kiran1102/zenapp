import { Component, Inject, Input, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getOrgList, getCurrentOrgId } from "oneRedux/reducers/org.reducer";

import { ModalService } from "sharedServices/modal.service";
import { CookieTemplateService } from "cookiesService/cookie-template.service";

import { IStore, IStoreState } from "interfaces/redux.interface";
import { ITemplateModalResolver } from "interfaces/cookies/cc-banner-template.interface";
import { IBannerTemplate, IBannerTemplateCreate } from "interfaces/cookies/cc-banner-template.interface";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { IOrganization } from "interfaces/org.interface";
import { find } from "lodash";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "edit-template-modal",
    templateUrl: "./edit-template-modal.component.html",
})
@Injectable()
export class EditTemplateModalController implements OnInit {
    public modalTitle: string;
    public createTemplateButton: string;
    public cancelButtonText: string;
    public templateNameLabel: string;
    public organizationNameLabel: string;
    public defaultLanguageLabel: string;
    public isLoading = false;
    public templateName = "";
    public orgGroupList: IOrganization[] = [];
    public languageList: ILanguage[] = [];
    public selectedDomain: string;
    public selectedLanguage: ILanguage;
    public selectedOrgGroup: IOrganization;
    public bannerTemplate: IBannerTemplate;
    public errorMessage = "";
    public editDisabled = false;

    @Input() autoCloseEnabled = true;
    @Input() resolve: ITemplateModalResolver;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        public Modal: ModalService,
        public router: Router,
        public cookieTemplateService: CookieTemplateService,
    ) {}

    ngOnInit(): void {
        const modalData = getModalData(this.store.getState());
        this.bannerTemplate = modalData.bannerTemplate;
        this.templateName = this.bannerTemplate.templateName;
        this.cookieTemplateService.getAllLanguagesForTemplate()
            .then((response: ILanguage[]): void => {
                this.languageList = response;
                this.selectedLanguage = find(this.languageList, (language: ILanguage) => language.id === this.bannerTemplate.defaultLanguage.id);
            });
        this.modalTitle = modalData.modalTitle;
        this.createTemplateButton = this.translatePipe.transform("Save");
        this.cancelButtonText = modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.templateNameLabel = modalData.templateNameLabel;
        this.organizationNameLabel = modalData.organizationNameLabel;
        this.defaultLanguageLabel = modalData.defaultLanguageLabel;
        this.selectedDomain = modalData.domain;
        this.orgGroupList = this.getOrgList();
        this.selectedOrgGroup = find(this.orgGroupList, (organization: IOrganization) => organization.Name === this.bannerTemplate.organization);
    }

    public getOrgList(): IOrganization[] {
        const selectedOrgGroup = getCurrentOrgId(this.store.getState());
        return getOrgList(selectedOrgGroup)(this.store.getState());
    }

    public selectOrgGroup(selection: IOrganization): void {
        this.selectedOrgGroup = selection;
    }

    public selectLanguage(selection: ILanguage): void {
        this.selectedLanguage = selection;
    }

    public closeModal(): void {
        this.Modal.closeModal();
        this.Modal.clearModalData();
    }

    public setTemplateName(value: string): void {
        this.templateName = value;
        this.errorMessage = "";
    }

    public validateForm = (): boolean => {
        if (this.errorMessage || !this.templateName || !this.selectedLanguage || !this.selectedOrgGroup) {
            return false;
        }
        return this.selectedLanguage.id && this.selectedOrgGroup.Id.length > 0;
    }

    public editTemplate(): void {
        this.editDisabled = true;
        const templateData: IBannerTemplateCreate = {templateName: this.templateName, orgGroupId: this.selectedOrgGroup.Id, defaultLanguage: this.selectedLanguage.id};
        this.cookieTemplateService.editTemplate(this.bannerTemplate.templateId, templateData)
            .then((response: boolean): void => {
                if (response) {
                    this.editDisabled = false;
                    this.bannerTemplate.templateName = templateData.templateName;
                    this.Modal.closeModal();
                    this.Modal.clearModalData();
                } else {
                    this.editDisabled = false;
                    this.errorMessage = this.translatePipe.transform("TemplateNameExist");
                }
            });
    }
}
