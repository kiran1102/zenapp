import { Component, Inject, Input, Injectable, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { StateService } from "@uirouter/core";

import { find } from "lodash";
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getOrgList, getCurrentOrgId } from "oneRedux/reducers/org.reducer";

import { ModalService } from "sharedServices/modal.service";
import { CookieTemplateService } from "cookiesService/cookie-template.service";

import { IStore, IStoreState } from "interfaces/redux.interface";
import { ITemplateModalResolver } from "interfaces/cookies/cc-banner-template.interface";
import { IBannerTemplate, IBannerTemplateCreate } from "interfaces/cookies/cc-banner-template.interface";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { IOrganization } from "interfaces/org.interface";
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "add-template-modal",
    templateUrl: "./add-template-modal.component.html",
})
@Injectable()
export class AddTemplateModalController implements OnInit {
    public modalTitle: string;
    public createTemplateButton: string;
    public cancelButtonText: string;
    public templateNameLabel: string;
    public organizationNameLabel: string;
    public defaultLanguageLabel: string;
    public isLoading = false;
    public templateName = "";
    public orgGroupList: IOrganization[] = [];
    public languageList: ILanguage[] = [];
    public selectedDomain: string;
    public selectedLanguage: ILanguage;
    public selectedOrgGroup: IOrganization;
    public addDisabled = false;
    public templates: IBannerTemplate[] = [];
    public isDuplicateTemplate = false;

    @Input() autoCloseEnabled = true;
    @Input() resolve: ITemplateModalResolver;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private stateService: StateService,
        private Modal: ModalService,
        private cookieTemplateService: CookieTemplateService,
        private translatePipe: TranslatePipe,
        public router: Router,
    ) {}

    ngOnInit(): void {
        const modalData = getModalData(this.store.getState());
        this.cookieTemplateService.getAllLanguagesForTemplate()
            .then((response: ILanguage[]): void => {
                this.languageList = response;
            });
        this.cookieTemplateService.getAllTemplates()
            .then((response: IBannerTemplate[]): void => {
                if (response) {
                    this.templates = response;
                }
            });
        this.modalTitle = modalData.modalTitle;
        this.createTemplateButton =
            modalData.createTemplateButton || this.translatePipe.transform("Create");
        this.cancelButtonText =
            modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.templateNameLabel = modalData.templateNameLabel;
        this.organizationNameLabel = modalData.organizationNameLabel;
        this.defaultLanguageLabel = modalData.defaultLanguageLabel;
        this.selectedDomain = modalData.domain;
        this.orgGroupList = this.getOrgList();
    }

    public getOrgList(): IOrganization[] {
        const selectedOrgGroup = getCurrentOrgId(this.store.getState());
        return getOrgList(selectedOrgGroup)(this.store.getState());
    }

    public selectOrgGroup(selection: IOrganization): void {
        this.selectedOrgGroup = selection;
    }

    public selectLanguage(selection: ILanguage): void {
        this.selectedLanguage = selection;
    }

    public closeModal(): void {
        this.Modal.closeModal();
        this.Modal.clearModalData();
    }

    public setTemplateName(value: string): void {
        this.templateName = value;
    }

    public validateForm = (): boolean => {
        if (!this.templateName || !this.selectedLanguage || !this.selectedOrgGroup) {
            return false;
        }
        return this.selectedLanguage.id && this.selectedOrgGroup.Id.length > 0 && this.templateName.trim().length > 0;
    }

    public createTemplate(): void {
        if (find(this.templates, (template: IBannerTemplate): boolean => template.templateName.toLowerCase() === this.templateName.toLowerCase())) {
            this.isDuplicateTemplate = true;
        } else {
            this.isDuplicateTemplate = false;
            this.addDisabled = true;
            const templateData: IBannerTemplateCreate = {templateName: this.templateName, orgGroupId: this.selectedOrgGroup.Id, defaultLanguage: this.selectedLanguage.id};
            this.cookieTemplateService.addTemplate(templateData)
            .then((response: IBannerTemplate): void => {
                this.addDisabled = false;
                this.Modal.closeModal();
                this.Modal.clearModalData();
                this.stateService.go("zen.app.pia.module.cookiecompliance.views.manage", { templateId: response.templateId });
                this.addDisabled = false;
            });
        }
    }
}
