// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Rxjs
import { Subject } from "rxjs";
import {
    startWith,
    takeUntil,
} from "rxjs/operators";

// 3rd party
import {
    remove,
    uniqBy,
    forEach,
    find,
    differenceBy,
    filter,
} from "lodash";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IPublishTemplateModalResolver,
    IPublishTemplateDomain,
    ITemplateMultipleSelectPayload,
    ITemplateDomainDetails,
} from "interfaces/cookies/publish-template-modal.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { StoreToken } from "tokens/redux-store.token";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieTemplateService } from "cookiesService/cookie-template.service";

@Component({
    selector: "publish-template-modal",
    templateUrl: "./publish-template-modal.component.html",
})
export class PublishTemplateModalController implements OnInit {
    modalTitle: string;
    submitButtonText: string;
    cancelButtonText: string;
    organizationNameLabel: string;
    publishConfirmationText: string;
    templateId: number;
    publishDisabled = false;
    validateForm = false;
    showDefaultDomains = true;
    selectedOption = null;
    selectedOptions: IPublishTemplateDomain[] = [];
    input$ = new Subject();
    destroy$ = new Subject();
    domainList: IPublishTemplateDomain[] = [];
    domainListLookUp: IPublishTemplateDomain[] = [];
    multiSelectEnabled = true;
    autoCloseEnabled = true;
    clearInputOnModelChange = true;
    inputValue: string;
    assignedTemplate: IPublishTemplateDomain[] = [];
    modalData: IPublishTemplateModalResolver;
    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        public modalService: ModalService,
        public webAuditService: WebAuditService,
        public cookieTemplateService: CookieTemplateService,
    ) { }

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.modalData.modalTitle;
        this.publishConfirmationText = this.modalData.publishConfirmationText;
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("Publish");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.templateId = this.modalData.templateId;
        this.input$.pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe(() => this.getDomainList());
    }

    ngOnDestroy(): void {
        this.destroy$.next(true);
        this.destroy$.complete();
    }

    getDomainList(): void {
        this.webAuditService.getWebsiteBanners()
            .then((domains: {domains: ITemplateDomainDetails[]}): void => {
                forEach(domains.domains, (domain: ITemplateDomainDetails): void => {
                    if (!domain.TemplateId && !find(this.domainList, (domainValue: IPublishTemplateDomain): boolean => domainValue.title === domain.DomainName)) {
                        this.domainList.push({
                            id: domain.DomainId,
                            title: domain.DomainName,
                        });
                    }
                });
             });
        this.cookieTemplateService.getAssignDomainByTemplateId(this.templateId)
            .then((templateDomains: any): void => {
                forEach(templateDomains, (value: string, key: number): void => {
                    if (!find(this.selectedOptions, (domainValue: IPublishTemplateDomain): boolean => domainValue.title === value)) {
                        this.assignedTemplate.push({
                            id: key,
                            title: value,
                        });
                    }
                });
                this.assignedTemplate = uniqBy(this.assignedTemplate, "title");
                if (this.assignedTemplate.length > 0) {
                     this.validateForm = true;
                 }
            });
    }

    onInputChange({ key, value }): void {
        if (key === "Enter") { return; }
        this.showDefaultDomains = false;
        this.filterList(uniqBy(this.domainList, "title"), value);
    }

    onModelChange({ currentValue, change, previousValue }: ITemplateMultipleSelectPayload): void {
        if (change) { this.validateForm = true; }
        if (!this.multiSelectEnabled) {
            this.selectedOption = currentValue;
            return;
        }
        if (change && !find(this.assignedTemplate, (domainValue: IPublishTemplateDomain): boolean => domainValue.id === change.id)) {
            this.selectedOptions = currentValue;
            if (!previousValue || currentValue.length > previousValue.length) {
                remove(this.domainList, (domain: IPublishTemplateDomain): boolean => (domain.id === change.id));
            } else {
                this.domainList.push(change);
                if (currentValue.length === 0 && this.assignedTemplate.length === 0) {
                    this.validateForm = false;
                }
            }
            this.domainListLookUp = uniqBy(this.domainList, "title");
        }
    }

    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public publishTemplate(): void {
        this.publishDisabled = true;
        const newlyaddedTemplates = differenceBy(this.selectedOptions, this.assignedTemplate, "id");
        const publishedDomain = {};
        forEach(this.assignedTemplate, (assign: IPublishTemplateDomain): void => {
            publishedDomain[assign.id] = true;
        });

        forEach(newlyaddedTemplates, (templates: IPublishTemplateDomain): void => {
            publishedDomain[templates.id] = false;
        });
        this.cookieTemplateService.publishTemplate(this.templateId, publishedDomain)
            .then((response: boolean): void => {
                this.publishDisabled = false;
                this.modalData.callback();
                this.modalService.closeModal();
                this.modalService.clearModalData();
            });
    }

    private filterList(domains: IPublishTemplateDomain[], stringSearch: string): any {
        this.domainListLookUp = filter(domains, (domain: IPublishTemplateDomain): boolean => (domain.title.search(stringSearch) !== -1));
    }
}
