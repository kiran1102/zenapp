import { Component, Inject, OnInit } from "@angular/core";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";
import { CookieTemplateService } from "cookiesService/cookie-template.service";
import { ModalService } from "sharedServices/modal.service";
import { ILanguage } from "interfaces/cookies/cc-banner-languages.interface";
import { IBannerTemplate, ITemplateAddLanguageModalResolver } from "interfaces/cookies/cc-banner-template.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";
import { CookieConsentService } from "cookiesService/cookie-consent.service";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

@Component({
    selector: "template-new-language",
    templateUrl: "./template-new-language.component.html",
    styles: ["[otFormElement] { margin-bottom: 20px; }"],
})

export class TemplateNewLanguageComponent implements OnInit {
    public templateId: number;
    public saveInProgress = false;
    public templates: IBannerTemplate[] = [];
    public selectedOption: IBannerTemplate;
    public isLoading = false;
    public selectedLanguage: ILanguage;
    public languageList: ILanguage[] = [];
    public templateJsonData: ITemplateJson;
    public modalData: ITemplateAddLanguageModalResolver;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private translatePipe: TranslatePipe,
        public modalService: ModalService,
        public cookieTemplateService: CookieTemplateService,
        public cookieConsentService: CookieConsentService,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.templateId = this.modalData.templateId;
        this.templateJsonData = this.modalData.templateJsonData;
        this.cookieTemplateService.getNotMappedLanguagesByTemplateId(this.templateId).then(
            (response: ILanguage[]): void => {
                this.languageList = response;
            });
    }
    public selectLanguage(selection: ILanguage): void {
        this.selectedLanguage = selection;
    }
    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    public validateForm = (): boolean => {
        if (!this.selectedLanguage) {
            return false;
        }
        return true;
    }
    public addLanguage = (): void => {
        this.isLoading = true;
        this.cookieTemplateService.addNewLanguageTemplate(this.templateId, this.selectedLanguage.id).then(
            (response: boolean): void => {
                if (response) {
                    this.cookieConsentService.getDomainTemplateModel(this.templateId, this.selectedLanguage.id).then(
                        (res: ITemplateJson): void => {
                            if (res) {
                                this.templateJsonData = res;
                                this.isLoading = false;
                                this.modalData.callback(this.selectedLanguage);
                                this.closeModal();
                            }
                        },
                    );
                } else {
                    this.isLoading = false;
                    this.closeModal();
                }
            });
    }
}
