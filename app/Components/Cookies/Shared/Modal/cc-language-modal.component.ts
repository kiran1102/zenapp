import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

import { ModalService } from "sharedServices/modal.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";

import {
    map,
    every,
    findIndex,
    find,
    filter,
    forEach,
    orderBy,
    sortBy,
    cloneDeep,
    intersection,
    sortedUniq,
    isEqual,
} from "lodash";
import {
    getModalData,
} from "oneRedux/reducers/modal.reducer";
import {
    IStore,
} from "interfaces/redux.interface";
import {
    StoreToken,
} from "tokens/redux-store.token";
import {
    IBannerLanguageTableData,
    IBannnerLanguageModalData,
    IBannerLanguageRequestData,
    IBannerLanguageResponse,
    IBannerLanguageRadio,
} from "interfaces/cookies/cc-banner-languages.interface";
import { LanguageRequestMethod } from "enums/cookies/cookie-list.enum";

import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "cc-language-modal",
    templateUrl: "./cc-language-modal.component.html",
})
export class CcLanguageModal implements OnInit {
    public translations;
    public tableColumns;
    public allRowSelected = false;
    public truncateCellContent = true;
    public rowBordered = true;
    public saveDisabled = true;
    public loadingTemplate = true;
    public saveLoading = false;
    public showDeleteLangMsg = false;
    public isDeleteLangChecked = false;
    public radioID: IBannerLanguageRadio;
    public defaultRadioID: IBannerLanguageRadio;
    public tableData: IBannerLanguageTableData[];
    public cloneTableData: IBannerLanguageTableData[];
    public defaultLanguage: number;
    public singleLanguageEnabled: boolean;
    public readonly overflowLength = 8;
    private modalData: IBannnerLanguageModalData;
    private deletedLanguagesData: IBannerLanguageRequestData[];
    private referenceTableData: IBannerLanguageTableData[];
    private defaultLanguageId: number;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private readonly translatePipe: TranslatePipe,
        private readonly modalService: ModalService,
        private readonly cookieMultiLanguageService: CookieMultiLanguageService,
    ) {
        this.translations = {
            save: this.translatePipe.transform("Save"),
            cancel: this.translatePipe.transform("Cancel"),
            success: this.translatePipe.transform("EditLanguageSuccess"),
            failure: this.translatePipe.transform("EditLanguageFailure"),
            selectAll: this.translatePipe.transform("SelectAll"),
            default: this.translatePipe.transform("Default"),
            noLanguageFound: this.translatePipe.transform("NoLanguageFound"),
            deleteMessage: this.translatePipe.transform("DeleteLanguage"),
            search: this.translatePipe.transform("Search") + "...",
        };
    }

    ngOnInit(): void {
        this.tableColumns = [
            {
                columnType: "checkbox",
            },
            {
                columnType: null,
                columnName: this.translations.selectAll,
                columnWidth: null,
                cellValueKey: "textdata",
                cellValueLang: "name",
            },
            {
                columnName: this.translations.default,
                columnType: "radio",
            },
        ];

        this.modalData = getModalData(this.store.getState());
        this.translations.headerTitle = this.modalData.headerTitle;
        this.translations.bodyText = this.modalData.bodyText;
        this.cookieMultiLanguageService.getLanguages(this.modalData.domainId).then((response: IBannerLanguageResponse[]) => {
            this.modalData.masterLanguagesList = response;
            this.afterLoadInit();
        });
    }

    public afterLoadInit() {
        const defaultLanguage = find(this.modalData.masterLanguagesList, "defaultLanguage");
        this.defaultLanguage = defaultLanguage ? defaultLanguage.language.id : null;
        this.defaultLanguageId = +this.defaultLanguage;
        this.tableData = this.buildLanguageData(this.modalData.masterLanguagesList);
        this.tableData = sortBy(this.tableData, "textdata");
        this.cloneTableData = cloneDeep(this.tableData);
        this.referenceTableData = this.cloneTableData.filter((obj) => obj.enabled);
        this.defaultRadioID = {
            id: this.tableData.filter((p) => p.isDefault)[0].id,
            code: this.defaultLanguage,
        };
        this.radioID = { ...this.defaultRadioID };
        this.allRowSelected = every(this.tableData, "enabled");
        this.singleLanguageEnabled = filter(this.tableData, ["enabled", true]).length === 1;
        this.loadingTemplate = false;
    }

    public buildLanguageData(masterLanguagesList: IBannerLanguageResponse[]): IBannerLanguageTableData[] {
        return map(masterLanguagesList, (obj: IBannerLanguageResponse) => {
            return {
                textdata: obj.language.englishName,
                id: obj.language.id,
                enabled: obj.checked,
                code: obj.language.id,
                isDefault: obj.defaultLanguage,
                dirty: obj.checked,
                name: obj.language.name,
                clang: obj.language.culture,
            };
        });
    }

    updateCloneTableData() {
        forEach(filter(this.tableData, "dirty"), (lang: IBannerLanguageTableData): void => {
            const langObject = find(this.cloneTableData, ["textdata", lang.textdata]);
            langObject.enabled = lang.enabled;
            langObject.isDefault = lang.isDefault;
            langObject.dirty = true;
        });
    }

    onSearch(value: string) {
        this.updateCloneTableData();
        this.tableData = filter(this.cloneTableData, (lang: IBannerLanguageTableData): boolean => {
            return (lang.textdata.toLowerCase().indexOf(value.toLowerCase()) === 0 || lang.name.toLowerCase().indexOf(value.toLowerCase()) === 0);
        });
        this.tableData = sortBy(this.tableData, "textdata");
        this.checkForMutations();
    }

    public handleSelect(radio: IBannerLanguageRadio): void {
        this.radioID = radio;
        this.defaultRadioID = { ...this.radioID };
        this.tableData.forEach((elem: IBannerLanguageTableData) => {
            elem.isDefault = false;
        });
        this.cloneTableData.forEach((elem: IBannerLanguageTableData) => {
            elem.isDefault = false;
        });
        const selectedIndex = findIndex(this.tableData, ["code", this.radioID.code]);
        this.tableData[selectedIndex].enabled = true;
        this.tableData[selectedIndex].isDefault = true;
        this.tableData[selectedIndex].dirty = true;
        this.checkForMutations();
        this.toggleSubmitButton();
    }

    public toggleSubmitButton(): void {
        this.updateCloneTableData();
        if (this.showDeleteLangMsg && !this.isDeleteLangChecked) {
            return;
        }
        this.saveDisabled = !this.buildResponseData().length;
    }

    public handleTableSelectClick(actionType: string, code: number): boolean {
        const defaultIndex: number = findIndex(this.tableData, ["code", this.radioID.code]);
        const selectedIndex: number = findIndex(this.tableData, ["code", code]);
        switch (actionType) {
            case "header":
                this.allRowSelected = !this.allRowSelected;
                this.tableData.forEach((elem: IBannerLanguageTableData, index: number) => {
                    if (index !== defaultIndex) {
                        elem.enabled = this.allRowSelected;
                    }
                    elem.dirty = true;
                });
                this.checkForMutations();
                break;
            case "row":
                if (selectedIndex !== defaultIndex) {
                    this.tableData[selectedIndex].enabled = !this.tableData[selectedIndex].enabled;
                    this.tableData[selectedIndex].dirty = true;
                    this.updateCloneTableData();
                    this.checkForMutations();
                    if (!this.buildResponseData().length) {
                        this.saveDisabled = true;
                    } else {
                        this.saveDisabled = this.showDeleteLangMsg ? !this.isDeleteLangChecked : false;
                    }
                } else {
                    this.tableData[selectedIndex].enabled = this.tableData[selectedIndex].enabled;
                    return false;
                }
                break;
        }
        return true;
    }

    public checkForMutations(): void {
        this.allRowSelected = every(this.tableData, "enabled");
        this.singleLanguageEnabled = filter(this.tableData, ["enabled", true]).length === 1;
        this.toggleSaveButton(this.isDeleteLangChecked);
    }

    public toggleDeleteLanguage(): void {
        this.isDeleteLangChecked = !this.isDeleteLangChecked;
        this.checkForMutations();
    }

    public toggleSaveButton(isDeleteLangChecked: boolean): void {
        const defaultCheckedIds = this.referenceTableData.map((e) => e.id);
        const deselectedLangs = this.cloneTableData.filter((e) => !e.enabled).map((e) => e.id);
        const list = intersection(defaultCheckedIds, deselectedLangs);
        const checkedIds = this.cloneTableData.filter((e) => e.enabled).map((e) => e.id);
        const isEqualIds = isEqual(sortedUniq(defaultCheckedIds), sortedUniq(checkedIds));
        if (list.length) {
            this.showDeleteLangMsg = true;
            this.saveDisabled = isDeleteLangChecked ?  false : true;
        } else if (isEqualIds) {
            this.saveDisabled = true;
        } else {
            this.saveDisabled = false;
            this.showDeleteLangMsg = false;
        }
    }

    buildResponseData(): IBannerLanguageRequestData[] {
        const tableData = filter(this.cloneTableData, "enabled");
        const changes = map(tableData, (obj) => {
            return {
                id: obj.id,
                method: LanguageRequestMethod.ADD,
                domainId: this.modalData.domainId,
                culture: obj.clang,
                defaultLanguage: obj.isDefault,
                languageId: obj.id,
            };
        });
        const presentLangIds = changes.map((e) => e.id);
        const defaultCheckedIds = this.referenceTableData.map((e) => e.id);
        const addedLanguagesList = changes.filter((e) => defaultCheckedIds.indexOf(e.id) < 0);
        const addedLanguages = addedLanguagesList.map((language) => {
            language.method = LanguageRequestMethod.ADD;
            return language;
        });

        this.deletedLanguagesData = this.referenceTableData
            .filter((e) => presentLangIds.indexOf(e.id) < 0)
            .map((language) => {
                return {
                    method: LanguageRequestMethod.DELETE,
                    defaultLanguage: false,
                    domainId: this.modalData.domainId,
                    culture: language.clang,
                    languageId: language.id,
                };
            });

        let result: IBannerLanguageRequestData[] = [...addedLanguages, ...this.deletedLanguagesData];
        const currentDefaultLanguage = this.cloneTableData.filter((lang) => lang.isDefault)[0];

        if (this.defaultLanguageId !== currentDefaultLanguage.id &&
            defaultCheckedIds.indexOf(currentDefaultLanguage.id) > -1) {
            result.push({
                id: currentDefaultLanguage.id,
                method: LanguageRequestMethod.UPDATE,
                defaultLanguage: true,
                domainId: this.modalData.domainId,
                culture: currentDefaultLanguage.clang,
                languageId: currentDefaultLanguage.id,
            });
        }
        result = orderBy(result, "defaultLanguage", ["desc"])
            .map((language) => {
                delete language.id;
                return language;
            });
        return result;
    }

    public onSave(): void {
        this.saveLoading = true;
        this.updateCloneTableData();
        const changes: IBannerLanguageRequestData[] = this.buildResponseData();
        this.cookieMultiLanguageService
            .addDomainLanguage(this.modalData.domainId, changes)
            .then((res: boolean) => {
                this.saveLoading = false;
                if (res) {
                    this.closeModal();
                    this.modalData.onModalClose(this.deletedLanguagesData.length);
                }
            });
    }

    public closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
