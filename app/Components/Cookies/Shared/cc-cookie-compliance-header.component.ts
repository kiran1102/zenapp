// Rxjs
import {
    Subject,
    timer,
} from "rxjs";
import {
    startWith,
    takeUntil,
    takeWhile,
    take,
} from "rxjs/operators";

declare var OneConfirm: any;
// 3rd party
import {
    cloneDeep,
    find,
} from "lodash";

// Services
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { CookieConsentService } from "cookiesService/cookie-consent.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import CookieScriptArchiveService from "cookiesService/cookie-script-archive.service";
import { CookieLanguageSwitcherService } from "app/scripts/Cookies/Services/cookie-language-switcher.service";
import { CookieApiService } from "modules/cookies/shared/services/cookie-api.service";
import { ModalService } from "sharedServices/modal.service";
import { CookiesStorageService } from "modules/cookies/services/helper/cookies-storage.service";
import { McmModalService } from "modules/mcm/mcm-modal.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";
import {
    IBannerLanguage,
    IBannerLangReponse,
} from "interfaces/cookies/cc-banner-languages.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import {
    IPublishScriptPayload,
    IJqueryVersionDetail,
    ICookieLimitResponse,
} from "interfaces/cookies/cookie-publish-script.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IPublishScriptModalResolver } from "interfaces/cookies/publish-script-modal-resolver.interface";
import { IStringMap } from "interfaces/generic.interface";

import { getPreferenceCenterDisabledLanguagesStatus } from "oneRedux/reducers/preference-center.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Enums
import { CookiePageName } from "enums/cookies/cookie-list.enum";
import { LicenseType } from "enums/cookies/cookie-limit.enum";

class CcCookieComplianceHeader implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "WebAuditService",
        "CookieConsentService",
        "Permissions",
        "Upgrade",
        "CookieMultiLanguageService",
        "$state",
        "CookieScriptArchiveService",
        "CookieLanguageSwitcherService",
        "ModalService",
        "store",
        "CookieApiService",
        "CookiesStorageService",
        "McmModalService",
    ];

    bannerLanguages: IBannerLanguage[];
    isPublishDisable = false;
    scriptPublishInProgress = false;
    showLoaderOnPublish = false;
    restoreCookieListConfiguration: () => void;
    selectedLanguage: IBannerLanguage;
    showPublishButton: boolean;
    isDomainDisabled: boolean;
    translate = this.$rootScope.t;
    permissionsObj: IStringMap<boolean>;
    initialPublishCall = true;
    canPublishDomain = this.permissions.canShow("CookiePublishDomain");

    private languageSwitcherEnabled: boolean;
    private geoLocationSwitcherEnabled: boolean;
    private selectedWebsite: string;
    private domainSelectionList: any;
    private domainId: number;
    private isUnpublishedChanges: boolean;
    private pageReady: boolean;
    private navigationTitle: string;
    private domainChangeFunction: any;
    private pageName: string;
    private isScriptArchiveFilterOpen: boolean;
    private doScriptArchiveFilterForDomain: boolean;
    private scriptArchiveDownloadInProgress = false;
    private pageReadyCallback: (isPageReady: { isPageReady: boolean }) => void;
    private options: IDropdownOption[] = [];
    private domainJqueryVersion: IJqueryVersionDetail;
    private destroy$ = new Subject();
    private canShowVersionDiff = this.permissions.canShow("CookieVersionDiffNotifications");

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly webAuditService: WebAuditService,
        readonly cookieConsentService: CookieConsentService,
        readonly permissions: Permissions,
        readonly Upgrade: any,
        readonly cookieMultiLanguageService: CookieMultiLanguageService,
        readonly $state: ng.ui.IStateService,
        readonly cookieScriptArchiveService: CookieScriptArchiveService,
        readonly cookieLanguageSwitcherService: CookieLanguageSwitcherService,
        private readonly modalService: ModalService,
        private store: IStore,
        private cookieApiService: CookieApiService,
        private cookiesStorageService: CookiesStorageService,
        private mcmModalService: McmModalService,
    ) { }

    $onInit(): void {
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        this.domainSelectionList = this.webAuditService.validDomainFilter(
            this.cookieComplianceStore.getWebsiteList(),
            this.doScriptArchiveFilterForDomain ? this.doScriptArchiveFilterForDomain : false,
            this.selectedWebsite,
        );
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.permissionsObj = {
            settingsCookieAdvanced: this.permissions.canShow("SettingsCookieAdvanced"),
            settingsCookieAdvancedUpgrade: this.permissions.canShow("SettingsCookieAdvancedUpgrade"),
            showScriptArchiveExport: this.permissions.canShow("CookieScriptArchiveExport"),
        };
        this.options = [{
        }, {
            text: this.translate("Restore"),
            action: (): void => {
                this.restoreCookieListConfiguration();
            },
        }];

        if (this.permissions.canShow("CookieIABIntegration")) {
            this.options.push({
                text: this.translate("WhitelistedVendors"),
                action: (): void => {
                    this.showDownloadPubVendorModal();
                },
            });
        }
        this.getBannerLanguages();
        const storeState = this.store.getState();
        this.isDomainDisabled = getPreferenceCenterDisabledLanguagesStatus(storeState);
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
            takeUntil(this.destroy$),
        ).subscribe((state: IStoreState): void =>
            this.componentWillReceiveState(state),
        );
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.showPublishButton) {
            this.showPublishButton = changes.showPublishButton.currentValue;
            if (this.showPublishButton) {
                this.getPublishStatus();
            }
        }
    }

    $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public showDownloadPubVendorModal(): void {
        const modalData: ISaveChangesModalResolve = {
            modalTitle: this.$rootScope.t("DownloadPubVendors"),
            confirmationText: this.$rootScope.t("DownloadPubVendorsMessage"),
            cancelButtonText: this.translate("Cancel"),
            discardChangesButtonText: this.translate("Discard"),
            showDiscard: false,
            saveButtonText: this.translate("Confirm"),
            promiseToResolve: (): ng.IPromise<boolean> => {
                this.scriptArchiveDownloadInProgress = true;
                return this.cookieScriptArchiveService.exportPubVendors(this.domainId).then((): boolean => {
                    this.scriptArchiveDownloadInProgress = false;
                    return true;
                });
            },
            discardCallback: (): void => { },
            closeCallback: (): void => { },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("saveChangesModal");
    }

    public filterByDomain = (selectedWebsite: string): void => {
        this.selectedWebsite = selectedWebsite;
        this.cookieComplianceStore.setSelectedWebsite(selectedWebsite);
        this.$rootScope.$emit("ccSetSelectedwebsite", selectedWebsite);
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        if (this.checkBannerIsCreated()) {
            this.domainChangeFunction();
            this.getBannerLanguages();
            this.initialPublishCall = true;
            this.getPublishStatus();
        }
    }
    public updateDomainId = (): void => {
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
    }
    public launchAdvancedSettings(): void {
        this.updateDomainId();
        this.webAuditService.launchCookieCompliance(this.domainId, true, false);
    }
    public getLanguageCulture = (): string => {
        this.updateDomainId();
        const banner = find(this.bannerLanguages, (e: IBannerLanguage): boolean => this.domainId === e.domainId);
        return banner.language.culture;
    }
    public upgradeModule(): void {
        // TODO: SettingsCookieAdvancedUpgrade
        this.mcmModalService.openMcmModal("Upgrade");
    }

    checkPublishPermissions() {
        this.showLoaderOnPublish = true;
        this.cookieApiService.getPublishLimit(this.domainId)
            .then((response: IProtocolResponse<ICookieLimitResponse>) => {
                this.showLoaderOnPublish = false;
                if (!response.result) {
                    return;
                }
                if (response.data.Publish) {
                    this.publish();
                } else {
                    const upgradeLink = response.data.LicenseType !== LicenseType.COOKIEPRO ?
                        response.data.UpgradeOther : response.data.UpgradeEcommerce;
                    this.modalService.setModalData({
                        isAdmin: response.data.IsAdmin,
                        upgradeLink,
                    });
                    this.modalService.openModal("cookieLimitModal");
                }
            });
    }

    publish(): void {
        if (!this.selectedWebsite) return;
        this.updateDomainId();
        if (this.canShowVersionDiff) {
            this.showLoaderOnPublish = true;
            const promises = [];
            promises.push(this.cookieApiService.getVersionDiff(this.domainId));
            promises.push(this.cookieApiService.getJqueryVersions(this.domainId));
            Promise.all(promises).then((response: any[]): void => {
                this.showLoaderOnPublish = false;
                const modalData: IPublishScriptModalResolver = {
                    bannerLanguages: cloneDeep(this.bannerLanguages),
                    currentDomainId: this.domainId,
                    jqueryVersions: response[1].result ? response[1].data : [],
                    scriptData: response[0].result ? response[0].data : null,
                    promiseToResolve: (
                        requireReconsent: boolean,
                        jqueryVersionId: number,
                        domainIds: number[]): void => this.publishModalCallback(
                            requireReconsent,
                            jqueryVersionId,
                            domainIds),
                };
                this.modalService.setModalData(modalData);
                this.modalService.openModal("downgradeCookiePublishScriptModal");
            });
        } else {
            this.showLoaderOnPublish = true;
            this.cookieApiService.getDomainJqueryVersion(this.domainId)
                .then((response: IProtocolResponse<IJqueryVersionDetail>): void => {
                    this.domainJqueryVersion = response.data;
                    this.showLoaderOnPublish = false;
                    OneConfirm(this.translate("Publish"), this.translate("DoYouRequireUsersToReConsent"),
                        "Confirm", "Yes", true, this.translate("Yes"), this.translate("No"))
                        .then((requireReconsent: boolean): void => {
                            this.publishModalCallback(requireReconsent, this.domainJqueryVersion.jqueryVersionId);
                        });
                });
        }
    }

    public toggleFilters(): void {
        this.isScriptArchiveFilterOpen = !this.isScriptArchiveFilterOpen;
    }

    public exportScriptArchiveList(): void {
        this.scriptArchiveDownloadInProgress = true;
        this.cookieScriptArchiveService.exportScriptArchiveList(this.domainId).then((): void => {
            this.scriptArchiveDownloadInProgress = false;
        });
    }

    updateBannerLanguageList(languages: IBannerLanguage[]): void {
        this.bannerLanguages = languages;
    }

    public changeDomain = (): void => {
        this.domainChangeFunction();
    }

    public toggleDomainDisable = (value: boolean): void => {
        this.isDomainDisabled = value;
    }

    private getPublishStatus(initialDelay: number = 0): void {
        if (!this.canShowVersionDiff) return;
        this.scriptPublishInProgress = true;
        if (this.domainId === 0) {
            return;
        }
        timer(initialDelay, 20000).pipe(
            take(5),
            takeWhile(() => this.scriptPublishInProgress),
            takeUntil(this.destroy$),
        )
            .subscribe(() => {
                this.cookieApiService.getPublishStatus(this.domainId)
                    .then((data) => {
                        if (this.initialPublishCall) {
                            this.initialPublishCall = false;
                        }
                        this.scriptPublishInProgress = data.data.Disable;
                    });
            });
    }

    private publishBanner(payload: IPublishScriptPayload): void {
        if (this.pageName === CookiePageName[CookiePageName.ccBannerPage]) {
            this.pageReadyCallback({ isPageReady: false });
        } else {
            this.pageReady = false;
        }
        this.isPublishDisable = true;
        this.webAuditService.publish(payload)
            .then((response: any): void => {
                this.isPublishDisable = false;
                if (response.result) {
                    this.isUnpublishedChanges = false;
                    if (this.pageName === CookiePageName[CookiePageName.ccBannerPage]) {
                        this.pageReadyCallback({ isPageReady: true });
                    } else {
                        this.pageReady = true;
                    }
                    this.$rootScope.$emit("ccSetArchiveFlagTrue");
                    if (this.canShowVersionDiff) {
                        this.getPublishStatus(10000);
                    }
                }
            });
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.isDomainDisabled = getPreferenceCenterDisabledLanguagesStatus(state);
    }

    private publishGeoLocationSwitcherChanges(): void {
        this.isPublishDisable = true;
        this.pageReady = false;
        this.cookieLanguageSwitcherService.publishGeoLocationSwitcherChanges(this.domainId)
            .then((): void => {
                this.pageReady = true;
                this.isPublishDisable = false;
            });
    }

    private publishLanguageLoader(): void {
        this.isPublishDisable = true;
        this.pageReady = false;
        this.cookieLanguageSwitcherService.publishLanguageLoader(this.domainId)
            .then((): void => {
                this.pageReady = true;
                this.isPublishDisable = false;
            });
    }

    private checkBannerIsCreated(): boolean {
        if (this.domainId === 0 && (this.pageName === CookiePageName[CookiePageName.ccPolicyPage] || this.pageName === CookiePageName[CookiePageName.ccPreferenceCenter])) {
            this.$rootScope.$emit("ccInitCookieData", [this.selectedWebsite, this.pageName]);
            return false;
        }
        return true;
    }

    private getBannerLanguages(): void {
        if (this.domainId !== 0) {
            this.cookieMultiLanguageService.getBannerLanguages(this.domainId)
                .then((response: IBannerLangReponse): void => {
                    this.bannerLanguages = response.languages;
                    const domainId = this.cookieComplianceStore.getSelectedDomainId();
                    this.selectedLanguage = this.bannerLanguages.filter((e) => e.domainId === domainId)[0];
                    this.cookiesStorageService.selectedLanguage.next(this.selectedLanguage);
                });
        }
    }

    private publishModalCallback(requireReconsent: boolean, jqueryVersionId: number, domainIds: number[] = []): void {
        if (requireReconsent === null) return;
        const payload: IPublishScriptPayload = {
            DomainIds: domainIds.length ? domainIds : [this.domainId],
            RequireReConsent: requireReconsent,
            JQueryVersionId: jqueryVersionId,
        };
        if (this.canShowVersionDiff) {
            this.publishBanner(payload);
            return;
        }
        if (this.pageName === CookiePageName[CookiePageName.ccScriptIntegration]) {
            if (this.languageSwitcherEnabled) {
                this.publishLanguageLoader();
            } else if (this.geoLocationSwitcherEnabled) {
                this.publishGeoLocationSwitcherChanges();
            } else {
                this.publishBanner(payload);
            }
        } else {
            this.publishBanner(payload);
        }
    }
}

const ccCookieComplianceHeader: ng.IComponentOptions = {
    bindings: {
        navigationTitle: "@",
        pageName: "@",
        domainChangeFunction: "&",
        isUnpublishedChanges: "=",
        pageReady: "=?",
        pageReadyCallback: "&?",
        showAdvanceSettingsButton: "<",
        showPublishButton: "<",
        showLanguageSelection: "<",
        showScriptArchiveControls: "<?",
        isScriptArchiveFilterOpen: "=?",
        doScriptArchiveFilterForDomain: "<?",
        languageSwitcherEnabled: "<?",
        geoLocationSwitcherEnabled: "<?",
        hideLanguageAddButton: "<?",
        useTemplate: "<?",
        restoreCookieListConfiguration: "&?",
    },
    controller: CcCookieComplianceHeader,
    template: `
        <one-header class="cc-cookie-compliance-header">
            <header-title>
				{{::$ctrl.navigationTitle}} /
			</header-title>
			<header-body>
				<div class="row-flex-start">
                    <one-select
                        class="cc-cookie-compliance-header__websiteList margin-left-1"
                        name="{{::$ctrl.translate('Status')}}"
                        options="$ctrl.domainSelectionList"
                        model="$ctrl.selectedWebsite"
                        is-disabled="$ctrl.isDomainDisabled"
                        on-change="$ctrl.filterByDomain(selection)"
                        label-key="auditDomain"
                        value-key="auditDomain"
                        select-class="height-38"
                    >
                    </one-select>
                    <cc-language-selection-component
                        class="cc-language-seletion__languageList row-flex-start margin-left-1"
                        *ngIf="$ctrl.showLanguageSelection && $ctrl.bannerLanguages.length"
                        [languages]="$ctrl.bannerLanguages"
                        [is-disabled]="$ctrl.isDomainDisabled"
                        [show-loading]="$ctrl.isDomainDisabled"
                        [selected-language]="$ctrl.selectedLanguage"
                        [hide-add-new-button]="$ctrl.hideLanguageAddButton"
                        (update-banner-language-list)="$ctrl.updateBannerLanguageList($event)"
                        (change-domain)="$ctrl.changeDomain()"
                        (disable-domain)="$ctrl.toggleDomainDisable($event)"
                    >
                    </cc-language-selection-component>
				</div>
			</header-body>
            <header-actions>
                <span
                    ng-if="$ctrl.scriptPublishInProgress && !$ctrl.initialPublishCall && $ctrl.canPublishDomain"
                    class="text-bold text-small"
                >
                    <i class="ot ot-spinner margin-right-1"></i>
                    {{$ctrl.translate('ScriptPublishInProgressWarning')}}
                </span>
				<one-button
					ng-if="$ctrl.showPublishButton && !$ctrl.useTemplate && $ctrl.canPublishDomain"
					type="primary"
					is-rounded="true"
					text="{{::$ctrl.translate('Publish')}}"
					identifier="cookiesPublish"
					class="cc-cookie-compliance-header__publish-button margin-left-1"
                    is-disabled="$ctrl.isPublishDisable"
                    enable-loading="true"
                    is-loading="$ctrl.showLoaderOnPublish"
					button-click="$ctrl.checkPublishPermissions()"
					>
				</one-button>
				<one-dropdown
					ng-if="$ctrl.pageName === 'ccPolicyPage'"
					class="margin-left-1"
					round-button="true"
					icon="ot ot-ellipsis-h"
					hide-caret="true"
					align-right="true"
					options="$ctrl.options"
				</one-dropdown>
				<one-button
					ng-if="::($ctrl.permissionsObj.settingsCookieAdvanced && $ctrl.showAdvanceSettingsButton)"
					text="{{::$ctrl.translate('AdvancedSettings')}}"
					icon="fa fa-external-link"
					button-click="$ctrl.launchAdvancedSettings()"
					identifier="cookiesAdvancedSettings"
					class="cc-cookie-compliance-header__advanced-settings-button margin-left-1"
					>
				</one-button>
				<one-button
					ng-if="::($ctrl.permissionsObj.settingsCookieAdvancedUpgrade && $ctrl.showAdvanceSettingsButton)"
					text="{{::$ctrl.translate('AdvancedSettings')}}"
					icon="fa fa-external-link"
					right-side-icon="upgrade__icon",
					button-click="$ctrl.upgradeModule()"
					identifier="cookiesAdvancedSettingsUpgrade"
					class="cc-cookie-compliance-header__advanced-settings-button margin-left-1"
					>
				</one-button>
				<one-button
					ng-if="::($ctrl.showScriptArchiveControls && $ctrl.permissionsObj.showScriptArchiveExport)"
					identifier="scriptArchiveExportToCSV"
					icon='ot ot-download'
					class="margin-left-1"
					enable-loading="true"
					text="{{::$root.t('ExportCSV')}}"
					button-click="$ctrl.exportScriptArchiveList()"
					is-disabled="$ctrl.scriptArchiveDownloadInProgress"
					is-loading="$ctrl.scriptArchiveDownloadInProgress"
				>
				</one-button>
				<one-button
					ng-if="$ctrl.showScriptArchiveControls"
					identifier="scriptArchiveTableHeaderToggleFilters"
					text="{{::$root.t('Filters')}}"
					button-click='$ctrl.toggleFilters()'
					icon='ot ot-search'
					class="margin-left-1"
                    >
				</one-button>
            </header-actions>
        </one-header>
    `,
};

export default ccCookieComplianceHeader;
