import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output,
} from "@angular/core";
import { StateService } from "@uirouter/core";
import { findIndex, filter } from "lodash";
import { CookieScanStatus, ScanBehindLoginStatus } from "constants/cookie.constant";
import {
    IDomain,
    IDomainColumns,
    IDomainSort,
} from "interfaces/cookies/cc-domain.interface";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IAssignTemplateModalResolver } from "interfaces/cookies/cc-banner-template.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

import { ModalService } from "sharedServices/modal.service";
import { WebAuditService } from "cookiesService/web-audit.service";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { StatusCodes } from "enums/status-codes.enum";

@Component({
    selector: "website-list-table",
    templateUrl: "./website-list-table.component.html",
})
export class WebsiteListTableComponent {
    public columns: IDomainColumns[] = [];
    public rows: IDomain[] = [];
    public ScanStatus: any = CookieScanStatus;
    public sortable = true;
    public menuClass: string;

    @Input() domains;
    @Input() cookiePolicyPermission;
    @Input() preferenceCenterPermission;
    @Input() auditSchedulePermission;
    @Input() tenantLevelAuditSchedulePermission;
    @Input() cookieOrgGroupAssignmentPermission;
    @Input() cookieScanBehindLoginPermission;
    @Input() cookieBannerTemplatePermission;
    @Input() cookieDeleteWebsitePermission;
    @Input() sortOrder;
    @Input() sortKey;
    @Input() rowFlagKey: string;
    @Output() goToReports = new EventEmitter();
    @Output() createConsentBanner = new EventEmitter();
    @Output() manageConsentBanner = new EventEmitter();
    @Output() manageCookiePolicy = new EventEmitter();
    @Output() managePreferenceCenter = new EventEmitter();
    @Output() setOrUpdateNextScan = new EventEmitter();
    @Output() reAuditCallback = new EventEmitter();
    @Output() loginCallback = new EventEmitter();
    @Output() deleteScanCallback = new EventEmitter();
    @Output() reAssignCallback = new EventEmitter();
    @Output() stopScanCallback = new EventEmitter();
    @Output() sortSearchCallback = new EventEmitter();
    @Output() reloadWebsiteListCallback = new EventEmitter();

    readonly translations = {
        notStarted: this.translatePipe.transform("NotStarted"),
        notSet: this.translatePipe.transform("NotSet"),
        manage: this.translatePipe.transform("Manage"),
        scanPending: this.translatePipe.transform("ScanPending"),
        inProgress: this.translatePipe.transform("InProgress"),
        completed: this.translatePipe.transform("Completed"),
        cancelled: this.translatePipe.transform("Cancelled"),
        scanError: this.translatePipe.transform("ScanError"),
        failedLogin: this.translatePipe.transform("FailedLogin"),
    };

    constructor(
        private readonly translatePipe: TranslatePipe,
        private stateService: StateService,
        public modalService: ModalService,
        public webAuditService: WebAuditService,
    ) { }

    ngOnInit(): void {
        this.columns = [
            {columnHeader: this.translatePipe.transform("Domain"), columnTitleKey: "domainTitle", cellValueKey: "auditDomain"},
            {columnHeader: this.translatePipe.transform("ScanResults"), columnTitleKey: "scanResultsLink", cellValueKey: null},
            {columnHeader: this.translatePipe.transform("Pages"), columnTitleKey: "numberPages", cellValueKey: "auditedPages"},
            {columnHeader: this.translatePipe.transform("UniqueCookies"), columnTitleKey: "cookiesCount", cellValueKey: "cookiesFound"},
            {columnHeader: this.translatePipe.transform("CookiesChange"), columnTitleKey: "cookiesChange", cellValueKey: null},
            {columnHeader: this.translatePipe.transform("Banner"), columnTitleKey: "bannerLink", cellValueKey: null},
            {columnHeader: this.translatePipe.transform("CookiePolicy"), columnTitleKey: "cookiePolicyLink", cellValueKey: null},
            {columnHeader: this.translatePipe.transform("PrefCenter"), columnTitleKey: "preferenceCenterLink", cellValueKey: null},
            {columnHeader: this.translatePipe.transform("NextScan"), columnTitleKey: "nextScanDate", cellValueKey: "scheduledDateOfNextScan"},
            {columnHeader: this.translatePipe.transform("Status"), columnTitleKey: "statusText", cellValueKey: "status"},
            {columnHeader: this.translatePipe.transform("Actions"), columnTitleKey: "actionMenu", cellValueKey: null},
            ];
        }

    ngOnChanges(): void {
        this.rows = this.filterDomains(JSON.parse(this.domains));
    }

    public assignTemplate = (domainId: number): void => {
        const modalData: IAssignTemplateModalResolver = {
            domainId,
            callback: (): void => {
                this.reloadWebsiteListCallback.emit(true);
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("assignTemplate");
    }

    filterDomains(domainList: IDomain[]): IDomain[] {
        return filter(domainList, (domain: IDomain): boolean => domain.auditDomain !== null);
    }

    isNewDomain = (domain: IDomain): boolean => (domain.status === CookieScanStatus.Complete && domain.isNew);

    calculateProgress = (domain: IDomain): string => {
        const upperLimit: number = domain.auditedPages < 50 ? Math.min(100, domain.pageLimit)
            : domain.auditedPages < 250 ? Math.min(500, domain.pageLimit)
                : domain.pageLimit;
        return (domain.auditedPages / upperLimit * 100) + "%";
    }

    canShowCompletedMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete) && domain.scanError === "" && domain.scanLoginInfoStatus !== ScanBehindLoginStatus.Failed);
    }

    canShowCancelledMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Cancelled) && domain.scanError === "" && domain.scanLoginInfoStatus !== ScanBehindLoginStatus.Failed);
    }

    canShowScanErrorMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled) && domain.scanError !== "");
    }

    canShowLoginFailedMessage = (domain: IDomain): boolean => {
        return ((domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled) && (domain.scanError === "" && domain.scanLoginInfoStatus === ScanBehindLoginStatus.Failed));
    }

    isDomainReadyForCreateBanner = (domain: IDomain): boolean => {
        return (!domain.hasBanner && domain.scanError === "" && domain.domainReadyForBanner && domain.status === CookieScanStatus.Complete);
    }

    isDomainReadyForManageBanner = (domain: IDomain): boolean => {
        return (domain.hasBanner && domain.scanError === "" && domain.domainReadyForBanner);
    }

    isDomainWaitingForScan = (domain: IDomain): boolean => {
        return (domain.scanError === "" && !domain.domainReadyForBanner);
    }

    isDomainContainScanError = (domain: IDomain): boolean => {
        return (domain.scanError !== "");
    }

    isTemplateAvailable = (domain: IDomain): boolean => {
        return (domain.hasBanner && domain.templateId !== 0 && domain.templateName !== "");
    }

    viewResultsWithoutLink = (domain: IDomain): boolean => {
        if ((domain.status === this.ScanStatus.Pending || domain.status === this.ScanStatus.InProgress) && domain.lastSuccessfulScanRowKey === null) {
            return true;
        } else if ((domain.status === this.ScanStatus.Complete || domain.status === this.ScanStatus.Cancelled) && (!domain.lastScanDate || domain.scanError !== "")) {
            return true;
        } else {
            return false;
        }
    }

    deleteWebsite(domain) {
        const modalData: IDeleteConfirmationModalResolve = {
            modalTitle: this.translatePipe.transform("ContentWillBeDeleted"),
            confirmationText: this.translatePipe.transform("DeleteWebsiteMessage"),
            cancelButtonText: this.translatePipe.transform("Cancel"),
            submitButtonText: this.translatePipe.transform("Delete"),
            promiseToResolve: (): any => {
                const promises = [];
                promises.push(this.webAuditService.deleteWebsite(domain.auditDomain));
                promises.push(this.webAuditService.deleteCompleteWebsite(domain.domainId));
                return Promise.all(promises).then((response: any[]) => {
                    if (response[0].status === StatusCodes.Success &&
                        response[1].status === StatusCodes.Success) {
                        this.deleteScanCallback.emit();
                        return true;
                    }
                    return false;
                });
            },
        };
        this.modalService.setModalData(modalData);
        this.modalService.openModal("cookieDeleteConfirmationComponent");
    }

    viewResultsLinkToHistory = (domain: IDomain): boolean => {
        return ((domain.status === this.ScanStatus.InProgress || domain.status === this.ScanStatus.Pending) && domain.lastSuccessfulScanRowKey !== null);
    }

    viewResultsLinkToLatestResults = (domain: IDomain): boolean => {
        return ((domain.status === this.ScanStatus.Complete || domain.status === this.ScanStatus.Cancelled) && domain.lastScanDate && domain.scanError === "");
    }

    navigateToPage = (columnTitleKey: string, row: IDomain): void => {
        if (this.isTemplateAvailable(row) && this.cookieBannerTemplatePermission === "true" && (columnTitleKey === "bannerLink" || columnTitleKey === "preferenceCenterLink")) {
            this.stateService.go("zen.app.pia.module.cookiecompliance.views.manage", { templateId: row.templateId });
        } else {
            switch (columnTitleKey) {
                case "bannerLink":
                    this.manageConsentBanner.emit(row.auditDomain);
                    break;
                case "cookiePolicyLink":
                    this.manageCookiePolicy.emit(row.auditDomain);
                    break;
                case "preferenceCenterLink":
                    this.managePreferenceCenter.emit(row.auditDomain);
                    break;
            }
        }
    }

    isScanComplete = (domain: IDomain): boolean => (domain.status === CookieScanStatus.Complete || domain.status === CookieScanStatus.Cancelled);

    canStopScan = (domain: IDomain): boolean => (domain.status === CookieScanStatus.Pending || domain.status === CookieScanStatus.InProgress);

    formatCookiesChange = (domain: IDomain): string => {
        const changeBy: number = domain.cookiesFound - domain.lastCookiesFound;
        return changeBy > 0 ? `+${changeBy}` : changeBy.toString();
    }

    getLoginIconColor(domain: IDomain): string {
        const scanLoginStatus: string = domain.scanLoginInfoStatus;
        if (scanLoginStatus && this.isScanComplete(domain)) {
            if (scanLoginStatus === ScanBehindLoginStatus.Verified) {
                return "cc-scan-results__login-icon-verified";
            } else if (scanLoginStatus === ScanBehindLoginStatus.NotVerified) {
                return "cc-scan-results__login-icon-not-verified";
            } else if (scanLoginStatus === ScanBehindLoginStatus.Failed) {
                return "cc-scan-results__login-icon-failed";
            } else if (scanLoginStatus === ScanBehindLoginStatus.NotAvailable) {
                return "cc-scan-results__login-icon-not-created";
            }
        }
        return "cc-scan-results__login-icon-not-created";
    }

    public getEllipsisOptions(domain: IDomain): () => IDropdownOption[] {
        const canReassignOrgGroup: boolean = this.cookieOrgGroupAssignmentPermission === "true" && this.isScanComplete(domain);
        const canAssignTemplate: boolean = this.isScanComplete(domain) && this.cookieBannerTemplatePermission === "true" && domain.domainId !== 0;
        const canLoginForm: boolean = this.cookieScanBehindLoginPermission === "true" && this.isScanComplete(domain);
        return (): IDropdownOption[] => {
            this.setMenuClass(domain);
            const options = [
                {
                    text: this.translatePipe.transform("ReScan"),
                    action: (): void => {
                        this.reAuditCallback.emit(domain.auditDomain);
                    },
                    isDisabled: !this.isScanComplete(domain),
                },
                {
                    text: this.translatePipe.transform("ReAssign"),
                    action: (): void => {
                        this.reAssignCallback.emit(domain);
                    },
                    isDisabled: !canReassignOrgGroup,
                },
                {
                    text: this.translatePipe.transform("AssignTemplate"),
                    action: (): void => {
                        this.assignTemplate(domain.domainId);
                    },
                    isDisabled: !canAssignTemplate,
                },
                {
                    text: this.translatePipe.transform("Log-in"),
                    action: (): void => {
                        const isExisting: boolean = (domain.scanLoginInfoStatus === ScanBehindLoginStatus.Verified
                            || domain.scanLoginInfoStatus === ScanBehindLoginStatus.NotVerified
                            || domain.scanLoginInfoStatus === ScanBehindLoginStatus.Failed);
                        this.stateService.go("zen.app.pia.module.cookiecompliance.views.scanlogin", { domain: decodeURIComponent(domain.auditDomain), isNew: !isExisting });
                    },
                    isDisabled: !canLoginForm,
                },
                {
                    text: this.translatePipe.transform("Schedule"),
                    action: (): void => {
                        this.setOrUpdateNextScan.emit(domain.auditDomain);
                    },
                    isDisabled: (this.auditSchedulePermission !== "true" && this.tenantLevelAuditSchedulePermission !== "true"),
                },
                {
                    text: this.translatePipe.transform("Stop"),
                    action: (): void => {
                        this.stopScanCallback.emit(domain.auditDomain);
                    },
                    isDisabled: !this.canStopScan(domain),
                }];
            if (this.cookieDeleteWebsitePermission === "true") {
                options.push({
                    text: this.translatePipe.transform("Delete"),
                    action: (): void => {
                        this.deleteWebsite(domain);
                    },
                    isDisabled: !this.isScanComplete(domain),
                });
            }

            if (!this.canShowLoginFailedMessage(domain) && domain.status !== CookieScanStatus.Pending) {
                options.push({
                    text: this.translatePipe.transform("ExportScanResult"),
                    action: (): void => {
                        this.webAuditService.exportScanResultsByScannedDate(domain.auditDomain);
                    },
                    isDisabled: false,
                });
            }
            return options;
        };
    }

    public handleSortChange(event: IDomainSort): void {
        this.sortSearchCallback.emit(event);
    }

    public navigateToReports(domain: IDomain): void {
        if (domain.isThirdPartyBannerEnabled && !domain.isBannerWarningClicked) {
          if (this.webAuditService.updateThirdPrtyBannerNotificationStatus(domain.auditDomain, true, false).then()) {
            this.reloadWebsiteListCallback.emit(true);
          }
        }
        if (domain.lastSuccessfulScanRowKey) {
            this.goToReports.emit({domain: domain.auditDomain, selectedRowKey: domain.lastSuccessfulScanRowKey, reScanTrigger: true});
        } else {
            this.goToReports.emit(domain.auditDomain);
        }
    }

    private setMenuClass(domain: IDomain): void | undefined {
        this.menuClass = "actions-table__context-list";
        if (this.rows.length <= 10) return;
        const halfwayPoint: number = (this.rows.length - 1) / 2;
        const rowIndex: number = findIndex(this.rows, (item: IDomain): boolean => domain.domainId === item.domainId);
        if (rowIndex > halfwayPoint) this.menuClass += " actions-table__context-list--above";
    }

}
