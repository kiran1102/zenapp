
const ccToggleSwitch = {
    bindings: {
        id: "@",
        name: "@",
        toggleState: "=",
    },
    template: (): string => {
        return `<label class="cc-toggle">
							<input class="cc-toggle__input"
								type="checkbox"
								name="$ctrl.name"
								id="$ctrl.id"
								ng-model="$ctrl.toggleState">
							</input>
							<span class="cc-toggle__slider cc-toggle__slider--round"></span>
						</label>`;
    },
};

export default ccToggleSwitch;
