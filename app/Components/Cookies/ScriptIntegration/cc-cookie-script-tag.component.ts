import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class CcCookieScriptTag implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private translate: any;
    private title: string;
    private scriptUrl: string;
    private commentTextStart: string;
    private commentTextEnd: string;
    private referenceType: string;
    private domain: string;
    private language: string;

    constructor(readonly $rootScope: IExtendedRootScopeService) {}

    public $onInit(): void {
        this.translate = this.$rootScope.t;
        if (this.referenceType && this.domain && this.language) {
            this.commentTextStart = this.translate("OptanonConsentNoticeByTypeStart", {type: this.referenceType, domain: this.domain, language: this.language });
            this.commentTextEnd = this.translate("OptanonConsentNoticeByTypeEnd", {type: this.referenceType, domain: this.domain, language: this.language });
        } else {
            this.commentTextStart = this.translate("OptanonConsentNoticeStart");
            this.commentTextEnd = this.translate("OptanonConsentNoticeEnd");
        }
    }
}

export default {
    bindings: {
        title: "@",
        scriptUrl: "@",
        referenceType: "@?",
        domain: "@?",
        language: "@?",
    },
    controller: CcCookieScriptTag,
    template: `
				<h6>{{::$ctrl.title}}</h6>
				<prettify>
					<p class="margin-all-0"> &lt;!-- {{::$ctrl.commentTextStart}} --&gt;</p>
					<p class="margin-all-0"> &lt;script src="{{::$ctrl.scriptUrl}}" type="text/javascript" charset="UTF-8"&gt;&lt;/script&gt;</p>
					<p class="margin-all-0"> &lt;script type="text/javascript"&gt;</p>
					<p class="margin-all-0"> function OptanonWrapper() { }</p>
					<p class="margin-all-0"> &lt;/script&gt;</p>
					<p class="margin-all-0"> &lt;!-- {{::$ctrl.commentTextEnd}} --&gt;</p>
				</prettify>
	`,
};
