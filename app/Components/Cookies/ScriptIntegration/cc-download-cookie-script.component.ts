import {CookieScriptIntegrationService} from "cookiesService/cookie-script-integration.service";

class CcDownloadCookieScript implements ng.IComponentController {
    static $inject: string[] = ["CookieScriptIntegrationService"];

    private titleHeading: string;
    private titleNotes: string;
    private buttonText: string;
    private buttonNotes: string;
    private domainId: number;
    private downloadInProgress: boolean;

    constructor(readonly CookieScriptIntegrationService: CookieScriptIntegrationService) {
        this.downloadInProgress = false;
    }

    private downloadCookieScript(): any {
        this.downloadInProgress = true;
        this.CookieScriptIntegrationService.downloadCookieScript(this.domainId).then((): void => {
            this.downloadInProgress = false;
        });
    }

}

export default {
    bindings: {
        titleHeading: "@",
        titleNotes: "@",
        buttonText: "@",
        buttonNotes: "@",
        domainId: "<",
    },
    controller: CcDownloadCookieScript,
    template: `
				<h5>{{::$ctrl.titleHeading}}</h5>
				<h6 class="padding-top-half padding-bottom-2">{{::$ctrl.titleNotes}}</h6>
				<one-button
					icon="true"
					icon-class="fa fa-download"
					enable-loading="true"
					text="{{ ::$ctrl.buttonText}}"
					type="primary"
					button-click="$ctrl.downloadCookieScript()"
					is-disabled="$ctrl.downloadInProgress"
					is-loading="$ctrl.downloadInProgress"
				>
				</one-button>
				<h6 class="cc-script-integration__download-button-notes">{{::$ctrl.downloadButtonNotes}}</h6>
	`,
};
