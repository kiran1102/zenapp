import { IPromise } from "angular";
// Services
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookieScriptIntegrationService } from "cookiesService/cookie-script-integration.service";
import { CookieMultiLanguageService } from "cookiesService/cookie-multi-language.service";
import { CookieLanguageSwitcherService } from "cookiesService/cookie-language-switcher.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ITabsNav } from "interfaces/one-tabs-nav.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import {
    IBannerLanguage,
    IBannerLangReponse,
} from "interfaces/cookies/cc-banner-languages.interface";
import { ILanguageSwitcher } from "interfaces/cookies/cc-language-switcher.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IDomainScript } from "interfaces/cookies/cc-script-integration.interface";
import { IGeoLocationSwitcher } from "interfaces/cookies/geo-location-switcher.interface";
import { IStringMap } from "interfaces/generic.interface";

class CcScriptIntegrationController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieScriptIntegrationService",
        "CookieComplianceStore",
        "CookiePolicyService",
        "Permissions",
        "CookieMultiLanguageService",
        "CookieLanguageSwitcherService",
    ];

    bannerLanguages: IBannerLanguage[];
    currentTab = "1";
    customJs = "";
    domainId: number;
    domainScriptModel: IDomainScript;
    geoLocationSwitcher: IGeoLocationSwitcher;
    isUnpublishedChanges = false;
    languageSwitcher: ILanguageSwitcher;
    productionUrls: IStringMap<string>;
    pageReady: boolean;
    permissions: IStringMap<boolean>;
    publishScriptInProgress = false;
    selectedWebsite: string;
    showPublishButton = false;
    geoLocationSwitcherEnabled = false;
    languageSwitcherEnabled = false;
    translate = this.$rootScope.t;
    tabOptions: ITabsNav[] = [{
        text: this.translate("ProductionCDN"),
        id: "1",
        action: (option: ITabsNav): void => this.changeTab(option),
    }, {
        text: this.translate("ProductionSingleLocation"),
        id: "2",
        action: (option: ITabsNav): void => this.changeTab(option),
    }, {
        text: this.translate("DownloadToLocal"),
        id: "3",
        action: (option: ITabsNav): void => this.changeTab(option),
    }, {
        text: this.translate("StagingOrTest"),
        id: "4",
        action: (option: ITabsNav): void => this.changeTab(option),
    }];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private cookieScriptIntegrationService: CookieScriptIntegrationService,
        private cookieComplianceStore: CookieComplianceStore,
        private cookiePolicyService: CookiePolicyService,
        private permissionsService: Permissions,
        private cookieMultiLanguageService: CookieMultiLanguageService,
        private cookieLanguageSwitcherService: CookieLanguageSwitcherService,
    ) { }

    $onInit(): void {
        this.permissions = {
            preferenceCenterEditor: this.permissionsService.canShow("PreferenceCenterEditor"),
            cookiePolicyEditor: this.permissionsService.canShow("CookiePolicyEditor"),
            cookieAllowCustomJs: this.permissionsService.canShow("CookieAllowCustomJs"),
            cookieGeoLocationBannerSwitch: this.permissionsService.canShow("CookieGeoLocationBannerSwitch"),
            canShowLanguageSwitcher: this.permissionsService.canShow("CookieLanguageSwitcher"),
        };
        this.initProductionUrls();
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        if (!this.selectedWebsite) {
            this.$rootScope.$emit("ccInitCookieData", [this.selectedWebsite, "ccScriptIntegration"]);
            this.$rootScope.$on("ccResumeScriptintegration", (event: ng.IAngularEvent, data: string): void => {
                this.populateScriptIntegration();
            });
        } else {
            this.populateScriptIntegration();
        }
    }

    saveCustomJS(): void {
        this.cookieScriptIntegrationService.saveCustomJs(this.customJs, this.domainId);
    }

    populateScriptIntegration(): void {
        this.pageReady = false;
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        if (this.domainId) {
            this.getScriptIntegrationPageDetail();
        } else {
            this.pageReady = true;
            this.domainScriptModel = null;
            this.bannerLanguages = [];
            this.showPublishButton = false;
        }
    }

    publishTestScript(): void {
        this.publishScriptInProgress = true;
        this.cookieScriptIntegrationService.publishTestScript(this.domainId)
            .then((): void => {
                this.publishScriptInProgress = false;
            });
    }

    switcherCallback(selection: ILanguageSwitcher | IGeoLocationSwitcher = null): void {
        this.languageSwitcherEnabled = false;
        this.geoLocationSwitcherEnabled = false;
        this.initProductionUrls();
        if (selection && selection.isEnabled) {
            const isLanguagSwitcherSelected = (selection as IGeoLocationSwitcher)["defaultLanguage"];
            if (isLanguagSwitcherSelected) {
                this.productionUrls.languageSwitcherProductionCDNURL = selection.productionCDNUrl;
                this.productionUrls.languageSwitcherProductionSingleURL = selection.productionSingleLocationUrl;
                this.languageSwitcherEnabled = true;
            } else {
                this.productionUrls.geoLocationSwitcherProductionCDNURL = selection.productionCDNUrl;
                this.productionUrls.geoLocationSwitcherProductionSingleURL = selection.productionSingleLocationUrl;
                this.geoLocationSwitcherEnabled = true;
            }
        }
    }

    updateJqueryToggle(value: boolean): void {
        this.domainScriptModel.useNoJQueryScript = value;
    }

    private initProductionUrls(): void {
        this.productionUrls = {
            languageSwitcherProductionCDNURL: null,
            languageSwitcherProductionSingleURL: null,
            geoLocationSwitcherProductionCDNURL: null,
            geoLocationSwitcherProductionSingleURL: null,
        };
    }
    private getScriptIntegrationPageDetail() {
        const promises = [
            this.getBannerLanguages(),
            this.getDomainScriptDetails(),
            this.getPublishStatus()];

        if (this.permissions.canShowLanguageSwitcher) {
            promises.push(this.getLanguageSwitcherDetail());
        }
        if (this.permissions.cookieAllowCustomJs) {
            promises.push(this.fetchCustomJS());
        }
        if (this.permissions.cookieGeoLocationBannerSwitch) {
            promises.push(this.getGeoLocationSwitcherDetails());
        }
        Promise.all(promises)
            .then((): void => {
                // updating script url's based on the selected switcher(geo/language)
                const selectedSwitcher: ILanguageSwitcher | IGeoLocationSwitcher = this.languageSwitcher && this.languageSwitcher.isEnabled ? this.languageSwitcher : this.geoLocationSwitcher;
                this.switcherCallback(selectedSwitcher);
                this.pageReady = true;
                this.showPublishButton = true;
            });
    }

    private fetchCustomJS(): IPromise<void> {
        return this.cookieScriptIntegrationService.fetchCustomJs(this.domainId)
            .then((response: IProtocolResponse<string>): void => {
                if (!response.result) return;
                this.customJs = response.data;
            });
    }

    private getBannerLanguages(): IPromise<void> {
        return this.cookieMultiLanguageService.getBannerLanguages(this.domainId)
            .then((response: IBannerLangReponse): void => {
                this.bannerLanguages = response.languages;
            });
    }

    private getLanguageSwitcherDetail(): IPromise<void> {
        return this.cookieLanguageSwitcherService.getLanguageSwitcherStatus(this.domainId)
            .then((response: ILanguageSwitcher | null): void => {
                if (!response) {
                    this.languageSwitcher = {
                        defaultLanguage: null,
                        domainId: this.domainId,
                        isEnabled: false,
                        productionCDNUrl: null,
                        productionSingleLocationUrl: null,
                    };
                } else {
                    this.languageSwitcher = response;
                }
            });
    }

    private getGeoLocationSwitcherDetails(): IPromise<void> {
        return this.cookieLanguageSwitcherService.getGeoLocationSwitcher(this.domainId)
            .then((response: IProtocolResponse<IGeoLocationSwitcher | null>): void => {
                if (!response.result || !response.data) {
                    this.geoLocationSwitcher = {
                        euLanguage: null,
                        isEnabled: false,
                        nonEuLanguage: null,
                        productionCDNUrl: null,
                        productionSingleLocationUrl: null,
                    };
                } else {
                    this.geoLocationSwitcher = response.data;
                }
            });
    }

    private getDomainScriptDetails(): IPromise<void> {
        return this.cookieScriptIntegrationService.getScriptIntegration(this.domainId)
            .then((response: IDomainScript): void => {
                this.domainScriptModel = response ? response : null;
            });
    }

    private getPublishStatus(): IPromise<void> {
        return this.cookiePolicyService.getPublishedStatus(this.domainId)
            .then((response: { unpublished: boolean } | null): void => {
                this.isUnpublishedChanges = response ? response.unpublished : false;
            });
    }

    private changeTab(option: ITabsNav): void {
        this.currentTab = option.id;
    }
}

export const CcScriptIntegration: ng.IComponentOptions = {
    controller: CcScriptIntegrationController,
    template: `
		<div class="cc-script-integration">
			<cc-cookie-compliance-header
				ng-if="$ctrl.selectedWebsite"
				navigation-title="{{::$ctrl.translate('ScriptIntegration')}}"
				domain-change-function="$ctrl.populateScriptIntegration()"
				is-unpublished-changes="$ctrl.isUnpublishedChanges"
				page-ready="$ctrl.pageReady"
				show-advance-settings-button="true"
				show-publish-button="$ctrl.showPublishButton"
                show-language-selection="true"
				page-name="ccScriptIntegration"
                language-switcher-enabled="$ctrl.languageSwitcherEnabled"
                geo-location-switcher-enabled="$ctrl.geoLocationSwitcherEnabled"
				hide-language-add-button="true"
			></cc-cookie-compliance-header>
            <div class="module-wrapper">
                <div
                    ng-if="$ctrl.pageReady && $ctrl.domainScriptModel"
                    class="padding-left-right-2"
                >
                    <script-integration-settings
                        ng-if="$ctrl.bannerLanguages.length"
                        [domain-id]="$ctrl.domainId"
                        [use-one-trust-jquery-script]="!$ctrl.domainScriptModel.useNoJQueryScript"
                        [languages]="$ctrl.bannerLanguages"
                        [language-switcher]="$ctrl.languageSwitcher"
                        [geo-location-switcher]="$ctrl.geoLocationSwitcher"
                        (switcher-callback)="$ctrl.switcherCallback($event)"
                        (update-jquery-toggle-callback)="$ctrl.updateJqueryToggle($event)"
                    ></script-integration-settings>
                    <!-- custom js input -->
                    <one-label-content
                        ng-if="$ctrl.permissions.cookieAllowCustomJs"
                        name={{::$ctrl.translate('CustomJavascript')}}
                        wrapper-class="margin-top-2"
                        icon="ot ot-info-circle"
                        icon-class="padding-left-1"
                        description="{{::$ctrl.translate('CustomJavascriptDisclaimerNote')}}"
                    >
                        <textarea
                            class="one-textarea width-40-percent"
                            ot-auto-id="CookieCustomJsInput"
                            placeholder="{{::$ctrl.translate('CustomJavascript')}}"
                            rows="5"
                            ng-model="$ctrl.customJs"
                            ng-model-options="{updateOn:'default blur', debounce:{default: 5000, blur: 0}}"
                            ng-change="$ctrl.saveCustomJS()"
                        >
                        </textarea>
                    </one-label-content>
					<h3 class="margin-top-3">{{::$ctrl.translate('MainCookiesScriptTag')}}</h3>
					<h6>{{::$ctrl.translate('MainCookiesScriptTagDetails')}}</h6>
					<div class="margin-top-3 width-auto">
						<one-tabs-nav class="text-bold"
							options="$ctrl.tabOptions"
							selected-tab="$ctrl.currentTab",
						></one-tabs-nav>
						<div class="overflow-y-auto">
							<section class="padding-top-2 padding-bottom-1">
								<div ng-if="$ctrl.currentTab === '1'">
                                    <cc-cookie-script-tag
                                        ng-if="!$ctrl.productionUrls.languageSwitcherProductionCDNURL && !$ctrl.productionUrls.geoLocationSwitcherProductionCDNURL"
										title="{{::$ctrl.translate('ProductionCDNDetails')}}"
										script-url="{{::$ctrl.domainScriptModel.cdnScriptUrl}}"
										reference-type="{{::$ctrl.translate('ProductionCDN')}}"
										domain="{{::$ctrl.selectedWebsite}}"
										language="{{::$ctrl.languageSwitcher.defaultLanguage}}"
									></cc-cookie-script-tag>
                                    <cc-cookie-script-tag
                                        ng-if="$ctrl.productionUrls.languageSwitcherProductionCDNURL"
										title="{{::$ctrl.translate('ProductionCDNDetails')}}"
										script-url="{{::$ctrl.productionUrls.languageSwitcherProductionCDNURL}}"
									></cc-cookie-script-tag>
                                    <cc-cookie-script-tag
                                        ng-if="$ctrl.productionUrls.geoLocationSwitcherProductionCDNURL"
                                        title="{{::$ctrl.translate('ProductionCDNDetails')}}"
                                        script-url="{{::$ctrl.productionUrls.geoLocationSwitcherProductionCDNURL}}"
                                    ></cc-cookie-script-tag>
								</div>
								<div ng-if="$ctrl.currentTab === '2'">
                                    <cc-cookie-script-tag
                                        ng-if="!$ctrl.productionUrls.languageSwitcherProductionSingleURL && !$ctrl.productionUrls.geoLocationSwitcherProductionSingleURL"
										title="{{::$ctrl.translate('UseThisCodeInYourLiveWebsite')}}"
										script-url="{{::$ctrl.domainScriptModel.publishedScriptUrl}}"
										reference-type="{{::$ctrl.translate('ProductionStandard')}}"
										domain="{{::$ctrl.selectedWebsite}}"
										language="{{::$ctrl.languageSwitcher.defaultLanguage}}"
									></cc-cookie-script-tag>
                                    <cc-cookie-script-tag
                                        ng-if="$ctrl.productionUrls.languageSwitcherProductionSingleURL"
										title="{{::$ctrl.translate('UseThisCodeInYourLiveWebsite')}}"
										script-url="{{::$ctrl.productionUrls.languageSwitcherProductionSingleURL}}"
									></cc-cookie-script-tag>
                                    <cc-cookie-script-tag
                                        ng-if="$ctrl.productionUrls.geoLocationSwitcherProductionSingleURL"
                                        title="{{::$ctrl.translate('UseThisCodeInYourLiveWebsite')}}"
                                        script-url="{{::$ctrl.productionUrls.geoLocationSwitcherProductionSingleURL}}"
                                    ></cc-cookie-script-tag>
								</div>
								<div ng-if="$ctrl.currentTab === '4'">
									<one-button
										icon="true"
										enable-loading="true"
										text="{{::$ctrl.translate('PublishTestScript')}}"
										type="primary"
										button-click="$ctrl.publishTestScript()"
										is-disabled="$ctrl.publishScriptInProgress"
										is-loading="$ctrl.publishScriptInProgress"
									>
									</one-button>
									<cc-cookie-script-tag
										title="{{::$ctrl.translate('YouCanUseThisVersionOnTesting')}}"
										script-url="{{::$ctrl.domainScriptModel.testScriptUrl}}"
										reference-type="{{::$ctrl.translate('Staging')}}"
										domain="{{::$ctrl.selectedWebsite}}"
										language="{{::$ctrl.languageSwitcher.defaultLanguage}}"
									></cc-cookie-script-tag>
								</div>
								<div ng-if="$ctrl.currentTab === '3'">
									<cc-download-cookie-script
										title-heading="{{::$ctrl.translate('DownloadLocalScriptFile')}}"
										title-notes="{{::$ctrl.translate('DownloadLocalScriptFileDetails')}}"
										button-text="{{::$ctrl.translate('DownloadLocalFile')}}"
										button-notes="{{::$ctrl.translate('DownloadLocalFileNote')}}"
										domain-id="$ctrl.domainId"
									></cc-download-cookie-script>
								</div>
							</section>
							<hr/>
							<section>
								<div ng-if="::($ctrl.permissions.preferenceCenterEditor)">
									<h4>{{::$ctrl.translate("CookieSettingsButtonTitle")}}</h4>
									<h6>{{::$ctrl.translate("CookieSettingsDetails")}}</h6>
									<prettify>
										<p class="margin-all-0"> &lt;!-- {{::$ctrl.translate("OptanonCookieSettingsButtonStart")}} --&gt;</p>
										<p class="margin-all-0"> &lt;a class="optanon-show-settings"&gt;{{::$ctrl.translate("CookieSettings")}}&lt;/a&gt;</p>
										<p class="margin-all-0"> &lt;!-- {{::$ctrl.translate("OptanonCookieSettingsButtonEnd")}} --&gt;</p>
									</prettify>
								</div>
								<div class="margin-top-2" ng-if="::($ctrl.permissions.cookiePolicyEditor)">
									<h4>{{::$ctrl.translate("CookieDisclosure")}}</h4>
									<h6>{{::$ctrl.translate("CookieDisclosureScriptDescription")}}</h6>
									<prettify>
										<p class="margin-all-0"> &lt;!-- {{::$ctrl.translate("OptanonCookieDisclosureStart")}} --&gt;</p>
										<p class="margin-all-0"> &lt;div id="optanon-cookie-policy"&gt;&lt;/div&gt;</p>
										<p class="margin-all-0"> &lt;!-- {{::$ctrl.translate("OptanonCookieDisclosureEnd")}} --&gt;</p>
									</prettify>
								</div>
							</section>
						</div>
					</div>
                </div>
                <loading
                    ng-if="!$ctrl.pageReady"
                    text="{{ ::labels.loading }}"
                ></loading>
                <div
                    ng-if="$ctrl.pageReady && !$ctrl.domainScriptModel"
                    class="cookiecompliance__message"
                >
					<h2 class="cookiecompliance__message-heading">{{::$ctrl.translate("YouHaveNotSetUpBanner")}}</h2>
					<h2 class="cookiecompliance__message-heading">{{::$ctrl.translate("WhenYouDoTheCodeWillAppear")}}</h2>
				</div>
			</div>
		</div>
	`,
};
