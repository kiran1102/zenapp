import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { ScanBehindLoginStatus } from "constants/cookie.constant";

class CcWebAuditActionListController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "Permissions",
    ];

    private translate = this.$rootScope.t;
    private reAuditCallback: () => void;
    private loginCallback: () => void;
    private deleteScanCallback: () => void;
    private reAssignCallback: () => void;
    private stopScanCallback: () => void;
    private isScanComplete: () => boolean;
    private canStopScan: () => boolean;
    private scanLoginInfoStatus: string;
    private canReassignOrgGroup = false;
    private canLoginForm = false;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
    ) {}

    private getLoginIconColor(scanLoginStatus: string): string {
        if (scanLoginStatus && this.isScanComplete()) {
            if (scanLoginStatus === ScanBehindLoginStatus.Verified) {
                return "cc-scan-results__login-icon-verified";
            } else if (scanLoginStatus === ScanBehindLoginStatus.NotVerified) {
                return "cc-scan-results__login-icon-not-verified";
            } else if (scanLoginStatus === ScanBehindLoginStatus.Failed) {
                return "cc-scan-results__login-icon-failed";
            } else if (scanLoginStatus === ScanBehindLoginStatus.NotAvailable) {
                return "cc-scan-results__login-icon-not-created";
            }
        }
        return "cc-scan-results__login-icon-not-created";
    }

    private getEllipsisOptions(): IDropdownOption[] {
        this.canReassignOrgGroup = this.permissions.canShow("CookieOrgGroupAssignment") && this.isScanComplete();
        this.canLoginForm = this.permissions.canShow("CookieScanBehindLogin") && this.isScanComplete();
        return [
            {
                id: "reAudit",
                text: this.translate("ReScan"),
                action: (): void => {
                    this.reAuditCallback();
                },
                icon: "fa fa-undo position-absolute left-1",
                isDisabled: !this.isScanComplete(),
            },
            {
                id: "reAssign",
                text: this.translate("ReAssign"),
                action: (): void => {
                    this.reAssignCallback();
                },
                icon: "ot ot-users position-absolute left-1",
                isDisabled: !this.canReassignOrgGroup,
            },
            {
                id: "loginForm",
                text: this.translate("Log-in"),
                action: (): void => {
                    this.loginCallback();
                },
                icon: `fa fa-key position-absolute left-1 ${this.getLoginIconColor(this.scanLoginInfoStatus)}`,
                isDisabled: !this.canLoginForm,
            },
            {
                id: "deleteScan",
                text: this.translate("Delete"),
                action: (): void => {
                    this.deleteScanCallback();
                },
                icon: "ot ot-trash position-absolute left-1",
                isDisabled: !this.isScanComplete(),
            },
            {
                id: "stopScan",
                text: this.translate("Stop"),
                action: (): void => {
                    this.stopScanCallback();
                },
                icon: "ot ot-ban position-absolute left-1",
                isDisabled: !this.canStopScan(),
            },
        ];
    }
}

export const CcWebAuditActionList: ng.IComponentOptions = {
    bindings: {
        reAuditCallback: "&",
        loginCallback: "&",
        deleteScanCallback: "&",
        reAssignCallback: "&",
        stopScanCallback: "&",
        isScanComplete: "&",
        canStopScan: "&",
        scanLoginInfoStatus: "@?",
    },
    controller: CcWebAuditActionListController,
    template: `
        <one-dropdown
            item-class="margin-left-2",
            icon="ot ot-ellipsis-h",
            data-id="cookieWebSiteListActions"
            hide-caret="true",
            align-right="true",
            get-options="$ctrl.getEllipsisOptions()"
            round-button="true"
        >
        </one-dropdown>
    `,
};
