import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

class CcItemCountTileController implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "$state",
        "Permissions",
    ];

    private translate: (key: string) => string;
    private tileType: string;
    private tileTitle: string;
    private itemCountTotal: number;
    private itemSubHeadingTotal: string;
    private itemCountBehindLogin: number | null;
    private itemSubHeadingBehindLogin: string | null;
    private isScanHistory: boolean;
    private toolTipText: string;
    private canShowScanBehindCount: boolean;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $state: ng.ui.IStateService,
        private permissions: Permissions,
    ) {}

    public $onInit(): void {
        this.translate = this.$rootScope.t;
        this.canShowScanBehindCount = this.permissions.canShow("CookieScanBehindLogin") && this.itemCountBehindLogin !== undefined && this.itemSubHeadingBehindLogin.length > 0;
    }

    public goToDetailedReport(): void {
         if (this.tileType === "Cookies") {
            this.goTo("zen.app.pia.module.cookiecompliance.views.cookies");
         } else if (this.tileType === "Pages") {
            this.goTo("zen.app.pia.module.cookiecompliance.views.pages");
         } else if (this.tileType === "LocalStorage") {
            this.goTo("zen.app.pia.module.cookiecompliance.views.localStorage");
         }
    }

    private goTo(route: string): void {
        this.$state.go(route);
    }
}

export const CcItemCountTile: ng.IComponentOptions = {
    bindings: {
        tileType: "@",
        tileTitle: "@",
        itemCountTotal: "<",
        itemSubHeadingTotal: "@",
        itemCountBehindLogin: "<?",
        itemSubHeadingBehindLogin: "@?",
        isScanHistory: "<",
        toolTipText: "@?",
    },
    controller: CcItemCountTileController,
    template: `
		<h2 class="full-width text-center text-large margin-left-right-0 margin-top-0 margin-bottom-1">
			{{::$ctrl.tileTitle}}
		</h2>
		<div ng-if="$ctrl.toolTipText && $ctrl.canShowScanBehindCount" class="position-absolute paragraph-small top-1 right-1">
			<i class="ot ot-info-circle"
				uib-tooltip="{{$ctrl.toolTipText}}"
				tooltip-append-to-body="true"
				>
			</i>
		</div>
		<div class="flex">
			<div
				class="text-center shadow7 color-white"
				ng-class="{'cc-item-count-tile__content-block background-blue': $ctrl.canShowScanBehindCount, 'cc-item-count-tile__content-block background-slate': !$ctrl.canShowScanBehindCount}"
				>
				<h3 class="text-thin-2 margin-bottom-0 margin-top-1">
					{{$ctrl.itemCountTotal}}
				</h3>
				<h4 class="text-uppercase margin-left-right-2 margin-top-0">
					{{::$ctrl.itemSubHeadingTotal}}
				</h4>
			</div>
			<div
				class="cc-item-count-tile__content-block background-green color-white text-center shadow7"
				ng-if="$ctrl.canShowScanBehindCount"
				>
				<h3 class="text-thin-2 margin-bottom-0 margin-top-1">
					{{$ctrl.itemCountBehindLogin}}
				</h3>
				<h4 class="text-uppercase margin-left-right-2 margin-top-0">
					{{::$ctrl.itemSubHeadingBehindLogin}}
				</h4>
			</div>
		</div>
		<div class="row">
			<button ng-if="!$ctrl.isScanHistory"
				class="one-button one-button--primary"
				ng-click="$ctrl.goToDetailedReport()"
			>
				{{::$ctrl.translate("Details")}}
			</button>
		</div>
	`,
};
