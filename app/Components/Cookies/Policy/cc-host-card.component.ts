import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { WebAuditService } from "cookiesService/web-audit.service";

class CcHostCard implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "WebAuditService"];

    private translate: any;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly WebAuditService: WebAuditService,
    ) {
        this.translate = $rootScope.t;
    }

    private getClassNameFor(purpose: string): string {
        return this.WebAuditService.getCookieClass(purpose, "graph-legend__");
    }
}

const ccHostCard = {
    bindings: {
        host: "<",
        hideCardBody: "<?",
        onDrag: "<?",
    },
    controller: CcHostCard,
    template: `
		<one-draggable-card
			id="$ctrl.host"
			class="cc-host-card flex static-vertical"
			selectable="false"
			title-bar-color="steel"
			hide-card-body="$ctrl.hideCardBody"
			on-drag="$ctrl.onDrag"
		>
			<card-title class="cc-host-card__card-title"
				ng-class="$ctrl.getClassNameFor($ctrl.cookie.purpose)">
				{{::$ctrl.host.hostName}}
			</card-title>
			<card-body>
				<p>
					<span class="text-bold">{{::$ctrl.translate("Purpose")}}:</span>
					{{::($ctrl.host.purpose || $ctrl.translate("Unknown"))}}
				</p>
				<p>
					<span class="text-bold">{{::$ctrl.translate("Cookies")}}:</span>
					{{::$ctrl.host.numberOfCookies}}
				</p>
				<p class="cc-host-card_type text-uppercase pull-right">
					{{::$ctrl.translate("ThirdParty")}}
				</p>
			</card-body>
		</one-draggable-card>
		`,
};

export default ccHostCard;
