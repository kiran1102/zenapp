import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { IUnassignedCookies } from "interfaces/cookies/cc-unassigned-cookies.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ICookies, IHosts } from "interfaces/cookies/cc-cookie-category.interface";
import { CookiePurposes } from "constants/cookie.constant";

class CcUnassignedList implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePolicyService",
        "CookieComplianceStore",
        "Permissions"];

    private translate: any;
    private domainId: number;
    private onDrop: any;
    private unassignedCookie: IUnassignedCookies;
    private populatePolicy: any;
    private autoAssignInProgress = false;
    private updateFromScanInProgress = false;
    private cookieCount: number;
    private reloadGroup: boolean;
    private isUnpublishedChanges: boolean;
    private isAutoAssignDisabled = false;
    private isUpdateFromScanDisabled = false;
    private canShowUpdateFromScan = false;
    private cookieHasKnownPurpose = false;

    constructor(
        $rootScope: IExtendedRootScopeService,
        private cookiePolicyService: CookiePolicyService,
        private cookieComplianceStore: CookieComplianceStore,
        private permissions: Permissions,
    ) {
        this.translate = $rootScope.t;
    }

    public $onInit(): void {
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.canShowUpdateFromScan = this.permissions.canShow("CustomUpdateFromRescan");
        this.checkIfCookieHasPurpose();
    }

    public onDropCard = (entity: any, index: number): boolean => {
        if (this.reloadGroup) {
            return false;
        }
        if (!this.isUnpublishedChanges && !entity.isParentGroup) {
            this.isUnpublishedChanges = true;
        }
        if (entity.id && entity.thirdParty === false) {
            return this.updateCookie(entity);
        } else if (entity.id) {
            return this.updateSubgroup(entity);
        } else if (entity.isParentGroup) {
            return false;
        }
        return this.updateHosts(entity);

    }

    public updateHosts = (entity: any): boolean => {
        this.cookiePolicyService.moveHosts(this.domainId, entity.hostName, entity.groupId, this.unassignedCookie.groupId);
        entity.groupId = this.unassignedCookie.groupId;
        this.unassignedCookie.hosts.push(entity);
        this.cookieCount = this.cookieCount + entity.numberOfCookies;
        this.checkIfCookieHasPurpose();
        return true;
    }

    public updateCookie = (entity: any): boolean => {
        this.unassignedCookie.cookies.push(entity);
        this.cookiePolicyService.moveCookie(this.domainId, entity.id, this.unassignedCookie.groupId);
        this.cookieCount = this.cookieCount + 1;
        this.checkIfCookieHasPurpose();
        return true;
    }

    public updateSubgroup = (entity: any): boolean => {
        this.reloadGroup = true;
        this.cookiePolicyService.getGroupCookies(this.domainId, entity.id).then(
            (responseObject: any): any => {
                _.each(responseObject.cookies, (cookie: any): void => {
                    this.unassignedCookie.cookies.push(cookie);
                    this.cookieCount = this.cookieCount + 1;
                });
                _.each(responseObject.hosts, (element: any): void => {
                    element.groupId = this.unassignedCookie.groupId;
                    this.unassignedCookie.hosts.push(element);
                    this.cookieCount = this.cookieCount + element.numberOfCookies;
                });
                this.cookiePolicyService.UpdateSubgroup(this.domainId, this.unassignedCookie.groupId, entity.id);
                this.checkIfCookieHasPurpose();
                this.reloadGroup = false;
                return true;
            });
        return true;
    }

    public onDragCard = (id: any): boolean => {
        _.remove(this.unassignedCookie.cookies, (cookie: any): boolean => cookie === id);
        this.cookieCount = this.cookieCount - 1;
        this.checkIfCookieHasPurpose();
        return true;
    }

    public autoAssign = (): void => {
        this.autoAssignInProgress = true;
        this.isAutoAssignDisabled = true;
        this.cookiePolicyService.autoAssignCookies(this.domainId).then((): void => {
            this.autoAssignInProgress = false;
            this.isAutoAssignDisabled = false;
            if (!this.isUnpublishedChanges) {
                this.isUnpublishedChanges = true;
            }
            this.populatePolicy();
        });
    }

    public onDragHostCard = (id: any): boolean => {
        _.remove(this.unassignedCookie.hosts, (host: any): boolean => host === id);
        this.cookieCount = this.cookieCount - id.numberOfCookies;
        this.checkIfCookieHasPurpose();
        return true;
    }

    public updateCookiesFromLatestScan(): void {
        this.updateFromScanInProgress = true;
        this.isUpdateFromScanDisabled = true;
        this.cookiePolicyService.updateCookiesForRescan(this.domainId).then((): void => {
            this.updateFromScanInProgress = false;
            this.isUpdateFromScanDisabled = false;
            if (!this.isUnpublishedChanges) {
                this.isUnpublishedChanges = true;
            }
            this.populatePolicy();
        });
    }

    private checkIfCookieHasPurpose = (): void => {
        _.find(this.unassignedCookie.cookies,
            (cookie: ICookies): boolean => this.cookieHasKnownPurpose = cookie["purpose"] !== CookiePurposes.Unknown);
        if (!this.cookieHasKnownPurpose) {
            _.find(this.unassignedCookie.hosts,
                (host: IHosts): boolean => this.cookieHasKnownPurpose = host["purpose"] !== CookiePurposes.Unknown);
        }

    }
}

const ccUnassignedList = {
    bindings: {
        domainId: "<",
        unassignedCookie: "<",
        populatePolicy: "&",
        isUnpublishedChanges: "=",
        reloadGroup: "<",
        cookieCount: "<",
    },
    controller: CcUnassignedList,
    template: `
			<div class="cc-unassigned-list column-horizontal-center height-100">
				<div class="cc-unassigned-list__header-container full-width static-vertical column-horizontal-center padding-all-2">
					<h3 class="cc-unassigned-list__header">{{::$ctrl.translate("Unassigned")}}</h3>
					<p class="text-bold">{{$ctrl.cookieCount}} {{::$ctrl.translate("Cookies")}}</p>

					<div class="row-wrap row-horizontal-center">
						<one-button
							type="{{$ctrl.cookieHasKnownPurpose && $ctrl.unassignedCookie.cookies.length ? 'primary':'select'}}"
							text='{{::$ctrl.translate("AutoAssign")}}'
							class="margin-all-half"
							button-click="$ctrl.autoAssign()"
							enable-loading="true"
							is-disabled="$ctrl.isAutoAssignDisabled || !$ctrl.unassignedCookie.cookies.length",
							is-loading="$ctrl.autoAssignInProgress"
						></one-button>
						<one-button
							ng-if="::$ctrl.canShowUpdateFromScan"
							type="primary"
							text='{{::$ctrl.translate("UpdateFromScan")}}'
							class="margin-all-half"
							enable-loading="true"
							is-loading="$ctrl.updateFromScanInProgress"
							is-disabled="$ctrl.isUpdateFromScanDisabled",
							button-click="$ctrl.updateCookiesFromLatestScan()"
							>
						</one-button>
					</div>
				</div>
				<div class="cc-unassigned-list__cookie-container padding-all-1
							row-horizontal-center row-wrap stretch-vertical full-width
							position-relative"
				>
					<div ng-if="$ctrl.reloadGroup"
						 class="cc-unassigned-list__loading position-absolute full-width"
					>
						<loading class="margin-all-2" show-text="false"/>
					</div>
					<one-drop-list
						class="full-width full-height position-relative"
						on-drop="$ctrl.onDropCard"
						wrap="true"
						loading="$ctrl.reloadGroup"
					>
						<div class="cc-unassigned-list__cookie-wrapper"
							ng-repeat="cookie in $ctrl.unassignedCookie.cookies">
							<cc-cookie-card
								class="cc-unassigned-list__cookie-card flex static-vertical"
								cookie="cookie"
								on-drag="$ctrl.onDragCard"
							>
							</cc-cookie-card>
						</div>
						<div class="cc-unassigned-list__cookie-wrapper"
							ng-repeat="host in $ctrl.unassignedCookie.hosts">
							<cc-host-card
									class="cc-unassigned-list__cookie-card flex static-vertical"
									host="host"
									on-drag="$ctrl.onDragHostCard"
							>
							</cc-host-card>
						</div>
					</one-drop-list>
				</div>
			</div>
`,
};

export default ccUnassignedList;
