import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { CookiePolicyService } from "cookiesService/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { IVendors, IPurpose } from "interfaces/cookies/cc-iab-purpose.interface";

export class CcGroupVendorModalCtrl implements ng.IController {
    static $inject: string[] = [
        "$rootScope",
        "$uibModalInstance",
        "$scope",
        "group",
        "purposeIds",
        "domainId",
        "selectedVendorIds",
        "CookiePolicyService",
        "CookieComplianceStore",
        "CookieIABIntegrationService",
    ];

    translate: any;
    private title: string;
    private isDisable: boolean;
    private groupName: string;
    private description: string;
    private vendors: IVendors;
    private loading = false;
    private vendorsList: IVendors;
    private searchTerm: string;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly $uibModalInstance: any,
        readonly $scope: any,
        readonly group: any,
        readonly puposeIds: number[],
        readonly domainId: number,
        readonly selectedVendorIds: number[],
        readonly CookiePolicyService: CookiePolicyService,
        readonly CookieComplianceStore: CookieComplianceStore,
        readonly CookieIABIntegrationService: CookieIABIntegrationService,
    ) {
        $scope.controller = this;
        this.translate = $rootScope.t;
        this.searchTerm = "";
        this.$rootScope = $rootScope;
    }

    $onInit() {
        this.getVendors(this.puposeIds);
        this.setCommonLabelText();
    }

    public cancel = (): void => {
        this.$uibModalInstance.close({ result: false, data: null });
    }

    // will be triggered once the activate button is clicked in the vendors modal
    public activateVendors = (): void => {
        const selectedVendors = this.vendors.filter((vendor) => vendor.isSelected);
        const listIds = selectedVendors.map((vendor) => vendor.vendorId);
        this.loading = true;
        this.CookieIABIntegrationService.moveVendors(this.domainId, this.group.parentGroupId, listIds).then(
            (response: boolean): void => {
                this.loading = false;
                this.$uibModalInstance.close({ result: true, data: selectedVendors });
            });
    }

    // Get the vendors based on the purpose ids.
    private getVendors = (purposeIds: number[]): void => {
        this.CookieIABIntegrationService.getVendorsForPurpose(purposeIds.join()).then(
            (response: IVendors): void => {
                response = response.map((vendor) => {
                    if (this.selectedVendorIds.indexOf(vendor.vendorId) >= 0) {
                      vendor.isSelected = true;
                    }
                    return vendor;
                  });

                this.vendorsList = response;
                this.handleSearch();
            });
    }

    // Handling search functionality
    private handleSearch = (value = ""): void  => {
        this.vendors = this.vendorsList.filter((vendor) => vendor.vendorName.toLowerCase().match(value.toLowerCase()));
    }

    private setCommonLabelText = (): void => {
        this.title = this.translate("ActivateVendors");
    }

}
