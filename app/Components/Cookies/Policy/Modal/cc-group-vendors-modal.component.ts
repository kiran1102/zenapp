declare var angular: any;

function controller() {
    return this.scope;
}
const groupTemplate = `
	<one-modal
		headline="{{$ctrl.title}}"
		cancel="$ctrl.cancel"
	>
		<modal-body class="cc-group-modal__modal-body">
			<one-search-input
				class="margin-left-auto"
				input-class="block"
				search-class="filter-item__search"
				name="VendorSearch"
				id="VendorSearch"
				model="$ctrl.searchTerm"
				search-class="inventory-list__search pull-right"
				placeholder="{{::$root.t('SearchByName')}}"
				on-search="$ctrl.handleSearch(model, clear)"
				throttle="250"
		></one-search-input>
		<ul class="vendors-list">
			<li ng-repeat="row in $ctrl.vendors track by $index" class="vendor-item">
				{{row.vendorName}}
				<one-checkbox
					identifier="{{'VendorSelection_'+($index+1)}}"
					class="margin-right-1 pull-right"
					identifier="{{row.vendorName}}"
					is-selected="row.isSelected"
					toggle="row.isSelected = !row.isSelected"
				></one-checkbox>
			</li>
		</ul>
		<div>
		<one-button
				focus-when="true"
				class="approver-modal__button margin-top-1 block pull-right"
				text="{{::$root.t('Activate')}}"
				button-click="$ctrl.activateVendors()"
				type="primary"
				enable-loading="true"
				is-rounded="true"
				is-loading="$ctrl.loading"
				identifier="ccGroupVendorModalActivate"
			>
			</one-button>
			<one-button
				focus-when="true"
				class="approver-modal__button margin-top-1 margin-right-1 block pull-right"
				text="{{::$root.t('Cancel')}}"
				button-click="$ctrl.cancel()"
				is-rounded="true"
				identifier="ccGroupVendorModalCancel"
			>
			</one-button>
		</div>
		</modal-body>
	</one-modal>`;

const CcGroupVendorModal: ng.IComponentOptions = {
    template: groupTemplate,
    controller,
    bindings: {
        scope: "<",
    },
};

export default CcGroupVendorModal;
