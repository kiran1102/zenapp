import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";

class CcSubgroup implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "CookiePolicyService", "CookieComplianceStore"];

    private translate: any;
    private domainId: number;
    private subGroup: any;
    private cookiesCount: number;
    private isUnpublishedChanges: boolean;
    private calculateCount: (cookieCount: number, subgroupId: number) => void;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly CookiePolicyService: CookiePolicyService,
        readonly CookieComplianceStore: CookieComplianceStore,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.domainId = this.CookieComplianceStore.getSelectedDomainId();
        this.cookiesCount = this.calculateCookiesCount();
    }

    public calculateCookiesCount = (): number => {
        let cookieCount = 0;
        cookieCount = this.subGroup.cookies.length;
        _.each(this.subGroup.hosts, (host: any): void => {
            cookieCount = cookieCount + host.numberOfCookies;
        });
        return cookieCount;
    }
    public onDropCard = (entity: any, index: number): boolean => {
        if (entity.id && entity.thirdParty === false) {
            return this.updateCookie(entity);
        } else if (entity.id) {
            return this.updateSubgroup(entity);
        } else if (entity.isParentGroup) {
            return false;
        }
        return this.updateHosts(entity);
    }
    public markPublish = (): void => {
        if (!this.isUnpublishedChanges) {
            this.isUnpublishedChanges = true;
        }
    }
    public updateCookie = (entity: any): boolean => {
        this.subGroup.cookies.push(entity);
        this.CookiePolicyService.moveCookie(this.domainId, entity.id, this.subGroup.groupId);
        this.cookiesCount = this.cookiesCount + 1;
        this.calculateCount(this.cookiesCount, this.subGroup.groupId);
        this.markPublish();
        return true;
    }
    public updateSubgroup = (entity: any): boolean => {
        return false;
    }
    public updateHosts = (entity: any): boolean => {
        this.CookiePolicyService.moveHosts(this.domainId, entity.hostName, entity.groupId, this.subGroup.groupId);
        entity.groupId = this.subGroup.groupId;
        this.subGroup.hosts.push(entity);
        this.cookiesCount = this.cookiesCount + entity.numberOfCookies;
        this.calculateCount(this.cookiesCount, this.subGroup.groupId);
        this.markPublish();
        return true;
    }
    public onDragCard = (id: any): boolean => {
        _.remove(this.subGroup.cookies, (cookie: any): boolean => cookie === id);
        this.cookiesCount = this.cookiesCount - 1;
        this.calculateCount(this.cookiesCount, this.subGroup.groupId);
        return true;
    }
    public onDragHostCard = (id: any): boolean => {
        _.remove(this.subGroup.hosts, (host: any): boolean => host === id);
        this.cookiesCount = this.cookiesCount - id.numberOfCookies;
        this.calculateCount(this.cookiesCount, this.subGroup.groupId);
        return true;
    }
}

const ccSubgroup: ng.IComponentOptions = {
    bindings: {
        subGroup: "=",
        isUnpublishedChanges: "=",
        calculateCount: "=",
    },
    controller: CcSubgroup,
    template: `
			<div class="cc-group__group-description">
				{{$ctrl.subGroup.description}}
			</div>
			<one-drop-list
				class="text-center flex static-vertical"
				on-drop="$ctrl.onDropCard"
			>
				<div class="cc-group__cookie-container row-flex-start row-wrap">
						<cc-cookie-card ng-repeat="cookie in $ctrl.subGroup.cookies track by $index"
							class="cc-unassigned-list__cookie-card flex static-vertical"
							cookie="cookie"
							on-drag="$ctrl.onDragCard"
							hide-card-body="true"
						>
						</cc-cookie-card>
						<cc-host-card ng-repeat="host in $ctrl.subGroup.hosts track by $index"
							class="cc-unassigned-list__cookie-card flex static-vertical"
							host="host"
							on-drag="$ctrl.onDragHostCard"
							hide-card-body="true"
						>
						</cc-host-card>
				</div>
			</one-drop-list>
		`,
};

export default ccSubgroup;
