declare var OneConfirm: any;

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ICookieListConfigurationDetail } from "interfaces/cookies/cookie-list-configuration-detail.interface";
import { ICookieListConsentDetail } from "interfaces/cookies/cookie-list-consent-detail.interface";
import { ICookieGroupDetail } from "interfaces/cookies/cookie-group-detail.interface";

// Services
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";

class CcPolicyHeader implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePolicyService",
        "CookieComplianceStore",
        "CookiePolicyService",
    ];

    cookieListConfigurationDetail: ICookieListConfigurationDetail;
    cookieListDetail: ICookieListConsentDetail;
    groups: ICookieGroupDetail[];
    isDntEnabled: boolean;
    isUnpublishedChanges: boolean;
    translate = this.$rootScope.t;
    domainId: number;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private CookiePolicyService: CookiePolicyService,
    ) { }

    onRestoreCookieListConfiguration(): void {
        OneConfirm(this.translate("RestoreDefault"),
            this.translate("AreYouSureYouWantToRestoreToDefaultLabels"), "Confirm", "Ok", true,
            this.translate("Ok"),
            this.translate("Cancel"))
            .then((revert: boolean): void => {
                if (!revert) return;
                const detail = {
                    CookiesText: this.translate("Cookies"),
                    CategoriesText: this.translate("Categories"),
                    LifespanText: this.translate("Lifespan"),
                    IsLifespanEnabled: false,
                    LifespanTypeText: this.translate("Session"),
                    LifespanDurationText: this.translate("Days"),
                    IsRestoreRequest: true,
                };
                this.CookiePolicyService.updateCookiePolicyLabels(this.domainId, detail)
                    .then((): void => {
                        this.cookieListConfigurationDetail = detail;
                    });

            });
    }
}

const ccPolicyHeader = {
    bindings: {
        cookieListConfigurationDetail: "=",
        cookieListDetail: "=",
        groups: "=",
        domainId: "<",
        isDntEnabled: "=",
        isUnpublishedChanges: "=",
        pageReady: "=",
        populatePolicy: "=",
        restoreCookieListConfiguration: "&",
    },
    controller: CcPolicyHeader,
    template: `
			<cc-cookie-compliance-header
				navigation-title="{{::$ctrl.translate('CookieDisclosure')}}",
				domain-change-function="$ctrl.populatePolicy()",
				is-unpublished-changes="$ctrl.isUnpublishedChanges",
				page-ready="$ctrl.pageReady",
				show-preview-button="true",
				show-advance-settings-button="true",
				show-publish-button="true",
				show-language-selection="true",
                page-name="ccPolicyPage",
                restore-cookie-list-configuration="$ctrl.onRestoreCookieListConfiguration()"
			>
			</cc-cookie-compliance-header>
			`,
};

export default ccPolicyHeader;
