// 3rd Party
import { assign } from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    ICookieSubGroup,
    ICookieGroupMetaDetail,
} from "interfaces/cookies/cookie-group-detail.interface";

// Services
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ModalService } from "sharedServices/modal.service";

declare var OneConfirm: any;

class CcSubgroupSummary implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "CookiePolicyService",
        "ModalService",
    ];

    private translate = this.$rootScope.t;
    private domainId: number;
    private onDrag: any;
    private reloadGroup = false;
    private subGroup: ICookieSubGroup;
    private triggerDeleteSubGroup: any;
    private populateUnAssigned: () => void;
    private response = {
        show: false,
        groupId: 0,
        name: "",
        cookies: [],
        hosts: [],
        description: "",
    };
    private isUnpublishedChanges: boolean;
    private deleteInProgress = false;
    private purposeGroupId: number;
    private isDntEnabled: boolean;
    private parentGroupId: number;
    private updateGroupId: (parentGroupId: number, customGroupId: string, subGroupId: number) => void;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly cookiePolicyService: CookiePolicyService,
        readonly modalService: ModalService) { }

    $onInit(): void {
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
    }

    public populateSubgroup = (id: number): void => {
        if (this.response.show) {
            this.response.show = false;
        } else {
            this.reloadGroup = true;
            this.response.show = true;
            this.cookiePolicyService.getGroupCookies(this.domainId, id).then(
                (responseObject: any): void => {
                    this.response.groupId = responseObject.groupId;
                    this.response.name = responseObject.name;
                    this.response.cookies = responseObject.cookies;
                    this.response.hosts = responseObject.hosts;
                    this.response.description = responseObject.description;
                    this.reloadGroup = false;
                });
        }
    }

    editSubGroup(): void {
        this.modalService.setModalData({
            canShowDntOption: false,
            customGroupId: this.subGroup.customGroupId,
            description: this.subGroup.description,
            domainId: this.domainId,
            groupId: this.subGroup.id,
            isNewGroup: false,
            isSubgroup: true,
            name: this.subGroup.name,
            purposeGroupId: this.purposeGroupId,
            promiseToResolve: (group: ICookieGroupMetaDetail): Promise<boolean> => {
                this.subGroup = assign({}, this.subGroup, group);
                this.response = assign({}, this.response, group);
                this.updateGroupId(this.parentGroupId, group.customGroupId, this.subGroup.id);
                if (!this.isUnpublishedChanges) {
                    this.isUnpublishedChanges = true;
                }
                return Promise.resolve(true);
            },
        });
        this.modalService.openModal("ccGroupModal");
    }

    public deleteSubGroup = (): void => {
        OneConfirm(this.translate("SubGroup"), this.translate("AreYouSureYouWantToDeleteThisSubGroup"), "Confirm", "Yes", true, this.translate("Yes"), this.translate("No")).then((confirmDelete: boolean): undefined | void => {
            if (!confirmDelete) return;
            this.deleteInProgress = true;
            this.cookiePolicyService.deleteGroup(this.subGroup.id).then((response: any): void => {
                if (response.result) {
                    this.triggerDeleteSubGroup(this.subGroup);
                    if (this.subGroup.numberOfCookies > 0) {
                        this.populateUnAssigned();
                    }
                }
                this.deleteInProgress = false;
            });
        });
    }
}

const ccSubgroupSummary = {
    bindings: {
        subGroup: "=",
        onDrag: "=",
        isUnpublishedChanges: "=",
        triggerDeleteSubGroup: "=",
        calculateCount: "=",
        purposeGroupId: "<",
        isDntEnabled: "=",
        populateUnAssigned: "&",
        updateGroupId: "<",
        parentGroupId: "<",
    },
    controller: CcSubgroupSummary,
    template: `<one-draggable-element
                    id="$ctrl.subGroup"
                    on-drag="$ctrl.onDrag"
				>
                    <element>
                        <h5 class="cc-group__subgroup-heading">
                            <i
                                class="fa fa-fw no-outline"
                                ng-class="{'fa-caret-down': $ctrl.response.show, 'fa-caret-right': !$ctrl.response.show }"
                                ng-click="$ctrl.populateSubgroup($ctrl.subGroup.id)"
                            />
                            {{$ctrl.subGroup.name}}
                            <span class="padding-left-1 position-relative media-middle">
                                <one-tag
                                    ng-if="$ctrl.subGroup.isDntEnabled && $ctrl.isDntEnabled"
                                    plain-text="{{::$ctrl.translate('DoNotTrack')}}"
                                    label-class="text-bold one-tag--override one-tag--primary-white"
                                ></one-tag>
                            </span>
                            <i
                                class="cc-group__edit-icon fa fa-edit margin-left-1 no-outline"
                                ng-if="!$ctrl.deleteInProgress"
                                ng-click="$ctrl.editSubGroup()"
                            />
                            <i
                                class="cc-group__delete-icon fa fa-trash-o no-outline padding-left-1"
                                ng-if="!$ctrl.deleteInProgress"
                                ng-click="$ctrl.deleteSubGroup()"
                            />
                            <loading class="margin-all-2" ng-if="$ctrl.deleteInProgress" show-text="false"/>
                            <span class="cc-group__group-count text-bold pull-right margin-right-1">
                                <span ng-if="$ctrl.subGroup.numberOfCookies == 1">{{$ctrl.subGroup.numberOfCookies}} {{::$ctrl.translate("Cookie")}}</span>
                                <span ng-if="$ctrl.subGroup.numberOfCookies > 1">{{$ctrl.subGroup.numberOfCookies}} {{::$ctrl.translate("Cookies")}}</span>
                            </span>
                        </h5>
                        <div uib-collapse="!$ctrl.response.show">
                            <div ng-show="$ctrl.reloadGroup">
                                <loading class="margin-all-2" show-text="false"/>
                            </div>
                            <div ng-if="!$ctrl.reloadGroup && !$ctrl.deleteInProgress">
                                <cc-subgroup
                                    sub-group="$ctrl.response"
                                    is-unpublished-changes="$ctrl.isUnpublishedChanges"
                                    calculate-count="$ctrl.calculateCount"
                                >
                                </cc-subgroup>
                            </div>
                        </div>
                    </element>
				</one-draggable-element>
				`,
};

export default ccSubgroupSummary;
