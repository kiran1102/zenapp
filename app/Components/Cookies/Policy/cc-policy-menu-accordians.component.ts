// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ICookieListConfigurationDetail } from "interfaces/cookies/cookie-list-configuration-detail.interface";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";

class CcPolicyMenuAccordions implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "Permissions",
        "CookiePolicyService",
    ];

    canShowIABSettings = false;
    cookieListConfigurationDetail: ICookieListConfigurationDetail;
    domainId: number;
    translate = this.$rootScope.t;
    canShowConsentPolicy = this.permissions.canShow("CookieConsentPolicy");

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private permissions: Permissions,
        private cookiePolicyService: CookiePolicyService,
    ) { }

    $onInit(): void {
        this.canShowIABSettings = this.permissions.canShow("CookieIABIntegration");
    }

    configurationContentChange(detail: ICookieListConfigurationDetail): void {
        detail.IsRestoreRequest = false;
        this.cookiePolicyService.updateCookiePolicyLabels(this.domainId, detail)
            .then((): void => {
                this.cookieListConfigurationDetail = { ...detail };
            });
    }
}

export default {
    bindings: {
        consentSettings: "=",
        cookieCount: "<",
        cookieListConfigurationDetail: "=",
        dntValueChangeCallback: "&",
        domainId: "<",
        isDntEnabled: "=",
        isUnpublishedChanges: "=",
        populatePolicy: "&",
        purposes: "=",
        reloadGroup: "<",
        showIabSettings: "=",
        unassignedCookie: "<",

    },
    controller: CcPolicyMenuAccordions,
    template: `
		<nav class="cc-accordion-navigation height-100">
				<uib-accordion close-others="false">
                    <div
                        ng-if="!$ctrl.canShowConsentPolicy"
                        uib-accordion-group
                        class="cc-accordion-navigation__item"
                    >
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('ConsentSettings')}}
                        </uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
                            <cc-preference-center-consent-settings
                                is-consent-logging-enabled="$ctrl.consentSettings.IsConsentLoggingEnabled"
								consent-settings="$ctrl.consentSettings.consentSettings"
								consent-models="$ctrl.consentSettings.consentModels"
                                default-statuses="$ctrl.consentSettings.defaultStatuses"
                                is-dnt-enabled="$ctrl.isDntEnabled"
                                dnt-value-change-callback="$ctrl.dntValueChangeCallback()"
							>
							</cc-preference-center-consent-settings>
						</div>
					</div>
				    <div uib-accordion-group class="cc-accordion-navigation__item">
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('Configuration')}}
						</uib-accordion-heading>
                        <div class="cc-accordion-navigation__item-content">
                            <policy-configuration
                                [configuration-detail]="$ctrl.cookieListConfigurationDetail"
                                (configuration-content-change-callback)="$ctrl.configurationContentChange($event)"
                            >
                            </policy-configuration>
						</div>
					</div>
					<div
						uib-accordion-group
						class="cc-accordion-navigation__item"
						ng-if="::$ctrl.canShowIABSettings"
					>
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('IABConsent')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
                            <iab-consent
								is-iab-enabled="$ctrl.consentSettings.IsIABEnabled"
								purposes="$ctrl.purposes"
								selected-list="$ctrl.consentSettings.SelectedList"
                                vendors-list="$ctrl.consentSettings.VendorLists"
                                is-iab-third-party-cookie-enabled="$ctrl.consentSettings.IsIabThirdPartyCookieEnabled"
                                domain-id="$ctrl.domainId"
							>
							</iab-consent>
						</div>
					</div>
					<div uib-accordion-group class="cc-accordion-navigation__item"
						is-open="true">
						<uib-accordion-heading class="cc-accordion-navigation__item-heading">
							{{::$ctrl.translate('AssignCookies')}}
						</uib-accordion-heading>
						<div class="cc-accordion-navigation__item-content">
							<cc-unassigned-list
                                class="cc-policy__unassigned-list"
                                domain-id="$ctrl.domainId"
                                unassigned-cookie="$ctrl.unassignedCookie"
                                populate-policy="$ctrl.populatePolicy()"
                                is-unpublished-changes="$ctrl.isUnpublishedChanges"
                                reload-group="$ctrl.reloadGroup"
                                cookie-count="$ctrl.cookieCount"
                            ></cc-unassigned-list>
						</div>
					</div>
				</ui-accordion>
		</nav>`,
};
