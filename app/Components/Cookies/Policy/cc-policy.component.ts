// 3rd party
import {
    each,
    head,
    some,
    find,
} from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import {
    IPurpose,
    IPurposes,
} from "interfaces/cookies/cc-iab-purpose.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import {
    ICookieGroupDetail,
    ICookieCategoryOptions,
} from "interfaces/cookies/cookie-group-detail.interface";
import { ICookieListConfigurationDetail } from "interfaces/cookies/cookie-list-configuration-detail.interface";
import {
    ICookieListConsentDetail,
    IConsentGroupStatusDetail,
    ISubGroupDetail,
} from "interfaces/cookies/cookie-list-consent-detail.interface";

// Enums
import { PurposeGroupId } from "enums/cookies/purpose-group-id.enum";

// Services
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";

class CcPolicy implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "CookiePolicyService",
        "CookieIABIntegrationService",
        "Permissions",
    ];

    cookieCategoryOptions: ICookieCategoryOptions[];
    cookieListConfigurationDetail: ICookieListConfigurationDetail;
    isDntEnabled: boolean;
    preferenceCenterData: ICookieListConsentDetail;
    unassignedGroupId: number;

    private ready: boolean;
    private translate: any;
    private selectedWebsite: string;
    private websiteList: any;
    private domainId: number;
    private groups: ICookieGroupDetail[];
    private pageReadyUnassign: boolean;
    private pageReadyAssign: boolean;
    private unassignedCookie: ICookieGroupDetail;
    private unAssignedCookieCount: number;
    private isUnpublishedChanges: boolean;
    private reloadGroup: boolean;
    private purposes: IPurposes = [];
    private canShowIABSettings: boolean;
    private pageReadyLevel: boolean;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private cookieComplianceStore: CookieComplianceStore,
        private cookiePolicyService: CookiePolicyService,
        private cookieIABIntegrationService: CookieIABIntegrationService,
        private permissions: Permissions,
    ) { }

    public $onInit(): void {
        this.ready = true;
        this.translate = this.$rootScope.t;
        this.canShowIABSettings = this.permissions.canShow("CookieIABIntegration");
        this.websiteList = this.cookieComplianceStore.getWebsiteList();
        this.populateCookieData();
    }

    public populateUnAssigned = (): void => {
        this.reloadGroup = true;
        this.cookiePolicyService.getUnAssignedGroupByDomainId(this.domainId).then(
            (response: ICookieGroupDetail[]): void => {
                this.unassignedCookie = head(response);
                this.unassignedGroupId = this.unassignedCookie.groupId;
                this.reloadGroup = false;
                this.unAssignedCookieCount = this.UnAssignedCookieCount();
            });
    }

    public populateCookieData = (): void => {
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        if (!this.selectedWebsite) {
            this.$rootScope.$emit("ccInitCookieData", [this.selectedWebsite, "ccPolicyPage"]);
            this.$rootScope.$on("ccResumeCookiePolicy", (event: ng.IAngularEvent, data: string): void => {
                this.populateData();
            });
        } else {
            this.populateData();
        }
    }

    public populateData = (): void => {
        this.cookieCategoryOptions = [];
        this.pageReadyUnassign = false;
        this.pageReadyAssign = false;
        this.pageReadyLevel = false;
        this.selectedWebsite = this.cookieComplianceStore.getSelectedWebsite();
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePolicyService.getPublishedStatus(this.domainId).then(
            (response: IProtocolPacket): void => {
                this.isUnpublishedChanges = response.data.unpublished;
            });
        this.cookiePolicyService.getAssignedGroupByDomainId(this.domainId).then(
            (response: ICookieGroupDetail[]): void => {
                let purposeIds: number[] = [];
                this.groups = response;
                each(this.groups, (group: ICookieGroupDetail): void => {
                    group.isParentGroup = true;
                    each(group.purposes, (purpose: IPurpose): void => {
                        purposeIds.push(purpose.purposeId);
                    });
                    this.cookieCategoryOptions.push({ id: group.groupId, name: group.name });
                });
                purposeIds = purposeIds.filter((el, index) => purposeIds.indexOf(el) === index);
                this.pageReadyAssign = true;
                if (this.canShowIABSettings) {
                    this.cookieIABIntegrationService.getPurposes().then((purposes: IPurposes): void => {
                        this.purposes = purposes.filter((purpose: IPurpose): boolean => purposeIds.indexOf(purpose.purposeId) < 0);
                    });
                }
            });
        this.cookiePolicyService.getUnAssignedGroupByDomainId(this.domainId).then(
            (response: any): any => {
                this.unassignedCookie = head(response);
                this.unassignedGroupId = this.unassignedCookie.groupId;
                this.cookieCategoryOptions.unshift({ id: this.unassignedCookie.groupId, name: this.unassignedCookie.name });
                this.unAssignedCookieCount = this.UnAssignedCookieCount();
                this.pageReadyUnassign = true;
            });
        this.cookiePolicyService.getConsentSettings(this.domainId)
            .then((response: IProtocolResponse<ICookieListConsentDetail>): void => {
                this.pageReadyLevel = true;
                if (!response.result) return;
                this.preferenceCenterData = response.data;
                this.isDntEnabled = this.preferenceCenterData.isDntEnabled;
                this.cookieListConfigurationDetail = {
                    CookiesText: this.preferenceCenterData.CookiesText,
                    CategoriesText: this.preferenceCenterData.CategoriesText,
                    LifespanText: this.preferenceCenterData.LifespanText,
                    IsLifespanEnabled: this.preferenceCenterData.IsLifespanEnabled,
                    LifespanTypeText: this.preferenceCenterData.LifespanTypeText,
                    LifespanDurationText: this.preferenceCenterData.LifespanDurationText,
                };
            });
    }

    public updateDropGroup = (cookieId: number, groupId: number): boolean => {
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.cookiePolicyService.moveCookie(this.domainId, cookieId, groupId);
        return true;
    }

    public UnAssignedCookieCount = (): number => {
        let cookieCount = 0;
        each(this.unassignedCookie.hosts, (host: any): void => {
            cookieCount += host.numberOfCookies;
        });
        return cookieCount + this.unassignedCookie.cookies.length;
    }

    onDntValueChange(): void {
        this.isDntEnabled = !this.isDntEnabled;
        this.preferenceCenterData.isDntEnabled = this.isDntEnabled;
        some(this.groups, (group: ICookieGroupDetail): void => {
            if (group.purposeGroupId === PurposeGroupId.TargettingCookies) {
                group.isDntEnabled = this.preferenceCenterData.isDntEnabled;
            }
        });
    }

    updateGroupId = (groupId: number, customGroupId: string, subGroupId?: number) => {
        const groups = this.preferenceCenterData.consentSettings.groupStatuses;
        const currentGroup = find(groups, (group: IConsentGroupStatusDetail) => group.groupId === groupId);
        if (currentGroup) {
            if (subGroupId) {
                some(currentGroup.subGroups, (e: ISubGroupDetail): boolean => {
                    if (e.id === subGroupId) {
                        e.customGroupId = customGroupId;
                        return true;
                    }
                    return false;
                });
            } else {
                currentGroup.customGroupId = customGroupId;
            }
        } else {
            some(this.groups, (e: ICookieGroupDetail): boolean => {
                if (e.groupId === groupId) {
                    e.customGroupId = customGroupId;
                    return true;
                }
                return false;
            });
        }
    }
}

export default {
    bindings: {},
    controller: CcPolicy,
    template: `
			<section class="cc-policy">
				<cc-policy-header ng-if="$ctrl.selectedWebsite"
					class="cc-policy__header"
					populate-policy="$ctrl.populateData"
					is-unpublished-changes="$ctrl.isUnpublishedChanges"
                    page-ready = "$ctrl.pageReadyAssign"
                    groups="$ctrl.groups"
                    is-dnt-enabled="$ctrl.isDntEnabled"
                    domain-id="::$ctrl.domainId"
                    cookie-list-detail="$ctrl.preferenceCenterData"
                    cookie-list-configuration-detail="$ctrl.cookieListConfigurationDetail"
				>
				</cc-policy-header>
				<div class="module-wrapper" ng-if="!($ctrl.pageReadyAssign && $ctrl.pageReadyUnassign && $ctrl.pageReadyLevel)">
					<loading text="{{ ::labels.loading }}" >
					</loading>
				</div>
                <div class="cookiecompliance__edit-wrapper flex" ng-if="($ctrl.pageReadyAssign && $ctrl.pageReadyUnassign && $ctrl.pageReadyLevel)">
					<cc-policy-menu-accordians
						class="cc-policy__unassigned-list stretch-vertical"
						domain-id="::$ctrl.domainId"
						unassigned-cookie="$ctrl.unassignedCookie"
						populate-policy="$ctrl.populateData()"
                        is-unpublished-changes="$ctrl.isUnpublishedChanges"
                        show-iab-settings="$ctrl.preferenceCenterData.IsIABEnabled && $ctrl.canShowIABSettings"
                        is-dnt-enabled="$ctrl.isDntEnabled"
						reload-group="$ctrl.reloadGroup"
						cookie-count="$ctrl.unAssignedCookieCount"
						consent-settings="$ctrl.preferenceCenterData"
                        purposes="$ctrl.purposes"
                        cookie-list-configuration-detail="$ctrl.cookieListConfigurationDetail"
                        dnt-value-change-callback="$ctrl.onDntValueChange()"
					>
					</cc-policy-menu-accordians>
					<cc-assigned-groups
						class="cc-policy__assigned-groups width-70-percent padding-all-2
						height-100 stretch-vertical cc-policy__assigned-groups--cards"
						domain-id="::$ctrl.domainId"
                        groups="$ctrl.groups"
                        unassigned-group-id=$ctrl.unassignedGroupId
                        cookie-category-options="$ctrl.cookieCategoryOptions"
                        is-unpublished-changes="$ctrl.isUnpublishedChanges"
                        is-dnt-enabled="$ctrl.preferenceCenterData.isDntEnabled"
                        show-iab-settings="$ctrl.preferenceCenterData.IsIABEnabled && $ctrl.canShowIABSettings"
                        populate-policy="$ctrl.populateData()"
                        populate-un-assigned="$ctrl.populateUnAssigned()"
                        update-group-id="$ctrl.updateGroupId"
					>
					</cc-assigned-groups>
				</div>
			</section>
			`,
};
