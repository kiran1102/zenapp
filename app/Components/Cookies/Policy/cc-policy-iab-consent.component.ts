// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { IPurposes } from "interfaces/cookies/cc-iab-purpose.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Services
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";

class IabConsent implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePolicyService",
        "Permissions",
    ];

    domainId: number;
    iabSettingsSaveInProgress = false;
    isIabEnabled: boolean;
    purposes: IPurposes;
    selectedList: number;
    isIabThirdPartyCookieEnabled: boolean;
    translate = this.$rootScope.t;
    cookieEnableIabThirdPartyCookie = false;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private cookiePolicyService: CookiePolicyService,
        private permissions: Permissions,
    ) { }

    $onInit(): void {
        this.$rootScope.$on("updatePurposes", (event: ng.IAngularEvent, data: IPurposes): void => {
            this.purposes.push(...data);
        });
        this.cookieEnableIabThirdPartyCookie = this.permissions.canShow("CookieEnableIabThirdPartyCookie");
    }

    onIabSettingsToggle(): void {
        this.isIabEnabled = !this.isIabEnabled;
        this.updateIabConfiguration();
    }

    onVendorSelection(selection: number): void {
        this.selectedList = selection;
        this.updateIabConfiguration();
    }

    onIabThirdPartyCookieToggle(): void {
        this.isIabThirdPartyCookieEnabled = !this.isIabThirdPartyCookieEnabled;
        this.updateIabConfiguration();
    }

    private updateIabConfiguration(): void {
        this.iabSettingsSaveInProgress = true;
        const payload = {
            IsIABEnabled: this.isIabEnabled,
            SelectedList: this.selectedList,
            IsIabThirdPartyCookieEnabled: this.isIabThirdPartyCookieEnabled,
        };
        this.cookiePolicyService.updateIabConfiguration(this.domainId, payload)
            .then((response: IProtocolResponse<void>): void => {
                this.iabSettingsSaveInProgress = false;
                if (!response.result) {
                    this.isIabEnabled = !this.isIabEnabled;
                }
            });
    }
}

export default {
    controller: IabConsent,
    bindings: {
        purposes: "<",
        vendorsList: "<",
        selectedList: "=",
        isIabEnabled: "=",
        isIabThirdPartyCookieEnabled: "=",
        domainId: "<",
    },
    template: `
    <div class="text-small form-group">
        <one-label-content wrapper-class="margin-bottom-2 text-bold">{{::$ctrl.translate('EnableIAB')}}
            <downgrade-switch-toggle
                class="cc-accordion-navigation__item-content-toggle pull-right padding-bottom-1"
                [is-disabled]="$ctrl.iabSettingsSaveInProgress"
                [toggle-state]="$ctrl.isIabEnabled"
                (callback)="$ctrl.onIabSettingsToggle()">
            </downgrade-switch-toggle>
        </one-label-content>
        <one-label-content
            ng-if="$ctrl.isIabEnabled && $ctrl.cookieEnableIabThirdPartyCookie"
            wrapper-class="margin-bottom-2 text-bold"
        >
            {{::$ctrl.translate('EuConsentCookie')}}
            <i
                class="ot ot-info-circle text-large"
                uib-tooltip="{{::$ctrl.translate('IsIabThirdPartyCookieTooltipInfo')}}",
                tooltip-append-to-body="true",
                tooltip-trigger="mouseenter"
            />
            <downgrade-switch-toggle
                class="cc-accordion-navigation__item-content-toggle padding-bottom-1 pull-right"
                [is-disabled]="$ctrl.iabSettingsSaveInProgress"
                [toggle-state]="$ctrl.isIabThirdPartyCookieEnabled"
                (callback)="$ctrl.onIabThirdPartyCookieToggle()">
            </downgrade-switch-toggle>

        </one-label-content>
        <one-label-content
            ng-if="$ctrl.isIabEnabled"
            wrapper-class="margin-bottom-2 text-bold"
        >
            {{::$ctrl.translate('VendorVersionList')}}
            <one-select
                name="{{::$ctrl.translate('Status')}}"
                options="$ctrl.vendorsList"
                model="$ctrl.selectedList"
                on-change="$ctrl.onVendorSelection(selection)"
                label-key="name"
                value-key="id"
                is-disabled="$ctrl.iabSettingsSaveInProgress"
                select-class="cc-language-seletion__select"
            >
            </one-select>
        </one-label-content>
        <one-drop-list
            ng-if="$ctrl.isIabEnabled && $ctrl.purposes.length"
            class="full-width full-height position-relative purposes-list"
            wrap="true"
        >
            <ul dld-list="$ctrl.purposes">
                <li
                    class="template-nodes-group-node"
                    dnd-draggable="purpose"
                    dnd-moved="$ctrl.purposes.splice($index, 1)"
                    ng-repeat="purpose in $ctrl.purposes track by $index"
                >
                    <div class="template-nodes-group-node-name text-left">
                        {{purpose.purposeName}}
                        <i class="fa fa-bars pull-right"></i>
                    </div>
                </li>
            </ul>
        </one-drop-list>
    </div>
	`,
};
