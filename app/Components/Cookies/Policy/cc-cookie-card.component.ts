// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

// Services
import { WebAuditService } from "cookiesService/web-audit.service";

class CcCookieCard implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "WebAuditService",
    ];

    translate = this.$rootScope.t;
    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly WebAuditService: WebAuditService,
    ) { }

    showLifespanText(isSession: boolean, lifeSpan: number): string {
        return (isSession ? this.translate("Session") : (lifeSpan < 1 ? `<1 ${this.translate("Day")}` : (lifeSpan > 1 ? `${lifeSpan} ${this.translate("Days")}` : `${lifeSpan} ${this.translate("Day")}`)));
    }

    getClassNameFor(purpose: string): string {
        return this.WebAuditService.getCookieClass(purpose, "graph-legend__");
    }
}

const ccCookieCard = {
    bindings: {
        hideCardBody: "<?",
        cookie: "<",
        onDrag: "<?",
        canShowIcon: "<?",
        iconClass: "@?",
        iconCallback: "&?",
    },
    controller: CcCookieCard,
    template: `
		<one-draggable-card
			id="$ctrl.cookie"
			class="cc-cookie-card flex static-vertical"
            icon-class="{{$ctrl.iconClass}}"
            can-show-icon="$ctrl.canShowIcon"
			hide-card-body="$ctrl.hideCardBody"
            on-drag="$ctrl.onDrag"
            icon-callback="$ctrl.iconCallback(cookie)"
		>
			<card-title class="cc-cookie-card__card-title"
				ng-class="$ctrl.getClassNameFor($ctrl.cookie.purpose)">
                {{::$ctrl.cookie.name}}
			</card-title>
			<card-body>
				<p>
					<span class="text-bold">{{::$ctrl.translate("Purpose")}}:</span>
					{{::($ctrl.cookie.purpose || $ctrl.translate("Unknown"))}}
				</p>
				<p>
					<span class="text-bold">{{::$ctrl.translate("Lifespan")}}:</span>
					{{::$ctrl.showLifespanText($ctrl.cookie.session, $ctrl.cookie.lifeSpan)}}
				</p>
				<p class="cc-cookie-card_type text-uppercase pull-right">
					{{::$ctrl.translate("FirstParty")}}
				</p>
			</card-body>
		</one-draggable-card>
		`,
};

export default ccCookieCard;
