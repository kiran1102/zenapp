// 3rd Party
import {
    assign,
    each,
    find,
    findIndex,
    remove,
} from "lodash";

// interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IPurpose } from "interfaces/cookies/cc-iab-purpose.interface";
import { ICookies } from "interfaces/cookies/cc-cookie-category.interface";
import {
    ICookieGroupDetail,
    ICookieSubGroup,
    ICookieGroupMetaDetail,
} from "interfaces/cookies/cookie-group-detail.interface";

// Serices
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieIABIntegrationService } from "cookiesService/cookie-iab-integration.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ModalService } from "sharedServices/modal.service";

declare var OneConfirm: any;

class CcGroup implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePolicyService",
        "CookieComplianceStore",
        "ModalService",
        "$timeout",
        "CookieIABIntegrationService",
    ];

    cookieCount: number;

    private translate = this.$rootScope.t;
    private group: ICookieGroupDetail;
    private onDrop: any;
    private domainId: number;
    private reloadGroup = false;
    private isUnpublishedChanges: boolean;
    private triggerDeleteGroup: any;
    private deleteInProgress = false;
    private populateUnAssigned: () => void;
    private isDntEnabled: boolean;
    private updateGroupId: (groupId: number, customGroupId: string) => void;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookiePolicyService: CookiePolicyService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly modalService: ModalService,
        readonly $timeout: ng.ITimeoutService,
        readonly cookieIABIntegrationService: CookieIABIntegrationService) {
    }

    $onInit(): void {
        this.domainId = this.cookieComplianceStore.getSelectedDomainId();
        this.group.isExpanded = true;
    }

    public updateCount = (cookieCount: number, subgroupId: number): void => {
        const index: number = findIndex(this.group.subGroups, (subgroup: ICookieSubGroup): boolean => subgroup.id === subgroupId);
        this.group.subGroups[index].numberOfCookies = cookieCount;
    }

    public onDropCard = (entity: ICookies, index: number): boolean => {
        if (entity.hasOwnProperty("purposeName")) {
            return false;
        } else if (entity.id && entity.thirdParty === false) {
            return this.updateCookie(entity);
        } else if (entity.id) {
            return this.updateSubgroup(entity);
        } else if (entity.isParentGroup) {
            return false;
        }
        return this.updateHosts(entity);
    }
    public markPublish = (): void => {
        if (!this.isUnpublishedChanges) {
            this.isUnpublishedChanges = true;
        }
    }
    public updateCookie = (entity: any): boolean => {
        this.group.cookies.push(entity);
        this.cookiePolicyService.moveCookie(this.domainId, entity.id, this.group.groupId);
        this.markPublish();
        return true;
    }
    public updateSubgroup = (entity: any): boolean => {
        const isSameGroup = find(this.group.subGroups, (subgroup: ICookieSubGroup): boolean => entity.id === subgroup.id);
        if (!isSameGroup) {
            this.group.subGroups.push(entity);
            this.cookiePolicyService.UpdateSubgroup(this.domainId, this.group.groupId, entity.id);
            this.markPublish();
            return true;
        }
        return false;
    }
    public updateHosts = (entity: any): boolean => {
        const isSameGroup = find(this.group.subGroups, (subgroup: ICookieSubGroup): boolean => entity.groupId === subgroup.id);
        if (!isSameGroup) {
            this.reloadGroup = true;
            this.cookiePolicyService.moveHosts(this.domainId, entity.hostName, entity.groupId, this.group.groupId).then(
                (): void => {
                    this.cookiePolicyService.getGroupCookies(this.domainId, this.group.groupId).then(
                        (responseObject: any): any => {
                            this.group.subGroups = responseObject.subGroups;
                            this.group.isExpanded = true;
                            this.reloadGroup = false;
                            this.markPublish();
                        });
                });
            return true;
        }
        return false;
    }
    public onDragCard = (id: any): boolean => {
        remove(this.group.cookies, (cookie: any): boolean => cookie === id);
        return true;
    }

    editGroup(): void {
        this.modalService.setModalData({
            canShowDntOption: this.isDntEnabled,
            description: this.group.description,
            domainId: this.domainId,
            groupId: this.group.groupId,
            isDntEnabled: this.group.isDntEnabled,
            isNewGroup: false,
            isSubgroup: false,
            name: this.group.name,
            purposeGroupId: this.group.purposeGroupId,
            customGroupId: this.group.customGroupId,
            promiseToResolve: (group: ICookieGroupMetaDetail): Promise<boolean> => {
                this.updateGroupId(this.group.groupId, group.customGroupId);
                this.group = assign({}, this.group, group);
                this.markPublish();
                return Promise.resolve(true);
            },
        });
        this.modalService.openModal("ccGroupModal");
    }

    addSubgroup(): void {
        this.modalService.setModalData({
            canShowDntOption: false,
            domainId: this.domainId,
            groupId: this.group.groupId,
            isNewGroup: true,
            isSubgroup: true,
            purposeGroupId: this.group.purposeGroupId,
            promiseToResolve: (group: ICookieGroupDetail): Promise<boolean> => {
                this.group.subGroups.push({
                    id: group.groupId,
                    name: group.name,
                    numberOfCookies: 0,
                    orderPreview: 0,
                    parentId: this.group.groupId,
                    description: group.description,
                    customGroupId: group.customGroupId,
                });
                this.markPublish();
                return Promise.resolve(true);
            },
        });
        this.modalService.openModal("ccGroupModal");
    }

    public onDragSubgroup = (id: ICookieSubGroup): boolean => {
        remove(this.group.subGroups, (subgroup: ICookieSubGroup): boolean => subgroup === id);
        return true;
    }

    public onDragPurpose = (id: IPurpose): boolean => {
        remove(this.group.purposes, (purpose: IPurpose): boolean => purpose === id);
        return true;
    }

    public deleteGroup = (): void | undefined => {
        OneConfirm(this.translate("Group"), this.translate("AreYouSureYouWantToDeleteThisGroup"), "Confirm", "Yes", true, this.translate("Yes"), this.translate("No")).then((confirmDelete: boolean): void => {
            if (!confirmDelete) return;
            this.deleteInProgress = true;
            this.cookiePolicyService.deleteGroup(this.group.groupId).then((response: any): void => {
                if (response.result) {
                    this.triggerDeleteGroup(this.group);
                    if (this.cookieCount > 0) {
                        this.populateUnAssigned();
                    }
                }
                this.deleteInProgress = false;
            });
        });
    }

    public deleteSubGroup = (subGroup: ICookieSubGroup): void => {
        remove(this.group.subGroups, (subgrp: ICookieSubGroup): boolean => subgrp.id === subGroup.id);
        this.markPublish();
    }

    public onToggleGroup = (): void => {
        this.group.isExpanded = !this.group.isExpanded;
    }
    public onDropOrderSubgroup = (entity: any, index: number): boolean => {
        if (entity.hasOwnProperty("purposeName")) {
            return false;
        }
        let dropReturn = false;
        this.$timeout((): void => {
            const subgroup: any = find(this.group.subGroups, (sub: ICookieSubGroup): boolean => sub.id === entity.id);
            if (subgroup) {
                const groupIds: number[] = [];
                remove(this.group.subGroups, (sub: ICookieSubGroup): boolean => sub.id === entity.id);
                this.group.subGroups.splice(index - 1, 0, entity);
                each(this.group.subGroups, (group: ICookieSubGroup): void => {
                    groupIds.push(group.id);
                });
                this.cookiePolicyService.changeGroupOrder(groupIds);
                this.markPublish();
                dropReturn = true;
            }
        }, 10);
        return dropReturn;
    }
    public onDropOrderPurpose = (entity: IPurpose, index: number): boolean => {
        if (!entity.hasOwnProperty("purposeName")) {
            return false;
        }
        let dropReturn = true;
        if (entity.purposeName) {
            this.group.purposes.push(entity);
        }
        const purposeIds = this.group.purposes.map((purpose: IPurpose): number => purpose.purposeId);
        this.cookieIABIntegrationService.movePurposes(this.domainId, this.group.groupId, purposeIds).then(
            (response: boolean): void => {
                dropReturn = true;
                this.markPublish();
            }).catch(() => {
                this.group.purposes.pop();
                dropReturn = false;
            });
        return dropReturn;
    }

    public removeElement = (index: number): void => {
        const deletedPurpose = this.group.purposes.slice(index, index + 1);
        const deletedPurposeId = deletedPurpose[0].purposeId;
        this.cookieIABIntegrationService.deletePurposes(this.domainId, this.group.groupId, deletedPurposeId).then((response: boolean): void => {
            this.$rootScope.$broadcast("updatePurposes", deletedPurpose);
            this.group.purposes.splice(index, 1);
        });
    }
}

const ccGroup = {
    bindings: {
        group: "<",
        isUnpublishedChanges: "=",
        triggerDeleteGroup: "=",
        populateUnAssigned: "&",
        cookieCount: "<",
        isDntEnabled: "=",
        showIabSettings: "=",
        updateGroupId: "<",
    },
    controller: CcGroup,
    template:
        `<div>
            <one-draggable-element
                id="$ctrl.group"
                on-drag="$ctrl.onDragGroup"
                is-disabled="$ctrl.group.isExpanded"
            >
                <element>
                    <div class="cc-group margin-bottom-2">
                        <div class="cc-group__header-container">
                            <h4 ng-class="{ 'cc-group__heading': !$ctrl.group.isExpanded }">
                                <i
                                    class="fa fa-fw no-outline"
                                    ng-class="{'fa-caret-down': $ctrl.group.isExpanded, 'fa-caret-right': !$ctrl.group.isExpanded }"
                                    ng-click="$ctrl.onToggleGroup()"
                                />
                                {{$ctrl.group.name}}
                                <span class="padding-left-1 position-relative media-middle">
                                    <one-tag
                                        ng-if="$ctrl.group.isDntEnabled && $ctrl.isDntEnabled"
                                        plain-text="{{::$ctrl.translate('DoNotTrack')}}"
                                        label-class="text-bold one-tag--override one-tag--primary-white"
                                    ></one-tag>
                                </span>
                                <i
                                    class="cc-group__edit-icon fa fa-edit margin-left-1 no-outline"
                                    ng-if="!$ctrl.deleteInProgress"
                                    ng-click="$ctrl.editGroup()"
                                />
                                <i
                                    class="cc-group__delete-icon fa fa-trash-o no-outline padding-left-1"
                                    ng-if="$ctrl.group.canDelete && !$ctrl.deleteInProgress"
                                    ng-click="$ctrl.deleteGroup()"
                                />
                                <loading ng-show="$ctrl.deleteInProgress" show-text="false"/>
                                <span class="cc-group__group-count text-bold pull-right margin-top-1">
                                    <span ng-if="$ctrl.group.subGroups.length === 1">{{$ctrl.group.subGroups.length}} {{::$ctrl.translate("Group")}}</span>
                                    <span ng-if="$ctrl.group.subGroups.length > 1">{{$ctrl.group.subGroups.length}} {{::$ctrl.translate("Groups")}}</span>
                                    <span class="margin-left-1" ng-if="$ctrl.cookieCount === 1">{{$ctrl.cookieCount}} {{::$ctrl.translate("Cookie")}}</span>
                                    <span class="margin-left-1" ng-if="$ctrl.cookieCount > 1">{{$ctrl.cookieCount}} {{::$ctrl.translate("Cookies")}}</span>
                                </span>
                            </h4>
                            <div class="cc-group__group-description margin-top-1" ng-class="{'margin-bottom-1': !$ctrl.group.isExpanded}">
                                    {{$ctrl.group.description}}
                            </div>
                        </div>
                        <div uib-collapse="!$ctrl.group.isExpanded">
                            <one-drop-list
                                on-drop="$ctrl.onDropCard"
                                class="position-relative"
                            >
                                <div class="cc-group__loading full-width"
                                    ng-if="$ctrl.reloadGroup"
                                >
                                    <loading class="margin-all-2" show-text="false"/>
                                </div>
                                <div class="cc-group__body" ng-disabled="$ctrl.reloadGroup || $ctrl.deleteInProgress">
                                    <div class="cc-group__cookie-container row-flex-start row-wrap">
                                        <cc-cookie-card
                                            ng-repeat="cookie in $ctrl.group.cookies track by cookie.id"
                                            class="cc-unassigned-list__cookie-card flex static-vertical"
                                            hide-card-body="true"
                                            cookie="cookie"
                                            on-drag="$ctrl.onDragCard"
                                        ></cc-cookie-card>
                                    </div>
                                    <div class="margin-left-2">
                                        <one-drop-list on-drop="$ctrl.onDropOrderSubgroup">
                                            <cc-subgroup-summary
                                                ng-if="($ctrl.group.subGroups.length)>0"
                                                ng-repeat="subGroup in $ctrl.group.subGroups track by subGroup.id"
                                                class="cc-group__header-container"
                                                sub-group="subGroup"
                                                purpose-group-id="$ctrl.group.purposeGroupId"
                                                on-drag="$ctrl.onDragSubgroup"
                                                is-unpublished-changes="$ctrl.isUnpublishedChanges"
                                                is-dnt-enabled="$ctrl.isDntEnabled"
                                                calculate-count="$ctrl.updateCount"
                                                trigger-delete-sub-group= "$ctrl.deleteSubGroup"
                                                parent-group-id="$ctrl.group.groupId"
                                                update-group-id="$ctrl.updateGroupId"
                                                populate-un-assigned="$ctrl.populateUnAssigned()"
                                            ></cc-subgroup-summary>
                                        </one-drop-list>
                                    </div>
                                    <one-button
                                        button-class="margin-top-1 one-button--small"
                                        button-click="$ctrl.addSubgroup()"
                                        text="{{::$ctrl.translate('CreateSubgroup')}}"
                                        icon="fa fa-plus"
                                        identifier="cookiesAddGroup"
                                    ></one-button>
                                </div>
                        </div>
                    </div>
                </element>
            </one-draggable-element>
            </div>
            <div class="margin-left-2 cc-group purposes-container"
                ng-if="$ctrl.showIabSettings"
                ng-class="{'purposes-empty' : !$ctrl.group.purposes.length}"
            >
            <h4>{{::$ctrl.translate('Purposes')}}</h4>
            <one-drop-list
                class="full-width full-height position-relative"
                wrap="true"
                on-drop="$ctrl.onDropOrderPurpose"
            >
                <h4 class="purposes-drag-placeholder" ng-if="!$ctrl.group.purposes.length">{{::$ctrl.translate('AddPurposes')}}</h4>
                <ul dnd-list="$ctrl.group.purposes">
                    <li
                        class="angular-ui-tree-node"
                        ng-repeat="node in $ctrl.group.purposes track by $index"
                        on-drag="$ctrl.onDragPurpose"
                        dnd-draggable="node"
                        dnd-moved="$ctrl.group.purposes.splice($index, 1)"
                    >
                        {{node.purposeName}}
                        <i class="fa fa-2x fa-times-circle-o"
                            ng-click="$ctrl.removeElement($index)">
                        </i>
                    </li>
                </ul>
            </one-drop-list>
        </div>`,
};

export default ccGroup;
