// 3rd party
import {
    each,
    keyBy,
    keys,
    remove,
} from "lodash";

// Services
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { ModalService } from "sharedServices/modal.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    ICookieCategoryOptions,
    ICookieGroupDetail,
    ICookieSubGroup,
} from "interfaces/cookies/cookie-group-detail.interface";
import { ICookies } from "interfaces/cookies/cc-cookie-category.interface";

class CcAssignedGroups implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookieComplianceStore",
        "ModalService",
        "CookiePolicyService",
        "Permissions",
    ];
    canAddCustomCookie = false;
    isDntEnabled: boolean;

    private cookieCategoryOptions: ICookieCategoryOptions[];
    private domainId: number;
    private groups: ICookieGroupDetail[];
    private isRemoveOldCookiesDisabled = false;
    private isUnpublishedChanges: boolean;
    private pageReady = false;
    private populatePolicy: () => void;
    private populateUnAssigned: () => void;
    private removeOldCookiesInProgress = false;
    private showIabSettings: boolean;
    private translate = this.$rootScope.t;
    private unassignedGroupId: number;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly modalService: ModalService,
        readonly cookiePolicyService: CookiePolicyService,
        private readonly permissions: Permissions,
    ) {
        this.translate = $rootScope.t;
    }

    $onInit(): void {
        this.canAddCustomCookie = this.permissions.canShow("CookieAddCustomCookie");
    }

    public markPublish = (): void => {
        if (!this.isUnpublishedChanges) {
            this.isUnpublishedChanges = true;
        }
    }

    public addGroup(): void {
        this.modalService.setModalData({
            canShowDntOption: this.isDntEnabled,
            domainId: this.domainId,
            groupId: 0,
            isDntEnabled: false,
            isNewGroup: true,
            isSubgroup: false,
            promiseToResolve: (response: ICookieGroupDetail): Promise<boolean> => {
                this.groups.push({
                    ...response,
                    isParentGroup: true,
                    showIabSettings: this.showIabSettings,
                });
                this.markPublish();
                return Promise.resolve(true);
            },
        });
        this.modalService.openModal("ccGroupModal");
    }

    public deleteGroup = (group: ICookieGroupDetail): void => {
        remove(this.groups, (grp: ICookieGroupDetail): boolean => grp.groupId === group.groupId);
        this.markPublish();
    }

    public onDropGroup = (entity: any, indexToMove: number): boolean => {
        if (entity.isParentGroup) {
            const groupIds: number[] = [];
            remove(this.groups, (group: ICookieGroupDetail): boolean => group.groupId === entity.groupId);
            this.groups.splice(indexToMove - 1, 0, entity);
            each(this.groups, (group: ICookieGroupDetail): void => {
                groupIds.push(group.groupId);
            });
            this.cookiePolicyService.changeGroupOrder(groupIds);
            this.markPublish();
            return true;
        }
        return false;
    }

    public removeOldCookies = (): void => {
        this.removeOldCookiesInProgress = true;
        this.isRemoveOldCookiesDisabled = true;
        this.cookiePolicyService.getOldcookies(this.domainId)
            .then((response: ICookieGroupDetail[]): void => {
                this.removeOldCookiesInProgress = false;
                this.isRemoveOldCookiesDisabled = false;
                let cookieList: ICookies[] = [];
                if (response.length) {
                    each(response, (cookieGroup: ICookieGroupDetail): void => {
                        cookieList = cookieList.concat(cookieGroup.cookies);
                    });
                }
                this.modalService.setModalData({
                    cookies: cookieList,
                    promiseToResolve: (updatedCookieList: ICookies[]): void => this.deleteCookie(updatedCookieList),
                });
                this.modalService.openModal("removeCookieModal");
            });
    }

    calculateCookieCount = (group: ICookieGroupDetail): number => {
        let cookieCount = 0;
        each(group.subGroups, (subGroup: ICookieSubGroup): void => {
            cookieCount = cookieCount + subGroup.numberOfCookies;
        });
        cookieCount = cookieCount + group.cookies.length;
        return cookieCount;
    }

    onAddCustomCookieClick(): void {
        this.modalService.setModalData({
            categoryOptions: this.cookieCategoryOptions,
            domainId: this.domainId,
            promiseToResolve: (): void => this.populatePolicy(),
        });
        this.modalService.openModal("addCustomCookieModal");
    }

    private deleteCookie(cookieList: ICookies[]): void {
        const cookieIds = keys(keyBy(cookieList, "id"));
        this.isRemoveOldCookiesDisabled = true;
        this.removeOldCookiesInProgress = true;
        this.cookiePolicyService.deleteOldcookies(cookieIds).then((): void => {
            this.isRemoveOldCookiesDisabled = false;
            this.removeOldCookiesInProgress = false;
            this.populatePolicy();
        });
    }

}
const ccAssignedGroups = {
    bindings: {
        domainId: "<",
        groups: "=",
        cookieCategoryOptions: "<",
        unassignedGroupId: "<",
        isDntEnabled: "=",
        isUnpublishedChanges: "=",
        showIabSettings: "=",
        populatePolicy: "&",
        populateUnAssigned: "&",
        updateGroupId: "<",
    },
    controller: CcAssignedGroups,
    template: `<div class = "row">
					<div class="col-sm-12 text-right">
					    <one-button
							button-class="one-button--small"
							button-click="$ctrl.removeOldCookies()"
							text="{{::$ctrl.translate('RemoveOldCookies')}}"
							enable-loading="true"
							is-loading="$ctrl.removeOldCookiesInProgress"
							is-disabled="$ctrl.isRemoveOldCookiesDisabled",
							identifier="removeOldCookies"
						>
						</one-button>
                        <one-button
							button-class="one-button--small"
							button-click="$ctrl.addGroup()"
							text="{{::$ctrl.translate('CreateGroup')}}"
							icon="fa fa-plus"
							identifier="cookiesAddGroup"
						>
                        </one-button>
                        <one-button
                            ng-if="$ctrl.canAddCustomCookie"
							button-class="one-button--small"
							button-click="$ctrl.onAddCustomCookieClick()"
							text="{{::$ctrl.translate('AddCookie')}}"
							identifier="cookiesAddCustomcookie"
						>
						</one-button>
					</div>
				</div>
				<div class="cc-assigned-groups">
					<one-drop-list
						class="text-center flex"
						on-drop="$ctrl.onDropGroup"
					>
						<cc-group ng-repeat="group in $ctrl.groups"
							group="group"
							is-unpublished-changes="$ctrl.isUnpublishedChanges"
                            trigger-delete-group="$ctrl.deleteGroup"
                            is-dnt-enabled="$ctrl.isDntEnabled"
                            show-iab-settings="$ctrl.showIabSettings"
                            populate-un-assigned="$ctrl.populateUnAssigned()"
                            cookie-count="$ctrl.calculateCookieCount(group)"
                            update-group-id="$ctrl.updateGroupId"
						></cc-group>
					</one-drop-list>
				</div>
              `,
};

export default ccAssignedGroups;
