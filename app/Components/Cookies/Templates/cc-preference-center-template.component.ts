// 3rd party
import {
    replace,
    find,
    includes,
} from "lodash";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IPrefCenterPreviewCategory,
    IPrefCenterLanguageResources,
} from "interfaces/cookies/cc-pref-center.interface";
import { IPreviewStyle } from "interfaces/cookies/cc-preview-script.interface";
import { ITemplateJson } from "interfaces/cookies/cc-template-json.interface";

// Services
import { CookieComplianceStore } from "cookiesService/cookie-compliance-store.service";
import { CookiePreferenceCenterService } from "app/scripts/Cookies/Services/cookie-preference-center.service";
import { CookiePolicyService } from "app/scripts/Cookies/Services/cookie-policy.service";

// Constants
import { CookieGroup } from "constants/cookies/cookie-group.constant";

// Enums
import { EditablePreferenceItems } from "enums/cookies/preference-center.enum";

class PreferenceCenterTemplate implements ng.IComponentController {
    static $inject: string[] = [
        "$rootScope",
        "CookiePreferenceCenterService",
        "CookieComplianceStore",
        "CookiePolicyService",
    ];
    translate = this.$rootScope.t;
    public itemBeingEdited: EditablePreferenceItems = null;
    private templateJson: ITemplateJson;
    private isUnpublishedChanges: boolean;
    private selectedWebsite: string;
    private previewCssUrl: string;
    private previewLogoUrl: string;
    private previewStyle: IPreviewStyle;
    private categoryPreview: IPrefCenterPreviewCategory;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly cookiePreferenceCenterService: CookiePreferenceCenterService,
        readonly cookieComplianceStore: CookieComplianceStore,
        readonly cookiePolicyService: CookiePolicyService,
    ) { }

    public $onInit(): void {
        this.assignCategoriesForPreview();
        this.handleStyleChange(this.templateJson.prefCenterData.styling);
    }

    public handleStyleChange = (prefStyle: any, revertToDefault: boolean = false): void => {
        this.previewCssUrl = this.cookiePreferenceCenterService.getPreferencePreviewCssUrl(prefStyle.style);
        this.previewLogoUrl = revertToDefault ? null : this.cookiePreferenceCenterService.getPreferencePreviewLogo(prefStyle.style, prefStyle.logoFileData);
        this.previewStyle = { cssURL: this.previewCssUrl, logoURL: this.previewLogoUrl };
        if (!includes(this.templateJson.bannerModel.skinName, "default_flat_center")) {
            this.updateSkinName(prefStyle.style);
        }
    }

    public handleCallbackSubGroupOptOut(toggleStatus: boolean, isToggleTrigger: boolean): boolean {
        if (isToggleTrigger) {
            this.templateJson.prefCenterData.consentSettings.showSubgroupToggles = toggleStatus;
            return toggleStatus;
        } else {
            return this.templateJson.prefCenterData.consentSettings.showSubgroupToggles;
        }
    }

    private updateSkinName(styleType: string): void {
        let newSkinTextPart = "default_flat_";
        let oldSkinTextPart = "default_responsive_alert_";
        if (styleType && styleType.toUpperCase() === "CLASSIC") {
            newSkinTextPart = "default_responsive_alert_";
            oldSkinTextPart = "default_flat_";
        }
        this.templateJson.bannerModel.skinName = replace(this.templateJson.bannerModel.skinName, oldSkinTextPart, newSkinTextPart);
    }

    private assignCategoriesForPreview(): void {
        if (this.templateJson.prefCenterData.languageResources.length) {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.findCookieGroupName(CookieGroup.StrictlyNecessaryCookies),
                PerformanceCookies: this.findCookieGroupName(CookieGroup.PerformanceCookies),
            };
        } else {
            this.categoryPreview = {
                StrictlyNecessaryCookies: this.translate("StrictlyNecessary"),
                PerformanceCookies: this.translate("PerformanceCookies"),
            };
        }
    }

    private findCookieGroupName(groupKey: string): string {
        const languageResources = this.templateJson.prefCenterData.languageResources;
        const language = find(languageResources, (resource: IPrefCenterLanguageResources): boolean => resource.key === groupKey);
        return language.value;
    }

    private checkIsCenterTileSkin(): boolean {
        return includes(this.templateJson.bannerModel.skinName, "default_flat_center");
    }
}

export const preferenceCenterTemplate: ng.IComponentOptions = {
    bindings: {
        templateJson: "=",
    },
    controller: PreferenceCenterTemplate,
    template: `
		<div class="cc-preference-center full-size column-nowrap">
    		<div class="cc-banner-template__module-wrapper">
				<div class="flex full-size overflow-auto">
					<div class="cookiecompliance__edit-wrapper flex">
                        <cc-preference-center-accordions
							preference-center-data="$ctrl.templateJson.prefCenterData"
							is-unpublished-changes="$ctrl.isUnpublishedChanges"
							handle-style-change="$ctrl.handleStyleChange($ctrl.templateJson.prefCenterData.styling)"
							handle-callback-sub-group-opt-out="$ctrl.handleCallbackSubGroupOptOut(toggleStatus, isToggleTrigger)"
							use-template="true"
							is-center-tile-skin="$ctrl.checkIsCenterTileSkin()"
							>
						</cc-preference-center-accordions>
						<downgrade-cc-preference-center-preview class="flex margin-bottom-1"
							[(preference-center-data)]="$ctrl.templateJson.prefCenterData"
							[preview-style]="$ctrl.previewStyle"
							[category-preview]="$ctrl.categoryPreview"
                            [use-template]="true"
                            [is-center-tile-skin]="$ctrl.checkIsCenterTileSkin()"
                            [item-being-edited]="$ctrl.itemBeingEdited"
						>
						</downgrade-cc-preference-center-preview>
					</div>
				</div>
			</div>
		</div>

	`,
};

export default preferenceCenterTemplate;
