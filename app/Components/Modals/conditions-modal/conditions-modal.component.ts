declare var angular: any;

function controller() {
    return this.scope;
}

const conditionsTemplate = `
        <one-modal
            cancel="$ctrl.cancel"
            headline="{{::$ctrl.translate('Conditions')}}"
            >
            <modal-body class="flex flex-full-height">
                <form
                    class="conditions-modal__form width-100-percent stretch-vertical"
                    ng-disabled="$ctrl.submitInProgress"
                    name="conditionsForm"
                    >
                    <section
                        class="conditions-modal__header padding-bottom-1 row-vertical-center"
                        >
                        <button
                            class="conditions-modal__question-number text-bold"
                            >
                            {{::$ctrl.question.number}}
                        </button>
                        {{::$ctrl.question.description}}
                    </section>
                    <section
                        class="conditions-modal__body padding-all-2"
                        >
                        <span
                            class="conditions-modal__empty full-width row-horizontal-center"
                            ng-if="::$ctrl.isPublished && !$ctrl.conditions.length"
                            >
                            {{::$ctrl.translate('QuestionHasNoConditions')}}
                        </span>
                        <div
                            class="conditions-modal__condition margin-bottom-2 flex"
                            ng-repeat="condition in $ctrl.conditions track by condition.id"
                            >
                            <div
                                class="conditions-modal__parameters stretch-horizontal"
                                >
                                <div
                                    class="conditions-modal__comparison row-vertical-center stretch-horizontal"
                                    ng-repeat="comparison in condition.comparisons track by comparison.id"
                                    >
                                    <div
                                        class="conditions-modal__parameters-left text-right"
                                        >
                                        <span
                                            class="text-bold"
                                            ng-if="comparison.id === 0"
                                            >
                                            {{::$ctrl.translate('IfAnswerIs')}}
                                        </span>
                                        <one-select
                                            ng-if="comparison.id === 1 && !$ctrl.isPublished"
                                            options="$ctrl.ENUMS.ConditionOperators"
                                            model="comparison.conditionalOperator"
                                            on-change="$ctrl.setConditionalOperator({conditionIndex: condition.id, comparisonIndex: comparison.id, selection: selection})"
                                            label-key="Name"
                                            value-key="Value"
                                        >
                                        </one-select>
                                        <span
                                            class="conditions-modal__text"
                                            ng-if="comparison.id > 1 || (comparison.id !== 0 && $ctrl.isPublished)"
                                            >
                                                {{comparison.conditionalOperatorLabel}}
                                        </span>
                                    </div>
                                    <div
                                        class="conditions-modal__comparison-options flex stretch-horizontal"
                                        ng-if="!$ctrl.isPublished"
                                        >
                                        <one-select
                                            class="conditions-modal__select conditions-modal__select--half stretch-horizontal"
                                            options="$ctrl.relationalOperators"
                                            model="comparison.relationalOperator"
                                            on-change="$ctrl.setRelationalOperator({conditionIndex: condition.id, comparisonIndex: comparison.id, selection: selection})"
                                            label-key="label"
                                            value-key="value"
                                        >
                                        </one-select>
                                        <one-select
                                            ng-if="comparison.relationalOperator !== $ctrl.ENUMS.DMRelationalOperators.NotSure"
                                            class="conditions-modal__select conditions-modal__select--half stretch-horizontal"
                                            model="comparison.questionKey"
                                            on-change="$ctrl.setComparisonValue({conditionIndex: condition.id, comparisonIndex: comparison.id, selection: selection})"
                                            options="$ctrl.updatedQuestion.options"
                                            label-key="value"
                                            value-key="key"
                                        >
                                        </one-select>
                                    </div>
                                    <div
                                        class="conditions-modal__comparison-options flex stretch-horizontal"
                                        ng-if="::$ctrl.isPublished"
                                        >
                                        <span
                                            class="conditions-modal__text text-underline text-italic"
                                            >
                                                {{::comparison.relationalOperatorLabel}}
                                        </span>
                                        <span
                                            class="conditions-modal__text"
                                            >
                                                {{::comparison.questionValue}}
                                        </span>
                                    </div>
                                    <div
                                        class="conditions-modal__comparison-actions"
                                        ng-if="::!$ctrl.isPublished"
                                        >
                                        <button
                                            class="conditions-modal__btn padding-all-0 margin-left-1"
                                            ng-click="$ctrl.addComparison(condition.id, comparison.id)"
                                            >
                                            <i class="conditions-modal__btn-icon margin-all-0 fa fa-plus"></i>
                                        </button>
                                        <button
                                            ng-if="comparison.id !== 0"
                                            class="conditions-modal__btn padding-all-0 margin-left-1"
                                            ng-click="$ctrl.deleteComparison(condition.id, comparison.id)"
                                            >
                                            <i class="conditions-modal__btn-icon margin-all-0 fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </div>
                                <div
                                    class="conditions-modal__result row-vertical-center"
                                    >
                                    <span
                                        class="conditions-modal__parameters-left text-right text-bold"
                                        >
                                        {{::$ctrl.translate('ThenSkipTo')}}
                                    </span>
                                    <one-select
                                        ng-if="::!$ctrl.isPublished"
                                        class="conditions-modal__select stretch-horizontal"
                                        options="$ctrl.questionOptions"
                                        model="condition.result"
                                        on-change="$ctrl.setResult({conditionIndex: condition.id, selection: selection})"
                                        label-key="name"
                                        value-key="templateQuestionUniqueId"
                                    >
                                    </one-select>
                                    <span
                                        class="conditions-modal__text"
                                        ng-if="::$ctrl.isPublished"
                                        >
                                            {{::condition.resultLabel}}
                                    </span>
                                </div>
                            </div>
                            <div>
                                <button
                                    ng-if="::!$ctrl.isPublished"
                                    class="conditions-modal__btn padding-all-0 margin-left-1"
                                    ng-click="$ctrl.deleteCondition(condition.id)"
                                    >
                                    <i class="conditions-modal__btn-icon margin-all-0 fa fa-trash-o"></i>
                                </button>
                            </div>
                        </div>
                        <new-item-field
                            class="conditions-modal__add"
                            ng-if="::!$ctrl.isPublished"
                            field-class="conditions-modal__add"
                            text="{{::$ctrl.translate('AddCondition')}}"
                            icon-class="fa-plus"
                            handle-click="$ctrl.addCondition()"
                            >
                        </new-item-field>
                    </section>
                </form>
                <footer
                    class="conditions-modal__footer row-flex-end static-vertical"
                    ng-if="!$ctrl.isPublished"
                    >
                    <fieldset
                        ng-disabled="$ctrl.submitInProgress"
                        >
                        <one-button
                            class="margin-right-1"
                            button-class="conditions-modal__submit"
                            text="{{::$ctrl.translate('Save')}}"
                            button-click="$ctrl.handleSubmit()"
                            type="primary"
                            enable-loading="true"
                            is-loading="$ctrl.submitInProgress"
                            >
                        </one-button>
                        <one-button
                            class="conditions-modal__cancel"
                            text="{{::$ctrl.translate('Cancel')}}"
                            button-click="$ctrl.cancel()"
                            >
                        </one-button>
                    <fieldset>
                </footer>
            </modal-body>
        </one-modal>
	`;

const conditionsComponent: any = {
    template: conditionsTemplate,
    controller,
    bindings: {
        scope: "<",
    },
};

export default conditionsComponent;
