import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IKeyValue, IStringMap } from "interfaces/generic.interface";

interface IComparison {
    conditionalOperator: any;
    conditionalOperatorLabel: string;
    value: string;
    questionValue: string;
    questionKey: string;
    relationalOperator: string;
    relationalOperatorLabel: string;
}

interface IConditionalOperator {
    Name: string;
    Value: number;
}

class ConditionsModalCtrl {
    static $inject: string[] = ["$rootScope", "$scope", "$uibModalInstance", "ENUMS", "DMTemplate", "templateId", "templateStatusId", "section", "question"];

    private submitInProgress = false;
    private updatedQuestion: any = { ...this.question };
    private translate: any;
    private conditions: any[];
    private relationalOperators: Array<IStringMap<string>> = [];
    private isPublished: boolean;
    private questionOptions: any[];

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly $scope: any,
        readonly $uibModalInstance: any,
        readonly ENUMS: any,
        readonly DMTemplate: any,
        readonly templateId: string,
        readonly templateStatusId: number,
        readonly section: any,
        readonly question: any) {

        $scope.controller = this;
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.relationalOperators = [
            {
                label: this.translate("Equals"),
                value: this.ENUMS.DMRelationalOperators.Equals,
            },
            {
                label: this.translate("NotEquals"),
                value: this.ENUMS.DMRelationalOperators.NotEquals,
            },
        ];

        const notSureOperator: any = {
            label: this.translate("NotSure"),
            value: this.ENUMS.DMRelationalOperators.NotSure,
        };
        if (this.question.allowNotSure) {
            this.relationalOperators.push(notSureOperator);
        }
        this.isPublished = Boolean(this.templateStatusId === this.ENUMS.DMTemplateStates.Published);
        this.questionOptions = _.filter(this.section.questions, (question: any): boolean => {
            return question.id > this.updatedQuestion.id && !question.disabled;
        });
        this.conditions = this.parseConditions();
        this.formatConditions();
    }

    public handleSubmit = (): void => {
        this.submitInProgress = true;
        const request: any = {
            conditionGroups: this.conditions.length ? this.formatConditionGroups() : [],
            name: this.updatedQuestion.name,
        };
        this.DMTemplate.updateQuestion(this.templateId, this.section.templateSectionUniqueId, this.updatedQuestion.templateQuestionUniqueId, request).then( (response: any): undefined|void => {
            if (!response.result) return;
            this.$uibModalInstance.close({result: true, data: response.data});
        });
    }

    public cancel = (): void => {
        this.$uibModalInstance.close({result: false, data: null});
    }

    public addCondition = (): void => {
        const defaultConditionConfig: any = {
            id: this.conditions.length,
            comparisons: [],
            result: this.questionOptions[0].templateQuestionUniqueId,
            resultLabel: this.questionOptions[0].name,
        };
        this.conditions.push(defaultConditionConfig);
        this.addComparison(this.conditions.length - 1);
        this.formatConditions();
    }

    public deleteCondition = (conditionIndex: number): void => {
        this.conditions.splice(conditionIndex, 1);
        this.formatConditions();
    }

    public setConditionalOperator = (data: any): void => {
        if (data.hasOwnProperty("conditionIndex") && data.hasOwnProperty("comparisonIndex")) {
            this.extendConditionalOperator(data.conditionIndex, data.selection);
        }
    }

    public setRelationalOperator = (data: any): void => {
        if (data.hasOwnProperty("conditionIndex") && data.hasOwnProperty("comparisonIndex")) {
            this.conditions[data.conditionIndex].comparisons[data.comparisonIndex].relationalOperator = data.selection;
            if ((data.selection === this.ENUMS.DMRelationalOperators.Equals
            || data.selection === this.ENUMS.DMRelationalOperators.NotEquals)
            && this.conditions[data.conditionIndex].comparisons[data.comparisonIndex].questionKey === "") {
                this.conditions[data.conditionIndex].comparisons[data.comparisonIndex].questionKey = this.updatedQuestion.options[0].key;
                this.conditions[data.conditionIndex].comparisons[data.comparisonIndex].questionValue = this.updatedQuestion.options[0].value;
            }
        }
    }

    public setComparisonValue = (data: any): void => {
        if (data.hasOwnProperty("conditionIndex") && data.hasOwnProperty("comparisonIndex")) {
            this.conditions[data.conditionIndex].comparisons[data.comparisonIndex].questionKey = data.selection;
        }
    }

    public addComparison = (conditionIndex: number, comparisonIndex?: number): void => {
        const defaultComparisonConfig: any = {
            id: this.conditions[conditionIndex].comparisons.length,
            relationalOperator: this.relationalOperators[0].value,
            relationalOperatorLabel: this.relationalOperators[0].label,
            questionKey: this.updatedQuestion.options[0].key,
            questionValue: this.updatedQuestion.options[0].value,
        };
        this.conditions[conditionIndex].comparisons.splice(comparisonIndex + 1, 0, defaultComparisonConfig);
        this.formatComparisons(conditionIndex);
        this.extendConditionalOperator(conditionIndex);
    }

    public deleteComparison = (conditionIndex: number, comparisonIndex: number): void => {
        this.conditions[conditionIndex].comparisons.splice(comparisonIndex, 1);
        this.formatComparisons(conditionIndex);
    }

    public setResult = (data: any): void => {
        this.conditions[data.conditionIndex].result = data.selection;
    }

    private parseConditions(): any[] {
        const conditions: any[] = this.updatedQuestion.conditionGroups || [];
        return _.map(conditions, (condition: any): any => {
            const targetQuestion: any = _.find(this.questionOptions, {templateQuestionUniqueId: condition.targetQuestionId});
            const parsedCondition: any = {
                comparisons: this.parseComparisons(condition.conditions),
                result: targetQuestion ? condition.targetQuestionId : "",
                resultLabel: targetQuestion ? targetQuestion.name : "",
            };
            return parsedCondition;
        });
    }

    private parseComparisons(condition: IComparison[]): IComparison[] {
        const comparisons: IComparison[] = [];
        _.forEach(condition, (comparison: IComparison) => {
            comparison.conditionalOperatorLabel = comparison.conditionalOperator ? _.find<IConditionalOperator>(this.ENUMS.ConditionOperators, {Value: comparison.conditionalOperator}).Name : "";
            if (comparison.value === "notSure") {
                comparison.questionValue = "";
                comparison.questionKey = "";
                comparison.relationalOperator = this.ENUMS.DMRelationalOperators.NotSure;
                comparison.relationalOperatorLabel = this.translate("NotSure");
            } else {
                comparison.questionValue = comparison.value ? _.find<IKeyValue<string>>(this.updatedQuestion.options, { key: comparison.value }).value : "";
                comparison.questionKey = comparison.value ? _.find<IKeyValue<string>>(this.updatedQuestion.options, { key: comparison.value }).key : "";
                comparison.relationalOperatorLabel = comparison.relationalOperator ? _.find(this.relationalOperators, { value: comparison.relationalOperator }).label : "";
            }
            comparisons.push(comparison);
        });
        return comparisons;
    }

    private formatConditions(): void {
        _.forEach(this.conditions, ( condition: any, index: number ) => {
            condition.id = index;
            this.formatComparisons(index);
        });
    }

    private formatComparisons(conditionIndex: number): void {
        _.forEach(this.conditions[conditionIndex].comparisons, ( comparison: any, index: number ) => {
            comparison.id = index;
        });
    }

    private formatConditionGroups(): any[] {
        const conditionGroups: any[] = [];
        _.forEach(this.conditions, (condition: any): void => {
            const formattedCondition: any = {
                conditions: this.formatConditionResponse(condition),
                targetQuestionId: condition.result,
            };
            conditionGroups.push(formattedCondition);
        });
        return conditionGroups;
    }

    private formatConditionResponse(condition: any): any[] {
        const formattedComparisons: any[] = [];
        _.forEach(condition.comparisons, (comparison: any): void => {
            const formattedComparison: any = {};
            if (comparison.id !== 0) formattedComparison.conditionalOperator = comparison.conditionalOperator;
            formattedComparison.relationalOperator = comparison.relationalOperator;
            formattedComparison.value = comparison.questionKey;
            if (formattedComparison.relationalOperator === this.ENUMS.DMRelationalOperators.NotSure) {
                formattedComparison.relationalOperator = this.ENUMS.DMRelationalOperators.Equals;
                formattedComparison.value = "notSure";
            }
            formattedComparisons.push(formattedComparison);
        });
        return formattedComparisons;
    }

    private extendConditionalOperator(conditionIndex: number, operator?: number): void {
        const comparisons: any[] = this.conditions[conditionIndex].comparisons;
        _.forEach(comparisons, ( comparison: any ): void => {
            if (operator) {
                comparison.conditionalOperator = operator;
                comparison.conditionalOperatorLabel = _.find<IConditionalOperator>(this.ENUMS.ConditionOperators, { Value: operator }).Name;
            } else {
                comparison.conditionalOperator = comparisons[0].conditionalOperator ? comparisons[0].conditionalOperator : this.ENUMS.ConditionOperators[0].Value;
                comparison.conditionalOperatorLabel = comparisons[0].conditionalOperatorLabel ? comparisons[0].conditionalOperatorLabel : this.ENUMS.ConditionOperators[0].Name;
            }
        });
    }
}

export default ConditionsModalCtrl;
