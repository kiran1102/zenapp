export function buildDefaultModalTemplate(template: string): string {
    return `
        <section
            class="one-modal-new flex-full-height max-height-100-percent width-100-percent overflow-y-auto"
            ot-auto-id="OneModal"
        >
            <section class="one-modal-new__header row-space-between static-vertical margin-left-right-2">
                <h4
                    class="one-modal-new__title stretch-horizontal margin-all-0 text-bold">
                    {{$ctrl.modalTitle || $ctrl.translations.modalTitle}}
                </h4>
                <button
                    class="one-modal-new__close static-horizontal no-outline"
                    ot-auto-id="OneModalCloseButton"
                    ng-click='$ctrl.cancel ? $ctrl.cancel() : $ctrl.closeModal()'>
                        <i class="ot ot-times"></i>
                </button>
            </section>
            ${template}
        </section>
    `;
}
