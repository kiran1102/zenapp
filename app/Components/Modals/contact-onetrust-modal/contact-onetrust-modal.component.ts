import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { SupportService } from "oneServices/support.service";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";
import { IUser } from "interfaces/user.interface";
import { ISupportUpgrade, IContactOneTrustModalResolve } from "interfaces/support.interface";
import { IStore } from "interfaces/redux.interface";

class ContactOnetrustModalCtrl implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "ModalService",
        "SupportService",
        "store",
        "$timeout",
    ];

    private modalTitle: string;
    private headerText: string;
    private sendingMessage: boolean;
    private messageSubmitted = false;
    private resolve: IContactOneTrustModalResolve;
    private upgrade: ISupportUpgrade = {
        comment: "",
        module: "",
    };

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly ModalService: ModalService,
        private readonly SupportService: SupportService,
        private readonly store: IStore,
        private readonly $timeout: ng.ITimeoutService,
    ) {}

    public $onInit(): void {
        this.modalTitle = this.$rootScope.t("InterestedInThisIntegration");
        this.headerText = this.$rootScope.t("ContactOneTrustSupport");
    }

    private closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private sendMessage(): void {
        const currentUser: IUser = getCurrentUser(this.store.getState());
        this.sendingMessage = true;
        this.upgrade.firstName = currentUser.FirstName;
        this.upgrade.lastName = currentUser.LastName;
        this.upgrade.email =  currentUser.Email;
        this.upgrade.module = this.resolve.module;
        this.SupportService.upgrade(this.upgrade).then((response: IProtocolResponse<void>): void => {
            if (response.result) {
                this.messageSubmitted = true;
                this.$timeout((): void => {
                    this.ModalService.closeModal();
                    this.ModalService.handleModalCallback();
                    this.ModalService.clearModalData();
                }, 2000);
            } else {
                this.sendingMessage = false;
            }
        });
    }

}

const contactModal = `
    <section class="padding-all-2">
        <form name="contactForm" role="form" ng-if="!$ctrl.messageSubmitted">
            <div class="text-bold text-center padding-top-bottom-3">
                {{::$ctrl.headerText}}
            </div>
            <one-textarea
                name="contactModalComment"
                is-required="true"
                input-changed="$ctrl.upgrade.comment = value;"
                placeholder="{{::$root.t('EnterMessage')}}"
            ></one-textarea>
            <p
                ng-if="contactForm.contactModalComment.$invalid &&
                    (!contactForm.contactModalComment.$pristine ||
                    contactForm.contactModalComment.$touched)"
                class="text-error text-small"
                ng-bind="$root.t('PleaseEnterMessage')"
            ></p>
            <footer class="align-right margin-top-4">
                <one-button
                    identifier="cancelContactOnetrustMessage"
                    text="{{::$root.t('Cancel')}}"
                    class="padding-right-2"
                    button-click="$ctrl.closeModal()"
                    is-rounded="true"
                ></one-button>
                <one-button
                    identifier="sendContactOnetrustMessage"
                    type="primary"
                    text="{{::$root.t('Send')}}"
                    button-click="$ctrl.sendMessage()"
                    is-rounded="true"
                    enable-loading="true"
                    is-loading="$ctrl.sendingMessage"
                    is-disabled="$ctrl.sendingMessage ||
                        (contactForm.contactModalComment &&
                        contactForm.contactModalComment.$invalid)"
                ></one-button>
            </footer>
        </form>
        <section
            ng-if="$ctrl.messageSubmitted"
            class="help-modal__thanks"
            >
            <h3 class="help-modal__thanks-title" ng-bind="::$root.t('ThankYou')"></h3>
            <p class="help-modal__thanks-text" ng-bind="::$root.t('MessageSubmittedToCustomerService')"></p>
        </section>
    </section>
`;

export const contactOnetrustModal: ng.IComponentOptions =  {
    controller: ContactOnetrustModalCtrl,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(contactModal),
};
