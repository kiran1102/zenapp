// Angular
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// 3rd Party Library
import { isFunction } from "lodash";

// Interface
import { ModalService } from "sharedServices/modal.service";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";
import { IStore } from "interfaces/redux.interface";

// Pipes
import { TranslatePipe } from "modules/pipes/translate.pipe";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { StoreToken } from "tokens/redux-store.token";

@Component({
    selector: "ot-delete-confirmation-modal",
    templateUrl: "./ot-delete-confirmation-modal.component.html",
})

export class OtDeleteConfirmationModal implements OnInit {

    modalData: IDeleteConfirmationModalResolve;
    promiseToResolve: () => ng.IPromise<boolean>;
    modalTitle: string;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    isSubmitting: boolean;
    callback: () => void;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private translatePipe: TranslatePipe,
        private readonly modalService: ModalService,
    ) {}

    ngOnInit(): void {
        this.modalData = getModalData(this.store.getState());
        this.promiseToResolve = this.modalData.promiseToResolve;
        this.callback = this.modalData.callback;
        this.modalTitle = this.modalData.modalTitle;
        this.confirmationText = this.modalData.confirmationText;
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("Submit");
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                } else {
                    this.isSubmitting = false;
                }
            });
        } else if (isFunction(this.callback)) {
            this.callback();
            this.closeModal();
        }
    }
}
