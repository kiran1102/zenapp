// External libraries
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Enums
import { KeyCodes } from "enums/key-codes.enum";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { DMTemplateService } from "datamappingservice/Template/dm-template.service";

// Interfaces
import { ISectionEditModalResolve } from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

@Component({
    selector: "section-edit-modal",
    templateUrl: "./section-edit-modal.component.html",
})
export class SectionEditModalComponent implements OnInit {

    sectionId: string;
    isNewSection: boolean;
    previousSectionId: string;
    templateId: string;
    section: any;
    disabled = false;
    emptyName = true;
    public modalData: ISectionEditModalResolve;
    private callback: (response) => void;

    constructor(
        @Inject(StoreToken) readonly store: IStore,
        readonly DMTemplateService: DMTemplateService,
        readonly modalService: ModalService,
     ) { }

    ngOnInit() {
        this.modalData = getModalData(this.store.getState());
        this.emptyName = this.modalData.sectionName.length === 0 ? true : false;
        this.templateId = this.modalData.templateId;
        this.sectionId = this.modalData.sectionId;
        this.previousSectionId = this.modalData.previousSectionId;
        this.callback = this.modalData.callback;
        this.section = this.modalData.section;
        this.isNewSection = this.modalData.isNewSection;
    }

    valuechange(newValue: string) {
        this.modalData.sectionName = newValue;
        this.emptyName = this.modalData.sectionName.length === 0 ? true : false;
    }

    onKeyPress($event: KeyboardEvent) {
        if ($event.keyCode === KeyCodes.Enter)
            $event.preventDefault();
    }

    handleSubmit(sectionName: string) {
        this.disabled = true;
        const request: any = {
            name: sectionName,
            description: "",
            previousSectionId: this.previousSectionId,
        };
        if (this.isNewSection) {
            this.DMTemplateService.addSection(this.templateId, request).then((response: IProtocolResponse<string>): void => {
                if (!response.result) return;
                if (this.modalData.callback) {
                    this.callback(response);
                }
                this.disabled = true;
                this.closeModal();
            });
        } else {
            this.DMTemplateService.updateSection(this.templateId, this.section.templateSectionUniqueId, request).then((response: IProtocolResponse<string>): void => {
                if (!response.result) return;
                if (this.modalData.callback) {
                    this.callback(response);
                }
                this.disabled = true;
                this.closeModal();
            });
        }
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}
