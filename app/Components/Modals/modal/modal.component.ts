export default {
    bindings: {
        headline: "@",
        cancel: "<",
        bodyClass: "@?",
    },
    transclude: {
        body: "modalBody",
    },
    template: `
        <section class="one-modal flex-full-height">
            <section class="one-modal__header row-space-between static-vertical border-bottom text-large">
                <span class="one-modal__title stretch-horizontal margin-all-0" ng-bind="$ctrl.headline"></span>
                <button
                    class="one-modal__close static-horizontal"
                    ng-if="$ctrl.cancel"
                    ng-click='$ctrl.cancel()'
                    >
                    <i class="ot ot-times"></i>
                </button>
            </section>
            <section
                class="one-modal__body flex stretch-vertical {{::$ctrl.bodyClass}}"
                ng-transclude="body"
                >
            </section>
        </section>
    `,
};
