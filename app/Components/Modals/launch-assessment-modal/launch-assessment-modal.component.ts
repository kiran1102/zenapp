// 3rd Party
import {
    Component,
    OnInit,
    Inject,
} from "@angular/core";
import {
    isArray,
    cloneDeep,
    forEach,
    merge,
    remove,
    isEqual,
    find,
    includes,
} from "lodash";
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getStatusMap } from "oneRedux/reducers/inventory.reducer";
import {
    getFilterData,
    getInventoryId,
    getRecordById,
    isShowingFilteredResults,
    getCurrentRecord,
} from "oneRedux/reducers/inventory.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Services
import Utilities from "Utilities";
import { DMAssessmentsService } from "datamappingservice/Assessment/dm-assessments.service";
import { Settings } from "sharedServices/settings.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { TimeStamp } from "oneServices/time-stamp.service";

// Constants and Enums
import {
    STATUS,
    NamesAttributes,
    OwnerAttributes,
    ManagingOrgAttributes,
} from "constants/inventory.constant";
import { Regex } from "constants/regex.constant";
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// Interface
import { IStringMap } from "interfaces/generic.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { IProjectSettings } from "interfaces/setting.interface";
import {
    ILaunchModalResolve,
    IAttribute,
    IRecordAssignment,
    IAssessmentStatusMap,
    ILaunchResponse,
    IInventoryRecord,
} from "interfaces/inventory.interface";
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IFilterTree } from "interfaces/filter.interface";

interface IInventoryColumnValue {
    ColumnEmail: string;
    ColumnOrgGroupId: string;
    ColumnOrgGroup: string;
    ColumnValue: string;
    ColumnValueId: string;
    Filtered?: boolean;
}

interface IInventoryItem {
    Comment: string;
    Deadline: string | Date | VarDate;
    Description: string;
    InventoryId: string;
    LeadId: string;
    Name: string;
    OrgGroupId: string;
    TemplateId?: string;
    TemplateType?: number;
    TemplateVersion?: number;
    Prepopulate?: boolean;
    ReminderDays: number;
    InventoryName: string;
    Tags: string[];
    defaultRespondent?: string;
    showingDetails?: boolean;
    showingReminder?: boolean;
    errors?: IStringMap<boolean>;
    deadlineFormatted?: string;
}

interface IInventoryRequest {
    approver: string;
    inventoryId: string;
    assignedUser: string;
    name: string;
    inventoryName: string;
    orgGroupId: string;
    templateType: number;
    prepopulate: boolean;
    comment?: string;
    deadlineDate?: Date;
}

@Component({
    selector: "launch-assessment-modal",
    templateUrl: "./launch-assessment-modal.component.html",
})

export class LaunchAssessmentModalComponent implements OnInit {

    dateFormat = this.timeStamp.getDatePickerDateFormat();
    disableUntilDate = this.timeStamp.getDisableUntilDate();
    itemValidMap: IStringMap<boolean> = {};
    lastFocusedInput: Element;
    isReady = false;
    isSaving = false;
    users: IUser[] | IInventoryColumnValue[];
    canPrepopulate: boolean;
    tableName: string;
    showingSuccessMessage = false;
    modalTitle: string;
    showingFilteredResults: boolean;
    filterData: IFilterTree;
    hasValidUsers: boolean;
    userDropdownType: OrgUserDropdownTypes = OrgUserDropdownTypes.AssignRespondentDm;

    private modalData: ILaunchModalResolve;
    private items: IInventoryItem[] = [];
    private projectSettings: IProjectSettings;
    private canSubmit = true;
    private processes: IRecordAssignment[];
    private inventoryId: number;
    private currentRecord: IInventoryRecord;
    private templateType: number;
    private callback: (response: IProtocolResponse<ILaunchResponse[]>) => void;

    constructor(
        @Inject(StoreToken) private store: IStore,
        public settings: Settings,
        public dMAssessments: DMAssessmentsService,
        public permissions: Permissions,
        public modalService: ModalService,
        public inventoryAction: InventoryActionService,
        public inventoryAdapter: InventoryAdapter,
        readonly timeStamp: TimeStamp,
    ) {
        this.canPrepopulate = permissions.canShow("DataMappingAssessmentPrepopulate");
    }

    ngOnInit() {
        const state = this.store.getState();
        this.modalData = getModalData(state);
        this.showingFilteredResults = isShowingFilteredResults(state);
        this.filterData = getFilterData(state);
        this.inventoryId = getInventoryId(state);
        this.currentRecord = getCurrentRecord(state);
        this.modalTitle = this.modalData.modalTitle;
        this.processes = this.modalData.Processes;
        this.templateType = this.modalData.TemplateType;
        this.tableName = this.modalData.TableName;
        this.callback = this.modalData.callback;

        const promiseArray: Array<ng.IPromise<IProtocolPacket>> = [
            this.getSettings(),
        ];

        Promise.all(promiseArray).then(() => {
            this.getItems();
            forEach(
                this.items,
                (item: IInventoryItem) => {
                    this.setRecordName(item);
                    this.setUpToggles(item);
                },
            );
            this.isReady = true;
        });
    }

    deleteItem = (selected: IInventoryItem) => {
        remove(this.items,
            (item: IInventoryItem): boolean => {
                const isSelected = selected === item;
                if (isSelected) delete this.itemValidMap[item.InventoryId];
                return isSelected;
            },
        );
        this.hasValidUsers = this.checkForValidUsers();

        // If deleting the last item in the list, close modal
        if (!this.items.length) {
            this.closeModal();
        } else {
            this.validateItems();
        }
    }

    toggleDetails = (item: IInventoryItem) => {
        item.showingDetails = !item.showingDetails;
    }

    toggleReminder = (item: IInventoryItem) => {
        item.showingReminder = !item.showingReminder;
        this.validateReminderItem(item);
    }

    togglePrepopulate = (item: IInventoryItem) => {
        item.Prepopulate = !item.Prepopulate;
    }

    changeDate(date: any, item: IInventoryItem) {
        item.deadlineFormatted = date.formatted;
        item.Deadline = date.jsdate;
        this.validateIfHasErrors(item, "reminder");
    }

    applyToAll = (keys: string[], baseItem: IInventoryItem) => {
        if (isArray(keys) && baseItem) {
            forEach(this.items, (item: IInventoryItem) => {
                if (!isEqual(item, baseItem)) {
                    forEach(keys, (key: string) => {
                        item[key] = cloneDeep(baseItem[key]);
                    });
                }
            });
            this.validateItems();
        }
    }

    sendAssessments = () => {
        this.validateItems();
        if (this.canSubmit) {
            this.isSaving = true;
            const requests: IInventoryRequest[] = this.serializeRequests(this.items);
            this.bulkCreate(requests);
        }
    }

    validateIfHasErrors = (item: IInventoryItem, errorKey: string) => {
        this.setUpToggles(item);

        if (item.errors && item.errors[errorKey]) {
            this.validateItems();
        }
    }

    orgUserSelected(org: any, item: IInventoryItem) {
        if (org.valid) {
            org.selection ?
            item.LeadId = org.selection.Id ? org.selection.Id : org.selection
            : item.LeadId = null;
        }
        this.itemValidMap[item.InventoryId] = org.isValid;
        this.hasValidUsers = this.checkForValidUsers();
        if (!this.canSubmit) {
            this.validateItems();
        }
    }

    validateItems() {
        this.canSubmit = true;
        forEach(
            this.items,
            (item: IInventoryItem) => {
                item.errors = item.errors || {};
                item.errors.reminder = this.validateReminder(item);
                item.errors.lead = this.validateIfHasRespondent(item);
                item.errors.name = this.validateAssessmentName(item);
                if (item.errors.lead || item.errors.reminder || item.errors.name) {
                    this.canSubmit = false;
                }
            },
        );
    }

    trackByFn(index: number, item: IInventoryItem): string {
        return item.InventoryId;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private getItems() {
        const item: IInventoryItem = {
            Name: "",
            Description: "",
            OrgGroupId: "",
            LeadId: "",
            Deadline: "",
            ReminderDays: 0,
            Comment: "",
            Tags: [],
            InventoryId: "",
            Prepopulate: true,
            InventoryName: "",
        };
        const inventoryItems: IInventoryItem[] = [];
        if (isArray(this.processes) && this.processes.length) {
            forEach(
                this.processes,
                (proc: IRecordAssignment): void => {
                    const record = this.currentRecord && proc.InventoryId === this.currentRecord.id ? this.currentRecord : getRecordById(proc.InventoryId)(this.store.getState());
                    proc.Name = this.inventoryAdapter.getCellLabelByAttributeCode(record, NamesAttributes[this.inventoryId]);
                    proc.OrgGroupId = this.inventoryAdapter.getCellValuePropByAttributeCode(record, ManagingOrgAttributes[this.inventoryId], "valueId");
                    proc.defaultRespondent = this.inventoryAdapter.getCellValuePropByAttributeCode(record, OwnerAttributes[this.inventoryId], "valueId")
                                          || this.inventoryAdapter.getCellValuePropByAttributeCode(record, OwnerAttributes[this.inventoryId], "value");
                    const defaultItem: IInventoryItem = cloneDeep(item);
                    if (proc.defaultRespondent) {
                        const isValidRespondent: boolean = Utilities.matchRegex(proc.defaultRespondent, Regex.GUID) || Utilities.matchRegex(proc.defaultRespondent, Regex.EMAIL);
                        defaultItem.LeadId = isValidRespondent ? proc.defaultRespondent : "";
                    }
                    this.itemValidMap[proc.InventoryId] = Boolean(proc.defaultRespondent);
                    inventoryItems.push(merge({}, defaultItem, proc));
                },
            );
            this.hasValidUsers = this.checkForValidUsers();
        }
        this.items = inventoryItems;
    }

    private checkForValidUsers(): boolean {
        return !includes(this.itemValidMap, false);
    }

    private getSettings(): ng.IPromise<IProtocolPacket> {
        if (!this.permissions.canShow("Assessments")) {
            return this.settings.getProject().then(
                (response: IProtocolPacket): IProtocolPacket => {
                    if (response.result) {
                        this.projectSettings = response.data;
                        this.projectSettings.MinDate = new Date();
                    }
                    return response;
                },
            );
        }
    }

    private setRecordName(item: IInventoryItem) {
        item.InventoryName = item.Name;
    }

    private setUpToggles = (item: IInventoryItem) => {
        if (!item.Deadline) {
            item.showingReminder = false;
            item.ReminderDays = (this.projectSettings && this.projectSettings.Reminder && this.projectSettings.Reminder.Show) ? this.projectSettings.Reminder.Value : null;
        }
    }

    private serializeRequests(items: IInventoryItem[]): IInventoryRequest[] {
        const requests: IInventoryRequest[] = [];
        if (isArray(items)) {
            forEach(
                items,
                (item: IInventoryItem) => {
                    const createRequest: IInventoryRequest = {
                        approver: item.LeadId,
                        inventoryId: item.InventoryId,
                        assignedUser: item.LeadId,
                        name: item.Name,
                        inventoryName: item.InventoryName,
                        orgGroupId: item.OrgGroupId,
                        templateType: this.templateType,
                        prepopulate: item.Prepopulate,
                    };
                    if (item.Deadline) {
                        createRequest.deadlineDate = item.Deadline as Date;
                    }
                    if (item.Comment) {
                        createRequest.comment = item.Comment;
                    }
                    requests.push(createRequest);
                },
            );
        }
        return requests;
    }

    private bulkCreate = (requests: IInventoryRequest[]) => {
        this.dMAssessments.dataMappingBulkCreate(requests).then(this.handleModalResponse(requests));
    }

    private handleModalResponse = (requests: IInventoryRequest[]): (response: IProtocolResponse<ILaunchResponse[]>) => void => {
        const assessmentStatusMap: IAssessmentStatusMap = getStatusMap(this.store.getState());
        return (response: IProtocolResponse<ILaunchResponse[]>) => {
            this.isSaving = false;
            if (response.result) {
                this.showingSuccessMessage = true;
                const processesLaunched: string[] = [];
                forEach(
                    requests,
                    (request: IInventoryRequest) => {
                        const recordResponse: ILaunchResponse = find(response.data, (data: ILaunchResponse): boolean => data.name === request.name );
                        assessmentStatusMap[request.inventoryId] = { assessmentId: recordResponse.assessmentId, status: STATUS.InDiscovery };
                        processesLaunched.push(request.inventoryId);
                    },
                );
                if (this.callback) {
                    this.callback(response);
                }
                setTimeout(() => {
                    this.closeModal();
                }, 2000);
            }
        };
    }

    private validateReminderItem(item: IInventoryItem) {
        this.canSubmit = true;
        item.errors = item.errors || {};
        item.errors.reminder = this.validateReminder(item);
        if (item.errors.lead || item.errors.reminder) {
            this.canSubmit = false;
        }
    }

    private validateReminder(item: IInventoryItem): boolean {
        let invalidReminder = false;
        if (item.showingReminder) {
            if (item.ReminderDays) {
                const reminderDate: Date = new Date(item.Deadline as VarDate);
                reminderDate.setTime(reminderDate.getTime() - 1000 * 60 * 60 * 24 * item.ReminderDays);
                const today: Date = new Date();
                invalidReminder = (today.toLocaleDateString() === reminderDate.toLocaleDateString()) || (reminderDate < today);
            } else {
                invalidReminder = true;
            }
        }
        return invalidReminder;
    }

    private validateIfHasRespondent(item: IInventoryItem): boolean {
        return !item.LeadId;
    }

    private validateAssessmentName(item: IInventoryItem): boolean {
        return !item.Name;
    }
}
