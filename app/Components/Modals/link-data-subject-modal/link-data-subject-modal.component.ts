// 3rd Party
import {
    forEach,
    uniqBy,
    isUndefined,
    find,
} from "lodash";
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import {
    IInventoryCellValue,
    IDataCategory,
    ILinkedElement,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// Services
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryLinkingService } from "modules/inventory/services/adapter/inventory-linking.service";

@Component({
    selector: "link-data-subject-modal",
    templateUrl: "./link-data-subject-modal.component.html",
})
export class LinkDataSubjectModalComponent implements OnInit {
    modalTitle: string;
    submitButtonText: string;
    cancelButtonText: string;
    selectedSubjectType: IInventoryCellValue;
    selectedCategory: IDataCategory;
    currentRecordElements: ILinkedElement[] = [];
    elementList: IInventoryCellValue[] = [];
    currentElements: IInventoryCellValue[];
    elementMap: IStringMap<boolean> = {};
    elementsToAdd: Array<{ id: string }> = [];
    elementsToRemove: Array<{ inventoryAssociationId: string }> = [];
    isSaving = false;
    allSelected = false;
    dataSubjectTypeList: IInventoryCellValue[] = [];
    dataCategoryList: IDataCategory[] = [];
    recordInventoryType: number;
    orgGroupId: string;
    hasCategories = true;
    fetchingCategories = false;
    hasChanges = false;
    recordId: string;
    private modalData: IRelateInventoryModalResolve;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private modalService: ModalService,
        private inventoryLinkingService: InventoryLinkingService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit() {
        this.defaultCategory();
        const state = this.store.getState();
        this.modalData = getModalData(state);
        this.recordId = this.modalData.recordId;
        this.recordInventoryType = this.modalData.inventoryType;
        this.modalTitle = this.modalData.modalTitle || this.translatePipe.transform("ManageDataElements");
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("Update");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.orgGroupId = this.modalData.orgGroupId;
        this.getDataSubjectTypeList();
    }

    selectCategory(category: IDataCategory) {
        this.selectedCategory = category;
        this.currentElements = category.elements;
        this.allElementsSelected();
    }

    setElementMapForCategory(categoryList: IDataCategory[]) {
        forEach(categoryList, (category: IDataCategory) => {
            this.elementList = uniqBy([...this.elementList, ...category.elements], "valueId");
            forEach(category.elements, (element: IInventoryCellValue) => {
                if (isUndefined(this.elementMap[element.valueId])) {
                    this.elementMap[element.valueId] = Boolean(find(this.currentRecordElements, ["inventoryId", element.valueId]));
                }
            });
        });
    }

    selectSubjectType(selection: IInventoryCellValue) {
        this.defaultCategory();
        this.selectedSubjectType = selection;
        this.getDataCategoryList(this.selectedSubjectType.valueId);
    }

    selectAll() {
        const checkStatus: boolean = !this.allSelected;
        forEach(this.currentElements, (element: IInventoryCellValue) => {
            this.elementMap[element.valueId] = checkStatus;
        });
        this.allSelected = checkStatus;
        this.setAddRemoveElements();
    }

    toggleElement(elementId: string) {
        this.elementMap[elementId] = !this.elementMap[elementId];
        this.setAddRemoveElements();
        this.allElementsSelected();
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    setAddRemoveElements() {
        this.elementsToAdd = this.formatLinkData();
        this.elementsToRemove = this.formatRemoveLinkData();
        this.hasChanges = Boolean(this.elementsToAdd.length || this.elementsToRemove.length);
    }

    saveInventory() {
        this.isSaving = true;
        const promises = [];
        if (this.elementsToAdd.length) promises.push(this.inventoryService.addInventoryLink(this.recordId, this.elementsToAdd));
        if (this.elementsToRemove.length) promises.push(this.inventoryService.removeInventoryLink(this.recordId, this.elementsToRemove));
        Promise.all(promises).then(() => {
            if (this.modalData.callback) {
                this.modalData.callback();
            }
            this.closeModal();
            this.isSaving = false;
        });
    }

    private getDataSubjectTypeList() {
        this.inventoryLinkingService.getDataSubjectList(this.orgGroupId).then((response: IInventoryCellValue[]) => {
            this.dataSubjectTypeList = this.setTranslatedValues(response);
            if (this.dataSubjectTypeList[0]) {
                this.selectedSubjectType = this.dataSubjectTypeList[0];
                this.getDataCategoryList(this.selectedSubjectType.valueId);
            }
        });
    }

    private setTranslatedValues(list: IInventoryCellValue[]) {
        list.map((item) => {
            item.valueKey = item.valueKey ? this.translatePipe.transform(item.valueKey) : item.value;
        });
        return list;
    }

    private getDataCategoryList(subjectId: string) {
        this.fetchingCategories = true;
        Promise.all([
            this.inventoryLinkingService.getDataCategoryList(subjectId, this.orgGroupId),
            this.inventoryLinkingService.getDataSubjectElements(this.recordId, subjectId),
        ]).then((response: [IDataCategory[], ILinkedElement[]]) => {
            this.dataCategoryList = response[0];
            this.currentRecordElements = [...this.currentRecordElements, ...response[1]];
            this.setElementMapForCategory(this.dataCategoryList);
            if (this.dataCategoryList && this.dataCategoryList[0]) {
                this.selectCategory(this.dataCategoryList[0]);
            }
            this.hasCategories = Boolean(this.dataCategoryList && this.dataCategoryList[0]);
            this.fetchingCategories = false;
        });
    }

    private allElementsSelected() {
        let allCurrentElementsSelected = true;
        for (let i = 0; i < this.currentElements.length; i++) {
            if (!this.elementMap[this.currentElements[i].valueId]) {
                allCurrentElementsSelected = false;
                break;
            }
        }
        this.allSelected = allCurrentElementsSelected;
    }

   private defaultCategory() {
        this.dataCategoryList = [];
        this.selectedCategory = {
            categoryId: "",
            categoryValue: "",
            elements: [],
        };
    }

    private formatRemoveLinkData(): Array<{ inventoryAssociationId: string }> {
        const unlinkData: Array<{ inventoryAssociationId: string }>  = [];
        forEach(this.currentRecordElements, (element: ILinkedElement) => {
            if (!this.elementMap[element.inventoryId]) {
                unlinkData.push({ inventoryAssociationId: element.inventoryAssociationId });
            }
        });
        return unlinkData;
    }

    private formatLinkData(): Array<{ id: string }> {
        const formattedData: Array<{ id: string }> = [];
        forEach(this.elementMap, (value: boolean, key: string) => {
            if (value && !Boolean(find(this.currentRecordElements, ["inventoryId", key]))) {
                formattedData.push({
                    id: key,
                });
            }
        });
        return formattedData;
    }
}
