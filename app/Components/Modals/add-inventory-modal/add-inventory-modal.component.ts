// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from "@angular/core";
import { StateService } from "@uirouter/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    isSavingRecord,
    requiredFieldsValid,
    getAllUsersValid,
    getAttributeMap,
    getStatusMap,
} from "oneRedux/reducers/inventory.reducer";
import { getUserById } from "oneRedux/reducers/user.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { AssessmentBulkLaunchActionService } from "sharedModules/services/actions/bulk-launch-assessment-action.service";
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { UserActionService } from "oneServices/actions/user-action.service";

// Interface
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAssessmentCreationDetails } from "interfaces/assessment/assessment-creation-details.interface";
import {
    IInventoryAddModalResolve,
    IRecordResponse,
    IAttributeDetails,
    IInventoryRecord,
    IAttribute,
} from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";

// Constants and Enums
import { InventoryContexts } from "enums/inventory.enum";
import {
    InventoryPermissions,
    OwnerAttributes,
} from "constants/inventory.constant";

@Component({
    selector: "add-inventory-modal",
    templateUrl: "./add-inventory-modal.component.html",
})
export class AddInventoryModalComponent implements OnInit, OnDestroy {
    usersValid: boolean;
    modalTitle: string;
    canLaunch: boolean;
    attributeSections: IAttributeDetails;
    attributeMap: IStringMap<IAttribute>;
    inventoryId: number;
    isSaving: boolean;
    addContext = InventoryContexts.Add;
    hideErrors = true;
    recordIsValid: boolean;
    sendAssessment: boolean;
    inventoryPermissions: IStringMap<boolean>;
    isLoadingUsers = false;
    private modalData: IInventoryAddModalResolve;
    private subscription: Subscription;
    private callback: () => void;

    constructor(
        private stateService: StateService,
        @Inject(StoreToken) private store: IStore,
        private permissions: Permissions,
        private assessmentBulkLaunchActionService: AssessmentBulkLaunchActionService,
        private modalService: ModalService,
        private inventoryAction: InventoryActionService,
        private inventoryAdapter: InventoryAdapter,
        private userActions: UserActionService,
    ) {}

    ngOnInit() {
        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.modalTitle = this.modalData.modalTitle;
        this.inventoryId = this.modalData.inventoryId;
        this.callback = this.modalData.callback;
        this.canLaunch = this.permissions.canShow(InventoryPermissions[this.inventoryId].launchV2Assessment);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
        this.inventoryAction.setRecordDefaultState();
    }

    saveAndClose() {
        this.saveRecord(() => {
            this.callback();
            this.closeModal();
        });
    }

    saveAndAddNew() {
        this.saveRecord(() => {
            this.callback();
            this.inventoryAction.setEmptyCurrentRecord();
            this.inventoryAction.setRequiredFieldsAsInvalid();
        });
    }

    toggleSendAssessment(event) {
        this.sendAssessment = event;
    }

    routeToSendAssessment(record: IRecordResponse) {
        const state = this.store.getState();
        const recordCells = this.inventoryAdapter.getRecordCells(this.attributeMap, getStatusMap(state), record);
        const inventoryRecord: IInventoryRecord = {
            id: record.id,
            cells: recordCells,
            canEdit: true,
            assessmentOutstanding: false,
        };
        const bulkInventoryList = this.inventoryAction.getBulkInventoryList([inventoryRecord], this.inventoryId);
        const bulkAssessmentDetails: IAssessmentCreationDetails[] = this.assessmentBulkLaunchActionService.getAssessmentCreationItems(bulkInventoryList, this.inventoryId);
        this.assessmentBulkLaunchActionService.setBulkLaunchAssessments(bulkAssessmentDetails);
        this.stateService.go("zen.app.pia.module.assessment.bulk-launch-inventory-assessment", { inventoryTypeId: this.inventoryId });
    }

    private saveRecord(successCb: () => void) {
        const ownerAttribute = Object.values(this.attributeMap).find((item: IAttribute) => item.fieldAttributeCode === OwnerAttributes[this.inventoryId]);
        this.inventoryAction.addRecord().then((response: IProtocolResponse<IRecordResponse>) => {
            const userId = ownerAttribute && ownerAttribute.id && response.data.map[ownerAttribute.id] && response.data.map[ownerAttribute.id][0] ? response.data.map[ownerAttribute.id][0].value : "";
            const currentUser = getUserById(userId)(this.store.getState());
            if (userId && !currentUser) {
                this.isLoadingUsers = true;
                this.userActions.getUser(userId, true).then(() => {
                    this.isLoadingUsers = false;
                    this.handleResponse(response, successCb);
                });
            } else {
                this.handleResponse(response, successCb);
            }
        });
    }

    private handleResponse(response: IProtocolResponse<IRecordResponse>, successCb: () => void) {
        if (response.result) {
            if (this.sendAssessment) {
                this.routeToSendAssessment(response.data);
            }
            successCb();
        } else {
            this.hideErrors = false;
            this.inventoryAction.isDoneSavingRecord();
        }
    }

    private componentWillReceiveState(state: IStoreState) {
        this.isSaving = isSavingRecord(state);
        this.modalData = getModalData(state);
        this.recordIsValid = requiredFieldsValid(state);
        this.usersValid = getAllUsersValid(state);
        this.attributeMap = getAttributeMap(state);
    }
}
