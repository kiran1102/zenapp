function controller() {
    return this.scope;
}

const questionComponent: ng.IComponentOptions = {
    bindings: {
        scope: "<",
    },
    controller,
    template: `
        <one-modal
            cancel="$ctrl.cancel"
            headline="{{::$root.t('QuestionBuilder')}}"
            body-class="flex"
            >
            <modal-body class="flex-full-height">
                <form
                    ng-disabled="$ctrl.submitInProgress"
                    name="questionForm"
                    class="
                        question-modal__form
                        flex
                        padding-left
                        full-width
                        stretch-vertical
                    ">
                    <section class="
                        question-modal__configuration
                        padding-right-2
                        padding-bottom-2
                        ">
                        <one-label-content
                            ng-if="::$ctrl.showProperty('name')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('Question')}}"
                            icon="fa fa-fw fa-info-circle"
                            description="{{::$root.t('DMAssetQuestionModalTitleToolTip')}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            is-required="true"
                            inline="true"
                            >
                            <input
                                class="question-modal__input one-input"
                                ng-model="$ctrl.updatedQuestion.name"
                                ng-change="$ctrl.updatePreview()"
                                maxlength="250"
                                name="questionTitle"
                                placeholder="{{::$root.t('EnterQuestion')}}"
                            />
                            <p
                                class="question-modal__error margin-top-1 margin-bottom-0 text-error"
                                ng-if="(questionForm.questionTitle.$touched || questionForm.questionTitle.$dirty) && !$ctrl.updatedQuestion.name"
                                >
                                {{::$root.t("ErrorEnterQuestion")}}
                            </p>
                        </one-label-content>
                        <one-label-content
                            ng-if="::$ctrl.showProperty('description')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('Description')}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            inline="true"
                            >
                            <textarea
                                class="question-modal__input one-text-area"
                                ng-model="$ctrl.updatedQuestion.description"
                                ng-change="$ctrl.updatePreview()"
                                maxlength="1500"
                                rows="3"
                                name="questionText"
                                placeholder="{{::$root.t('EnterDescription')}}"
                                >
                            </textarea>
                        </one-label-content>
                        <one-label-content
                            ng-if="::$ctrl.showProperty('reportFriendlyName')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('AttributeName')}}"
                            icon="fa fa-fw fa-info-circle"
                            description="{{::$root.t('DMTemplateQuestionModalRFNToolTip', {templateTypeName: $ctrl.templateTypeName})}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            is-required="true"
                            inline="true"
                            >
                            <input
                                class="question-modal__input one-input"
                                ng-model="$ctrl.updatedQuestion.reportFriendlyName"
                                maxlength="100"
                                name="reportFriendlyName"
                                placeholder="{{::$root.t('AttributeName')}}"
                            />
                            <p
                                class="question-modal__error margin-top-1 margin-bottom-0 text-error"
                                ng-if="(questionForm.reportFriendlyName.$touched || questionForm.reportFriendlyName.$dirty) && !$ctrl.updatedQuestion.reportFriendlyName"
                                >
                                {{::$root.t("ErrorAttributeName")}}
                            </p>
                        </one-label-content>
                        <one-label-content
                            ng-if="$ctrl.showProperty('type')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('Type')}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            is-required="true"
                            inline="true"
                            >
                            <one-select
                                name="question"
                                options="$ctrl.QuestionTypeOptions"
                                model="$ctrl.baseQuestionType"
                                on-change="$ctrl.setQuestionType(selection)"
                                label-key="label"
                                value-key="value"
                            >
                            </one-select>
                        </one-label-content>
                        <one-label-content
                            ng-if="$ctrl.showProperty('displayType')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('DisplayType')}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            is-required="true"
                            inline="true"
                            >
                            <one-select
                                options="$ctrl.DisplayTypeOptions"
                                model="$ctrl.updatedQuestion.displayType"
                                on-change="$ctrl.setDisplayType(selection)"
                                label-key="label"
                                value-key="value"
                            >
                            </one-select>
                        </one-label-content>
                        <one-label-content
                            ng-if="$ctrl.showProperty('options')"
                            class="question-modal__group block margin-bottom-2"
                            name="{{::$root.t('Options')}}"
                            label-class="question-modal__label margin-right-1"
                            body-class="question-modal__input-container"
                            is-required="true"
                            inline="true"
                            >
                            <div
                                class="question-modal__question-option one-input flex-full-width margin-bottom-1 padding-all-0"
                                ng-repeat="option in $ctrl.updatedQuestion.options track by $index"
                                >
                                <input
                                    class="question-modal__option-input stretch-horizontal padding-left-1"
                                    ng-model="option.value"
                                    ng-change="$ctrl.setOption($index)"
                                    maxlength="2000"
                                    placeholder="{{::$root.t('OptionName')}}"
                                    focus-if="option.focusMe"
                                    ng-focus="$ctrl.removeOptionFocus(option)"
                                    ng-keyup="$ctrl.handleOptionKeyup($event)"
                                />
                                <button
                                    class="question-modal__option-delete"
                                    type="button"
                                    ng-click="$ctrl.removeOption($index)">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <one-button
                                text="{{::$root.t('AddOption')}}"
                                button-click="$ctrl.addOption()"
                                >
                            </one-button>
                            <p
                                class="question-modal__error margin-top-1 margin-bottom-0 text-error"
                                ng-if="$ctrl.hasDuplicateOptions"
                                >
                                {{::$root.t("DuplicateValuesNotSupported")}}
                            </p>
                        </one-label-content>
                        <div class="
                            question-modal__question-parameters
                            flex-full-width
                            row-wrap
                            row-space-between
                            ">
                            <div
                                ng-if="$ctrl.showProperty('required')"
                                class="question-modal__parameter margin-top-2"
                                >
                                <one-checkbox
                                    class="question-modal__checkbox"
                                    is-selected="$ctrl.updatedQuestion.required"
                                    is-disabled="$ctrl.propertyIsDisabled('required')"
                                    toggle="$ctrl.toggleProperty('required')"
                                    identifier="toggleRequired"
                                    >
                                </one-checkbox>
                                <label>{{::$root.t("Required")}}</label>
                            </div>
                            <div
                                ng-if="$ctrl.showProperty('allowNotSure')"
                                class="question-modal__parameter margin-top-2"
                                >
                                <one-checkbox
                                    class="question-modal__checkbox"
                                    is-selected="$ctrl.updatedQuestion.allowNotSure"
                                    is-disabled="$ctrl.propertyIsDisabled('notSure')"
                                    toggle="$ctrl.toggleProperty('allowNotSure')"
                                    identifier="toggleAllowNotSure"
                                    >
                                </one-checkbox>
                                <label>{{::$root.t("AllowNotSure")}}</label>
                            </div>
                            <div
                                ng-if="$ctrl.showProperty('allowOther')"
                                class="question-modal__parameter margin-top-2"
                                >
                                <one-checkbox
                                    class="question-modal__checkbox"
                                    is-selected="$ctrl.updatedQuestion.allowOther"
                                    is-disabled="$ctrl.propertyIsDisabled('allowOther')"
                                    toggle="$ctrl.toggleProperty('allowOther')"
                                    identifier="toggleAllowOther"
                                    >
                                </one-checkbox>
                                <label>{{$root.t("AllowOther")}}</label>
                            </div>
                            <div
                                ng-if="$ctrl.showProperty('multiselect')"
                                class="question-modal__parameter margin-top-2"
                                >
                                <one-checkbox
                                    class="question-modal__checkbox"
                                    is-selected="$ctrl.isMultiSelect()"
                                    is-disabled="$ctrl.propertyIsDisabled('multiselect')"
                                    toggle="$ctrl.toggleMultiSelect()"
                                    identifier="toggleMultiSelect"
                                    >
                                </one-checkbox>
                                <label>{{::$root.t("Multiselect")}}</label>
                            </div>
                        </div>
                    </section>
                    <section class="question-modal__preview flex-full-height padding-left-2">
                        <label>{{::$root.t('Preview')}}</label>
                        <div class="
                            question-modal__preview-question
                            flex-full-height
                            padding-all-1
                            ">
                            <one-question
                                class="margin-top-bottom-auto"
                                question="$ctrl.updatedQuestion"
                            >
                            </one-question>
                        </div>
                    </section>
                </form>
                <footer class="question-modal__footer row-flex-end static-vertical">
                    <one-button
                        class="question-modal__submit margin-right-1"
                        text="{{$ctrl.submitText}}"
                        is-disabled="$ctrl.disableSubmit() || $ctrl.submitInProgress"
                        button-click="$ctrl.handleSubmit()"
                        type="primary"
                        enable-loading="true"
                        is-loading="$ctrl.submitInProgress"
                        identifier="submitQuestion"
                        >
                    </one-button>
                    <one-button
                        class="question-modal__button"
                        text="{{::$root.t('Cancel')}}"
                        button-click="$ctrl.cancel()"
                        identifier="cancelQuestion"
                        >
                    </one-button>
                </footer>
            </modal-body>
        </one-modal>
    `,
};

export default questionComponent;
