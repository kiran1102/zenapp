// 3rd Party
import {
    cloneDeep,
    some,
    isUndefined,
    map,
    toLower,
    uniq,
} from "lodash";

// Services
import Utilities from "Utilities";
import { NotificationService } from "modules/shared/services/provider/notification.service";

// Interfaces
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ILabelValue } from "interfaces/generic.interface";

// Constants and Enums
import { StatusCodes } from "enums/status-codes.enum";
import {
    InventoryTableIds,
    OptionTypes,
} from "enums/inventory.enum";
import {
    DMQuestionTypes,
    DMQuestionDisplayTypes,
} from "enums/question-types.enum";
import { DMTemplateStates } from "enums/template-states.enum";
import { KeyCodes } from "enums/key-codes.enum";
import {
    InventoryAttributeCodes,
    DMAssessmentTemplateTypeNames,
} from "constants/inventory.constant";
import { Regex } from "constants/regex.constant";

export interface IQuestionOption {
    key: string;
    value: string;
    focusMe?: boolean;
}

export interface IQuestionSaveRequest {
    name: string;
    description: string;
    reportFriendlyName: string;
    questionType: number;
    required: boolean;
    allowNotSure: boolean;
    allowOther: boolean;
    previousQuestionId?: string;
    options?: IQuestionOption[];
    displayType?: number;
    responseType?: number;
    conditionGroups?: any[];
}

class QuestionModalCtrl implements ng.IController {

    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$uibModalInstance",
        "DMTemplate",
        "templateId",
        "templateStatusId",
        "templateType",
        "sectionId",
        "question",
        "previousQuestionId",
        "config",
        "NotificationService",
    ];

    private translate: Function;
    private QuestionTypeOptions: Array<ILabelValue<number>>;
    private DisplayTypeOptions: Array<ILabelValue<number>>;
    private updatedQuestion: any;
    private submitText: string;
    private templateTypeName: string;
    private baseQuestionType: number;
    private isPublished: boolean;
    private isNewQuestion: boolean = !Boolean(this.question);
    private submitInProgress = false;
    private hasDuplicateOptions = false;

    constructor(
        $rootScope: IExtendedRootScopeService,
        readonly $scope: any,
        readonly $uibModalInstance: any,
        readonly DMTemplate: any,
        readonly templateId: string,
        readonly templateStatusId: number,
        readonly templateType: number,
        readonly sectionId: string,
        readonly question: any,
        readonly previousQuestionId: string,
        readonly config: any,
        private notificationService: NotificationService,
    ) {
        $scope.controller = this;
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.QuestionTypeOptions = [
            {
                label: this.translate("Text"),
                value: DMQuestionTypes.Text,
            },
            {
                label: this.translate("MultiChoice"),
                value: DMQuestionTypes.SingleSelect,
            },
        ];
        this.DisplayTypeOptions = [
            {
                label: this.translate("Buttons"),
                value: DMQuestionDisplayTypes.Buttons,
            },
            {
                label: this.translate("TypeaheadSearch"),
                value: DMQuestionDisplayTypes.Typeahead,
            },
        ];
        this.submitText = this.isNewQuestion ? this.translate("AddQuestion") : this.translate("Save");
        const options: IQuestionOption[] = cloneDeep(this.question.options);
        this.updatedQuestion = this.isNewQuestion ? this.createBlankQuestion() : { ...this.question, options };
        this.isPublished = this.templateStatusId === DMTemplateStates.Published;
        this.baseQuestionType = this.getBaseQuestionType();
        this.templateTypeName = this.templateType ? DMAssessmentTemplateTypeNames[this.templateType] : "";
    }

    public handleSubmit = (): void => {
        this.submitInProgress = true;
        const request: IQuestionSaveRequest = {
            name: this.updatedQuestion.name || "",
            description: this.updatedQuestion.description || "",
            reportFriendlyName: this.updatedQuestion.reportFriendlyName.replace(Regex.MULTI_SPACES, " ") || "",
            questionType: this.updatedQuestion.questionType,
            required: this.updatedQuestion.required,
            allowNotSure: this.updatedQuestion.allowNotSure,
            allowOther: this.updatedQuestion.allowOther,
            previousQuestionId: this.previousQuestionId || (this.config ? this.config.previousQuestionId : ""),
            conditionGroups: this.updatedQuestion.conditionGroups && this.updatedQuestion.questionType !== DMQuestionTypes.Text ? this.updatedQuestion.conditionGroups : [],
        };
        if (this.updatedQuestion.questionType !== DMQuestionTypes.Text) {
            request.displayType = this.updatedQuestion.displayType;
            request.options = this.updatedQuestion.options;
        }
        if (this.isNewQuestion) {
            this.DMTemplate.addQuestion(this.templateId, this.sectionId, request).then( (response: IProtocolPacket): void | undefined => {
                this.checkForDuplicateAttribute(response);
            });
        } else {
            this.DMTemplate.updateQuestion(this.templateId, this.sectionId, this.question.templateQuestionUniqueId, request).then( (response: IProtocolPacket): void | undefined => {
                this.checkForDuplicateAttribute(response);
            });
        }
    }

    public checkForDuplicateAttribute(response: IProtocolPacket): void | undefined {
        if (response.status === StatusCodes.Conflict) {
            this.notificationService.alertError(this.translate("Error"), this.translate("DuplicateAttributeMessage"));
            this.submitInProgress = false;
        } else if (response.status !== StatusCodes.Success) {
            this.notificationService.alertError(this.translate("Error"), this.translate("QuestionUpdateError"));
            this.submitInProgress = false;
        }
        if (!response.result) return;
        this.$uibModalInstance.close({ result: true, data: response.data });
    }

    public cancel = (): void => {
        this.$uibModalInstance.close({result: false, data: null});
    }

    public setQuestionType = (selection: number): void => {
        if (selection === DMQuestionTypes.Text) {
            this.updatedQuestion.questionType = DMQuestionTypes.Text;
        } else {
            this.updatedQuestion.questionType = DMQuestionTypes.SingleSelect;
            if (!this.updatedQuestion.displayType) this.updatedQuestion.displayType = DMQuestionDisplayTypes.Buttons;
            if (!this.updatedQuestion.options || !this.updatedQuestion.options.length) {
                this.updatedQuestion.options = [];
                this.addOption();
            }
        }
        this.baseQuestionType = this.getBaseQuestionType();
    }

    public isMultiSelect = (): boolean => {
        return (this.updatedQuestion.questionType === DMQuestionTypes.MultiSelect) ||
            (this.updatedQuestion.questionType === DMQuestionTypes.MultiSelectLoop) ||
            (this.updatedQuestion.questionType === DMQuestionTypes.TypeaheadMultiselectLoop);
    }

    public updatePreview = (): void => {
        this.updatedQuestion = { ...this.updatedQuestion };
    }

    public setDisplayType = (selection: number): void => {
        this.updatedQuestion.displayType = selection;
    }

    public addOption = (): void => {
        const blankOption: IQuestionOption = {
            key: "",
            value: "",
            focusMe: this.updatedQuestion.options.length,
        };
        this.updatedQuestion.options.push(blankOption);
        this.updatePreview();
    }

    public removeOptionFocus = (option: IQuestionOption): void => {
        option.focusMe = false;
    }

    public setOption = (index: number): void => {
        const option: IQuestionOption = this.updatedQuestion.options[index];
        if (!option.key || !Utilities.matchRegex(option.key, Regex.GUID)) {
            option.key = option.value;
        }
        this.updatePreview();
    }

    public removeOption = (index: number): void => {
        this.updatedQuestion.options.splice(index, 1);
        this.updatePreview();
    }

    public toggleMultiSelect = (): void => {
        this.updatedQuestion.questionType = this.updatedQuestion.questionType === DMQuestionTypes.SingleSelect ? DMQuestionTypes.MultiSelect : DMQuestionTypes.SingleSelect;
        this.updatePreview();
    }

    public toggleProperty = (property: string): undefined | void => {
        if (!property || !this.updatedQuestion || isUndefined(this.updatedQuestion[property])) return;
        this.updatedQuestion[property] = !this.updatedQuestion[property];
        this.updatePreview();
    }

    public disableSubmit = (): boolean => {
        this.hasDuplicateOptions = this.updatedQuestion.questionType !== DMQuestionTypes.Text ?
            this.optionsHaveDuplicates(this.updatedQuestion.options) : false;
        return !this.updatedQuestion.name ||
            !this.updatedQuestion.reportFriendlyName ||
            !this.hasRequiredOptions() ||
            this.hasDuplicateOptions;
    }

    public propertyIsDisabled = (property: string): boolean => {
        if (!property) return false;
        switch (property) {
            case "allowOther":
                return this.updatedQuestion.responseType === OptionTypes.Locations;
            case "multiselect":
                return Boolean(this.updatedQuestion.templateQuestionUniqueId);
            case "required":
            case "notSure":
                return this.updatedQuestion.attributeCode === InventoryAttributeCodes[InventoryTableIds.Assets].Hosting_Location;
            default:
                return false;
        }
    }

    public handleOptionKeyup(event: KeyboardEvent): void {
        if (event.keyCode === KeyCodes.Enter) {
            event.preventDefault();
            this.addOption();
        }
    }

    private hasRequiredOptions(): boolean {
        return this.isPublished ||
            this.updatedQuestion.questionType === DMQuestionTypes.Text ||
            this.updatedQuestion.responseType ||
            (this.updatedQuestion.options.length && !some(this.updatedQuestion.options, {key: "", value: ""}));
    }

    private createBlankQuestion(): IQuestionSaveRequest {
        return {
            name: "",
            description: "",
            reportFriendlyName: "",
            questionType: this.config ? this.config.questionType : DMQuestionTypes.Text,
            displayType: this.config && this.config.questionType === DMQuestionTypes.SingleSelect ? DMQuestionDisplayTypes.Buttons : null,
            responseType: OptionTypes.Custom,
            required: null,
            allowOther: null,
            allowNotSure: null,
        };
    }

    private getBaseQuestionType(): number | null {
        if (!this.updatedQuestion.questionType) return null;
        else if (this.updatedQuestion.questionType !== DMQuestionTypes.Text) {
            if (!this.updatedQuestion.options || !this.updatedQuestion.options.length) {
                this.updatedQuestion.options = [];
                this.addOption();
            }
            return DMQuestionTypes.SingleSelect;
        }
        return DMQuestionTypes.Text;
    }

    private showProperty(property?: string): boolean {
        if (!property) return !this.isPublished;
        switch (property) {
            case "type":
                return this.showType();
            case "displayType":
            case "options":
                return this.showSelectProperties();
            case "required":
            case "allowNotSure":
                return !this.isPublished;
            case "allowOther":
            case "multiselect":
                return this.showSelectModifiers();
            default:
                return true;
        }
    }

    private showType(): boolean {
        return !this.updatedQuestion.templateQuestionUniqueId;
    }

    private showSelectModifiers(): boolean {
        return !this.isPublished && this.updatedQuestion.questionType !== DMQuestionTypes.Text;
    }

    private showSelectProperties(): boolean {
        switch (this.updatedQuestion.responseType) {
            case OptionTypes.Users:
            case OptionTypes.Locations:
            case OptionTypes.Orgs:
            case OptionTypes.Assets:
            case OptionTypes.Processes:
                return false;
            default:
                return this.showSelectModifiers();
        }
    }

    private optionsHaveDuplicates(options: IQuestionOption[]): boolean {
        const trimmedValues: string[] = map(options, (option: IQuestionOption): string => {
            return toLower(option.value).replace(Regex.MULTI_SPACES, " ");
        });
        return options.length !== uniq(trimmedValues).length;
    }

}

export default QuestionModalCtrl;
