import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Rxjs
import { startWith } from "rxjs/operators";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getSchema } from "oneRedux/reducers/inventory-record.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import {
    IInventoryCopyV2ModalResolve,
    IFormattedAttribute,
    IAttributeOptionV2,
} from "interfaces/inventory.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Const
import {
    InventoryTranslationKeys,
    RequiredFieldNames,
    AttributeFieldNames,
} from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

@Component({
    selector: "copy-inventory-v2-modal",
    templateUrl: "./copy-inventory-v2-modal.component.html",
})
export class CopyInventoryV2ModalComponent implements OnInit {

    modalData: IInventoryCopyV2ModalResolve;
    schema: IFormattedAttribute[];
    copyAttributes = RequiredFieldNames;
    inventoryId: number;
    modalHeader: string;
    isSaving = false;
    copyRelated = true;
    copyDisabled = true;
    hasErrors = false;
    hasAPIError = false;
    isReady = true;
    hasValidFields = false;
    itemNameLabel: string;
    typeOptions: IAttributeOptionV2[];
    selectedName: string;
    selectedOrg: IAttributeOptionV2;
    selectedLocation: IAttributeOptionV2;
    selectedType: IAttributeOptionV2;
    prepopulatedName: string;
    prepopulatedOrg: IAttributeOptionV2;
    prepopulatedLocation: IAttributeOptionV2;
    prepopulatedType: IAttributeOptionV2;
    locationOptions: IAttributeOptionV2[];
    filteredLocationOptions: IAttributeOptionV2[];
    filteredTypeOptions: IAttributeOptionV2[];
    attributeFieldNames = AttributeFieldNames;

    constructor(
        @Inject(StoreToken) public store: IStore,
        readonly inventoryService: InventoryService,
        readonly modalService: ModalService,
        readonly lookupService: LookupService,
    ) {}

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.inventoryId = this.modalData.inventoryId;
        this.modalHeader = InventoryTranslationKeys[this.inventoryId].copyInventoryItem;
        this.itemNameLabel = InventoryTranslationKeys[this.inventoryId].itemName;
        this.setPrepopulatedValues();
    }

    copyRecord(closeModal: boolean = true) {
        const primaryKeys: IStringMap<string | IAttributeOptionV2> = this.getPrimaryKeys();
        this.isSaving = true;
        this.copyDisabled = this.canCopyDisabled();
        this.inventoryService.copyRecord(
            NewInventorySchemaIds[this.modalData.inventoryId],
            this.modalData.record.id as string,
            primaryKeys,
            this.copyRelated,
        ).then((response: IProtocolResponse<null>) => {
            if (response.result) {
                this.handleResponse(closeModal);
                this.hasAPIError = false;
            } else {
                this.hasAPIError = true;
            }
            this.isSaving = false;
            this.copyDisabled = this.canCopyDisabled();
        });
    }

    handleResponse(closeModal: boolean) {
        if (this.modalData.callback) this.modalData.callback();
        if (closeModal) {
            this.closeModal();
        } else {
            this.selectedName = this.prepopulatedName;
            this.selectedOrg = this.prepopulatedOrg;
            if (this.copyAttributes[this.inventoryId].Location) this.selectedLocation = this.prepopulatedLocation;
            if (this.copyAttributes[this.inventoryId].Type) this.selectedType = this.prepopulatedType;
            this.hasValidFields = this.checkValidFields();
            this.copyDisabled = this.canCopyDisabled();
        }
    }

    getPrimaryKeys(): IStringMap<string | IAttributeOptionV2> {
        const primaryKeys = {
            [this.copyAttributes[this.inventoryId].Name]: this.selectedName,
            [this.copyAttributes[this.inventoryId].Org]: this.selectedOrg,
        };
        if (this.copyAttributes[this.inventoryId].Location) {
            primaryKeys[this.copyAttributes[this.inventoryId].Location] = this.selectedLocation;
        }
        if (this.copyAttributes[this.inventoryId].Type) {
            primaryKeys[this.copyAttributes[this.inventoryId].Type] = this.selectedType;
        }
        return primaryKeys;
    }

    nameSelect(selection: string) {
        this.selectedName = selection;
        this.hasErrors = !Boolean(selection);
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    orgSelect(event: string) {
        const selection = getOrgById(event)(this.store.getState());
        this.selectedOrg = event && selection ? { value: selection.Name, id: selection.Id } : { value: null, id: null };
        this.hasErrors = this.selectedOrg.id === null;
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    locationSelect(event: {previousValue, currentValue, change}) {
        this.selectedLocation = event && event.currentValue ? { value: event.currentValue.value, id: event.currentValue.id } : null;
        this.filteredLocationOptions = this.lookupService.filterOptionsBySelections([this.selectedLocation], this.locationOptions, "id");
        this.hasErrors = this.selectedLocation === null;
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    typeSelect(event: {previousValue, currentValue, change}) {
        this.selectedType = event && event.currentValue ? { value: event.currentValue.value, id: event.currentValue.id } : null;
        this.filteredTypeOptions = this.lookupService.filterOptionsBySelections([this.selectedType], this.typeOptions, "id");
        this.hasErrors = this.selectedType === null;
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    checkValidFields(): boolean {
        if (this.copyAttributes[this.inventoryId].Location) {
            return !(
                this.selectedLocation &&
                this.selectedLocation.value === this.prepopulatedLocation.value &&
                this.selectedName.trim() === this.prepopulatedName &&
                this.selectedOrg === this.prepopulatedOrg
            );
        } else if (this.copyAttributes[this.inventoryId].Type) {
            return !(
                this.selectedType &&
                this.selectedType.value === this.prepopulatedType.value &&
                this.selectedName.trim() === this.prepopulatedName &&
                this.selectedOrg.value === this.prepopulatedOrg.value
            );
        } else {
            return !(
                this.selectedName.trim() === this.prepopulatedName &&
                this.selectedOrg === this.prepopulatedOrg
            );
        }
    }

    canCopyDisabled(): boolean {
        return this.isSaving || this.hasErrors || !this.hasValidFields;
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    handleInputChange(event: any, fieldName: string) {
        if (fieldName === this.attributeFieldNames.Location) {
            this.filteredLocationOptions = this.lookupService.filterOptionsByInput([], this.locationOptions, event.value, "id", "value");
        } else if (fieldName === this.attributeFieldNames.Type) {
            this.filteredTypeOptions = this.lookupService.filterOptionsByInput([], this.typeOptions, event.value, "id", "value");
        }
    }

    copyRelatedCheck() {
        this.copyRelated = !this.copyRelated;
        this.copyDisabled = this.canCopyDisabled();
    }

    private setPrepopulatedValues() {
        this.prepopulatedName = this.modalData.record[this.copyAttributes[this.inventoryId].Name] as string;
        this.prepopulatedOrg = this.modalData.record[this.copyAttributes[this.inventoryId].Org] as IAttributeOptionV2;
        this.selectedName = this.prepopulatedName;
        this.selectedOrg = this.prepopulatedOrg;
        if (this.copyAttributes[this.inventoryId].Location) {
            this.prepopulatedLocation = this.modalData.record[this.copyAttributes[this.inventoryId].Location] as IAttributeOptionV2;
            this.selectedLocation = this.prepopulatedLocation;
            const locationAttribute = this.schema.find((attribute) => attribute.fieldName === this.copyAttributes[this.inventoryId].Location);
            this.locationOptions = locationAttribute && locationAttribute.values ? locationAttribute.values : [];
            this.filteredLocationOptions = this.lookupService.filterOptionsBySelections([this.prepopulatedLocation], this.locationOptions, "id");
        }
        if (this.copyAttributes[this.inventoryId].Type) {
            this.prepopulatedType = this.modalData.record[this.copyAttributes[this.inventoryId].Type] as IAttributeOptionV2;
            this.selectedType = this.prepopulatedType;
            const typeAttribute = this.schema.find((attribute) => attribute.fieldName === this.copyAttributes[this.inventoryId].Type);
            this.typeOptions = typeAttribute && typeAttribute.values ? typeAttribute.values : [];
            this.filteredTypeOptions = this.lookupService.filterOptionsBySelections([this.prepopulatedType], this.typeOptions, "id");
        }
    }

    private componentWillReceiveState(state: IStoreState) {
        this.modalData = getModalData(state);
        this.schema = getSchema(state);
    }
}
