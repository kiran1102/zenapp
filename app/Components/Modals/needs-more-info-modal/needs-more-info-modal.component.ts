// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Third Party
import {
    cloneDeep,
    isArray,
} from "lodash";

// Enums
import { QuestionStates } from "enums/assessment-question.enum";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion, IQuestionSerialized } from "interfaces/question.interface";
import { IProject } from "interfaces/project.interface";
import { IStringMap } from "interfaces/generic.interface";
import { INeedMoreInfoModalData } from "interfaces/assessment.interface";

// Models
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { ModalService } from "app/scripts/Shared/Services/modal.service";
import NeedsMoreInfoPermission from "generalcomponent/Assessment/assessment-nmi-permission.service";
import ProjectQuestions from "oneServices/project-question.service";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class NeedsMoreInfoModalController implements ng.IComponentController {

    static $inject: string[] = [
        "store",
        "$rootScope",
        "ModalService",
        "ProjectQuestions",
        "NeedsMoreInfoPermission",
    ];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private translations: IStringMap<string>;
    private viewState: IStringMap<boolean>;
    private project: IProject;
    private question: IQuestion;
    private questionClone: IQuestion;
    private currentUser: IUser;
    private subscription: Subscription;
    private submitInfoRequestInProgress = false;
    private removingInfoRequestInProgress = false;
    private isFromViewInfoAddedModal: boolean;

    constructor(
        private store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly ModalService: ModalService,
        private readonly ProjectQuestions: ProjectQuestions,
        private readonly NeedsMoreInfoPermission: NeedsMoreInfoPermission,
    ) {}

    public $onInit() {
        this.translations = {
            modalTitle: this.$rootScope.t("NeedMoreInformation"),
        };

        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((store) => this.componentWillReceiveState(store.modals.modalData));
    }

    public $onDestroy(): void {
        this.subscription.unsubscribe();
    }

    private componentWillReceiveState(modalData: INeedMoreInfoModalData): void {
        if (modalData) {
            this.currentUser = modalData.currentUser;
            this.project = modalData.project;
            this.question = modalData.question;
            this.isFromViewInfoAddedModal = modalData.isFromViewInfoAddedModal;
            this.questionClone = cloneDeep(modalData.question);
            this.updateViewState();
        }
    }

    private updateViewState(): void  {
        this.viewState = {
            canEditNeedsMoreInfoComment: this.NeedsMoreInfoPermission.hasQuestionPermission("canEditNeedsMoreInfoComment", this.currentUser, this.project, this.questionClone),
            canSubmitNeedsMoreInfoModal: (this.questionClone.MoreInfoComment !== this.question.MoreInfoComment) && this.NeedsMoreInfoPermission.hasQuestionPermission("canSubmitNeedsMoreInfoModal", this.currentUser, this.project, this.questionClone),
            canRemoveInfoModal: Boolean(this.question.MoreInfoComment) && this.NeedsMoreInfoPermission.hasQuestionPermission("canRemoveInfoModal", this.currentUser, this.project, this.questionClone),
        };
    }

    private closeModal(isFromViewInfoAddedModal: boolean): void {
        if (isFromViewInfoAddedModal) {
            this.question.State = QuestionStates.InfoAdded;
        }
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private handleInputChange(value: string): void {
        this.questionClone.MoreInfoComment = value;
        this.updateViewState();
    }

    private isAnswered(question: IQuestion): boolean {
        return isArray(question.Responses) && question.Responses.length > 0;
    }

    private removeNeedsInfo(): void {
        const answered: boolean = this.isAnswered(this.questionClone);
        const newState: IQuestionSerialized = {
            Id: this.questionClone.Id,
            State: answered ? QuestionStates.Answered : QuestionStates.Unanswered,
            Comment: "",
        };
        this.removingInfoRequestInProgress = true;
        this.ProjectQuestions.updateState(newState).then((res: IProtocolPacket): void => {
            if (res.result) {
                this.question.MoreInfoComment = "";
                const isAnswered: boolean = this.isAnswered(this.question);
                this.question.State = isAnswered ? QuestionStates.Answered : QuestionStates.Unanswered;
                this.ModalService.handleModalCallback();
                this.closeModal(false);
            } else {
                this.removingInfoRequestInProgress = false;
            }
        });
    }

    private addMoreInfo(): void {
        const newState: IQuestionSerialized = {
            Id: this.questionClone.Id,
            State: QuestionStates.InfoRequested,
            Comment: this.questionClone.MoreInfoComment,
        };
        this.submitInfoRequestInProgress = true;
        this.ProjectQuestions.updateState(newState).then((res: IProtocolPacket): void => {
            if (res.result) {
                this.question.MoreInfoComment = this.questionClone.MoreInfoComment;
                this.question.State = QuestionStates.InfoRequested;
                this.ModalService.handleModalCallback();
                this.closeModal(false);
            } else {
                this.submitInfoRequestInProgress = false;
            }
        });
    }
}

const needsMoreInfoModalTemplate: IStringMap<string> = {
  template: `
    <section class="padding-all-2">
        <form name="needsMoreInfoForm">
            <one-label-content
                name="{{::$root.t('InfoRequested')}}"
                ng-if="$ctrl.question.MoreInfoComment"
                label-class="needs-info__label"
                wrapper-class="margin-bottom-2">
                <p class="text-left">
                    {{$ctrl.question.MoreInfoComment}}
                </p>
            </one-label-content>
            <one-label-content
                name="{{$root.t('WhatInformationIsNeeded')}}"
                label-class="needs-info__label"
                wrapper-class="margin-bottom-2">
                <one-textarea
                    disable-resize="true"
                    input-changed="$ctrl.handleInputChange(value)"
                    name="Comment"
                    placeholder="{{::$root.t('Comments')}}"
                    textarea-class="needs-info__textarea">
                </one-textarea>
                <p
                    class="text-center text-danger"
                    ng-if="$ctrl.showError">
                    {{::$root.t("EnterCommentsBeforeSubmitting")}}
                </p>
            </one-label-content>
            <footer class="margin-top-2 needs-info__btn">
                <one-button
                    button-click="$ctrl.removeNeedsInfo()"
                    class="margin-right-auto"
                    enable-loading="true"
                    is-loading="$ctrl.removingInfoRequestInProgress"
                    is-disabled="$ctrl.removingInfoRequestInProgress || $ctrl.submitInfoRequestInProgress"
                    ng-if="$ctrl.viewState.canRemoveInfoModal"
                    text="{{::$root.t('Remove')}}">
                </one-button>
                <one-button
                    button-click="$ctrl.closeModal($ctrl.isFromViewInfoAddedModal)"
                    class="margin-right-1"
                    text="{{::$root.t('Cancel')}}">
                </one-button>
                <one-button
                    button-click="$ctrl.addMoreInfo()"
                    enable-loading="true"
                    is-loading="$ctrl.submitInfoRequestInProgress"
                    is-disabled="!$ctrl.viewState.canSubmitNeedsMoreInfoModal || $ctrl.submitInfoRequestInProgress || $ctrl.removingInfoRequestInProgress"
                    text="{{::$root.t('Submit')}}"
                    type="primary">
                </one-button>
            </footer>
        </form>
    </section>
`};

const needsMoreInfoModalComponent: ng.IComponentOptions = {
    controller: NeedsMoreInfoModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(needsMoreInfoModalTemplate.template),
};

export default needsMoreInfoModalComponent;
