import _, { isFunction } from "lodash";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { IDeleteConfirmationModalResolve } from "interfaces/delete-confirmation-modal-resolve.interface";

class DeleteConfirmationModalController implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "ModalService"];

    public modalIcon = "ot ot-trash-o";
    public modalIconTextClass = "color-cherry";
    private resolve: IDeleteConfirmationModalResolve;
    private promiseToResolve: () => ng.IPromise<any>;
    private modalTitle: string;
    private confirmationText: string;
    private cancelButtonText: string;
    private submitButtonText: string;
    private isSubmitting: boolean;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
    ) {}

    public $onInit(): void {
        this.promiseToResolve = this.resolve.promiseToResolve;
        this.modalTitle = this.resolve.modalTitle;
        this.confirmationText = this.resolve.confirmationText;
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.submitButtonText = this.resolve.submitButtonText || this.$rootScope.t("Submit");
        this.modalIcon = this.resolve.modalIcon || this.modalIcon;
        this.modalIconTextClass = this.resolve.modalIconTextClass || this.modalIconTextClass;
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }
}

const deleteConfirmationModalTemplate: any = {
    template: `
        <section class="delete-confirmation-modal padding-all-2">
            <div class="row-horizontal-center margin-bottom-2">
                <span class="delete-confirmation-modal__icon fa-stack fa-lg">
                    <i
                        class="fa fa-circle fa-stack-2x"
                        ng-class="$ctrl.modalIconTextClass"
                    ></i>
                    <i
                        class="fa-stack-1x fa-inverse ot-icon"
                        ng-class="$ctrl.modalIcon"
                    ></i>
                </span>
            </div>
            <div class="margin-top-2 margin-bottom-4 text-center text-color-default text-bold" ng-bind-html="$ctrl.confirmationText"></div>
            <div ng-if="$ctrl.resolve.customKeyText"
                class="margin-top-2 margin-bottom-4 text-center text-color-default"
                ng-bind="$ctrl.resolve.customKeyText"
            ></div>
            <div class="margin-top-2 margin-bottom-4 text-center text-color-default text-bold"
                ng-bind="$ctrl.resolve.customConfirmationText"
            ></div>
            <footer class="row-flex-end margin-top-2">
                <one-button
                    identifier="{{$ctrl.resolve.cancelIdentifier}}"
                    is-rounded="true"
                    button-class="margin-right-1"
                    button-click="$ctrl.closeModal()"
                    text="{{$ctrl.cancelButtonText}}">
                </one-button>
                <one-button
                    identifier="{{$ctrl.resolve.deleteIdentifier}}"
                    is-rounded="true"
                    button-click="$ctrl.handleSubmit()"
                    enable-loading="true"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="$ctrl.isSubmitting"
                    text="{{$ctrl.submitButtonText}}"
                    type="primary">
                </one-button>
            </footer>
        </section>
    `,
};

export const deleteConfirmationModal: ng.IComponentOptions = {
    controller: DeleteConfirmationModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(deleteConfirmationModalTemplate.template),
};
