// Services
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { ModalService } from "sharedServices/modal.service";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";

// Enums
import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";

class ReassignmentModal {

    static $inject: string[] = [
        "$rootScope",
        "store",
        "ModalService",
    ];

    canSubmit = false;
    model: IOrgUserAdapted | string = "";
    headerString: string;
    headlineString: string;
    placeholderText: string;
    reassignInProgress = false;
    dropdownType: string;
    modalData: any;
    modalTitle: string;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly store: IStore,
        readonly ModalService: ModalService,
    ) {}

    $onInit() {
        this.modalData = getModalData(this.store.getState());
        this.modalTitle = this.$rootScope.t(this.modalData.headlineTranslationKey);
        this.headerString = this.$rootScope.t("AssessmentCurrentlyAssignedTo", { assessmentName: this.modalData.assessmentName, respondentName: this.modalData.currentUser });
        this.dropdownType = OrgUserDropdownTypes[this.modalData.dropdownTypeKey];
    }

    userSelected(selection: any, isValid: boolean): void {
        this.model = selection;
        this.canSubmit = isValid;
    }

    cancel = (): void => {
        this.modalData.callback({ result: false, data: null });
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    reassign = (): void => {
        if (this.canSubmit) {
            this.reassignInProgress = true;
            this.cancel = null;
            this.modalData.updateCallback(this.model).then((response: any): void => {
                if (response.result) {
                    this.modalData.callback({ result: true, data: this.model });
                    this.ModalService.closeModal();
                    this.ModalService.clearModalData();
                } else {
                    this.reassignInProgress = false;
                    this.cancel = (): void => {
                        this.modalData.callback({ result: false, data: null });
                        this.ModalService.closeModal();
                        this.ModalService.clearModalData();
                    };
                }
            });
        }
    }

}

export const reassignmentComponent: ng.IComponentOptions = {
    controller: ReassignmentModal,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(`
        <p class="text-center margin-top-bottom-1 text-bold">{{::$ctrl.headerString}}</p>
        <org-user-dropdown
            input-class="reassignment-modal__typeahead"
            is-disabled="$ctrl.reassignInProgress"
            on-select="$ctrl.userSelected(selection, isValid)"
            org-id="$ctrl.resolve.orgId"
            type="$ctrl.dropdownType"
            wrapper-class="reassignment-modal__form margin-all-2">
        </org-user-dropdown>
        <footer class="text-center margin-bottom-2">
            <one-button
                class="reassignment-modal__button margin-top-1 block"
                text="{{::$root.t('ReAssign')}}"
                button-click="$ctrl.reassign()"
                enable-loading="true"
                is-disabled="!$ctrl.canSubmit || $ctrl.reassignInProgress"
                is-loading="$ctrl.reassignInProgress"
                type="primary">
            </one-button>
        </footer>
	`),
};
