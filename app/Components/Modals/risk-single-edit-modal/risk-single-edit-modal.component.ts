import { RiskSingleEditModalActions } from "./../../../constants/risk-single-edit-modal-actions.constant";
import invariant from "invariant";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getTempRiskById } from "oneRedux/reducers/risk.reducer";
import { buildDefaultModalTemplate } from "generalcomponent/Modals/modal-template/modal-template";
import { IStore, IUnsubscribeCallback } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import { IRisk } from "interfaces/risk.interface";
import { RiskStates } from "enums/risk-states.enum";
import { areAllPropsDefined, isAnyPropChanged } from "oneRedux/utils/utils";
import { ModalService } from "app/scripts/Shared/Services/modal.service";

interface IRiskSingleEditModalViewState {
    modalTitle: string;
    textareaLabelText: string;
    textareaPlaceholder: string;
    messageToDisplay: string;
    stateToProceed: number;
    modelPropToEdit: string;
}

class RiskSingleEditModalController implements ng.IComponentController {

    static $inject: string[] = ["store", "$rootScope", "$q", "ModalService", "RiskAction"];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private translations: any;
    private unsub: IUnsubscribeCallback;
    private origRiskModel: IRisk;
    private updatedRiskModel: IRisk;
    private dataLoaded = false;
    private modelPropToEdit: string;
    private viewState: IRiskSingleEditModalViewState;
    private canSubmit: boolean;
    private isSubmitting: boolean;
    private modalTitle: string;

    private messageToDisplay: string;

    constructor(
        private readonly store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly $q: ng.IQService,
        private readonly ModalService: ModalService,
        private readonly RiskAction: RiskActionService,
    ) {
        this.translations = {
            cancel: $rootScope.t("Cancel"),
            submit: $rootScope.t("Submit"),
        };
    }

    public $onInit(): void {
        const { selectedModelId, modalAction } = getModalData(this.store.getState());
        this.origRiskModel = getTempRiskById(selectedModelId)(this.store.getState()) as IRisk;
        this.updatedRiskModel = this.origRiskModel;
        invariant(this.origRiskModel, "risk-single-edit-modal: Risk model must be defined.");
        invariant(modalAction, "risk-single-edit-modal: Risk action must be defined.");

        this.viewState = this.calculateNewModalViewState(this.origRiskModel.State, modalAction);

        this.modalTitle = this.viewState.modalTitle;

        this.unsub = this.store.subscribe((): void => this.componentWillReceiveState());
        this.componentWillReceiveState();

        this.dataLoaded = true;
    }

    public $onDestroy(): void {
        if (this.unsub) this.unsub();
    }

    private componentWillReceiveState(): void {
        this.updatedRiskModel = getTempRiskById(this.origRiskModel.Id)(this.store.getState()) as IRisk;
        if (!this.updatedRiskModel) return;

        this.canSubmit = this.validateTextarea(this.origRiskModel, this.updatedRiskModel, this.viewState.modelPropToEdit);
    }

    private calculateNewModalViewState(riskState: number, modalAction: string): IRiskSingleEditModalViewState {
        switch (modalAction) {
            case RiskSingleEditModalActions.ADD_RISK_RECOMMENDATION:
                return {
                    modalTitle: this.$rootScope.t("AddRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation }),
                    textareaLabelText: this.$rootScope.t("EnterRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation }),
                    textareaPlaceholder: this.$rootScope.t("PleaseProvideRecommendation", { Recommendation: this.$rootScope.customTerms.Recommendation }),
                    messageToDisplay: this.$rootScope.t("SubmittingWillChangeRiskStatus", { Attribute: this.$rootScope.customTerms.Recommendation }),
                    stateToProceed: RiskStates.Analysis,
                    modelPropToEdit: "Recommendation",
                };
            case RiskSingleEditModalActions.ADD_RISK_REMEDIATION:
                return {
                    modalTitle: this.$rootScope.t("ProposeRemediation"),
                    textareaLabelText: this.$rootScope.t("EnterRemediation"),
                    textareaPlaceholder: this.$rootScope.t("PleaseProvideRemediation"),
                    messageToDisplay: this.$rootScope.t("SubmittingWillChangeRiskToRemediationProposed"),
                    stateToProceed: RiskStates.Addressed,
                    modelPropToEdit: "Mitigation",
                };
            case RiskSingleEditModalActions.ADD_RISK_EXCEPTION:
                return {
                    modalTitle: this.$rootScope.t("RequestException"),
                    textareaLabelText: this.$rootScope.t("EnterException"),
                    textareaPlaceholder: this.$rootScope.t("PleaseProvideException"),
                    messageToDisplay: this.$rootScope.t("SubmittingWillChangeRiskToExceptionRequested"),
                    stateToProceed: RiskStates.Exception,
                    modelPropToEdit: "RequestedException",
                };
            default:
                invariant(false, "risk-single-edit-modal: Correct action must be defined.");
        }
    }

    private validateTextarea(oldRiskModel: IRisk, newRiskModel: IRisk, modelPropToValidate: string): boolean {
        const propsToValidate: string[] = [modelPropToValidate];

        return areAllPropsDefined(propsToValidate)(oldRiskModel, newRiskModel);
    }

    private handleInputChange(value: string): void {
        this.RiskAction.updateTempRisk(this.origRiskModel.Id, { [this.viewState.modelPropToEdit]: value });
    }

    private closeModal(): void {
        this.RiskAction.deleteTempRisk(this.origRiskModel.Id);
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private submitModal(): void {
        this.isSubmitting = true;

        this.RiskAction.saveRiskComment(this.updatedRiskModel, this.viewState.stateToProceed).then((newRiskId: string): ng.IPromise<void> | boolean => {
            if (newRiskId) {
                this.RiskAction.deleteTempRisk(this.updatedRiskModel.Id);
                this.ModalService.closeModal();
                this.ModalService.clearModalData();
            }
            this.isSubmitting = false;
            return false;
        });
    }
}

const riskModalTemplate: any = {
  template: `
    <loading ng-if="!$ctrl.dataLoaded" show-text="false" style="height: 300px;"></loading>
    <section ng-if="$ctrl.dataLoaded" class="padding-all-2 risk-single-edit-modal">
        <one-label-content
            name={{$ctrl.viewState.textareaLabelText}}
            is-required="true"
            label-class="risk-single-edit-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                textarea-class="risk-single-edit-modal risk-single-edit-modal__textarea"
                input-text={{$ctrl.updatedRiskModel[$ctrl.viewState.modelPropToEdit]}}
                input-changed="$ctrl.handleInputChange(value, $ctrl.viewState.modelPropToEdit)"
                placeholder="{{$ctrl.viewState.textareaPlaceholder}}"
                max-length="4000"
                is-required="true"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.updatedRiskModel[$ctrl.viewState.modelPropToEdit].length || 0}} / 4000</div>
        </one-label-content>

        <div>{{$ctrl.viewState.messageToDisplay}}</div>

        <footer class="row-flex-end margin-top-2">
            <one-button
                button-class="margin-right-2"
                button-click="$ctrl.closeModal()"
                text="{{$ctrl.translations.cancel}}"
            ></one-button>
            <one-button
                button-click="$ctrl.submitModal()"
                enable-loading="true"
                is-loading="$ctrl.isSubmitting"
                is-disabled="!$ctrl.canSubmit || $ctrl.isSubmitting"
                text="{{$ctrl.translations.submit}}"
                type="primary"
            ></one-button>
        </footer>
    </section>
`};

export default {
    controller: RiskSingleEditModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(riskModalTemplate.template),
};
