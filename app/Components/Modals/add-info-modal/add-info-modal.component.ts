// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

import { cloneDeep } from "lodash";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import observableFromStore from "oneRedux/utils/observable-from-store";

import { ModalService } from "app/scripts/Shared/Services/modal.service";
import NeedsMoreInfoPermission from "generalcomponent/Assessment/assessment-nmi-permission.service";
import ProjectQuestions from "oneServices/project-question.service";

import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion, IQuestionSerialized } from "interfaces/question.interface";
import { IProject } from "interfaces/project.interface";
import { IStringMap } from "interfaces/generic.interface";

import { QuestionStates } from "enums/assessment-question.enum";

class AddInfoModalController implements ng.IComponentController {
    static $inject: string[] = [
        "store",
        "$rootScope",
        "ModalService",
        "ProjectQuestions",
        "NeedsMoreInfoPermission",
    ];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private translations: IStringMap<string>;
    private viewState: IStringMap<boolean>;
    private project: IProject;
    private question: IQuestion;
    private questionClone: IQuestion;
    private currentUser: IUser;
    private subscription: Subscription;
    private reviewerName: string;
    private reviewerComment: string;
    private requestInProgress: boolean;

    constructor(
        private store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly ModalService: ModalService,
        private readonly ProjectQuestions: ProjectQuestions,
        private readonly NeedsMoreInfoPermission: NeedsMoreInfoPermission,
    ) {}

    public $onInit(): void {
        this.translations = {
            modalTitle: this.$rootScope.t("AddInformation"),
        };

        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((store) => this.componentWillReceiveState(store.modals.modalData));
    }

    public $onDestroy(): void {
        this.subscription.unsubscribe();
    }

    private componentWillReceiveState(modalData: any): void {
        if (modalData) {
            this.currentUser = modalData.currentUser;
            this.project = modalData.project;
            this.question = modalData.question;
            this.questionClone = cloneDeep(modalData.question);
            this.reviewerComment = modalData.question.MoreInfoComment ? modalData.question.MoreInfoComment : "---";
            this.reviewerName = modalData.reviewerName || this.$rootScope.t("Approver");
            this.updateViewState();
        }
    }

    private updateViewState(): void {
        this.viewState = {
            canSubmitAddInformationModal: (this.question.InfoAddedComment !== this.questionClone.InfoAddedComment) && this.NeedsMoreInfoPermission.hasQuestionPermission("canSubmitAddInformationModal", this.currentUser, this.project, this.question),
        };
    }

    private closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private handleInputChange(value: string): void {
        this.questionClone.InfoAddedComment = value;
        this.updateViewState();
    }

    private submit(): void {
        this.requestInProgress = true;
        const newState: IQuestionSerialized = {
            Id: this.questionClone.Id,
            State: QuestionStates.InfoAdded,
            Comment: this.questionClone.InfoAddedComment,
        };
        this.ProjectQuestions.updateState(newState).then((res: IProtocolPacket): void => {
            if (res.result) {
                this.question.InfoAddedComment = this.questionClone.InfoAddedComment;
                this.ModalService.closeModal();
                this.ModalService.handleModalCallback();
                this.ModalService.clearModalData();
            }
            this.requestInProgress = false;
        });
    }
}

const addInfoModalTemplate: IStringMap<string> = {
  template: `
    <section class="padding-all-2">
        <form name="addInfoForm">
            <div class="add-info__label">{{::$root.t("InformationRequested")}}</div>
            <blockquote class="blockquote">{{ $ctrl.reviewerComment }}
                <footer class="blockquote__footer">{{ $ctrl.reviewerName }}</footer>
            </blockquote>
            <div ng-if="$ctrl.question.InfoAddedComment" class="add-info__label">{{::$root.t("InformationAdded")}}</div>
            <blockquote ng-if="$ctrl.question.InfoAddedComment" class="blockquote">
                {{ $ctrl.question.InfoAddedComment }}
            </blockquote>
            <one-label-content
                name="{{$root.t('EnterInformationHere')}}"
                label-class="needs-info__label"
                wrapper-class="margin-bottom-2">
                <one-textarea
                    disable-resize="true"
                    input-changed="$ctrl.handleInputChange(value)"
                    name="Comment"
                    placeholder="{{::$root.t('EnterInformationHere')}}"
                    textarea-class="needs-info__textarea">
                </one-textarea>
                <p
                    class="text-center text-danger"
                    ng-if="$ctrl.showError">
                    {{::$root.t("EnterCommentsBeforeSubmitting")}}
                </p>
            </one-label-content>
            <footer class="row-flex-end margin-top-2 add-info__btn">
                <one-button
                    class="margin-right-1"
                    button-click="$ctrl.closeModal()"
                    text="{{::$root.t('Close')}}">
                </one-button>
                <one-button
                    button-click="$ctrl.submit()"
                    enable-loading="true"
                    is-loading="$ctrl.requestInProgress"
                    is-disabled="!$ctrl.viewState.canSubmitAddInformationModal || $ctrl.requestInProgress"
                    text="{{::$root.t('Submit')}}"
                    type="primary">
                </one-button>
            </footer>
        </form>
    </section>
`};

export default {
    controller: AddInfoModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(addInfoModalTemplate.template),
};
