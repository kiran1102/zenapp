import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { INotifyUserModal } from "interfaces/notify-user-modal.interface";

class NotifyUserModalController implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "ModalService"];

    private resolve: INotifyUserModal;
    private modalTitle: string;
    private notificationText: string;
    private closeButtonText: string;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
    ) { }

    public $onInit(): void {
        this.modalTitle = this.resolve.modalTitle;
        this.notificationText = this.resolve.notificationText;
        this.closeButtonText = this.resolve.closeButtonText || this.$rootScope.t("Ok");
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }
}

const notifyUserModalTemplate: { template: string } = {
    template: `
        <section class="notify-user-modal padding-all-2">
            <div class="notify-user-modal__text text-color-default">{{$ctrl.notificationText}}</div>
            <footer class="row-horizontal-center margin-top-2">
                <one-button
                    button-text-class="padding-left-right-3"
                    button-click="$ctrl.closeModal()"
                    type="primary"
                    text="{{$ctrl.closeButtonText}}"
                    is-rounded="true"
                    >
                </one-button>
            </footer>
        </section>
    `,
};

export default {
    controller: NotifyUserModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(notifyUserModalTemplate.template),
};
