// Rxjs
import {
    Observable,
    Subscription,
    Subject,
} from "rxjs";
import {
    map as rxMap,
    filter as rxFilter,
    startWith,
    withLatestFrom,
    first,
    takeUntil,
} from "rxjs/operators";

// Third Party
import invariant from "invariant";
import _, {
    concat,
    get,
    find,
    map,
    difference,
} from "lodash";

// Enums
import { RiskStates } from "enums/risk-states.enum";
import { OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { AssessmentPermissions } from "constants/assessment.constants";
import { getCurrentRelatedOrgIdsMap } from "oneRedux/reducers/org.reducer";
import { EmptyGuid } from "constants/empty-guid.constant";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IRisk, IRiskDetails, IRiskModalPermissionConfig, IHeatMapTile, IRiskHeatMapLabels, IRiskModalViewConfig } from "interfaces/risk.interface";
import { IStringToBooleanMap, IStore, IStoreState } from "interfaces/redux.interface";
import { IProject } from "interfaces/project.interface";
import { ISection } from "interfaces/section.interface";
import { IQuestion } from "interfaces/question.interface";
import { IUser } from "interfaces/user.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Models
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import {
    isAnyPropChanged,
    areAllPropsDefined,
    multiForkJoin,
    areAllEqualTo,
} from "oneRedux/utils/utils";
import { getTempRiskById } from "oneRedux/reducers/risk.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getUserById,
    isNotProjectViewer,
    getFilteredUsers,
    getAllUserIds,
    isNotInvitedUser,
    getCurrentUser,
} from "oneRedux/reducers/user.reducer";
import { isHeatMapEnabled } from "oneRedux/reducers/setting.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import RiskModalViewLogicService from "oneServices/view-logic/risk-modal-view-logic.service";
import RiskBusinessLogicService from "oneServices/logic/risk-business-logic.service";
import { OrgGroupApiService } from "oneServices/api/org-group-api.service";
import { UserActionService } from "oneServices/actions/user-action.service";

class RiskModalController implements ng.IComponentController {

    static $inject: string[] = [
        "store",
        "ModalService",
        "RiskBusinessLogic",
        "RiskAction",
        "UserActionService",
        "RiskModalViewLogic",
        "OrgGroups",
    ];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private dataLoaded = false;
    private canSubmit = false;
    private isSubmitting = false;
    private permissions: IRiskModalPermissionConfig;
    private translations: any;

    private minDeadlineDate: Date | null;

    private riskModel: IRisk;

    private riskOptions: IDropdownOption[];
    private defaultRiskOption: IDropdownOption;
    private selectedRisk: IDropdownOption;

    private axisLabels: IRiskHeatMapLabels;
    private tileMap: IHeatMapTile[][];
    private selectedHeatMap: IHeatMapTile | undefined;

    private selectedOwner: IDropdownOption;
    private userList: IOrgUserAdapted[];
    private currentUser: IUser;
    private projectModel: IProject;
    private viewState: IRiskModalViewConfig;
    private destroy$ = new Subject();

    constructor(
        private store: IStore,
        private modalService: ModalService,
        private riskBusinessLogic: RiskBusinessLogicService,
        private riskAction: RiskActionService,
        private userActionService: UserActionService,
        private riskModalViewLogic: RiskModalViewLogicService,
        private orgGroups: OrgGroupApiService,
        ) {
            this.currentUser = getCurrentUser(store.getState());
        }

    $onInit(): void {
        this.orgGroups.getOrgUsersWithPermission(this.currentUser.OrgGroupId, OrgUserTraversal.Branch, false, [AssessmentPermissions.CanBeRiskOwner]).then((res: IProtocolResponse<IOrgUserAdapted[]>) => {
            const riskId: string = getModalData(this.store.getState()).selectedModelId;
            const riskModel: IRisk = getTempRiskById(riskId)(this.store.getState()) as IRisk;
            this.projectModel = getModalData(this.store.getState()).projectModel; // ideally, build project reducer and get the project from there

            if (!res.result) {
                this.userList = this.findAndSetUsersRelatedToCurrentOrg(riskModel);
                this.initializeModal(riskId, riskModel);
            } else {
                const responseUserList = res.data;
                const cachedStoreUserIds = getAllUserIds(this.store.getState());
                const responseUserIdsList = map(responseUserList, (user) => user.Id);
                const unknownIds: string[] = difference(responseUserIdsList, cachedStoreUserIds);

                if (unknownIds.length > 0) {
                    this.userActionService.fetchUsers().then(() => {
                        this.initializeRiskUser();
                    });
                } else {
                    this.initializeRiskUser();
                }
                this.initializeModal(riskId, riskModel);
            }
        });
    }

    $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public handleTypeaheadChange(option: any): void {
        if (option) {
            // we have to send null to backend if we are removing a risk owner.
            let RiskOwnerId;
            if (option.IsPlaceholder) {
                RiskOwnerId = null;
            } else {
                RiskOwnerId = option.Id;
            }
            this.riskAction.updateTempRisk(this.riskModel.Id, { RiskOwnerId });
        }
    }

    public handleInputChange(payload: any, prop: string): void {
        this.riskAction.updateTempRisk(this.riskModel.Id, { [prop]: payload });
    }

    private initializeRiskUser(): void {
        const noInvited: IUser[] = getFilteredUsers(isNotInvitedUser)(this.store.getState());
        const sectionAssigneesWhoAreInvited: IUser[] = _(this.projectModel.Sections)
            .filter((section) => section.Id !== EmptyGuid)
            .map((section) => section.Assignment.AssigneeId)
            .uniqBy((id) => id)
            .map((id) => getUserById(id)(this.store.getState()))
            .filter((user) => user && user.RoleName === "Invited")
            .value();

        if (sectionAssigneesWhoAreInvited.length) {
            this.userList = [...noInvited, ...sectionAssigneesWhoAreInvited];
        } else {
            this.userList = noInvited;
        }
    }

    private initializeModal(riskId: string, riskModel: IRisk) {
        // we check if the risk is a new risk by checking its id. we do this because the user may have manually removed a risk owner
        if (!riskModel.RiskOwnerId && riskModel.Id.indexOf("temp") > -1) {
            const { section }: { section: ISection } = getModalData(this.store.getState());
            const riskOwnerId: string = section.Assignment.AssigneeId;
            const userModel: IOrgUserAdapted = find(this.userList, (user: IOrgUserAdapted) => user.Id === riskOwnerId);

            if (userModel) {
                this.riskAction.updateTempRisk(riskModel.Id, { RiskOwnerId: riskOwnerId });
            }
        }

        this.translations = this.riskBusinessLogic.getRiskModalTranslations(riskId);

        const minDeadlineDate: Date = new Date();
        minDeadlineDate.setDate(minDeadlineDate.getDate() + getModalData(this.store.getState()).reminderDays + 1); // get reminderdays from setting state
        this.minDeadlineDate = minDeadlineDate;

        this.prepareRiskLevelOptionsForTemplateUse();

        const store$: Observable<IStoreState> = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        );

        const riskModel$: Observable<IRisk | IRiskDetails> = store$.pipe(
            rxMap(getTempRiskById(riskId)),
            rxFilter((data: IRisk | undefined): boolean => Boolean(data)),
        );

        const origRiskModel$: Observable<IRisk | IRiskDetails> = riskModel$.pipe(first());

        riskModel$.pipe(
            withLatestFrom(origRiskModel$),
            takeUntil(this.destroy$),
        ).subscribe(([newModel, oldModel]): void => this.componentWillReceiveState(newModel as IRisk, oldModel as IRisk));

        this.dataLoaded = true;
    }

    private componentWillReceiveState(newModel: IRisk, oldModel: IRisk): void {
        this.viewState = this.riskModalViewLogic.calculateRiskModalViewState(this.currentUser, this.projectModel, oldModel, newModel);
        this.findAndSetMatchingRiskLevel(newModel);
        this.selectedOwner = this.findAndSetMatchingRiskOwner(newModel);
        this.canSubmit = this.validateRiskModal(oldModel, newModel);
        this.riskModel = newModel;
    }

    private prepareRiskLevelOptionsForTemplateUse(): void {
        this.riskBusinessLogic.getRiskLevelOptions()
            .pipe(takeUntil(this.destroy$))
            .subscribe((riskOptions: IDropdownOption[]) => {
                this.defaultRiskOption = riskOptions[0];
                this.riskOptions = riskOptions.slice(1);
                if (this.riskModel) {
                    this.findAndSetMatchingRiskLevel(this.riskModel);
                }
            });
        const { axisLabels, tileMap }: { axisLabels: IRiskHeatMapLabels, tileMap: IHeatMapTile[][] } = this.riskBusinessLogic.buildRiskTileMap(4, 4);
        this.axisLabels = axisLabels;
        this.tileMap = tileMap;
    }

    private findAndSetUsersRelatedToCurrentOrg(riskModel: IRisk): IUser[] {
        const { projectModel }: { projectModel: IProject } = getModalData(this.store.getState());
        const section: ISection = find(projectModel.Sections, (sectionItem: ISection): boolean => {
            return Boolean(find(sectionItem.Questions, (question: IQuestion): boolean => {
                return question.Id === riskModel.QuestionId;
            }));
        });
        const sectionAssigneeId: string = section.Assignment.AssigneeId;

        const relatedOrgIds: IStringToBooleanMap = getCurrentRelatedOrgIdsMap(this.store.getState());
        const userExistsInOrgIdMap = (user: IUser): boolean => Boolean(relatedOrgIds[user.OrgGroupId]);
        const removeDisabledUser = (user: IUser): boolean => Boolean(user.IsActive);
        const removeInvitedUsersWhoAreNotRespondentOrRiskOwner = (sectionAssigneeIdItem: string) => (user: IUser): boolean => Boolean(user.RoleName !== "Invited" || user.Id === sectionAssigneeIdItem || user.Id === riskModel.RiskOwnerId);
        const isUserNotInSiblingOrgsAndNotProjectViewer = multiForkJoin(areAllEqualTo(true), isNotProjectViewer, userExistsInOrgIdMap, removeInvitedUsersWhoAreNotRespondentOrRiskOwner(sectionAssigneeId), removeDisabledUser);
        const getUsersThatAreNotInSiblingOrgsAndNotProjectViewer = getFilteredUsers(isUserNotInSiblingOrgsAndNotProjectViewer);

        return getUsersThatAreNotInSiblingOrgsAndNotProjectViewer(this.store.getState());
    }

    private findAndSetMatchingRiskLevel(newModel: IRisk): void {
        if (isHeatMapEnabled(this.store.getState())) {
            const pathToModel = `[${this.tileMap.length - newModel.RiskImpactLevel}][${newModel.RiskProbabilityLevel - 1}]`;
            this.selectedHeatMap = get(this.tileMap, pathToModel);
        } else {
            this.selectedRisk = find(this.riskOptions, (option: IDropdownOption): boolean => option.value === newModel.Level);
        }
    }

    private findAndSetMatchingRiskOwner(newModel): IOrgUserAdapted {
        return find(this.userList, (user: IOrgUserAdapted): boolean => user.Id === newModel.RiskOwnerId);
    }

    private riskLevelSelected(option: IDropdownOption): void {
        if (isHeatMapEnabled(this.store.getState())) {
            const keyValueToMerge: any = { RiskProbabilityLevel: option.xValue, RiskImpactLevel: option.yValue, Level: option.riskLevel };
            this.riskAction.updateTempRisk(this.riskModel.Id, keyValueToMerge);
        } else {
            this.riskAction.updateTempRisk(this.riskModel.Id, { Level: option });
        }
    }

    private validateRiskModal(oldRiskModel: IRisk, newRiskModel: IRisk): boolean {
        const heatMapEnabled: boolean = isHeatMapEnabled(this.store.getState());
        const riskModel: IRisk = getTempRiskById(newRiskModel.Id)(this.store.getState()) as IRisk;
        invariant(heatMapEnabled !== undefined, "HeatMap Setting cannot be undefined when validating.");
        invariant(Boolean(riskModel), "Risk Model cannot be undefined when validating.");

        let propsThatMustBeDefined: string[] = ["Description", "Level"];
        let propsThatCanChange: string[] = [...propsThatMustBeDefined, "Deadline", "RiskOwnerId"];

        if (heatMapEnabled) {
            // commented out due to risks that were created before heatmap enable and that don't have impact and probability
            // propsThatMustBeDefined = concat(propsThatMustBeDefined, ["RiskImpactLevel", "RiskProbabilityLevel"]);
            propsThatCanChange = concat(propsThatCanChange, ["RiskImpactLevel", "RiskProbabilityLevel"]);
        }

        const { Identified, Analysis, Addressed, Exception } = RiskStates;
        const riskModelState = riskModel.State;

        switch (riskModelState) {
            case Identified:
                propsThatCanChange = concat(propsThatCanChange, "Recommendation");
                break;
            case Analysis:
                propsThatCanChange = concat(propsThatCanChange, ["Recommendation", "Mitigation", "RequestedException"]);
                break;
            case Addressed:
                propsThatMustBeDefined = concat(propsThatMustBeDefined, "Mitigation");
                propsThatCanChange = concat(propsThatCanChange, "Mitigation");
                break;
            case Exception:
                propsThatMustBeDefined = concat(propsThatMustBeDefined, "RequestedException");
                propsThatCanChange = concat(propsThatCanChange, "RequestedException");
                break;
        }

        return areAllPropsDefined(propsThatMustBeDefined)(oldRiskModel, newRiskModel)
            && isAnyPropChanged(propsThatCanChange)(oldRiskModel, newRiskModel);
    }

    private closeModal(): void {
        this.riskAction.deleteTempRisk(this.riskModel.Id);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private submitModal(): void {
        const { reminderDays }: { reminderDays: number } = getModalData(this.store.getState());
        const riskModelWithReminderDays: IRisk = { ...this.riskModel, ReminderDays: reminderDays };

        this.isSubmitting = true;

        this.riskAction.saveQuestionRisk(riskModelWithReminderDays).then((newRiskId: string): ng.IPromise<void> | ng.IPromise<any> | boolean => {
            if (newRiskId) {
                return this.riskAction.reloadRiskModel(newRiskId).then((res: any): void => {
                    this.riskAction.deleteTempRisk(this.riskModel.Id);
                    this.modalService.closeModal();
                    this.modalService.updateModalData({ selectedModelId: newRiskId });
                    this.modalService.handleModalCallback();
                    this.modalService.clearModalData();
                });
            }
            this.isSubmitting = false;
            return false;
        });
    }
}

const riskModalTemplate: any = {
    template: `
    <loading ng-if="!$ctrl.dataLoaded" show-text="false" style="height: 300px;"></loading>
    <section ng-if="$ctrl.dataLoaded" class="padding-all-2 risk-modal">
        <one-label-content
            class="flex-grow-1"
            ng-if="$ctrl.viewState.riskHeatmapShown || $ctrl.viewState.riskHeatmapEnabled"
            name={{$ctrl.translations.riskLevel}}
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-risk-tile-button
                axis-labels="$ctrl.axisLabels"
                tile-map="$ctrl.tileMap"
                selected-model="$ctrl.selectedHeatMap"
                on-change="$ctrl.riskLevelSelected(value)"
                is-disabled="!$ctrl.viewState.riskHeatmapEnabled"
            ></one-risk-tile-button>
        </one-label-content>

        <one-label-content
            class="flex-grow-1 risk-modal__input--half-width"
            ng-if="$ctrl.viewState.riskLevelShown || $ctrl.viewState.riskLevelEnabled"
            name={{$ctrl.translations.riskLevel}}
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2 risk-modal__label--half-width"
        >
            <reactive-dropdown
                button-class="risk-modal__dropdown-button width-100-percent text-left"
                dropdown-class="risk-modal__dropdown"
                left-icon-class="{{$ctrl.selectedRisk ? $ctrl.selectedRisk.icon : $ctrl.defaultRiskOption.icon}}"
                right-icon-class="risk-modal__dropdown-arrow"
                item-class="risk-modal__dropdown-item"
                list-class="width-100-percent"
                text="{{$ctrl.translations.riskLevel}}"
                options="$ctrl.riskOptions"
                default-option="$ctrl.defaultRiskOption"
                selected-model="$ctrl.selectedRisk"
                on-change="$ctrl.riskLevelSelected(value)"
                is-disabled="!$ctrl.viewState.riskLevelEnabled"
            ></reactive-dropdown>
        </one-label-content>

        <div class="flex-full-width row-space-between">
            <one-label-content
                class="flex-grow-1 flex-basis-0 margin-right-2"
                ng-if="$ctrl.viewState.riskDeadlineShown || $ctrl.viewState.riskDeadlineEnabled"
                name={{$ctrl.translations.deadline}}
                label-class="risk-modal__label"
                wrapper-class="margin-bottom-2"
            >
                <one-datepicker
                    date-model="$ctrl.riskModel.Deadline"
                    placeholder="{{::$ctrl.translations.deadline}}"
                    on-change="$ctrl.handleInputChange(value, 'Deadline')"
                    is-disabled="!$ctrl.viewState.riskDeadlineEnabled"
                ></one-datepicker>
            </one-label-content>

            <one-label-content
                class="flex-grow-1 flex-basis-0"
                ng-if="$ctrl.viewState.riskOwnerShown || $ctrl.viewState.riskOwnerEnabled"
                name={{$ctrl.translations.owner}}
                is-required="false"
                label-class="risk-modal__label"
                wrapper-class="margin-bottom-2"
            >
                <typeahead
                    class="risk-modal__typeahead"
                    options="$ctrl.userList"
                    model="$ctrl.selectedOwner"
                    on-select="$ctrl.handleTypeaheadChange(selection)"
                    label-key="FullName"
                    placeholder-text="$ctrl.translations.ownerInputPlaceholder"
                    allow-other="false"
                    other-option-prepend="{{::$root.t('ReAssignTo')}}"
                    disable-if-no-options="false"
                    is-disabled="!$ctrl.viewState.riskOwnerEnabled"
                    allow-empty="true"
                    empty-text="{{::$root.t('NoRiskOwner')}}"
                    input-limit="2"
                    use-virtual-scroll="true"
                    options-limit="1000"
                ></typeahead>
            </one-label-content>
        </div>

        <one-label-content
            ng-if="$ctrl.viewState.riskSummaryShown || $ctrl.viewState.riskSummaryEnabled"
            name={{$ctrl.translations.description}}
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-1"
        >
            <one-textarea
                input-text={{$ctrl.riskModel.Description}}
                input-changed="$ctrl.handleInputChange(value, 'Description')"
                is-disabled="!$ctrl.viewState.riskSummaryEnabled"
                max-length="4000"
                is-required="true"
                show-char-count="true"
            ></one-textarea>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskRecommendationShown || $ctrl.viewState.riskRecommendationEnabled"
            name={{$ctrl.translations.recommendation}}
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text={{$ctrl.riskModel.Recommendation}}
                input-changed="$ctrl.handleInputChange(value, 'Recommendation')"
                is-disabled="!$ctrl.viewState.riskRecommendationEnabled"
                max-length="4000"
                show-char-count="true"
            ></one-textarea>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskRemediationShown || $ctrl.viewState.riskRemediationEnabled"
            name={{$ctrl.translations.mitigation}}
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text={{$ctrl.riskModel.Mitigation}}
                input-changed="$ctrl.handleInputChange(value, 'Mitigation')"
                is-disabled="!$ctrl.viewState.riskRemediationEnabled"
                max-length="1000"
                is-required="true"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.Mitigation.length || 0}} / 1000</div>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskExceptionShown || $ctrl.viewState.riskExceptionEnabled"
            name={{$ctrl.translations.requestedException}}
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text={{$ctrl.riskModel.RequestedException}}
                input-changed="$ctrl.handleInputChange(value, 'RequestedException')"
                is-disabled="!$ctrl.viewState.riskExceptionEnabled"
                max-length="1000"
                is-required="true"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.RequestedException.length || 0}} / 1000</div>
        </one-label-content>

        <footer class="row-flex-end margin-top-2">
            <one-button
                button-class="margin-right-2"
                button-click="$ctrl.closeModal()"
                text="{{$ctrl.translations.cancel}}"
            ></one-button>
            <one-button
                button-click="$ctrl.submitModal()"
                enable-loading="true"
                is-loading="$ctrl.isSubmitting"
                is-disabled="!$ctrl.canSubmit || $ctrl.isSubmitting"
                text="{{$ctrl.translations.submit}}"
                type="primary"
            ></one-button>
        </footer>
    </section>
`};

export default {
    controller: RiskModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(riskModalTemplate.template),
};
