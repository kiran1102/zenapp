// Rxjs
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    forEach,
    findIndex,
    uniqBy,
    remove,
    find,
    values,
    includes,
    map,
    filter,
} from "lodash";
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getCurrentRecord,
    getAttributeMap,
    getInventoryId,
} from "oneRedux/reducers/inventory.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryRelatedService } from "modules/inventory/services/adapter/inventory-related.service";

// Interfaces
import { IInventoryRecord } from "interfaces/inventory.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";
import {
    IRelatedListOption,
    IInventoryCellValue,
    IAttribute,
} from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IAttributeMap } from "interfaces/inventory-reducer.interface";

// Const and Enums
import { InventoryTableIds } from "enums/inventory.enum";
import {
    InventoryTranslationKeys,
    RelatedInventoryAttributes,
} from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "relate-inventory-modal",
    templateUrl: "./relate-inventory-modal.component.html",
})
export class RelateInventoryModalComponent implements OnInit {
    modalData: IRelateInventoryModalResolve;
    modalTitle: string;
    submitButtonText: string;
    cancelButtonText: string;
    isLoadingInventory = true;
    isSaving = false;
    requiredFieldsPopulated = false;
    selectedOption: IRelatedListOption = null;
    selectedOptions: IRelatedListOption[] = [];
    howIsInventoryRelatedText: string;
    chooseInventoryItemText: string;
    multiSelectEnabled: boolean;
    relationshipOptions: IAttribute[];
    relatedAttributes: IStringMap<boolean> = {};
    orgGroupId: string;
    filteredInventoryList: IRelatedListOption[] = [];
    inventoryList: IRelatedListOption[] = [];
    private record: IInventoryRecord;
    private inventoryType: number;
    private recordInventoryType: number;
    private attributeMap: IAttributeMap;

    constructor(
        @Inject(StoreToken) private store: IStore,
        private modalService: ModalService,
        private inventoryRelatedService: InventoryRelatedService,
        private inventoryActionService: InventoryActionService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit(): void {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.inventoryType = this.modalData.inventoryType;
        this.orgGroupId = this.inventoryService.getOrgGroupId(this.record, this.recordInventoryType);
        this.multiSelectEnabled = this.inventoryType === InventoryTableIds.Processes;
        this.modalTitle = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].addRelatedInventoryHeader) || "";
        this.howIsInventoryRelatedText = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].howIsInventoryRelatedText) || "";
        this.chooseInventoryItemText = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].chooseInventoryItem) || "";
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("AddRelated");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.getInventoryList();
        this.getRelationshipOptions();
    }

    onSelect(event: any): void {
        if (this.multiSelectEnabled) {
            this.selectedOptions.push(event.$item);
        } else {
            this.selectedOption = event.currentValue;
            this.filteredInventoryList = this.removeSelectionFromOptions(this.inventoryList, this.selectedOption);
        }
        this.hasFieldsPopulated();
    }

    onRemove(event: any): void {
        remove(this.selectedOptions, (option: IRelatedListOption): boolean => {
            return event.$item === option;
        });
        this.hasFieldsPopulated();
    }

    hasFieldsPopulated(): void {
        this.requiredFieldsPopulated = (Boolean(this.selectedOption) && this.attributeSelected()) || Boolean(this.selectedOptions.length);
    }

    attributeSelected(): boolean {
        return includes(values(this.relatedAttributes), true);
    }

    saveInventory(closeModal: boolean = true): void {
        this.isSaving = true;
        const updatedRecord = this.formatRecordForSave();
        this.inventoryActionService.saveRecord(updatedRecord).then((response: IProtocolResponse<IInventoryRecord>): void => {
            if (response.result) {
                if (this.modalData.callback) {
                    this.modalData.callback();
                }
                if (closeModal) {
                    this.closeModal();
                } else {
                    this.defaultForm();
                    this.hasFieldsPopulated();
                }
            }
            this.isSaving = false;
        });
    }

    checkboxChange(attributeId: string): void {
        this.relatedAttributes[attributeId] = !this.relatedAttributes[attributeId];
        this.hasFieldsPopulated();
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    searchItems({value}) {
        this.filteredInventoryList = this.removeSelectionFromOptions(this.inventoryList, this.selectedOption);
        if (!value) return;
        this.filteredInventoryList = filter(this.filteredInventoryList, (option: IRelatedListOption): boolean => {
            return option.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
        });
    }

    private defaultForm(): void {
        this.selectedOption = null;
        forEach(this.relatedAttributes, (value: boolean, key: string): void => {
            this.relatedAttributes[key] = false;
        });
    }

    private getRelationshipOptions(): void {
        const options: string[] = RelatedInventoryAttributes[this.inventoryType];
        this.relationshipOptions = [];
        forEach(options, (option: string): void => {
            const attribute = find(this.attributeMap, ["fieldAttributeCode", option]);
            if (attribute) {
                this.relationshipOptions.push(attribute);
                this.relatedAttributes[attribute.id] = false;
            }
        });
    }

    private getInventoryList(): void {
        this.inventoryRelatedService.getRelatedInventoryList(this.inventoryType, this.orgGroupId).then((response: IRelatedListOption[]): void => {
            if (response) {
                this.isLoadingInventory = false;
                this.inventoryList = this.formatInventoryList(response);
                this.filteredInventoryList = [...this.inventoryList];
            }
        });
    }

    private formatInventoryList(options: IRelatedListOption[]): IRelatedListOption[] {
        return map(options, (option: IRelatedListOption): IRelatedListOption => {
            if (option.location) {
                option.label = `${option.name} - ${option.organization.value} - ${option.location.value}`;
            } else {
                option.label = `${option.name} - ${option.organization.value}`;
            }
            return option;
        });
    }

    private formatRecordForSave(): IInventoryRecord {
        const updatedRecord = this.record;
        if (this.multiSelectEnabled) {
            const attributeIndex = findIndex(updatedRecord.cells, ["attributeCode", RelatedInventoryAttributes[this.inventoryType]]);
            const options = this.formatOptionsForSave(this.selectedOptions);
            const currentValues = updatedRecord.cells[attributeIndex].values || [];
            updatedRecord.cells[attributeIndex].values = uniqBy([...currentValues, ...options], "valueId");
        } else {
            forEach(this.relationshipOptions, (relationshipOption: IAttribute): void => {
                if (this.relatedAttributes[relationshipOption.id]) {
                    const option = this.formatOptionForSave(this.selectedOption);
                    const attributeIndex = findIndex(updatedRecord.cells, ["attributeId", relationshipOption.id]);
                    const currentValues = updatedRecord.cells[attributeIndex].values || [];
                    updatedRecord.cells[attributeIndex].values = uniqBy([...currentValues, option], "valueId");
                }
            });
        }
        return updatedRecord;
    }

    private formatOptionsForSave(options: IRelatedListOption[]): IInventoryCellValue[] {
        const formattedOptions: IInventoryCellValue[] = [];
        forEach(options, (option: IRelatedListOption): void => {
            formattedOptions.push(this.formatOptionForSave(option));
        });
        return formattedOptions;
    }

    private formatOptionForSave(option: IRelatedListOption): IInventoryCellValue {
        const formattedOption: IInventoryCellValue = {
            value: "",
            valueId: "",
        };
        formattedOption.value = option.name;
        formattedOption.valueId = option.id;
        return formattedOption;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.record = getCurrentRecord(state);
        this.attributeMap = getAttributeMap(state);
        this.modalData = getModalData(state);
        this.recordInventoryType = getInventoryId(state);
    }

    private removeSelectionFromOptions(options: IRelatedListOption[], selection: IRelatedListOption): IRelatedListOption[] {
        if (!selection) return options;
        return filter(options, (option: IRelatedListOption): boolean => option.id !== selection.id);
    }
}
