import { IStringMap } from "interfaces/generic.interface";
import { isFunction } from "lodash";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { ISaveChangesModalResolve } from "interfaces/save-changes-modal-resolve.interface";

class SaveChangesModalController implements ng.IComponentController {

    public static $inject: string[] = ["$rootScope", "ModalService"];

    private resolve: ISaveChangesModalResolve;
    private promiseToResolve: () => ng.IPromise<any>;
    private closeCallback: () => any;
    private discardCallback: () => any;
    private modalTitle: string;
    private confirmationText: string;
    private cancelButtonText: string;
    private discardChangesButtonText: string;
    private saveButtonText: string;
    private iconClass: string;
    private isSubmitting: boolean;
    private showDiscard: boolean;
    private showSave: boolean;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
    ) { }

    public $onInit(): void {
        this.promiseToResolve = this.resolve.promiseToResolve;
        this.modalTitle = this.resolve.modalTitle;
        this.confirmationText = this.resolve.confirmationText;
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.discardChangesButtonText = this.resolve.discardChangesButtonText || this.$rootScope.t("Discard");
        this.saveButtonText = this.resolve.saveButtonText || this.$rootScope.t("Save");
        this.iconClass = this.resolve.iconClass || "";
        this.discardCallback = this.resolve.discardCallback;
        this.closeCallback = this.resolve.closeCallback;
        this.showDiscard = this.resolve.hasOwnProperty("showDiscard") ? this.resolve.showDiscard : true;
        this.showSave = this.resolve.hasOwnProperty("showSave") ? this.resolve.showSave : true;
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
        if (isFunction(this.closeCallback)) {
            this.closeCallback();
        }
    }

    private handleDiscard(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
        if (isFunction(this.discardCallback)) {
            this.discardCallback();
        }
    }

    private handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.clearModalData();
                }
                this.isSubmitting = false;
            });
        }
    }
}

const saveChangesModalTemplate: IStringMap<string> = {
    template: `
        <section class="save-changes-modal padding-all-2">
            <div class="row-horizontal-center margin-bottom-2">
                <i class="save-changes-modal__icon ot ot-exclamation-circle color-orange"></i>
            </div>
            <div class="save-changes-modal__text text-center text-color-default">{{$ctrl.confirmationText}}</div>
            <footer class="static-vertical margin-top-2 row-nowrap">
                <one-button
                    class="margin-right-1 margin-left-auto"
                    button-class="{{$ctrl.showSave && $ctrl.showDiscard ? 'one-button--flat' : 'one-button'}}"
                    button-click="$ctrl.closeModal()"
                    is-disabled="$ctrl.isSubmitting"
                    text="{{::$ctrl.cancelButtonText}}"
                    is-rounded="true"
                    >
                </one-button>
                <one-button
                    class="margin-right-1"
                    button-click="$ctrl.handleDiscard()"
                    is-disabled="$ctrl.isSubmitting"
                    ng-if="$ctrl.showDiscard"
                    text="{{::$ctrl.discardChangesButtonText}}"
                    is-rounded="true"
                    >
                </one-button>
                <one-button
                    button-click="$ctrl.handleSubmit()"
                    enable-loading="true"
                    ng-if="$ctrl.showSave"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="$ctrl.isSubmitting"
                    text="{{::$ctrl.saveButtonText}}"
                    type="primary"
                    is-rounded="true"
                    >
                </one-button>
            </footer>
        </section>
    `,
};

export const saveChangesModal: ng.IComponentOptions = {
    controller: SaveChangesModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(saveChangesModalTemplate.template),
};
