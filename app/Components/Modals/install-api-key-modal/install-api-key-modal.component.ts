import Utilities from "Utilities";

import { ModalService } from "sharedServices/modal.service";
import { NotificationService } from "modules/shared/services/provider/notification.service";

import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

import { IInstallApiKeyModalResolve } from "modules/intg/interfaces/integration.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class InstallAPIKeyModalCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "ModalService", "NotificationService"];

    private resolve: IInstallApiKeyModalResolve;
    private modalTitle: string;
    private cancelButtonText: string;
    private apiKey: string;
    private connectionName: string;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly ModalService: ModalService,
        private notificationService: NotificationService,
    ) {}

    public $onInit(): void {
        this.modalTitle = this.resolve.modalTitle || this.$rootScope.t("InstallSuccessful");
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Close");
        this.apiKey = this.resolve.apiKey;
        this.connectionName = this.resolve.connectionName;
    }

    private closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private copyAPIKey(apikey: string): void {
        Utilities.copyToClipboard(apikey);
        this.notificationService.alertSuccess(
            this.$rootScope.t("Success"),
            this.$rootScope.t("APIKeyCopied"),
        );
    }
}

const showAPIKeyHTML = `
        <section class="flex-full-height padding-all-2">
            <div class="column-horizontal-vertical-center">
                <one-numbered-circle
                    class="margin-top-1"
                    circle-class="ot ot-check-square-o"
                    type="success"
                    size="large"
                    >
                </one-numbered-circle>
            </div>
            <h4 class="margin-top-4 text-center">
                {{::$root.t("InstallAPIModalMessage", { connectionName: $ctrl.connectionName }) }}
            </h4>
            <div class="margin-top-bottom-1 padding-top-bottom-1 row-horizontal-center">
                <span class="text-medium text-bold" ng-bind="$ctrl.apiKey"></span>
                <i
                    ng-click="$ctrl.copyAPIKey($ctrl.apiKey)"
                    class="ot ot-copy text-link text-decoration-none text-color-default margin-left-right-1 padding-top-half"
                ></i>
            </div>
            <footer class="margin-top-4 text-center static-vertical">
                <one-button
                    identifier="ApiKeymodalCancel"
                    type="primary"
                    is-rounded="true"
                    button-class="margin-right-1"
                    button-click="$ctrl.closeModal()"
                    text="{{::$ctrl.cancelButtonText}}">
                </one-button>
            </footer>
        </section>
`;

export const installAPIKeyModal: ng.IComponentOptions = {
    controller: InstallAPIKeyModalCtrl,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(showAPIKeyHTML),
};
