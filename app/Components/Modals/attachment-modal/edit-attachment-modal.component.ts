// Templates
import { buildDefaultModalTemplate } from "generalcomponent/Modals/modal-template/modal-template";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { AttachmentService } from "oneServices/attachment.service";
import { ModalService } from "sharedServices/modal.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IEditAttachmentModalResolve } from "interfaces/edit-attachment-modal-resolve.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// constants
import { InventoryPermissions } from "constants/inventory.constant";

// enums
import { InventoryTableIds } from "enums/inventory.enum";

class EditAttachmentModalController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "ModalService",
        "AttachmentService",
        "Permissions",
    ];

    private resolve: IEditAttachmentModalResolve;
    private modalTitle: string;
    private cancelButtonText: string;
    private submitButtonText: string;
    private isSubmitting: boolean;
    private requestId: string;
    private attachmentName: string;
    private attachmentComment: string;
    private inventoryId: number;
    private translatedComment: string;

    private attachmentResponse: IAttachmentFileResponse[];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private modalService: ModalService,
        private attachmentService: AttachmentService,
        private permissions: Permissions,
    ) { }

    public $onInit(): void {
        this.modalTitle = this.resolve.modalTitle || this.$rootScope.t("EditAttachment");
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.submitButtonText = this.resolve.submitButtonText || this.$rootScope.t("Save");
        this.attachmentName = this.resolve.attachmentName;
        this.attachmentComment = this.resolve.attachmentComment;
        this.requestId = this.resolve.requestId;
        this.inventoryId = this.resolve.inventoryId;
        this.translatedComment = this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorDocumentTab)
                                && this.inventoryId === InventoryTableIds.Vendors ? this.$rootScope.t("Description") : this.$rootScope.t("Comments");
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private handleCommentChange(value: string): void {
        this.attachmentComment = value;
    }

    private handleSubmit(): void {
        this.isSubmitting = true;
        this.attachmentService.editAttachment(
            this.requestId, { Name: this.attachmentName, Comments: this.attachmentComment },
        ).then((attachmentResponse: IProtocolResponse<IAttachmentFileResponse[]>): void => {
            if (attachmentResponse.result) {
                this.modalService.handleModalCallback(attachmentResponse);
                this.closeModal();
            }
            this.isSubmitting = false;
        });
    }
}

const editAttachmentModalTemplate = `
    <section class="edit-attachment-modal padding-all-2">
        <section>
            <one-label-content
                name="{{::$root.t('Name')}}"
                label-class="edit-attachment-modal__name-label"
                wrapper-class="margin-bottom-2"
                is-required="true"
                >
                <input
                    type="text"
                    class="one-input full-width"
                    ng-model="$ctrl.attachmentName"
                    ng-change="$ctrl.handleChange({text: $ctrl.text})"
                    required
                    >
                </input>
            </one-label-content>
            <one-label-content
                name="{{$ctrl.translatedComment}}"
                label-class="edit-attachment-modal__comments-label"
                wrapper-class="margin-bottom-2">
                <one-textarea
                    disable-resize="true"
                    input-text="{{$ctrl.attachmentComment}}"
                    input-changed="$ctrl.handleCommentChange(value)"
                    name="Comment"
                    placeholder="{{::$root.t('EnterInformationHere')}}"
                    textarea-class="attachment-modal__comments-textarea"
                    >
                </one-textarea>
            </one-label-content>
        </section>
        <footer class="row-flex-end margin-top-2">
            <one-button
                is-rounded="true"
                button-class="margin-right-1"
                button-click="$ctrl.closeModal()"
                text="{{::$ctrl.cancelButtonText}}"
                >
            </one-button>
            <one-button
                button-click="$ctrl.handleSubmit()"
                enable-loading="true"
                is-loading="$ctrl.isSubmitting"
                is-disabled="$ctrl.isSubmitting || !$ctrl.attachmentName"
                is-rounded="true"
                text="{{::$ctrl.submitButtonText}}"
                type="primary"
                >
            </one-button>
        </footer>
    </section>
`;

export const editAttachmentModal: ng.IComponentOptions = {
    controller: EditAttachmentModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(editAttachmentModalTemplate),
};
