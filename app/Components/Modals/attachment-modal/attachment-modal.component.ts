// 3rd Party
import {
    isArray,
    isFunction,
    filter,
    forEach,
} from "lodash";

// Templates
import { buildDefaultModalTemplate } from "generalcomponent/Modals/modal-template/modal-template";

// Services
import { ModalService } from "sharedServices/modal.service";
import { AttachmentService } from "oneServices/attachment.service";

// Interfaces
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IAttachmentFile,
    IAttachmentFileResponse,
} from "interfaces/attachment-file.interface";
import { IAttachmentModalResolve } from "interfaces/attachment-modal-resolve.interface";
import {
    IProtocolPacket,
    IProtocolResponse,
} from "interfaces/protocol.interface";
import { Permissions } from "modules/shared/services/helper/permissions.service";

// constants
import { InventoryPermissions } from "constants/inventory.constant";

// enums
import { InventoryTableIds } from "enums/inventory.enum";

class AttachmentModalController implements ng.IComponentController {

    static $inject: string[] = [
        "$rootScope",
        "ModalService",
        "AttachmentService",
        "Permissions",
    ];

    private resolve: IAttachmentModalResolve;
    private modalTitle: string;
    private cancelButtonText: string;
    private submitButtonText: string;
    private showFileList: boolean;
    private allowAttachmentNaming: boolean;
    private allowMultipleUploads: boolean;
    private allowComments: boolean;
    private isSubmitting: boolean;
    private acceptedFileTypes: string;
    private attachmentType: number;
    private requestId: string;
    private attachmentName: string;
    private attachmentComment: string;
    private isInternal: boolean;
    private inventoryId: number;
    private translatedComment: string;

    private attachments: IAttachmentFile[];
    private promiseArr: Array<ng.IPromise<IProtocolPacket>>;
    private attachmentResponse: IAttachmentFileResponse[];

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private modalService: ModalService,
        private attachmentService: AttachmentService,
        private permissions: Permissions,
    ) { }

    public $onInit(): void {
        this.modalTitle = this.resolve.modalTitle || this.$rootScope.t("UploadAttachments");
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.submitButtonText = this.resolve.submitButtonText || this.$rootScope.t("Upload");
        this.acceptedFileTypes = this.resolve.acceptedFileTypes || ".csv, .doc, .docx, .jpg, .jpeg, .mpp, .msg, .pdf, .png, .ppt, .pptx, .txt, .vsd, .vsdx, .xls, .xlsx";
        this.isInternal = this.resolve.isInternal || true;
        this.showFileList = this.resolve.showFileList || false;
        this.allowComments = this.resolve.allowComments || false;
        this.allowAttachmentNaming = this.resolve.allowAttachmentNaming || false;
        this.allowMultipleUploads = this.resolve.allowMultipleUploads || false;
        this.attachmentType = this.resolve.attachmentType;
        this.requestId = this.resolve.requestId;
        this.inventoryId = this.resolve.inventoryId;
        this.translatedComment = this.permissions.canShow(InventoryPermissions[this.inventoryId].vendorDocumentTab)
                                && this.inventoryId === InventoryTableIds.Vendors ? this.$rootScope.t("Description") : this.$rootScope.t("Comments");
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private handleCommentChange(value: string): void {
        this.attachmentComment = value;
    }

    private handleSubmit(): void {
        this.isSubmitting = true;
        this.attachmentService.handleAttachmentUpload(
            this.attachments,
            this.requestId,
            this.isInternal,
            this.attachmentType,
            this.attachmentName,
            this.attachmentComment,
        ).then((attachmentResponse: IProtocolResponse<IAttachmentFileResponse[]>): void => {
            if (attachmentResponse.result) {
                this.modalService.handleModalCallback(attachmentResponse);
                this.closeModal();
            }
            this.isSubmitting = false;
        });
    }

    private uploadFiles(files: IAttachmentFile[]) {
        if (isArray(files) && files.length) {
            this.attachments = files;
        }
    }
}

const attachmentModalTemplate = `
    <section class="attachment-modal padding-all-2">
        <section>
            <one-label-content
                ng-if="$ctrl.allowAttachmentNaming"
                name="{{::$root.t('Name')}}"
                label-class="attachment-modal__name-label"
                wrapper-class="margin-bottom-2"
                is-required="true"
                >
                <input
                    type="text"
                    class="one-input full-width"
                    ng-model="$ctrl.attachmentName"
                    ng-change="$ctrl.handleChange({text: $ctrl.text})"
                    required
                    >
                </input>
            </one-label-content>
            <one-label-content
                ng-if="$ctrl.allowComments"
                name="{{$ctrl.translatedComment}}"
                label-class="attachment-modal__comments-label"
                wrapper-class="margin-bottom-2">
                <one-textarea
                    disable-resize="true"
                    input-changed="$ctrl.handleCommentChange(value)"
                    name="Comment"
                    placeholder="{{::$root.t('EnterInformationHere')}}"
                    textarea-class="attachment-modal__comments-textarea"
                    >
                </one-textarea>
            </one-label-content>
            <file-input
                class="margin-top-2"
                allow-multiple="$ctrl.allowMultipleFiles"
                show-file-list="$ctrl.showFileList"
                accept="{{$ctrl.acceptedFileTypes}}"
                done="$ctrl.uploadFiles(files)"
                name="Data"
                show-file-name="$ctrl.showFileName"
                select-text="::$root.t('UploadAttachments')"
                >
            </file-input>
        </section>
        <footer class="row-flex-end margin-top-2">
            <one-button
                is-rounded="true"
                button-class="margin-right-1"
                button-click="$ctrl.closeModal()"
                text="{{::$ctrl.cancelButtonText}}"
                >
            </one-button>
            <one-button
                button-click="$ctrl.handleSubmit()"
                enable-loading="true"
                is-loading="$ctrl.isSubmitting"
                is-disabled="$ctrl.isSubmitting || !$ctrl.attachments.length || ($ctrl.allowAttachmentNaming && !$ctrl.attachmentName)"
                is-rounded="true"
                text="{{::$ctrl.submitButtonText}}"
                type="primary"
                >
            </one-button>
        </footer>
    </section>
`;

export const attachmentModal: ng.IComponentOptions = {
    controller: AttachmentModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(attachmentModalTemplate),
};
