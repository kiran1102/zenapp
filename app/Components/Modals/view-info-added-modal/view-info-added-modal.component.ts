// Rxjs
import { Subscription } from "rxjs";
import { startWith } from "rxjs/operators";

// Enums
import { QuestionStates } from "enums/assessment-question.enum";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IUser } from "interfaces/user.interface";
import { IQuestion } from "interfaces/question.interface";
import { IProject } from "interfaces/project.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IViewInfoAddedModalData } from "interfaces/assessment.interface";

// Models
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "app/scripts/Shared/Services/modal.service";
import NeedsMoreInfoPermission from "generalcomponent/Assessment/assessment-nmi-permission.service";

class ViewInfoAddedModalController implements ng.IComponentController {
    static $inject: string[] = [
        "store",
        "$rootScope",
        "ModalService",
        "NeedsMoreInfoPermission",
    ];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private translations: IStringMap<string>;
    private modalData: any;
    private viewState: IStringMap<boolean>;
    private project: IProject;
    private question: IQuestion;
    private currentUser: IUser;
    private subscription: Subscription;
    private reviewerName: string;
    private reviewerComment: string;
    private infoAddedComment: string;

    constructor(
        private store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly ModalService: ModalService,
        private readonly NeedsMoreInfoPermission: NeedsMoreInfoPermission,
    ) {}

    $onInit(): void {
        this.translations = {
            modalTitle: this.$rootScope.t("ViewInformationAdded"),
        };

        this.subscription = observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((store) => this.componentWillReceiveState(store.modals.modalData));
    }

    $onDestroy(): void {
        this.subscription.unsubscribe();
    }

    needInfo = (): void => {
        const selectedQuestionState: number = QuestionStates.InfoRequested;
        const isFromViewInfoAddedModal = true;
        this.question.State = QuestionStates.InfoRequested;
        this.ModalService.updateModalData({ ...this.modalData });
        this.ModalService.closeModal();
        this.ModalService.handleModalCallback({ selectedQuestionState, isFromViewInfoAddedModal });
    }

    accept = (): void => {
        const selectedQuestionState: number = QuestionStates.Accepted;
        this.ModalService.updateModalData({ ...this.modalData });
        this.ModalService.closeModal();
        this.ModalService.handleModalCallback({ selectedQuestionState });
    }

    closeModal(): void {
        this.ModalService.closeModal();
        this.ModalService.clearModalData();
    }

    private componentWillReceiveState(modalData: IViewInfoAddedModalData): void {
        if (modalData) {
            this.modalData = modalData;
            this.currentUser = modalData.currentUser;
            this.project = modalData.project;
            this.question = modalData.question;
            this.reviewerComment = modalData.question.MoreInfoComment ? modalData.question.MoreInfoComment : "---";
            this.infoAddedComment = modalData.question.InfoAddedComment ? modalData.question.InfoAddedComment : "---";
            this.reviewerName = modalData.reviewerName || this.$rootScope.t("Approver");
            this.viewState = {
                canViewNeedMoreInfoButtonInfoAddedModal: this.NeedsMoreInfoPermission.hasQuestionPermission("canViewNeedMoreInfoButtonInfoAddedModal", this.currentUser, this.project, this.question),
                canViewAcceptButtonInfoAddedModal: this.NeedsMoreInfoPermission.hasQuestionPermission("canViewAcceptButtonInfoAddedModal", this.currentUser, this.project, this.question),
            };
        }
    }
}

const viewInfoAddedModalTemplate: IStringMap<string> = {
  template: `
    <section class="padding-all-2">
        <div class="info-added__comments">
            <div class="info-added__label">{{::$root.t("InformationRequested")}}</div>
                <blockquote class="blockquote">{{ $ctrl.reviewerComment }}
                    <footer class="blockquote__footer">{{ $ctrl.reviewerName }}</footer>
                </blockquote>
            <div class="info-added__label">{{::$root.t("InformationAdded")}}</div>
            <blockquote class="blockquote">{{ $ctrl.infoAddedComment }}</blockquote>
        </div>
        <div class="info-added__btn">
            <one-button
                button-click="$ctrl.closeModal()"
                ng-if="!$ctrl.viewState.canViewNeedMoreInfoButtonInfoAddedModal && !$ctrl.viewState.canViewAcceptButtonInfoAddedModal"
                text="{{::$root.t('Close')}}">
            </one-button>
            <one-button
                button-class="margin-right-1"
                button-click="$ctrl.needInfo()"
                ng-if="$ctrl.viewState.canViewNeedMoreInfoButtonInfoAddedModal"
                text="{{::$root.t('NeedMoreInfo')}}">
            </one-button>
            <one-button
                button-click="$ctrl.accept()"
                ng-if="$ctrl.viewState.canViewAcceptButtonInfoAddedModal"
                text="{{::$root.t('Accept')}}"
                type="primary">
            </one-button>
        </div>
    </section>
`};

export default {
    controller: ViewInfoAddedModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(viewInfoAddedModalTemplate.template),
};
