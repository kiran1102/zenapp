import { IStringMap } from "interfaces/generic.interface";
import _, { isFunction } from "lodash";
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { IConfirmationModalResolve } from "interfaces/confirmation-modal-resolve.interface";

class ConfirmationModalController implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "ModalService"];
    modalTitle: string;
    confirmationText: string;
    cancelButtonText: string;
    submitButtonText: string;
    iconClass: string;
    // NOTE: use this for iconTemplate if you want the green circle with white check
    // `<span class="fa-stack fa-3x"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-check fa-stack-1x fa-inverse"></i></span>`,
    iconTemplate: string;
    isSubmitting: boolean;
    hideSubmitButton: boolean;

    private resolve: IConfirmationModalResolve;
    private promiseToResolve: () => ng.IPromise<any>;
    private successCallback: () => void;

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
    ) {}

    public $onInit(): void {
        this.promiseToResolve = this.resolve.promiseToResolve;
        this.successCallback = this.resolve.successCallback;
        this.modalTitle =       this.resolve.modalTitle;
        this.confirmationText = this.resolve.confirmationText;
        this.cancelButtonText = this.resolve.cancelButtonText || this.$rootScope.t("Cancel");
        this.submitButtonText = this.resolve.submitButtonText || this.$rootScope.t("Submit");
        this.iconClass = this.resolve.iconClass || "";
        this.iconTemplate = this.resolve.iconTemplate || "";
        this.hideSubmitButton = this.resolve.hideSubmitButton;
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.handleModalCallback({ result: false, data: null });
        this.modalService.clearModalData();
    }

    private handleSubmit(): void {
        if (isFunction(this.promiseToResolve)) {
            this.isSubmitting = true;
            this.promiseToResolve().then((success: boolean) => {
                if (success) {
                    this.modalService.closeModal();
                    this.modalService.handleModalCallback();
                    this.modalService.clearModalData();
                    if (isFunction(this.resolve.successCallback)) {
                        this.resolve.successCallback();
                    }
                }
                this.isSubmitting = false;
            });
        }
    }
}

const confirmationModalTemplate: IStringMap<string> = {
    template: `
        <section class="confirmation-modal padding-all-2">
            <div ng-if="::$ctrl.iconClass" class="row-horizontal-center margin-bottom-2">
                <span class="confirmation-modal__icon">
                    <i class="{{::$ctrl.iconClass}}"></i>
                </span>
            </div>
            <div ng-if="$ctrl.iconTemplate"class="row-horizontal-center margin-bottom-2 text-center">
                <ng-bind-html
                    ng-bind-html="::$ctrl.iconTemplate"
                >
                </ng-bind-html>
                <div class="position-absolute text-bold padding-all-2" ng-bind="$ctrl.resolve.confirmationRowKey"></div>
            </div>
            <div class="margin-top-2 margin-bottom-4 text-center text-bold" ng-bind-html="$ctrl.confirmationText"></div>
            <footer class="row-flex-end margin-top-2">
                <one-button
                    identifier="TemplateConfirmationModalCancelButton"
                    is-rounded="true"
                    button-class="margin-right-1"
                    button-click="$ctrl.closeModal()"
                    text="{{$ctrl.cancelButtonText}}">
                </one-button>
                <one-button
                    identifier="TemplateConfirmationModalSubmitButton"
                    is-rounded="true"
                    ng-if="!$ctrl.hideSubmitButton"
                    button-click="$ctrl.handleSubmit()"
                    enable-loading="true"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="$ctrl.isSubmitting"
                    text="{{$ctrl.submitButtonText}}"
                    type="primary">
                </one-button>
            </footer>
        </section>
    `,
};

export const confirmationModal: ng.IComponentOptions = {
    controller: ConfirmationModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(confirmationModalTemplate.template),
};
