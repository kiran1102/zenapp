import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Rxjs
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    find,
    forIn,
    forEach,
    cloneDeep,
    includes,
    filter,
    isNil,
} from "lodash";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getOrgById } from "oneRedux/reducers/org.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getAttributeMap,
    getInventoryId,
    getStatusMap,
    isSavingRecord,
} from "oneRedux/reducers/inventory.reducer";
import observableFromStore from "oneRedux/utils/observable-from-store";

// Services
import { Permissions } from "modules/shared/services/helper/permissions.service";
import { ModalService } from "sharedServices/modal.service";
import { InventoryAdapter } from "oneServices/adapters/inventory-adapter.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { LookupService } from "sharedModules/services/helper/lookup.service";

// Constants / Enums
import {
    InventoryTranslationKeys,
    InventoryAttributeCodes,
    CopyAttributes,
    UniqueAttributes,
    PrimaryKeyAttributes,
    InventoryPermissions,
    AttributeFieldNames,
} from "constants/inventory.constant";
import {
    InputTypes,
    OptionTypes,
    InventoryTableIds,
} from "enums/inventory.enum";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

// Interfaces
import {
    IInventoryRecord,
    IAttributeValue,
    IInventoryCellValue,
    IInventoryCopyModalResolve,
    IAssessmentStatusMap,
    IInventoryCell,
    IRecordResponse,
} from "interfaces/inventory.interface";
import { IStore, IStoreState } from "interfaces/redux.interface";
import { IAttributeMap } from "interfaces/inventory-reducer.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IStringMap, IValueId } from "interfaces/generic.interface";
import { ISelection } from "interfaces/generic.interface";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "copy-inventory-modal",
    templateUrl: "./copy-inventory-modal.component.html",
})
export class CopyInventoryModalComponent implements OnInit {
    modalData: IInventoryCopyModalResolve;
    isSaving = false;
    sourceRecord: IInventoryRecord;
    inventoryId: number;
    itemNameLabel: string;
    modalHeader: string;
    isReady = false;
    hasErrors = false;
    requiredFieldsPopulated = true;
    copyRelated = true;
    hasLinkingV2: boolean;
    hasValidFields = false;
    copyDisabled = true;
    orgOptions: IInventoryCellValue[];
    locationOptions: IInventoryCellValue[];
    filteredLocationOptions: IInventoryCellValue[];
    filteredTypeOptions: IInventoryCellValue[];
    typeOptions: IInventoryCellValue[];
    selectedName: IInventoryCellValue;
    selectedOrg: IInventoryCellValue;
    selectedLocation: IInventoryCellValue;
    selectedType: IInventoryCellValue;
    prepopulatedName: string;
    prepopulatedOrg: IInventoryCellValue;
    prepopulatedLocation: IInventoryCellValue;
    prepopulatedType: IInventoryCellValue;
    record: IInventoryRecord;
    updatedRecord: IInventoryRecord;
    statusMap: IAssessmentStatusMap;
    fullRecordCells: IInventoryCell[] = [];
    copyAttributes: any = CopyAttributes;
    attributeFieldNames = AttributeFieldNames;
    private attributeMap: IAttributeMap;

    constructor(
        @Inject(StoreToken) public store: IStore,
        private modalService: ModalService,
        private inventoryAdapter: InventoryAdapter,
        private inventoryActionService: InventoryActionService,
        private inventoryService: InventoryService,
        private permissions: Permissions,
        private lookupService: LookupService,
        public translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.hasLinkingV2 = this.permissions.canShow(InventoryPermissions[this.inventoryId].inventoryLinkingV2);
        this.inventoryService.getRecord(this.inventoryId.toString(), this.modalData.recordId).then((response: IProtocolResponse<IRecordResponse>) => {
            if (!response || !response.result) return;
            this.inventoryAdapter.setRecordUsers(response.data).then(() => {
                this.sourceRecord = {
                    id: response.data.id,
                    canEdit: response.data.canEdit,
                    assessmentOutstanding: false,
                    cells: this.inventoryAdapter.getRecordCells(this.attributeMap, this.statusMap, response.data),
                };
                this.updatedRecord = cloneDeep(this.sourceRecord);
                this.setPrepopulatedValues();
                this.updatedRecord = this.removeUniqueAttributeValues(this.updatedRecord);
                this.isReady = true;
                this.copyDisabled = this.canCopyDisabled();
            });
        });
        this.modalHeader = InventoryTranslationKeys[this.inventoryId].copyInventoryItem;
        this.itemNameLabel = InventoryTranslationKeys[this.inventoryId].itemName;
        this.orgOptions = this.inventoryAdapter.formatOptions(this.getOptions(CopyAttributes[this.inventoryId].Org));
    }

    updateRecord(record: IRecordResponse, closeModal: boolean) {
        this.updatedRecord.cells = this.fullRecordCells;
        this.updatedRecord.id = record.id;
        this.updatedRecord = this.formatTextAttributes(this.updatedRecord);
        this.updatedRecord = this.removeUniqueAttributeValues(this.updatedRecord);
        forIn(record.map, (value: IInventoryCellValue[], key: string) => {
            const attribute = find(this.updatedRecord.cells, ["attributeId", key]);
            if (attribute) {
                if (attribute.routeParams && attribute.routeParams.recordId) {
                    attribute.routeParams.recordId = record.id;
                    attribute.displayValue = record.map[key][0].value;
                }
                attribute.values = record.map[key];
            }
        });
        this.saveRecord(this.updatedRecord, closeModal);
    }

    copyRecord(closeModal: boolean = true) {
        this.updatedRecord = this.getPrimaryKeyAttributes(this.updatedRecord);
        if (this.hasLinkingV2) {
            const primaryKeys: IStringMap<string | IValueId<string>> = this.transformPrimaryKeys(this.updatedRecord.cells);
            this.inventoryActionService.isSavingRecord();
            this.inventoryService.copyRecord(NewInventorySchemaIds[this.inventoryId], this.modalData.recordId, primaryKeys, this.copyRelated).then((response: IProtocolResponse<null>) => {
                if (response.result) {
                    this.handleResponse(closeModal);
                } else {
                    this.hasErrors = true;
                    this.copyDisabled = this.canCopyDisabled();
                }
                this.inventoryActionService.isDoneSavingRecord();
            });
        } else {
            this.inventoryActionService.addRecord(this.updatedRecord).then((response: IProtocolResponse<IRecordResponse>) => {
                if (response.data) {
                    this.updateRecord(response.data, closeModal);
                } else {
                    this.hasErrors = true;
                    this.copyDisabled = this.canCopyDisabled();
                }
            });
        }
    }

    formatTextAttributes(record: IInventoryRecord): IInventoryRecord {
        const updatedRecord: IInventoryRecord = cloneDeep(record);
        for (let i = updatedRecord.cells.length - 1; i >= 0; i--) {
            const isTextAttribute: boolean =
                updatedRecord.cells[i].fieldResponseTypeId &&
                updatedRecord.cells[i].fieldResponseTypeId === InputTypes.Text &&
                updatedRecord.cells[i].attributeCode !== CopyAttributes[this.inventoryId].Name &&
                Boolean(updatedRecord.cells[i].values) &&
                Boolean(updatedRecord.cells[i].values.length);

            if (isTextAttribute && updatedRecord.cells[i].values[0].value === "") {
                updatedRecord.cells.splice(i, 1);
            } else if (isTextAttribute) {
                delete updatedRecord.cells[i].values[0].valueId;
            }
        }
        return updatedRecord;
    }

    saveRecord(record: IInventoryRecord, closeModal: boolean) {
        this.inventoryActionService.saveRecord(record, false).then((response: IProtocolResponse<IRecordResponse>) => {
            if (response.result) {
                this.handleResponse(closeModal);
            } else {
                this.hasErrors = true;
                this.copyDisabled = this.canCopyDisabled();
            }
        });
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    getPrimaryKeyAttributes(record: IInventoryRecord): IInventoryRecord {
        if (!this.fullRecordCells.length) {
            this.fullRecordCells = record.cells;
        }
        const updatedRecord = cloneDeep(record);
        updatedRecord.cells = filter(updatedRecord.cells, (cell: IInventoryCell) => {
            return includes(PrimaryKeyAttributes[this.inventoryId], cell.attributeCode);
        });
        return updatedRecord;
    }

    transformPrimaryKeys(attributes: IInventoryCell[]): IStringMap<string | IValueId<string>> {
        const nameAttribute = find(attributes, (cell: IInventoryCell): boolean => {
            return (
                cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Asset ||
                cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Processing_Activity ||
                cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Name
            );
        });

        const orgAttribute = find(attributes, (cell: IInventoryCell): boolean => {
            return (
                cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Managing_Organization ||
                cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Organization
            );
        });

        const transformedPayload: IStringMap<string | IValueId<string>> = {
            name: nameAttribute.values[0].value,
            organization: {
                id: orgAttribute.values[0].valueId,
                value: orgAttribute.values[0].value,
            },
        };

        if (this.inventoryId === InventoryTableIds.Vendors) {
            const typeAttribute = find(attributes, (cell: IInventoryCell): boolean => {
                return cell.attributeCode === InventoryAttributeCodes[this.inventoryId].VendorType;
            });
            transformedPayload.type = {
                id: typeAttribute.values[0].valueId,
                value: typeAttribute.values[0].value,
            };
        }

        if (this.inventoryId === InventoryTableIds.Assets) {
            const locationAttribute = find(attributes, (cell: IInventoryCell): boolean => {
                return cell.attributeCode === InventoryAttributeCodes[this.inventoryId].Hosting_Location;
            });
            transformedPayload.location = {
                id: locationAttribute.values[0].valueId,
                value: locationAttribute.values[0].value,
            };
        }

        return transformedPayload;
    }

    handleResponse(closeModal: boolean) {
        if (this.modalData.callback) this.modalData.callback();
        if (closeModal) {
            this.closeModal();
        } else {
            this.updatedRecord = cloneDeep(this.sourceRecord);
            this.fullRecordCells = [];
            this.selectedName = { value: this.prepopulatedName };
            this.selectedOrg = this.prepopulatedOrg;
            if (this.copyAttributes[this.inventoryId].Location) this.selectedLocation = this.prepopulatedLocation;
            if (this.copyAttributes[this.inventoryId].Type) this.selectedType = this.prepopulatedType;
            this.hasValidFields = this.checkValidFields();
            this.copyDisabled = this.canCopyDisabled();
        }
    }

    removeUniqueAttributeValues(record: IInventoryRecord): IInventoryRecord {
        const updatedRecord = record;
        const isPrepopulatedOrg: boolean = this.selectedOrg === this.prepopulatedOrg;
        const uniqueAttributes = isPrepopulatedOrg ? UniqueAttributes[this.inventoryId].SameOrg : UniqueAttributes[this.inventoryId].DifferentOrg;
        forEach(updatedRecord.cells, (attribute: IInventoryCell) => {
            if (
                includes(uniqueAttributes, attribute.attributeCode) ||
                (attribute.fieldAttributeTypeId === OptionTypes.Users && !isPrepopulatedOrg)
            ) {
                attribute.displayValue = "";
                attribute.values = [{value: null, valueId: null}];
            }
        });
        return updatedRecord;
    }

    orgSelect(event: string) {
        const selection = getOrgById(event)(this.store.getState());
        this.selectedOrg = event && selection ? { value: selection.Name, valueId: selection.Id } : { value: null, valueId: null };
        this.hasErrors = isNil(this.selectedOrg.valueId);
        this.requiredFieldsPopulated = this.checkRequiredFields();
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Org);
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    typeSelect(event: ISelection<IInventoryCellValue>) {
        this.selectedType = event ? event.currentValue : null;
        this.filteredTypeOptions = this.lookupService.filterOptionsBySelections([this.selectedType], this.typeOptions, "valueId");
        this.hasErrors = false;
        this.requiredFieldsPopulated = this.checkRequiredFields();
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Type);
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    locationSelect(event: ISelection<IInventoryCellValue>) {
        this.selectedLocation = event ? event.currentValue : null;
        this.filteredLocationOptions = this.lookupService.filterOptionsBySelections([this.selectedLocation], this.locationOptions, "valueId");
        this.hasErrors = false;
        this.requiredFieldsPopulated = this.checkRequiredFields();
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Location);
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    nameSelect(selection: string) {
        this.selectedName = { value: selection };
        this.hasErrors = false;
        this.requiredFieldsPopulated = this.checkRequiredFields();
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Name);
        this.hasValidFields = this.checkValidFields();
        this.copyDisabled = this.canCopyDisabled();
    }

    handleInputChange(event: any, fieldName: string) {
        if (fieldName === this.attributeFieldNames.Location) {
            this.filteredLocationOptions = this.lookupService.filterOptionsByInput([], this.locationOptions, event.value, "valueId", "value");
        } else if (fieldName === this.attributeFieldNames.Type) {
            this.filteredTypeOptions = this.lookupService.filterOptionsByInput([], this.typeOptions, event.value, "valudId", "value");
        }
    }

    checkRequiredFields(): boolean {
        if (this.copyAttributes[this.inventoryId].Location) {
            return Boolean(this.selectedLocation && this.selectedLocation.value && this.selectedName.value && this.selectedOrg.value);
        } else if (this.copyAttributes[this.inventoryId].Type) {
            return Boolean(this.selectedType && this.selectedType.value && this.selectedName.value && this.selectedOrg.value);
        } else {
            return Boolean(this.selectedName.value && this.selectedOrg.value);
        }
    }

    checkValidFields(): boolean {
        if (this.copyAttributes[this.inventoryId].Location) {
            return !(this.selectedLocation &&  this.selectedLocation.value === this.prepopulatedLocation.value && this.selectedName.value.trim() === this.prepopulatedName && this.selectedOrg.value === this.prepopulatedOrg.value);
        } else if (this.copyAttributes[this.inventoryId].Type) {
            return !(this.selectedType && this.selectedType.value === this.prepopulatedType.value && this.selectedName.value.trim() === this.prepopulatedName && this.selectedOrg.value === this.prepopulatedOrg.value);
        } else {
            return !(this.selectedName.value.trim() === this.prepopulatedName && this.selectedOrg.value === this.prepopulatedOrg.value);
        }
    }

    canCopyDisabled(): boolean {
        return this.isSaving || this.hasErrors || !this.requiredFieldsPopulated || !this.hasValidFields;
    }

    private setPrepopulatedValues() {
        this.prepopulatedName = this.getPrepopulatedName();
        this.prepopulatedOrg = this.getPrepopulatedValue(CopyAttributes[this.inventoryId].Org);
        this.selectedName = { value: this.prepopulatedName };
        this.selectedOrg = this.prepopulatedOrg;
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Name);
        this.setRecordAttribute(CopyAttributes[this.inventoryId].Org);
        if (this.copyAttributes[this.inventoryId].Location) {
            this.prepopulatedLocation = this.getPrepopulatedValue(CopyAttributes[this.inventoryId].Location);
            this.selectedLocation = this.prepopulatedLocation;
            this.setRecordAttribute(CopyAttributes[this.inventoryId].Location);
            this.locationOptions = this.inventoryAdapter.formatOptions(this.getOptions(CopyAttributes[this.inventoryId].Location));
            this.filteredLocationOptions = this.lookupService.filterOptionsBySelections([this.prepopulatedLocation], this.locationOptions, "valueId");
        }
        if (this.copyAttributes[this.inventoryId].Type) {
            this.prepopulatedType = this.getPrepopulatedValue(CopyAttributes[this.inventoryId].Type);
            this.selectedType = this.prepopulatedType;
            this.setRecordAttribute(CopyAttributes[this.inventoryId].Type);
            this.typeOptions = this.inventoryAdapter.formatOptions(this.getOptions(CopyAttributes[this.inventoryId].Type));
            this.filteredTypeOptions = this.lookupService.filterOptionsBySelections([this.prepopulatedType], this.typeOptions, "valueId");
        }
    }

    private getOptions(attributeCode: string): IAttributeValue[] {
        const attribute = find(this.attributeMap, ["fieldAttributeCode", attributeCode]);
        return attribute && attribute.values ? attribute.values : [];
    }

    private setRecordAttribute(attributeCode: string) {
        const attribute = find(this.updatedRecord.cells, ["attributeCode", attributeCode]);
        switch (attributeCode) {
            case CopyAttributes[this.inventoryId].Name:
                attribute.displayValue = this.selectedName.value;
                attribute.values = [this.selectedName];
                break;
            case CopyAttributes[this.inventoryId].Org:
                attribute.displayValue = this.selectedOrg.value;
                attribute.values = [this.selectedOrg];
                break;
            case CopyAttributes[this.inventoryId].Location:
                attribute.displayValue = this.selectedLocation ? this.selectedLocation.value : null;
                attribute.values = [this.selectedLocation];
                break;
            case CopyAttributes[this.inventoryId].Type:
                attribute.displayValue = this.selectedType ? this.selectedType.value : null;
                attribute.values = [this.selectedType];
                break;
            default:
                break;
        }
    }

    private getPrepopulatedName(): string {
        const nameAttribute = find(this.sourceRecord.cells, ["attributeCode", CopyAttributes[this.inventoryId].Name]);
        return nameAttribute && nameAttribute.displayValue ? nameAttribute.displayValue : "";
    }

    private getPrepopulatedValue(attributeCode: string): IInventoryCellValue {
        const attribute = find(this.sourceRecord.cells, ["attributeCode", attributeCode]);
        return attribute && attribute.values ? attribute.values[0] : { value: null, valueId: null };
    }

    private componentWillReceiveState(state: IStoreState) {
        this.attributeMap = getAttributeMap(state);
        this.modalData = getModalData(state);
        this.inventoryId = getInventoryId(state);
        this.isSaving = isSavingRecord(state);
        this.statusMap = getStatusMap(state);
        this.copyDisabled = this.canCopyDisabled();
    }
}
