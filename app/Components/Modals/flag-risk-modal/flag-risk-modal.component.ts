// Rxjs
import {
    Observable,
    Subject,
} from "rxjs";
import {
    map,
    filter,
    startWith,
    withLatestFrom,
    first,
    takeUntil,
} from "rxjs/operators";

// Third Party
import invariant from "invariant";
import {
    concat,
    get,
    find,
} from "lodash";

// Enums
import {
    RiskV2States,
} from "enums/riskV2.enum";

// Types
import { RiskV2UpdateActions } from "modules/assessment/types/risk-v2.types";

// Interfaces
import { IDropdownOption } from "interfaces/one-dropdown.interface";
import { IUser } from "interfaces/user.interface";
import {
    IRiskDetails,
    IRisk,
    IRiskModalPermissionConfig,
    IHeatMapTile,
    IRiskHeatMapLabels,
    IRiskModalViewConfig,
} from "interfaces/risk.interface";
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";

// Models
import { buildDefaultModalTemplate } from "modals/modal-template/modal-template";

// Services
import { ModalService } from "sharedServices/modal.service";
import { RiskActionService } from "oneServices/actions/risk-action.service";
import RiskBusinessLogicService from "oneServices/logic/risk-business-logic.service";
import { SettingActionService } from "settingsModule/services/actions/setting-action.service";

// Redux
import {
    isAnyPropChanged,
    areAllPropsDefined,
} from "oneRedux/utils/utils";
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getTempRiskById } from "oneRedux/reducers/risk.reducer";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { isHeatMapEnabled } from "oneRedux/reducers/setting.reducer";
import { getCurrentUser } from "oneRedux/reducers/user.reducer";

class RiskModalController implements ng.IComponentController {

    static $inject: string[] = [
        "$injector",
        "store",
        "ModalService",
        "RiskBusinessLogic",
        "RiskAction",
        "SettingAction",
    ];

    private resolve: any;
    private close: any;
    private dismiss: any;

    private dataLoaded = false;
    private canSubmit = false;
    private isSubmitting = false;
    private permissions: IRiskModalPermissionConfig;
    private translations: any;

    private minDeadlineDate: Date | null;

    private riskModel: IRiskDetails;
    private initialRiskModel: IRiskDetails;

    private riskOptions: IDropdownOption[];
    private defaultRiskOption: IDropdownOption;
    private selectedRisk: IDropdownOption;

    private axisLabels: IRiskHeatMapLabels;
    private tileMap: IHeatMapTile[][];
    private selectedHeatMap: IHeatMapTile | undefined;

    private selectedOwner: IDropdownOption;
    private userList: IUser[];
    private currentUser: IUser;
    private viewState: IRiskModalViewConfig;
    private riskSubject: any;
    private viewService: any;
    private getViewState: (currentUser: IUser, riskSubject: any, riskModel: IRiskDetails, tempRiskModel: IRiskDetails) => IRiskModalViewConfig;
    private destroy$ = new Subject();

    constructor(
        private $injector: ng.auto.IInjectorService,
        private store: IStore,
        private modalService: ModalService,
        private riskBusinessLogic: RiskBusinessLogicService,
        private riskAction: RiskActionService,
        private settingAction: SettingActionService,
    ) {}

    public $onInit(): void {
        // The injected view logic service must have a method called "calculateRiskModalViewState" that accepts these params
        const viewService: any = this.$injector.get(getModalData(this.store.getState()).viewService);
        this.getViewState = viewService.calculateRiskModalViewState;
        this.currentUser = getCurrentUser(this.store.getState());
        const setRisk: () => ng.IPromise<any> = getModalData(this.store.getState()).setRisk;
        if (setRisk) {
            setRisk().then(() => { this.initializeModal(); });
        } else {
            this.initializeModal();
        }
    }

    public handleTypeaheadChange(option: any): void {
        // we have to send null to backend if we are removing a risk owner.
        let riskOwnerId;
        if (option.IsPlaceholder) {
            riskOwnerId = null;
        } else {
            riskOwnerId = option.Id;
        }
        this.riskAction.updateTempRisk(this.riskModel.id, { riskOwnerId });
    }

    public handleInputChange(payload: any, prop: string): void {
        this.riskAction.updateTempRisk(this.riskModel.id, { [prop]: payload });
    }

    public $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private initializeModal(): void {
        this.riskSubject = getModalData(this.store.getState()).riskSubject;
        const riskId: string = getModalData(this.store.getState()).selectedModelId;
        this.userList = getModalData(this.store.getState()).selectedModelId.userList;

        const riskModel: IRiskDetails = getTempRiskById(riskId)(this.store.getState()) as IRiskDetails;

        const minDeadlineDate: Date = new Date();
        minDeadlineDate.setDate(minDeadlineDate.getDate() + getModalData(this.store.getState()).reminderDays + 1); // get reminderdays from setting state
        this.minDeadlineDate = minDeadlineDate;

        this.settingAction.fetchRiskSettings().then((): void => {
            this.translations = this.riskBusinessLogic.getRiskModalTranslations(riskId, false);
            this.prepareRiskLevelOptionsForTemplateUse();
            const store$: Observable<IStoreState> = observableFromStore(this.store).pipe(
                startWith(this.store.getState()));

            const riskModel$: Observable<IRiskDetails | IRisk> = store$.pipe(
                map(getTempRiskById(riskId)),
                filter((data: IRiskDetails | undefined): boolean => Boolean(data)),
            );

            const origRiskModel$: Observable<IRiskDetails | IRisk> = riskModel$.pipe(first());

            riskModel$.pipe(
                withLatestFrom(origRiskModel$),
                takeUntil(this.destroy$),
            ).subscribe(([newModel, oldModel]): void => this.componentWillReceiveState(newModel as IRiskDetails, oldModel as IRiskDetails));
            this.dataLoaded = true;
        });
    }

    private componentWillReceiveState(newModel: IRiskDetails, oldModel: IRiskDetails): void {
        oldModel.recommendation = oldModel.recommendation ? oldModel.recommendation : "";
        if (!this.initialRiskModel) this.initialRiskModel = oldModel;
        this.viewState = this.getViewState(this.currentUser, this.riskSubject, oldModel, newModel);

        this.findAndSetMatchingRiskLevel(newModel);

        this.selectedOwner = this.findAndSetMatchingRiskOwner(newModel);

        this.canSubmit = this.validateRiskModal(oldModel, newModel);

        this.riskModel = newModel;
    }

    private prepareRiskLevelOptionsForTemplateUse(): void {
        this.riskBusinessLogic.getRiskLevelOptions()
            .pipe(takeUntil(this.destroy$))
            .subscribe((riskOptions: IDropdownOption[]) => {
                this.defaultRiskOption = riskOptions[0];
                this.riskOptions = riskOptions.slice(1);
                if (this.riskModel) {
                    this.findAndSetMatchingRiskLevel(this.riskModel);
                }
            });

        const { axisLabels, tileMap }: { axisLabels: IRiskHeatMapLabels, tileMap: IHeatMapTile[][] } = this.riskBusinessLogic.buildRiskTileMap(4, 4);
        this.axisLabels = axisLabels;
        this.tileMap = tileMap;
    }

    private findAndSetMatchingRiskLevel(newModel: IRiskDetails): void {
        if (this.viewState && this.viewState.riskHeatmapEnabled) {
            const pathToModel = `[${this.tileMap.length - newModel.impactLevelId}][${newModel.probabilityLevelId - 1}]`;
            this.selectedHeatMap = get(this.tileMap, pathToModel);
        } else {
            this.selectedRisk = find(this.riskOptions, (option: IDropdownOption): boolean => option.value === newModel.levelId);
        }
    }

    private findAndSetMatchingRiskOwner(newModel): IUser {
        return find(this.userList, (user: IUser): boolean => user.Id === newModel.RiskOwnerId);
    }

    private riskLevelSelected(option: number | IDropdownOption): void {
        if (this.viewState.riskHeatmapEnabled) {
            const keyValueToMerge = {
                probabilityLevel: (option as IDropdownOption).xValue,
                impactLevel: (option as IDropdownOption).yValue,
                level: (option as IDropdownOption).riskLevel,
            };
            this.riskAction.updateTempRisk(this.riskModel.id, keyValueToMerge);
        } else {
            this.riskAction.updateTempRisk(this.riskModel.id, { levelId: option as number });
        }
    }

    private validateRiskModal(oldRiskModel: IRiskDetails, newRiskModel: IRiskDetails): boolean {
        const heatMapEnabled: boolean = isHeatMapEnabled(this.store.getState());
        const riskModel: IRiskDetails = getTempRiskById(newRiskModel.id)(this.store.getState()) as IRiskDetails;
        invariant(heatMapEnabled !== undefined, "HeatMap Setting cannot be undefined when validating.");
        invariant(Boolean(riskModel), "Risk Model cannot be undefined when validating.");

        let propsThatMustBeDefined: string[] = ["description", "levelId"];
        let propsThatCanChange: string[] = [...propsThatMustBeDefined, "deadline", "riskOwnerId"];

        if (this.viewState.riskHeatmapEnabled) {
            // commented out due to risks that were created before heatmap enable and that don't have impact and probability
            // propsThatMustBeDefined = concat(propsThatMustBeDefined, ["impactLevel", "probabilityLevel"]);
            propsThatCanChange = concat(propsThatCanChange, ["impactLevel", "probabilityLevel"]);
        }
        switch (riskModel.state) {
            case RiskV2States.IDENTIFIED:
                propsThatCanChange = concat(propsThatCanChange, "recommendation");
                break;
            case RiskV2States.RECOMMENDATION_ADDED:
                propsThatCanChange = concat(propsThatCanChange, ["recommendation", "mitigation", "requestedException"]);
                break;
            case RiskV2States.REMEDIATION_PROPOSED:
                propsThatMustBeDefined = concat(propsThatMustBeDefined, "mitigation");
                propsThatCanChange = concat(propsThatCanChange, "mitigation");
                break;
            case RiskV2States.EXCEPTION_REQUESTED:
                propsThatMustBeDefined = concat(propsThatMustBeDefined, "requestedException");
                propsThatCanChange = concat(propsThatCanChange, "requestedException");
                break;
        }

        const allPropsDefined: boolean = areAllPropsDefined(propsThatMustBeDefined)(oldRiskModel, newRiskModel);
        const anyPropChanged: boolean = isAnyPropChanged(propsThatCanChange)(oldRiskModel, newRiskModel);

        return allPropsDefined && anyPropChanged;
    }

    private closeModal(): void {
        this.riskAction.deleteTempRisk(this.riskModel.id);
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private getUpdatedRiskModel({ reminderDays }: { reminderDays: number }): IRiskDetails {
        const updatedRiskModel: IRiskDetails = { ...this.riskModel, reminderDays };
        if (this.initialRiskModel.recommendation !== this.riskModel.recommendation) {
            if (this.riskModel.recommendation.trim() === "") {
                updatedRiskModel.action = RiskV2States.RECOMMENDATION_REMOVED as RiskV2UpdateActions;
            } else {
                updatedRiskModel.action = RiskV2States.RECOMMENDATION_ADDED as RiskV2UpdateActions;
            }
        }
        return updatedRiskModel;
    }

    private submitModal(): void {
        const saveRisk: (id: string, risk: IRiskDetails) => ng.IPromise<IRiskDetails> = getModalData(this.store.getState()).saveRisk;
        const { reminderDays }: { reminderDays: number } = getModalData(this.store.getState());
        this.riskModel.orgGroupId = getModalData(this.store.getState()).inventoryOrgId;
        const updatedRiskModel = this.getUpdatedRiskModel({reminderDays});
        this.isSubmitting = true;
        saveRisk(this.riskModel.id, updatedRiskModel).then((newRisk: IRiskDetails): void => {
            this.modalService.handleModalCallback(newRisk);
            this.modalService.closeModal();
            this.modalService.clearModalData();
        });
    }
}

const riskModalTemplate: any = {
    template: `
    <loading ng-if="!$ctrl.dataLoaded" show-text="false" style="height: 300px;"></loading>
    <section ng-if="$ctrl.dataLoaded" class="stretch-vertical overflow-y-auto padding-all-2 risk-modal">
        <one-label-content
            class="flex-grow-1"
            ng-if="$ctrl.viewState.riskHeatmapShown || $ctrl.viewState.riskHeatmapEnabled"
            name="{{$ctrl.translations.riskLevel}}"
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-risk-tile-button
                axis-labels="$ctrl.axisLabels"
                tile-map="$ctrl.tileMap"
                selected-model="$ctrl.selectedHeatMap"
                on-change="$ctrl.riskLevelSelected(value)"
                is-disabled="!$ctrl.viewState.riskHeatmapEnabled"
            ></one-risk-tile-button>
        </one-label-content>

        <one-label-content
            class="flex-grow-1 risk-modal__input--half-width"
            ng-if="$ctrl.viewState.riskLevelShown || $ctrl.viewState.riskLevelEnabled"
            name="{{$ctrl.translations.riskLevel}}"
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2 risk-modal__label--half-width"
        >
            <reactive-dropdown
                button-class="risk-modal__dropdown-button width-100-percent text-left"
                dropdown-class="risk-modal__dropdown"
                left-icon-class="{{$ctrl.selectedRisk ? $ctrl.selectedRisk.icon : $ctrl.defaultRiskOption.icon}}"
                right-icon-class="risk-modal__dropdown-arrow"
                item-class="risk-modal__dropdown-item"
                list-class="width-100-percent"
                text="{{$ctrl.translations.riskLevel}}"
                options="$ctrl.riskOptions"
                default-option="$ctrl.defaultRiskOption"
                selected-model="$ctrl.selectedRisk"
                on-change="$ctrl.riskLevelSelected(value)"
                is-disabled="!$ctrl.viewState.riskLevelEnabled"
            ></reactive-dropdown>
        </one-label-content>

        <div class="flex-full-width row-space-between">
            <one-label-content
                class="flex-grow-1 flex-basis-0 margin-right-2"
                ng-if="$ctrl.viewState.riskDeadlineShown || $ctrl.viewState.riskDeadlineEnabled"
                name="{{$ctrl.translations.deadline}}"
                label-class="risk-modal__label"
                wrapper-class="margin-bottom-2"
            >
                <one-datepicker
                    date-model="$ctrl.riskModel.deadline"
                    placeholder="{{::$ctrl.translations.deadline}}"
                    on-change="$ctrl.handleInputChange(value, 'deadline')"
                    is-disabled="!$ctrl.viewState.riskDeadlineEnabled"
                ></one-datepicker>
            </one-label-content>

            <one-label-content
                class="flex-grow-1 flex-basis-0"
                ng-if="$ctrl.viewState.riskOwnerShown || $ctrl.viewState.riskOwnerEnabled"
                name="{{$ctrl.translations.owner}}"
                is-required="false"
                label-class="risk-modal__label"
                wrapper-class="margin-bottom-2"
            >
                <typeahead
                    class="risk-modal__typeahead"
                    options="$ctrl.userList"
                    model="$ctrl.selectedOwner"
                    on-select="$ctrl.handleTypeaheadChange(selection)"
                    label-key="FullName"
                    placeholder-text="$ctrl.translations.ownerInputPlaceholder"
                    allow-other="false"
                    other-option-prepend="{{::$root.t('ReAssignTo')}}"
                    disable-if-no-options="false"
                    is-disabled="!$ctrl.viewState.riskOwnerEnabled"
                    allow-empty="true"
                    empty-text="{{::$root.t('NoRiskOwner')}}"
                    input-limit="2"
                    use-virtual-scroll="true"
                    options-limit="1000"
                ></typeahead>
            </one-label-content>
        </div>

        <one-label-content
            ng-if="$ctrl.viewState.riskSummaryShown || $ctrl.viewState.riskSummaryEnabled"
            name="{{$ctrl.translations.description}}"
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-1"
        >
            <one-textarea
                input-text="{{$ctrl.riskModel.description}}"
                input-changed="$ctrl.handleInputChange(value, 'description')"
                is-disabled="!$ctrl.viewState.riskSummaryEnabled"
                max-length="1000"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.description.length || 0}} / 1000</div>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskRecommendationShown || $ctrl.viewState.riskRecommendationEnabled"
            name="{{$ctrl.translations.recommendation}}"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text="{{$ctrl.riskModel.recommendation}}"
                input-changed="$ctrl.handleInputChange(value, 'recommendation')"
                is-disabled="!$ctrl.viewState.riskRecommendationEnabled"
                max-length="1000"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.recommendation.length || 0}} / 1000</div>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskRemediationShown || $ctrl.viewState.riskRemediationEnabled"
            name="{{$ctrl.translations.mitigation}}"
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text="{{$ctrl.riskModel.mitigation}}"
                input-changed="$ctrl.handleInputChange(value, 'mitigation')"
                is-disabled="!$ctrl.viewState.riskRemediationEnabled"
                max-length="1000"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.mitigation.length || 0}} / 1000</div>
        </one-label-content>

        <one-label-content
            ng-if="$ctrl.viewState.riskExceptionShown || $ctrl.viewState.riskExceptionEnabled"
            name="{{$ctrl.translations.requestedException}}"
            is-required="true"
            label-class="risk-modal__label"
            wrapper-class="margin-bottom-2"
        >
            <one-textarea
                input-text="{{$ctrl.riskModel.requestedException}}"
                input-changed="$ctrl.handleInputChange(value, 'requestedException')"
                is-disabled="!$ctrl.viewState.riskExceptionEnabled"
                max-length="1000"
            ></one-textarea>
            <div class="row-flex-end">{{$ctrl.riskModel.requestedException.length || 0}} / 1000</div>
        </one-label-content>
    </section>
    <footer class="static-vertical row-flex-end padding-top-bottom-2 margin-left-right-2 border-top">
        <one-button
            button-class="margin-right-2"
            button-click="$ctrl.closeModal()"
            text="{{::$ctrl.translations.cancel}}"
        ></one-button>
        <one-button
            button-click="$ctrl.submitModal()"
            enable-loading="true"
            is-loading="$ctrl.isSubmitting"
            is-disabled="!$ctrl.canSubmit || $ctrl.isSubmitting"
            text="{{::$ctrl.translations.submit}}"
            type="primary"
        ></one-button>
    </footer>
`};

export const flagRiskModal: ng.IComponentOptions = {
    controller: RiskModalController,
    bindings: {
        resolve: "<",
        close: "&",
        dismiss: "&",
    },
    template: buildDefaultModalTemplate(riskModalTemplate.template),
};
