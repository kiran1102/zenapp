// 3rd Party
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import { getModalData } from "oneRedux/reducers/modal.reducer";
import { getInventoryId } from "oneRedux/reducers/inventory.reducer";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryService } from "inventorySharedModule/shared/services/inventory-api.service";
import { InventoryLinkingService } from "modules/inventory/services/adapter/inventory-linking.service";

// Interfaces
import { IStore } from "interfaces/redux.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";
import {
    IRelatedListOption,
    ILinkedInventoryRow,
    IInventoryListItem,
} from "interfaces/inventory.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";

// Const and Enums
import { InventoryTranslationKeys } from "constants/inventory.constant";
import { NewInventorySchemaIds } from "constants/inventory-config.constant";

@Component({
    selector: "link-inventory-modal",
    templateUrl: "./link-inventory-modal.component.html",
})
export class LinkInventoryModalComponent implements OnInit {
    modalData: IRelateInventoryModalResolve;
    modalTitle: string;
    submitButtonText: string;
    cancelButtonText: string;
    recordId: string;
    isLoadingInventory = true;
    isSaving = false;
    requiredFieldsPopulated = false;
    selectedOption: IRelatedListOption = null;
    howIsInventoryRelatedText: string;
    chooseInventoryItemText: string;
    inventoryList: IRelatedListOption[] = [];
    inventoryListLookUp: IRelatedListOption[] = [];
    relationshipOptions: IInventoryListItem[] = [];
    linkedOptions: IStringMap<boolean> = {};
    orgGroupId: string;
    showDefaultList = true;
    private inventoryType: number;
    private recordInventoryType: number;

    constructor(
        @Inject(StoreToken) public store: IStore,
        public modalService: ModalService,
        private inventoryLinkingService: InventoryLinkingService,
        private inventoryService: InventoryService,
        private translatePipe: TranslatePipe,
    ) {}

    ngOnInit() {
        const state = this.store.getState();
        this.modalData = getModalData(state);
        this.recordInventoryType = getInventoryId(state);
        this.inventoryType = this.modalData.inventoryType;
        this.recordId = this.modalData.recordId;
        this.orgGroupId = this.modalData.orgGroupId;
        this.modalTitle = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].addRelatedInventoryHeader) || "";
        this.howIsInventoryRelatedText = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].howIsInventoryRelatedText) || "";
        this.chooseInventoryItemText = this.translatePipe.transform(InventoryTranslationKeys[this.inventoryType].chooseInventoryItem) || "";
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("AddRelated");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.getListData();
    }

    onInputChange({key, value}) {
        if (key === "Enter") return;
        this.showDefaultList = false;
        this.filterList(this.inventoryList, value);
    }

    onModelChange({ currentValue }) {
        if (!currentValue) this.showDefaultList = true;
        this.selectedOption = currentValue;
        this.requiredFieldsPopulated = this.hasFieldsPopulated();
    }

    linkTypeSelected(): boolean {
        return Object.values(this.linkedOptions).includes(true) || !this.relationshipOptions.length;
    }

    saveInventoryLink(closeModal: boolean = true) {
        this.isSaving = true;
        this.inventoryService.addInventoryLink(
            this.recordId,
            this.formatLinkData(),
        ).then((response: IProtocolResponse<ILinkedInventoryRow[]>) => {
            if (response.result) {
                if (this.modalData.callback) {
                    this.modalData.callback();
                }
                if (closeModal) {
                    this.closeModal();
                } else {
                    this.defaultForm();
                    this.requiredFieldsPopulated = this.hasFieldsPopulated();
                }
            }
            this.isSaving = false;
        });
    }

    checkboxChange(option: string) {
        this.linkedOptions[option] = !this.linkedOptions[option];
        this.requiredFieldsPopulated = this.hasFieldsPopulated();
    }

    closeModal() {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    trackByFn(index: number): number {
        return index;
    }

    private filterList(options: IRelatedListOption[], stringSearch: string) {
        this.inventoryListLookUp = options.filter((option: IRelatedListOption): boolean => (option.label.search(new RegExp(stringSearch, "i")) !== -1));
    }

    private hasFieldsPopulated(): boolean {
        return Boolean(this.selectedOption) && this.linkTypeSelected();
    }

    private defaultForm() {
        this.inventoryListLookUp = this.inventoryList;
        this.selectedOption = null;
        Object.keys(this.linkedOptions).forEach((key: string) => {
            this.linkedOptions[key] = false;
        });
    }

    private getRelationshipOptions(linkTypes: IInventoryListItem[]) {
        this.relationshipOptions = [];
        linkTypes.forEach((option: IInventoryListItem) => {
            this.relationshipOptions.push(option);
            this.linkedOptions[option.nameKey] = false;
        });
    }

    private getListData() {
        const recordInventoryType = typeof this.recordInventoryType === "number" ? NewInventorySchemaIds[this.recordInventoryType] : this.recordInventoryType;
        const inventoryType = typeof this.inventoryType === "number" ? NewInventorySchemaIds[this.inventoryType] : this.inventoryType;
        const promises: Array<ng.IPromise<IRelatedListOption[] | IInventoryListItem[]>> = [
            this.inventoryLinkingService.getRelatedInventoryList(this.inventoryType, this.orgGroupId),
            this.inventoryLinkingService.getLinkAssociationType(recordInventoryType, inventoryType),
        ];

        Promise.all(promises).then((responses: [IRelatedListOption[], IInventoryListItem[]]) => {
            if (responses[0] && responses[1]) {
                this.inventoryList = this.formatInventoryList(responses[0]).filter((option: IRelatedListOption): boolean => this.recordId !== option.id);
                this.getRelationshipOptions(responses[1]);
            }
            this.isLoadingInventory = false;
        });
    }

    private formatLinkData(): Array<IStringMap<string>> {
        const formattedData: Array<IStringMap<string>> = [];
        if (this.relationshipOptions.length) {
            Object.keys(this.linkedOptions).forEach((key: string) => {
                if (this.linkedOptions[key]) {
                    formattedData.push({
                        id: this.selectedOption.id,
                        relation: key,
                    });
                }
            });
        } else {
            formattedData.push({
                id: this.selectedOption.id,
            });
        }
        return formattedData;
    }

    private formatInventoryList(options: IRelatedListOption[]): IRelatedListOption[] {
        return options.map((option: IRelatedListOption): IRelatedListOption => {
            option.label = `${option.name} - ${option.organization.value}`;
            if (option.location) option.label = `${option.label} - ${option.location.value}`;
            if (option.type) option.label = `${option.label} - ${option.type.value}`;
            if (option.primaryOperatingLocation) option.label = `${option.label} - ${option.primaryOperatingLocation.value}`;
            return option;
        });
    }
}
