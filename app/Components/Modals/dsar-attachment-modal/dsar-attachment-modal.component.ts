import _, { isFunction } from "lodash";
import { buildDefaultModalTemplate } from "./../modal-template/modal-template";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { ModalService } from "sharedServices/modal.service";
import { IAttachmentFile, IAttachmentFileResponse } from "interfaces/attachment-file.interface";
import { IDSARAttachmentModalResolve } from "interfaces/dsar-attachment-modal-resovle.interface";
import { IProtocolPacket } from "interfaces/protocol.interface";
import { RequestQueueService } from "dsarservice/request-queue.service";

class DSARAttachmentModalController implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "ModalService", "requestQueueServiceDowngraded", "$q"];

    private resolve: IDSARAttachmentModalResolve;
    private modalTitle: string;
    private cancelButtonText: string;
    private submitButtonText: string;
    private isSubmitting: boolean;
    private isInternal: boolean;
    private requestId: string;

    private acceptedFileTypes = ".csv, .doc, .docx, .zip, .jpg, .jpeg, .mpp, .msg, .pdf, .png, .ppt, .pptx, .txt, .vsd, .vsdx, .xls, .xlsx";
    private attachments: IAttachmentFile[];
    private promiseArr: Array<ng.IPromise<IProtocolPacket>>;
    private attachmentResponse: IAttachmentFileResponse[];

    constructor(
        private readonly $rootScope: IExtendedRootScopeService,
        private readonly modalService: ModalService,
        private readonly requestQueueService: RequestQueueService,
        readonly $q: ng.IQService,
    ) {
        this.modalTitle = $rootScope.t("UploadAttachments");
        this.cancelButtonText = $rootScope.t("Cancel");
        this.submitButtonText = $rootScope.t("Upload");
    }

    public $onInit(): void {
        this.isInternal = this.resolve.isInternal;
        this.requestId = this.resolve.requestId;
    }

    private closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    private handleSubmit(): void {
        this.isSubmitting = true;
        this.promiseArr = [];
        _.forEach(this.attachments, (file: IAttachmentFile) => {
            const fileData: IAttachmentFile = {
                FileName: `${file.FileName}.${file.Extension}`,
                Extension: file.Extension,
                Blob: file.Blob,
                IsInternal: this.isInternal,
                Type: 30,
                RequestId: this.requestId,
                Name: file.FileName,
            };
            this.promiseArr.push(this.requestQueueService.addAttachment(fileData));
        });
        this.$q.all(this.promiseArr).then((responses: IProtocolPacket[]): any => {
            this.attachmentResponse = [];
            if (_.isArray(responses) && responses.length) {
                const failedResponses: IProtocolPacket[] = _.filter(responses, {
                    result: false,
                });
                if (!failedResponses.length) {
                    _.forEach(responses, (res: IProtocolPacket): void => {
                        const attachmentObjectForComment: IAttachmentFileResponse = {
                            Id: res.data.Id,
                            FileName: res.data.FileName,
                        };
                        this.attachmentResponse.push(attachmentObjectForComment);
                    });
                    this.modalService.handleModalCallback({ result: true, data: this.attachmentResponse });
                    this.closeModal();
                } else {
                    this.modalService.handleModalCallback({ result: false, data: this.attachmentResponse });
                }
            }
            this.isSubmitting = false;
        });
    }

    private uploadFiles(files: IAttachmentFile[]) {
        if (_.isArray(files) && files.length) {
           this.attachments = files;
        }
    }
}

const dsarAttachmentModalTemplate: any = {
    template: `
        <section class="confirmation-modal padding-all-2">
            <section>
                <file-input
                    class="margin-top-2"
                    allow-multiple="true"
                    accept="{{$ctrl.acceptedFileTypes}}"
                    done="$ctrl.uploadFiles(files)"
                    name="Data"
                    show-file-name="false"
                    select-text="::$root.t('UploadAttachments')"
                    >
                </file-input>
            </section>
            <footer class="row-flex-end margin-top-2">
                <one-button
                    button-class="margin-right-1"
                    button-click="$ctrl.closeModal()"
                    text="{{$ctrl.cancelButtonText}}">
                </one-button>
                <one-button
                    button-click="$ctrl.handleSubmit()"
                    enable-loading="true"
                    is-loading="$ctrl.isSubmitting"
                    is-disabled="$ctrl.isSubmitting || !$ctrl.attachments.length"
                    text="{{$ctrl.submitButtonText}}"
                    type="primary">
                </one-button>
            </footer>
        </section>
    `,
};

export default {
    controller: DSARAttachmentModalController,
    bindings: {
        resolve: "<?",
    },
    template: buildDefaultModalTemplate(dsarAttachmentModalTemplate.template),
};
