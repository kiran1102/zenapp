// Rxjs
import { startWith } from "rxjs/operators";

// 3rd Party
import {
    forEach,
    findIndex,
    uniqBy,
    isUndefined,
    some,
} from "lodash";
import {
    Component,
    Inject,
    OnInit,
} from "@angular/core";

// Tokens
import { StoreToken } from "tokens/redux-store.token";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getModalData } from "oneRedux/reducers/modal.reducer";
import {
    getCurrentRecord,
    getInventoryId,
} from "oneRedux/reducers/inventory.reducer";

// Interfaces
import {
    IStore,
    IStoreState,
} from "interfaces/redux.interface";
import {
    IInventoryCellValue,
    IDataCategory,
    IInventoryRecord,
} from "interfaces/inventory.interface";
import { IStringMap } from "interfaces/generic.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IRelateInventoryModalResolve } from "interfaces/relate-inventory-modal.interface";

// Services
import { ModalService } from "sharedServices/modal.service";
import { InventoryActionService } from "inventoryModule/services/action/inventory-action.service";
import { InventoryRelatedService } from "modules/inventory/services/adapter/inventory-related.service";

// Const and Enums
import { InventoryTableIds } from "enums/inventory.enum";
import { InventoryAttributeCodes } from "constants/inventory.constant";

// Pipes
import { TranslatePipe } from "pipes/translate.pipe";

@Component({
    selector: "relate-data-subject-modal",
    templateUrl: "./relate-data-subject-modal.component.html",
})
export class RelateDataSubjectModalComponent implements OnInit {
    modalTitle: string;
    submitButtonText: string;
    cancelButtonText: string;
    selectedSubjectType: IInventoryCellValue;
    selectedCategory: IDataCategory;
    currentRecordElements: IInventoryCellValue[];
    elementList: IInventoryCellValue[] = [];
    currentElements: IInventoryCellValue[];
    elementMap: IStringMap<boolean> = {};
    isSaving = false;
    allSelected = false;
    dataSubjectTypeList: IInventoryCellValue[] = [];
    dataCategoryList: IDataCategory[] = [];
    recordInventoryType: number;
    orgGroupId: string;
    hasCategories = true;
    fetchingCategories = false;
    private modalData: IRelateInventoryModalResolve;
    private record: IInventoryRecord;

    constructor(
        @Inject(StoreToken) public store: IStore,
        public modalService: ModalService,
        private inventoryRelatedService: InventoryRelatedService,
        private inventoryActionService: InventoryActionService,
        private translatePipe: TranslatePipe,
    ) { }

    ngOnInit(): void {
        this.defaultCategory();
        observableFromStore(this.store).pipe(
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState) => this.componentWillReceiveState(state));
        this.modalTitle = this.modalData.modalTitle || this.translatePipe.transform("ManageDataElements");
        this.submitButtonText = this.modalData.submitButtonText || this.translatePipe.transform("Update");
        this.cancelButtonText = this.modalData.cancelButtonText || this.translatePipe.transform("Cancel");
        this.orgGroupId = this.modalData.orgGroupId;
        this.getCurrentDataElements();
        this.getDataSubjectTypeList();
    }

    getDataSubjectTypeList(): void {
        this.inventoryRelatedService.getDataSubjectList(this.orgGroupId).then((response: IInventoryCellValue[]) => {
            this.dataSubjectTypeList = this.setTranslatedValues(response);
            if (this.dataSubjectTypeList[0]) {
                this.selectedSubjectType = this.dataSubjectTypeList[0];
                this.getDataCategoryList(this.selectedSubjectType.valueId);
            }
        });
    }

    getDataCategoryList(subjectId: string): void {
        this.fetchingCategories = true;
        this.inventoryRelatedService.getDataCategoryList(subjectId, this.orgGroupId).then((response: IDataCategory[]): void => {
            this.dataCategoryList = response;
            this.setElementMapForCategory(this.dataCategoryList);
            if (this.dataCategoryList && this.dataCategoryList[0]) {
                this.hasCategories = true;
                this.selectCategory(this.dataCategoryList[0]);
            } else {
                this.hasCategories = false;
            }
            this.fetchingCategories = false;
        });
    }

    selectCategory(category: IDataCategory): void {
        this.selectedCategory = category;
        this.currentElements = category.elements;
        this.allElementsSelected();
    }

    setElementMapForCategory(categoryList: IDataCategory[]): void {
        forEach(categoryList, (category: IDataCategory): void => {
            this.elementList = uniqBy([...this.elementList, ...category.elements], "valueId");
            forEach(category.elements, (element: IInventoryCellValue): void => {
                if (isUndefined(this.elementMap[element.valueId])) {
                    this.elementMap[element.valueId] = some(this.currentRecordElements, element);
                }
            });
        });
    }

    selectSubjectType(selection: IInventoryCellValue): void {
        this.defaultCategory();
        this.selectedSubjectType = selection;
        this.getDataCategoryList(this.selectedSubjectType.valueId);
    }

    allElementsSelected(): void {
        let allCurrentElementsSelected = true;
        for (let i = 0; i < this.currentElements.length; i++) {
            if (!this.elementMap[this.currentElements[i].valueId]) {
                allCurrentElementsSelected = false;
                break;
            }
        }
        this.allSelected = allCurrentElementsSelected;
    }

    selectAll(): void {
        const checkStatus: boolean = !this.allSelected;
        forEach(this.currentElements, (element: IInventoryCellValue): void => {
            this.elementMap[element.valueId] = checkStatus;
        });
        this.allSelected = checkStatus;
    }

    defaultCategory(): void {
        this.dataCategoryList = [];
        this.selectedCategory = {
            categoryId: "",
            categoryValue: "",
            elements: [],
        };
    }

    toggleElement(elementId: string): void {
        this.elementMap[elementId] = !this.elementMap[elementId];
        this.allElementsSelected();
    }

    closeModal(): void {
        this.modalService.closeModal();
        this.modalService.clearModalData();
    }

    saveInventory(): void {
        this.isSaving = true;
        const updatedRecord = this.formatRecordForSave();
        this.inventoryActionService.saveRecord(updatedRecord).then((response: IProtocolResponse<IInventoryRecord>): void => {
            if (response.result) {
                if (this.modalData.callback) {
                    this.modalData.callback();
                }
                this.closeModal();
            }
            this.isSaving = false;
        });
    }

    getCurrentDataElements() {
        const attributeIndex = findIndex(this.record.cells, ["attributeCode", InventoryAttributeCodes[InventoryTableIds.Processes].Data_Elements]);
        this.currentRecordElements = this.record.cells[attributeIndex].values || [];
    }

    private setTranslatedValues(list: IInventoryCellValue[]) {
        list.map((item) => {
            item.valueKey = item.valueKey ? this.translatePipe.transform(item.valueKey) : item.value;
        });
        return list;
    }

    private formatRecordForSave(): IInventoryRecord {
        const updatedRecord = this.record;
        const attributeIndex = findIndex(updatedRecord.cells, ["attributeCode", InventoryAttributeCodes[InventoryTableIds.Processes].Data_Elements]);
        const options = this.formatOptionsForSave();
        updatedRecord.cells[attributeIndex].values = uniqBy([...this.currentRecordElements, ...options], "valueId");
        if (!updatedRecord.cells[attributeIndex].values.length) updatedRecord.cells[attributeIndex].values = [{value: null, valueId: null}];
        return updatedRecord;
    }

    private formatOptionsForSave(): IInventoryCellValue[] {
        const options: IInventoryCellValue[] = [];
        forEach(this.elementList, (element: IInventoryCellValue) => {
            const valueIndex: number = findIndex(this.currentRecordElements, element);
            if (this.elementMap[element.valueId]) {
                options.push(element);
            } else if (valueIndex !== -1) {
                this.currentRecordElements.splice(valueIndex, 1);
            }
        });
        return options;
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.modalData = getModalData(state);
        this.record = getCurrentRecord(state);
        this.recordInventoryType = getInventoryId(state);
    }
}
