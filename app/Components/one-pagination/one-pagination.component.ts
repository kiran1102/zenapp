import { IPageable } from "interfaces/pagination.interface";
import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class OnePagination implements ng.IComponentController {

    static $inject: string[] = ["$rootScope", "$scope"];

    private list: IPageable;
    private hidePaginationNumber: boolean;
    private page: number;
    private onPageChange: (pageObj: {page: number}) => any;

    constructor(readonly $rootScope: IExtendedRootScopeService, readonly $scope: any) {}

    public $onChanges(changes: ng.IOnChangesObject): void {
        this.page = this.list.number + 1;
    }

    public handlePageChange(): void {
        if (_.isFunction(this.onPageChange)) {
            this.onPageChange({page: this.page - 1});
        } else {
            this.$scope.$emit("CHANGE_PAGE", this.page - 1);
        }
    }

    private getPageRange(): string {
        if (!this.list || !this.list.totalElements) return "0";
        const firstNum = this.list.first ? 1 : this.list.number * this.list.size + 1;
        const secondNum = this.list.last ? this.list.totalElements : firstNum + this.list.size - 1;
        return `${firstNum} - ${secondNum}`;
    }
}

const paginationComponent: ng.IComponentOptions = {
    bindings: {
        list: "<",
        hidePaginationNumber: "<?",
        paginationClass: "@?",
        hideCount: "<?",
        onPageChange: "&?",
        identifier: "@?",
    },
    controller: OnePagination,
    template: `
        <div class="one-pagination row-wrap {{::$ctrl.paginationClass}}">
            <ul
                class="one-pagination__list pagination-sm"
                ot-auto-id="{{$ctrl.identifier || 'OnePaginationList'}}"
                uib-pagination
                ng-model="$ctrl.page"
                next-text=">"
                previous-text="<"
                max-size="4"
                force-ellipses="true"
                boundary-link-numbers="true"
                rotate="true"
                total-items="$ctrl.list.totalElements"
                items-per-page="$ctrl.list.size"
                ng-change="$ctrl.handlePageChange()"
                ng-if="!$ctrl.hidePaginationNumber"
                >
            </ul>
            <p
                class="one-pagination__count"
                ng-if="!$ctrl.hideCount"
                >
                {{ $ctrl.$rootScope.t("ShowingPageRangeOfTotalItemCount", {PageRange: $ctrl.getPageRange(), TotalItemCount: $ctrl.list.totalElements}) }}
            </p>
        </div>
    `,
};

export default paginationComponent;
