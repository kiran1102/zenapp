import { OrgGroupApiService } from "oneServices/api/org-group-api.service.ts";
import { OrgUserDropdownService } from "generalcomponent/org-user-dropdown/org-user-dropdown.service";
import Utilities from "Utilities";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IProtocolResponse } from "interfaces/protocol.interface";
import { IOrgUserAdapted, IUserDropdownConfig, IUserDropdownValidation } from "interfaces/org-user.interface";
import { IStore } from "interfaces/redux.interface";
import { OrgUserDropdownTypes, OrgUserTraversal } from "enums/org-user-dropdown.enum";
import { getCurrentUser, getUserById } from "oneRedux/reducers/user.reducer";
import { find, isUndefined } from "lodash";

import { GUID } from "constants/regex.constant";
/*
OrgUser Dropdown Component How To Use:
    NOTE: All selections are sent to parent
    NOTE: 4 Ways to Use this Component:
        1) No Validations - Only Selections sent to parent
        2) No Validations - Both Selection AND Custom text sent to parent
        3) Require Validations - Only Selections sent to parent
        4) Require Validations - Both Selection AND Custom text sent to parent
    NOTE: All bindings are optional except "onSelect"
    NOTE: Passing in individual bindings will overwrite values declared in the userDropdownTemplates in org-user-dropdown.service
    NOTE: Custom Validation methods are defined and called in orgUserDropdownValidations in the org-user-dropdown.service

Best Practice:
    1) Use/Create a template from userDropdownTemplates in the org-user-dropdown.service
    2) Define additional custom validations in the org-user-dropdown.service if needed
    3) Pass in your UserDropdownTemplate enum with the "type" binding and custom validations with the "validations" binding
    4) Overwrite the userDropdownTemplate defaults by passing in your own individual bindings
*/

class OrgUserDropdownController implements ng.IComponentController {

    static $inject: string[] = [
        "store",
        "$rootScope",
        "OrgGroups",
        "OrgUserDropdownService",
    ];

    private dropdownOptions: IOrgUserAdapted[];
    private isTypeaheadTouched: boolean;
    private isSelectionValid: boolean;
    private selection: any;
    private filterValidationErrors: string[] | undefined;
    private apiCallFailedMessage: string | undefined;
    private config: IUserDropdownConfig;
    private typeaheadIdentifier = "orgUserDropdownTypeahead";
    // Bindings
    private allowEmpty: boolean | undefined;
    private allowOther: boolean | undefined;
    private allowInvalidPrepopulated: boolean;
    private prepopulatedValueInvalid: boolean;
    private doValidate: boolean | undefined;
    private defaultCurrentUser: boolean | undefined;
    private emptyText: string | undefined;
    private errorText: string | undefined;
    private filterCurrentUser: boolean | undefined;
    private hideLabel: boolean | undefined;
    private identifier: string | undefined;
    private inputLimit: number = null;
    private label: string | undefined;
    private labelKey: string | undefined;
    private labelDescription: string | undefined;
    private loadExternalUserOptions: boolean;
    private loadUserOptionsOnClick: boolean;
    private userOptions: IOrgUserAdapted[] | undefined;
    private orgId: string | undefined;
    private otherOptionPrependText: string | undefined;
    private model: string;
    private permissionKeys: string[] | undefined;
    private placeholder: string | undefined;
    private removeCurrentUser: boolean | undefined;
    private traversal: OrgUserTraversal | undefined;
    private type: OrgUserDropdownTypes | undefined;
    private validations: IUserDropdownValidation[] | undefined;
    private onSelect: (selectParam: { selection: IOrgUserAdapted | string, isValid: boolean }) => void;
    private isLoadingOptions: boolean;

    constructor(
        readonly store: IStore,
        readonly $rootScope: IExtendedRootScopeService,
        readonly OrgGroups: OrgGroupApiService,
        readonly orgUserDropdownService: OrgUserDropdownService,
    ) {}

    public getOptionsAsync = (): ng.IPromise<IProtocolResponse<IOrgUserAdapted[]>> => {
        return this.OrgGroups.getOrgUsersWithPermission(
            this.config.orgId,
            this.config.traversal,
            this.config.filterCurrentUser,
            this.config.permissionKeys,
        ).then((res: IProtocolResponse<IOrgUserAdapted[]>): any|void => {
            if (res.result) {
                this.apiCallFailedMessage = undefined;
                return res.data;
            } else {
                this.apiCallFailedMessage = this.$rootScope.t("ErrorRetrievingOrganizationGroupsWithPermission");
            }
        });
    }

    public $onInit(): void {
        if (isUndefined(this.loadExternalUserOptions)) this.loadExternalUserOptions = false;
        if (isUndefined(this.loadUserOptionsOnClick)) this.loadUserOptionsOnClick = false;
        if (isUndefined(this.allowInvalidPrepopulated)) this.allowInvalidPrepopulated = false;
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        const preConfig: IUserDropdownConfig = {
            allowOther: this.allowOther,
            doValidate: this.doValidate,
            errorText: this.errorText,
            filterCurrentUser: this.filterCurrentUser,
            hideLabel: this.hideLabel,
            label: this.label,
            labelKey: this.labelKey,
            labelDescription: this.labelDescription,
            orgId: this.orgId,
            otherOptionPrependText: this.otherOptionPrependText,
            permissionKeys: this.permissionKeys,
            placeholder: this.placeholder,
            traversal: this.traversal,
            type: this.type,
            validations: this.validations,
            allowEmpty: this.allowEmpty,
            emptyText: this.emptyText,
        };
        this.config = this.orgUserDropdownService.getOrgUserDropdownConfig(preConfig);
        if (changes.orgId || changes.permissionKeys || changes.type || changes.userOptions || changes.model || changes.loadExternalUserOptions) {
            if (this.removeCurrentUser) {
                this.selection = undefined; // removing selection when org id is changed
            }
            if (this.loadExternalUserOptions) {
                this.dropdownOptions = this.userOptions;
                this.updateSelection(changes.model ? changes.model.currentValue : this.model);
            }
            if (this.loadUserOptionsOnClick) {
                this.inputLimit = 0;
                return;
            } else if (!this.loadExternalUserOptions) {
                this.isLoadingOptions = true;
                this.OrgGroups.getOrgUsersWithPermission(
                    this.config.orgId,
                    this.config.traversal,
                    this.config.filterCurrentUser,
                    this.config.permissionKeys,
                ).then((res: IProtocolResponse<IOrgUserAdapted[]>): void => {
                    if (res.result) {
                        this.apiCallFailedMessage = undefined;
                        this.dropdownOptions = res.data;
                        this.updateSelection(changes.model ? changes.model.currentValue : this.model);
                    } else {
                        this.apiCallFailedMessage = this.$rootScope.t("ErrorRetrievingOrganizationGroupsWithPermission");
                    }
                    this.isLoadingOptions = false;
                });
            }
        }

        if (changes.model && changes.model.isFirstChange()) {
            this.isTypeaheadTouched = false;
            this.selection = getUserById(this.model)(this.store.getState())
                            || find(this.dropdownOptions, (option: IOrgUserAdapted): boolean => option.Id === this.model)
                            || changes.model.currentValue;
            this.onSelect({ selection: this.selection, isValid: (this.config.allowEmpty && this.selection === "") || Boolean(this.selection) });
        } else if (changes.model && this.allowEmpty && !changes.model.currentValue) {
            this.selection = "";
        } else if (changes.defaultCurrentUser) {
            this.isTypeaheadTouched = false;
            this.selection = this.defaultCurrentUser ? getCurrentUser(this.store.getState()) : undefined;
            this.onSelect({ selection: this.selection, isValid: Boolean(this.selection) });
        }

        if (this.identifier) {
            this.typeaheadIdentifier = this.identifier + "Typeahead";
        }
    }

    private updateSelection(model: string): void {
        // update selection based on the the fullName and id of the current model inside the dropdown list (when user is not yet in the user store)
        if (this.model) {
            this.isTypeaheadTouched = false;
            if (this.allowInvalidPrepopulated && !Utilities.matchRegex(model, GUID) && !this.orgUserDropdownService.isValidEmail(model)) {
                this.prepopulatedValueInvalid = true;
                this.selection = model;
                this.onSelect({ selection: this.selection, isValid: false });
                this.allowInvalidPrepopulated = false;
            } else {
                this.prepopulatedValueInvalid = false;
                this.selection = find(this.dropdownOptions, (option: IOrgUserAdapted): boolean => option.Id === model) || (this.orgUserDropdownService.isValidEmail(model) ? model : "");
                this.onSelect({ selection: this.selection, isValid: Boolean(this.selection) });
            }
        }
        // Do not add filters - BE filters users by Permission Keys and also removes Inactive users
    }

    private doFirstOnSelection(): void {
        this.isTypeaheadTouched = true;
        this.filterValidationErrors = undefined;
    }

    private onSelection = (item: any): void => {
        this.doFirstOnSelection();
        this.prepopulatedValueInvalid = false;

        // allow empty - have no errors when input is empty
        if (this.config.allowEmpty) {
            // check if the item's full name is equal to empty text (if provided) or if there is no FullName (when no emptyText Provided)
            this.isSelectionValid = true;
            if (!item || !item.FullName || item.FullName === this.config.emptyText) {
                this.selection = "";
                this.onSelect({ selection: this.selection, isValid: this.isSelectionValid });
                return;
            }
        }

        // When Validation is NOT required...
        if (!this.config.doValidate) {
            if (!item) {
                this.selection = item;
                this.isSelectionValid = false;
            } else if (this.config.allowOther) {
                this.selection = item.InputValue ? item.InputValue : item;
                this.isSelectionValid = true;
            } else {
                this.selection = item;
                this.isSelectionValid = true;
            }
            this.onSelect({ selection: this.selection, isValid: this.isSelectionValid });
            return;
        }

        // When Validation IS required...
        if (!item) {
            this.selection = item;
        } else if (this.config.allowOther) {
            this.selection = item.InputValue ? item.InputValue : item;
        } else {
            this.selection = item;
        }

        // When Custom Validations are provided
        if (this.selection && this.config.validations.length) {
            const { isValid, errors } = this.orgUserDropdownService.runCustomValidations(this.selection, this.config.validations);
            if (!isValid) {
                this.isSelectionValid = false;
                this.filterValidationErrors = errors;
                this.onSelect({ selection: this.selection, isValid: this.isSelectionValid });
                return;
            }
        }

        this.isSelectionValid = this.selection ? this.orgUserDropdownService.isValidGuidOrEmail(this.selection) : false;
        this.onSelect({ selection: this.selection, isValid: this.isSelectionValid });
        return;
    }
}

export const orgUserDropdown: ng.IComponentOptions = {
    bindings: {
        allowEmpty: "<?",
        allowOther: "<?",               // [True]: Dropdown allows other entries (custom text). [False]: Dropdown restricts other entries
        allowInvalidPrepopulated: "<?", // [True]: Do not clear value if previous value saved by user is invalid
        appendToBody: "<?",
        bodyClass: "@?",
        defaultCurrentUser: "<?",       // [True]: Dropdown will default to currentUser.  [False]: Dropdown will default to placeholder
        doValidate: "<?",               // [True]: Dropdown sends entries up to parent with validation. [False]: Dropdown sends entries up to parent without validation
        emptyText: "@?",                // pass it in as "----" or any other string when you have allowEmpty true
        errorText: "@?",                // The standard error text shown
        hideLabel: "<?",                // [True]: Hides label [False]: Shows label
        identifier: "@?",
        inputClass: "@?",
        isDisabled: "<?",
        isParentModal: "<?",
        isRequired: "<?",
        requiredFlagBeforeLabel: "<?",
        label: "@?",                    // The "name" attr for the <one-label-content> wrapping the typeahead
        labelClass: "@?",
        labelInline: "<?",              // [True]: Makes label inline with dropdown. [False]: Puts label on top of dropdown.
        labelKey: "<?",                 // The "label-key" attr for the <typeahead> dropdown... determines the property value shown in the dropdown (Ex: FullName)
        labelDescription: "<?",         // The "label-key" attr for the <typeahead> dropdown... determines the property value shown in the dropdown (Ex: FullName)
        labelStyle: "<?",
        onSelect: "&",
        orgId: "<?",                    // The org Id for the API call - defaults to currentUser.OrgGroupId
        otherOptionPrependText: "@?",   // When allowOther is TRUE, this will be the text that prepends the text - defaults to "Re-assign To"
        model: "<?",                    // The id of the current user selected. This can be used to prepopulate the dropdown.
        permissionKeys: "<?",           // The array of permission keys for the API call - defaults to "HelpContact" (given to all users)
        placeholder: "<?",              // The "placeholder-text" attr for the <typeahead> dropdown
        removeCurrentUser: "<?",
        scrollParent: "<?",             // Parent element to append dropdown to for scrolling
        traversal: "<?",                // The org direction for the API call based on OrgUserTraversal enum
        tooltipPosition: "@?",          // The position of the tooltip
        type: "<?",                     // The OrgUserDropdownType being used - default is OrgUserDropdownType.Default
        wrapperClass: "@?",
        validations: "<?",              // An array of validation enums that control how the user dropdown options are validated upon selection
        loadUserOptionsOnClick: "<?",
        loadExternalUserOptions: "<?",  // [True]: Will not make API calls, and will use options from "userOptions" binding. [False]: Will make API call
        userOptions: "<?",              // Pass list of userOptions from outside the component, & mark "loadExternalUserOptions" as true, so that additional API calls are not made to get the options.
        filterCurrentUser: "<?",        // Pass boolean to fiter current user
    },
    controller: OrgUserDropdownController,
    template: `
        <one-label-content
            body-class="full-width {{$ctrl.bodyClass}}"
            name="{{::$ctrl.config.label}}"
            hide-name="$ctrl.hideLabel"
            inline="$ctrl.labelInline"
            is-required="$ctrl.isRequired"
            label-class="org-user-dropdown__label {{$ctrl.labelClass}}"
            wrapper-class="{{$ctrl.wrapperClass}}"
            body-class="full-width {{$ctrl.bodyClass}}"
            is-required="$ctrl.isRequired"
            required-flag-before-label = "$ctrl.requiredFlagBeforeLabel"
            description="{{::$ctrl.config.labelDescription}}"
            label-style="$ctrl.labelStyle"
            tooltip-position="{{$ctrl.tooltipPosition}}"
            >
            <typeahead
                allow-empty="$ctrl.config.allowEmpty"
                allow-other="$ctrl.config.allowOther"
                class="org-user-dropdown__input {{$ctrl.inputClass}}"
                disable-if-no-options="false"
                empty-text="{{$ctrl.config.emptyText}}"
                has-arrow="!$ctrl.isDisabled"
                has-error="$ctrl.prepopulatedValueInvalid || (!$ctrl.isSelectionValid && $ctrl.isTypeaheadTouched && $ctrl.isRequired) || $ctrl.apiCallFailedMessage"
                is-disabled="$ctrl.isDisabled"
                is-required="$ctrl.isRequired"
                identifier="$ctrl.typeaheadIdentifier"
                label-key="{{$ctrl.config.labelKey}}"
                model="$ctrl.selection"
                on-select="$ctrl.onSelection(selection)"
                options="$ctrl.dropdownOptions"
                order-by-label="true"
                other-option-prepend="{{::$ctrl.config.otherOptionPrependText}}"
                placeholder-text="$ctrl.config.placeholder"
                scroll-parent="$ctrl.scrollParent"
                append-to-body="$ctrl.appendToBody"
                is-parent-modal="$ctrl.isParentModal"
                load-on-click="$ctrl.loadUserOptionsOnClick"
                input-limit="$ctrl.inputLimit"
                async-options-function="$ctrl.getOptionsAsync()"
                is-loading="$ctrl.isLoadingOptions"
            >
            </typeahead>
        </one-label-content>
        <one-label-content
            body-class="full-width"
            hide-name="true"
            inline="true"
            label-class="org-user-dropdown__label {{$ctrl.labelClass}}"
            label-style="$ctrl.labelStyle"
            wrapper-class="{{$ctrl.wrapperClass}}">
            <div
                class="{{$ctrl.inputClass}}">
                <p
                    ng-if="$ctrl.prepopulatedValueInvalid || ($ctrl.isRequired && !$ctrl.isSelectionValid && !$ctrl.filterValidationErrors && $ctrl.isTypeaheadTouched)"
                    class="text-small text-error margin-top-bottom-half">
                    {{$ctrl.config.errorText}}
                </p>
                <p
                    ng-if="!$ctrl.isSelectionValid && $ctrl.isTypeaheadTouched && $ctrl.filterValidationErrors"
                    ng-repeat="validationError in $ctrl.filterValidationErrors track by $index"
                    class="text-small text-error margin-top-bottom-half">
                    {{validationError}}
                </p>
                <p
                    ng-if="$ctrl.apiCallFailedMessage"
                    class="text-small text-error margin-top-bottom-half">
                    {{$ctrl.apiCallFailedMessage}}
                </p>
            </div>
        </one-label-content>
    `,
};
