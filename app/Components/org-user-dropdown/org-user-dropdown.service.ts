import { includes, find, map, isBoolean } from "lodash";
import Utilities from "Utilities";
import { getCurrentUser, getDisabledUsers, getProjectViewers } from "oneRedux/reducers/user.reducer";

import { Regex } from "constants/regex.constant";
import { EmptyGuid } from "constants/empty-guid.constant";

import { OrgUserDropdownTypes, OrgUserDropdownValidations, OrgUserTraversal } from "enums/org-user-dropdown.enum";

import { IStore } from "interfaces/redux.interface";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IUser } from "interfaces/user.interface";
import { INumberMap } from "interfaces/generic.interface";
import { IUserDropdownConfig, IUserDropdownValidationResult, IUserDropdownValidation } from "interfaces/org-user.interface";
import { AssessmentPermissions } from "constants/assessment.constants";

export class OrgUserDropdownService {

    static $inject: string[] = [
        "store",
        "$rootScope",
    ];

    private currentUser: IUser;
    private userDropdownTemplates: INumberMap<IUserDropdownConfig>;
    private orgUserDropdownValidations: INumberMap<IUserDropdownValidation>;

    constructor(
        private readonly store: IStore,
        private readonly $rootScope: IExtendedRootScopeService,
    ) {
        const { checkProjectViewer, checkDisabledUser } = OrgUserDropdownValidations;
        this.currentUser = getCurrentUser(store.getState());

        this.orgUserDropdownValidations = {
            [OrgUserDropdownValidations.checkProjectViewer]: {
                errorMessage: this.$rootScope.t("Error.EmailBelongsToProjectViewer"),
                id: checkProjectViewer,
                validate: (selection: any) => !Boolean(find(getProjectViewers(this.store.getState()), (user) => user.Email === selection)),
            },
            [OrgUserDropdownValidations.checkDisabledUser]: {
                errorMessage: this.$rootScope.t("Error.EmailBelongsToDisabledUser"),
                id: checkDisabledUser,
                validate: (selection: any) => !Boolean(find(getDisabledUsers(this.store.getState()), (user) => user.Email === selection)),
            },
        };

        this.userDropdownTemplates = {
            [OrgUserDropdownTypes.RespondentPia]: {
                allowOther: true,
                doValidate: true,
                errorText: this.$rootScope.t("PleaseAssignValidProjectRespondent", { Project: this.$rootScope.customTerms.Project }),
                filterCurrentUser: false,
                label: this.$rootScope.t("Respondent"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: getCurrentUser(store.getState()).OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("AssignTo"),
                permissionKeys: [AssessmentPermissions.AssessmentCanBeRespondent],
                placeholder: this.$rootScope.t("AssignRespondent"),
                traversal: OrgUserTraversal.Branch,
                type: OrgUserDropdownTypes.RespondentPia,
            },
            [OrgUserDropdownTypes.RespondentDm]: {
                allowOther: true,
                doValidate: true,
                errorText: this.$rootScope.t("RessignmentValidation"),
                filterCurrentUser: false,
                label: this.$rootScope.t("ReAssignTo"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                permissionKeys: ["DataMappingAssessmentsRespond"],
                placeholder: this.$rootScope.t("AssignRespondent"),
                traversal: OrgUserTraversal.Branch,
                type: OrgUserDropdownTypes.RespondentDm,
            },
            [OrgUserDropdownTypes.AssignRespondentDm]: {
                allowOther: true,
                doValidate: true,
                errorText: this.$rootScope.t("PleaseSelectFromTheDropdown"),
                filterCurrentUser: false,
                label: this.$rootScope.t("Respondent"),
                labelKey: "FullName",
                labelDescription: this.$rootScope.t("AnEmailWithAssessmentLinkIsSentToThisIndividual"),
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("AssignTo"),
                permissionKeys: ["DataMappingAssessmentsRespond"],
                placeholder: this.$rootScope.t("AssignRespondent"),
                traversal: OrgUserTraversal.Branch,
                type: OrgUserDropdownTypes.AssignRespondentDm,
            },
            [OrgUserDropdownTypes.ApproverPia]: {
                allowOther: false,
                doValidate: false,
                errorText: this.$rootScope.t("AssignValidProjectApprover", { Project: this.$rootScope.customTerms.Project }),
                filterCurrentUser: false,
                label: this.$rootScope.t("Approver"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                permissionKeys: [AssessmentPermissions.AssessmentCanBeApprover],
                placeholder: this.$rootScope.t("AssignApprover"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.ApproverPia,
            },
            [OrgUserDropdownTypes.ApproverDm]: {
                allowOther: false,
                doValidate: false,
                errorText: this.$rootScope.t("AssignValidProjectApprover", { Project: this.$rootScope.customTerms.Project }),
                filterCurrentUser: false,
                label: this.$rootScope.t("ReAssignTo"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                permissionKeys: ["DataMappingAssessmentsApprove"],
                placeholder: this.$rootScope.t("AssignApprover"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.ApproverDm,
            },
            [OrgUserDropdownTypes.RiskOwner]: {
                allowOther: false,
                doValidate: false,
                errorText: this.$rootScope.t("PleaseAssignValidOwner"),
                filterCurrentUser: false,
                label: this.$rootScope.t("RiskOwner"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [
                    this.orgUserDropdownValidations[checkProjectViewer],
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                permissionKeys: [AssessmentPermissions.CanBeRiskOwner],
                placeholder: this.$rootScope.t("PleaseSelectOwner"),
                traversal: OrgUserTraversal.Branch,
                type: OrgUserDropdownTypes.RiskOwner,
            },
            [OrgUserDropdownTypes.Default]: {
                allowOther: false,
                doValidate: false,
                errorText: this.$rootScope.t("InvalidUserWarning"),
                filterCurrentUser: false,
                label: this.$rootScope.t("User"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [],
                orgId: this.currentUser.OrgGroupId,
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                permissionKeys: ["HelpContact"],
                placeholder: this.$rootScope.t("SelectUserOrEnterEmail"),
                traversal: OrgUserTraversal.All,
                type: OrgUserDropdownTypes.Default,
            },
            [OrgUserDropdownTypes.HelpContact]: {
                allowOther: false,
                doValidate: false,
                errorText: this.$rootScope.t("InvalidUserWarning"),
                filterCurrentUser: false,
                label: this.$rootScope.t("EmailAddress"),
                labelKey: "FullName",
                labelDescription: "",
                validations: [],
                orgId: this.currentUser.OrgGroupId,
                permissionKeys: ["CanBeHelpContact"],
                placeholder: this.$rootScope.t("SelectUserOrEnterEmail"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.HelpContact,
            },
            [OrgUserDropdownTypes.DSARSubTaskAssignee]: {
                allowEmpty: true,
                allowOther: true,
                doValidate: true,
                emptyText: "- - - -",
                errorText: this.$rootScope.t("RessignmentValidation"),
                filterCurrentUser: false,
                labelKey: "FullName",
                validations: [
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                permissionKeys: ["HelpContact"], // TODO: change when proper Permission added
                placeholder: this.$rootScope.t("AssignRespondentOrInviteEmail"),
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                traversal: OrgUserTraversal.All,
                type: OrgUserDropdownTypes.DSARSubTaskAssignee,
            },
            [OrgUserDropdownTypes.DSARWebFromApprover]: {
                allowEmpty: true,
                allowOther: false,
                doValidate: false,
                emptyText: "- - - -",
                filterCurrentUser: false,
                labelKey: "FullName",
                validations: [
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                permissionKeys: ["HelpContact"], // TODO: change when proper Permission added
                placeholder: this.$rootScope.t("SelectAssignee"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.DSARWebFromApprover,
            },
            [OrgUserDropdownTypes.DSARRequestAssignee]: {
                allowEmpty: true,
                allowOther: true,
                doValidate: true,
                emptyText: "- - - -",
                errorText: this.$rootScope.t("RessignmentValidation"),
                filterCurrentUser: false,
                labelKey: "FullName",
                validations: [
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                orgId: this.currentUser.OrgGroupId,
                permissionKeys: ["HelpContact"], // TODO: change when proper Permission added
                placeholder: this.$rootScope.t("AssignRespondentOrInviteEmail"),
                otherOptionPrependText: this.$rootScope.t("ReAssignTo"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.DSARRequestAssignee,
            },
            [OrgUserDropdownTypes.OrgGroupDefaultApprover]: {
                allowOther: false,
                filterCurrentUser: false,
                labelKey: "FullName",
                labelDescription: "",
                permissionKeys: ["OrgGroupsPrivacyOfficer"],
                placeholder: this.$rootScope.t("SearchDefaultApprover"),
                traversal: OrgUserTraversal.Up,
                type: OrgUserDropdownTypes.OrgGroupDefaultApprover,
            },
            [OrgUserDropdownTypes.DMDetails]: {
                doValidate: true,
                errorText: this.$rootScope.t("InvalidUserWarning"),
                filterCurrentUser: false,
                labelKey: "FullName",
                orgId: this.currentUser.OrgGroupId,
                traversal: OrgUserTraversal.Branch,
                type: OrgUserDropdownTypes.DMDetails,
                permissionKeys: [],
                validations: [
                    this.orgUserDropdownValidations[checkDisabledUser],
                ],
                otherOptionPrependText: this.$rootScope.t("Add"),
            },
        };
    }

    public getOrgUserDropdownConfig(
        config?: IUserDropdownConfig,
    ): IUserDropdownConfig {
        const template: IUserDropdownConfig = config.type ? this.userDropdownTemplates[config.type] : this.userDropdownTemplates[OrgUserDropdownTypes.Default];
        return {
            ...template,
            allowOther:             isBoolean(config.allowOther) ? config.allowOther : template.allowOther,
            doValidate:             isBoolean(config.doValidate) ? config.doValidate : template.doValidate,
            filterCurrentUser:      isBoolean(config.filterCurrentUser) ? config.filterCurrentUser : template.filterCurrentUser,
            label:                  config.hideLabel ? "" : config.label || template.label,
            labelKey:               config.labelKey || template.labelKey,
            labelDescription:       config.labelDescription || template.labelDescription,
            orgId:                  config.orgId || template.orgId,
            otherOptionPrependText: config.otherOptionPrependText || template.otherOptionPrependText,
            permissionKeys:         config.permissionKeys || template.permissionKeys,
            placeholder:            config.placeholder || template.placeholder,
            traversal:              config.traversal || template.traversal,
            validations:            config.validations || template.validations,
            errorText:              config.errorText || template.errorText,
            allowEmpty:             config.allowEmpty || template.allowEmpty,
            emptyText:              config.emptyText || template.emptyText,
        };
    }

    public isValidGuidOrEmail(item: any): boolean {
        const isGuidOrEmail: boolean = Utilities.matchRegex(item.Id, Regex.GUID) || this.isValidEmail(item);
        return item && item !== EmptyGuid && isGuidOrEmail;
    }

    public isValidEmail(email: string): boolean {
        return Utilities.matchRegex(email, Regex.EMAIL);
    }

    public runCustomValidations(selection: any, filters: IUserDropdownValidation[]): IUserDropdownValidationResult {
        if (!this.isValidGuidOrEmail(selection)) {
            return { isValid: false, errors: [this.$rootScope.t("EnterAValidEmailAddress")]};
        }
        const errors: string[] = [];
        const validations: boolean[] = map(filters, (userFilter) => {
            const isValid: boolean = userFilter.validate(selection);
            if (!isValid) errors.push(userFilter.errorMessage);
            return isValid;
        });

        return {
            isValid: !includes(validations, false),
            errors,
        };
    }
}
