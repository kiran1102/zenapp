import { KeyCodes } from "enums/key-codes.enum";
import * as _ from "lodash";

export interface IEditableConfig {
    allowEmpty?: boolean;
    allowOther?: boolean;
    clearOnSelect?: boolean;
    disableIfNoOptions?: boolean;
    emptyOptionLast: boolean;
    emptyText?: string;
    highlightOnShow?: boolean;
    identifier?: string;
    inputType: number;
    isDisabled?: boolean;
    isEditable?: boolean;
    mapModel?: any;
    openOnFocus?: boolean;
    openOnInit?: boolean;
    optionLabelKey?: string;
    options?: any[];
    orderByLabel?: boolean;
    otherText?: string;
    placeholder?: string;
    saveOnBlur?: boolean;
}

class EditableController implements ng.IController {

    static $inject: string[] = ["$scope", "$timeout", "$element", "ENUMS"];

    private InputTypes: any;
    private model: any;
    private tempModel: any;
    private config: IEditableConfig;
    private isEditable: boolean;
    private isEditMode = false;
    private onFocus: any;
    private onBlur: any;
    private save: any;

    constructor(readonly $scope: any, readonly $timeout: ng.ITimeoutService, readonly $element: HTMLElement, ENUMS: any) {
        this.InputTypes = ENUMS.InputTypes;
    }

    $onInit() {
        if (this.config.openOnInit) {
            this.toggleEditMode(true);
            this.focus();
        }
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        const model: any = changes.model ? changes.model.currentValue : this.model;
        this.tempModel = this.config.mapModel ? this.config.mapModel(model) : model;

        if (_.isUndefined(this.config.isEditable)) this.config.isEditable = true;
        this.isEditable = this.config.isEditable && !this.config.isDisabled;
    }

    private toggleEditMode(value?: boolean): void {
        this.isEditMode = (!_.isUndefined(value) ? value : !this.isEditMode);
    }

    private focus(): any | undefined {
        if (this.config.openOnFocus) this.toggleEditMode(true);
        this.$timeout((): void => {
            this.setupInputContents();
        }, 0);
        if (this.onFocus) return this.onFocus(this.model);
    }

    private blur(model?: any): any | undefined {
        if (this.config.saveOnBlur && !_.isUndefined(model) && this.tempModel !== model) this.save({ model });
        this.toggleEditMode(false);
        if (this.onBlur) return this.onBlur();
    }

    private handleSave($model: any = this.tempModel): void {
        this.save({ model: $model });
        this.toggleEditMode(false);
    }

    private handleMultiselectSave($model: any, $item: any): void {
        if (!this.config.saveOnBlur) this.save({ model: $model });
    }

    private setupInputContents(): void {
        if (this.config.inputType === this.InputTypes.Multiselect) {
            const inputElToggle: any = this.getInputElementToggle(this.$element);
            if (inputElToggle) inputElToggle.click();
        }
        const inputEl: any = this.getInputElement(this.$element);
        if (inputEl) {
            inputEl.onkeyup = this.handleKeyEvents;
            inputEl.focus();
            if (this.config.highlightOnShow) inputEl.select();
        }
    }

    private getInputElement(elem: HTMLElement): HTMLElement {
        return (elem && elem[0]) ? elem[0].querySelector("input") : null;
    }

    private getInputElementToggle(elem: HTMLElement): HTMLElement {
        return (elem && elem[0]) ? elem[0].querySelector(".ui-select-toggle") : null;
    }

    private handleKeyEvents = (event: any): void | undefined => {
        if (!event || !event.keyCode) return;
        if (event.keyCode === KeyCodes.Esc) this.handleEscapeEvent(event);
    }

    private handleEscapeEvent(event: any): void {
        this.$scope.$apply(() => {
            this.toggleEditMode(false);
        });
    }

}

const EditableTemplate = `
	<div class="one-editable">
        <span
            class="one-editable__non-editable"
            ng-if="::!$ctrl.isEditable"
            ng-transclude
            >
        </span>
		<section ng-if="::$ctrl.isEditable"
            >
			<a
				tabindex="0"
				class="one-editable__link toggle-link toggled"
				ng-if="!$ctrl.isEditMode"
				ng-click="$ctrl.toggleEditMode(true)"
				ng-focus="$ctrl.focus()"
				ng-transclude
				>
			</a>
			<form
                tabindex="-1"
				class="one-editable__form full-width"
				ng-if="$ctrl.isEditMode"
                ng-submit="$ctrl.handleSave()"
				>
                <input
                    data-input="$ctrl.InputTypes.Text"
                    data-id="{{::$ctrl.config.identifier}}"
                    class="one-editable__input full-width one-input"
                    ng-if="::($ctrl.config.inputType === $ctrl.InputTypes.Text)"
                    ng-model="$ctrl.tempModel"
                    ng-blur="$ctrl.handleSave()"
                />
				<one-select
					data-input="$ctrl.InputTypes.List"
					class="one-editable__list"
					ng-if="::($ctrl.config.inputType === $ctrl.InputTypes.List)"
					model="$ctrl.tempModel"
					on-change="$ctrl.handleListSave()"
					options="$ctrl.config.options"
					label-key="$ctrl.config.optionLabelKey"
					>
				</one-select>
				<typeahead
                    data-input="$ctrl.InputTypes.Select"
                    class="one-editable__typeahead"
                    ng-if="::($ctrl.config.inputType === $ctrl.InputTypes.Select)"
                    options="$ctrl.config.options"
                    order-by-label="$ctrl.config.orderByLabel"
                    model="$ctrl.tempModel"
                    on-select="$ctrl.handleSave(selection)"
                    on-blur="$ctrl.blur()"
                    label-key="{{::$ctrl.config.optionLabelKey}}"
                    has-arrow="false"
                    clear-on-select="::$ctrl.config.clearOnSelect"
                    save-on-blur="::$ctrl.config.saveOnBlur"
                    disable-if-no-options="::$ctrl.config.disableIfNoOptions"
                    placeholder-text="::$ctrl.config.placeholder"
                    allow-other="$ctrl.config.allowOther"
                    other-option-prepend="{{::$ctrl.config.otherText}}"
                    allow-empty="$ctrl.config.allowEmpty"
                    empty-text="{{::$ctrl.config.emptyText}}"
                    empty-option-last="$ctrl.config.emptyOptionLast"
                    input-limit="2"
                    use-virtual-scroll="true"
                    options-limit="1000"
                    >
                </typeahead>
                <one-multiselect
                    data-input="$ctrl.InputTypes.Multiselect"
                    class="one-editable__multiselect"
                    tabindex="-1"
                    ng-if="::($ctrl.config.inputType === $ctrl.InputTypes.Multiselect)"
                    identifier="{{::$ctrl.config.identifier}}"
                    list="$ctrl.config.options"
                    placeholder="{{::$ctrl.config.placeholder}}"
                    label-key="{{::$ctrl.config.optionLabelKey}}"
                    open-on-init="true"
                    order-by-label="::$ctrl.config.orderByLabel"
                    model="$ctrl.tempModel"
                    handle-select="$ctrl.handleMultiselectSave($model, $item)"
                    handle-remove="$ctrl.handleMultiselectSave($model, $item)"
                    on-blur="$ctrl.blur($model)"
                    allow-other="::$ctrl.config.allowOther"
                    allow-other-label="{{::$ctrl.config.otherText}}"
                    allow-empty="$ctrl.config.allowEmpty"
                    empty-option-text="{{::$ctrl.config.emptyText}}"
                    empty-option-last="$ctrl.config.emptyOptionLast"
                    >
                </one-multiselect>
			</form>
		</section>
	</div>
`;

const EditableComponent: any = {
    controller: EditableController,
    template: EditableTemplate,
    transclude: true,
    bindings: {
        model: "<",
        save: "&",
        config: "<?",
        onFocus: "&?",
        onBlur: "&?",
    },
};

export default EditableComponent;
