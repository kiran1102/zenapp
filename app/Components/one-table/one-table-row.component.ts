
const OneTableRowTemplate = `
    <div
        class="
            one-table-row
            {{$ctrl.rowClass}}
			{{$ctrl.loadingClass}}
            {{$ctrl.isDisabled ? 'one-table-row--disabled' : ''}}
            {{::($ctrl.rowHidden ? 'one-table-row--hidden' : '')}}
            {{::($ctrl.rowFirst ? 'one-table-row--first' : '')}}
            {{::($ctrl.rowLast ? 'one-table-row--last' : '')}}
        "
        ng-transclude
        >
    </div>
    `;

const OneTableRowComponent = {
    template: OneTableRowTemplate,
    transclude: true,
    bindings: {
        rowClass: "@?",
        rowFirst: "<?",
        rowLast: "<?",
        rowHidden: "<?",
        isDisabled: "<?",
        loadingClass: "@?",
    },
};

export default OneTableRowComponent;
