const OneTableCellTemplate = `
    <div class="
            one-table-cell
            {{::($ctrl.width ? ('one-table-cell--' + $ctrl.width) : $ctrl.size ? 'one-table-cell--custom' : '')}}
            {{::$ctrl.cellClass}}
            "
        ng-style="::$ctrl.size"
        ng-class="{
            'one-table-cell--error': $ctrl.hasError,
        }"
        >
        <div class="one-table-cell__wrapper {{::$ctrl.wrapperClass}}"
            ng-transclude="content"
            ng-if="::!$ctrl.hideContent"></div>
        <div class="one-table-cell__toggle" ng-transclude="toggle"></div>
    </div>
`;

const OneTableCellComponent = {
    template: OneTableCellTemplate,
    transclude: {
        content: "?oneTableCellContent",
        toggle: "?oneTableCellToggle",
    },
    bindings: {
        width: "@?",
        size: "<?",
        hasError: "<?",
        hideContent: "<?",
        cellClass: "@?",
        wrapperClass: "@?",
    },
};

export default OneTableCellComponent;
