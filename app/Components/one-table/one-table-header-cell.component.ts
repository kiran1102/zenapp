
class OneTableHeaderCellController {

    static $inject: string[] = ["ENUMS"];

    private width: string;
    private leftArrow = false;
    private rightArrow = false;
    private isSorted: boolean;
    private sortType: number;
    private SortTypes: any;
    private sortDirection: number;
    private cellClass: string;
    private wrapperClass: string;

    constructor(ENUMS: any) {
        this.SortTypes = ENUMS.SortTypes;
    }

}

const OneTableHeaderCellTemplate = `
    <div class="
            one-table-cell
            one-table-header__cell
            {{::($ctrl.width ? ('one-table-cell--' + $ctrl.width) : $ctrl.size ? 'one-table-cell--custom' : '')}}
            {{::$ctrl.cellClass}}
        "
        ng-style="::$ctrl.size"
        >
        <span
            class="
                one-table-arrow
                one-table-arrow--left
            "
            ng-class="{'fa fa-angle-left': $ctrl.leftArrow}"
            >
        </span>
        <div class="one-table-cell__wrapper text-bold {{::$ctrl.wrapperClass}}">
            <ng-transclude></ng-transclude>
            <sup
                ng-if="$ctrl.isRequired"
                class="helper-icon-required one-label-content__required"
                >
            </sup>
            <i
                ng-if="$ctrl.isSorted"
                class="one-table-cell__sort"
                ng-class="{
                    'fa fa-caret-down': $ctrl.sortType === $ctrl.SortTypes.Descending || $ctrl.sortDirection === $ctrl.SortTypes.Descending,
                    'fa fa-caret-up': $ctrl.sortType === $ctrl.SortTypes.Ascending || $ctrl.sortDirection === $ctrl.SortTypes.Ascending,
                }"
                >
            </i>
        </div>
        <span
            class="
                one-table-arrow
                one-table-arrow--right
            "
            ng-class="{'fa fa-angle-right': $ctrl.rightArrow}"
            >
        </span>
    </div>
    `;

const OneTableHeaderCellComponent = {
    controller: OneTableHeaderCellController,
    template: OneTableHeaderCellTemplate,
    transclude: true,
    bindings: {
        width: "@?",
        size: "<?",
        leftArrow: "<?",
        rightArrow: "<?",
        isRequired: "<?",
        isSorted: "<?",
        sortType: "<?",
        sortDirection: "<?",
        cellClass: "@?",
        wrapperClass: "@?",
    },
};

export default OneTableHeaderCellComponent;
