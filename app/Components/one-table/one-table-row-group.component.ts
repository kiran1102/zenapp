
const OneTableRowGroupTemplate = `
        <div id="{{$ctrl.scrollId}}" class="one-table-rows {{$ctrl.rowGroupClass}}" ng-transclude></div>
    `;

const OneTableRowGroupComponent = {
    template: OneTableRowGroupTemplate,
    bindings: {
        scrollId: "@?",
        rowGroupClass: "@?",
    },
    transclude: true,
};

export default OneTableRowGroupComponent;
