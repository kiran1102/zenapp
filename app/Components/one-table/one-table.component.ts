const OneTableComponent: ng.IComponentOptions = {
    bindings: {
        wrapperClass: "@?",
        headerClass: "@?",
        bodyClass: "@?",
    },
    transclude: {
        header: "oneTableHeader",
        body: "oneTableBody",
    },
    template: `
        <div data-id="one-table" class="one-table {{::$ctrl.wrapperClass}}" data-one-table>
            <div class="one-table-header {{::$ctrl.headerClass}}" ng-transclude="header"></div>
            <div id="one-table-body" class="one-table-body {{$ctrl.bodyClass}}" ng-transclude="body"></div>
        </div>
    `,
};

export default OneTableComponent;
