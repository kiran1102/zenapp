import { isFunction } from "lodash";

class OneTextarea {
    private inputChanged: (arg: any) => any;
    private inputText: string;
    private placeholder: string;
    private tempModel: string;
    private textareaClass: string;

    public $onInit(): void {
        this.tempModel = this.inputText;
    }

    public handleChange(value: string): void {
        if (isFunction(this.inputChanged)) {
            this.inputChanged({ value });
        }
    }
}

const OneTextareaComponent = {
    controller: OneTextarea,
    bindings: {
        identifier: "@?",
        inputChanged: "&?",
        inputText: "@?",
        name: "@?",
        placeholder: "@?",
        disableResize: "@?",
        textareaClass: "@?",
        isRequired: "<?",
        isDisabled: "<?",
        maxLength: "@?",
        showCharCount: "<?",
    },
    template: `
        <textarea
            class="one-text-area one-textarea full-width full-height {{$ctrl.textareaClass}}"
            ot-auto-id="{{$ctrl.identifier}}"
            name="{{$ctrl.name}}"
            ng-class="{ 'resize-none': $ctrl.disableResize, 'read-only': $ctrl.isDisabled }"
            ng-value="$ctrl.inputText || ''"
            ng-model="$ctrl.tempModel"
            ng-change="$ctrl.handleChange($ctrl.tempModel)"
            placeholder="{{$ctrl.placeholder}}"
            ng-readonly="$ctrl.isDisabled"
            maxlength="{{$ctrl.maxLength}}"
            ng-required="$ctrl.isRequired"
        ></textarea>
        <p class="align-right" ng-if="$ctrl.showCharCount">{{$ctrl.inputText.length}}/{{$ctrl.maxLength}}</p
    `,
};

export default OneTextareaComponent;
