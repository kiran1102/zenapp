// Rxjs
import {
    Observable,
    Subscription,
} from "rxjs";

import * as angular from "angular";

// Third Party
import * as Quill from "quill";
import Delta from "quill-delta";
import {
    merge,
    includes,
 } from "lodash";

// Interfaces
import { IStringMap } from "interfaces/generic.interface";
import { Regex } from "constants/regex.constant";

// Services
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { NotificationService } from "sharedModules/services/provider/notification.service";

class OneRichTextEditorController {

    static $inject: string[] = [
        "$rootScope",
        "$scope",
        "$element",
        "$sce",
        "NotificationService",
    ];

    private editorClassName: string;
    private config: any;
    private content: string;
    private contentObservable: Observable<string>;
    private contentSubscription: Subscription;
    private quill: any;
    private onContentChange: (contentParam: { data: string }) => void;
    private translations: IStringMap<string>;
    private DefaultQuillConfig: any;
    private htmlContent: boolean;

    constructor(
        private $rootScope: IExtendedRootScopeService,
        private $scope: any,
        private $element: any,
        private $sce: ng.ISCEService,
        private notificationService: NotificationService,
    ) {}

    public $onInit(): void {
        const self = this;
        this.translations = {
            error: this.$rootScope.t("Error"),
            extensionNotAllowed: this.$rootScope.t("FileTypeExtensionNotValid"),
        };
        this.DefaultQuillConfig = {
            modules: {
                toolbar: {
                    container: [
                        ["bold", "italic", "underline", "strike", "image", { list: "ordered" }, { list: "bullet" }],
                        [{ align: "" }, { align: "right" }, { align: "center" }, { align: "justify" }, "link"],
                    ],
                    handlers: {
                        link(): void {
                            const range: IStringMap<number> = this.quill.getSelection();
                            if (!range.length) {
                                return;
                            }

                            const preview: string = this.quill.getText(range);
                            const tooltip: any = this.quill.theme.tooltip;
                            const protocol: string = preview.slice(0, preview.indexOf(":"));
                            const isProtocolUsedFromList: boolean = includes(["http", "https", "mailto", "tel", "ftp", "sftp"], protocol);
                            if (!isProtocolUsedFromList) {
                                tooltip.edit("link", "http://" + preview);
                            } else {
                                tooltip.edit("link", preview);
                            }
                        },
                        image() {
                            let fileInput: HTMLInputElement = this.container.querySelector("input.ql-image[type=file]");
                            if (fileInput == null) {
                                fileInput = document.createElement("input");
                                fileInput.setAttribute("type", "file");
                                fileInput.setAttribute("accept", "image/png, image/gif, image/jpeg, image/bmp, image/x-icon");
                                fileInput.classList.add("ql-image");
                                fileInput.addEventListener("change", (e: any): boolean | void => {
                                    if (e.target.files[0].name.split(".").pop().toLowerCase() === "exe") {
                                        self.notificationService.alertError(self.translations.error, self.translations.extensionNotAllowed);
                                        self.$scope.$apply();
                                        fileInput.value = "";
                                        return false;
                                    }
                                    if (fileInput.files != null && fileInput.files[0] != null) {
                                        const reader: FileReader = new FileReader();
                                        reader.onload = (event: any): void => {
                                            const range: any = this.quill.getSelection(true);
                                            this.quill.updateContents(new Delta()
                                                .retain(range.index)
                                                .delete(range.length)
                                                .insert({ image: event.target.result })
                                                , Quill.sources.USER);
                                            fileInput.value = "";
                                        };
                                        reader.readAsDataURL(fileInput.files[0]);
                                    }
                                });
                                this.container.appendChild(fileInput);
                            }
                            fileInput.click();
                        },
                    },
                },
            },
            theme: "snow",
            readOnly: false,
            bounds: document.querySelector(".save-question__label"),
        };

        (this.editorClassName) ? this.initEditor(this.editorClassName) : this.initEditor("");
        this.attachEvents();
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (this.quill && changes.content && changes.content.currentValue !== changes.content.previousValue && this.quill.root.innerHTML !== changes.content.currentValue) {
            this.quill.root.innerHTML = this.$sce.getTrustedHtml(this.content.replace(Regex.REMOVE_SPACE_BETWEEN_TAGS, ">"));
        }
    }

    public $onDestroy(): void {
        if (this.contentSubscription) {
            this.contentSubscription.unsubscribe();
        }
    }

    private initEditor(editorClassName: string): void {
        /* Adding container for editor element dynamically, to support customization. */
        // Todo: Remove angular dependency and use section tag
        const editorElement = angular.element(`<div class="editor ${editorClassName}"></div>`);
        this.$element.append(editorElement);

        this.config = merge(this.DefaultQuillConfig, this.config);

        this.quill = new Quill(editorElement[0], this.config);

        if (this.contentObservable) {
            this.contentSubscription = this.contentObservable.subscribe((content: string = ""): void => {
                this.quill.root.innerHTML = this.$sce.getTrustedHtml(content.replace(Regex.REMOVE_SPACE_BETWEEN_TAGS, ">"));
            });
        } else if (this.content) {
            this.quill.root.innerHTML = this.$sce.getTrustedHtml(this.content.replace(Regex.REMOVE_SPACE_BETWEEN_TAGS, ">"));
        }
    }

    private attachEvents = (): void => {
        this.quill.on("text-change", (delta: Delta, oldDelta: Delta, source: string): void => {
            if (source === "user") { // we have to check whether the update was done by "user" or "api" to prevent quill from making an api call in the beginning. "user" is quill's default
                if (this.htmlContent) {
                    this.onContentChange({ data: this.quill.root.innerHTML });
                } else {
                    this.onContentChange({ data: this.quill.container.textContent ? this.quill.root.innerHTML : "" });
                }
                this.$scope.$apply();
            }
        });
    }
}

export default {
    bindings: {
        editorClassName: "<",
        content: "<?",
        contentObservable: "<?",
        config: "<",
        onContentChange: "&",
        htmlContent: "<?",
    },
    controller: OneRichTextEditorController,
    template: () => {
        return `<div></div>`;
    },
};
