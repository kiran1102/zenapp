import { IReactiveDropdownOption } from "interfaces/reactive-dropdown-option.interface";

class ReactiveDropdownController implements ng.IComponentController {
    private leftIconClass: string;
    private rightIconClass: string;
    private hideCaret: boolean;
    private dropdownClass: string;
    private listClass: string;
    private alignRight: boolean;
    private itemClass: string;
    private options: IReactiveDropdownOption[];
    private roundButton: boolean;
    private flatButton: boolean;
    private reactive: boolean;
    private defaultOption: IReactiveDropdownOption;
    private selectedModel: IReactiveDropdownOption;
    private onChange: () => any;
    private placeholder: string;
}

const ReactiveDropdownComponent: ng.IComponentOptions = {
    bindings: {
        alignRight: "<?",
        buttonClass: "@?",
        buttonTextClass: "@?",
        defaultOption: "<",
        dropdownClass: "@?",
        flatButton: "<?",
        itemClass: "@?",
        leftIconClass: "@?",
        listClass: "@?",
        onChange: "&",
        options: "<",
        rightIconClass: "@?",
        roundButton: "<?",
        selectedModel: "<",
        placeholder: "@",
        isDisabled: "<?",
        identifier: "@?",
    },
    controller: ReactiveDropdownController,
    template: `
        <span
            class="one-dropdown {{ ::$ctrl.dropdownClass }} block"
            uib-dropdown
            keyboard-nav
            auto-close="always outsideClick"
        >
            <button
                ng-if="!$ctrl.selectedModel && $ctrl.defaultOption"
                class="one-button one-dropdown__button {{ ::$ctrl.buttonClass }}"
                uib-dropdown-toggle
                ng-class="{'one-dropdown__button--round': $ctrl.roundButton, 'one-dropdown__button--flat': $ctrl.flatButton}"
                ng-disabled="$ctrl.isDisabled"
            >
                <!-- do not single-bind leftIconClass -->
                <i class="media-middle {{$ctrl.leftIconClass}} margin-right-half" ng-if="$ctrl.leftIconClass"></i>
                <span class="one-dropdown__button-text {{$ctrl.buttonTextClass}}">{{$ctrl.defaultOption.text}}</span>
                <span class="one-dropdown__button-icon {{ ::$ctrl.rightIconClass }} fa fa-caret-down" ng-if="::$ctrl.rightIconClass"></span>
            </button>
            <button
                ng-if="!$ctrl.selectedModel && !$ctrl.defaultOption && $ctrl.placeholder"
                class="one-button one-dropdown__button {{ ::$ctrl.buttonClass }}"
                uib-dropdown-toggle
                ng-class="{'one-dropdown__button--round': $ctrl.roundButton, 'one-dropdown__button--flat': $ctrl.flatButton}"
                ng-disabled="$ctrl.isDisabled"
            >
                <!-- do not single-bind leftIconClass -->
                <i class="media-middle {{$ctrl.leftIconClass}} margin-right-half" ng-if="$ctrl.leftIconClass"></i>
                <span class="one-dropdown__button-text color-frost {{$ctrl.buttonTextClass}}">{{$ctrl.placeholder}}</span>
                <span class="one-dropdown__button-icon {{ ::$ctrl.rightIconClass }} fa fa-caret-down" ng-if="::$ctrl.rightIconClass"></span>
            </button>
            <button
                ng-if="$ctrl.selectedModel"
                class="one-button one-dropdown__button {{ ::$ctrl.buttonClass }}"
                uib-dropdown-toggle
                ng-class="{'one-dropdown__button--round': $ctrl.roundButton, 'one-dropdown__button--flat': $ctrl.flatButton}"
                ng-disabled="$ctrl.isDisabled"
            >
                <!-- do not single-bind leftIconClass -->
                <i class="media-middle {{$ctrl.leftIconClass}} margin-right-half" ng-if="$ctrl.leftIconClass"></i>
                <span class="one-dropdown__button-text {{$ctrl.buttonTextClass}}">{{$ctrl.selectedModel.text}}</span>
                <span class="one-dropdown__button-icon {{ ::$ctrl.rightIconClass }} fa fa-caret-down" ng-if="::$ctrl.rightIconClass"></span>
            </button>
            <reactive-dropdown-menu
                identifier="{{$ctrl.identifier}}"
                list-class="{{$ctrl.listClass}}"
                align-right="$ctrl.alignRight"
                item-class="{{$ctrl.itemClass}}"
                on-change="$ctrl.onChange({ value: value })"
                options="$ctrl.options"
            ></reactive-dropdown-menu>
        </span>
    `,
};

export default ReactiveDropdownComponent;
