import _ from "lodash";

class OneRiskTileDropdown implements ng.IComponentController {

    static $inject: any = [];

    private onTileSelect: (arg: any) => any;
    private isOpen: boolean;
    private xAxisDesc = "- - - -";
    private yAxisDesc = "- - - -";

    private tileMap: any[];
    private axisLabels: any;

    private toggled(open): void {
        this.isOpen = open;
        if (!open) {
            this.xAxisDesc = "- - - -";
            this.yAxisDesc = "- - - -";
        }
    }

    private handleTileHover(tile: any): void {
        this.xAxisDesc = `${this.axisLabels.xAxis[tile.xValue - 1]}`;
        this.yAxisDesc = `${this.axisLabels.yAxis[tile.yValue - 1]}`;
    }

    private handleTileLeave(): void {
        this.xAxisDesc = "- - - -";
        this.yAxisDesc = "- - - -";
    }

    private handleTileClick(tile: any): void {
        if (_.isFunction(this.onTileSelect)) {
            this.onTileSelect({ value: tile });
        }
    }
}

const OneRiskTileDropdownComponent: ng.IComponentOptions = {
    controller: OneRiskTileDropdown,
    bindings: {
        onTileSelect: "&",
        axisLabels: "<?",
        tileMap: "<?",
        alignRight: "<?",
    },
    template: `
        <div
            uib-dropdown-menu
            role="menu"
            class="risk-tile-dropdown__menu padding-none"
            ng-class="{ 'dropdown-menu-right': $ctrl.alignRight }"
        >
            <div class="risk-tile-dropdown__desc column-horizontal-vertical-center padding-top-half">
                <div class="width-100-percent padding-left-right-1">
                    <div class="text-center text-bold overflow-ellipsis text-nowrap">
                        {{$ctrl.yAxisDesc}}
                    </div>
                </div>
                <div class="width-100-percent padding-left-right-1">
                    <div class="text-center text-bold overflow-ellipsis text-nowrap">
                        {{$ctrl.xAxisDesc}}
                    </div>
                </div>
            </div>
            <div class="risk-tile-dropdown__content row-nowrap">
                <div class="risk-tile-dropdown__tile-container-padding column-horizontal-vertical-center">
                    <div class="risk-tile-dropdown__y-axis-label text-center">
                        <div class="text-italic overflow-ellipsis text-nowrap">
                            {{$ctrl.axisLabels.yAxisLabel}}
                        </div>
                    </div>
                </div>
                <div class="risk-tile-dropdown__tile-container column-nowrap">
                    <div
                        ng-repeat="row in $ctrl.tileMap"
                        class="flex flex-grow-1"
                    >
                        <div
                            ng-repeat="col in row"
                            class="risk-tile-dropdown__tile flex-grow-1"
                            ng-class="col.htmlClass"
                            ng-mouseover="$ctrl.handleTileHover(col)"
                            ng-mouseleave="$ctrl.handleTileLeave()"
                            ng-click="$ctrl.handleTileClick(col)"
                        >
                        </div>
                    </div>
                </div>
                <div class="risk-tile-dropdown__tile-container-padding"></div>
            </div>
            <div class="risk-tile-dropdown__x-axis-label row-horizontal-vertical-center">
                <div class="text-italic overflow-ellipsis text-nowrap">
                    {{$ctrl.axisLabels.xAxisLabel}}
                </div>
            </div>
        </div>
    `,
};

export default OneRiskTileDropdownComponent;
