import * as _ from "lodash";
import { AssessmentStates } from "constants/assessment.constants";

class PiaProjectTableController  implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "$scope", "ENUMS"];

    private projectStates: any;
    private projectCustomTerm: string;
    private tableData: any[]; // array of row objects
    private columns: any[]; // array of column objects that represent table-columns. use order of elements to order the columns.
    private translate: any;
    private sortColumn: any; // key of the column that is sorted.
    private sortDirection: any; // enum value representing sort direction.
    private sortTypes: any; // enum collection of sort directions.
    private loadingRowIndex: number; // index of the row that is loading from changing the project version.
    private projectIdInProgress: string; // id of project that is currently processing an action. used by project pdf export.
    private actionCallback: (action: any) => any; // callback if child column sends any action.
    private permissions: any;
    private isSelfService: boolean;
    private CheckboxTypes: any;
    private templateData: string; // To check whether template filter is set or not

    constructor($rootScope, readonly $scope: ng.IScope, ENUMS: any) {
        this.translate = $rootScope.t;
        this.sortTypes = ENUMS.SortTypes;
        this.projectStates = AssessmentStates;
        this.projectCustomTerm = $rootScope.customTerms.Project;
        this.CheckboxTypes = ENUMS.CheckboxTypes;
    }

    public $onInit() {
        if (this.isSelfService) {
            this.columns = [
                { text: "ID", sortable: true, sortKey: "number", columnKey: "Number", style: { weight: 4 } },
                { text: this.translate("State"), sortable: true, sortKey: "state", columnKey: "StateName", component: "pia-project-state-label", style: { weight: 10 } },
                { text: this.translate("Name"), sortable: true, sortKey: "name", columnKey: "Name", style: { weight: 10 } },
                { text: this.translate("Type"), sortable: true, sortKey: "templatename", columnKey: "TemplateName", style: { weight: 10 } },
                { text: this.translate("Deadline"), hidden: !this.permissions.canShowProjectsDeadline, sortable: true, sortKey: "deadline", columnKey: "Deadline", component: "pia-project-deadline", style: { weight: 8 } },
            ];
        } else {
            this.columns = [
                { text: "", preventClick: true, sortable: false, columnKey: "Projectcheckbox", component: "one-checkbox", style: { weight: 4 } },
                { text: "ID", sortable: true, sortKey: "number", columnKey: "Number", style: { weight: 4 } },
                { text: this.translate("Risk"), sortable: true, sortKey: "risk", columnKey: "", component: "pia-project-risk-state", style: { weight: 5 } },
                { text: this.translate("Name"), sortable: true, sortKey: "name", columnKey: "Name", component: "pia-project-name", style: { weight: 15 } },
                { text: this.translate("State"), sortable: true, sortKey: "state", columnKey: "StateName", component: "pia-project-state-label", style: { weight: 12 } },
                { text: this.translate("Organization"), sortable: true, sortKey: "organization", columnKey: "OrgGroup", style: { "weight": 10, "overflow": "hidden", "text-overflow": "ellipsis" } },
                { text: this.translate("Creator"), sortable: true, sortKey: "creator", columnKey: "Creator", style: { "weight": 11, "overflow": "hidden", "text-overflow": "ellipsis" } },
                { text: this.translate("Respondent"), sortable: true, sortKey: "lead", columnKey: "Lead", style: { "weight": 11, "word-break": "break-all" } },
                { text: this.translate("Approver"), sortable: true, sortKey: "reviewer", columnKey: "Reviewer", style: { "weight": 11, "overflow": "hidden", "text-overflow": "ellipsis" } },
                { text: this.translate("DateCreated"), defaultSorted: true, defaultSortDirection: 1, sortable: true, sortKey: "created", columnKey: "CreatedDT", component: "pia-project-date", style: { weight: 10 } },
                { text: this.translate("Deadline"), hidden: !this.permissions.canShowProjectsDeadline, sortable: true, sortKey: "deadline", columnKey: "Deadline", component: "pia-project-deadline", style: { weight: 9 } },
                { text: "", preventClick: true, sortable: false, component: "pia-project-buttons", style: { weight: 3 } },
            ];
        }

        this.columns = this.computeColumnWidth(this.columns, "weight");

        if (_.isUndefined(this.sortColumn) && _.isUndefined(this.sortDirection)) {
            const { sortColumn, sortDirection } = this.getDefaultSort(this.columns);
            this.sortColumn = sortColumn;
            this.sortDirection = sortDirection;
        }
    }

    private computeColumnWidth(arr: any[], weight: string): any[] {
        const sumOfWeights: number = _(arr).filter((elem: any): boolean => !elem.hidden)
            .reduce((acc: number, curr: any): number => acc + curr.style[weight], 0);

        return arr.map((elem: any): any => _.assign({}, elem, { style: { ..._.omit(elem.style, "weight"), width: 100 / sumOfWeights * elem.style[weight] + "%" } }));
    }

    private getDefaultSort(columns: any[]): any {
        const res = { sortColumn: "", sortDirection: 0 };
        for (const column of columns) {
            if (column.defaultSorted) {
                res.sortColumn = column.sortKey;
                res.sortDirection = column.defaultSortDirection;
            }
        }
        return res;
    }

    private handleColumnClick(sortColumn: any): void {
        if (sortColumn) {
            if (this.sortColumn !== sortColumn) {
                this.sortColumn = sortColumn;
                this.sortDirection = null;
                this.sortDirection = this.getSortDir(this.sortDirection);
            } else {
                this.sortDirection = this.getSortDir(this.sortDirection);
            }

            if (_.isFunction(this.actionCallback)) {
                this.actionCallback({
                    action: "SORT_COLUMN",
                    payload: {
                        sortColumn: this.sortColumn,
                        sortDirection: this.sortDirection,
                    },
                });
            }
        }
    }

    private getSortDir(sortDirection: number): number {
        if (sortDirection === this.sortTypes.Descending) {
            return this.sortTypes.Ascending;
        } else if (sortDirection === this.sortTypes.Ascending) {
            return this.sortTypes.Descending;
        }
        return this.sortTypes.Ascending;
    }

    private handleRowClick(rowData: any): void {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action: "GO_TO_PROJECT", payload: { rowData } });
        }
    }

    private getVersioningTooltipText(rowData: any): string {
        if (rowData.TemplateState === "Archived") {
            return this.translate("ArchivedTemplateVersioning");
        } else if (rowData.IsLinked) {
            return this.translate("ThresholdQuestionsVersionUnAvailable");
        } else if (rowData.StateDesc === this.projectStates.InProgress) {
            return this.translate("SubmitProjectBeforeCreatingNewVersionDuplicate", { ProjectTerm: this.projectCustomTerm });
        }
        return this.translate("AddNewVersion");
    }

    private getDuplicatingTooltipText(rowData: any): string {
        if (rowData.TemplateState === "Archived") {
            return this.translate("DuplicatingProjectUnavailableArchived", { Project: this.projectCustomTerm });
        } else if (rowData.IsLinked) {
            return this.translate("DuplicatingProjectUnavailableThreshold", { Project: this.projectCustomTerm });
        }
        return this.translate("AddNewVersion");
    }

    private sendAction(action: string, payload: any): void {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action, payload });
        }
    }
}

export default {
    bindings: {
        tableData: "<?",
        sortColumn: "<?",
        sortDirection: "<?",
        loadingRowIndex: "<?",
        projectIdInProgress: "<?",
        actionCallback: "&?",
        permissions: "<?",
        isSelfService: "<?",
        templateData: "<?",
    },
    controller: PiaProjectTableController,
    template: `
        <section
            data-id="piaProjectTableSection"
            class="pia-project-table"
        >
            <one-table
                data-id="piaProjectTable"
                header-class="width-100-percent"
                body-class="width-100-percent background-white"
            >
                <one-table-header data-id="piaProjectTableHeader">
                    <one-table-row
                        data-id="piaProjectTableRow"
                        row-class="pia-project-table__row--header"
                    >
                        <one-table-header-cell
                            data-id="piaProjectTableHeaderCell"
                            class="focus-outline-0"
                            ng-repeat="column in $ctrl.columns track by $index"
                            ng-if="!column.hidden"
                            ng-click="$ctrl.handleColumnClick(column.sortKey)"
                            ng-style="column.style"
                            cell-class="pia-project-table__cell"
                            is-sorted="column.sortable && $ctrl.sortColumn === column.sortKey"
                            sort-direction="$ctrl.sortDirection"
                            wrapper-class="{{column.columnKey === 'Name' && !$ctrl.isSelfService ? 'text-left padding-left-2' : ''}}"
                        >
                            <span ng-if="column.text">{{column.text}}</span>
                        </one-table-header-cell>
                    </one-table-row>
                </one-table-header>
                <one-table-body data-id="piaProjectTableBody">
                    <one-table-row
                        data-id="piaProjectTableBodyRow"
                        ng-repeat="rowData in $ctrl.tableData track by rowData.Id"
                        ng-init="rowIndex = $index"
						row-class="pia-project-table__row transition-opacity-0p3-linear {{rowData.isSelected ? 'pia-project-table__row-selected' : ''}}"
                        is-disabled="rowData.isDisabled"
                        loading-class="{{$index === $ctrl.loadingRowIndex ? 'opacity-0' : ''}}"
                    >
                        <div
                            data-id="piaProjectTableBodyCell"
                            ng-repeat="column in $ctrl.columns track by $index"
                            ng-if="!column.hidden"
                            ng-click="column.preventClick || $ctrl.handleRowClick(rowData)"
							class="pia-project-table__row-cell row-horizontal-vertical-center focus-outline-0"
                            ng-class="{ 'padding-top-bottom-1 padding-left-right-half': !$last, 'padding-all-0': $last }"
                            ng-style="column.style"
                        >
                            <span
                                data-id="piaProjectTableBodyCellContentText"
                                class="text-center overflow-ellipsis-inherit word-break-inherit"
                                ng-if="!column.component && column.columnKey"
                            >
                                    {{rowData[column.columnKey]}}
                            </span>
                            <pia-project-name
                                data-id="piaProjectTableBodyCellContentNameComponent"
                                class="width-100-percent overflow-ellipsis"
                                ng-if="column.component === 'pia-project-name'"
                                row-data="rowData"
                                action-callback="$ctrl.sendAction(action, payload)">
                            </pia-project-name>
                            <pia-project-risk-state
                                data-id="piaProjectTableBodyCellContentRiskStateComponent"
                                class="text-center overflow-ellipsis"
                                ng-if="column.component === 'pia-project-risk-state'"
                                row-data="rowData">
                            </pia-project-risk-state>
                            <pia-project-state-label
                                data-id="piaProjectTableBodyCellContentStateLabelComponent"
                                class="text-center overflow-ellipsis"
                                ng-if="column.component === 'pia-project-state-label'"
                                row-data="rowData">
                            </pia-project-state-label>
                          	<one-date
                                data-id="piaProjectTableBodyCellContentDateComponent"
                                class="text-center overflow-ellipsis"
                                ng-if="column.component === 'pia-project-date'"
                                date-to-format="rowData.CreatedDT"
                                enable-tooltip="true">
                            </one-date>
                            <pia-project-deadline
                                data-id="piaProjectTableBodyCellContentDeadlineComponent"
                                class="text-center overflow-ellipsis"
                                ng-if="column.component === 'pia-project-deadline'"
                                row-data="rowData">
                            </pia-project-deadline>
                            <pia-project-buttons
                                data-id="piaProjectTableBodyCellContentButtonsComponent"
                                class="row-horizontal-vertical-center width-100-percent height-100"
                                ng-if="column.component === 'pia-project-buttons'"
                                row-data="rowData"
                                display-spinner="rowData.Id === $ctrl.projectIdInProgress"
                                action-callback="$ctrl.sendAction(action, payload)"
                                tagging-enabled="$ctrl.permissions.taggingEnabled && rowData.IsLatestVersion"
                                duplicating-enabled="$ctrl.permissions.canDuplicateProject"
                                duplicating-deactivated="!rowData.canBeDuplicated"
                                duplicating-tooltip-text="{{$ctrl.getDuplicatingTooltipText(rowData)}}"
                                versioning-enabled="$ctrl.permissions.versioningEnabled &&
                                                    rowData.IsLatestVersion"
                                versioning-deactivated="rowData.StateDesc === $ctrl.projectStates.InProgress ||
                                                    rowData.TemplateState === 'Archived' ||
                                                    rowData.IsLinked"
                                versioning-tooltip-text="{{$ctrl.getVersioningTooltipText(rowData)}}"
                                permissions="$ctrl.permissions"
                            >
                            </pia-project-buttons>
                        </div>
                    </one-table-row>
                </one-table-body>
            </one-table>
        </section>
    `,
};
