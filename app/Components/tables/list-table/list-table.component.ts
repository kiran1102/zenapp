/*
    List table is a table that is HTML <table> element based. rows are <tr> elements and cells are <td> elements
*/

export default {
    bindings: {
        tableClass: "@",
    },
    transclude: {
        theadTr: "theadTr",
        tbodyTr: "tbodyTr",
    },
    template: `
        <table class="full-width {{$ctrl.tableClass}}">
            <thead>
                <tr ng-transclude-replace="theadTr"></tr>
            </thead>
            <tbody>
                 <tr ng-transclude-replace="tbodyTr"></tr>
            </tbody>
        </table>
    `,
};
