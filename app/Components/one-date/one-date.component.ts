import { TimeStamp } from "oneServices/time-stamp.service";

class OneDateController implements ng.IComponentController {
    static $inject: string[] = ["TimeStamp"];

    private dateToFormat: string;
    private enableTooltip: boolean;
    private tooltipDate: string;
    private dateTooltipPlacement: string;
    private isDateValid: boolean;
    private formattedDate: string;

    constructor(
        private readonly TimeStamp: TimeStamp,
    ) {}

    public $onChanges(changes: ng.IOnChangesObject): void {
        this.formattedDate = this.TimeStamp.formatDate(this.dateToFormat);
        this.isDateValid = this.TimeStamp.isValidDate(this.dateToFormat);

        if (this.enableTooltip) {
            this.tooltipDate = this.TimeStamp.formatDate(this.dateToFormat);
        }
    }
}

const OneDateComponent: ng.IComponentOptions = {
    controller: OneDateController,
    bindings: {
        dateToFormat: "<",
        enableTooltip: "<?",
        dateTooltipPlacement: "@?",
        fallBackToValue: "<?",
    },
    template: `
        <span
            ng-if="$ctrl.dateToFormat && $ctrl.isDateValid"
            uib-tooltip='{{::$ctrl.tooltipDate}}'
            tooltip-placement="{{::$ctrl.dateTooltipPlacement}}"
            tooltip-trigger='mouseenter'
            tooltip-enable="{{::$ctrl.enableTooltip}}"
            >
                {{$ctrl.formattedDate}}
        </span>
        <span ng-if="!$ctrl.isDateValid">
            {{ $ctrl.fallBackToValue ? $ctrl.dateToFormat : "- - - -" }}
        </span>
    `,
};

export default OneDateComponent;
