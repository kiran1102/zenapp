export default {
    bindings: {
        color: "@?",
        backgroundColor: "@?", // needed to change the background color when the tag is rounded
        labelClass: "@?",
        plainText: "@?",
        size: "@?",
        translatedText: "@?",
        iconClass: "@?",
        icon: "@?",
        wrapperClass: "@?",
    },
    template: `
        <span
            title="{{$root.t($ctrl.translatedText) || $ctrl.plainText}}"
            class="
                one-tag
                text-uppercase
                text-center
                inline-block
                overflow-ellipsis-nowrap
                {{$ctrl.labelClass ? $ctrl.labelClass : 'one-tag--standard'}}
                {{$ctrl.wrapperClass}}
                {{::$ctrl.size ? 'one-tag--' + $ctrl.size : 'one-tag--small'}}
            "
            data-ng-style="{'color': $ctrl.color, 'background-color': $ctrl.backgroundColor}">
            <i
                ng-if="$ctrl.icon"
                ng-class="[$ctrl.icon, $ctrl.iconClass]">
            </i>
            {{$root.t($ctrl.translatedText) || $ctrl.plainText}}
        </span>
    `,
};
