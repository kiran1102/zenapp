class OnePopupTableController implements ng.IComponentController {

    static $inject: string[] = ["$q", "$timeout", "ENUMS"];

    private ready = false;
    private hidePopup = false;
    private timer: any;
    private defaultPopupDelay: number;
    private popupIsOpen = false;
    private promiseArray: any[] = [];

    private popupAppendToBody = true;
    private popupClass: string;
    private popupDelay: number;
    private popupPlacement = "bottom";
    private popupTrigger: string;

    private popupColumns: any[];
    private popupRows: any[];
    private popupColumnsPromise: any;
    private popupRowsPromise: any;
    private popupColumnsOrder: string;

    constructor(private readonly $q: ng.IQService, private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
        this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
    }

    public $onDestroy(): void {
        if (this.timer) this.cancelToggleTimer();
    }

    private init(): void {
        if (!this.ready) {
            if (this.popupColumnsPromise) this.promiseArray.push(this.handleColumnsPromise());
            if (this.popupRowsPromise) this.promiseArray.push(this.handleRowsPromise());

            if (this.promiseArray.length) {
                this.$q.all(this.promiseArray).then((): void => {
                    this.toggleReady();
                });
            } else {
                this.toggleReady();
            }
        }
    }

    private startToggleTimer(): void {
        if (this.timer) this.cancelToggleTimer();
        this.timer = this.$timeout(
            (): void => {
                this.togglePopup();
            },
            (this.popupDelay || this.defaultPopupDelay),
        );
    }

    private conditionallyToggleTimer(): void {
        if (this.popupIsOpen) {
            this.startToggleTimer();
        } else {
            this.cancelToggleTimer();
        }
    }

    private cancelToggleTimer(): void {
        this.$timeout.cancel(this.timer);
    }

    private togglePopup(): void {
        this.popupIsOpen = !this.popupIsOpen;
    }

    private toggleReady(): void {
        this.hidePopup = !(this.popupColumns.length && this.popupRows.length);
        this.ready = true;
    }

    private handleColumnsPromise(): void {
        return this.popupColumnsPromise().then((columns: any[]): void => {
            this.popupColumns = columns || [];
        });
    }

    private handleRowsPromise(): void {
        return this.popupRowsPromise().then((rows: any[]): void => {
            this.popupRows = rows || [];
        });
    }

}

const OnePopupTableTemplate = `
    <span
        ng-if="!($ctrl.popupTitle || ($ctrl.popupColumns || $ctrl.popupColumnsPromise) || ($ctrl.popupRows || $ctrl.popupRowsPromise)) || $ctrl.hidePopup"
        ng-transclude
        >
    </span>
    <span
        ng-if="($ctrl.popupTitle || ($ctrl.popupColumns || $ctrl.popupColumnsPromise) || ($ctrl.popupRows || $ctrl.popupRowsPromise)) && !$ctrl.hidePopup"
        uib-popover-template='"one-popup-table.html"'
        popover-placement="{{::$ctrl.popupPlacement}}"
        popover-trigger="{{::$ctrl.popupTrigger}}"
        popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
        popover-class="one-popup one-popup-table {{::$ctrl.popupClass}}"
        popover-popup-delay="{{::$ctrl.popupDelay}}"
        popover-is-open="$ctrl.popupIsOpen"
        ng-mouseenter="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
        ng-mouseleave="($ctrl.popupTrigger || $ctrl.conditionallyToggleTimer())"
        ng-transclude
        >
    </span>
    <script type="text/ng-template" id="one-popup-table.html">
        <div
            class="one-popup__body overflow-y-auto"
            ng-init="$ctrl.init()"
            one-click-outside="(!$ctrl.popupTrigger && $ctrl.popupIsOpen)"
            one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
            ng-mouseenter="($ctrl.popupTrigger || $ctrl.cancelToggleTimer())"
            ng-mouseleave="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
            >
            <loading ng-if="!$ctrl.ready" show-text="false"></loading>
            <one-table ng-if="$ctrl.ready">
                <one-table-header>
                    <one-table-row>
                        <one-table-header-cell
                            ng-repeat="col in $ctrl.popupColumns | orderBy: $ctrl.popupColumnsOrder track by $index"
                            width="{{::col.settings.width}}"
                            left-arrow="col.hasParent"
                            right-arrow="col.hasChild"
                            >
                            {{::col.name}}
                        </one-table-header-cell>
                    </one-table-row>
                </one-table-header>
                <one-table-body>
                    <one-table-row ng-repeat="row in $ctrl.popupRows track by $index">
                        <one-table-cell ng-repeat="col in $ctrl.popupColumns | orderBy: $ctrl.popupColumnsOrder track by $index" width="col.settings.width">
                            <one-table-cell-content>
                                {{row.value}}
                            </one-table-cell-content>
                        </one-table-cell>
                    </one-table-row>
                </one-table-body>
            </one-table>
        </div>
    </script>
`;

const OnePopupTableComponent: ng.IComponentOptions = {
    controller: OnePopupTableController,
    template: OnePopupTableTemplate,
    transclude: true,
    bindings: {
        popupAppendToBody: "<?",
        popupClass: "@?",
        popupDelay: "<?",
        popupPlacement: "@?",
        popupTrigger: "@?",
        popupColumns: "<?",
        popupColumnsPromise: "<?",
        popupColumnsOrder: "@?",
        popupRows: "<?",
        popupRowsPromise: "<?",
    },
};

export default OnePopupTableComponent;
