import * as _ from "lodash";

class OnePopupToggleController implements ng.IComponentController {

    static $inject: string[] = ["$scope", "$timeout", "ENUMS"];

    private ready = false;
    private hidePopup = false;
    private timer: any;
    private defaultPopupDelay: number;
    private popupIsOpen = false;
    private callback: Function;

    private popupAppendToBody = false;
    private popupClass: string;
    private popupDelay: number;
    private popupPlacement = "top-right";
    private popupTrigger: string;

    private popupTitle: string;
    private popupContent: string;
    private popupContentPromise: any;

    constructor(readonly $scope: any, private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
        this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
    }

    public $onDestroy(): void {
        if (this.timer) this.cancelToggleTimer();
    }

    private init(): void {
        if (this.popupContentPromise) {
            this.popupContentPromise().then(
                (content: any): void => {
                    this.popupContent = content;
                    this.ready = true;
                },
            );
        } else {
            this.ready = true;
        }
    }

    private startToggleTimer(): void {
        if (this.timer) this.cancelToggleTimer();
        this.timer = this.$timeout(
            (): void => {
                this.togglePopup();
            },
            (this.popupDelay || this.defaultPopupDelay),
        );
    }

    private conditionallyToggleTimer(): void {
        if (this.popupIsOpen) {
            this.startToggleTimer();
        } else {
            this.cancelToggleTimer();
        }
    }

    private cancelToggleTimer(): void {
        this.$timeout.cancel(this.timer);
    }

    private togglePopup(): void {
        this.popupIsOpen = !this.popupIsOpen;
        this.callback(this.popupIsOpen);
    }

    private handleAction(option: any): void {
        if (_.isFunction(option.action)) {
            option.action(option);
        }
    }
}

const OnePopupToggleComponent: ng.IComponentOptions = {
    bindings: {
        popupAppendToBody: "<?",
        popupClass: "@?",
        popupDelay: "<?",
        popupPlacement: "@?",
        popupTrigger: "@?",
        popupTitle: "@?",
        popupContent: "<?",
        popupContentPromise: "<?",
        options: "<",
        callback: "&?",
    },
    controller: OnePopupToggleController,
    transclude: true,
    template:
        `
            <span
                ng-if="$ctrl.hidePopup"
                ng-transclude
                >
            </span>
            <span
                ng-if="!$ctrl.hidePopup"
                uib-popover-template='"one-popup-toggle.html"'
                popover-placement="{{::$ctrl.popupPlacement}}"
                popover-trigger="{{::$ctrl.popupTrigger}}"
                popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
                popover-class="one-popup one-popup-toggle padding-all-0 border-none shadow6 {{::$ctrl.popupClass}}"
                popover-popup-delay="{{::$ctrl.popupDelay}}"
                popover-is-open="$ctrl.popupIsOpen"
                ng-transclude
                >
            </span>
            <script type="text/ng-template" id="one-popup-toggle.html">
                <div
                    ng-init="$ctrl.init()"
                    one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
                    >
                    <div
                        ng-if="$ctrl.popupTitle"
                        class="text-bold one-popup__item one-popup-toggle__item--head one-popup__item--separated"
                        ng-bind="$ctrl.popupTitle"
                        >
                    </div>
                        <button
                            data-id="togglePopoverClose"
                            class="one-popup__close static-horizontal text-large"
                            ng-click="$ctrl.togglePopup()"
                            >
                            <i class="ot ot-times"></i>
                        </button>
                    <loading ng-if="!$ctrl.ready" show-text="false"></loading>
                    <div
                        class="label-wrapper"
                        ng-class="$ctrl.popupTitle ? '' : 'margin-top-3'"
                        >
                        <one-label-content
                            ng-repeat="option in $ctrl.options track by $index"
                            legend-color="{{option.legendColor ? option.legendColor : ''}}"
                            inline="true"
                            split="true"
                            name="{{::option.title}}"
                            wrapper-class="margin-bottom-0 padding-all-1 border-bottom"
                            label-class="text-thin-2"
                            body-class="webform-content__input row-flex-end"
                            >
                            <downgrade-switch-toggle
                                [toggle-state]="option.value"
                                (callback)="$ctrl.handleAction(option)"
                                >
                            </downgrade-switch-toggle>
                        </one-label-content>
                    </div>
                </div>
            </script>
        `,
};

export default OnePopupToggleComponent;
