
    class OnePopupController implements ng.IComponentController {

        static $inject: string[] = ["$timeout", "ENUMS"];

        private ready = false;
        private hidePopup = false;
        private timer: any;
        private defaultPopupDelay: number;
        private popupIsOpen = false;

        private popupAppendToBody = true;
        private popupClass: string;
        private popupDelay: number;
        private popupPlacement = "bottom";
        private popupTrigger: string;

        private popupTitle: string;
        private popupContent: string;
        private popupContentPromise: any;

        constructor(private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
            this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
        }

        public $onDestroy(): void {
            if (this.timer) this.cancelToggleTimer();
        }

        private init(): void {
            if (this.popupContentPromise) {
                this.popupContentPromise().then(
                    (content) => {
                        this.popupContent = content;
                        this.hidePopup = !(this.popupContent);
                        this.ready = true;
                    },
                );
            } else {
                this.hidePopup = !(this.popupContent);
                this.ready = true;
            }
        }

        private startToggleTimer(): void {
            if (this.timer) this.cancelToggleTimer();
            this.timer = this.$timeout(
                (): void => {
                    this.togglePopup();
                },
                (this.popupDelay || this.defaultPopupDelay),
            );
        }

        private conditionallyToggleTimer(): void {
            if (this.popupIsOpen) {
                this.startToggleTimer();
            } else {
                this.cancelToggleTimer();
            }
        }

        private cancelToggleTimer(): void {
            this.$timeout.cancel(this.timer);
        }

        private togglePopup(): void {
            this.popupIsOpen = !this.popupIsOpen;
        }

    }

    const OnePopupTemplate = `
        <span
            ng-if="!($ctrl.popupTitle || ($ctrl.popupContent || $ctrl.popupContentPromise)) || $ctrl.hidePopup"
            ng-transclude
            >
        </span>
        <span
            ng-if="($ctrl.popupTitle || ($ctrl.popupContent || $ctrl.popupContentPromise)) && !$ctrl.hidePopup"
            uib-popover-template='"one-popup.html"'
            popover-placement="{{::$ctrl.popupPlacement}}"
            popover-trigger="{{::$ctrl.popupTrigger}}"
            popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
            popover-class="one-popup {{::$ctrl.popupClass}}"
            popover-popup-delay="{{::$ctrl.popupDelay}}"
            popover-is-open="$ctrl.popupIsOpen"
            ng-mouseenter="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
            ng-mouseleave="($ctrl.popupTrigger || $ctrl.conditionallyToggleTimer())"
            ng-transclude
            >
        </span>
        <script type="text/ng-template" id="one-popup.html">
            <div
                ng-init="$ctrl.init()"
                one-click-outside="(!$ctrl.popupTrigger && $ctrl.popupIsOpen)"
                one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
                ng-mouseenter="($ctrl.popupTrigger || $ctrl.cancelToggleTimer())"
                ng-mouseleave="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
                >
                <div
                    ng-if="$ctrl.popupTitle"
                    class="text-bold one-popup__item one-popup__item--head one-popup__item--separated"
                    ng-bind="$ctrl.popupTitle"
                    >
                </div>
                <loading ng-if="!$ctrl.ready" show-text="false"></loading>
                <div
                    class="one-popup__body overflow-y-auto margin-top-bottom-1"
                    ng-if="$ctrl.ready && $ctrl.popupContent"
                    ng-bind="$ctrl.popupContent"
                    >
                </div>
            </div>
        </script>
    `;

    const OnePopupComponent: ng.IComponentOptions = {
        controller: OnePopupController,
        template: OnePopupTemplate,
        transclude: true,
        bindings: {
            popupAppendToBody: "<?",
            popupClass: "@?",
            popupDelay: "<?",
            popupPlacement: "@?",
            popupTrigger: "@?",
            popupTitle: "@?",
            popupContent: "<?",
            popupContentPromise: "<?",
        },
    };

    export default OnePopupComponent;
