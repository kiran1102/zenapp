class OnePopupListController implements ng.IComponentController {

    static $inject: string[] = ["$timeout", "ENUMS"];

    private SortTypes: any;

    private ready = false;
    private hidePopup = false;
    private timer: any;
    private defaultPopupDelay: number;
    private popupIsOpen = false;

    private popupAppendToBody = true;
    private popupClass: string;
    private popupDelay: number;
    private popupPlacement = "bottom";
    private popupTrigger: string;

    private popupTitle: string;
    private popupList: any[];
    private popupListPromise: any;

    private orderBy: number;

    constructor(private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
        this.SortTypes = ENUMS.SortTypes;
        this.orderBy = this.SortTypes.Descending;
        this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
    }

    public $onDestroy(): void {
        if (this.timer) this.cancelToggleTimer();
    }

    private init(): void {
        if (this.popupListPromise) {
            this.popupListPromise().then(
                (options) => {
                    this.popupList = options;
                    this.hidePopup = !(this.popupList.length);
                    this.ready = true;
                },
            );
        } else {
            this.hidePopup = !(this.popupList.length);
            this.ready = true;
        }
    }

    private startToggleTimer(): void {
        if (this.timer) this.cancelToggleTimer();
        this.timer = this.$timeout(
            (): void => {
                this.togglePopup();
            },
            (this.popupDelay || this.defaultPopupDelay),
        );
    }

    private conditionallyToggleTimer(): void {
        if (this.popupIsOpen) {
            this.startToggleTimer();
        } else {
            this.cancelToggleTimer();
        }
    }

    private cancelToggleTimer(): void {
        this.$timeout.cancel(this.timer);
    }

    private togglePopup(): void {
        this.popupIsOpen = !this.popupIsOpen;
    }

    private toggleSortBy(): number {
        if (!this.popupList.length || this.popupList.length < 2) {
            return this.SortTypes.None;
        } else if (this.orderBy === this.SortTypes.Descending) {
            this.orderBy = this.SortTypes.Ascending;
        } else {
            this.orderBy = this.SortTypes.Descending;
        }
        return this.orderBy;
    }

    private listSortBy(): string {
        return (this.orderBy === this.SortTypes.Descending) ? "+text" : "-text";
    }

}

const OnePopupListTemplate = `
    <span
        ng-if="!($ctrl.popupTitle || ($ctrl.popupList || $ctrl.popupListPromise)) || $ctrl.hidePopup"
        ng-transclude
        >
    </span>
    <span
        ng-if="($ctrl.popupTitle || ($ctrl.popupList || $ctrl.popupListPromise)) && !$ctrl.hidePopup"
        uib-popover-template='"one-popup-list.html"'
        popover-placement="{{::$ctrl.popupPlacement}}"
        popover-trigger="{{::$ctrl.popupTrigger}}"
        popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
        popover-class="one-popup {{::$ctrl.popupClass}}"
        popover-popup-delay="{{::$ctrl.popupDelay}}"
        popover-is-open="$ctrl.popupIsOpen"
        ng-mouseenter="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
        ng-mouseleave="($ctrl.popupTrigger || $ctrl.conditionallyToggleTimer())"
        ng-transclude
        >
    </span>
    <script type="text/ng-template" id="one-popup-list.html">
        <div
            ng-init="$ctrl.init()"
            one-click-outside="(!$ctrl.popupTrigger && $ctrl.popupIsOpen)"
            one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
            ng-mouseenter="($ctrl.popupTrigger || $ctrl.cancelToggleTimer())"
            ng-mouseleave="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
            >
            <loading ng-if="!$ctrl.ready" show-text="false"></loading>
            <ul
                ng-if="$ctrl.ready"
                class="one-popup__list margin-none overflow-ellipses overflow-y-auto"
                >
                <li ng-if="$ctrl.popupTitle"
                    class="text-bold one-popup__item one-popup__item--head one-popup__item--separated"
                    >
                    <span
                        class="one-popup__text"
                        ng-click="$ctrl.toggleSortBy()"
                        >
                        {{::$ctrl.popupTitle}}
                        <i
                            class="one-popup__order"
                            ng-class="{
                                'fa fa-caret-up': $ctrl.orderBy === $ctrl.SortTypes.Ascending,
                                'fa fa-caret-down': $ctrl.orderBy === $ctrl.SortTypes.Descending,
                            }"
                            >
                        </i>
                    </span>
                </li>
                <one-configurable-item
                    ng-if="$ctrl.popupList"
                    ng-repeat="item in $ctrl.popupList | orderBy:$ctrl.listSortBy() track by $index"
                    item="item"
                    item-separated="true"
                    item-first="$first"
                    item-last="$last"
                    item-index="$index"
                    >
                </one-configurable-item>
            </ul>
        </div>
    </script>
`;

const OnePopupListComponent: ng.IComponentOptions = {
    controller: OnePopupListController,
    template: OnePopupListTemplate,
    transclude: true,
    bindings: {
        popupAppendToBody: "<?",
        popupClass: "@?",
        popupDelay: "<?",
        popupPlacement: "@?",
        popupTrigger: "@?",
        popupTitle: "@?",
        popupList: "<?",
        popupListPromise: "<?",
    },
};

export default OnePopupListComponent;
