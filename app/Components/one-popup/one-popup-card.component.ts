class OnePopupCardController implements ng.IComponentController {

    static $inject: string[] = ["$timeout", "ENUMS"];

    private ready = false;
    private hidePopup = false;
    private timer: any;
    private defaultPopupDelay: number;
    private popupIsOpen = false;

    private popupAppendToBody = true;
    private popupClass: string;
    private popupDelay: number;
    private popupPlacement = "bottom-left";
    private popupTrigger: string;

    private popupCardList: any[];
    private popupCardListPromise: any;

    constructor(private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
        this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
    }

    public $onDestroy(): void {
        if (this.timer) this.cancelToggleTimer();
    }

    private init(): void {
        if (this.popupCardListPromise) {
            this.popupCardListPromise().then((content: any[]): void => {
                this.popupCardList = content;
                this.hidePopup = !(this.popupCardList);
                this.ready = true;
            });
        } else {
            this.hidePopup = !(this.popupCardList);
            this.ready = true;
        }
    }

    private startToggleTimer(): void {
        if (this.timer) this.cancelToggleTimer();
        this.timer = this.$timeout(
            (): void => {
                this.togglePopup();
            },
            (this.popupDelay || this.defaultPopupDelay),
        );
    }

    private conditionallyToggleTimer(): void {
        if (this.popupIsOpen) {
            this.startToggleTimer();
        } else {
            this.cancelToggleTimer();
        }
    }

    private cancelToggleTimer(): void {
        this.$timeout.cancel(this.timer);
    }

    private togglePopup(): void {
        this.popupIsOpen = !this.popupIsOpen;
    }

}

const OnePopupCardTemplate = `
    <span
        ng-if="$ctrl.hidePopup"
        ng-transclude
        >
    </span>
    <span
        ng-if="!$ctrl.hidePopup"
        uib-popover-template='"one-popup.html"'
        popover-placement="{{::$ctrl.popupPlacement}}"
        popover-trigger="{{::$ctrl.popupTrigger}}"
        popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
        popover-class="one-popup one-popup-card {{::$ctrl.popupClass}}"
        popover-popup-delay="{{::$ctrl.popupDelay}}"
        popover-is-open="$ctrl.popupIsOpen"
        ng-mouseenter="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
        ng-mouseleave="($ctrl.popupTrigger || $ctrl.conditionallyToggleTimer())"
        ng-transclude
        >
    </span>
    <script type="text/ng-template" id="one-popup.html">
        <div
            ng-init="$ctrl.init()"
            one-click-outside="(!$ctrl.popupTrigger && $ctrl.popupIsOpen)"
            one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
            ng-mouseenter="($ctrl.popupTrigger || $ctrl.cancelToggleTimer())"
            ng-mouseleave="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
            >
            <loading ng-if="!$ctrl.ready" show-text="false" loading-class="one-popup-card__loading"></loading>
            <div
                ng-if="$ctrl.ready"
                ng-repeat="list in $ctrl.popupCardList track by $index"
                >
                <div
                    class="
                        text-bold
                        one-popup__item
                        one-popup__item--head
                        one-popup__item--separated
                        one-popup-card__title
                    "
                    ng-bind="list.text"
                >
                </div>
                <ul class="one-popup__list one-popup-card__list row-wrap overflow-ellipses overflow-y-auto">
                    <one-configurable-item
                        ng-if="::list.items"
                        class="
                            one-popup-card__item
                            {{ ::(item.modifierClass ? 'one-popup-card__item--'+item.modifierClass : '') }}
                        "
                        ng-repeat="item in list.items track by $index"
                        item="item"
                        item-class="one-popup-card__item--padding-min"
                        item-label-class="one-popup-card__label"
                        item-text-class="one-popup-card__text"
                        item-inline="true",
                        item-hide-description="true"
                        >
                    </one-configurable-item>
                </ul>
            </div>
        </div>
    </script>
`;

const OnePopupCardComponent: ng.IComponentOptions = {
    controller: OnePopupCardController,
    template: OnePopupCardTemplate,
    transclude: true,
    bindings: {
        popupAppendToBody: "<?",
        popupClass: "@?",
        popupDelay: "<?",
        popupPlacement: "@?",
        popupTrigger: "@?",
        popupCardList: "<?",
        popupCardListPromise: "<?",
    },
};

export default OnePopupCardComponent;
