class OnePopupSearchCtrl implements ng.IComponentController {

    static $inject: string[] = ["$timeout", "ENUMS"];

    private SortTypes: any;
    private hidePopup = false;
    private timer: any;
    private defaultPopupDelay: number;
    private popupIsOpen = false;
    private popupAppendToBody = true;
    private popupClass: string;
    private popupDelay: number;
    private popupPlacement = "bottom";
    private popupTrigger: string;
    private popupTitle: string;
    private canSubmit: boolean;
    private searchConfig: any;
    private selectConfig: any;
    private clearConfig: any;
    private buttonConfig: any;

    constructor(private readonly $timeout: ng.ITimeoutService, ENUMS: any) {
        this.defaultPopupDelay = ENUMS.TransitionTimes.PopupDelay;
    }

    $onInit() {
        this.canSubmit = this.checkSubmitStatus();
    }

    public $onDestroy(): void {
        if (this.timer) this.cancelToggleTimer();
    }

    private startToggleTimer(): void {
        if (this.timer) this.cancelToggleTimer();
        this.timer = this.$timeout(
            (): void => {
                this.togglePopup();
            },
            (this.popupDelay || this.defaultPopupDelay),
        );
    }

    private conditionallyToggleTimer(): void {
        if (this.popupIsOpen) {
            this.startToggleTimer();
        } else {
            this.cancelToggleTimer();
        }
    }

    private cancelToggleTimer(): void {
        this.$timeout.cancel(this.timer);
    }

    private togglePopup(): void {
        this.popupIsOpen = !this.popupIsOpen;
    }

    private handleSelection(selection: any): void {
        const formData: any = {
            selection,
            search: this.searchConfig ? this.searchConfig.searchTerm : null,
        };
        if (this.selectConfig.handleSelection) {
            this.selectConfig.handleSelection(formData);
        }
        this.canSubmit = this.checkSubmitStatus();
    }

    private handleSearch(search: string): void {
        const formData: any = {
            selection: this.selectConfig ? this.selectConfig.selection : null,
            search,
        };
        this.searchConfig.searchTerm = search;
        if (this.searchConfig && this.searchConfig.handleSearch) {
            this.searchConfig.handleSearch(formData);
        }
        this.canSubmit = this.checkSubmitStatus();
    }

    private handleButtonClick(): void {
        const formData: any = {
            selection: this.selectConfig ? this.selectConfig.selection : null,
            search: this.searchConfig ? this.searchConfig.searchTerm : null,
        };
        if (this.buttonConfig && this.buttonConfig.handleClick) {
            this.buttonConfig.handleClick(formData);
        }
        if (this.buttonConfig.closeOnClick) {
            this.popupIsOpen = false;
        }
    }

    private handleClear(): void {
        if (this.searchConfig) this.searchConfig.searchTerm = "";
        if (this.selectConfig) this.selectConfig.selection = "";
        if (this.clearConfig && this.clearConfig.handleClick) {
            this.clearConfig.handleClick();
        }
        if (this.clearConfig.closeOnClick) {
            this.popupIsOpen = false;
        }
        this.canSubmit = this.checkSubmitStatus();
    }

    private checkSubmitStatus(): boolean {
        let canSubmit = true;
        if (this.searchConfig && this.searchConfig.required && !this.searchConfig.searchTerm) {
            canSubmit = false;
        }
        if (this.selectConfig && this.selectConfig.required && !this.selectConfig.selection) {
            canSubmit = false;
        }
        return canSubmit;
    }

}

const OnePopupSearch: ng.IComponentOptions = {
    controller: OnePopupSearchCtrl,
    transclude: true,
    bindings: {
        popupAppendToBody: "<?",
        popupClass: "@?",
        popupDelay: "<?",
        popupPlacement: "@?",
        popupTrigger: "@?",
        popupTitle: "@?",
        searchConfig: "<",
        selectConfig: "<?",
        buttonConfig: "<?",
        clearConfig: "<?",
    },
    template: `
        <span
            uib-popover-template='"one-popup-search.html"'
            popover-placement="{{::$ctrl.popupPlacement}}"
            popover-trigger="{{::$ctrl.popupTrigger}}"
            popover-append-to-body="{{::$ctrl.popupAppendToBody}}"
            popover-class="one-popup padding-top-1 padding-bottom-2 {{::$ctrl.popupClass}}"
            popover-popup-delay="{{::$ctrl.popupDelay}}"
            popover-is-open="$ctrl.popupIsOpen"
            ng-mouseenter="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
            ng-mouseleave="($ctrl.popupTrigger || $ctrl.conditionallyToggleTimer())"
            ng-transclude
            >
        </span>
        <script type="text/ng-template" id="one-popup-search.html">
            <div
                one-click-outside="(!$ctrl.popupTrigger && $ctrl.popupIsOpen)"
                one-click-action="$ctrl.togglePopup(); $ctrl.cancelToggleTimer()"
                ng-mouseenter="($ctrl.popupTrigger || $ctrl.cancelToggleTimer())"
                ng-mouseleave="($ctrl.popupTrigger || $ctrl.startToggleTimer())"
                >
                <one-label-content
                    ng-if="$ctrl.selectConfig"
                    name="{{::$ctrl.selectConfig.fieldLabel}}"
                    wrapper-class="margin-bottom-1"
                    >
                    <one-select
                        options="$ctrl.selectConfig.list"
                        model="$ctrl.selectConfig.selection"
                        on-change="$ctrl.handleSelection(selection)"
                        label-key="{{$ctrl.selectConfig.labelKey}}"
                        value-key="{{$ctrl.selectConfig.valueKey}}"
                        placeholder-text="{{$ctrl.selectConfig.placeholder}}"
                        >
                    </one-select>
                </one-label-content>
                <one-label-content
                    name="{{::$ctrl.searchConfig.fieldLabel}}"
                    wrapper-class="margin-bottom-1"
                    >
                    <one-search
                        model="$ctrl.searchConfig.searchTerm"
                        on-search="$ctrl.handleSearch(model)"
                        placeholder="{{::$ctrl.searchConfig.placeholder}}"
                        >
                    </one-search>
                </one-label-content>
                <footer
                    class="row-space-between padding-top-half"
                    >
                    <one-button
                        ng-if="$ctrl.buttonConfig"
                        type="primary"
                        text="{{::$ctrl.buttonConfig.buttonText}}"
                        button-click="$ctrl.handleButtonClick()"
                        identifier="popupSearchBtn"
                        is-disabled="!$ctrl.canSubmit"
                        >
                    </one-button>
                    <one-button
                        ng-if="$ctrl.clearConfig"
                        text="{{::$ctrl.clearConfig.buttonText}}"
                        button-click="$ctrl.handleClear()"
                        identifier="popupSearchClearBtn"
                        >
                    </one-button>
                </footer>
            </div>
        </script>
    `,
};

export default OnePopupSearch;
