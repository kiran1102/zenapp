import { IStringMap } from "interfaces/generic.interface";
import { ActionKeys } from "constants/action-keys.constant";
import { IActionIcon } from "interfaces/action-icons.interface";

class ActionIcons implements ng.IComponentController {
    private actionKeys: IStringMap<string> = ActionKeys;
}

export default {
    bindings: {
        actions: "<",
        target: "<?",
    },
    controller: ActionIcons,
    template: `
		<div class="action-icons row-horizontal-center">
            <span
                ng-repeat="action in $ctrl.actions track by $index"
                class="action-icons__button action-icons__button--{{action.key}} {{action.buttonClass}}"
                ng-if="!action.isHidden"
                ng-click="action.handleClick($ctrl.target)"
                ng-disabled="action.isDisabled || action.isLoading"
                >
                <span class="fa-stack text-center">
                    <i
                        ng-if="action.isLoading || !action.hasTooltip"
                        class="action-icons__icon fa {{!action.isLoading && action.iconClass ? action.iconClass : ''}}"
                        ng-class="{'fa-spinner fa-pulse fa-fw': action.isLoading}"
                        ></i>
                    <i
                        ng-if="!action.isLoading && action.hasTooltip"
                        class="action-icons__icon fa {{action.iconClass ? action.iconClass : ''}}"
                        tooltip-append-to-body="true"
                        tooltip-placement="{{::action.tooltipPlacement}}"
                        tooltip-popup-close-delay="{{::action.tooltipCloseDelay}}"
                        tooltip-trigger="{{::action.tooltipTrigger}}"
                        uib-tooltip="{{::$root.t(action.tooltipTranslationKey)}}"
                        ></i>
                    <i
                        class="action-icons__icon fa fa-circle fa-stack-1x action-icons__conditions-active"
                        ng-if="action.showPill"
                        >
                    </i>
                </span>
            </span>
		</div>
	`,
};
