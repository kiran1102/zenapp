import { prettyPrintOne } from "google-code-prettify/bin/prettify.min.js";

class PrettyPrintCtrl implements ng.IComponentController {
    static $inject: string[] = ["$element"];

    private $element: any;

    constructor($element: any) {
        this.$element = $element;
    }

    public $postLink(): void {
        window.setTimeout((): void => {
            const pretty: any = prettyPrintOne(this.$element.html());
            this.$element.html(pretty);
        }, 0);
    }
}

const prettify: ng.IComponentOptions = {
    controller: PrettyPrintCtrl,
    transclude: true,
    template: `<div class="block padding-all-1 pretty-print"><ng-transclude></ng-transclude></div>`,
};

export default prettify;
