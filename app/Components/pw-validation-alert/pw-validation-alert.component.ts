const pwValidationAlertComponent: ng.IComponentOptions = {
    bindings: {
        isErroneous: "<",
        text: "@",
    },
    template: () => {
        return `
            <p data-ng-class="{
                   'login__error' : $ctrl.isErroneous,
                   'login__success' : !$ctrl.isErroneous,
               }">
                <i data-ng-class="{
                    'login__warning fa fa-exclamation-circle' : $ctrl.isErroneous,
                    'ot ot-check' : !$ctrl.isErroneous,
                }" />
                <span>{{::$ctrl.text}}</span>
            </p>`;
    },
};

export default pwValidationAlertComponent;
