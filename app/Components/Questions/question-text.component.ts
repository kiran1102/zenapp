import * as _ from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class QuestionTextController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "ENUMS"];

    public answer: any = {};

    private translate: any;
    private question: any;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, ENUMS: any) {
        this.translate = $rootScope.t;
    }

    $onInit() {
        this.initialize();
    }

    $onChanges(data: ng.IOnChangesObject): void {
        if (!data.question.isFirstChange()) {
            const newValue = data.question ? data.question.currentValue : null;
            const oldValue = data.question ? data.question.previousValue : null;
            const newLoopQuestion = Boolean(newValue && oldValue && newValue.loopQuestionId && newValue.loopQuestionId !== oldValue.loopQuestionId);
            const newQuestion = Boolean(newValue && oldValue && newValue.id !== oldValue.id);
            if (newLoopQuestion || newQuestion) {
                this.initialize();
            }
        }
    }

    initialize(): void {
        this.answer = {};
        this.parseResponses();
        this.renderAnswer();
    }

    toggleNotSureAnswer(): void {
        this.answer.notSure = !this.answer.notSure;
        if (this.answer.notSure) {
            this.answer.response = "";
        }
        this.answerQuestion();
    }

    answerQuestion(): void {
        if (this.answer.response) {
            this.answer.notSure = false;
        }
        this.$scope.$emit("ANSWER_QUESTION", this.question, this.answer);
    }

    private parseResponses(): void {
        if (_.isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderAnswer(): void | undefined {
        if (!this.question.response) return;
        if (this.question.response.textResponse) {
            this.answer.response = this.question.response.textResponse;
        }
        if (this.question.response.notSure) {
            this.answer.notSure = this.question.response.notSure;
        }
    }
}

const QuestionTextTemplate = `
    <section class="question-text centered-max-70">
        <question-content question="$ctrl.question"></question-content>
        <div class="question-text__wrapper column-vertical-center">
            <textarea
                class="question-text__text-area one-text-area"
                ng-if="!$ctrl.readOnly"
                placeholder="{{::$ctrl.translate('EnterYourAnswerHere')}}"
                ng-change="$ctrl.answerQuestion()"
                ng-model="$ctrl.answer.response"
                ng-model-options="{ updateOn: 'default blur', debounce: { default: 500, blur: 0 } }"
                rows="3"
                >
            </textarea>
            <span
                class="question-text__read-only text-center paragraph-large"
                ng-if="$ctrl.readOnly"
                >
                    <span class="text-bold">
                        {{::$ctrl.translate('Answer') + ": "}}
                    </span>
                    <span>
                        {{$ctrl.answer.response}}
                    </span>
            </span>
            <div class="question-multichoice__wrapper margin-top-1">
                <one-button
                    ng-if="$ctrl.question.allowNotSure"
                    class="question-multichoice__btn-container"
                    button-class="question-multichoice__btn"
                    button-click="$ctrl.toggleNotSureAnswer()"
                    text="{{::$ctrl.translate('NotSure')}}"
                    type="select"
                    is-selected="$ctrl.answer.notSure"
                    is-disabled="$ctrl.readOnly"
                    wrap-text="true"
                    identifier="answerNotSure"
                    >
                </one-button>
            </div>
        </div>
    </section>`;

const QuestionTextComponent: ng.IComponentOptions = {
    controller: QuestionTextController,
    template: QuestionTextTemplate,
    bindings: {
        question: "<",
        readOnly: "<?",
    },
};

export default QuestionTextComponent;
