class OneQuestion implements ng.IComponentController {
    static $inject: string[] = ["$scope", "ENUMS"];

    private question: any;
    private questionTypes: any;
    private responseTypes: any;
    private displayTypes: any;
    private isInventoryResponse: boolean;

    constructor($scope: any, ENUMS: any) {
        this.questionTypes = ENUMS.DMQuestionTypes;
        this.responseTypes = ENUMS.OptionTypes;
        this.displayTypes = ENUMS.DMQuestionDisplayTypes;
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        this.isInventoryResponse =  (this.question.responseType === this.responseTypes.Assets || this.question.responseType === this.responseTypes.Processes);
    }

}

const oneQuestionComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<?",
        context: "<?",
    },
    controller: OneQuestion,
    template: `
        <section class="one-question" ng-if="!$ctrl.isInventoryResponse">
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.Welcome">
                <question-welcome
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-welcome>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.Info">
                <dm-section-intro
                    question="$ctrl.question"
                    context="$ctrl.context"
                    >
                </dm-section-intro>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.Bumper">
                <dm-section-exit
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </dm-section-exit>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.SingleSelect && $ctrl.question.displayType === $ctrl.displayTypes.Buttons">
                <question-single-select-buttons
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-single-select-buttons>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiSelect && $ctrl.question.displayType === $ctrl.displayTypes.Buttons">
                <question-multichoice
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-multichoice>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiSelectLoop">
                <question-multichoice
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-multichoice>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.SingleSelectLoop">
                <question-single-select-buttons
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly">
                </question-single-select-buttons>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.AppInfo">
                <question-app-info
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-app-info>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.SingleSelect && $ctrl.question.displayType === $ctrl.displayTypes.Typeahead">
                <question-typeahead
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-typeahead>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.TypeaheadMultiselectLoop || ($ctrl.question.questionType === $ctrl.questionTypes.MultiSelect && $ctrl.question.displayType === $ctrl.displayTypes.Typeahead)">
                <question-typeahead-multiselect
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-typeahead-multiselect>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.Text">
                <question-text
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-text>
            </span>
            <span ng-if="$ctrl.question.questionType === $ctrl.questionTypes.MultiSelect && $ctrl.question.displayType === $ctrl.displayTypes.Dropdown">
                <question-multichoice
                    question="$ctrl.question"
                    read-only="$ctrl.readOnly"
                    context="$ctrl.context"
                    >
                </question-multichoice>
            </span>
        </section>
        <section class="one-question" ng-if="$ctrl.isInventoryResponse">
            <question-asset
                question="$ctrl.question"
                read-only="$ctrl.readOnly"
                context="$ctrl.context"
                >
            </question-asset>
        </section>
    `,
};

export default oneQuestionComponent;
