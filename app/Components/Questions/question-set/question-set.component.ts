const questionSetComponent: ng.IComponentOptions = {
    bindings: {
        questions: "<",
        readOnly: "<",
        context: "<?",
    },
    template: (): string => {
        return `
            <ul class="question-set margin-top-4">
                <li
                    class="question-set__item margin-bottom-2"
                    ng-repeat="question in $ctrl.questions track by question.setId"
                    >
                    <one-question
                        question="question"
                        read-only="$ctrl.readOnly"
                        context="::$ctrl.context"
                        >
                    </one-question>
                </li>
            </ul>
        `;
    },
};

export default questionSetComponent;
