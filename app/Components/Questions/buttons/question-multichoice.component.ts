import { forEach, isString, isArray, indexOf, find, toLower, some, includes } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionOption } from "interfaces/question-options.interface";

export interface IMultichoiceAnswer {
    responses?: IQuestionOption[];
    otherResponses?: string[];
    notSure?: boolean;
}

class QuestionMultichoice implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "ENUMS"];

    public answer: IMultichoiceAnswer;
    private translate: any;
    private question: any;
    private otherConfig: any;
    private newOtherConfig: any;
    private newOtherModel = "";

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, readonly ENUMS) {
        this.translate = $rootScope.t;
        this.otherConfig = {
            isEditable: true,
            isButton: true,
            inputType: ENUMS.InputTypes.Text,
        };
        this.newOtherConfig = {...this.otherConfig, clearOnSelect: true};
    }

    public $onChanges(): void {
        this.initialize();
    }

    public initialize(): void {
        this.answer = {responses: [], otherResponses: []};
        this.parseResponses();
        if (this.question.questionType === this.ENUMS.DMQuestionTypes.MultiSelectLoop) {
            this.renderLoopAnswer();
        } else {
            this.renderAnswer();
        }
    }

    public toggleNotSureAnswer(): void {
        this.answer.notSure = !this.answer.notSure;
        if (this.answer.notSure) {
            forEach(this.question.options, (option: any): void => {
                option.isSelected = false;
            });
            this.answer = {
                responses: [],
                otherResponses: [],
                notSure: true,
            };
        }
        this.answerQuestion();
    }

    public answerQuestion(): void {
        if (this.answer.responses.length || this.answer.otherResponses.length) {
            this.answer.notSure = false;
        }
        this.$scope.$emit("ANSWER_QUESTION", this.question, this.answer);
    }

    private answerOption(option: any): void {
        if (!option.isSelected) {
            this.answer.responses.push(option.key);
        } else {
            this.removeOptionFromAnswers(option.key);
        }
        option.isSelected = !option.isSelected;
        this.answerQuestion();
    }

    private updateOther(response: string, index: number): void | undefined {
        if (!isString(this.answer.otherResponses[index])) return;
        if (this.isCurrentOtherValue(response) && this.answer.otherResponses[index] !== response) response = "";

        const currentQuestionOption: IQuestionOption = this.currentQuestionOption(response);

        if (response && currentQuestionOption) {
            currentQuestionOption.isSelected = true;
            if (!includes(this.answer.responses, currentQuestionOption.key as IQuestionOption)) this.answer.responses.push(currentQuestionOption.key as IQuestionOption);
            response = "";
        }
        if (response) {
            this.answer.otherResponses[index] = response;
        } else {
            this.answer.otherResponses.splice(index, 1);
        }
        this.answerQuestion();
    }

    private parseResponses(): void {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderAnswer(): void | undefined {
        if (!this.question.response) return;
        if (this.question.response.notSure) {
            this.answer.notSure = true;
            return;
        }
        if (isArray(this.question.response.multiSelectResponse) && this.question.response.multiSelectResponse.length) {
            this.answer.responses = this.question.response.multiSelectResponse;
            forEach(this.answer.responses, (selection: any): void => {
                const selectedOption: any = find(this.question.options, {key: selection});
                if (selectedOption) selectedOption.isSelected = true;
            });
        }
        if (isArray(this.question.response.otherResponses) && this.question.response.otherResponses.length) {
            this.answer.otherResponses = this.question.response.otherResponses;
        }
    }

    private renderLoopAnswer(): void {
        if (!this.question.response) return;
        if (this.question.response.notSure) {
            this.answer.notSure = true;
            return;
        }
        const hasLoopResponse: boolean = Boolean(this.question.response.loopMultiSelectResponse);
        const selections: IQuestionOption[] = hasLoopResponse ? this.question.response.loopMultiSelectResponse.selectedOptions : null;
        const otherSelections: string[] = hasLoopResponse ? this.question.response.loopMultiSelectResponse.otherResponse : null;
        const hasSelections: boolean = hasLoopResponse && isArray(selections) && Boolean(selections.length);
        const hasOtherSelections: boolean = hasLoopResponse && isArray(otherSelections) && Boolean(otherSelections.length);
        if (hasSelections) {
            this.answer.responses = selections;
            forEach(this.answer.responses, (selection: any): void => {
                const selectedOption: any = find(this.question.options, {key: selection});
                if (selectedOption) selectedOption.isSelected = true;
            });
        }
        if (hasOtherSelections) this.answer.otherResponses = otherSelections;
    }

    private removeOptionFromAnswers(optionKey: any): void {
        if (isArray(this.answer.responses) && this.answer.responses.length) {
            const optionIndex: number = indexOf(this.answer.responses, optionKey);
            this.answer.responses.splice(optionIndex, 1);
        }
    }

    private addOther(otherResponse: string): void {
        if (otherResponse && this.isCurrentOtherValue(otherResponse)) return;

        const currentQuestionOption: IQuestionOption = this.currentQuestionOption(otherResponse);

        if (otherResponse && currentQuestionOption) {
            currentQuestionOption.isSelected = true;
            if (!includes(this.answer.responses, currentQuestionOption.key as IQuestionOption)) this.answer.responses.push(currentQuestionOption.key as IQuestionOption);
            this.answerQuestion();
        } else if (otherResponse) {
            this.answer.otherResponses.push(otherResponse);
            this.answerQuestion();
        }
    }

    private isCurrentOtherValue(value: string): boolean {
        return some(this.answer.otherResponses, (other: string) => toLower(other) === toLower(value));
    }

    private currentQuestionOption(value: string): IQuestionOption {
        return find(this.question.options, (option: IQuestionOption) => toLower(option.value) === toLower(value));
    }

}

const template = `
    <section class="question-multichoice centered-max-70">
        <question-content question="$ctrl.question"></question-content>
        <div class="question-multichoice__wrapper">
            <one-button
                class="question-multichoice__btn-container"
                ng-if="option.value && option.key"
                ng-repeat="option in $ctrl.question.options track by option.key"
                button-click="$ctrl.answerOption(option)"
                text="{{option.value}}"
                type="select"
                is-selected="option.isSelected"
                button-class="question-multichoice__btn"
                is-disabled="$ctrl.readOnly"
                wrap-text="true"
                identifier="questionMultichoiceOption{{$index}}"
                >
            </one-button>
            <one-editable-types
                class="question-multichoice__btn-container"
                ng-if="$ctrl.question.allowOther"
                ng-repeat="otherResponse in $ctrl.answer.otherResponses track by $index"
                model="otherResponse"
                config="$ctrl.otherConfig"
                show-cancel="true"
                handle-cancel="$ctrl.close()"
                handle-edit="$ctrl.updateOther(model, $index)"
                is-disabled="$ctrl.readOnly"
                >
            </one-editable-types>
            <one-editable-types
                class="question-multichoice__btn-container"
                ng-if="$ctrl.question.allowOther"
                model="$ctrl.newOtherModel"
                config="$ctrl.newOtherConfig"
                show-cancel="true"
                handle-cancel="$ctrl.close()"
                handle-edit="$ctrl.addOther(model)"
                is-disabled="$ctrl.readOnly"
                >
            </one-editable-types>
            <one-button
                class="question-multichoice__btn-container"
                button-class="question-multichoice__btn"
                ng-if="$ctrl.question.allowNotSure"
                button-click="$ctrl.toggleNotSureAnswer()"
                text="{{::$ctrl.translate('NotSure')}}"
                type="select"
                is-selected="$ctrl.answer.notSure"
                is-disabled="$ctrl.readOnly"
                wrap-text="true"
                identifier="answerNotSure"
                >
            </one-button>
        </div>
    </section>
`;

const questionMultichoiceComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: QuestionMultichoice,
    template,
};

export default questionMultichoiceComponent;
