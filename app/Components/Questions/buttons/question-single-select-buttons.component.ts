import { toLower, isArray, isString, some, find } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionOption } from "interfaces/question-options.interface";

class QuestionSingleSelectButtonsController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope", "ENUMS"];
    public answer: any = {};

    private translate: any;
    private question: any;
    private otherConfig: any;
    private QuestionTypes: any;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, ENUMS: any) {
        this.translate = $rootScope.t;
        this.QuestionTypes = ENUMS.DMQuestionTypes;
        this.otherConfig = {
            isEditable: true,
            isButton: true,
            inputType: ENUMS.InputTypes.Text,
        };
    }

    $onInit() {
        this.initialize();
    }

    public $onChanges(data: ng.IOnChangesObject): void {
        if (!data.question.isFirstChange()) {
            const newValue = data.question ? data.question.currentValue : null;
            const oldValue = data.question ? data.question.previousValue : null;
            const newLoopQuestion = Boolean(newValue && oldValue && newValue.loopQuestionId && newValue.loopQuestionId !== oldValue.loopQuestionId);
            const newQuestion = Boolean(newValue && oldValue && newValue.id !== oldValue.id);
            if (newLoopQuestion || newQuestion) {
                this.initialize();
            }
        }
    }

    public initialize(): void {
        this.answer = {};
        this.parseResponses();
        if (this.question.questionType === this.QuestionTypes.SingleSelectLoop) {
            this.renderLoopAnswer();
        } else {
            this.renderAnswer();
        }
    }

    public toggleNotSureAnswer(): void {
        this.answer.notSure = !this.answer.notSure;
        if (this.answer.notSure) {
            this.answer = {notSure: true};
        }
        this.answerQuestion();
    }

    public answerOther(response: string): void {
        if (some(this.question.options, (option: IQuestionOption) => toLower(option.value) === toLower(response))) {
            const option: IQuestionOption = find(this.question.options, (opt: IQuestionOption) => toLower(opt.value) === toLower(response));
            if (this.answer.response !== option.key) this.toggleOption(option.key);
        } else {
            this.answer = { otherResponse: response };
            this.answerQuestion();
        }
    }

    public toggleOption(answer: any): void {
        if (this.answer.response === answer) {
            this.answer = {response: ""};
        } else {
            this.answer = {response: answer};
        }
        this.answerQuestion();
    }

    public answerQuestion(): void {
        this.$scope.$emit("ANSWER_QUESTION", this.question, this.answer);
    }

    private parseResponses(): void {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderLoopAnswer(): void | undefined {
        if (!this.question.response || !this.question.response.loopSingleSelectResponse) return;
        if (this.question.response.loopSingleSelectResponse.notSure) {
            this.answer.notSure = true;
            return;
        }
        if (this.question.response.loopSingleSelectResponse.selectedOption) {
            this.answer.response = this.question.response.loopSingleSelectResponse.selectedOption;
            return;
        }
        if (this.question.response.loopSingleSelectResponse.otherResponse) {
            this.answer.otherResponse = this.question.response.loopSingleSelectResponse.otherResponse;
            return;
        }
    }

    private renderAnswer(): void | undefined {
        if (!this.question.response) return;
        if (this.question.response.singleSelectResponse) {
            this.answer.response = this.question.response.singleSelectResponse;
            return;
        }
        if (isArray(this.question.response.otherResponses) && this.question.response.otherResponses.length) {
            this.answer.otherResponse = this.question.response.otherResponses[0];
            return;
        }
        if (this.question.response.notSure) {
            this.answer.notSure = this.question.response.notSure;
            return;
        }
    }

}

const questionSingleSelectButtonsTemplate = `
    <section class="question-multichoice centered-max-70">
        <question-content question="$ctrl.question"></question-content>
        <div class="question-multichoice__wrapper">
            <one-button
                class="question-multichoice__btn-container"
                ng-repeat="option in $ctrl.question.options track by $index"
                ng-if="option.value && option.key"
                button-click="$ctrl.toggleOption(option.key)"
                text="{{option.value}}"
                type="select"
                is-selected="$ctrl.answer.response === option.key"
                is-disabled="$ctrl.readOnly"
                button-class="question-multichoice__btn"
                wrap-text="true"
                identifier="singleAnswer"
                >
            </one-button>
            <one-editable-types
                class="question-multichoice__btn-container"
                ng-if="$ctrl.question.allowOther"
                model="$ctrl.answer.otherResponse"
                config="$ctrl.otherConfig"
                show-cancel="true"
                handle-cancel="$ctrl.close()"
                handle-edit="$ctrl.answerOther(model)"
                is-disabled="$ctrl.readOnly"
                options="$ctrl.question.options"
                >
            </one-editable-types>
            <one-button
                class="question-multichoice__btn-container"
                button-class="question-multichoice__btn"
                ng-if="$ctrl.question.allowNotSure"
                button-click="$ctrl.toggleNotSureAnswer()"
                text="{{::$ctrl.translate('NotSure')}}"
                type="select"
                is-selected="$ctrl.answer.notSure"
                is-disabled="$ctrl.readOnly"
                wrap-text="true"
                identifier="answerNotSure"
                >
            </one-button>
        </div>
    </section>
`;

const questionSingleSelectButtonsComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: QuestionSingleSelectButtonsController,
    template: questionSingleSelectButtonsTemplate,
};

export default questionSingleSelectButtonsComponent;
