import {
    isArray,
    forEach,
    cloneDeep,
    map,
    replace,
    isString,
    includes,
    find,
    findIndex,
    indexOf,
    isNumber,
    remove,
    uniqBy,
} from "lodash";
import Utilities from "Utilities";

interface IAnswer {
    assetName: string;
    attributeResponses: any[];
}

interface IAttributeResponse {
    attributeId: string;
    inventoryValues: any[];
}

class QuestionAssetController implements ng.IComponentController {
    static $inject: string[] = ["$scope", "ENUMS"];

    private question: any;
    private assetQuestion: any;
    private followUpSets: any[];
    private answers: any = {};
    private QuestionTypes: any;
    private DisplayTypes: any;
    private isMultiSelect: boolean;

    constructor(readonly $scope: any, ENUMS: any) {
        this.QuestionTypes = ENUMS.DMQuestionTypes;
        this.DisplayTypes = ENUMS.DMQuestionDisplayTypes;
    }

    $onInit() {
        this.startListeners();
    }

    $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.question) {
            this.assetQuestion = {...this.question};
            this.followUpSets = [];
            this.isMultiSelect = this.question.questionType === this.QuestionTypes.MultiSelect;
            this.parseResponses();
            this.renderResponses();
        }
    }

    public answerQuestion(data: any, passEventCatch?: boolean): void {
        this.$scope.$emit("ANSWER_QUESTION", this.question, data, passEventCatch);
    }

    private parseResponses(): void {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderResponses(): void | undefined {
        if (!this.question.response) return;
        const responseKey: string = this.isMultiSelect ? "multiSelectResponse" : "singleSelectResponse";
        const typeaheadResponses: any = {otherResponses: [], notSure: false};
        typeaheadResponses[responseKey] = this.isMultiSelect ? [] : null;
        if (isArray(this.question.response) && this.question.response.length) {
            const newQuestionSets: any[] = [];
            let setCount = 0;
            forEach(this.question.response, (response: any, setIndex: number): void => {
                if (response.inventoryId) {
                    if (this.isMultiSelect) {
                        typeaheadResponses[responseKey].push(response.inventoryId);
                    } else {
                        typeaheadResponses[responseKey] = response.inventoryId;
                    }
                } else  {
                    typeaheadResponses.otherResponses.push(response.assetName);
                    newQuestionSets.push( this.createFollowupSet(response.assetName, setCount, response.attributeResponses) );
                    setCount++;
                }
            });
            this.followUpSets = newQuestionSets;
            this.answers.assetResponses = [...this.question.response];
            this.assetQuestion.response = typeaheadResponses;
            return;
        }
        if (this.question.response.notSure) {
            typeaheadResponses.notSure = true;
            this.answers.notSure = true;
            this.assetQuestion.response = typeaheadResponses;
        }
    }

    private createFollowupSet(assetName: string, setIndex: number, attributeResponses?: any[]): any {
        const newQuestionSet: any[] = cloneDeep(this.question.followUpQuestions);
        const questions: any[] = map(newQuestionSet, (question: any, questionIndex: number): any => {
            question.name = replace(question.name, "/KEY/", `<strong>${assetName}</strong>`);
            question.description = replace(question.description, "/KEY/", `<strong>${assetName}</strong>`);
            question.setId = Utilities.uuid();
            if (attributeResponses) {
                const matchingResponse: any = find(attributeResponses, {attributeId: question.attributeId});
                if (matchingResponse && isArray(matchingResponse.inventoryValues) && matchingResponse.inventoryValues.length) {
                    question.response = this.formatInventoryForResponse(question, matchingResponse.inventoryValues);
                }
            }
            return {...question, setIndex, assetName, questionIndex};
        });
        return { assetName, questions };
    }

    private renderOtherAssets(answers: any[], otherResponses: string[]): any {
        if (isArray(otherResponses) && otherResponses.length) {
            const otherResponseLength: number = otherResponses.length;
            const followUpLength: number = this.followUpSets.length;
            if (otherResponseLength > followUpLength) {
                for (let i = 0; i < otherResponseLength; i++) {
                    const responseSet: any = find(this.followUpSets, {assetName: otherResponses[i]} as any);
                    if (!responseSet) {
                        this.followUpSets.push(this.createFollowupSet(otherResponses[i], followUpLength, null) );
                        answers.push( {assetName: otherResponses[i], attributeResponses: []});
                        break;
                    }
                }
            } else if (otherResponseLength < followUpLength) {
                for (let i = 0; i < followUpLength; i++) {
                    const followUpSet: any = find(otherResponses, (otherResponse: any): boolean => {
                        return this.followUpSets[i].assetName === otherResponse;
                    });
                    const otherIndex: any = indexOf(otherResponses, this.followUpSets[i].assetName);
                    if (otherIndex === -1) {
                        const answerIndex: number = findIndex(answers, (answer: any): boolean => {
                            return this.followUpSets[i].assetName === answer.assetName;
                        });
                        answers.splice(answerIndex, 1);
                        this.followUpSets.splice(i, 1);
                        break;
                    }
                }
            }
        } else {
            this.followUpSets = [];
            answers = [];
        }
        return uniqBy(answers, "assetName");
    }

    private renderSingleOtherAssets(otherAssetName?: string): any[] {
        this.followUpSets = [];
        if (otherAssetName) {
            this.followUpSets.push( this.createFollowupSet(otherAssetName, 0, null) );
            return [{assetName: otherAssetName, attributeResponses: []}];
        }
        return [];
    }

    private renderExistingAssets(responses: any[]): any {
        return map(responses, (response: string): { inventoryId: string } => {
            return { inventoryId: response };
        });
    }

    private createFollowupResponse(answers: IAnswer[], question: any, data: any): IAnswer[] | undefined {
        if (isArray(answers) && answers.length) {
            const answerIndex: number = findIndex(answers, {assetName: question.assetName});
            if (!isNumber(answerIndex) || answerIndex === -1) return answers;
            const attrResponses: any[] = answers[answerIndex].attributeResponses;
            const attrResponseIndex: number = findIndex(attrResponses, {attributeId: question.attributeId});
            const newAttrResponse: any = {
                attributeId: question.attributeId,
                inventoryValues: this.formatResponseForInventory(question, data),
            };
            if (!isNumber(attrResponseIndex) || attrResponseIndex === -1) {
                answers[answerIndex].attributeResponses.push(newAttrResponse);
                return answers;
            }
            if (!data || (isArray(data) && !data.length)) {
                answers[answerIndex].attributeResponses.splice(attrResponseIndex, 1);
            } else {
                answers[answerIndex].attributeResponses[attrResponseIndex] = newAttrResponse;
            }
        }
        return answers;
    }

    private formatResponseForInventory(question: any, data: any): any[] {
        switch (question.questionType) {
            case this.QuestionTypes.MultiSelect:
                return map(data.responses, (res: string): any => {
                    return {valueId: res};
                });
            case this.QuestionTypes.SingleSelect:
                if (question.displayType === this.DisplayTypes.Typeahead) {
                    if (!data) return;
                    if (data.notSure) return [{notSure: true}];
                    if (data.key) return [{valueId: data.key}];
                    if (data.value) return [{value: data.value}];
                    return;
                } else {
                    if (!data) return;
                    if (data.notSure) return [{notSure: true}];
                    if (data.response) return [{valueId: data.response}];
                    if (data.otherResponse) return [{value: data.otherResponse}];
                    return;
                }
            case this.QuestionTypes.Text:
                return [{value: data.response}];
        }
    }

    private formatInventoryForResponse(question: any, inventoryResponse: any): any {
        if (!isArray(inventoryResponse) || !inventoryResponse.length) return;
        const response: any = inventoryResponse[0];
        switch (question.questionType) {
            case this.QuestionTypes.MultiSelect:
                return {multiSelectResponse : map(inventoryResponse, (res: any): any => res.valueId)};
            case this.QuestionTypes.SingleSelect:
                if (response.notSure) return {notSure: true};
                if (response.valueId) return {singleSelectResponse: response.valueId};
                if (response.value) return {otherResponses: [response.value]};
                return;
            case this.QuestionTypes.Text:
                return {textResponse: response.value};
        }
    }

    private startListeners(): void | undefined {
        this.$scope.$on("ANSWER_QUESTION", (event: ng.IAngularEvent, question: any, data: any, passEventCatch?: boolean): void | undefined => {
            if (passEventCatch) return;
            event.stopPropagation();
            if (isNumber(question.setIndex) && isNumber(question.questionIndex)) {
                this.answers.assetResponses = this.createFollowupResponse(this.answers.assetResponses, question, data);
            } else {
                if (data.notSure) {
                    this.answers.notSure = true;
                    this.answers.assetResponses = [];
                    this.followUpSets = [];
                } else {
                    this.answers.notSure = false;
                    this.answers.assetResponses =  this.isMultiSelect ? this.formatMultiSelectResponse(data) : this.formatSingleSelectResponse(data);
                }
            }
            this.answerQuestion(this.answers, true);
        });
    }

    private formatSingleSelectResponse(data: any): any[] {
        if (data.value && !data.key) {
            return this.renderSingleOtherAssets(data.value);
        }
        this.renderSingleOtherAssets();
        if (data.key) {
            return [{inventoryId: data.key}];
        }
        return [];
    }

    private formatMultiSelectResponse(data: any): any[] {
        const otherAnswers: any[] = remove(this.answers.assetResponses, (answer: any): boolean => !answer.inventoryId);
        return [...this.renderOtherAssets(otherAnswers, data.otherResponses), ...this.renderExistingAssets(data.responses)];
    }

}

const questionAssetTemplate = `
    <section class="question-asset centered-max-70">
        <question-typeahead-multiselect
            ng-if="$ctrl.isMultiSelect"
            question="$ctrl.assetQuestion"
            read-only="$ctrl.readOnly"
            context="::$ctrl.context"
            >
        </question-typeahead-multiselect>
        <question-typeahead
            ng-if="!$ctrl.isMultiSelect"
            question="$ctrl.assetQuestion"
            read-only="$ctrl.readOnly"
            context="::$ctrl.context"
            >
        </question-typeahead>
        <question-set
            class="question-asset__followup-list"
            ng-repeat="questionSet in $ctrl.followUpSets track by $index"
            questions="questionSet.questions"
            read-only="$ctrl.readOnly"
            context="::$ctrl.context"
            >
        </question-set>
    </section>
`;

const questionAssetComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
        context: "<?",
    },
    controller: QuestionAssetController,
    template: questionAssetTemplate,
};

export default questionAssetComponent;
