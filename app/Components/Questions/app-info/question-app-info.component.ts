import * as _ from "lodash";

class QuestionAppInfo {
    static $inject: string[] = ["$scope", "ENUMS"];

    private question: any;
    private attributeTypes: any;

    constructor(readonly $scope: any, ENUMS: any) {
        this.attributeTypes = ENUMS.QuestionAttributeTypes;
    }

    $onInit() {
        this.parseResponses();
        this.renderAnswers();
    }

    public answerQuestion(data: any): void {
        this.$scope.$emit("ANSWER_QUESTION", this.question, data);
    }

    private parseResponses(): void {
        if (_.isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderAnswers(): void {
        const appResponses: any[] = this.question.response ? this.question.response.assetResponses : [];
        const attributes: any[] = this.question.applicationAttributes;
        if (_.isArray(appResponses) && appResponses.length && _.isArray(attributes) && attributes.length) {
            _.forEach(appResponses, (app: any): void => {
                const responses: any[] = app.attributeResponses;
                if (_.isArray(responses) && responses.length) {
                    _.forEach(responses, (response: any): void => {
                        const matchingAttribute: any = _.find(attributes, { id: response.attributeId });
                        if (matchingAttribute) {
                            matchingAttribute.answer = _.isArray(response.value) ? response.value[0] : response.value;
                        }
                    });
                }
            });
        }
    }

}

const questionAppInfo = {
    bindings: {
        question: "<",
        readOnly: "<",
    },
    controller: QuestionAppInfo,
    template: () => {
        return `<section class="question-app-info centered-max-70">
                    <question-content question="$ctrl.question"></question-content>
                    <ul class="question-app-info__table">
                        <li class="question-app-info__row">
                            <label class="question-app-info__label text-bold text-center"> Question </label>
                            <label
                                class="question-app-info__label question-app-info__label--fixed text-bold text-center"
                                ng-repeat="application in $ctrl.question.applications track by $index">
                                    {{ application.name }}
                            </label>
                        </li>
                        <li
                            class="question-app-info__row"
                            ng-repeat="attribute in $ctrl.question.applicationAttributes track by $index">
                                <p class="question-app-info__label"> {{ attribute.name }} </p>
                                <div
                                    class="question-app-info__cell"
                                    ng-repeat="application in $ctrl.question.applications track by $index">
                                        <one-select
                                            class="question-app-info__select"
                                            ng-if="attribute.type === $ctrl.attributeTypes.Select"
                                            is-disabled="$ctrl.readOnly"
                                            options="attribute.options"
                                            on-change="$ctrl.answerQuestion({selection: selection, attrId: attribute.id, attrType: attribute.type, appId: application.id})"
                                            model="attribute.answer"
                                            label-key="value"
                                            value-key="key"
                                            placeholder-text="- - - -">
                                        </one-select>
                                        <one-select
                                            class="question-app-info__select"
                                            ng-if="attribute.type === $ctrl.attributeTypes.MultiSelect"
                                            is-disabled="$ctrl.readOnly"
                                            options="attribute.options"
                                            on-change="$ctrl.answerQuestion({selection: [selection], attrId: attribute.id, attrType: attribute.type, appId: application.id})"
                                            model="attribute.answer"
                                            label-key="value"
                                            value-key="key"
                                            placeholder-text="- - - -">
                                        </one-select>
                                        <input
                                            class="question-app-info__input one-input"
                                            placeholder="- - - -"
                                            ng-if="attribute.type === $ctrl.attributeTypes.Text"
                                            ng-change="$ctrl.answerQuestion({value: attribute.answer, attrId: attribute.id, attrType: attribute.type, appId: application.id})"
                                            ng-model="attribute.answer"
                                            ng-model-options="{ debounce: {'default': 1000} }"
                                            ng-disabled="$ctrl.readOnly" />
                                </div>
                        </li>
                    </ul>
                </section>`;
    },
};

export default questionAppInfo;
