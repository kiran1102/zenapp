
interface IQuestion {
    readonly id: string;
    readonly name: string;
    readonly questionType: number;
    readonly statusId: number;
    readonly templateQuestionId: number;
    readonly templateSectionId: number;
    readonly assessmentId: string;
}

class QuestionWelcome {
    static $inject: string[] = ["$scope"];

    private scope: any;
    private question: IQuestion;

    constructor($scope: any) {
        this.scope = $scope;
    }

    public startAssessment(): void {
        this.scope.$emit("COMPLETE_SECTION", this.question);
    }

}

const questionWelcome = {
    bindings: {
        question: "<",
        readOnly: "<?",
    },
    controller: QuestionWelcome,
    template: () => {
        return `<section class="question-welcome centered-max-70">
                            <question-content question="$ctrl.question"></question-content>
                            <one-button
                                ng-if="!$ctrl.readOnly"
                                class="question-welcome__btn"
                                button-click="$ctrl.startAssessment()"
                                text="{{$root.t('GetStarted')}}"
                                type="primary"
                                is-disabled="$ctrl.readOnly">
                            <one-button>
                        </section>`;
    },
};

export default questionWelcome;
