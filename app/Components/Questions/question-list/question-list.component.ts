class QuestionListCtrl implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$injector", "ENUMS"];

    private questions: any[];
    private readOnly: boolean;
    private context: number;
    private ContextTypes: any;
    private dataSource: any;

    constructor(readonly $scope: any, readonly $injector: ng.auto.IInjectorService, ENUMS: any) {
        this.ContextTypes = ENUMS.ContextTypes;
    }

    public $onInit(): void {
        if (this.context === this.ContextTypes.DMAssessment) {
            this.dataSource = this.$injector.get("OneAssessment");
        }
        this.startListeners();
        if (this.context === this.ContextTypes.DMAssessment) {
            this.questions = this.dataSource.getActiveQuestions();
            this.readOnly = this.dataSource.getReadOnly();
        }
    }

    private startListeners(): void {
        if (this.context === this.ContextTypes.DMAssessment) {
            this.$scope.$on("SECTION_CHANGED", (): void => {
                this.questions = this.dataSource.getActiveQuestions();
            });
            this.$scope.$on("ANSWER_CHANGED", (): void => {
                this.questions = this.dataSource.getActiveQuestions();
            });
        }
    }

}

const questionListComponent: ng.IComponentOptions = {
    controller: QuestionListCtrl,
    bindings: {
        questions: "<?",
        context: "<?",
    },
    template: (): string => {
        return `<ul class="question-list">
                    <li
                        class="question-list__item flex shadow shadow-transition margin-left-right-auto margin-top-bottom-3"
                        ng-repeat="question in $ctrl.questions track by $index">
                            <one-question
                                class="question-list__question full-width center-block padding-left-right-2 padding-top-bottom-3"
                                question="question"
                                read-only="$ctrl.readOnly"
                                context="::$ctrl.context"
                                >
                            </one-question>
                    </li>
                </ul>`;
    },
};

export default questionListComponent;
