
const questionContent = {
    bindings: {
        question: "<",
    },
    template: `
        <h3 class="heading3 margin-top-0 margin-bottom-2 text-thin-2"
            >
            <span
                ng-bind-html="$ctrl.question.name"
                >
            </span>
            <sup
                ng-if="$ctrl.question.required"
                class="helper-icon-required"
                >
            </sup>
        </h3>
        <p
            class="question-content__description paragraph-medium margin-bottom-3"
            ng-bind-html="$ctrl.question.description"
            >
        </p>
    `,
};

export default questionContent;
