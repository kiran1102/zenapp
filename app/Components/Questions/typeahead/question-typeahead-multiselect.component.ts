import { ContextTypes } from "enums/context-types.enum";
import { DMQuestionTypes } from "enums/question-types.enum";
import { cloneDeep, toLower, some, isArray, forEach, findIndex, isString, find, map, includes } from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionOption } from "interfaces/question-options.interface";

class QuestionTypeaheadMultiselect implements ng.IComponentController {
    static $inject: string[] = ["$scope", "$rootScope"];
    public selections: string[];
    public options: IQuestionOption[];
    public answers: IQuestionOption[];
    private translate: any;
    private question: any;
    private notSure: boolean;
    private scrollParent: Element;
    private context: number;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
        if (this.context && this.context === ContextTypes.DMAssessment) {
            const scrollEl: HTMLCollectionOf<Element> = document.getElementsByClassName("dm-assessment__body");
            this.scrollParent = scrollEl ? scrollEl[0] : null;
        }
    }

    $onInit() {
        this.initialize();
    }

    $onChanges(data: ng.IOnChangesObject): void {
        if (data.question && !data.question.isFirstChange()) {
            this.initialize();
        }
    }

    initialize(): void {
        this.selections = [];
        this.answers = [];
        this.notSure = false;
        this.options = cloneDeep(this.question.options);
        this.parseResponses();
        if (this.question.questionType === DMQuestionTypes.TypeaheadMultiselectLoop) {
            this.renderLoopAnswers();
        } else {
            this.renderAnswers();
        }
    }

    toggleNotSureAnswer(): void {
        this.notSure = !this.notSure;
        if (this.notSure) {
            this.removeAnswersForOptions();
        }
        this.answerQuestion();
    }

    answerQuestion(): void {
        if (this.answers.length || this.selections.length) {
            this.notSure = false;
        }
        const updatedAnswer: any = {
            responses: [],
            otherResponses: [],
            notSure: this.notSure,
        };
        if (!this.notSure) {
            forEach(this.answers, (answer: any): void => {
                if (answer.key && !includes(updatedAnswer.responses, answer.key)) {
                    updatedAnswer.responses.push(answer.key);
                } else if (!some(updatedAnswer.otherResponses, (otherResponse: string) => toLower(otherResponse) === toLower(answer.value))) {
                    updatedAnswer.otherResponses.push(answer.value);
                }
            });
        }
        this.$scope.$emit("ANSWER_QUESTION", this.question, updatedAnswer);
    }

    addAnswer(option: any): void | undefined {
        if (option) {
            const existingOption: IQuestionOption | undefined = find(this.question.options, (questionOption: IQuestionOption): boolean => toLower(questionOption.value) === toLower(option.InputValue));
            const isExistingSelection: boolean = some(this.selections, (response: IQuestionOption): boolean => toLower(response.value) === toLower(option.InputValue));

            if ((option.IsPlaceholder && !option.InputValue) || isExistingSelection) {
                return;
            }
            if (option.key) {
                this.removeOption(option);
            } else if (existingOption) {
                option = existingOption;
                this.removeOption(option);
            } else {
                option = {value: option.InputValue};
            }

            if (!includes(this.answers, option) && !includes(this.selections, option)) {
                this.answers.push(option);
                this.selections.push(option);
                this.answerQuestion();
            }
        }
    }

    removeAnswer(option: any): void {
        if (isArray(this.answers) && this.answers.length) {
            const optionIndex: number = this.answers.indexOf(option);
            this.answers.splice(optionIndex, 1);
            if (option.key) this.options.push(option);
            if (this.options) this.options = [...this.options];
            this.removeSelection(option);
            this.answerQuestion();
        }
    }

    removeSelection(option: any): void {
        if (isArray(this.selections) && this.selections.length) {
            const optionIndex: number = findIndex(this.selections, (selection: any): boolean => {
                return option.key ? option.key === selection.key : option.value === selection.value;
            });
            this.selections.splice(optionIndex, 1);
        }
    }

    removeOption(option): void {
        if (isArray(this.options) && this.options.length) {
            const optionIndex: number = findIndex(this.options, (opt: any): boolean => {
                return option.key === opt.key;
            });
            this.options.splice(optionIndex, 1);
            this.options = [...this.options];
        }
    }

    private parseResponses(): void {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderAnswers(): void | undefined {
        const hasResponse: boolean = Boolean(this.question.response);
        const responseIsArray: boolean = hasResponse ? isArray(this.question.response.multiSelectResponse) : false;
        const responseLength: number = responseIsArray ? this.question.response.multiSelectResponse.length : 0;
        const otherResponseIsArray: boolean = hasResponse ? isArray(this.question.response.otherResponses) : false;
        const otherResponseLength: number = otherResponseIsArray ? this.question.response.otherResponses.length : 0;
        if (!hasResponse) return;
        if (this.question.response.notSure) {
            this.notSure = true;
            return;
        }
        if (responseIsArray && responseLength) {
            forEach(this.question.response.multiSelectResponse, (response: string): void => {
                const selectedOption: IQuestionOption = find(this.question.options, (option: IQuestionOption): boolean => option.key === response);
                if (selectedOption) {
                    this.answers.push(selectedOption);
                }
            });
        }
        if (otherResponseIsArray && otherResponseLength) {
            const otherAnswers: any[] = map(this.question.response.otherResponses, (response: any): any => {
                return {value: response};
            });
            this.answers = [...this.answers, ...otherAnswers];
        }
        this.removeOptionsForAnswers();
    }

    private renderLoopAnswers(): void {
        const hasResponse: boolean = Boolean(this.question.response);
        const hasLoopResponse: boolean = hasResponse ? Boolean(this.question.response.loopMultiSelectTypeAheadResponse) : false;
        const loopResponseIsArray: boolean = hasLoopResponse ? isArray(this.question.response.loopMultiSelectTypeAheadResponse.selectedOptions) : false;
        const loopResponseLength: number = loopResponseIsArray ? this.question.response.loopMultiSelectTypeAheadResponse.selectedOptions.length : 0;

        if (loopResponseLength) {
            forEach(this.question.response.loopMultiSelectTypeAheadResponse.selectedOptions, (response: string): void => {
                const selectedOption: IQuestionOption = find(this.question.options, (option: IQuestionOption): boolean => option.key === response);
                if (selectedOption) {
                    this.answers.push(selectedOption);
                }
            });
            this.removeOptionsForAnswers();
        }
    }

    private removeOptionsForAnswers(): void {
        if (isArray(this.answers) && this.answers.length) {
            forEach(this.answers, (selection: any): void => {
                this.selections.push(selection);
                if (selection.key) {
                    this.removeOption(selection);
                }
            });
        }
    }

    private removeAnswersForOptions(): void {
        const updatedOptions: IQuestionOption[] = [];
        if (isArray(this.answers) && this.answers.length) {
            forEach(this.answers, (selection: any): void => {
                if (selection.key) {
                    updatedOptions.push(selection);
                }
            });
            this.selections = [];
            this.answers = [];
            this.options = [...updatedOptions, ...this.options];
        }
    }

}

const questionTypeaheadMultiselectTemplate = `
    <section class="question-typeahead-multiselect centered-max-70">
        <question-content question="$ctrl.question"></question-content>
        <typeahead
            class="question-typeahead-multiselect__input"
            options="$ctrl.options"
            on-select="$ctrl.addAnswer(selection)"
            type="$ctrl.question.questionType"
            label-key="value"
            has-arrow="true"
            clear-on-select="true"
            is-disabled="$ctrl.readOnly"
            allow-other="$ctrl.question.allowOther"
            allow-empty="true"
            empty-text="- - - -"
            disable-if-no-options="!$ctrl.question.allowOther"
            other-option-prepend="{{::$ctrl.translate('Add')}}"
            scroll-parent="$ctrl.scrollParent"
            order-by-label="false"
            input-limit="2"
            use-virtual-scroll="true"
            options-limit="1000"
            >
        </typeahead>
        <div class="question-multichoice__wrapper">
            <one-button
                ng-if="$ctrl.question.allowNotSure"
                class="question-multichoice__btn-container"
                button-click="$ctrl.toggleNotSureAnswer()"
                text="{{::$ctrl.translate('NotSure')}}"
                type="select"
                is-selected="$ctrl.notSure"
                is-disabled="$ctrl.readOnly"
                wrap-text="true"
                identifier="answerNotSure"
                >
            </one-button>
        </div>
        <pill-list
            list="$ctrl.selections",
            name-key="value",
            handle-remove="$ctrl.removeAnswer(item)"
            is-disabled="$ctrl.readOnly">
        </pill-list>
    </section>
`;

const questionTypeaheadMultiselectComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
        context: "<?",
    },
    controller: QuestionTypeaheadMultiselect,
    template: questionTypeaheadMultiselectTemplate,
};

export default questionTypeaheadMultiselectComponent;
