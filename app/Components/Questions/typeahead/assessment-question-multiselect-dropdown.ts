import {
    cloneDeep,
    toLower,
    some,
    isArray,
    forEach,
    findIndex,
    find,
    map,
    includes,
    isEmpty,
    filter,
    isFunction,
    assign,
} from "lodash";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import {
    IQuestion,
    IAssessmentDataElement,
    IAssessmentQuestionAction,
    IQuestionResponse,
} from "interfaces/question.interface";
import { QuestionResponseTypes } from "enums/assessment-question.enum";

class AssessmentQuestionMultiselectDropdownController implements ng.IComponentController {
    static
     $inject: string[] = ["$rootScope"];
    public answers: IAssessmentDataElement[];
    public selections: IAssessmentDataElement[];
    public options: IAssessmentDataElement[];
    private question: IQuestion;
    private QuestionResponseTypes: any;
    private callback: Function;

    constructor($rootScope: IExtendedRootScopeService) {}

    public $onInit(): void {
        this.QuestionResponseTypes = QuestionResponseTypes;
        this.initialize();
    }

    public $onChanges(data: ng.IOnChangesObject): void {
        if (data.question && !data.question.isFirstChange()) {
            this.initialize();
        }
    }

    public initialize(): void {
        this.selections = [];
        this.answers = [];
        if (isEmpty(this.options)) {
            this.options = cloneDeep(this.question.Options);
        }
        this.renderAnswers();
    }

    public addOption(option: IAssessmentDataElement): void | undefined {
        if (option) {
            const existingOption: IAssessmentDataElement = find(this.question.Options, (questionOption: IAssessmentDataElement): boolean => toLower(questionOption.Value) === toLower(option.Value));
            const isExistingSelection: boolean = some(this.selections, (selected: IAssessmentDataElement): boolean => toLower(selected.Value) === toLower(option.Value));

            if ((option.IsPlaceholder && !option.InputValue) || isExistingSelection) {
                return;
            }

            if (existingOption) {
                option = existingOption;
                this.removeOption(option);
            } else {
                option = { ...option, Name: option.InputValue, Value: option.InputValue };
            }

            if (!includes(this.answers, option) && !includes(this.selections, option)) {
                this.answers.push(option);
                this.selections.push(option);
                this.sendAction("MULTISELECT_OPTION_SELECTED", option);
            }
        }
    }

    public removeAnswer(option: IAssessmentDataElement): void {
        if (isArray(this.answers) && this.answers.length) {
            const optionIndex: number = this.answers.indexOf(option);
            if (optionIndex >= 0) {
                this.answers.splice(optionIndex, 1);
            }
            if (option.Name) this.options.push(option);
            if (this.options) this.options = [...this.options];
            this.removeSelection(option);
            this.sendAction("MULTISELECT_OPTION_SELECTED", option);
        }
    }

    public removeSelection(option: IAssessmentDataElement): void {
        if (isArray(this.selections) && this.selections.length) {
            const optionIndex: number = findIndex(this.selections, (selection: any): boolean => {
                return option.Name ? option.Name === selection.Name : option.Value === selection.Value;
            });
            if (optionIndex >= 0) {
                this.selections.splice(optionIndex, 1);
            }
        }
    }

    public removeOption(option: IAssessmentDataElement): void {
        if (isArray(this.options) && this.options.length) {
            const optionIndex: number = findIndex(this.options, (opt: IAssessmentDataElement): boolean => {
                return option.Name === opt.Name;
            });
            if (optionIndex >= 0) {
                this.options.splice(optionIndex, 1);
                this.options = [...this.options];
            }
        }
    }

    private renderAnswers(): void | undefined {
        const { NotSure, Other, NotApplicable } = QuestionResponseTypes;
        const hasResponse: boolean = !isEmpty(this.question.Responses) && isArray(this.question.Responses);
        const hasAnswer: boolean = !isEmpty(this.question.Answer) && isArray(this.question.Answer);

        const responseLength: number = hasResponse ? this.question.Responses.length : 0;
        const answerLength: number = hasAnswer ? this.question.Answer.length : 0;

        const tempOtherOptions: IAssessmentDataElement[] = hasResponse ? filter(this.question.Options, (option: IAssessmentDataElement): boolean => option.Type === Other) : null;
        const otherResponseIsArray: boolean = hasResponse ? (isEmpty(tempOtherOptions) ? false : isArray(tempOtherOptions)) : false;
        const otherResponseLength: number = otherResponseIsArray ? tempOtherOptions.length : 0;

        const notSureSelected: boolean = hasResponse ? Boolean(find(this.question.Responses, (response: IQuestionResponse): boolean => response.Type === NotSure)) : false;
        const notApplicableSelected: boolean = hasResponse ? Boolean(find(this.question.Responses, (response: IQuestionResponse): boolean => response.Type === NotApplicable)) : false;
        if (notSureSelected) {
            return;
        }

        if (!hasResponse || !hasAnswer) return;

        if (hasResponse && responseLength || hasAnswer && answerLength) {
            forEach(this.question.Responses, (response: IQuestionResponse): void => {
                const selectedOption: IAssessmentDataElement = find(this.question.Options, (option: IAssessmentDataElement): boolean => option.Value === response.Value);
                if (selectedOption) {
                    this.answers.push(selectedOption);
                }
            });
        }

        if (otherResponseIsArray && otherResponseLength) {
            const otherAnswers: IAssessmentDataElement[] = map(tempOtherOptions, (option: IAssessmentDataElement): IAssessmentDataElement => {
                return option;
            });
            this.answers = [...this.answers, ...otherAnswers];
        }
        this.removeOptionsForAnswers();
    }

    private removeOptionsForAnswers(): void {
        if (isArray(this.answers) && this.answers.length) {
            forEach(this.answers, (selection: IAssessmentDataElement): void => {
                this.selections.push(selection);
                if (selection.Name) {
                    this.removeOption(selection);
                }
            });
        }
    }

    private removeAnswersForOptions(): void {
        const updatedOptions: IAssessmentDataElement[] = [];
        if (isArray(this.answers) && this.answers.length) {
            forEach(this.answers, (answer: IAssessmentDataElement): void => {
                if (answer.Name) {
                    updatedOptions.push(answer);
                }
            });
            this.selections = [];
            this.answers = [];
            this.options = [...updatedOptions, ...this.options];
        }
    }

    private sendAction(type: string, value?: any): void {
        const actionType: IAssessmentQuestionAction = { type };
        this.options = [...this.question.Options];
        if (type === "MULTISELECT_OPTION_SELECTED") {
            assign(actionType, { value });
        }
        this.callback({ actionType });
    }
}

export const AssessmentQuestionMultiselectDropdown: ng.IComponentOptions = {
    bindings: {
        question: "<",
        options: "<?",
        readOnly: "<",
        callback: "&?",
    },
    controller: AssessmentQuestionMultiselectDropdownController,
    template: `
        <div class="question__multichoice">
            <question-content
                question="$ctrl.question">
            </question-content>
            <div
                ng-if="$ctrl.question.State">
                <typeahead
                    class="question-typeahead-multiselect__input"
                    options="$ctrl.options"
                    on-select="$ctrl.addOption(selection)"
                    type="$ctrl.question.QuestionType"
                    has-arrow="true"
                    clear-on-select="true"
                    is-disabled="$ctrl.readOnly"
                    allow-empty="true"
                    empty-text="- - - -"
                    other-option-prepend="{{::$root.t('Add')}}"
                    order-by-label="false"
                    label-key="Name"
                    input-limit="2"
                    use-virtual-scroll="true"
                    options-limit="1000"
                    >
                </typeahead>
                <pill-list
                    list="$ctrl.selections"
                    name-key="Name"
                    handle-remove="$ctrl.removeAnswer(item)"
                    is-disabled="$ctrl.readOnly">
                </pill-list>
                <div class="btn-group"
                    ng-if="$ctrl.question.State">
                    <div class="option-container">
                        <assessment-question-not-sure-button
                            button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                            is-disabled="$ctrl.readOnly"
                            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotSure"
                            on-click="$ctrl.sendAction('NOT_SURE_SELECTED')"
                            ng-if="$ctrl.question.AllowNotSure">
                        </assessment-question-not-sure-button>
                    </div>
                    <div class="option-container">
                        <assessment-question-not-applicable-button
                            button-class="margin-right-1 margin-top-bottom-1 width-100-percent"
                            is-disabled="$ctrl.readOnly"
                            is-selected="$ctrl.question.ResponseType === $ctrl.QuestionResponseTypes.NotApplicable"
                            on-click="$ctrl.sendAction('NOT_APPLICABLE_SELECTED')"
                            ng-if="$ctrl.question.AllowNotApplicable">
                        </assessment-question-not-applicable-button>
                    </div>
                </div>
            </div>
        </div>
    `,
};
