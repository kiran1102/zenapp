import {
    find,
    cloneDeep,
    toLower,
    isArray,
    isString,
    map,
} from "lodash";
import Utilities from "Utilities";

import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IQuestionOption } from "interfaces/question-options.interface";
import { IOrgUserAdapted } from "interfaces/org-user.interface";

import { OrgUserDropdownTypes } from "enums/org-user-dropdown.enum";
import { Regex } from "constants/regex.constant";

class QuestionTypeahead implements ng.IComponentController {

    static $inject: string[] = ["$scope", "$rootScope", "ENUMS"];

    public answer: any = {};
    public options: IQuestionOption[];
    public userOptions: IOrgUserAdapted[];
    public isUserQuestion: boolean;
    public OrgUserDropdownTypes = OrgUserDropdownTypes;
    private question: any;
    private translate: any;
    private optionTypes: any;
    private otherConfig: any;
    private scrollParent: Element;
    private context: number;

    constructor(readonly $scope: any, $rootScope: IExtendedRootScopeService, readonly ENUMS: any) {
        this.translate = $rootScope.t;
        this.optionTypes = ENUMS.OptionTypes;
        this.otherConfig = {
            isEditable: true,
            isButton: true,
            inputType: ENUMS.InputTypes.Text,
        };
    }

    public $onInit() {
        this.initialize();
        if (this.context && this.context === this.ENUMS.ContextTypes.DMAssessment) {
            const scrollEl: HTMLCollectionOf<Element> = document.getElementsByClassName("dm-assessment__body");
            this.scrollParent = scrollEl ? scrollEl[0] : null;
        }
        this.isUserQuestion = this.question.responseType === this.optionTypes.Users;
        if (this.isUserQuestion) {
            this.updateOptions();
        }
    }

    public $onChanges(data: ng.IOnChangesObject): void {
        if (!data.question.isFirstChange()) {
            this.updateOptions();
            const newValue = data.question ? data.question.currentValue : null;
            const oldValue = data.question ? data.question.previousValue : null;
            const newLoopQuestion = Boolean(newValue && oldValue && newValue.loopQuestionId && newValue.loopQuestionId !== oldValue.loopQuestionId);
            const newQuestion = Boolean(newValue && oldValue && newValue.id !== oldValue.id);
            if (newLoopQuestion || newQuestion) {
                this.initialize();
            }
        }
    }

    public initialize(): void {
        this.answer = {};
        this.updateOptions();
        this.parseResponses();
        this.renderAnswers();
    }

    public updateOptions(): void {
        this.options = cloneDeep(this.question.options);
        if (this.isUserQuestion) {
            this.userOptions = map(this.options, (option: IQuestionOption): IOrgUserAdapted => {
                return {
                    Id: option.key,
                    FullName: option.value,
                };
            });
        }
    }

    public toggleNotSureAnswer(): void {
        this.answer.notSure = !this.answer.notSure;
        if (this.answer.notSure) {
            this.answer = {notSure: true, value: "", key: ""};
        }
        this.answerQuestion();
    }

    public addAnswer(option: any, isValid: boolean): void {
        if (this.isUserQuestion) {
            if (
                !isValid ||
                (!option && option !== "") ||
                (isString(option) && (!Utilities.matchRegex(option, Regex.EMAIL) && option !== "")) ||
                (this.answer && this.answer.value && option === this.answer.value) ||
                (this.answer && this.answer.key && option && option.Id && option.Id === this.answer.key)
            ) {
                return;
            }
            if (option && option.Id) {
                this.answer = { key: option.Id, value: option.FullName };
            } else if (isString(option)) {
                const existingOption: IOrgUserAdapted = find(this.userOptions, (questionOption: IOrgUserAdapted) => toLower(questionOption.Id) === toLower(option));
                if (existingOption) {
                    this.answer = { key: existingOption.Id, value: existingOption.FullName };
                } else {
                    this.answer = { value: option };
                }
            }
        } else {
            if (option && option.key) {
                this.answer = { key: option.key, value: option.value };
            } else if (option.InputValue) {
                const existingOption: IQuestionOption | undefined = find(this.options, (questionOption: IQuestionOption) => toLower(questionOption.value) === toLower(option.InputValue));
                if (existingOption) {
                    this.answer = { key: existingOption.key, value: existingOption.value };
                } else {
                    this.answer = { value: option.InputValue };
                }
            } else {
                this.answer = {};
            }
        }
        this.answerQuestion();
    }

    public answerQuestion(): void {
        this.$scope.$emit("ANSWER_QUESTION", this.question, this.answer);
    }

    private parseResponses(): void {
        if (isString(this.question.response)) {
            this.question.response = JSON.parse(this.question.response);
        }
    }

    private renderAnswers(): void | undefined {
        if (!this.question.response) return;
        if (this.question.response.notSure) {
            this.answer = {notSure: true};
            return;
        }
        if (this.question.response.singleSelectResponse) {
            const response: string = this.question.response.singleSelectResponse;
            const options: any[] = this.options;
            if (response && isArray(options) && options.length) {
                this.answer = find(options, { key: response } as any);
            }
            return;
        }
        if (isArray(this.question.response.otherResponses) && this.question.response.otherResponses.length) {
            this.answer = {value: this.question.response.otherResponses[0]};
        }
    }
}

const questionTypeaheadTemplate = `
    <section class="question-typeahead centered-max-70">
        <question-content question="$ctrl.question"></question-content>
        <typeahead
            ng-if="!$ctrl.isUserQuestion"
            class="question-typeahead__input"
            options="$ctrl.options"
            model="$ctrl.answer.value"
            on-select="$ctrl.addAnswer(selection)"
            type="$ctrl.question.questionType"
            label-key="value"
            has-arrow="true"
            is-disabled="$ctrl.readOnly"
            allow-empty="true"
            empty-text="- - - -"
            other-option-prepend="{{::$ctrl.translate('Add')}}"
            allow-other="$ctrl.question.allowOther"
            disable-if-no-options="!$ctrl.question.allowOther"
            scroll-parent="$ctrl.scrollParent"
            order-by-label="false"
            input-limit="2"
            use-virtual-scroll="true"
            options-limit="1000"
            >
        </typeahead>
        <org-user-dropdown
            ng-if="$ctrl.isUserQuestion"
            class="question-typeahead__input"
            user-options="$ctrl.userOptions"
            type="::$ctrl.OrgUserDropdownTypes.DMDetails"
            model="$ctrl.answer.key || $ctrl.answer.value"
            load-external-user-options="true"
            is-disabled="$ctrl.readOnly"
            allow-empty="true"
            empty-text="- - - -"
            allow-other="$ctrl.question.allowOther"
            other-option-prepend-text="{{::$ctrl.translate('Add')}}"
            hide-label="true"
            scroll-parent="$ctrl.scrollParent"
            on-select="$ctrl.addAnswer(selection, isValid)"
            >
        </org-user-dropdown>
        <div class="question-multichoice__wrapper margin-top-1">
            <one-button
                ng-if="$ctrl.question.allowNotSure"
                class="question-multichoice__btn-container"
                button-click="$ctrl.toggleNotSureAnswer()"
                text="{{::$ctrl.translate('NotSure')}}"
                type="select"
                is-selected="$ctrl.answer.notSure"
                is-disabled="$ctrl.readOnly"
                wrap-text="true"
                identifier="answerNotSure"
                >
            </one-button>
        </div>
    </section>
`;

const questionTypeaheadComponent: ng.IComponentOptions = {
    bindings: {
        question: "<",
        readOnly: "<",
        context: "<?",
    },
    controller: QuestionTypeahead,
    template: questionTypeaheadTemplate,
};

export default questionTypeaheadComponent;
