declare var d3: any;
import * as _ from "lodash";
import Utilities from "Utilities";

class OneDonutGraph implements ng.IComponentController {
    static $inject: string[] = ["$scope"];

    private donutName: string;
    private donutData: any;
    private graphId: string;
    private margin: number;
    private outerRadius: number;
    private innerRadius: number;
    private sortArcs: boolean;
    private centerLabel: string;
    private disablePercentage: boolean;
    private donutClickCallback: (value: any, index: number) => void;

    constructor(private $scope: any) { }

    public $onInit(): void {
        this.donutData = _.filter(this.donutData, (status: any): boolean => {
            return status.legendLabel;
        });
        this.renderSvg(this.donutName, this.donutData, this.graphId, this.margin, this.outerRadius, this.innerRadius, this.sortArcs, this.centerLabel, this.disablePercentage, this.donutClickCallback);
    }

    public $onChanges(): void {
        if (this.$scope.svg) {
            this.redrawSvg();
        }
    }

    public $onDestroy() {
        this.destroySvg();
    }

    private redrawSvg(): void {
        if (this.$scope.svg) {
            this.destroySvg();
            this.renderSvg(this.donutName, this.donutData, this.graphId, this.margin, this.outerRadius, this.innerRadius, this.sortArcs, this.centerLabel, this.disablePercentage, this.donutClickCallback);
        } else {
            this.renderSvg(this.donutName, this.donutData, this.graphId, this.margin, this.outerRadius, this.innerRadius, this.sortArcs, this.centerLabel, this.disablePercentage, this.donutClickCallback);
        }
    }

    private destroySvg() {
        this.$scope.svg.remove();
    }

    private renderSvg(donutName: any, dataSet: any, selectString: string, margin: number, outerRadius: number, innerRadius: number, sortArcs: boolean, centerlabel: string, disablePercentage: boolean, donutClickCallback: Function) {
        if (selectString) {
            this.$scope.selector = `${selectString ? selectString : "donut"}-${Utilities.uuid()}`;
        }

        if (!this.$scope.parent) {
            this.$scope.parent = d3.select("#donut").attr("id", this.$scope.selector);
        } else {
            this.$scope.parent.attr("id", this.$scope.selector);
        }

        const canvasWidth = 520;
        const pieWidthTotal: number = outerRadius * 1.1;
        const pieCenterX: number = outerRadius + margin / 2;
        const pieCenterY: number = outerRadius + margin / 2;
        const legendBulletOffset = 10;
        const legendVerticalOffset: number = outerRadius - margin;
        const legendTextOffset = 20;
        const textVerticalSpace = 20;

        let canvasHeight = 0;
        const pieDrivenHeight: number = outerRadius * 2 + margin * 2;
        const legendTextDrivenHeight: number = (dataSet.length * textVerticalSpace) + margin * 2;

        // Autoadjust Canvas Height
        if (pieDrivenHeight >= legendTextDrivenHeight) {
            canvasHeight = pieDrivenHeight;
        } else {
            canvasHeight = legendTextDrivenHeight;
        }

        const x: any = d3.scale.linear().domain([0, d3.max(dataSet, (d: any): number => {
            return d.magnitude;
        })]).rangeRound([0, pieWidthTotal]);
        const y: any = d3.scale.linear().domain([0, dataSet.length]).range([0, (dataSet.length * 20)]);

        // tslint:disable-next-line:only-arrow-functions
        const synchronizedMouseOver: any = function (d: any, i: number): void {
            const arc: any = d3.select(this);
            const indexValue: any = arc.attr("index_value");
            const arcSelector: string = "." + "pie-" + donutName + "-arc-" + indexValue;
            const selectedArc: any = d3.selectAll(arcSelector);

            const arcOver: any = d3.svg.arc()
                .innerRadius(innerRadius * 1.2) // Causes center of pie to be hollow
                .outerRadius(outerRadius);

            selectedArc.transition()
                .duration(300)
                .ease("elastic")
                .attr("d", arcOver);
        };

        // tslint:disable-next-line:only-arrow-functions
        const synchronizedMouseOut: any = function (d: any, i: number): void {
            const arc: any = d3.select(this);
            const indexValue: any = arc.attr("index_value");
            const arcSelector: string = "." + "pie-" + donutName + "-arc-" + indexValue;
            const selectedArc: any = d3.selectAll(arcSelector);

            const arcOver: any = d3.svg.arc()
                .innerRadius(innerRadius) // Causes center of pie to be hollow
                .outerRadius(outerRadius * 0.9);

            selectedArc.transition()
                .duration(200)
                .ease("exp")
                .attr("d", arcOver);
        };

        const synchronizedMouseClick: any = (d: any, i: number): void => {
            if (_.isFunction(donutClickCallback)) {
                donutClickCallback({ value: d.data, index: i });
            }
        };

        const arc: any = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius * 0.9);

        const tweenPie: any = (b: any): any => {
            b.innerRadius = 0;
            const i = d3.interpolate({
                startAngle: 0,
                endAngle: 0,
            }, b);
            return (t): any => {
                return arc(i(t));
            };
        };

        this.$scope.svg = d3.select(`#${this.$scope.selector}`)
            .append("svg")
            .data([dataSet])
            .attr("width", canvasWidth)
            .attr("height", canvasHeight)
            .attr("align", "center");

        const canvas: any = this.$scope.svg
            .append("g")
            .attr("transform", "translate(" + (160) + "," + (canvasHeight / 2) + ")");

        if (centerlabel) {
            const centerText = canvas
                .append("text")
                .attr("font-size", "3em")
                .attr("text-anchor", "middle")
                .style("font-weight", "bold")
                .text(centerlabel)
                // Disabled rule so that `getBBox` doesn't fail.
                // tslint:disable-next-line:only-arrow-functions
                .attr("transform", function (d: any): string {
                    return `translate(0, ${this.getBBox().height / 4})`;
                });
        }

        // Define a pie layout: the pie angle encodes the value of dataSet.
        // Since our data is in the form of a post-parsed CSV string, the
        // values are Strings which we coerce to Numbers.
        const pie: any = d3.layout.pie()
            .value((d: any): any => d.magnitude)
            .sort((a: any, b: any): (number | any) => {
                if (sortArcs) {
                    return b.magnitude - a.magnitude;
                } else {
                    return null;
                }
            });

        const arcs: any = canvas.selectAll("g.slice")
            .data(pie)
            .enter()
            .append("g")
            .attr("class", "slice")
            .attr("d", arc);

        arcs.append("path")
            .attr("fill", (d: any, i: number): string => {
                return d.data.color;
            })
            .attr("color_value", (d: any): string => {
                return d.data.color;
            }) // Bar fill color
            .attr("index_value", (d: any, i: number): string => {
                return "index-" + i;
            })
            .attr("class", (d: any, i: number): string => {
                return "pie-" + donutName + "-arc-index-" + i;
            })
            .attr("d", arc)
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut)
            .on("click", synchronizedMouseClick)
            .transition()
            .ease("exp")
            .duration(700)
            .attrTween("d", tweenPie);

        arcs.filter((d: any): boolean => {
            return d.endAngle - d.startAngle > .2;
        }).append("text")
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("transform", (d: any): string => {
                d.outerRadius = outerRadius;
                d.innerRadius = innerRadius;
                const c = arc.centroid(d);
                return "translate(" + c[0] * 1.1 + "," + c[1] * 1.1 + ")";
            })
            .style("fill", "White")
            .style("font-weight", "bold")
            .text((d: any): string => {
                if (disablePercentage) {
                    return d.data.count;
                }
                return (d.data.percentage + "%");
            })
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut)
            .on("click", synchronizedMouseClick);

        function angle(d: any): number {
            const a: number = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
            return a > 90 ? a - 180 : a;
        }

        canvas.selectAll("legend")
            .data(dataSet).enter().append("svg:circle")
            .attr("cx", pieWidthTotal + legendBulletOffset)
            .attr("cy", (d: any, i: number): number => {
                return i * textVerticalSpace - legendVerticalOffset;
            })
            .attr("stroke-width", ".5")
            .style("fill", (d: any, i: number): string => {
                return d.color;
            }) // Bullet fill color
            .attr("r", 5)
            .attr("color_value", (d: any): string => {
                return d.color;
            }) // Bar fill color
            .attr("index_value", (d: any, i: number): string => {
                return "index-" + i;
            })
            .attr("class", (d: any, i: number): string => {
                return "pie-" + donutName + "-legendBullet-index-" + i;
            })
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut)
            .on("click", synchronizedMouseClick);

        canvas.selectAll(".legend_link")
            .data(dataSet)
            .enter()
            .append("text")
            .attr("text-anchor", "center")
            .attr("x", pieWidthTotal + legendBulletOffset + legendTextOffset)
            .attr("y", (d: any, i: number): number => {
                return i * textVerticalSpace - legendVerticalOffset;
            })
            .attr("dx", 0)
            .attr("dy", "5px")
            .text((d: any): string => {
                return d.legendLabel.toUpperCase();
            })
            .attr("color_value", (d: any): string => {
                return d.color;
            })
            .attr("index_value", (d: any, i: number): string => {
                return "index-" + i;
            })
            .attr("class", (d: any, i: number): string => {
                return "pie-" + donutName + "-legendText-index-" + i;
            })
            .on("mouseover", synchronizedMouseOver)
            .on("mouseout", synchronizedMouseOut)
            .on("click", synchronizedMouseClick);
    }
}

const oneDonutGraph = {
    bindings: {
        donutName: "<",
        donutData: "<",
        graphId: "@?",
        margin: "<",
        outerRadius: "<",
        innerRadius: "<",
        sortArcs: "<",
        centerLabel: "@?",
        disablePercentage: "<?",
        donutClickCallback: "&?",
    },
    controller: OneDonutGraph,
    template: () => {
        return `
                    <div id="donut"></div>
                    `;
    },
};

export default oneDonutGraph;
