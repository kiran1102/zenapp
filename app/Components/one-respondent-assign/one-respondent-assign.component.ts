import { ITenant } from "interfaces/tenant.interface";
import { IUser } from "interfaces/user.interface";
import Utilities from "Utilities";
import TreeMap from "TreeMap";
import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { toLower, find } from "lodash";

class OneRespondentAssignController implements ng.IComponentController {
    static $inject: string[] = ["$rootScope", "Users", "ENUMS", "OrgGroupStoreNew"];

    private showMultiRespondent: false;
    private placeholder: string;
    private tooltipText: string;
    private emailRegExp: string;
    private guidRegExp: string;

    private currentUser: IUser;
    private users: IUser[];
    private disabledUserList: IUser[];
    private isTypedEmailDisabledUser: boolean;
    private project: any;
    private updateParent: any;
    private unsubscribe: any;
    private treeMap: any;

    constructor(
        readonly $rootScope: IExtendedRootScopeService,
        readonly Users: any,
        ENUMS: any,
        private readonly OrgGroupStoreNew: any,
    ) {
        this.emailRegExp = ENUMS.Regex.Email;
        this.guidRegExp = ENUMS.Regex.Guid;
    }

    public $onInit() {
        this.isTypedEmailDisabledUser = false;
        this.disabledUserList = this.Users.getDisabledUsers(this.users);
        this.users = this.Users.getSomeoneElseAssignments(this.users);
        this.placeholder = this.project.Lead || this.$rootScope.t("AssignProjectRespondent");
        this.tooltipText = this.$rootScope.t("OneRespondentAssignTooltip");
        this.unsubscribe = this.OrgGroupStoreNew.subscribe(() => {
            this.treeMap = new TreeMap("Id", "ParentId", "Children", this.OrgGroupStoreNew.tenantOrgTree);
        });
    }

    public $onDestroy() {
        this.unsubscribe();
    }
    private assignRespondent(user: IUser): void {
        this.showMultiRespondent = false;
        this.project.LeadId = user.Id;
        this.project.Lead = user.FullName;
        this.placeholder = user.FullName;
        this.updateParent();
    }

    private someoneElseButtonClicked(): void {
        this.project.LeadId = "";
        this.project.Lead = "";
        this.placeholder = this.$rootScope.t("AssignProjectRespondent");
        this.updateParent();
    }

    private blur(event: any): void {
        const respondentEmail: string = event.target.value;
        this.isTypedUserEmailDisabled(respondentEmail);

        const id: string = this.project.LeadId || respondentEmail;
        const isInvitedUser: boolean = Utilities.findByIdReturnBoolean(this.users, id);
        const isValidEmail: boolean = Utilities.matchRegex(respondentEmail, this.emailRegExp);

        if (!isInvitedUser && isValidEmail && !this.isTypedEmailDisabledUser) {
            this.project.LeadId = respondentEmail;
            this.project.Lead = respondentEmail;
            this.placeholder = this.project.Lead;
            this.updateParent();
        } else if (!isInvitedUser && !isValidEmail || this.isTypedEmailDisabledUser) {
            this.someoneElseButtonClicked();
        }
    }

    private formatLeadLabel(model: any): string {
        if (model) {
            if (this.currentUser &&
                this.currentUser.Id &&
                (this.currentUser.Id === model)) {
                return this.Users.formatTeamLabel(this.users, model);
            }
            if (Utilities.matchRegex(model, this.guidRegExp)) {
                return this.Users.formatTeamLabel(this.users, model);
            }
            if (Utilities.matchRegex(model, this.emailRegExp)) {
                return model;
            }
        }
        return "";
    }

    private isTypedUserEmailDisabled(typedEmail: string): void {
        const typedDisabledUser: IUser = find(this.disabledUserList, (specificUser: IUser): boolean => {
            return (toLower(specificUser.Email) === toLower(typedEmail));
        });
        this.isTypedEmailDisabledUser = Boolean(typedDisabledUser);
    }

}

const OneRespondentAssign: ng.IComponentOptions = {
    bindings: {
        project: "<",
        currentUser: "<",
        users: "<",
        updateParent: "&",
    },
    controller: OneRespondentAssignController,
    template: `
        <section class="project-info__form-group">
            <label class="project-info__label">
                {{ ::$root.t("Respondent") }}
                <sup class="fa fa-asterisk"></sup>
                <i
                    class="fa fa-info-circle margin-none",
                    uib-tooltip="{{ ::$ctrl.tooltipText }}",
                    tooltip-append-to-body="true",
                    tooltip-placement="top"></i>
                <div
                    class="warning-text"
                    data-ng-show="(projectInfoForm.Owner.$touched || projectInfoForm.Owner.$dirty) &amp;&amp; (projectInfoForm.Owner.$error.required || formatLeadLabel($ctrl.project.LeadId) === '')"></div>
            </label>
            <div
                class="project-info__input-group"
                data-ng-disabled="$ctrl.showMultiRespondent">
                <div
                    class="project-info__assignment one-toggle-picker"
                    data-ng-class="{'is-disabled': !$ctrl.project.OrgGroupId || $ctrl.project.LeadId === $ctrl.currentUser.Id}">
                    <div class="project-info__assignment-toggle one-toggle-picker__toggle">
                        <button
                            class="project-info__assignment-toggle-btn one-toggle-picker__toggle-btn"
                            data-ng-model="type"
                            type="button"
                            data-ng-class="{'selected': $ctrl.project.LeadId === $ctrl.currentUser.Id}"
                            data-ng-click="$ctrl.assignRespondent($ctrl.currentUser)"
                            data-ng-disabled="!$ctrl.project.OrgGroupId || $ctrl.showMultiRespondent">
                            {{ ::$root.t("Me") }}
                        </button>
                        <button
                            class="project-info__assignment-toggle-btn one-toggle-picker__toggle-btn"
                            data-ng-model="type"
                            type="button"
                            data-ng-class="{'selected': $ctrl.project.LeadId !== $ctrl.currentUser.Id}"
                            data-ng-click="$ctrl.someoneElseButtonClicked()"
                            data-ng-disabled="!$ctrl.project.OrgGroupId || $ctrl.showMultiRespondent">
                            {{ ::$root.t("SomeoneElse") }}
                        </button>
                    </div>
                    <input
                        class="project-info__assignment-input one-toggle-picker__input one-input full-width block"
                        data-ng-blur="$ctrl.blur($event)"
                        data-ng-class="{'respondent-disabled': $ctrl.showMultiRespondent}"
                        data-ng-disabled="!$ctrl.project.OrgGroupId || $ctrl.project.LeadId === $ctrl.currentUser.Id"
                        data-ng-model="$ctrl.project.LeadId"
                        focus-me="$ctrl.project.LeadId !== $ctrl.currentUser.Id"
                        id="newProjectOwner"
                        name="Owner"
                        placeholder="{{$ctrl.placeholder}}"
                        type="text"
                        typeahead-input-formatter="$ctrl.formatLeadLabel($model)"
                        typeahead-min-length="0"
                        typeahead-show-hint="true"
                        typeahead-on-select="$ctrl.assignRespondent($item)"
                        uib-typeahead="user.Id as user.FullName for user in $ctrl.users | filter:$viewValue | IsInParentTree:$ctrl.treeMap:$ctrl.project.OrgGroupId:'OrgGroupId'"/>
                    </div>
                    <p
                        class="project-info__error"
                        data-ng-show="$ctrl.isTypedEmailDisabledUser || (projectInfoForm.Owner.$touched || projectInfoForm.Owner.$dirty) &amp;&amp; (projectInfoForm.Owner.$error.required || formatLeadLabel($ctrl.project.LeadId) === '') &amp;&amp; !$ctrl.showMultiRespondent">
                        {{error.lead || 'Please assign a valid '+$root.customTerms.Project+' Respondent'}}.
                    </p>
                </div>
            </section>`,
};

export default OneRespondentAssign;
