const newItemFieldTemplate = `
    <button
        class="new-item-field {{$ctrl.fieldClass}}"
        ng-click="$ctrl.handleClick()"
        >
            <i
                class="new-item-field__icon fa {{$ctrl.iconClass}}"
                ng-if="$ctrl.iconClass"
                >
            </i>
            {{$ctrl.text}}
    </button>
`;

const newItemFieldComponent = {
    template: newItemFieldTemplate,
    bindings: {
        text: "@",
        fieldClass: "@?",
        iconClass: "@?",
        handleClick: "&",
    },
};

export default newItemFieldComponent;
