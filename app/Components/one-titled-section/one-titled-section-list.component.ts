
const OneSectionListTemplate = `
        <one-titled-section
            section-title="{{::$ctrl.sectionTitle}}"
            section-icon="{{::$ctrl.sectionIcon}}"
            >
            <ul
                class="one-titled-section-list margin-top-1"
                ng-if="$ctrl.sectionList"
                >
                <one-configurable-item
                    ng-repeat="item in $ctrl.sectionList track by $index"
                    class="
                        one-titled-section-list__item margin-bottom-1
                        {{ item.modifierClass ? 'one-titled-section-list__item--' + item.modifierClass : '' }}
                    "
                    item="item"
                    item-first="$first"
                    item-last="$last"
                    item-index="$index"
                    item-editable="$ctrl.editable"
                    >
                </one-configurable-item>
                <span class="spacer one-titled-section-list__item"></span>
                <span class="spacer one-titled-section-list__item"></span>
                <span class="spacer one-titled-section-list__item"></span>
            </ul>
        </one-titled-section>
    `;

const OneSectionListComponent: any = {
    template: OneSectionListTemplate,
    bindings: {
        sectionTitle: "@?",
        sectionIcon: "@?",
        sectionList: "<",
        editable: "<?",
    },
};

export default OneSectionListComponent;
