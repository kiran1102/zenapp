const OneTitledSectionTemplate = `
        <div class="one-titled-section">
            <one-titled-section-header
                class="one-titled-section__header"
                section-title="{{$ctrl.sectionTitle}}"
                section-icon="{{$ctrl.sectionIcon}}"
                >
            </one-titled-section-header>
            <div class="one-titled-section__body" ng-transclude>
            </div>
        </div>
    `;

const OneTitledSectionComponent: any = {
    template: OneTitledSectionTemplate,
    bindings: {
        sectionTitle: "@?",
        sectionIcon: "@?",
    },
    transclude: true,
};

export default OneTitledSectionComponent;
