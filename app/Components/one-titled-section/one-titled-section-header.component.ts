
const OneTitledSectionHeaderTemplate = `
        <div class="one-titled-section-header">
            <i
                ng-if="$ctrl.sectionIcon"
                class="one-titled-section-header__icon {{$ctrl.sectionIcon}}"
                >
            </i>
            <span
                ng-if="$ctrl.sectionTitle"
                class="one-titled-section-header__text text-bold"
                ng-bind="$ctrl.sectionTitle"
                >
            </span>
        </div>
    `;

const OneTitledSectionHeaderComponent: any = {
    template: OneTitledSectionHeaderTemplate,
    bindings: {
        sectionTitle: "@?",
        sectionIcon: "@?",
    },
};

export default OneTitledSectionHeaderComponent;
