import * as _ from "lodash";
import { AnyAction } from "redux";

class OneOrgDropdown {

    private orgList: any;
    private selectedOrg: any;
    private alignRight: boolean;
    private actionCallback: (action: any) => void;

    private orgSelected(org: any): void {
        if (_.isFunction(this.actionCallback)) {
            this.actionCallback({ action: "ORG_SELECTED", payload: { org } });
        }
    }
}

const oneOrgDropdownComponent: ng.IComponentOptions = {
    controller: OneOrgDropdown,
    bindings: {
        identifier: "@?",
        orgList: "<",
        actionCallback: "&?",
        selectedOrg: "<?",
        alignRight: "<?",
    },
    template: `
        <ul
            uib-dropdown-menu
            role="menu"
            ng-class="{ 'dropdown-menu-right': $ctrl.alignRight }"
            class="one-org-dropdown__list z-index-9999"
        >
            <li
                ng-repeat="org in $ctrl.orgList track by org.id"
                ng-class="{ 'selected': $ctrl.selectedOrg.id === org.id }"
                ng-click="$ctrl.orgSelected(org)"
                ot-auto-id="{{$ctrl.identifier +'_'+ org.id }}"
                class="one-org-dropdown__list-item"
            >
                <span
                    ng-if="org.childrenIds.length"
                    ng-style="{ 'padding-left': org.level + 'rem' }"
                    class="one-org-dropdown__list-item-icon fa fa-caret-down"></span>
                <span
                    ng-if="!org.childrenIds.length"
                    ng-style="{ 'padding-left': org.level + 'rem' }"
                    class="one-org-dropdown__list-item-icon bullet"></span>
                {{org.name}}
            </li>
        </ul>
    `,
};

export default oneOrgDropdownComponent;
