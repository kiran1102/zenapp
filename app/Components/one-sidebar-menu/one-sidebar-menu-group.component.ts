export default {
    bindings: {
        menuGroup: "<",
        config: "<?",
    },
    template: `
        <div
            ng-if="::$ctrl.menuGroup.name"
            class="one-sidebar-menu__title text-small text-bold padding-top-bottom-half padding-left-right-2 row-vertical-center"
            ng-click="$ctrl.menuGroup.isToggled = !$ctrl.menuGroup.isToggled"
            >
            <span class="text-bold {{$ctrl.config.menuTitleClass}}" ng-bind="$ctrl.menuGroup.name"></span>
            <span
                ng-if="$ctrl.menuGroup.canToggle"
                class="margin-left-auto"
                ng-class="{
                    'ot ot-chevron-down': $ctrl.menuGroup.isToggled,
                    'ot ot-chevron-right': !$ctrl.menuGroup.isToggled
                }"
            ></span>
        </div>
        <one-sidebar-menu-item
            ng-if="!$ctrl.menuGroup.canToggle || ($ctrl.menuGroup.canToggle && $ctrl.menuGroup.isToggled)"
            ng-repeat="link in $ctrl.menuGroup.links track by $index"
            menu-item="link"
        ></one-sidebar-menu-item>
    `,
};
