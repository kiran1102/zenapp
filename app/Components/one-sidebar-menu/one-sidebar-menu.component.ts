import { IExtendedRootScopeService } from "interfaces/root-scope.interface";
import { IMenuGroup, IMenuLink, IMenuConfig } from "interfaces/one-menu.interface";
import { map } from "lodash";

class OneSidebarMenuCtrl implements ng.IComponentController {
    static $inject: string[] = ["$rootScope"];

    private menu: IMenuGroup[];
    private pinnedMenu: IMenuGroup[];

    constructor(private readonly $rootScope: IExtendedRootScopeService) {}

    public $onChanges(changes: ng.IOnChangesObject): void {
        if (changes.menu) {
            this.menu = this.modifyMenuForRendering(this.menu);
        }
        if (changes.pinnedMenu) {
            this.pinnedMenu = this.modifyMenuForRendering(this.pinnedMenu);
        }
    }

    private modifyMenuForRendering(menu: IMenuGroup[]): IMenuGroup[] {
        return map(menu, (group: IMenuGroup): IMenuGroup => {
            group = this.applyNameTranslation(group);
            group.links = this.modifyLinksForRendering(group.links);
            return group;
        });
    }

    private modifyLinksForRendering(links: IMenuLink[]): IMenuLink[] {
        return map(links, (link: IMenuLink): IMenuLink => {
            link = this.applyNameTranslation(link);
            if (!link.activeRoute && link.route) {
                link.activeRoute = link.route;
            }
            return link;
        });
    }

    private applyNameTranslation(item: any): any {
        if (!item.name && item.nameKey) {
            item.name = this.$rootScope.t(item.nameKey);
        }
        return item;
    }
}

export default {
    bindings: {
        config: "<",
        menu: "<",
        pinnedMenu: "<?",
        menuTitle: "@?",
    },
    controller: OneSidebarMenuCtrl,
    template: `
        <section
            class="
                one-sidebar-menu
                height-100
                column-nowrap
                overflow-y-auto
                border-right
                {{$ctrl.config.menuClass}}
            "
            ng-class="{
                'is-open': $ctrl.config.isOpen,
                'beta-ui': $ctrl.config.betaUI,
            }">
            <div
                class="
                    one-sidebar-menu__header
                    static-vertical
                    border-bottom
                    padding-all-2
                    text-uppercase
                "
                ng-class="{
                    'row-vertical-center': $ctrl.menuTitle,
                    'display-mobile-only': !$ctrl.menuTitle
                }"
                >
                <span
                    ng-if="::$ctrl.menuTitle"
                    class="text-bold {{$ctrl.config.menuTitleClass}}"
                    ng-bind="$ctrl.menuTitle"
                ></span>
                <span
                     ng-if="$ctrl.config.isOpen"
                     class="text-bold display-mobile-only ot ot-times margin-left-auto pull-right"
                     ng-click="$ctrl.config.toggleMenu()"
                 ></span>
            </div>
            <div class="column-space-between stretch-vertical">
                <div class="height-100 position-relative">
                    <one-sidebar-menu-group
                        ng-if="$ctrl.menu"
                        ng-repeat="menuGroup in $ctrl.menu track by $index"
                        menu-group="menuGroup"
                    ></one-sidebar-menu-group>
                </div>
                <div class="column-nowrap">
                    <one-sidebar-menu-group
                        ng-if="$ctrl.pinnedMenu"
                        ng-repeat="menuGroup in $ctrl.pinnedMenu track by $index"
                        menu-group="menuGroup"
                    ></one-sidebar-menu-group>
                </div>
            </div>
        </section>
    `,
};
