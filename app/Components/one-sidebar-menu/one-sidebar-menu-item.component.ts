import { IMenuLink } from "interfaces/one-menu.interface";
class OneSidebarMenuItemCtrl implements ng.IComponentController {
    static $inject: string[] = ["$state"];

    private url: string;
    private menuItem: IMenuLink;

    constructor(private readonly $state: ng.ui.IStateService) {}

    public $onChanges(): void {
        if (this.menuItem && this.menuItem.route) {
            this.url = `${this.menuItem.route}(${JSON.stringify(this.menuItem.params) || ""})`;
            this.menuItem.options = {inherit: false, ...this.menuItem.options };
        }
    }
}

export default {
    bindings: {
        menuItem: "<",
    },
    controller: OneSidebarMenuItemCtrl,
    template: `
        <div
            class="one-sidebar-menu-link row-vertical-center border-bottom"
            ng-class="{'active': $ctrl.menuItem.getIsActive ? $ctrl.menuItem.getIsActive() : $ctrl.$state.includes($ctrl.menuItem.activeRoute)}"
            >
            <a
                ng-if="::$ctrl.menuItem.route"
                class="one-sidebar-menu-link__anchor row-vertical-center full-width padding-top-bottom-1 padding-left-right-2"
                ui-sref="{{$ctrl.url}}"
                ui-sref-opts="$ctrl.menuItem.options"
                >
                <span
                    ng-if="::$ctrl.menuItem.icon"
                    class="padding-right-1 {{$ctrl.menuItem.icon}}"
                ></span>
                <span ng-bind="$ctrl.menuItem.name"></span>
                <span ng-if="::$ctrl.menuItem.isUpgrade" class="upgrade__icon"></span>
            </a>
            <a
                ng-if="$ctrl.menuItem.action"
                class="one-sidebar-menu-link__anchor row-vertical-center full-width padding-top-bottom-1 padding-left-right-2"
                ng-click="$ctrl.menuItem.action({item: $ctrl.menuItem})"
                >
                <span
                    ng-if="::$ctrl.menuItem.icon"
                    class="padding-right-1 {{::$ctrl.menuItem.icon}}"
                ></span>
                <span ng-bind="$ctrl.menuItem.name"></span>
                <span ng-if="::$ctrl.menuItem.isUpgrade" class="upgrade__icon"></span>
            </a>
        </div>
    `,
};
