import $ from "jquery";
import JQuery from "jquery";

interface ICellStyle {
    "border-bottom-left-radius"?: string;
    "border-bottom-right-radius"?: string;
    "border-top-right-radius"?: string;
    "border-top-left-radius"?: string;
    "top"?: string;
    "bottom"?: string;
}

class ScrollableCellCtrl implements ng.IComponentController {
    static $inject: string[] = ["$element", "$document"];

    private containerEl: Element;
    private content: string;
    private cellStyle: ICellStyle = {};
    private windowOpen = false;
    private windowActive = false;

    constructor(private readonly $element: JQuery, private readonly $document: JQuery) {}

    $onInit(): void {
        this.containerEl = this.$element[0].children[0];
    }

    private setStyles(event: ng.IAngularEvent): void {
        const hasOverflow: boolean = this.containerEl.scrollWidth > this.containerEl.clientWidth;
        const distanceFromTop: number = this.$element.offset().top;
        const screenHeight: number = this.$document.height();
        const overflowDirection: string = screenHeight - distanceFromTop > 250 ? "bottom" : "top";
        if (hasOverflow) {
            this.cellStyle[overflowDirection] = "auto";
            this.cellStyle[`border-${overflowDirection}-left-radius`] = "2px";
            this.cellStyle[`border-${overflowDirection}-right-radius`] = "2px";
            this.windowOpen = true;
        }
    }

    private setWindowActive(): void {
        this.windowActive = true;
    }

    private checkIfShouldClose(): void {
        this.windowOpen = this.windowActive;
    }

    private closeWindow(): void {
        this.windowActive = false;
        this.windowOpen = false;
        this.cellStyle = {};
    }

}

export default {
    bindings: {
        isDisabled: "<?",
    },
    transclude: {
        content: "?cellContent",
    },
    controller: ScrollableCellCtrl,
    template: `
        <span
            class="text-nowrap overflow-ellipsis"
            ng-mouseover="$ctrl.isDisabled || $ctrl.setStyles()"
            ng-mouseout="$ctrl.isDisabled || $ctrl.checkIfShouldClose()"
            >
            <span ng-transclude="content"></span>
            <div
                ng-if="!$ctrl.isDisabled"
                class="scrollable-cell--active position-absolute-full padding-all-1 background-white z-index-9999 shadow overflow-y-auto text-wrap text-left"
                ng-style="$ctrl.cellStyle"
                ng-show="$ctrl.windowOpen"
                ng-mouseover="$ctrl.setWindowActive()"
                ng-mouseout="$ctrl.closeWindow()"
                ng-transclude="content"
                >
            </div>
        </span>
    `,
};
