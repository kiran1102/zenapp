import isFunction from "lodash/isFunction";

class OneInputController implements ng.IComponentController {
    private inputChanged: (arg: any) => any;
    private inputBlurred: (arg: any) => any;
    private inputFocused: (arg: any) => any;
    private inputText: string;
    private placeholder: string;
    private tempModel: string;
    private inputClass: string;

    handleChange(value: string): void {
        if (isFunction(this.inputChanged)) {
            this.inputChanged({ value });
        }
    }

    handleBlur(value: string): void {
        if (isFunction(this.inputBlurred)) {
            this.inputBlurred({ value });
        }
    }

    handleFocus(value: string): void {
        if (isFunction(this.inputFocused)) {
            this.inputFocused({ value });
        }
    }
}

export const OneInput: ng.IComponentOptions = {
    controller: OneInputController,
    bindings: {
        identifier: "@?",
        inputChanged: "&?",
        inputFocused: "&?",
        inputBlurred: "&?",
        inputText: "@?",
        maxlength: "<?",
        placeholder: "@?",
        inputClass: "@?",
        type: "@?",
        isRequired: "<?",
        isDisabled: "<?",
    },
    template: `
        <input
            data-id="{{$ctrl.identifier}}"
            class="one-input full-width full-height"
            maxlength="{{::$ctrl.maxlength}}"
            ng-value="$ctrl.inputText"
            ng-model="$ctrl.tempModel"
            ng-change="$ctrl.handleChange($ctrl.tempModel)"
            ng-focus="$ctrl.handleFocus($ctrl.tempModel)"
            ng-blur="$ctrl.handleBlur($ctrl.tempModel)"
            placeholder="{{$ctrl.placeholder}}"
            ng-class="$ctrl.inputClass"
            type="{{$ctrl.type}}"
            ng-required="$ctrl.isRequired"
            ng-disabled="$ctrl.isDisabled"
        >
        </input>
    `,
};
