import _ from "lodash";

class OneRiskTileButton implements ng.IComponentController {

    static $inject: any = [];

    private onChange: (arg: any) => any;
    private isDropdownOpen: boolean;
    private isTileSelected: boolean;
    private selectedModel: any;

    private tileMap: any[];
    private axisLabels: any;

    public $onInit(): void {
        if (this.selectedModel) {
            this.isTileSelected = true;
        }
    }

    private handleDropdownToggle(isOpen: boolean): void {
        this.isDropdownOpen = isOpen;
    }

    private handleTileClick(tile: any): void {
        this.isTileSelected = true;
        if (_.isFunction(this.onChange)) {
            this.onChange({ value: tile });
        }
    }
}

const OneRiskTileButtonComponent: ng.IComponentOptions = {
    controller: OneRiskTileButton,
    bindings: {
        onChange: "&?",
        axisLabels: "<?",
        tileMap: "<?",
        selectedModel: "<?",
        isDisabled: "<?",
    },
    template: `
        <div
            class="risk-tile-button flex"
            uib-dropdown
            auto-close="always outsideClick"
            on-toggle="$ctrl.handleDropdownToggle(open)"
        >
            <button
                class="risk-tile-button__button margin-right-1"
                ng-class="{
                    'shadow-on-hover': !$ctrl.isDisabled,
                    'risk-tile-button__button--disabled': $ctrl.isDisabled
                }"
                uib-dropdown-toggle
                ng-disabled="$ctrl.isDisabled"
            >
                <div class="risk-tile-button__container full-width flex-full-height">
                    <div
                        class="risk-tile-button__color-preview column-nowrap row-flex-end"
                        ng-class="$ctrl.selectedModel.htmlClass"
                    >
                        <div class="risk-tile-button__arrow-container row-horizontal-vertical-center">
                            <i
                                ng-if="!$ctrl.isDropdownOpen"
                                ng-class="{'white-arrow': $ctrl.isTileSelected }"
                                class="risk-tile-button__arrow-icon fa fa-chevron-down"
                            ></i>
                            <i
                                ng-if="$ctrl.isDropdownOpen"
                                ng-class="{'white-arrow': $ctrl.isTileSelected }"
                                class="risk-tile-button__arrow-icon fa fa-chevron-up"
                            ></i>
                        </div>
                    </div>
                </div>
            </button>
            <div
                ng-if="$ctrl.isTileSelected"
                class="risk-tile-button__risk-status column-vertical-center full-width"
            >
                <div class="">
                    {{$ctrl.axisLabels.yAxis[$ctrl.selectedModel.yValue - 1]}}
                </div>
                <div class="">
                    {{$ctrl.axisLabels.xAxis[$ctrl.selectedModel.xValue - 1]}}
                </div>
            </div>
            <div
                ng-if="!$ctrl.isTileSelected"
                class="risk-tile-button__risk-status row-vertical-center full-width text-italic"
            >
                {{::$root.t("ChooseRiskLevel")}}
            </div>
            <one-risk-tile-dropdown
                ng-class="{ 'open': $ctrl.isDropdownOpen }"
                on-tile-select="$ctrl.handleTileClick(value)"
                axis-labels="$ctrl.axisLabels"
                tile-map="$ctrl.tileMap"
            >
            </one-risk-tile-dropdown>
        </div>
    `,
};

export default OneRiskTileButtonComponent;
