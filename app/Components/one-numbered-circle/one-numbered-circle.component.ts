export default {
    bindings: {
        number: "<?",
        size: "@?",
        type: "@?",
        circleClass: "@?",
        icon: "@?",
    },
    template: `
        <div
            class="one-numbered-circle row-horizontal-vertical-center text-center inline-block text-bold {{::$ctrl.circleClass}}"
            ng-class="{
                'one-numbered-circle--primary': $ctrl.type === 'primary',
                'one-numbered-circle--success': $ctrl.type === 'success',
                'one-numbered-circle--warning': $ctrl.type === 'warning',
                'one-numbered-circle--danger': $ctrl.type === 'danger',
                'one-numbered-circle--info': $ctrl.type === 'info',
                'one-numbered-circle--small': $ctrl.size === 'small',
                'one-numbered-circle--medium': $ctrl.size === 'medium',
                'one-numbered-circle--large': $ctrl.size === 'large',
            }">
            <span class="{{::$ctrl.icon}}">{{::$ctrl.number}}</span>
        </div>
    `,
};
