// Rxjs
import {
    Subscription,
    Subject,
} from "rxjs";
import {
    takeUntil,
    startWith,
} from "rxjs/operators";

// Third Party
import { isFunction } from "lodash";

// Interfaces
import { IWindowObservablesService } from "interfaces/window-observables.interface";
import {
    IStoreState,
    IStore,
} from "interfaces/redux.interface";

// Redux
import observableFromStore from "oneRedux/utils/observable-from-store";
import { getDrawerState } from "oneRedux/reducers/drawer.reducer";

// Services
import { DrawerActionService } from "oneServices/actions/drawer-action.service";

class SideDrawerController implements ng.IComponentController {

    static $inject: string[] = [
        "store",
        "$timeout",
        "WindowObservables",
        "DrawerActionService",
    ];

    private subscription: Subscription;
    private isDrawerOpen: boolean;
    private defaultOpen: boolean;
    private containerHeight: string;
    private toggleCb: (arg: any) => any;
    private resizeSubscription: Subscription;
    private destroy$ = new Subject();

    constructor(
        private readonly store: IStore,
        readonly $timeout: ng.ITimeoutService,
        readonly WindowObservables: IWindowObservablesService,
        private readonly drawerActionService: DrawerActionService,
    ) {}

    public $onInit(): void {
        observableFromStore(this.store).pipe(
            takeUntil(this.destroy$),
            startWith(this.store.getState()),
        ).subscribe((state: IStoreState): void => this.componentWillReceiveState(state));
        this.resizeSubscription = this.WindowObservables.get("RESIZE").subscribe(() => this.resizeDrawer());
        this.$timeout((): void => {
            this.resizeDrawer();
        }, 0, true);
        if (this.defaultOpen) this.drawerActionService.openDrawer();
    }

    public $onDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private componentWillReceiveState(state: IStoreState): void {
        this.isDrawerOpen = getDrawerState(state);
    }

    private toggleDrawer(): void {
        if (isFunction(this.toggleCb)) {
            this.toggleCb();
        } else {
            if (this.isDrawerOpen) {
                this.drawerActionService.closeDrawer();
            } else {
                this.drawerActionService.openDrawer();
            }
        }
    }

    private resizeDrawer(): void {
        if (document.getElementsByTagName("side-drawer")[0] && document.getElementsByTagName("side-drawer")[0].parentElement) {
            this.containerHeight = `${document.getElementsByTagName("side-drawer")[0].parentElement.clientHeight}px`;
            document.getElementById("side-drawer-wrapper").style.height = this.containerHeight;
        } else if (document.getElementsByTagName("upgraded-side-drawer")[0] && document.getElementsByTagName("upgraded-side-drawer")[0].parentElement) {
            this.containerHeight = `${document.getElementsByTagName("upgraded-side-drawer")[0].parentElement.clientHeight}px`;
            document.getElementById("side-drawer-wrapper").style.height = this.containerHeight;
        }
    }
}

export default {
    bindings: {
        offscreenHide: "<?",
        showingDrawer: "<?",
        defaultOpen: "<?",
        toggleCb: "<?",
    },
    controller: SideDrawerController,
    transclude: true,
    template: `
        <div class="side-drawer"
            ng-class="$ctrl.isDrawerOpen ? 'side-drawer--open' : ($ctrl.offscreenHide ? 'side-drawer--close-full' : 'side-drawer--close')"
            id="side-drawer-wrapper"
            >
            <div class="side-drawer__contents full-size column-nowrap">
                <div class="side-drawer__contents--header row-vertical-center margin-all-2 static-vertical">
                    <i
                        class="side-drawer__icon fa fa-arrow-circle-o-left"
                        ng-click="$ctrl.toggleDrawer()">
                    </i>
                    <span class="side-drawer__close-text margin-left-1">{{$root.t("Close")}}</span>
                </div>
                <ng-transclude class="side-drawer__contents--body block stretch-vertical height-100"></ng-transclude>
            </div>
        </div>
    `,
};
