# OneTrust Web Application

## Development

Run `npm run register` to set and login to the OneTrust npm registry hosted at `https://nexus.onetrust.com/nexus/repository/npm-group/`. This is required before running `npm install`.
Once you have registered, install packages via `npm install`.
For Developers using a Mac, you may need to run `brew install libpng` for the Webpack Image Loader.

If you have had to updated your Node version to the LTS version matching the the specifications in `package.json`, the please run `npm rebuild node-sass`.

## Commits

All commits must include a JIRA Ticket number or the remote pre-recieve hook will reject the commit.
Upon commit, the pre-commit hook will run and lint the application. If this fails, the commit will be rejected.

## Serve

Run `npm run serve:{env}` to serve a particular environment on your local machine.

Find the supported environments in the `package.json`.

To add more environments, go to package.json and add the following code:
`"serve:YOUR_NEW_ENV": "npm run serve -- --env.dev=YOUR_NEW_ENV_URL",`
e.g. `"serve:lkgb": "npm run serve -- --env.dev=https://lkgb-app.qa.otdev.org",`

To serve to it, now use `npm run serve:lkgb`

## Scripts

### Build Licenses

This script will iterate over all dependencies in the node_modules and create a licenses report for them.

```bash
npm run build:licenses
```

### Version

This script will increment the version of the application based on supplied parameters and commit the changes thereof.
The version paramaters can be one of the following:

* <newversion>
* major
* minor
* patch
* premajor
* preminor
* prepatch
* prerelease
* from-git

This is then followed by a commit message. This is done by passing in `-m` followed by a message.
Note that a ticket must be supplied in this message, as with all commits. The message allows for including the new version via the `%s` variable.

```bash
npm run update:version minor CORE-3294
```
