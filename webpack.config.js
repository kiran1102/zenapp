const merge = require("webpack-merge");
const path = require("path");
const webpack = require("webpack");

//Plugins
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CircularDependencyPlugin = require("circular-dependency-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

//configuration
const aliases = require("./build-utils/path-aliases")(__dirname);
const copyPatterns = require("./build-utils/copyplugin-patterns");
const version = require("./package.json").version;

//properties
const appVersion = version.replace(/\./g, "-");

const getModeConfig = env =>
    require(`./build-utils/webpack.${env.mode}.js`)(env);

const loadPresets = (presets = []) => {
    presets = [].concat(...[presets]);

    const configs = presets.map(name => {
        return require(`./build-utils/presets/webpack.${name}`);
    });

    return merge({}, ...configs);
};

module.exports = (env) => {
    const config = merge.smart(
        {
            mode: env.mode,

            context: __dirname + "/app",

            entry: {
                polyfills: "./polyfills.ts",
                legacy: "./scripts/Shared/app.ts",
                app: "./main.ts"
            },

            output: {
                path: __dirname + "/.tmp",
                filename: `scripts/[name].${appVersion}.bundle.js`,
                chunkFilename: `scripts/[name].${appVersion}.bundle.js`
            },

            optimization: {
                splitChunks: {
                    cacheGroups: {
                        commons: {
                            chunks: "initial",
                            minChunks: 2,
                            name: "commons"
                        },
                        vendor: {
                            test(module, chunks) {
                                const name =
                                    module.nameForCondition &&
                                    module.nameForCondition();
                                return chunks.some(
                                    chunk =>
                                        !/pdfjs-dist/.test(name) &&
                                        !/chart.js/.test(name) &&
                                        (/angular-ui-tinymce/.test(name) ||
                                            !/tinymce/.test(name)) &&
                                        !/backbone/.test(name) &&
                                        !/graphlib/.test(name) &&
                                        !/dagre/.test(name) &&
                                        !/datamaps/.test(name) &&
                                        /node_modules/.test(name)
                                );
                            },
                            chunks: chunk => chunk.name !== "polyfills",
                            name: "vendor",
                            enforce: true,
                            priority: 10
                        }
                    }
                }
            },

            resolve: {
                extensions: [".ts", ".js"],
                modules: [path.resolve(__dirname, "app"), "node_modules"],
                alias: aliases
            },

            module: {
                rules: [
                    {
                        test: /\.html$/,
                        use: {
                            loader: "html-loader",
                            options: {
                                minifyCSS: false,
                                removeAttributeQuotes: false,
                                caseSensitive: true,
                                customAttrSurround: [
                                    [/#/, /(?:)/],
                                    [/\*/, /(?:)/],
                                    [/\[?\(?/, /(?:)/]
                                ],
                                customAttrAssign: [/\)?\]?=/]
                            }
                        },
                        exclude: /node_modules/
                    },
                    {
                        test: /\.jade$/,
                        use: "jade-loader",
                        exclude: /node_modules/
                    },
                    {
                        test: /\.(woff|woff2|eot|[ot]tf)$/,
                        exclude: /images/,
                        use: [
                            {
                                loader: "file-loader",
                                query: {
                                    name: "assets/[name].[ext]"
                                }
                            }
                        ]
                    },
                    {
                        test: /\.(ico|db)$/,
                        include: /images|mapdata/,
                        use: [
                            {
                                loader: "file-loader",
                                query: {
                                    name: "[path][name].[ext]"
                                }
                            }
                        ]
                    }
                ]
            },

            plugins: [
                new webpack.ProvidePlugin({
                    $: "jquery",
                    jQuery: "jquery",
                    "window.jQuery": "jquery",
                    moment: "moment"
                }),

                new HtmlWebpackPlugin({
                    template: "index.html",
                    chunks: "all",
                    chunksSortMode: function (chunk1, chunk2) {
                        var orders = [
                            "polyfills",
                            "vendor",
                            "commons",
                            "legacy",
                            "app"
                        ];
                        var order1 = orders.indexOf(chunk1.names[0]);
                        var order2 = orders.indexOf(chunk2.names[0]);
                        if (order1 > order2) {
                            return 1;
                        } else if (order1 < order2) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                }),

                new CircularDependencyPlugin({
                    exclude: /a\.js|node_modules/,
                    failOnError: true,
                    cwd: process.cwd()
                }),

                new CleanWebpackPlugin([".tmp", "aot"], {
                    root: __dirname,
                    verbose: true,
                    dry: false,
                    watch: false
                }),

                new CopyWebpackPlugin(copyPatterns),

                new webpack.DefinePlugin({
                    VERSION: JSON.stringify(version)
                }),

            ]
        },
        getModeConfig(env),
        loadPresets(env.presets)
    );

    return config;
};
