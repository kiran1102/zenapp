const webpack = require("webpack");
const path = require("path");

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, ".tmp"),
        inline: true,
        compress: true,
        port: 9000,
        open: true,
        hot: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ]
};
