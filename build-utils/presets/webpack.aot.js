const AngularCompilerPlugin = require('@ngtools/webpack').AngularCompilerPlugin;

module.exports = {
    entry: {
        polyfills: "./polyfills.ts",
        legacy: './scripts/Shared/app.ts',
        app: './main.aot.ts',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: '@ngtools/webpack',
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new AngularCompilerPlugin({
            tsConfigPath: 'tsconfig.aot.json',
            entryModule: "./app/ng2.module#Ng2Module"
        }),
    ]
};
