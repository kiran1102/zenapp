const webpack = require("webpack");
const path = require("path");
const autoprefixer = require('autoprefixer');

//configuration
const version = require("../package.json").version;

//properties
const appVersion = version.replace(/\./g, '-');

//Plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const AngularCompilerPlugin = require('@ngtools/webpack').AngularCompilerPlugin;

module.exports = function ({ mode }) {

    return {

        entry: {
            polyfills: "./polyfills.ts",
            legacy: './scripts/Shared/app.ts',
            app: './main.aot.ts',
        },

        output: {
            filename: `scripts/[name].[chunkhash].${appVersion}.bundle.js`,
            chunkFilename: `scripts/[name].[chunkhash].${appVersion}.bundle.js`,
        },

        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loader: '@ngtools/webpack',
                    exclude: /node_modules/
                },
                {
                    test: /\.(svg|jpe?g|png|gif)$/,
                    exclude: /images/,
                    use: [{
                        loader: 'file-loader',
                        query: {
                            name: "assets/[name].[ext]"
                        }
                    },
                        "image-webpack-loader"
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    include: /images|mapdata/,
                    use: [{
                        loader: 'file-loader',
                        query: {
                            name: "[path][name].[ext]"
                        },
                    },
                        "image-webpack-loader"
                    ]
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [autoprefixer];
                                }
                            }
                        },
                        'sass-loader'
                    ],
                },
            ]
        },

        plugins: [
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false
            }),

            new MiniCssExtractPlugin({
                filename: 'main-[contenthash].css',
            }),

            new webpack.DefinePlugin({
                "process.env": {
                    "NODE_ENV": JSON.stringify(mode),
                },
            }),

            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

            new AngularCompilerPlugin({
                tsConfigPath: 'tsconfig.aot.json',
                entryModule: "./app/ng2.module#Ng2Module"
            }),
        ],

    };

};
