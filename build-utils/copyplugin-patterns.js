module.exports = [
    {
        from: '../node_modules/pdfjs-dist/build/pdf.worker.min.js',
        to: 'scripts/pdf.worker.min.js'
    },
    {
        from: '../node_modules/tinymce/skins',
        to: 'scripts/skins'
    },
    {
        from: '../node_modules/tinymce/skins',
        to: 'skins'
    },
    {
        from: './Components/Cookies/Shared/optanon.css',
        to: 'scripts/optanon.css'
    },
    {
        from: './Components/Cookies/Shared/center-tile.css',
        to: 'scripts/center-tile.css'
    },
    {
        from: './Components/Cookies/Shared/skins/default_flat_bottom_two_button_black/v2/css/optanon.css',
        to: 'scripts/optanon-modern.css'
    },
    {
        from: './Components/Cookies/Shared/skins/default_flat_bottom_two_button_black/v2/images',
        to: 'images'
    },
    {
        from: './Components/Cookies/Shared/skins/default_responsive_alert_bottom_two_button_black/v2/css/optanon.css',
        to: 'scripts/optanon-classic.css'
    },
    {
        from: './Components/Cookies/Shared/skins/default_responsive_alert_bottom_two_button_black/v2/images',
        to: 'images'
    },
    {
        from: '../node_modules/@salesforce-ux/design-system/assets/icons/utility-sprite',
        to: 'assets/icons/utility-sprite'
    },
    {
        from: '../node_modules/@salesforce-ux/design-system/assets/icons/action-sprite',
        to: 'assets/icons/action-sprite'
    },
    {
        from: '../node_modules/ng-diff-match-patch/bundles/ng-diff-match-patch.umd.min.js',
        to: 'scripts/ng-diff-match-patch.umd.min.js'
    },
];
