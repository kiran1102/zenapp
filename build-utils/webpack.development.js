const webpack = require("webpack");
const path = require("path");

//plugin
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

module.exports = function ({ dev, mode, thread= false }) {
    return {
        devtool: "cheap-source-map",
        module: {
            strictExportPresence: false,
            rules: [
                {
                    test: /\.ts$/,
                    use: [
                        { loader: 'cache-loader' },
                        thread && {
                            loader: 'thread-loader',
                            options: {
                                workers: require('os').cpus().length - 1,
                                workerNodeArgs: ['--stack-size=2048', '--max-old-space-size=2048'],
                            },
                        },
                        {
                            loader: 'ts-loader',
                            options: {
                                happyPackMode: true,
                                transpileOnly: true
                            }
                        },
                        "angular2-template-loader",
                        "angular-router-loader"
                    ].filter(Boolean),
                    exclude: /node_modules/
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        thread && {
                            loader: 'thread-loader',
                            options: {
                                workers: 2,
                                workerNodeArgs: ['--stack-size=2048', '--max-old-space-size=2048'],
                            },
                        },
                        "style-loader",
                        "css-loader",
                        "postcss-loader",
                        "sass-loader"
                    ].filter(Boolean)
                },
                {
                    test: /\.(svg|jpe?g|png|gif)$/,
                    exclude: /images/,
                    use: [
                        {
                            loader: "file-loader",
                            query: {
                                name: "assets/[name].[ext]"
                            }
                        }
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg)$/,
                    include: /images|mapdata/,
                    use: [
                        {
                            loader: "file-loader",
                            query: {
                                name: "[path][name].[ext]"
                            }
                        }
                    ]
                },
            ]
        },

        devServer: {
            contentBase: path.join(__dirname, ".tmp"),
            inline: true,
            compress: true,
            port: 9000,
            open: true
        },

        plugins: [
            new webpack.DefinePlugin({
                "process.env": {
                    NODE_ENV: JSON.stringify(mode),
                    ONETRUST_DEV_ENV: JSON.stringify(dev)
                }
            }),

            new ForkTsCheckerWebpackPlugin({
                checkSyntacticErrors: true,
                tsconfig: path.resolve("./tsconfig.json")
            }),
        ]

    };
};
