var kebabCase = require('lodash.kebabcase');
const createFile = require('create-file');
const componentName = process.argv[2];
const componentNameKebabCase = kebabCase(componentName);
const haveScss = process.argv[3] === "scss";

const ctrlName = `${componentName.charAt(0).toUpperCase() + componentName.slice(1)}Ctrl`;
const tsTemplate = `import { IExtendedRootScopeService } from "interfaces/root-scope.interface";

class ${ctrlName} implements ng.IComponentController {

    static $inject: string[] = ["$rootScope"];

    private translate: Function;

    constructor(readonly $rootScope: IExtendedRootScopeService) {
        this.translate = $rootScope.t;
    }

    public $onChanges(changes: ng.IOnChangesObject): void {
        //
    }

}

export default {
    bindings: {},
    controller: ${ctrlName},
    template: \`\`,
};`;

createFile(`app/Components/${componentNameKebabCase}/${componentNameKebabCase}.component.ts`, tsTemplate, () => { console.log(`${componentNameKebabCase}.component.ts created successfully.`) });

if (haveScss) {
    const scssTemplate = `.${componentNameKebabCase} {

};`;
    createFile(`app/Components/${componentNameKebabCase}/${componentNameKebabCase}.component.scss`, scssTemplate, () => { console.log(`${componentNameKebabCase}.component.scss created successfully.`) });
}
