const exec = require("child_process").exec;

const validVersionProps = [
    "#.#.#",
    "major",
    "minor",
    "patch",
    "premajor",
    "preminor",
    "prepatch",
    "prerelease",
];
const validateVersion = /(^(\d+\.)(\d+\.)(\d+)$)|(^major$)|(^minor$)|(^patch$)|(^premajor$)|(^prepatch$)|(^prerelease$)/;
const validateTicket = /^((?!([A-Z0-9a-z]{1,10})-?$)[A-Z]{1}[A-Z0-9]+-\d+)$/;

function commit(commitPath, commitMsg, callback) {
    console.debug(`Committing to ${commitPath} with message "${commitMsg}"`);
    const commitCmd = `git add ${commitPath} && git commit -m "${commitMsg}" --no-verify`;
    console.debug(commitCmd);
    return exec(commitCmd, (err, stdout, stderr) => {
        let result = false;
        if (err) {
            console.error(err);
            result = false;
        } else {
            console.log(stdout);
            result = true;
        }
        if (callback) {
            callback(result);
        }
    });
}

const [node, script, version, ticket] = process.argv;

console.debug(`Initiating script at ${script}`);
console.debug(`Version: ${version}`);
console.debug(`Ticket: ${ticket}`);

const validateVersionResult = validateVersion.test(version);
console.debug("Version Validation Result", validateVersionResult);
if (!version || !validateVersion.test(version)) {
    console.error(`Valid version not supplied. Detected "${version}". Please specify a version by providing any one of the following.\n${validVersionProps.join(" | ")}`);
    process.exit(1);
}

const validateTicketResult = validateTicket.test(ticket);
console.debug("Ticket Validation Result", validateTicketResult);
if (!ticket || !validateTicket.test(ticket)) {
    console.error(`Valid JIRA ticket not supplied. Detected "${ticket}". Please provide a valid JIRA ticket. e.g "CORE-3294`);
    process.exit(1);
}

console.debug("Valid version and ticket provided. Updating version now.");
const versionCommand = `npm version ${version} --no-git-tag-version`;
console.debug(versionCommand);
exec(versionCommand, (err, stdout, stderr) => {
    if (err) {
        console.error(err);
        process.exit(1);
    } else {
        console.log(stdout);
        const version = stdout.trim().substr(1);
        const versionCommitMsg = `${ticket} : Updating version to ${version}.`;
        commit(".", versionCommitMsg, (result) => {
            console.debug("Commit Result: ", result);
            process.exit(result ? 0 : 1);
        });
    }
});
