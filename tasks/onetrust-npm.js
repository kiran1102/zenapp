let { execSync, spawn } = require("child_process");

function run(cmd) {
    return execSync(cmd)
        .toString()
        .replace(/(\r\n|\n|\r)/gm, "");
}

const config = {
    registry: "https://nexus.onetrust.com/nexus/repository/npm-group/",
    currentRegistry: "",
    username: "npm-developer",
    password: "OneTrustNpmNow",
    email: "",
    scope: process.argv[2] || "",
};

config.currentRegistry = run("npm get registry");

if (config.currentRegistry === config.registry) {
    console.log(`Registry already set to ${config.registry}.`);
    process.exit();
} else {
    console.log(`Registry not set to ${config.registry}.`);
}

config.email = run("git config user.email") || "";

console.debug("Config Values: ", config);

console.log(`Registering ${config.email || "anonymous user"} with OneTrust NPM.`)
run(`npm set registry=${config.registry}`);

function handleRegistrationResponse(spawn, output) {
    const outputString = output.toString().trim();
    if (outputString.startsWith("Username")) {
        registrationSpawn.stdin.write(config.username + "\n");
    } else if (outputString.startsWith("Password")) {
        registrationSpawn.stdin.write(config.password + "\n");
    } else if (outputString.startsWith("Email")) {
        registrationSpawn.stdin.write(config.email + "\n");
    } else if (outputString.startsWith("Logged in")) {
        console.debug("Registration complete, preparing to exit.");
    } else {
        console.error("Encountered unexpected error during registration.")
        process.exit(1);
    }
}

const registrationArguments = ["login", `--registry=${config.registry}`];
if (config.scope) {
    registrationArguments.push(`--scope=${config.scope}`);
}
const registrationSpawn = spawn(`npm`, registrationArguments);

registrationSpawn.stdout.on("data", function(data) {
    console.log(data.toString());
    handleRegistrationResponse(registrationSpawn, data);
});
registrationSpawn.stderr.on("data", function(data) {
    console.error(data.toString());
});
registrationSpawn.on("exit", function(code) {
    console.log(`Done... Thanks for the OneTrust NPM registration tool.\nPlease run "npm install" to continue.`);
    process.exit();
});
