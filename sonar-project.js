const sonarqubeScanner = require("sonarqube-scanner");
sonarqubeScanner({
    serverUrl: "https://nexus.onetrust.com/sonar",
    options: {
        "sonar.login": "7e1bea5a8a5366f996c22907516908872a1bea9c",
        "sonar.projectKey": "com.onetrust.ZenApp",
        "sonar.projectName": "ZenApp",
        "sonar.projectVersion": "1.0",
        "sonar.sources": "app",
        "sonar.sourceEncoding": "UTF-8",
        "sonar.exclusions": "**/node_modules/**,**/*.spec.ts",
        "sonar.tests": "app",
        "sonar.test.inclusions": "**/*.spec.ts",
        "sonar.ts.tslintconfigpath": "tslint.json",
        "sonar.typescript.lcov.reportPaths":"coverage/lcov.info",
        "sonar.typescript.exclusions": "**/node_modules/**,**/typings.d.ts,**/main.ts,**/environments/environment*.ts,**/*routing.module.ts"
    }
}, () => {});
