FROM nginx:alpine

RUN mkdir -p /usr/share/nginx/html/app

COPY app/robots.txt .tmp/
COPY .tmp/ /usr/share/nginx/html/app

EXPOSE 80
